import { Injector } from '@angular/core';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CashadvanceappService } from '../../service/cashadvanceapp.service';
import { StaffService } from '../../../loan/service/staff.service';
import { DepartmentService } from '../../../generalsetup/service/department.service';
import { AccountcodeService } from '../../../generalsetup/service/accountcode.service';
import { FundService } from '../../../generalsetup/service/fund.service';
import { ActivitycodeService } from '../../../generalsetup/service/activitycode.service';
import { CashdeclartionService } from "../../service/cashdeclaration.service";
import { ItemsetupService } from '../../../procurement/service/itemsetup.service';
import { FinancialyearService } from '../../../generalledger/service/financialyear.service';
import { CashadvancetypeService } from "../../service/cashadvancetype.service";
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'CashdeclarationFormComponent',
    templateUrl: 'cashdeclarationform.component.html'
})

export class CashdeclarationFormComponent implements OnInit {
    applyloanform: FormGroup;
    cashdeclarationList = [];
    cashdeclarationData = {};
    cashdeclarationDataheader = {};
    activitycodeList = [];
    accountcodeList = [];
    staffList = [];
    recevierList = [];
    loannameList = [];
    cashadvanceList = [];
    fundList = [];
    id: number;
    ajaxCount: number;
    itemList = [];
    DeptList = [];
    fyearList = [];
    idview: number;
    viewDisabled: boolean;
    showLastRow: boolean;
    showLastRowMinus: boolean;
    showLastRowPlus: boolean;
    listshowLastRowMinus: boolean;
    showReference: boolean = false;
    listshowLastRowPlus: boolean;
    constructor(
        private CashadvanceappService: CashadvanceappService,
        private AccountcodeService: AccountcodeService,
        private StaffService: StaffService,
        private ActivitycodeService: ActivitycodeService,
        private DepartmentService: DepartmentService,
        private ItemsetupService: ItemsetupService,
        private FundService: FundService,
        private FinancialyearService: FinancialyearService,
        private CashadvancetypeService: CashadvancetypeService,
        private CashdeclartionService: CashdeclartionService,
        private spinner: NgxSpinnerService,
        private route: ActivatedRoute,
        private router: Router

    ) { }
    showIcons() {
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));


        if (this.cashdeclarationList.length > 0) {
            this.showLastRowMinus = true;
            this.showLastRowPlus = true;
            this.listshowLastRowMinus = true;
        }

        if (this.cashdeclarationList.length > 1) {
            if (this.showLastRow == false) {
                this.listshowLastRowPlus = false;
                this.listshowLastRowMinus = true;
                this.showLastRowPlus = true;

            } else {
                this.listshowLastRowPlus = true;
                this.listshowLastRowMinus = false;

            }
        }

        if (this.cashdeclarationList.length == 1 && this.showLastRow == false) {
            this.listshowLastRowMinus = true;
            this.listshowLastRowPlus = false;

        }
        if (this.cashdeclarationList.length == 1 && this.showLastRow == true) {
            this.listshowLastRowMinus = false;
            this.listshowLastRowPlus = true;

        }
        if (this.cashdeclarationList.length == 0 && this.showLastRow == false) {
            this.showLastRowMinus = false;
            this.showLastRowPlus = true;
        }
        if (this.cashdeclarationList.length == 0 && this.showLastRow == true) {
            this.showLastRowMinus = true;
            this.showLastRowPlus = false;
        }
        console.log(this.cashdeclarationList.length);
        console.log("Show last row" + this.showLastRow);
        console.log("showLastRowMinus" + this.showLastRowMinus);
        console.log("showLastRowPlus" + this.showLastRowPlus);
        console.log("listshowLastRowMinus" + this.listshowLastRowMinus);
        console.log("listshowLastRowPlus" + this.listshowLastRowPlus);

    }
    showNewRow() {
        this.showLastRow = false;
        this.showIcons();
    }
    deletemodule() {


        this.showLastRow = true;
        this.showIcons();
    }

    ngDoCheck() {
        const change = this.ajaxCount;
        if (this.ajaxCount == 0) {
            this.editFunction();
            this.spinner.hide();
            this.ajaxCount = 10;
        }
    }

    getAmount() {
        if (parseFloat(this.cashdeclarationData['f052fquantity']) && parseFloat(this.cashdeclarationData['f052famount'])) {
            let FinalTotal = parseFloat(this.cashdeclarationData['f052fquantity']) * parseFloat(this.cashdeclarationData['f052famount'])
            this.cashdeclarationData['f052ftotalAmount'] = FinalTotal;
        } else {
            this.cashdeclarationData['f052ftotalAmount'] = 0;

        }
    }
    editFunction() {
        this.ajaxCount++;
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.CashdeclartionService.getItemsDetailId(this.id).subscribe(
                data => {
                    this.ajaxCount--;
                    if (data['result'] == '') {
                        this.prefillDetails();
                    } else {
                        this.cashdeclarationDataheader = data['result'][0];
                        this.cashdeclarationDataheader['f052fcashAdvance'] = data['result'][0]['f052fcashAdvance'];
                        this.cashdeclarationDataheader['f052fitem'] = data['result'][0]['f052fitem'];

                        this.cashdeclarationDataheader['f052fidStaff'] = data['result'][0]['f052fidStaff'];
                        this.cashdeclarationDataheader['f052fidDepartment'] = data['result'][0]['f052fdepartment'];
                        this.cashdeclarationDataheader['f052fidFinancialYear'] = data['result'][0]['f052fidFinancialYear'];
                        this.cashdeclarationDataheader['f052freceiptNo'] = data['result'][0]['f052freceiptNo'];
                        this.cashdeclarationDataheader['f052freceiptAmount'] = data['result'][0]['f052freceiptAmount'];
                        this.cashdeclarationDataheader['f052fidDeclare'] = data['result'][0]['f052fidDeclare'];

                        this.cashdeclarationDataheader['f052fstatus'] = data['result'][0]['f052fstatus'];

                        this.cashdeclarationList = data['result'];
                        this.showIcons();

                        // for(var i=0;i<this.cashdeclarationList.length;i++) {
                        //     this.cashadvanceList
                        // }
                        // this.cashadvanceappList = data['cash-details'];
                        console.log(this.cashdeclarationList);
                        this.route.paramMap.subscribe((data) => this.idview = + data.get('idview'));
                        if (data['result'][0]['f052fapprovalStatus'] > 0) {
                            // this.idview = 5;
                            this.viewDisabled = true;

                            setTimeout(function () {

                                $("#target input,ng-select,select").prop("disabled", true);
                                $("#target1 input,ng-select,select").prop("disabled", true);

                            }, 5000);
                        }
                        if (this.idview > 0) {
                            this.viewDisabled = true;

                            setTimeout(function () {

                                $("#target input,ng-select,select").prop("disabled", true);
                                $("#target1 input,ng-select,select").prop("disabled", true);

                            }, 5000);

                        }
                    }
                }
            );
        }
    }

    prefillDetails() {
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {

            this.CashadvanceappService.getItemsDetail(this.id).subscribe(
                data => {
                    console.log("asdf");
                    this.cashdeclarationDataheader = data['result'][0];
                    this.cashdeclarationDataheader['f052fcashAdvance'] = data['result'][0]['f052fcashAdvance'];
                    this.cashdeclarationDataheader['f052fitem'] = data['result'][0]['f052fitem'];

                    this.cashdeclarationDataheader['f052fidStaff'] = data['result'][0]['f052fidStaff'];
                    this.cashdeclarationDataheader['f052fidDepartment'] = data['result'][0]['f052fdepartment'];
                    this.cashdeclarationDataheader['f052fidFinancialYear'] = data['result'][0]['f052fidFinancialYear'];
                    this.cashdeclarationDataheader['f052freceiptNo'] = data['result'][0]['f052freceiptNo'];
                    this.cashdeclarationDataheader['f052freceiptAmount'] = data['result'][0]['f052freceiptAmount'];

                    this.cashdeclarationDataheader['f052fstatus'] = data['result'][0]['f052fstatus'];

                    this.cashdeclarationList = data['result'];
                    // this.cashadvanceappList = data['cash-details'];
                    console.log(this.cashdeclarationList);
                    // for (var i = 0; i < this.cashdeclarationList.length; i++) {
                    //     this.cashdeclarationList[i]['f052fitem'] = parseInt(this.cashdeclarationList[i]['f052fitem']);
                    // }

                    this.route.paramMap.subscribe((data) => this.idview = + data.get('idview'));
                    if (this.idview > 0) {

                        setTimeout(function () {

                            $("#target input,ng-select").prop("disabled", true);
                            $("#target1 input,ng-select").prop("disabled", true);

                        }, 5000);
                    }
                    this.showIcons();
                }, error => {
                    console.log(error);
                });
        }
    }

    ngOnInit() {

        this.ajaxCount = 0;
        this.spinner.show();
        // this.applyloanData['f089fidCategory'] = '';
        this.ajaxCount++;
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        if (this.id < 1) {
            this.showLastRow = false;
            this.showLastRowMinus = false;
            this.showLastRowPlus = true;
        } else {
            this.showLastRow = true;

        }
        this.route.paramMap.subscribe((data) => this.idview = + data.get('idview'));
        console.log(this.idview);

        this.ajaxCount++;
        //fund dropdown
        this.FundService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.fundList = data['result']['data'];
            }, error => {
                console.log(error);
            });

        // activity dropdown
        this.ajaxCount++;
        this.ActivitycodeService.getItemsByLevel().subscribe(
            data => {
                console.log(data);
                this.ajaxCount--;
                this.activitycodeList = data['result']['data'];
                for (var i = 0; i < this.activitycodeList.length; i++) {
                    this.activitycodeList[i]['f058fcompleteCode'] = this.activitycodeList[i]['f058fcompleteCode'];
                    this.activitycodeList[i]['f058fcompleteCode'].trim();
                }

                console.log(this.activitycodeList);
            }, error => {
                console.log(error);
            });
        // account dropdown
        this.ajaxCount++;
        this.AccountcodeService.getItemsByLevel().subscribe(
            data => {
                this.ajaxCount--;
                this.accountcodeList = data['result']['data'];
                for (var i = 0; i < this.accountcodeList.length; i++) {
                    if (this.accountcodeList[i]['f059fid'] == '1046') {
                        this.accountcodeList[i]['f059fcompleteCode'] = '00000';
                    }
                }
            }, error => {
                console.log(error);
            });

        // department dropdown
        this.ajaxCount++;
        this.DepartmentService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.DeptList = data['result']['data'];
            }, error => {
                console.log(error);
            });

        //item dropdown
        this.ajaxCount++;
        this.ItemsetupService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                // console.log(this.ajaxCount);

                this.itemList = data['result']['data'];
            }
        );
        //fyear dropdown
        this.ajaxCount++;
        this.FinancialyearService.getActiveItems().subscribe(
            data => {
                this.ajaxCount--;
                this.fyearList = data['result'];
            }, error => {
                console.log(error);
            });


        // Staff dropdown
        this.ajaxCount++;
        this.StaffService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.staffList = data['result']['data'];
            }, error => {
                console.log(error);
            });

        // cash advance id dropdown
        this.ajaxCount++;
        this.CashadvancetypeService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.cashadvanceList = data['result']['data'];
                // for(var i=0;i<this.cashadvanceList.length;i++) {
                //     this.cashadvanceList[i]['displayname'] = this.cashadvanceList[i]['f051fcashId'] +' - '+ 
                //     this.cashadvanceList[i]['f051fdescription']
                // }
                for (var i = 0; i < this.cashadvanceList.length; i++) {
                    this.cashadvanceList[i]['displayname'] = this.cashadvanceList[i]['f051fdescription']
                }
            }, error => {
                console.log(error);
            });

        let typeObject = {};
        typeObject['type'] = 'Cash';

        this.CashadvanceappService.generateReferenceNumber(typeObject).subscribe(
            data => {
                this.cashdeclarationDataheader['f052fcashAdvance'] = data['number'];
            }
        );
        this.showIcons();
    }
    saveCashadvanceapp() {
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }

        if (this.cashdeclarationDataheader['f052fidStaff'] == undefined) {
            alert("Please Select Staff Name");
            return false;
        }
        if (this.cashdeclarationDataheader['f052fidDepartment'] == undefined) {
            alert("Please Select Department");
            return false;
        }
        if (this.cashdeclarationDataheader['f052fidFinancialYear'] == undefined) {
            alert("Please Select Budget Year");
            return false;
        }
        if (this.showLastRow == false) {
            var addactivities = this.addcashadvanceapplist();
            if (addactivities == false) {
                return false;
            }

        }
        let cashadvanceappObject = {};
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        console.log(this.id);
        if (this.id > 0) {
            cashadvanceappObject['f052fid'] = this.id;
        }

        cashadvanceappObject['f052fcashAdvance'] = this.cashdeclarationDataheader['f052fcashAdvance'];
        cashadvanceappObject['f052fitem'] = this.cashdeclarationDataheader['f052fitem'];
        cashadvanceappObject['f052fidStaff'] = this.cashdeclarationDataheader['f052fidStaff'];
        cashadvanceappObject['f052fidDepartment'] = this.cashdeclarationDataheader['f052fidDepartment'];
        cashadvanceappObject['f052fidFinancialYear'] = this.cashdeclarationDataheader['f052fidFinancialYear'];
        cashadvanceappObject['f052freceiptAmount'] = this.cashdeclarationDataheader['f052freceiptAmount'];
        cashadvanceappObject['f052freceiptNo'] = this.cashdeclarationDataheader['f052freceiptNo'];
        cashadvanceappObject['f052fidCash'] = this.id;
        if (this.cashdeclarationDataheader['f052fidDeclare'] == undefined) {

        } else {
            cashadvanceappObject['f052fidDeclare'] = this.cashdeclarationDataheader['f052fidDeclare'];
        }
        cashadvanceappObject['f052fstatus'] = 0;
        cashadvanceappObject['cash-details'] = this.cashdeclarationList;
        console.log(cashadvanceappObject);
        this.CashdeclartionService.insertCashdeclartionItems(cashadvanceappObject).subscribe(
            data => {
                if (data['status'] == 409) {
                    alert('Cash Advance ID Already Exist');

                }
                else {
                    alert('Apply Cash Advance has been declared');
                    this.router.navigate(['cashadvances/cashdeclaration']);
                }

            }, error => {
                console.log(error);
            });
    }
    deletecashadvanceapp(object) {
        var cnf = confirm("Do you really want to delete");
        if (cnf == true) {
            var index = this.cashdeclarationList.indexOf(object);
            if (index > -1) {
                this.cashdeclarationList.splice(index, 1);
            }
            this.showIcons();
        }
    }
    getUOM() {
        let itemId = this.cashdeclarationData['f052fitem'];
        console.log(itemId);
        this.ItemsetupService.getItemsDetail(itemId).subscribe(
            data => {
                //this.itemList = data['result']['data'];
                console.log(data);
                this.cashdeclarationData['f052funit'] = data['result'][0]['f029funit'];
            }
        );
    }
    addcashadvanceapplist() {
        console.log(this.cashdeclarationData);


        if (this.cashdeclarationData['f052fidCashType'] == undefined) {
            alert("Please Enter Cash Type");
            return false;
        }
        if (this.cashdeclarationData['f052fitem'] == undefined) {
            alert("Please Select Item");
            return false;
        }
        if (this.cashdeclarationData['f052ffund'] == undefined) {
            alert("Please Select Fund");
            return false;
        }
        if (this.cashdeclarationData['f052factivity'] == undefined) {
            alert("Please Select Activity");
            return false;
        }
        if (this.cashdeclarationData['f052fdepartment'] == undefined) {
            alert("Please Select Cost Center");
            return false;
        }
        if (this.cashdeclarationData['f052faccount'] == undefined) {
            alert("Please Select Account Code");
            return false;
        }
        if (this.cashdeclarationData['f052fsoCode'] == undefined) {
            alert("Please Enter So Code");
            return false;
        }
        if (this.cashdeclarationData['f052famount'] == undefined) {
            alert("Please Enter Amount");
            return false;
        }
        if (this.cashdeclarationData['f052fquantity'] == undefined) {
            alert("Please Enter Quantity");
            return false;
        }
        var dataofCurrentRow = this.cashdeclarationData;
        this.cashdeclarationData = {};
        this.cashdeclarationList.push(dataofCurrentRow);
        console.log(this.cashdeclarationList);
        this.showIcons();
    }
    onlyNumberKey(event) {
        if (event.charCode == 46) {

        } else {
            return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
        }
    }
    maximize() {
        $("#myModal").addClass("modal");
        $("#overlay1").show();
        $(".maximize-icon").addClass("hidden");
        $(".minimize-icon").removeClass("hidden");
        $(".modal-backdrop").show();

    }
    minimize() {
        $("#myModal").removeClass("modal");
        $("#overlay1").hide();
        $(".maximize-icon").removeClass("hidden");
        $(".minimize-icon").addClass("hidden");
        $(".modal-backdrop").hide();
        $('#myModal').show();
    }
}


