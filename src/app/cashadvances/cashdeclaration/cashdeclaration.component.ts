
import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { ApprovalService } from '../service/approval.service'
import { CashdeclartionService } from "../service/cashdeclaration.service";
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'Cashdeclaration',
    templateUrl: 'cashdeclaration.component.html'
})

export class CashdeclarationComponent implements OnInit {

    cashdeclarationList = [];
    cashdeclarationData = {};
    selectAllCheckbox = true;

    constructor(
        private CashdeclartionService: CashdeclartionService,
        private spinner: NgxSpinnerService,
    ) { }

    ngOnInit() {
        this.getListData();
    }
    getListData() {
        this.spinner.show();
        this.CashdeclartionService.getItems().subscribe(
            data => {
                this.spinner.hide();
                this.cashdeclarationList = data['result'];
            }, error => {
                console.log(error);
            });
    }

    cashdeclartionListData() {
        console.log(this.cashdeclarationList);
        var approvaloneIds = [];
        for (var i = 0; i < this.cashdeclarationList.length; i++) {
            if (this.cashdeclarationList[i].f052fstatus == true) {
                approvaloneIds.push(this.cashdeclarationList[i].f052fid);
            }
        }
        var approvaloneUpdate = {};
        approvaloneUpdate['id'] = approvaloneIds;
        this.CashdeclartionService.updateCashdeclartionItems(approvaloneUpdate).subscribe(
            data => {
                alert(" Updated Sucessfully ! ");

                this.getListData();
            }, error => {
                console.log(error);
            });
    }

    selectAll() {
        console.log("asdf");
        console.log(this.selectAllCheckbox);
        for (var i = 0; i < this.cashdeclarationList.length; i++) {
            this.cashdeclarationList[i].f052fapprovalStatus = this.selectAllCheckbox;
        }
        console.log(this.cashdeclarationList);
    }

}
