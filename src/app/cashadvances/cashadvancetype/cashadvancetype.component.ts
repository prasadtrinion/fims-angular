
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CashadvancetypeService } from '../service/cashadvancetype.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'Cashadvancetype',
    templateUrl: 'cashadvancetype.component.html'
})
export class CashadvancetypeComponent implements OnInit {
    cashadvancesList = [];
    cashadvancesData = {};
    id: number;
    constructor(
        private CashadvancetypeService: CashadvancetypeService,
        private spinner: NgxSpinnerService,                

    ) { }
    ngOnInit() {
        this.spinner.show();                                        
        this.CashadvancetypeService.getItems().subscribe(
            data => {
                this.spinner.hide();
                this.cashadvancesList = data['result']['data'];
                
            }, error => {
                console.log(error);
            });
    }
}

