import { Injector } from '@angular/core';
import { Component, OnInit, ViewChild } from '@angular/core';
import { CashadvancetypeService } from '../../service/cashadvancetype.service';
import { FundService } from '../../../generalsetup/service/fund.service';
import { ActivitycodeService } from '../../../generalsetup/service/activitycode.service';
import { AccountcodeService } from '../../../generalsetup/service/accountcode.service';
import { DepartmentService } from '../../../generalsetup/service/department.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'CashadvancetypeFormComponent',
    templateUrl: 'cashadvancetypeform.component.html'
})

export class CashadvancetypeFormComponent implements OnInit {
    cashadvancesform: FormGroup;
    cashadvancesList = [];
    cashadvancesData = {};
    DeptList = [];
    fundList = [];
    accountcodeList = [];
    activitycodeList = [];
    ajaxCount: number;
    id: number;
    constructor(

        private CashadvancetypeService: CashadvancetypeService,
        private ActivitycodeService: ActivitycodeService,
        private AccountcodeService: AccountcodeService,
        private DepartmentService: DepartmentService,
        private spinner: NgxSpinnerService,
        private FundService: FundService,
        private route: ActivatedRoute,
        private router: Router,
    ) { }
    ngDoCheck() {
        if (this.ajaxCount == 0) {
            this.spinner.hide();
        }
    }
    ngOnInit() {
        this.ajaxCount = 0;
        this.spinner.show();
        this.cashadvancesData['f051fstatus'] = 1;
        this.cashadvancesform = new FormGroup({
            cashadvances: new FormControl('', Validators.required),
        });
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.ajaxCount++;
        this.FundService.getActiveItems().subscribe(
            data => {
                this.ajaxCount--;
                this.fundList = data['result']['data'];
            }, error => {
                console.log(error);
            });
        // activity dropdown
        this.ajaxCount++;
        this.ActivitycodeService.getItemsByLevel().subscribe(
            data => {
                this.ajaxCount--;
                this.activitycodeList = data['result']['data'];
                for (var i = 0; i < this.activitycodeList.length; i++) {
                    this.activitycodeList[i]['f058fcompleteCode'] = this.activitycodeList[i]['f058fcompleteCode'];
                    this.activitycodeList[i]['f058fcompleteCode'].trim();
                }
            }, error => {
                console.log(error);
            });
        // account dropdown
        this.ajaxCount++;
        this.AccountcodeService.getItemsByLevel().subscribe(
            data => {
                this.ajaxCount--;
                this.accountcodeList = data['result']['data'];
            }
        );
        // department dropdown
        this.ajaxCount++;
        this.DepartmentService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.DeptList = data['result']['data'];
            }, error => {
                console.log(error);
            });
        if (this.id > 0) {
            this.ajaxCount++;
            this.CashadvancetypeService.getItemsDetail(this.id).subscribe(
                data => {
                    this.ajaxCount--;
                    this.cashadvancesData = data['result'][0];
                    this.cashadvancesData['f051fstatus'] = parseInt(this.cashadvancesData['f051fstatus']);
                    // this.cashadvancesData['f051ffund'] = parseInt(this.cashadvancesData['f051ffund']);
                    console.log(this.cashadvancesData);
                }, error => {
                    console.log(error);
                });
        }
        let typeObject = {};
        typeObject['type'] = 'CAT';

        this.CashadvancetypeService.generateReferenceNumber(typeObject).subscribe(
            data => {
                this.cashadvancesData['f051fcashId'] = data['number'];
            }
        );
    }
    addCashadvances() {
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.CashadvancetypeService.updateCashadvancetypeItems(this.cashadvancesData, this.id).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert('Cash Advance Type Already Exist');
                    }
                    else {
                        this.router.navigate(['cashadvances/cashadvancetype']);
                        alert(" Cash Advance Type has been Updated Sucessfully!!");
                    }
                }, error => {
                    alert('Cash Advance Type Already Exist');
                    return false;
                });
        } else {
            this.CashadvancetypeService.insertCashadvancetypeItems(this.cashadvancesData).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert('Cash Advance Type Already Exist');
                        return false;
                    }
                    this.router.navigate(['cashadvances/cashadvancetype']);
                    alert("Cash Advance Type has been Added Sucessfully !!");
                }, error => {
                    console.log(error);
                });
        }
    }
}


