import { Injector } from '@angular/core';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CashadvanceappService } from '../../service/cashadvanceapp.service';
import { StaffService } from '../../../loan/service/staff.service';
import { AccountcodeService } from '../../../generalsetup/service/accountcode.service';
import { FundService } from '../../../generalsetup/service/fund.service';
import { ActivitycodeService } from '../../../generalsetup/service/activitycode.service';
import { DepartmentService } from '../../../generalsetup/service/department.service';
import { ItemsetupService } from '../../../procurement/service/itemsetup.service';
import { PurchaserequistionentryService } from '../../../procurement/service/purchaserequistionentry.service'
import { FinancialyearService } from '../../../generalledger/service/financialyear.service';
import { CashadvancetypeService } from "../../service/cashadvancetype.service";
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'CashadvanceappFormComponent',
    templateUrl: 'cashadvanceappform.component.html'
})

export class CashadvanceappFormComponent implements OnInit {
    applyloanform: FormGroup;
    cashadvanceappList = [];
    cashadvanceappData = {};
    cashadvanceappDataheader = {};
    activitycodeList = [];
    accountcodeList = [];
    budgetcostcenterList = [];
    costCenterFund = [];
    costCenterAccount = [];
    costCenterActivity = [];
    staffList = [];
    recevierList = [];
    loannameList = [];
    cashadvanceList = [];
    fundList = [];
    itemUnitList = [];
    id: number;
    ajaxCount: number;
    itemList = [];
    DeptList = [];
    financialyearList = [];
    deleteList = [];
    idview: number;
    showLastRow: boolean;
    showLastRowMinus: boolean;
    showLastRowPlus: boolean;
    listshowLastRowMinus: boolean;
    showReference: boolean = false;
    listshowLastRowPlus: boolean;
    viewDisabled: boolean;
    title: string;

    constructor(
        private CashadvanceappService: CashadvanceappService,
        private AccountcodeService: AccountcodeService,
        private StaffService: StaffService,
        private ActivitycodeService: ActivitycodeService,
        private DepartmentService: DepartmentService,
        private FundService: FundService,
        private ItemsetupService: ItemsetupService,
        private PurchaserequistionentryService: PurchaserequistionentryService,
        private FinancialyearService: FinancialyearService,
        private CashadvancetypeService: CashadvancetypeService,
        private spinner: NgxSpinnerService,
        private route: ActivatedRoute,
        private router: Router

    ) { }

    showIcons() {
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        console.log(this.cashadvanceappList.length);
        console.log("Show last row" + this.showLastRow);
        console.log("showLastRowMinus" + this.showLastRowMinus);
        console.log("showLastRowPlus" + this.showLastRowPlus);
        console.log("listshowLastRowMinus" + this.listshowLastRowMinus);
        console.log("listshowLastRowPlus" + this.listshowLastRowPlus);

        if (this.cashadvanceappList.length > 0) {
            this.showLastRowMinus = true;
            this.showLastRowPlus = true;
            this.listshowLastRowMinus = true;
        }
        if (this.cashadvanceappList.length > 1) {
            if (this.showLastRow == false) {
                this.listshowLastRowPlus = false;
                this.listshowLastRowMinus = true;
                this.showLastRowPlus = true;
            } else {
                this.listshowLastRowPlus = true;
                this.listshowLastRowMinus = false;
            }
        }
        if (this.cashadvanceappList.length == 1 && this.showLastRow == false) {
            this.listshowLastRowMinus = true;
            this.listshowLastRowPlus = false;

        }
        if (this.cashadvanceappList.length == 1 && this.showLastRow == true) {
            this.listshowLastRowMinus = false;
            this.listshowLastRowPlus = true;

        }
        if (this.cashadvanceappList.length == 0 && this.showLastRow == false) {
            this.showLastRowMinus = false;
            this.showLastRowPlus = true;
        }
        if (this.cashadvanceappList.length == 0 && this.showLastRow == true) {
            this.showLastRowMinus = true;
            this.showLastRowPlus = false;
        }
        if (this.cashadvanceappList.length > 0) {
            if (this.viewDisabled == true || this.cashadvanceappList[0]['f017fapprovedStatus'] == 1 || this.cashadvanceappList[0]['f017fapprovedStatus'] == 2) {
                this.listshowLastRowPlus = false;
            }
        }
        else {
            if (this.viewDisabled == true) {
                this.listshowLastRowPlus = false;
            }
        }
    }

    deletemodule() {
        this.showLastRow = true;
        this.showIcons();
    }

    ngDoCheck() {
        this.showIcons();
        const change = this.ajaxCount;
        if (this.ajaxCount == 0) {
            this.editFunction();
            this.ajaxCount = 10;
            this.spinner.hide();
        }
    }
    getAmount() {
        console.log('get Amount');

        if (parseFloat(this.cashadvanceappData['f052fquantity']) && parseFloat(this.cashadvanceappData['f052famount'])) {
            let FinalTotal = parseFloat(this.cashadvanceappData['f052fquantity']) * parseFloat(this.cashadvanceappData['f052famount'])
            this.cashadvanceappData['f052ftotalAmount'] = FinalTotal;
        } else {
            this.cashadvanceappData['f052ftotalAmount'] = 0;
        }
        this.showIcons();
    }
    getAmount1() {
        console.log('get Amount1');
        for (var i = 0; i < this.cashadvanceappList.length; i++) {
            if (parseFloat(this.cashadvanceappList[i]['f052fquantity']) && parseFloat(this.cashadvanceappList[i]['f052famount'])) {
                let FinalTotal = parseFloat(this.cashadvanceappList[i]['f052fquantity']) * parseFloat(this.cashadvanceappList[i]['f052famount'])
                this.cashadvanceappList[i]['f052ftotalAmount'] = FinalTotal;
            } else {
                this.cashadvanceappList[i]['f052ftotalAmount'] = 0;
            }
        }
    }
    editFunction() {
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.ajaxCount++;
            this.showReference = true;
            this.CashadvanceappService.getItemsDetail(this.id).subscribe(
                data => {
                    this.ajaxCount--;
                    this.cashadvanceappDataheader = data['result'][0];

                    this.cashadvanceappDataheader['f052fcashAdvance'] = data['result'][0]['f052fcashAdvance'];
                    this.cashadvanceappDataheader['f052fidStaff'] = data['result'][0]['f052fidStaff'];
                    this.cashadvanceappDataheader['f052fidDepartment'] = data['result'][0]['f052fdepartment'];
                    this.cashadvanceappDataheader['f052fidFinancialYear'] = data['result'][0]['f052fidFinancialYear'];
                    this.cashadvanceappDataheader['f052fstatus'] = data['result'][0]['f052fstatus'];

                    this.cashadvanceappList = data['result'];

                    this.getAllpurchaseGlList()

                    this.route.paramMap.subscribe((data) => this.idview = + data.get('idview'));
                    if (data['result'][0]['f052fapprovalStatus'] > 0) {
                        // this.idview = 5;
                        this.viewDisabled = true;

                        setTimeout(function () {

                            $("#target input,ng-select,select").prop("disabled", true);
                            $("#target1 input,ng-select,select").prop("disabled", true);

                        }, 5000);
                    }
                    if (this.idview > 0) {
                        this.viewDisabled = true;

                        setTimeout(function () {

                            $("#target input,ng-select,select").prop("disabled", true);
                            $("#target1 input,ng-select,select").prop("disabled", true);

                        }, 5000);
                        this.getAmount();
                        this.showIcons();
                    }

                }, error => {
                    //console.log(error);
                });
        }
    }
    showNewRow() {
        var confirmPop = confirm("Do you want to Add?");
        if (confirmPop == false) {
            return false;
        }
        this.showLastRow = false;
        this.showIcons();
    }
    ngOnInit() {
        this.ajaxCount = 0;
        this.spinner.show();
        this.viewDisabled = false
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        if (this.id < 1) {
            this.showLastRow = false;
            this.showLastRowMinus = false;
            this.showLastRowPlus = true;
        } else {
            this.showLastRow = true;
        }
        this.viewDisabled = false;
        this.route.paramMap.subscribe((data) => this.idview = + data.get('idview'));
        switch (this.idview) {
            case 0: this.title = "Apply Cash Advance";
                break;
            case 2: this.title = "Approve Cash Advance";
                break;
            case 3: this.title = "Cash Advance Process";
                break;
            case 4: this.title = "Cash Declaration";
                break;
            case 5: this.title = "Cash Advance Listing";
                break;
        }
        this.ajaxCount++;
        this.FundService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.fundList = data['result']['data'];
            }, error => {

            });

        // activity dropdown
        this.ajaxCount++;
        this.ActivitycodeService.getItemsByLevel().subscribe(
            data => {
                this.ajaxCount--;
                this.activitycodeList = data['result']['data'];
                for (var i = 0; i < this.activitycodeList.length; i++) {
                    this.activitycodeList[i]['f058fcompleteCode'] = this.activitycodeList[i]['f058fcompleteCode'];
                    this.activitycodeList[i]['f058fcompleteCode'].trim();
                }
            }, error => {

            });
        //item dropdown
        this.ajaxCount++;
        this.ItemsetupService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.itemList = data['result']['data'];
            }
        );
        // account dropdown
        this.ajaxCount++;
        this.AccountcodeService.getItemsByLevel().subscribe(
            data => {
                this.ajaxCount--;
                this.accountcodeList = data['result']['data'];
            }, error => {

            });

        // department dropdown
        this.ajaxCount++;
        this.DepartmentService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.DeptList = data['result']['data'];
            }, error => {

            });

        this.ajaxCount++;
        //fyear dropdown
        this.FinancialyearService.getActiveItems().subscribe(
            data => {
                this.ajaxCount--;
                this.financialyearList = data['result'];
            }, error => {

            });


        // financial year
        // if (this.id < 0 || this.id == undefined) {
        //     this.FinancialyearService.getActiveItems().subscribe(
        //         data => {

        //             this.financialyearList = data['result'];
        //             if (data['result'].length == 1) {
        //                 this.cashadvanceappDataheader['f052fidFinancialYear'] = data['result'][0]['f015fid'];

        //             }
        //         }, error => {
        //             console.log(error);
        //         });
        // }


        // Staff dropdown
        this.ajaxCount++;
        this.StaffService.getActiveList().subscribe(
            data => {
                this.ajaxCount--;
                this.staffList = data['result']['data'];
                var staffId = localStorage.getItem('staffId');
                this.cashadvanceappDataheader['f052fidStaff'] = staffId;
            }, error => {
                //console.log(error);
            });

        // cash advance id dropdown
        this.ajaxCount++;
        this.CashadvancetypeService.getActiveItems().subscribe(
            data => {
                this.ajaxCount--;
                this.cashadvanceList = data['result']['data'];
                for (var i = 0; i < this.cashadvanceList.length; i++) {
                    this.cashadvanceList[i]['displayname'] = this.cashadvanceList[i]['f051fdescription']
                }
            }, error => {
                //console.log(error);
            });

        let typeObject = {};
        typeObject['type'] = 'Cash';
        this.ajaxCount++;
        this.CashadvanceappService.generateReferenceNumber(typeObject).subscribe(
            data => {
                this.ajaxCount--;
                this.cashadvanceappDataheader['f052fcashAdvance'] = data['number'];
            }
        );
        this.showIcons();
    }
    saveCashadvanceapp(ids) {
        if (this.showLastRow == false) {
            var addactivities = this.addcashadvanceapplist(ids);
            if (addactivities == false) {
                return false;
            }

        }
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }

        if (this.cashadvanceappDataheader['f052fidStaff'] == undefined) {
            alert(" Select Staff Name");
            return false;
        }
        if (this.cashadvanceappDataheader['f052fidDepartment'] == undefined) {
            alert(" Select Department");
            return false;
        }
        if (this.cashadvanceappDataheader['f052fidFinancialYear'] == undefined) {
            alert(" Select Financial Year");
            return false;
        }

        let cashadvanceappObject = {};
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        if (this.id > 0) {
            cashadvanceappObject['f052fid'] = this.id;
        }
        cashadvanceappObject['f052fcashAdvance'] = this.cashadvanceappDataheader['f052fcashAdvance'];
        cashadvanceappObject['f052fidStaff'] = this.cashadvanceappDataheader['f052fidStaff'];
        cashadvanceappObject['f052fidDepartment'] = this.cashadvanceappDataheader['f052fidDepartment'];
        cashadvanceappObject['f052fidFinancialYear'] = this.cashadvanceappDataheader['f052fidFinancialYear'];


        cashadvanceappObject['f052fstatus'] = 0;
        cashadvanceappObject['cash-details'] = this.cashadvanceappList;

        let totalAmountOfCashAdvance = 0;
        for (var i = 0; i < this.cashadvanceappList.length; i++) {
            totalAmountOfCashAdvance = totalAmountOfCashAdvance + parseInt(this.cashadvanceappList[i]['f052ftotalAmount']);
        }
        cashadvanceappObject['f052ftotal'] = totalAmountOfCashAdvance;

        if (this.id > 0) {
            this.CashadvanceappService.updateCashadvanceappItems(cashadvanceappObject, this.id).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert('Cash Advance Already Exist');
                    }
                    else if (data['status'] == 411) {
                        alert('You have exceeded the cash advance limit');
                    }
                    else {
                        alert('Apply Cash Advance has been Updated Sucessfully');
                        this.router.navigate(['cashadvances/cashadvanceapp']);

                    }
                }, error => {

                });
        } else {
            this.CashadvanceappService.insertCashadvanceappItems(cashadvanceappObject).subscribe(
                data => {

                    if (data['status'] == 409) {
                        alert('Cash Advance Already Exist');

                    } else if (data['status'] == 411) {
                        alert('You have exceeded the cash advance limit');
                    }
                    else {
                        alert('Apply Cash Advance has been Added Sucessfully');
                        this.router.navigate(['cashadvances/cashadvanceapp']);
                    }

                }, error => {

                });
        }
    }
    getAllpurchaseGlList() {
        var idfinancialYear = this.cashadvanceappDataheader['f052fidFinancialYear'];

        this.PurchaserequistionentryService.urlgetAllpurcahseGls(idfinancialYear).subscribe(
            data => {
                var depArray = data['result']['Department'];
                this.budgetcostcenterList = depArray;

                this.costCenterFund = data['result']['Fund'];

                this.costCenterAccount = data['result']['Account'];

                this.costCenterActivity = data['result']['Activity'];

                console.log(data);
            }, error => {
                console.log(error);
            }
        );
    }
    deletecashadvanceapp(object) {
        var cnf = confirm("Do you want to delete");
        if (cnf == true) {
            var index = this.cashadvanceappList.indexOf(object);
            this.deleteList.push(this.cashadvanceappList[index]['f052fidDetails']);
            let deleteObject = {};
            deleteObject['id'] = this.deleteList;
            this.CashadvanceappService.deleteItems(deleteObject).subscribe(
                data => {

                }, error => {
                    console.log(error);
                });
            if (index > -1) {
                this.cashadvanceappList.splice(index, 1);
            }
            this.showIcons();
        }
    }
    getUOM() {
        let itemId = this.cashadvanceappData['f052fitem'];
        console.log(itemId);
        this.ItemsetupService.getItemsDetail(itemId).subscribe(
            data => {
                console.log(data);
                this.cashadvanceappData['f052funit'] = data['result'][0]['f029funit'];
            }
        );
    }
    addcashadvanceapplist(ids) {
        if (this.cashadvanceappData['f052fidCashType'] == undefined) {
            alert(" Select Cash Advance Type");
            return false;
        }
        if (this.cashadvanceappData['f052fitem'] == undefined) {
            alert(" Select Item");
            return false;
        }
        if (this.cashadvanceappData['f052ffund'] == undefined) {
            alert(" Select Fund");
            return false;
        }
        if (this.cashadvanceappData['f052factivity'] == undefined) {
            alert(" Select Activity Code");
            return false;
        }
        if (this.cashadvanceappData['f052fdepartment'] == undefined) {
            alert(" Select Cost Center");
            return false;
        }
        if (this.cashadvanceappData['f052faccount'] == undefined) {
            alert(" Select Account Code");
            return false;
        }
        if (this.cashadvanceappData['f052fbudgetFund'] == undefined) {
            alert(" Select Budget Fund");
            return false;
        }
        if (this.cashadvanceappData['f052fbudgetActivity'] == undefined) {
            alert(" Select Budget Activity Code");
            return false;
        }
        if (this.cashadvanceappData['f052fbudgetDepartment'] == undefined) {
            alert(" Select Budget Cost Center");
            return false;
        }
        if (this.cashadvanceappData['f052fbudgetAccount'] == undefined) {
            alert(" Select Budget Account Code");
            return false;
        }
        if (this.cashadvanceappData['f052fsoCode'] == undefined) {
            alert(" Enter So Code");
            return false;
        }
        if (this.cashadvanceappData['f052famount'] == undefined) {
            alert(" Enter Amount");
            return false;
        }
        if (this.cashadvanceappData['f052fquantity'] == undefined) {
            alert(" Enter Quantity");
            return false;
        }
        var confirmPop = confirm("Do you want to Add?");
        var dataofCurrentRow = this.cashadvanceappData;
        this.cashadvanceappData = {};
        this.cashadvanceappList.push(dataofCurrentRow);
        this.showIcons();
        let totalAmountOfCashAdvance = 0;
        for (var i = 0; i < this.cashadvanceappList.length; i++) {
            totalAmountOfCashAdvance = totalAmountOfCashAdvance + parseInt(this.cashadvanceappList[i]['f052ftotalAmount']);
        }
        this.cashadvanceappDataheader['f052ftotal'] = totalAmountOfCashAdvance;
    }
    onlyNumberKey(event) {
        if (event.charCode == 46) {

        } else {
            return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
        }
    }
    maximize() {
        $("#myModal").addClass("modal");
        $("#overlay1").show();
        $(".maximize-icon").addClass("hidden");
        $(".minimize-icon").removeClass("hidden");
        $(".modal-backdrop").show();

    }
    minimize() {
        $("#myModal").removeClass("modal");
        $("#overlay1").hide();
        $(".maximize-icon").removeClass("hidden");
        $(".minimize-icon").addClass("hidden");
        $(".modal-backdrop").hide();
        $('#myModal').show();

    }  
}


