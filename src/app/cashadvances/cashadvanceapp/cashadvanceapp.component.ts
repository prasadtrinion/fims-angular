import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CashadvanceappService } from '../service/cashadvanceapp.service'
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'Cashadvanceapp',
    templateUrl: 'cashadvanceapp.component.html'
})
export class CashadvanceappComponent implements OnInit {
    cashadvanceappList = [];
    cashadvanceappData = {};
    id: number;
    constructor(
        private CashadvanceappService: CashadvanceappService,
        private spinner: NgxSpinnerService,
    ) { }

    ngOnInit() {
        this.spinner.show();
        this.CashadvanceappService.getItems().subscribe(
            data => {
                this.spinner.hide();
                let cashadvancedata = [];
                cashadvancedata = data['result']['data'];
                for (var i = 0; i < cashadvancedata.length; i++) {
                    if (cashadvancedata[i]['f052fapprovalStatus'] == 1) {
                        cashadvancedata[i]['f052fapprovalStatus'] = 'Approved';
                    } else if (cashadvancedata[i]['f052fapprovalStatus'] == 2) {
                        cashadvancedata[i]['f052fapprovalStatus'] = 'Rejected';
                    } else {
                        cashadvancedata[i]['f052fapprovalStatus'] = 'Pending';
                    }
                }
                this.cashadvanceappList = cashadvancedata;
            }, error => {
                console.log(error);
            });
    }

}