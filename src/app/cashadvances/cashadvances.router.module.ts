import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CashadvanceappComponent } from "./cashadvanceapp/cashadvanceapp.component";
import { CashadvanceappFormComponent } from "./cashadvanceapp/cashadvanceappform/cashadvanceappform.component";

import { CashadvanceapprovalComponent } from "./cashadvanceapproval/cashadvanceapproval.component";
import { CashadvanceprocessComponent } from "./cashadvanceprocess/cashadvanceprocess.component";

import { CashadvancetypeComponent } from "./cashadvancetype/cashadvancetype.component";
import { CashadvancetypeFormComponent } from "./cashadvancetype/cashadvancetypeform/cashadvancetypeform.component";

import { StafftypeComponent } from "../cashadvances/stafftype/stafftype.component";
import { StafftypeFormComponent } from "../cashadvances/stafftype/stafftypeform/stafftypeform.component";

import { CashadvancerateComponent } from "../cashadvances/cashadvancerate/cashadvancerate.component";
import { CashadvancerateFormComponent } from "../cashadvances/cashadvancerate/cashadvancerateform/cashadvancerateform.component";

import { CashadvancelimitComponent } from "../cashadvances/cashadvancelimit/cashadvancelimit.component";
import { CashadvancelimitFormComponent } from "../cashadvances/cashadvancelimit/cashadvancelimitform/cashadvancelimitform.component";

import { CashdeclarationComponent } from "./cashdeclaration/cashdeclaration.component";
import { CashdeclarationFormComponent } from "./cashdeclaration/cashdeclarationform/cashdeclarationform.component";
import { CashadvancelistingComponent } from "./cashadvancelisting/cashadvancelisting.component";
import { DeclarationapprovalComponent } from "./declarationapproval/declarationapproval.component";

const routes: Routes = [

  { path: 'cashadvanceapp', component: CashadvanceappComponent },
  { path: 'declarationapproval', component: DeclarationapprovalComponent },
  { path: 'addnewcashadvanceapp', component: CashadvanceappFormComponent },
  { path: 'editcashadvanceapp/:id/:idview', component: CashadvanceappFormComponent },
  { path: 'viewcashadvanceapp/:id/:idview', component: CashadvanceappFormComponent },
  { path: 'cashadvanceapproval', component: CashadvanceapprovalComponent },
  { path: 'cashadvanceprocess', component: CashadvanceprocessComponent },
  { path: 'cashadvancetype', component: CashadvancetypeComponent },

  { path: 'stafftype', component: StafftypeComponent },
  { path: 'addnewstafftype', component: StafftypeFormComponent },
  { path: 'editstafftype/:id', component: StafftypeFormComponent },

  { path: 'cashadvancerate', component: CashadvancerateComponent },
  { path: 'addnewcashadvancerate', component: CashadvancerateFormComponent },
  { path: 'editcashadvancerate/:id', component: CashadvancerateFormComponent },


  { path: 'addnewcashadvancetype', component: CashadvancetypeFormComponent },
  { path: 'editcashadvancetype/:id', component: CashadvancetypeFormComponent },
  { path: 'cashdeclaration', component: CashdeclarationComponent },
  { path: 'addcashdeclaration/:id/:idview', component: CashadvanceappFormComponent },

  { path: 'cashdeclarationform', component: CashdeclarationFormComponent },
  { path: 'cashadvancelisting', component: CashadvancelistingComponent },
  { path: 'editcashdeclaration/:id', component: CashdeclarationFormComponent },

  { path: 'cashadvancelimit', component: CashadvancelimitComponent },
  { path: 'addnewcashadvancelimit', component: CashadvancelimitFormComponent },
  { path: 'editcashadvancelimit/:id', component: CashadvancelimitFormComponent },

];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],

})
export class CashadvancesRoutingModule {

}