
import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { CashadvanceprocessService } from "../service/cashadvanceprocess.service";
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'Cashadvanceprocess',
    templateUrl: 'cashadvanceprocess.component.html'
})

export class CashadvanceprocessComponent implements OnInit {

    cashadvanceprocessList = [];
    cashadvanceprocessData = {};
    selectAllCheckbox = true;

    constructor(
        private CashadvanceprocessService: CashadvanceprocessService,
        private spinner: NgxSpinnerService,
    ) { }

    ngOnInit() {
        this.getListData();
    }
    getListData() {
        this.spinner.show();
        this.CashadvanceprocessService.getItemsDetail().subscribe(
            data => {
                this.spinner.hide();
                this.cashadvanceprocessList = data['result'];
            }, error => {
                console.log(error);
            });
    }

    cashadvanceprocessListData() {
        var confirmPop = confirm("Do you want to process?");
        if (confirmPop == false) {
            return false;
        }
        console.log(this.cashadvanceprocessList);
        var approvaloneIds = [];
        for (var i = 0; i < this.cashadvanceprocessList.length; i++) {
            if (this.cashadvanceprocessList[i].processStatus == true) {
                approvaloneIds.push(this.cashadvanceprocessList[i].f052fid);
            }
        }
        if (approvaloneIds.length > 0) {

        } else {
            alert(" Select atleast one Cash Advance to Process");
            return false;
        }
        var approvaloneUpdate = {};
        approvaloneUpdate['id'] = approvaloneIds;
        this.CashadvanceprocessService.insertCashadvanceapprovalItems(approvaloneUpdate).subscribe(
            data => {
                alert("Cash advance has been Processed");

                this.getListData();
            }, error => {
                console.log(error);
            });
    }
}
