
import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DeclarationapprovalService } from '../service/declarationapproval.service'
import { AlertService } from './../../_services/alert.service';
@Component({
    selector: 'Declarationapproval',
    templateUrl: 'declarationapproval.component.html'
})

export class DeclarationapprovalComponent implements OnInit {
    applyloanapprovalList = [];
    applyloanapprovalData = {};
    selectAllCheckbox = true;
    level: number;
    status: number;
    typeText: string;
    approveBtnLabel: string;
    rejectBtnLabel: string;
    constructor(
        private DeclarationapprovalService: DeclarationapprovalService,
        private AlertService: AlertService,
        private route: ActivatedRoute,
    ) { }

    ngOnInit() {
        this.getListData();
    }
    getListData() {

        this.DeclarationapprovalService.getItemsDetail(0).subscribe(
            data => {
                console.log(data['result']);
                this.applyloanapprovalList = data['result']['data'];
            }, error => {
                console.log(error);
            });
    }
    applyloanapprovalListData() {
        this.route.paramMap.subscribe((data) => this.level = + data.get('level'));
        console.log(this.applyloanapprovalList);
        var approvaloneIds = [];
        var approvalId = 0;
        for (var i = 0; i < this.applyloanapprovalList.length; i++) {
            if (this.applyloanapprovalList[i].fapprovalStatus == true) {
                approvaloneIds.push(this.applyloanapprovalList[i].f052fidDeclare);
            }
        }
        var approvaloneUpdate = {};
        approvaloneUpdate['id'] = approvaloneIds;
        approvaloneUpdate['level'] = this.level;
        approvaloneUpdate['status'] = '1';
        approvaloneUpdate['reason'] = this.applyloanapprovalData['reason'];
        this.DeclarationapprovalService.updateApplyloanapprovalItems(approvaloneUpdate).subscribe(
            data => {
                this.AlertService.success(" Updated Sucessfully ! ");

                this.getListData();
            }, error => {
                console.log(error);
            });
    }
    applyloanRejectListData() {
        this.route.paramMap.subscribe((data) => this.level = + data.get('level'));

        console.log(this.applyloanapprovalList);
        var approvaloneIds = [];
        var approvalId = 0;
        for (var i = 0; i < this.applyloanapprovalList.length; i++) {
            if (this.applyloanapprovalList[i].fapprovalStatus == true) {
                approvaloneIds.push(this.applyloanapprovalList[i].f052fidDeclare);
            }
        }
        var approvaloneUpdate = {};
        approvaloneUpdate['id'] = approvaloneIds;
        approvaloneUpdate['level'] = this.level;
        approvaloneUpdate['status'] = '2';
        approvaloneUpdate['reason'] = this.applyloanapprovalData['reason'];
        this.DeclarationapprovalService.updateApplyloanapprovalItems(approvaloneUpdate).subscribe(
            data => {
                this.AlertService.success(" Updated Sucessfully ! ");

                this.getListData();
            }, error => {
                console.log(error);
            });
    }
}
