import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { MaterialModule } from '../material/material.module';
import { HttpClientModule, HttpClient, HTTP_INTERCEPTORS } from '@angular/common/http';
import { DataTableModule } from "angular-6-datatable";
import { Ng2SearchPipeModule } from 'ng2-search-filter';

import { CashadvanceappComponent } from "./cashadvanceapp/cashadvanceapp.component";
import { CashadvanceappFormComponent } from "./cashadvanceapp/cashadvanceappform/cashadvanceappform.component";
import { CashadvanceappService } from "./service/cashadvanceapp.service";
import { AssetItemService } from './../assets/service/assetitem.service';

import { StaffService } from "../loan/service/staff.service";
import { CashadvancesRoutingModule } from './cashadvances.router.module';

import { StafftypeComponent } from "../cashadvances/stafftype/stafftype.component";
import { StafftypeFormComponent } from "../cashadvances/stafftype/stafftypeform/stafftypeform.component";
import { StafftypeService } from "./service/stafftype.service";

import { CashadvancerateComponent } from "../cashadvances/cashadvancerate/cashadvancerate.component";
import { CashadvancerateFormComponent } from "../cashadvances/cashadvancerate/cashadvancerateform/cashadvancerateform.component";
import { CashadvancerateService } from "./service/cashadvancerate.service";

import { CashadvancelimitComponent } from "../cashadvances/cashadvancelimit/cashadvancelimit.component";
import { CashadvancelimitFormComponent } from "../cashadvances/cashadvancelimit/cashadvancelimitform/cashadvancelimitform.component";
import { CashadvancelimitService } from "./service/cashadvancelimit.service";

import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { JwtInterceptor } from '../_helpers/jwt.interceptors';

import { CashadvanceapprovalComponent } from "./cashadvanceapproval/cashadvanceapproval.component";
import { CashadvanceapprovalService } from "./service/cashadvanceapproval.service";

import { CashadvanceprocessComponent } from "./cashadvanceprocess/cashadvanceprocess.component";
import { CashadvanceprocessService } from "./service/cashadvanceprocess.service";

import { CashadvancelistingComponent } from "./cashadvancelisting/cashadvancelisting.component";
import { CashadvancelistingService } from "./service/cashadvancelisting.service";

import { CashadvancetypeComponent } from "./cashadvancetype/cashadvancetype.component";
import { CashadvancetypeFormComponent } from "./cashadvancetype/cashadvancetypeform/cashadvancetypeform.component";
import { CashadvancetypeService } from "./service/cashadvancetype.service";

import { CashdeclarationComponent } from "./cashdeclaration/cashdeclaration.component";
import { CashdeclarationFormComponent } from "./cashdeclaration/cashdeclarationform/cashdeclarationform.component";
import { CashdeclartionService } from "./service/cashdeclaration.service";
import { NgSelectModule } from '@ng-select/ng-select';
import { SharedModule } from "../shared/shared.module";
import { ArraySortPipe } from "./sort.pipe";

import { DeclarationapprovalComponent } from "./declarationapproval/declarationapproval.component";
import { DeclarationapprovalService } from "./service/declarationapproval.service";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        MaterialModule,
        CashadvancesRoutingModule,
        HttpClientModule,
        TabsModule.forRoot(),
        Ng2SearchPipeModule,
        HttpClientModule,
        DataTableModule,
        FontAwesomeModule,
        NgSelectModule,
        SharedModule
    ],
    exports: [],
    declarations: [
        ArraySortPipe,
        CashadvanceappComponent,
        CashadvanceappFormComponent,
        CashadvanceapprovalComponent,
        CashadvanceprocessComponent,
        CashadvancetypeComponent,
        CashadvancetypeFormComponent,
        CashadvancelimitComponent,
        CashdeclarationComponent,
        CashdeclarationFormComponent,
        CashadvancelistingComponent,
        DeclarationapprovalComponent,
        StafftypeComponent,
        StafftypeFormComponent,
        CashadvancerateComponent,
        CashadvancelimitFormComponent,
        CashadvancerateFormComponent
    ],
    providers: [
        DeclarationapprovalService,
        CashadvanceappService,
        CashadvanceapprovalService,
        CashadvanceprocessService,
        CashadvancetypeService,
        CashdeclartionService,
        AssetItemService,
        CashadvancelistingService,
        StafftypeService,
        CashadvancerateService,
        CashadvancelimitService,
        StaffService,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: JwtInterceptor,
            multi: true
        }

    ],
})
export class CashadvancesModule { }
