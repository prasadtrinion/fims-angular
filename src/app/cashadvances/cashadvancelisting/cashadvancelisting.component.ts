
import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { CashadvancelistingService } from "../service/cashadvancelisting.service";
import { AlertService } from './../../_services/alert.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'Cashadvancelisting',
    templateUrl: 'cashadvancelisting.component.html'
})

export class CashadvancelistingComponent implements OnInit {

    cashadvancelistingList = [];
    cashadvancelistingData = {};
    selectAllCheckbox = true;
    constructor(
        private CashadvancelistingService: CashadvancelistingService,
        private AlertService: AlertService,
        private spinner: NgxSpinnerService,                        
    ) { }
    ngOnInit() {
        this.getListData();
    }
    getListData() {
        this.spinner.show();                                        
        this.CashadvancelistingService.getItemsDetail().subscribe(
            data => {
                this.spinner.hide();
                this.cashadvancelistingList = data['result'];
            }, error => {
                console.log(error);
            });
    }

    cashadvancelistingListData() {
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        console.log(this.cashadvancelistingList);
        var cashlistingIds = [];
        for (var i = 0; i < this.cashadvancelistingList.length; i++) {
            if (this.cashadvancelistingList[i].f052fapprovalStatus == true) {
                cashlistingIds.push(this.cashadvancelistingList[i].f052fid);
            }
        }
        var cashlistingUpdate = {};
        cashlistingUpdate['id'] = cashlistingIds;
        this.CashadvancelistingService.insertCashadvancelistingItems(cashlistingUpdate).subscribe(
            data => {
                this.AlertService.success(" Updated Sucessfully ! ");

                this.getListData();
            }, error => {
                console.log(error);
            });
    }
}
