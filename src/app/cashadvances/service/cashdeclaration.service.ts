import { Injectable } from '@angular/core';
import { HttpClientModule,HttpHeaders, HttpClient} from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({ 'X-Auth-Client': 'qazwsx123',
    'Content-Type': 'application/json' })
    
  };
@Injectable()

export class CashdeclartionService {

     url: string = environment.api.base + environment.api.endPoints.getCashApplicationProcessedList
     approvalPuturl: string = environment.api.base + environment.api.endPoints.getCashApplicationStaffList
     cashDeclarationurl: string = environment.api.base + environment.api.endPoints.cashDeclaration
    constructor(private httpClient: HttpClient) {    
    }
    getItems() {
        return this.httpClient.get(this.url,httpOptions);
    }
    getItemsDetail(id) {
        return this.httpClient.get(this.url+'/'+id, httpOptions);
    }
    getItemsDetailId(id) {
        return this.httpClient.get(this.cashDeclarationurl+'/'+id, httpOptions);
    }
    insertCashdeclartionItems(approvalData): Observable<any> {
        return this.httpClient.post(this.cashDeclarationurl, approvalData,httpOptions);
    }
    updateCashdeclartionItems(approvalData): Observable<any> {
        return this.httpClient.post(this.approvalPuturl, approvalData,httpOptions);
    }
   
}
