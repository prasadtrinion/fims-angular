import { Injectable } from '@angular/core';
import { HttpClientModule,HttpHeaders, HttpClient} from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({ 'X-Auth-Client': 'qazwsx123',
    'Content-Type': 'application/json' })
  };

@Injectable()
export class CashadvanceappService {
    url: string = environment.api.base + environment.api.endPoints.cashApplication;
    getCreditnoteNumberUrl : string = environment.api.base + environment.api.endPoints.generateNumber;
    deleteUrl : string = environment.api.base + environment.api.endPoints.deleteCashApplicationDetails;
    
    constructor(private httpClient: HttpClient) {    
    }
    getItems() {
        return this.httpClient.get(this.url,httpOptions);
    }
    deleteItems(deleteObject){
        return this.httpClient.post(this.deleteUrl,deleteObject,httpOptions);
    }
    getItemsDetail(id) {
        return this.httpClient.get(this.url+'/'+id,httpOptions);
    }
    insertCashadvanceappItems(cashadvanceappData): Observable<any> {
        return this.httpClient.post(this.url, cashadvanceappData,httpOptions);
    }
    generateReferenceNumber(cashadvanceappData): Observable<any> {
        return this.httpClient.post(this.getCreditnoteNumberUrl, cashadvanceappData,httpOptions);
    }
    updateCashadvanceappItems(cashadvanceappData,id): Observable<any> {
        return this.httpClient.put(this.url+'/'+id, cashadvanceappData,httpOptions);
       }
}