import { Injectable } from '@angular/core';
import { HttpClientModule,HttpHeaders, HttpClient} from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({ 'X-Auth-Client': 'qazwsx123',
    'Content-Type': 'application/json' })
    
  };
@Injectable()

export class CashadvanceprocessService {

     url: string = environment.api.base + environment.api.endPoints.getCashApplicationNonProcessedList
     approvalPuturl: string = environment.api.base + environment.api.endPoints.cashApplicationProcess
    constructor(private httpClient: HttpClient) {    
    }
    getItems() {
        return this.httpClient.get(this.url+'/'+2,httpOptions);
    }
    getItemsDetail() {
        return this.httpClient.get(this.url, httpOptions);
    }
    insertCashadvanceapprovalItems(approvalData): Observable<any> {
        return this.httpClient.post(this.approvalPuturl, approvalData,httpOptions);
    }
    updateCashadvanceapprovalItems(approvalData): Observable<any> {
        return this.httpClient.post(this.approvalPuturl, approvalData,httpOptions);
    }
   
}
