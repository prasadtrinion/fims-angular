import { Injectable } from '@angular/core';
import { HttpClientModule, HttpHeaders, HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';

const httpOptions = {
    headers: new HttpHeaders({
        'X-Auth-Client': 'qazwsx123',
        'Content-Type': 'application/json'
    })

};
@Injectable()
export class StafftypeService {
    url: string = environment.api.base + environment.api.endPoints.stafftype;
    ActiveListurl: string = environment.api.base + environment.api.endPoints.Activestafftype
    constructor(private httpClient: HttpClient) {
    }
    getDataItems() {
        return this.httpClient.get(this.url, httpOptions);
    }
    getItems() {
        return this.httpClient.get(this.url, httpOptions);
    }
    getActiveItems() {
        return this.httpClient.get(this.ActiveListurl, httpOptions);
    }
    getItemsDetail(id) {
        return this.httpClient.get(this.url + '/' + id, httpOptions);
    }
    insertStafftypeItems(stafftypeData): Observable<any> {
        return this.httpClient.post(this.url, stafftypeData, httpOptions);
    }
    updateStafftypeItems(stafftypeData, id): Observable<any> {
        return this.httpClient.put(this.url + '/' + id, stafftypeData, httpOptions);
    }
}