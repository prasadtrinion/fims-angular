import { Injectable } from '@angular/core';
import { HttpClientModule,HttpHeaders, HttpClient} from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({ 'X-Auth-Client': 'qazwsx123',
    'Content-Type': 'application/json' })
    
  };
@Injectable()

export class DeclarationapprovalService {
     url: string = environment.api.base + environment.api.endPoints.approvedCashDeclarations
     approveCashDeclarationUrl: string = environment.api.base + environment.api.endPoints.approveCashDeclaration

     constructor(private httpClient: HttpClient) {    
    }
    getItems() {
        return this.httpClient.get(this.url+'/'+2,httpOptions);
    }
    getItemsDetail(id) {
        return this.httpClient.get(this.url+'/'+id, httpOptions);
    }
    updateApplyloanapprovalItems(applyloanapprovalData): Observable<any> {
        return this.httpClient.post(this.approveCashDeclarationUrl, applyloanapprovalData,httpOptions);
    }   
}