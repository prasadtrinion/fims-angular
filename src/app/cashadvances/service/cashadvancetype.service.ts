import { Injectable } from '@angular/core';
import { HttpClientModule, HttpHeaders, HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({
        'X-Auth-Client': 'qazwsx123',
        'Content-Type': 'application/json'
    })
};

@Injectable()
export class CashadvancetypeService {
    url: string = environment.api.base + environment.api.endPoints.cashAdvanceType;
    ActiveListurl: string = environment.api.base + environment.api.endPoints.cashAdvanceactiveType;
    getCreditnoteNumberUrl: string = environment.api.base + environment.api.endPoints.generateNumber;

    constructor(private httpClient: HttpClient) {
    }
    getItems() {
        return this.httpClient.get(this.url, httpOptions);
    }
    getActiveItems() {
        return this.httpClient.get(this.ActiveListurl, httpOptions);
    }
    getItemsDetail(id) {
        return this.httpClient.get(this.url + '/' + id, httpOptions);
    }
    insertCashadvancetypeItems(cashadvanceappData): Observable<any> {
        return this.httpClient.post(this.url, cashadvanceappData, httpOptions);
    }
    generateReferenceNumber(cashadvanceappData): Observable<any> {
        return this.httpClient.post(this.getCreditnoteNumberUrl, cashadvanceappData, httpOptions);
    }
    updateCashadvancetypeItems(cashadvanceappData, id): Observable<any> {
        return this.httpClient.put(this.url + '/' + id, cashadvanceappData, httpOptions);
    }
}