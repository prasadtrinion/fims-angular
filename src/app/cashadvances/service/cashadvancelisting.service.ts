import { Injectable } from '@angular/core';
import { HttpClientModule,HttpHeaders, HttpClient} from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({ 'X-Auth-Client': 'qazwsx123',
    'Content-Type': 'application/json' })
    
  };
@Injectable()

export class CashadvancelistingService {

     url: string = environment.api.base + environment.api.endPoints.getStaffCashAdvances
     url1: string = environment.api.base + environment.api.endPoints.getCashApplicationApprovalList+'/1';
     approvalPuturl: string = environment.api.base + environment.api.endPoints.getCashApplicationStaffList
  
    constructor(private httpClient: HttpClient) {    
    }
    getItems() {
        return this.httpClient.get(this.url,httpOptions);
    }
    getItemsDetail() {
        return this.httpClient.get(this.url1, httpOptions);
    }
    insertCashadvancelistingItems(cashadvancelistingData): Observable<any> {
        return this.httpClient.post(this.approvalPuturl, cashadvancelistingData,httpOptions);
    }
    updateCashadvancelistingItems(cashadvancelistingData): Observable<any> {
        return this.httpClient.post(this.approvalPuturl, cashadvancelistingData,httpOptions);
    }
   
}
