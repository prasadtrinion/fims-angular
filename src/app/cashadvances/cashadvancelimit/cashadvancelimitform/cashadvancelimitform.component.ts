import { Injector, Injectable } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CashadvancelimitService } from "../../service/cashadvancelimit.service";
import { StaffService } from '../../../loan/service/staff.service';
import { CashadvancetypeService } from "../../service/cashadvancetype.service";
import { NgxSpinnerService } from 'ngx-spinner';

@Injectable()
@Component({
    selector: 'CashadvancelimitFormComponent',
    templateUrl: 'cashadvancelimitform.component.html'
})

export class CashadvancelimitFormComponent implements OnInit {
    cashlimitList = [];
    cashlimitData = {};
    stafftypeList = [];
    ajaxCount: number;
    staffList = [];
    loannameList = [];
    cashadvanceList = [];


    id: number;
    constructor(
        private CashadvancelimitService: CashadvancelimitService,
        private StaffService: StaffService,
        private CashadvancetypeService: CashadvancetypeService,
        private route: ActivatedRoute,
        private router: Router,
        private spinner: NgxSpinnerService,
    ) {
    }
    ngDoCheck() {

        if (this.ajaxCount == 0) {
            this.spinner.hide();
        }
    }
    ngOnInit() {
        this.ajaxCount = 0;
        this.spinner.show();
        this.cashlimitData['f119fstatus'] = 1;
        //  staff dropdown
        this.ajaxCount++;
        this.StaffService.getItems().subscribe(
            data => {
                console.log(data);
                this.ajaxCount--;
                this.staffList = data['result']['data'];
            }, error => {
                this.spinner.hide();
            }
        );
        // cash advance id dropdown
        this.ajaxCount++;
        this.CashadvancetypeService.getActiveItems().subscribe(
            data => {
                this.ajaxCount--;
                this.cashadvanceList = data['result']['data'];
                for (var i = 0; i < this.cashadvanceList.length; i++) {
                    this.cashadvanceList[i]['displayname'] = this.cashadvanceList[i]['f051fdescription']
                }
            }, error => {
                this.spinner.hide();
            });
        //  this.cashadvancerateData['f117famount'] = totalAm.toFixed(2);
        this.ajaxCount++;
        this.StaffService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.stafftypeList = data['result']['data'];
            }, error => {
                console.log(error);
                this.spinner.hide();
            });

        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.ajaxCount++;
            this.CashadvancelimitService.getItemsDetail(this.id).subscribe(
                data => {
                    this.ajaxCount--;
                    this.cashlimitData = data['result'][0];
                    // converting from string to int for radio button default selection
                    this.cashlimitData['f119fstatus'] = parseInt(data['result'][0]['f119fstatus']);

                    console.log(this.cashlimitData);
                }, error => {
                    console.log(error);
                });
        }
        console.log(this.id);
    }

    addCashadvancelimit() {
        console.log(this.cashlimitData);
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }

        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.CashadvancelimitService.updateCashadvancelimitItems(this.cashlimitData, this.id).subscribe(
                data => {
                    if (data['status'] == 409) {
                    }
                    else {
                        this.router.navigate(['cashadvances/cashadvancelimit']);
                        alert("Cash Entitlement has been Updated Sucessfully !!");
                    }
                }, error => {
                });
        } else {
            this.CashadvancelimitService.insertCashadvancelimitItems(this.cashlimitData).subscribe(
                data => {
                    if (data['status'] == 409) {
                    }
                    else {
                        this.router.navigate(['cashadvances/cashadvancelimit']);
                        alert("Cash Entitlement has been Added Sucessfully ! ");
                    }
                }, error => {
                    console.log(error);
                });
        }
    }
    onlyNumberKey(event) {
        if (event.charCode == 46) {

        } else {
            return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
        }
    }
}