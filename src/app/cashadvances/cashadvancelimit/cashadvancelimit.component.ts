
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CashadvancelimitService } from '../service/cashadvancelimit.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'Cashadvancelimit',
    templateUrl: 'cashadvancelimit.component.html'
})
export class CashadvancelimitComponent implements OnInit {
    cashlimitList = [];
    cashlimitData = {};
    id: number;
    constructor(
        private CashadvancelimitService: CashadvancelimitService,
        private spinner: NgxSpinnerService,

    ) { }
    ngOnInit() {
        this.spinner.show();
        this.CashadvancelimitService.getItems().subscribe(
            data => {
                this.spinner.hide();
                this.cashlimitList = data['result']['data'];

            }, error => {
                console.log(error);
            });
    }
}

