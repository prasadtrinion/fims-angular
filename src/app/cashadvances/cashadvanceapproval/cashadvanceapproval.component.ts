
import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { CashadvanceapprovalService } from "../service/cashadvanceapproval.service";
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'CashadvanceapprovalComponent',
    templateUrl: 'cashadvanceapproval.component.html'
})

export class CashadvanceapprovalComponent implements OnInit {
    cashadvanceapprovalList = [];
    cashadvanceapprovalData = {};
    selectAllCheckbox = true;
    reason: string;

    constructor(
        private CashadvanceapprovalService: CashadvanceapprovalService,
        private spinner: NgxSpinnerService,
    ) { }

    ngOnInit() {
        this.getListData();
    }
    getListData() {
        this.spinner.show();
        this.CashadvanceapprovalService.getItemsDetail(0).subscribe(
            data => {
                this.spinner.hide();
                this.cashadvanceapprovalList = data['result'];
            }, error => {
                console.log(error);
            });
    }
    updateCashadvanceRejectedItems() {
        var approvaloneIds = [];
        for (var i = 0; i < this.cashadvanceapprovalList.length; i++) {
            if (this.cashadvanceapprovalList[i].f052fapprovalStatus == true) {
                approvaloneIds.push(this.cashadvanceapprovalList[i].f052fid);
            }
        }
        if (approvaloneIds.length > 0) {

        } else {
            alert("Select atleast one cash advance application to reject");
            return false;
        }

        var confirmPop = confirm("Do you want to Reject?");
        if (confirmPop == false) {
            return false;
        }
        console.log(this.cashadvanceapprovalList);

        var approvaloneUpdate = {};
        approvaloneUpdate['id'] = approvaloneIds;
        approvaloneUpdate['reason'] = this.reason;
        this.CashadvanceapprovalService.updateCashadvanceRejectedItems(approvaloneUpdate).subscribe(
            data => {
                alert("Cash Advance has been Rejected");

                this.getListData();
            }, error => {
                console.log(error);
            });
    }
    cashadvanceapprovalListData() {
        console.log(this.cashadvanceapprovalList);
        var approvaloneIds = [];
        for (var i = 0; i < this.cashadvanceapprovalList.length; i++) {
            if (this.cashadvanceapprovalList[i].f052fapprovalStatus == true) {
                approvaloneIds.push(this.cashadvanceapprovalList[i].f052fid);
            }
        }
        if (approvaloneIds.length > 0) {

        } else {
            alert(" select atleast one Cash Advance to Approve");
            return false;
        }
        var confirmPop = confirm("Do you want to Approve?");
        if (confirmPop == false) {
            return false;
        }

        var approvaloneUpdate = {};
        approvaloneUpdate['id'] = approvaloneIds;
        approvaloneUpdate['status'] = '1';

        approvaloneUpdate['reason'] = this.cashadvanceapprovalData['reason'];
        this.CashadvanceapprovalService.updateCashadvanceapprovalItems(approvaloneUpdate).subscribe(
            data => {
                this.cashadvanceapprovalData['reason'] = '';

                alert("Apply cash advance has been approved successfully");

                this.getListData();
            }, error => {
                console.log(error);
            });
    }
    rejecttwoListData() {
        console.log(this.cashadvanceapprovalList);
        var rejectoneIds = [];

        for (var i = 0; i < this.cashadvanceapprovalList.length; i++) {
            if (this.cashadvanceapprovalList[i].f052fapprovalStatus == true) {
                rejectoneIds.push(this.cashadvanceapprovalList[i].f052fid);
            }
        }
        if (rejectoneIds.length > 0) {

        } else {
            alert(" Select atleast one Cash Advance to Reject");
            return false;
        }
        if (this.cashadvanceapprovalData['reason'] == undefined) {
            alert(' Enter the Description');
            return false;
        }
        console.log(this.cashadvanceapprovalData['reason']);

        if (this.cashadvanceapprovalData['reason'] == '' || this.cashadvanceapprovalData['reason'] == undefined) {
            alert("Provide the reason for Rejection !");
        } else {
            var confirmPop = confirm("Do you want to Reject?");
            if (confirmPop == false) {
                return false;
            }
            var rejecttwoUpdate = {};
            rejecttwoUpdate['id'] = rejectoneIds;
            rejecttwoUpdate['reason'] = this.cashadvanceapprovalData['reason'];

            this.CashadvanceapprovalService.updateCashadvanceRejectedItems(rejecttwoUpdate).subscribe(
                data => {
                    alert("cash Advance has been Rejected ! ");
                    this.cashadvanceapprovalData['reason'] = '';

                    this.getListData();
                }, error => {
                    console.log(error);
                });
        }

    }
}
