import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CashadvancerateService } from "../service/cashadvancerate.service";
import { faSearch, faEdit } from '@fortawesome/free-solid-svg-icons';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'CashadvancerateComponent',
    templateUrl: 'cashadvancerate.component.html'
})
export class CashadvancerateComponent implements OnInit {
    cashadvancerateList = [];
    cashadvancerateData = {};
    id: number;

    constructor(
        private CashadvancerateService: CashadvancerateService,
        private spinner: NgxSpinnerService

    ) {
    }
    ngOnInit() {
        this.spinner.show();
        this.CashadvancerateService.getDataItems().subscribe(
            data => {
                this.spinner.hide();
                this.cashadvancerateList = data['result']['data'];
            }, error => {
                console.log(error);
            });
    }
}