import { Injector, Injectable } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { StafftypeService } from "../../service/stafftype.service";
import { CashadvancerateService } from "../../service/cashadvancerate.service";
import { NgxSpinnerService } from 'ngx-spinner';

@Injectable()
@Component({
    selector: 'CashadvancerateFormComponent',
    templateUrl: 'cashadvancerateform.component.html'
})

export class CashadvancerateFormComponent implements OnInit {
    cashadvancerateList = [];
    cashadvancerateData = {};
    stafftypeList = [];
    id: number;
    ajaxCount: number;
    constructor(
        private CashadvancerateService: CashadvancerateService,
        private StafftypeService: StafftypeService,
        private route: ActivatedRoute,
        private router: Router,
        private spinner: NgxSpinnerService,
    ) { }

    ngDoCheck() {

        if (this.ajaxCount == 0) {
            this.spinner.hide();
        }
    }
    ngOnInit() {
        this.ajaxCount = 0;
        this.spinner.show();
        this.cashadvancerateData['f117fstatus'] = 1;

        //  this.cashadvancerateData['f117famount'] = totalAm.toFixed(2);
        this.ajaxCount++;
        this.StafftypeService.getActiveItems().subscribe(
            data => {
                this.ajaxCount--;
                this.stafftypeList = data['result']['data'];
            }, error => {
                console.log(error);
            });
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        console.log(this.id);
        if (this.id > 0) {
            this.ajaxCount++;
            this.CashadvancerateService.getItemsDetail(this.id).subscribe(
                data => {
                    this.ajaxCount--;
                    this.cashadvancerateData = data['result'][0];

                    // converting from string to int for radio button default selection
                    this.cashadvancerateData['f117fstatus'] = parseInt(data['result'][0]['f117fstatus']);

                    console.log(this.cashadvancerateData);
                }, error => {
                    console.log(error);
                });
        }
    }
    addCashadvancerate() {
        console.log(this.cashadvancerateData);
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.CashadvancerateService.updateCashadvancerateItems(this.cashadvancerateData, this.id).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert('Cash Advance Rate Already Exist');
                    }
                    else {
                        this.router.navigate(['cashadvances/cashadvancerate']);
                        alert("Cash Advance Rate has been Updated Sucessfully !!");
                    }
                }, error => {
                });
        } else {
            this.CashadvancerateService.insertCashadvancerateItems(this.cashadvancerateData).subscribe(
                data => {
                    if (data['status'] == 409) {
                    }
                    else {
                        this.router.navigate(['cashadvances/cashadvancerate']);
                        alert("Cash Advance Rate has been Added Sucessfully ! ");
                    }
                }, error => {
                    console.log(error);
                });
        }
    }
    onlyNumberKey(event) {
        if (event.charCode == 46) {

        } else {
            return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
        }
    }
}