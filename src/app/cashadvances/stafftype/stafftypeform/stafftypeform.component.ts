import { Injector, Injectable } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { StafftypeService } from "../../service/stafftype.service";
// import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { NgxSpinnerService } from 'ngx-spinner';

@Injectable()
@Component({
    selector: 'StafftypeFormComponent',
    templateUrl: 'stafftypeform.component.html'
})

export class StafftypeFormComponent implements OnInit {
    stafftypeList = [];
    stafftypeData = {};
    id: number;
    ajaxCount: number;
    constructor(
        private StafftypeService: StafftypeService,
        private route: ActivatedRoute,
        private router: Router,
        private spinner: NgxSpinnerService,
    ) { }
    ngDoCheck() {

        if (this.ajaxCount == 0) {
            this.spinner.hide();
        }
    }
    ngOnInit() {
        this.ajaxCount = 0;
        this.spinner.show();
        this.stafftypeData['f116fstatus'] = 1;
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        console.log(this.id);
        if (this.id > 0) {
            this.ajaxCount++;
            this.StafftypeService.getItemsDetail(this.id).subscribe(
                data => {
                    this.ajaxCount--;
                    this.stafftypeData = data['result'][0];
                    // converting from string to int for radio button default selection
                    this.stafftypeData['f116fstatus'] = parseInt(data['result'][0]['f116fstatus']);

                    console.log(this.stafftypeData);
                }, error => {
                    console.log(error);
                });
        }
        console.log(this.id);
    }
    addStaffType() {
        console.log(this.stafftypeData);
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.StafftypeService.updateStafftypeItems(this.stafftypeData, this.id).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert('Staff Type Already Exist');
                    }
                    else {
                        this.router.navigate(['cashadvances/stafftype']);
                        alert("Staff Type has been Updated Sucessfully !!");
                    }
                }, error => {
                    alert('Staff Type Already Exist');
                    return false;
                });
        } else {
            this.StafftypeService.insertStafftypeItems(this.stafftypeData).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert("Staff Type  Already Exist");
                    }
                    else {
                        this.router.navigate(['cashadvances/stafftype']);
                        alert("Staff Type has been Added Sucessfully ! ");
                    }
                }, error => {
                    console.log(error);
                });
        }
    }
}