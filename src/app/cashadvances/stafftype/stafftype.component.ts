import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { StafftypeService } from "../service/stafftype.service";
import { faSearch, faEdit } from '@fortawesome/free-solid-svg-icons';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'Stafftype',
    templateUrl: 'stafftype.component.html'
})
export class StafftypeComponent implements OnInit {
    stafftypeList = [];
    stafftypeData = {};
    id: number;
    constructor(
        private StafftypeService: StafftypeService,
        private spinner: NgxSpinnerService
    ) {
    }
    ngOnInit() {
        this.spinner.show();
        this.StafftypeService.getDataItems().subscribe(
            data => {
                this.spinner.hide();
                this.stafftypeList = data['result']['data'];
            }, error => {
                console.log(error);
            });
    }
}