import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { PageNotFoundComponent } from './errors/page-not-found/page-not-found.component';
import { VoucherlayoutComponent } from './layout/voucherlayout/voucherlayout.component';
import { GllayoutComponent } from './layout/gllayout/gllayout.component';
import { BudgetlayoutComponent } from './layout/budgetlayout/budgetlayout.component';
import { MaintenanceLayoutComponent } from './layout/maintenancelayout/maintenancelayout.component';
import { AccountsreceivablelayoutComponent } from './layout/accountsreceivablelayout/accountsreceivable.component';
import { AccountspayablelayoutComponent } from './layout/accountspayablelayout/accountspayablelayout.component';
import { StudentfinancelayoutComponent } from './layout/studentfinancelayout/studentfinancelayout.component';
import { ProcurementlayoutComponent } from './layout/procurementlayout/procurementlayout.component';

import { StudentlayoutComponent } from './layout/studentlayout/studentlayout.component';
import { InvestmentlayoutComponent } from './layout/investmentlayout/investmentlayout.component';
import { AuthGuard } from "./_guards/auth.guard";
import { AuthModule } from './auth/auth.module';
import { DefaultlayoutComponent } from "./layout/defaultlayout/defaultlayout.component";
import { ReportlayoutComponent } from "./layout/report/reportlayout.component";
import { ReturnsComponent } from "./studentfinance/returns/returns.component";
import { StudentloginComponent } from "./auth/studentlogin/studentlogin.component";
import { SupplierRegistrationComponent } from './auth/supplierregistration/supplierregistration.component';

import { PayrolllayoutComponent } from './layout/payrolllayout/payrolllayout.component';
import { CashadvanceslayoutComponent } from './layout/cashadvanceslayout/cashadvanceslayout.component';
import { VendorlayoutComponent } from './layout/vendorlayout/vendorlayout.component';

import { AssetslayoutComponent } from './layout/assetslayout/assetslayout.component';
import { LoanlayoutComponent } from './layout/loanlayout/loanlayout.component';
import { SupplierLoginComponent } from "./auth/supplierlogin/supplierlogin.component";


const routes: Routes = [

  { path: 'auth',component:DefaultlayoutComponent, loadChildren: 'app/auth/auth.module#AuthModule' },

  { path: 'generalledger', component: GllayoutComponent, loadChildren: 'app/generalledger/generalledger.module#GeneralledgerModule' ,canActivate: [AuthGuard]},
  { path: 'generalsetup', component: MaintenanceLayoutComponent,loadChildren: 'app/generalsetup/generalsetup.module#GeneralsetupModule' ,canActivate: [AuthGuard]},
  { path: 'date', loadChildren: 'app/date/date.module#DateModule' ,canActivate: [AuthGuard]},
  { path: 'cashadvances', component: CashadvanceslayoutComponent, loadChildren: 'app/cashadvances/cashadvances.module#CashadvancesModule' ,canActivate: [AuthGuard]},

  { path: 'accountsubsidary', loadChildren: 'app/accountsubsidary/accountsubsidary.module#AccountsubsidaryModule' ,canActivate: [AuthGuard]},
  { path: 'budget', component: BudgetlayoutComponent, loadChildren: 'app/budget/budget.module#BudgetModule' ,canActivate: [AuthGuard]},

  { path: 'investment', component: InvestmentlayoutComponent, loadChildren: 'app/investment/investment.module#InvestmentModule',canActivate: [AuthGuard] },
  { path: 'voucher', component: VoucherlayoutComponent, loadChildren: 'app/voucher/voucher.module#VoucherModule',canActivate: [AuthGuard] },
  { path: 'report', component: GllayoutComponent, loadChildren: 'app/report/report.module#ReportModule',canActivate: [AuthGuard] },

  { path: 'accountspayable', component: AccountspayablelayoutComponent, loadChildren: 'app/accountspayable/accountspayable.module#AccountspayableModule',canActivate: [AuthGuard] },
  { path: 'accountsrecivable', component: AccountsreceivablelayoutComponent, loadChildren: 'app/accountsrecivable/accountsrecivable.module#AccountsrecivableModule',canActivate: [AuthGuard] },

  { path: 'studentfinance', component: StudentfinancelayoutComponent, loadChildren: 'app/studentfinance/studentfinance.module#StudentfinanceModule',canActivate: [AuthGuard] },

  { path: '', component:DefaultlayoutComponent, loadChildren: 'app/auth/auth.module#AuthModule' },
   { path: 'procurement', component: ProcurementlayoutComponent, loadChildren: 'app/procurement/procurement.module#ProcurementModule',canActivate: [AuthGuard] },
   { path: 'assets', component: AssetslayoutComponent, loadChildren: 'app/assets/assets.module#AssetsModule',canActivate: [AuthGuard] },
   { path: 'loan', component: LoanlayoutComponent, loadChildren: 'app/loan/loan.module#LoanModule',canActivate: [AuthGuard] },

  { path: 'payroll', component: PayrolllayoutComponent, loadChildren: 'app/payroll/payroll.module#PayrollModule',canActivate: [AuthGuard] },
  { path: 'vendor', component: VendorlayoutComponent, loadChildren: 'app/vendor/vendor.module#VendorModule',canActivate: [AuthGuard] },



  // {
  //   path: 'student',component: GllayoutComponent, children: [
  //     { path: 'returns', component: ReturnsComponent }
  //   ]
  // }, 
  {
    path: 'dcalogin',component: DefaultlayoutComponent, children: [
      { path: '', component: StudentloginComponent },
    ]
  },
  {
    path: 'vendorregister',component: DefaultlayoutComponent, children: [
      { path: '', component: SupplierRegistrationComponent },
    ]
  },
  {
    path: 'vendorlogin',component: DefaultlayoutComponent, children: [
      { path: '', component: SupplierLoginComponent },
    ]
  },
  { path: '', component:DefaultlayoutComponent, loadChildren: 'app/auth/auth.module#AuthModule' },
  { path: '**', component: PageNotFoundComponent }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule { }
