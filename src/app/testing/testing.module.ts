import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MaterialModule } from '../material/material.module';

import { TestingRoutingModule } from './testing.router.module';

import { TestingComponent } from './testing/testing.component';

@NgModule({
    imports: [
        CommonModule,
        MaterialModule,
        TestingRoutingModule
    ],
    exports: [],
    declarations: [
        TestingComponent
    ],
    providers: [],
})
export class TestingModule { }
