import { BrowserModule } from '@angular/platform-browser';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
//import { RecaptchaModule } from 'ng-recaptcha';


import { RouterModule } from '@angular/router';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { MaterialModule } from './material/material.module';
import { AppRoutingModule } from './app.router.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './layout/header/header.component';
import { VendorHeaderComponent } from './layout/vendorheader/vendorheader.component';
import { FooterComponent } from './layout/footer/footer.component';
import { VoucherlayoutComponent } from './layout/voucherlayout/voucherlayout.component';
import { AccountsreceivablelayoutComponent } from './layout/accountsreceivablelayout/accountsreceivable.component';
import { AccountspayablelayoutComponent } from './layout/accountspayablelayout/accountspayablelayout.component';
import { AuthModule } from './auth/auth.module'
import { NguiAutoCompleteModule } from '@ngui/auto-complete';
import { MaintenanceLayoutComponent } from './layout/maintenancelayout/maintenancelayout.component';

import { BudgetlayoutComponent } from './layout/budgetlayout/budgetlayout.component';
import { StudentlayoutComponent } from './layout/studentlayout/studentlayout.component';
import { StudentfinancelayoutComponent } from './layout/studentfinancelayout/studentfinancelayout.component';
import { InvestmentlayoutComponent } from './layout/investmentlayout/investmentlayout.component';
import { ReportlayoutComponent } from "./layout/report/reportlayout.component";
import { PayrolllayoutComponent } from './layout/payrolllayout/payrolllayout.component';
import { CashadvanceslayoutComponent } from './layout/cashadvanceslayout/cashadvanceslayout.component';

import { GllayoutComponent } from './layout/gllayout/gllayout.component';
import { PageNotFoundComponent } from './errors/page-not-found/page-not-found.component';
import { AuthTokenInterceptorService } from './auth/auth.token.interceptor.service';
import { BudgetModule } from './budget/budget.module';
import { DateModule } from './date/date.module';
import { AccountsModule } from './accounts/accounts.module';
import { GeneralledgerModule } from './generalledger/generalledger.module';
import { CashadvancesModule } from './cashadvances/cashadvances.module';
import { AccountsubsidaryModule } from './accountsubsidary/accountsubsidary.module';
import { VoucherModule } from './voucher/voucher.module';
import { ReportModule } from './report/report.module';
import { InvestmentModule } from './investment/investment.module';
import { ProcurementModule } from "./procurement/procurement.module";
import { VendorModule } from './vendor/vendor.module';
import { LoanModule } from './loan/loan.module';
import { JwtInterceptor } from './../app/_helpers/jwt.interceptors';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AccordionModule } from 'primeng/accordion';
import { PanelMenuModule } from 'primeng/panelmenu';
import { StudentfinanceModule } from './studentfinance/studentfinance.module';
import { AccountsrecivableModule } from "./accountsrecivable/accountsrecivable.module";
import { MenuModule } from 'primeng/menu';
import { DefaultlayoutComponent } from "./layout/defaultlayout/defaultlayout.component";
import { ProcurementlayoutComponent } from './layout/procurementlayout/procurementlayout.component';

import { AssetslayoutComponent } from './layout/assetslayout/assetslayout.component';
import { LoanlayoutComponent } from './layout/loanlayout/loanlayout.component';
import { VendorlayoutComponent } from './layout/vendorlayout/vendorlayout.component';
import { CreditnoteComponent } from './accountsrecivable/creditnote/creditnote.component';
import { CreditnoteformComponent } from './accountsrecivable/creditnote/creditnoteform/creditnoteform.component';

import { PayrollModule } from './payroll/payroll.module';

import { AlertComponent } from './_directives/index';
import { AlertService } from './_services/index';
import { AuthGuard } from "./_guards/auth.guard";
import { LayoutService } from "./layout/layout.service";
import { DataTableModule } from "angular-6-datatable";
import { Ng4LoadingSpinnerModule } from 'ng4-loading-spinner';
import { StudentloginComponent } from "./auth/studentlogin/studentlogin.component";
import { SupplierLoginComponent } from "./auth/supplierlogin/supplierlogin.component";
import { SupplierRegistrationComponent } from './auth/supplierregistration/supplierregistration.component';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { NgSelectModule } from '@ng-select/ng-select';
import { SharedModule } from "./shared/shared.module";
import { NgxSpinnerModule } from 'ngx-spinner';
import { NgxQRCodeModule } from 'ngx-qrcode2';
import { GeneralsetupModule } from './generalsetup/generalsetup.module';

//Shared Components
@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    VendorHeaderComponent,
    FooterComponent,
    PageNotFoundComponent,
    VoucherlayoutComponent,
    GllayoutComponent,
    BudgetlayoutComponent,
    AccountsreceivablelayoutComponent,
    AccountspayablelayoutComponent,
    StudentfinancelayoutComponent,
    StudentlayoutComponent,
    MaintenanceLayoutComponent,
    AlertComponent,
    DefaultlayoutComponent,
    InvestmentlayoutComponent,
    ReportlayoutComponent,
    ProcurementlayoutComponent,
    PayrolllayoutComponent,
    StudentloginComponent,
    SupplierRegistrationComponent,
    AssetslayoutComponent,
    LoanlayoutComponent,
    CashadvanceslayoutComponent,
    SupplierLoginComponent,
    VendorlayoutComponent,

  ],

  imports: [
    NgbModule,
    NgSelectModule,
    NgxSpinnerModule,
    SharedModule,
    BrowserModule,
    FontAwesomeModule,
    RouterModule,
    HttpClientModule,

    GeneralsetupModule,
    BudgetModule,
    AccountsrecivableModule,
    GeneralledgerModule,
    AccountsubsidaryModule,
    InvestmentModule,
    PayrollModule,
    ProcurementModule,
    CashadvancesModule,
    StudentfinanceModule,
    ReportModule,
    LoanModule,
    AuthModule,
    FormsModule,
    DataTablesModule,
    NgxQRCodeModule,
    AppRoutingModule // Should be last import
  ],
  schemas: [NO_ERRORS_SCHEMA],

  providers: [
    AlertService,
    AuthGuard,
    LayoutService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: JwtInterceptor,
      multi: true
    }
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule { }
