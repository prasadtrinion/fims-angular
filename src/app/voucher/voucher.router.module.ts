import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CreditComponent } from './credit/credit.component';
import { CreditFormComponent } from './credit/creditform/creditform.component';

import { DebitComponent } from './debit/debit.component';
import { DebitFormComponent } from './debit/debitform/debitform.component';

import { PaymentComponent } from './payment/payment.component';
import { PaymentFormComponent } from './payment/paymentform/paymentform.component';

import { PurchaseComponent } from './purchase/purchase.component';
import { PurchaseFormComponent } from './purchase/purchaseform/purchaseform.component';
 
import { VoucherlistComponent } from './voucherlist/voucherlist.component';
import { VoucherlistFormComponent } from './voucherlist/voucherlistform/voucherlistform.component';

const routes: Routes = [
  
  { path: 'credit/:voucherType', component: CreditComponent },
  { path: 'addnewcredit', component: CreditFormComponent },
  { path: 'editcredit/:id', component: CreditFormComponent },

  { path: 'debit', component:DebitComponent },
  { path: 'addnewdebit', component: DebitFormComponent },
  { path: 'editdebit/:id', component: DebitFormComponent },

  { path: 'payment', component: PaymentComponent },
  { path: 'addnewpayment', component: PaymentFormComponent },
  { path: 'editpayment/:id', component: PaymentFormComponent },

  { path: 'purchase', component:PurchaseComponent },
  { path: 'addnewpurchase', component: PurchaseFormComponent },
  { path: 'editdebit/:id', component: PurchaseFormComponent },

  { path: 'voucherlist', component:VoucherlistComponent },
  { path: 'addnewvoucherlist', component: VoucherlistFormComponent },
  { path: 'editvoucherlist/:id', component: VoucherlistFormComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  
})
export class VoucherRoutingModule { 
  
}