import { Injectable } from '@angular/core';
import { HttpClientModule,HttpHeaders, HttpClient} from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';

const ParseHeaders = {
    headers: new HttpHeaders({
     'Content-Type'  : 'application/json'
    })
   };

@Injectable()

export class PurchaseService {
    url: string = environment.api.base + environment.api.endPoints.purchase;
    constructor(private httpClient: HttpClient) { 
        
    }
    
    getItems() {
        return this.httpClient.get(this.url);
    }

    getItemsDetail(id) {
        return this.httpClient.get(this.url+'/'+id);
    }


    insertPurchaseItems(purchaseData): Observable<any> {
        return this.httpClient.post(this.url, purchaseData);
    }
  
    updatePurchaseItems(purchaseData) {

        return this.httpClient.post('http://localhost/testing/update.php', purchaseData);
       }
}