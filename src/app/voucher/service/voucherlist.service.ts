import { Injectable } from '@angular/core';
import { HttpClientModule,HttpHeaders, HttpClient} from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({ 'X-Auth-Client': 'qazwsx123',
    'Content-Type': 'application/json' })
    
  };
  
@Injectable()

export class VoucherlistService {
    url: string = environment.api.base + environment.api.endPoints.voucher;
    ledgeturl: string = environment.api.base + environment.api.endPoints.ledger;

    constructor(private httpClient: HttpClient) { 
        
    }
    
    getItems() {
        return this.httpClient.get(this.url,httpOptions);
    }

    getLedgerItems() {
        return this.httpClient.get(this.ledgeturl,httpOptions);
    }

    getItemsDetail(id) {
        return this.httpClient.get(this.url+'/'+id,httpOptions);
    }

    insertVoucherlistItems(voucherlistData): Observable<any> {
        return this.httpClient.post(this.url, voucherlistData,httpOptions);
    }

    updateVoucherlistItems(voucherlistData,id): Observable<any> {
        return this.httpClient.put(this.url+'/'+id, voucherlistData,httpOptions);
       }
}