import { Injectable } from '@angular/core';
import { HttpClientModule,HttpHeaders, HttpClient} from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';

const ParseHeaders = {
    headers: new HttpHeaders({
     'Content-Type'  : 'application/json'
    })
   };

@Injectable()

export class DebitService {
    url: string = environment.api.base + environment.api.endPoints.debit;
    constructor(private httpClient: HttpClient) { 
        
    }
    
    getItems() {
        return this.httpClient.get(this.url);
    }

    getItemsDetail(id) {
        return this.httpClient.get(this.url+'/'+id);
    }

  
    insertDebitItems(debitData): Observable<any> {
        return this.httpClient.post(this.url, debitData);
    }

    updateDebitItems(debitData) {

        return this.httpClient.post('http://localhost/testing/update.php', debitData);
       }
}