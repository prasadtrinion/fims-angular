import { Injectable } from '@angular/core';
import { HttpClientModule,HttpHeaders, HttpClient} from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({ 'X-Auth-Client': 'qazwsx123',
    'Content-Type': 'application/json' })
    
  };
@Injectable()

export class CreditService {
    url: string = environment.api.base + environment.api.endPoints.voucher;
    constructor(private httpClient: HttpClient) { 
        
    }
    getledgerName () {
        return this.httpClient.get('https://api.myjson.com/bins/t2aj2');
    }
    getItems() {
        return this.httpClient.get(this.url);
    }

    getItemsDetail(id) {
        return this.httpClient.get(this.url+'/'+id);
    }

    insertCreditItems(creditData): Observable<any> {
        return this.httpClient.post(this.url, creditData,httpOptions);
    }

    updateCreditItems(creditData) {

        return this.httpClient.post('http://localhost/testing/update.php', creditData);
       }
}