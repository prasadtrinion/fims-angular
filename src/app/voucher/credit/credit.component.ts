import {Injector} from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { CreditService } from '../service/credit.service'
import { ActivatedRoute,Router } from '@angular/router';
import { VoucherlistService } from '../service/voucherlist.service'

@Component({
    selector: 'Credit',
    templateUrl: 'credit.component.html'
})

export class CreditComponent implements OnInit {
    creditList =  [];
    ledgerList = [];
    creditData = {};
    indGlCode = {};
    debit = 0;
    credit = 0;
    totalDebitAmount = 0;
    totalCreditAmount = 0;
    selectedOption = 0;
    debtorlist = {};
    voucherType = '';
    voucherTypeId = "";
    showBankDetails = false;
    options = [
        { name: "DR", value: 'DR' },
        { name: "CR", value: 'CR' }
      ]
    //   ledgerList = [
    //       {
    //         "name": "crecdit",
    //         "openingbalance": "1000",
    //         "id": "1"
    //       },
    //       {
    //         "name": "debit",
    //         "openingbalance": "2000",
    //         "id": "2"
    //       }
    //     ];
    constructor(
        private VoucherlistService: VoucherlistService,
        private CreditService: CreditService,
        private route: ActivatedRoute,
        private router:Router
    ) { }

    ngOnInit() { 
        this.voucherType = this.route.snapshot.params['voucherType'];
        switch(this.voucherType) {
            case 'contra' : this.showBankDetails = true; this.voucherTypeId = "Contra"; break;
            case 'CreditNote' : this.showBankDetails = true; this.voucherTypeId = "Credit Note"; break;
            case 'debitnote' : this.showBankDetails = false; this.voucherTypeId = "Debit Note"; break;
            case 'journal' : this.showBankDetails = true; this.voucherTypeId = "Journal";  break;
            case 'optional' : this.showBankDetails = false; this.voucherTypeId = "Optional"; break;
            case 'payment' : this.showBankDetails = true; this.voucherTypeId = "Payment";  break;
            case 'purchase' : this.showBankDetails = false;this.voucherTypeId = "Purchase";  break;
            case 'purchasereturn' : this.showBankDetails = true; this.voucherTypeId = "Purchase Return";  break;
            case 'sales' : this.showBankDetails = false; this.voucherTypeId = "Sales";  break;
            case 'salesreturn' : this.showBankDetails = true;this.voucherTypeId = "Sales Return";  break;
        }
        this.VoucherlistService.getLedgerItems().subscribe(
            data => {
                this.ledgerList = data['result']['data'];
                console.log(this.ledgerList);
        }, error => {
            console.log(error);
        });

       

    }

    updateValue(object) {
        for(var j=0;j<this.ledgerList.length;j++) {
             if(this.ledgerList[j]['f041fname']==object['debtorList'] && object['f032ftype']=='DR') {
                 return (this.ConvertToInt(this.ledgerList[j]['f041fopeningBalance']) - this.ConvertToInt(object['f032fdebit']));
             }
             if(this.ledgerList[j]['f041fname']==object['debtorList'] && object['f032ftype']=='CR') {
                return (this.ConvertToInt(this.ledgerList[j]['f041fopeningBalance']) + this.ConvertToInt(object['f032fcredit']));
            }
        }
    }

    updateLedgetListAmount(object) {
        for(var j=0;j<this.ledgerList.length;j++) {
             if(this.ledgerList[j]['f041fname']==object['debtorList'] && object['f032ftype']=='DR') {
                 var amountPending  =  (this.ConvertToInt(this.ledgerList[j]['f041fopeningBalance']) - this.ConvertToInt(object['f032fdebit']));
                 this.ledgerList[j]['f041fopeningBalance'] = amountPending.toString();
             }
             if(this.ledgerList[j]['f041fname']==object['debtorList'] && object['f032ftype']=='CR') {
                var amountPending  =  (this.ConvertToInt(this.ledgerList[j]['f041fopeningBalance']) + this.ConvertToInt(object['f032fcredit']));
                this.ledgerList[j]['f041fopeningBalance'] = amountPending.toString();
            }
        }
    }

    validateSave() {
        this.voucherType = this.route.snapshot.params['voucherType'];
        this.voucherTypeId = "Credit Note";
        switch(this.voucherType) {
            case 'contra' : this.showBankDetails = true; this.voucherTypeId = "Contra"; break;
            case 'CreditNote' : this.showBankDetails = true; this.voucherTypeId = "Credit Note"; break;
            case 'debitnote' : this.showBankDetails = false; this.voucherTypeId = "Debit Note"; break;
            case 'journal' : this.showBankDetails = true; this.voucherTypeId = "Journal";  break;
            case 'optional' : this.showBankDetails = false; this.voucherTypeId = "Optional"; break;
            case 'payment' : this.showBankDetails = true; this.voucherTypeId = "Payment";  break;
            case 'purchase' : this.showBankDetails = false;this.voucherTypeId = "Purchase";  break;
            case 'purchasereturn' : this.showBankDetails = true; this.voucherTypeId = "Purchase Return";  break;
            case 'sales' : this.showBankDetails = false; this.voucherTypeId = "Sales";  break;
            case 'salesreturn' : this.showBankDetails = true;this.voucherTypeId = "Sales Return";  break;
        }

        this.creditData['f031fvoucher_type'] = this.voucherTypeId;
        this.creditData['f031fvoucher_amount'] = this.totalDebitAmount;
        this.creditData['f031fvoucher_days'] = 2;
        this.creditData['f031fcreated_by']= 1,
        this.creditData['f031fupdated_by'] = 1 ;
        this.creditData['f031fapproved_by'] = 1 ;
        this.creditData['f031fstatus'] = 1 ;
        this.creditData['f031fapproved_status'] = "0";
        this.creditData['f031famount_type'] = 'Cash';

        this.creditData['data'] = this.creditList;
        console.log(this.creditData);
        
        if(this.totalDebitAmount === this.totalCreditAmount) {
            console.log(this.creditData);
              alert('Data saved successfully');
              this.addcredit(this.creditData);
        } else {
            alert('Cannot save the data');
        }
    }
    
    ConvertToInt(val){
        return parseInt(val);
      }

    addGlCode() {
        console.log(this.debtorlist);
        if(this.debtorlist['f041fname']==undefined) {
            alert('Select the Ledger Name');
            return false;;
        }
        if(this.selectedOption==0){
            alert('select DR / CR');
            return false;
        }
        var creditData = {}
        creditData['f032fdebit'] = this.debit;
        creditData['f032fcredit'] = this.credit;
        creditData['f032ftype'] = this.selectedOption;
        creditData['debtorList'] = this.debtorlist['f041fname'];
        creditData['f032fledger'] = this.debtorlist['f041fid'];
        if(creditData['f032fcredit']=='') {
            creditData['f032fcredit'] = 0;
        }
        if(creditData['f032fdebit']=='') {
            creditData['f032fdebit'] = 0;
        }
        this.totalCreditAmount  = this.ConvertToInt(this.totalCreditAmount) + this.ConvertToInt(this.credit);
        this.totalDebitAmount  = this.ConvertToInt(this.totalDebitAmount) + this.ConvertToInt(this.debit);
        var amount = this.updateValue(creditData);
        this.updateLedgetListAmount(creditData);
        creditData['currentBalance'] = amount;
        this.creditList.push(creditData);
        console.log(this.creditList);
        this.indGlCode['debit'] = '';
        this.debit = 0;
        this.credit = 0;
    }
  
    addcredit(voucherData){
        this.CreditService.insertCreditItems(voucherData).subscribe(
            data => {
                alert(data['message']);
                if(data['status']=='200') {
                    this.router.navigate(['voucher/credit/Contra']);

                }
        }, error => {
            console.log(error);
        });

    }
}