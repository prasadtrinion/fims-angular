import {Injector} from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { CreditService } from '../../service/credit.service'

@Component({
    selector: 'CreditFormComponent',
    templateUrl: 'creditform.component.html'
})

export class CreditFormComponent implements OnInit {
    creditList =  [];
    creditData = {};

    constructor(
        
        private CreditService: CreditService
    ) { }

    ngOnInit() { 
    }
    addcredit(){
           console.log(this.creditData);
        this.CreditService.insertCreditItems(this.creditData).subscribe(
            data => {
                this.creditList = data['todos'];
        }, error => {
            console.log(error);
        });

    }
    goToListPage(){
        
    }
}