import {Injector} from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { PaymentService } from '../service/payment.service'

@Component({
    selector: 'Payment',
    templateUrl: 'payment.component.html'
})

export class PaymentComponent implements OnInit {
    paymentList =  [];
    paymentData = {};

    constructor(
        
        private PaymentService: PaymentService
    ) { }

    ngOnInit() { 
        
        this.PaymentService.getItems().subscribe(
            data => {
                this.paymentList = data['result']['data'];
        }, error => {
            console.log(error);
        });
    }

    addpayment(){
           console.log(this.paymentData);
        this.PaymentService.insertPaymentItems(this.paymentData).subscribe(
            data => {
                this.paymentList = data['todos'];
        }, error => {
            console.log(error);
        });

    }
}