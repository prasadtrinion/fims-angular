import {Injector} from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { PaymentService } from '../../service/payment.service'

@Component({
    selector: 'PaymentFormComponent',
    templateUrl: 'paymentform.component.html'
})

export class PaymentFormComponent implements OnInit {
    paymentList = [];
    paymentData = {};

    constructor(
        
        private PaymentService: PaymentService
    ) { }

    ngOnInit() { 
    }
    addcredit(){
           console.log(this.paymentData);
        this.PaymentService.insertPaymentItems(this.paymentData).subscribe(
            data => {
                this.paymentList = data['todos'];
        }, error => {
            console.log(error);
        });

    }
    goToListPage(){
        
    }
}