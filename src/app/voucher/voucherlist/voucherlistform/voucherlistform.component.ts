import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { VoucherlistService } from '../../service/voucherlist.service'
import { ActivatedRoute, Router } from '@angular/router';

@Component({
    selector: 'VoucherlistFormComponent',
    templateUrl: 'voucherlistform.component.html'
})

export class VoucherlistFormComponent implements OnInit {
    voucherlistList = [];
    voucherlistData = {};
    id: number;
    constructor(
        private VoucherlistService: VoucherlistService,
        private route: ActivatedRoute,
        private router: Router
    ) { }

    ngOnInit() {
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        console.log(this.id);

        if (this.id > 0) {
            this.VoucherlistService.getItemsDetail(this.id).subscribe(
                data => {
                    this.voucherlistData = data['result'];
                    if(data['result'].f031fstatus===true) {
                    console.log(data);
                    this.voucherlistData['f031fstatus'] = 1;
                } else {
                    this.voucherlistData['f031fstatus'] = 0;

                }
                console.log(this.voucherlistData);
                }, error => {
                    console.log(error);
                });
        }


        console.log(this.id);
    }

    addvoucherlist() {
        console.log(this.voucherlistData);
           this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if(this.id>0) {
            this.VoucherlistService.updateVoucherlistItems(this.voucherlistData,this.id).subscribe(
                data => {
                    this.router.navigate(['voucher/voucherlist']);
            }, error => {
                console.log(error);
            });

       
    } else {
        this.VoucherlistService.insertVoucherlistItems(this.voucherlistData).subscribe(
            data => {
                this.router.navigate(['voucher/voucherlist']);

        }, error => {
            console.log(error);
        });

    }

}
}