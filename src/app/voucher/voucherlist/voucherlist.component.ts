import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { VoucherlistService } from '../service/voucherlist.service'

@Component({
    selector: 'Vocherlist',
    templateUrl: 'voucherlist.component.html'
})

export class VoucherlistComponent implements OnInit {
    voucherlistList =  [];
    voucherlistData = {};
    id: number;

    constructor(
        
        private VoucherlistService: VoucherlistService
        
    ) { }

    ngOnInit() { 
        
        this.VoucherlistService.getItems().subscribe(
            data => {
                this.voucherlistList = data['data']['data'];
        }, error => {
            console.log(error);
        });
    }

    addVoucherlist(){
           console.log(this.voucherlistData);
        this.VoucherlistService.insertVoucherlistItems(this.voucherlistData).subscribe(
            data => {
                this.voucherlistList = data['todos'];
            }, error => {
                console.log(error);
            });

    }
}