import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MaterialModule } from '../material/material.module';

import { CreditComponent } from './credit/credit.component';
import { CreditFormComponent } from './credit/creditform/creditform.component';

import { DebitComponent } from './debit/debit.component';
import { DebitFormComponent } from './debit/debitform/debitform.component';

import { PaymentComponent } from './payment/payment.component';
import { PaymentFormComponent } from './payment/paymentform/paymentform.component';

import { PurchaseComponent } from './purchase/purchase.component';
import { PurchaseFormComponent } from './purchase/purchaseform/purchaseform.component';

import { VoucherlistComponent } from './voucherlist/voucherlist.component';
import { VoucherlistFormComponent } from './voucherlist/voucherlistform/voucherlistform.component';
import { Ng2SearchPipeModule } from 'ng2-search-filter';

import { CreditService } from './service/credit.service'
import { DebitService } from './service/debit.service'
import { PaymentService } from './service/payment.service'
import { PurchaseService } from './service/purchase.service'
import { VoucherlistService } from './service/voucherlist.service'
import { VoucherRoutingModule } from './voucher.router.module';
import {HttpClientModule, HttpClient} from '@angular/common/http';

@NgModule({
    imports: [
        CommonModule,
        Ng2SearchPipeModule,
        FormsModule,
        ReactiveFormsModule,
        MaterialModule,
        VoucherRoutingModule,
        HttpClientModule
    ],
    exports: [],
    declarations: [
        CreditComponent,
        CreditFormComponent,
        DebitComponent,
        DebitFormComponent,
        PaymentComponent,
        PaymentFormComponent,
        PurchaseComponent,
        PurchaseFormComponent,
        VoucherlistComponent,
        VoucherlistFormComponent

    ],
    providers: [
        CreditService,
        DebitService,
        PaymentService,
        PurchaseService,
        VoucherlistService,
    ],
})
export class VoucherModule { }
