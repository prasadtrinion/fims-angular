import {Injector} from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { PurchaseService } from '../../service/purchase.service'

@Component({
    selector: 'PurchaseFormComponent',
    templateUrl: 'purchaseform.component.html'
})

export class PurchaseFormComponent implements OnInit {
    purchaseList = [];
    purchaseData = {};

    constructor(
        
        private PurchaseService: PurchaseService
    ) { }

    ngOnInit() { 
    }
    addpurchase(){
           console.log(this.purchaseData);
        this.PurchaseService.insertPurchaseItems(this.purchaseData).subscribe(
            data => {
                this.purchaseList = data['todos'];
        }, error => {
            console.log(error);
        });

    }
    goToListPage(){
        
    }
}