import {Injector} from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { PurchaseService } from '../service/purchase.service'

@Component({
    selector: 'Purchase',
    templateUrl: 'purchase.component.html'
})

export class PurchaseComponent implements OnInit {
    purchaseList =[];
    purchaseData = {};

    constructor(
        
        private PurchaseService: PurchaseService
    ) { }

    ngOnInit() { 
        
        this.PurchaseService.getItems().subscribe(
            data => {
                this.purchaseList = data['result']['data'];
        }, error => {
            console.log(error);
        });
    }

    addpurchase(){
           console.log(this.purchaseData);
        this.PurchaseService.insertPurchaseItems(this.purchaseData).subscribe(
            data => {
                this.purchaseList = data['todos'];
        }, error => {
            console.log(error);
        });

    }
}