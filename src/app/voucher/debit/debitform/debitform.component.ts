import {Injector} from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { DebitService } from '../../service/debit.service'

@Component({
    selector: 'DebitFormComponent',
    templateUrl: 'debitform.component.html'
})

export class DebitFormComponent implements OnInit {
    debitList = [];
    debitData = {};

    constructor(
        
        private DebitService: DebitService
    ) { }

    ngOnInit() { 
    }
    addcredit(){
           console.log(this.debitData);
        this.DebitService.insertDebitItems(this.debitData).subscribe(
            data => {
                this.debitList = data['todos'];
        }, error => {
            console.log(error);
        });

    }
    goToListPage(){
        
    }
}