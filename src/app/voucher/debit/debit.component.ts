import {Injector} from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { DebitService } from '../service/debit.service'

@Component({
    selector: 'Debit',
    templateUrl: 'debit.component.html'
})

export class DebitComponent implements OnInit {
    debitList =  [];
    debitData = {};

    constructor(
        
        private DebitService: DebitService
    ) { }

    ngOnInit() { 
        
        this.DebitService.getItems().subscribe(
            data => {
                this.debitList = data['result']['data'];
        }, error => {
            console.log(error);
        });
    }

    adddebit(){
           console.log(this.debitData);
        this.DebitService.insertDebitItems(this.debitData).subscribe(
            data => {
                this.debitList = data['todos'];
        }, error => {
            console.log(error);
        });

    }
}