import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AccountsComponent } from './accounts/accounts.component';
 import{ ProfitComponent } from './profit/profit.component';


const routes: Routes = [
  { path: 'accounts', component: AccountsComponent },
  { path: 'profit', component: ProfitComponent }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  
})
export class AccountsRoutingModule { 
  
}