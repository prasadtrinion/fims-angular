import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MaterialModule } from '../material/material.module';

import { AccountsComponent } from './accounts/accounts.component';
import { ProfitComponent } from './profit/profit.component';

import { AccountsRoutingModule } from './accounts.router.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        MaterialModule,
        AccountsRoutingModule
    ],
    exports: [],
    declarations: [
        AccountsComponent,
        ProfitComponent

    ],
    providers: [
    ],
})
export class AccountsModule { }
