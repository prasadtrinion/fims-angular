import { Component, ElementRef, ViewChild, ViewEncapsulation, Injectable } from '@angular/core';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./app.component.css']
})
export class AppComponent {
    token : any;
    constructor(private spinnerService: Ng4LoadingSpinnerService) {
      
  }
  ngOnInit(){
    
    this.token = sessionStorage.getItem('f014ftoken');
  }
  onlyNumberKey(event) {
    console.log(event);
    return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
}
  
}
