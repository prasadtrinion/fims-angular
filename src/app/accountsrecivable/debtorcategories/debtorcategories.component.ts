import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DebtorCategoryService } from "../service/debtorcategory.service";
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'DebtorCategories',
    templateUrl: 'debtorcategories.component.html'
})

export class DebtorCategoriesComponent implements OnInit {
    DebtorCatList =  [];
    id: number;

    constructor(
        private DebtorCategoryService: DebtorCategoryService,
        private spinner: NgxSpinnerService,

    ) { }

    ngOnInit() { 
        this.spinner.show();        
        this.DebtorCategoryService.getItems().subscribe(
            data => {
                this.spinner.hide();
                this.DebtorCatList = data['result'];
        }, error => {
            console.log(error);
        });
    }
}