import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DebtorCategoryService } from "../../service/debtorcategory.service";
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'DebtorCategoryFormComponent',
    templateUrl: 'debtorcategoriesform.component.html'
})

export class DebtorCategoryFormComponent implements OnInit {
    premiseform: FormGroup;
    premiseList = [];
    DebtorCatData = {};
    Debtortype = [];
    DebtorCatList = [];
    ajaxcount = 0;
    stateData = [];
    ajaxCount: number;
    id: number;
    constructor(
        private spinner: NgxSpinnerService,
        private route: ActivatedRoute,
        private DebtorCategoryService: DebtorCategoryService,
        private router: Router
    ) { }
    ngDoCheck() {
        if (this.ajaxCount == 0) {
            this.spinner.hide();
        }
    }
    ngOnInit() {
        this.ajaxCount = 0
        this.spinner.show();
        this.DebtorCatData['f130fstatus'] = 1;

        this.premiseform = new FormGroup({
            premise: new FormControl('', Validators.required),

        });
        let debtortypeobj = {};
        debtortypeobj['name'] = 'Kerajaan';
        this.Debtortype.push(debtortypeobj);
        debtortypeobj = {};
        debtortypeobj['name'] = 'Swasta';
        this.Debtortype.push(debtortypeobj);
        debtortypeobj = {};
        debtortypeobj['name'] = 'Lain-lain';
        this.Debtortype.push(debtortypeobj);
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        if (this.id > 0) {
            this.ajaxCount++;
            this.DebtorCategoryService.getItemsDetail(this.id).subscribe(
                data => {
                    this.ajaxCount--;
                    //console.log(data['result'][0]);
                    this.DebtorCatData = data['result'][0];
                    this.DebtorCatData['f130fstatus'] = parseInt(this.DebtorCatData['f130fstatus']);
                }, error => {
                    console.log(error);
                });
        }
    }

    addDebtorCategory() {
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        if (this.id > 0) {
            this.DebtorCategoryService.updatePremiseItems(this.DebtorCatData, this.id).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert("Debtor Category Already Exists");
                        return false;
                    } else {
                        this.router.navigate(['accountsrecivable/debtorcategory']);
                        alert("Debtor Category has been Updated Successfully");
                    }
                }, error => {
                    alert("Debtor Category Already Exists");
                    console.log(error);
                });


        } else {
            this.DebtorCategoryService.insertPremiseItems(this.DebtorCatData).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert("Debtor Category Already Exists");
                        return false;
                    } else {
                        this.router.navigate(['accountsrecivable/debtorcategory']);
                        alert("Debtor Category has been Added Successfully");
                    }
                }, error => {
                    console.log(error);
                });

        }

    }
}