import { Injector, Injectable } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PremisetypeService } from '../../service/premisetype.service'
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { NgxSpinnerService } from 'ngx-spinner';
import { DefinationmsService } from '../../../studentfinance/service/definationms.service';

@Injectable()
@Component({
    selector: 'PremisetypeFormComponent',
    templateUrl: 'premisetypeform.component.html'
})

export class PremisetypeFormComponent implements OnInit {
    premiseList = [];
    PremiseData = {};
    id: number;
    ajaxCount: number;
    constructor(
        private PremisetypeService: PremisetypeService,
        private route: ActivatedRoute,
        private spinner: NgxSpinnerService,
        private router: Router,
        private spinnerService: Ng4LoadingSpinnerService,
        private DefinationmsService: DefinationmsService,

    ) {
    }
    ngDoCheck() {
        if (this.ajaxCount == 0) {
            this.spinner.hide();
        }
    }
    ngOnInit() {
        this.ajaxCount = 0
        this.spinner.show();
        this.PremiseData['f037fstatus'] = 1;
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.ajaxCount++;
            this.PremisetypeService.getItemsDetail(this.id).subscribe(
                data => {
                    this.ajaxCount--;
                    this.PremiseData = data['result'][0];
                    this.PremiseData['f037fstatus'] = parseInt(data['result'][0]['f037fstatus']);
                }, error => {
                    console.log(error);
                });
        }
    }
    addPremiseType() {
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        this.PremiseData['f037fupdatedBy'] = 1;
        this.PremiseData['f037fcreatedBy'] = 1;
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.PremisetypeService.updatePremiseTypeItems(this.PremiseData, this.id).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert('Premise Code Already Exist');
                    }
                    else {
                        this.router.navigate(['accountsrecivable/premisetype']);
                        alert("Premise Category has been Updated Successfully");
                    }

                }, error => {
                    console.log(error);
                    alert('Premise Code Already Exist');
                });
        } else {
            this.PremisetypeService.insertPremiseTypeItems(this.PremiseData).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert("Premise Code Already Exist");
                    }
                    else {
                        this.router.navigate(['accountsrecivable/premisetype']);
                        alert("Premise Category has been Added Successfully");
                    }

                }, error => {
                    console.log(error);
                });
        }
    }
    onlyNumberKey(event) {
        return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
    }
    checkDuplication() {
        let duplicationObj = {};
        if (this.id > 0) {
            duplicationObj['id'] = this.id;
        }
        duplicationObj['key'] = 'Premise Category';
        duplicationObj['premiseCode'] = this.PremiseData['f037fcode'];
        if(this.PremiseData['f037fcode'] != undefined && this.PremiseData['f037fcode'] != ""){
            this.DefinationmsService.checkDuplication(duplicationObj).subscribe(
                data => {
                    if (parseInt(data['result']) == 1) {
                        alert("Premise Code Already Exist");
                        this.PremiseData['f037fcode'] = '';
                    }
                }, error => {
                    console.log(error);
                });
            }
    }
}