import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { PremisetypeService } from '../service/premisetype.service'
import { faSearch, faEdit } from '@fortawesome/free-solid-svg-icons';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'PremisetypeComponent',
    templateUrl: 'premisetype.component.html'
})
export class PremisetypeComponent implements OnInit {


    premiseData = {};
    premiseList = [];
    id: number;
    faSearch = faSearch;
    faEdit = faEdit;
    constructor(
        private FundService: PremisetypeService,
        private spinner: NgxSpinnerService,
    ) {

    }
    ngOnInit() {
        this.spinner.show();
        this.FundService.getItems().subscribe(
            data => {
                this.spinner.hide();
                this.premiseList = data['result']['data'];
            }, error => {
                console.log(error);
            });
    }
}