import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { VoucherService } from '../../service/voucher.service';
import { RequestinvoiceService } from '../../service/requestinvoice.service ';
import { CustomerService } from '../../../accountsrecivable/service/customer.service';
import { GlcodegenerationService } from '../../../generalledger/service/glcodegeneration.service';
import { TaxsetupcodeService } from '../../../generalledger/service/taxsetupcode.service'
import { ItemService } from '../../service/item.service';
import { FundService } from '../../../generalsetup/service/fund.service';
import { AccountcodeService } from '../../../generalsetup/service/accountcode.service'
import { ActivitycodeService } from '../../../generalsetup/service/activitycode.service';
import { DepartmentService } from '../../../generalsetup/service/department.service';



@Component({
    selector: 'RequestinvoiceapprovalformComponent',
    templateUrl: 'requestinvoiceapprovalform.component.html',
    styleUrls: ['./requestinvoiceapprovalform.component.css']

})

export class RequestinvoiceapprovalformComponent implements OnInit {
    requestinvoiceList = [];
    requestinvoiceData = {};
    requestinvoiceDataheader = {};
    glcodeList = [];
    itemList = [];
    customerList = [];
    taxcodeList = [];
    DeptList = [];
    accountcodeList = [];
    fundList = [];
    activitycodeList = [];
    id: number;
    valueDate: string;
    ajaxCount: number;
    gstValue: number;
    Amount: number;
    taxAmount: number;
    totalAmount: number;

    constructor(
        private GlcodegenerationService: GlcodegenerationService,
        private RequestinvoiceService: RequestinvoiceService,
        private DepartmentService: DepartmentService,
        private ActivitycodeService: ActivitycodeService,
        private AccountcodeService: AccountcodeService,
        private FundService: FundService,
        private VoucherService: VoucherService,
        private TaxsetupcodeService: TaxsetupcodeService,
        private CustomerService: CustomerService,
        private ItemService: ItemService,
        private route: ActivatedRoute,
        private router: Router
    ) { }

    ngDoCheck() {
        const change = this.ajaxCount;
        console.log(this.ajaxCount);
        if (this.ajaxCount == 0) {
            this.editFunction();
            this.ajaxCount = 10;
        }
    }

    editFunction() {
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.RequestinvoiceService.getInvoiceDetailsById(this.id).subscribe(
                data => {
                    this.requestinvoiceList = data['result'];
                    this.requestinvoiceDataheader['f071fapprovedBy'] = 1;
                    this.requestinvoiceDataheader['f071finvoiceDate'] = data['result'][0].f071finvoiceDate;
                    this.requestinvoiceDataheader['f071finvoiceNumber'] = data['result'][0].f071finvoiceNumber;
                    this.requestinvoiceDataheader['f071finvoiceTotal'] = data['result'][0].f071finvoiceTotal;
                    this.requestinvoiceDataheader['f071finvoiceType'] = data['result'][0].f071finvoiceType;
                    this.requestinvoiceDataheader['f071fidCustomer'] = data['result'][0].f071fidCustomer;
                    this.requestinvoiceDataheader['f071fbillType'] = data['result'][0].f071fbillType;
                    this.requestinvoiceDataheader['f071fid'] = data['result'][0].f071fid;

                    console.log(this.requestinvoiceList);
                }, error => {
                    console.log(error);
                });
        }
        if(this.id>0) {
            $("#target *").prop('disabled',true);
        }
    }
    ngOnInit() {
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        console.log(this.id);


        this.ajaxCount = 0;
        //item dropdown
        this.ajaxCount++;
        this.ItemService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.itemList = data['result'];
            }
        );
        
        this.ajaxCount = 0;
        //Customer dropdown
        this.ajaxCount++;
        this.CustomerService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.customerList = data['result']['data'];
            }
        );

        this.ajaxCount = 0;
        //Glcode dropdown
        this.ajaxCount++;
        this.GlcodegenerationService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.glcodeList = data['result']['data'];
            }
        );

        this.ajaxCount = 0;
        //Department dropdown
        this.ajaxCount++;
        this.DepartmentService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.DeptList = data['result']['data'];
            }
        );

        this.ajaxCount = 0;
        //Activity dropdown
        this.ajaxCount++;
        this.ActivitycodeService.getItemsByLevel().subscribe(
            data => {
                this.ajaxCount--;
                this.activitycodeList = data['result']['data'];
            }
        );

        this.ajaxCount = 0;
        //Account dropdown
        this.ajaxCount++;
        this.AccountcodeService.getItemsByLevel().subscribe(
            data => {
                this.ajaxCount--;
                this.accountcodeList = data['result']['data'];
            }
        );


        this.ajaxCount = 0;
        //Fund dropdown
        this.ajaxCount++;
        this.FundService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.fundList = data['result']['data'];
            }
        );

        this.ajaxCount = 0;
        //Tax code dropdown
        this.ajaxCount++;
        this.TaxsetupcodeService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.taxcodeList = data['result']['data'];
            }
        );

    }

    getInvoiceNumber() {
        let typeObject = {};
        typeObject['type'] = this.requestinvoiceDataheader['f071finvoiceType'];
        this.VoucherService.getInvoiceNumber(typeObject).subscribe(
            data => {
                console.log(data);
                this.requestinvoiceDataheader['f071finvoiceNumber'] = data['number'];
                this.valueDate = new Date().toISOString().substr(0, 10);
                console.log(this.valueDate);
                this.requestinvoiceDataheader['f071finvoiceDate'] = this.valueDate;
            }
        );
    }

    ConvertToFloat(val) {
        return parseFloat(val);
    }

    saveRequestinvoice() {

        let invoiceObject = {};
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        console.log(this.id);
        if (this.id > 0) {
            invoiceObject['f071fid'] = this.id;
        }
        invoiceObject['f071finvoiceNumber'] = this.requestinvoiceDataheader['f071finvoiceNumber'];
        invoiceObject['f071finvoiceType'] = this.requestinvoiceDataheader['f071finvoiceType'];
        invoiceObject['f071finvoiceDate'] = this.requestinvoiceDataheader['f071finvoiceDate'];
        invoiceObject['f071finvoiceTotal'] = this.ConvertToFloat(this.requestinvoiceDataheader['f071finvoiceTotal']).toFixed(2);
        invoiceObject['f071fidCustomer'] = this.requestinvoiceDataheader['f071fidCustomer'];
        invoiceObject['f071fbillType'] = this.requestinvoiceDataheader['f071fbillType'];
        invoiceObject['f071finvoiceFrom'] = 2;
        invoiceObject['f071fdescription'] = "Description";
        invoiceObject['f071fstatus'] = 0;
        invoiceObject['f071fisRcp'] = 1;
        invoiceObject['invoice-details'] = this.requestinvoiceList;

        if (this.id > 0) {
            this.RequestinvoiceService.updateRequestInvoiceItems(invoiceObject, this.id).subscribe(
                data => {
                    this.router.navigate(['accountsrecivable/requestinvoice']);

                }, error => {
                    console.log(error);
                });
        } else {
            this.RequestinvoiceService.insertRequestinvoiceItems(invoiceObject).subscribe(
                data => {
                    this.router.navigate(['accountsrecivable/requestinvoice']);

                }, error => {
                    console.log(error);
                });
        }



    }

    getGstvalue() {
        let taxId = this.requestinvoiceData['f072fgstCode'];
        let quantity = this.requestinvoiceData['f072fquantity'];
        let price = this.requestinvoiceData['f072fprice'];
        console.log(this.taxcodeList);
        var taxSelectedObject = {};
        for (var k = 0; k < this.taxcodeList.length; k++) {
            if (this.taxcodeList[k]['f081fid'] == taxId) {
                taxSelectedObject = this.taxcodeList[k];
            }
        }
        this.gstValue = taxSelectedObject['f081fpercentage'];
        this.requestinvoiceData['f072fgstValue'] = this.gstValue;
        this.Amount = (this.ConvertToFloat(price)) * (this.ConvertToFloat(quantity));
        this.taxAmount = ((this.ConvertToFloat(this.Amount) / 100) * this.ConvertToFloat(this.gstValue));//(this.ConvertToFloat(this.gstValue)) / 100 * this.ConvertToFloat((this.Amount));
        let totalAm = ((this.taxAmount) + (this.Amount));
        this.requestinvoiceData['f072ftotalExc'] = this.Amount.toFixed(2);
        this.requestinvoiceData['f072ftaxAmount'] = this.taxAmount.toFixed(2);
        this.requestinvoiceData['f072ftotal'] = totalAm.toFixed(2);

    }

    onBlurMethod() {
        console.log(this.requestinvoiceList);
        var finaltotal = 0;
        for (var i = 0; i < this.requestinvoiceList.length; i++) {
            var totalamount = (this.ConvertToFloat(this.requestinvoiceList[i]['f072fquantity'])) * (this.ConvertToFloat(this.requestinvoiceList[i]['f072fprice']));
            var gstamount = ((this.ConvertToFloat(totalamount) / 100) * this.ConvertToFloat(this.requestinvoiceList[i]['f072fgstValue']));//this.ConvertToFloat(this.gstValue) / 100 * 
            this.requestinvoiceList[i]['f072ftotal'] = (gstamount + totalamount);
            console.log(gstamount);
            console.log(totalamount)
            finaltotal = finaltotal + this.requestinvoiceList[i]['f072ftotal'];
            this.requestinvoiceList[i]['f072ftaxAmount'] = gstamount.toFixed(2);
            this.requestinvoiceList[i]['f072ftotalExc'] = totalamount.toFixed(2);
        }

        this.requestinvoiceDataheader['f071finvoiceTotal'] = (this.ConvertToFloat(finaltotal)).toFixed(2);
    }



    deleteRequestinvoice(object) {
        var cnf = confirm("Do you really want to delete");
        if (cnf == true) {
            console.log(this.requestinvoiceList);
            var index = this.requestinvoiceList.indexOf(object);;
            if (index > -1) {
                this.requestinvoiceList.splice(index, 1);
            }
            this.onBlurMethod();
        }
    }

    addrequestinvoicelist() {
        console.log(this.requestinvoiceData);

        var dataofCurrentRow = this.requestinvoiceData;
        this.requestinvoiceData = {};
        this.requestinvoiceList.push(dataofCurrentRow);
        this.onBlurMethod();

    }

}