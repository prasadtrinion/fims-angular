import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AmpcashbookService } from '../service/ampcashbook.service'
import { BankService } from '../../generalsetup/service/bank.service'
import { HttpClientModule, HttpHeaders, HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { BanksetupaccountService } from '../../generalsetup/service/banksetupaccount.service';
import { environment } from "../../../environments/environment";

@Component({
    selector: 'Ampcashbook',
    templateUrl: 'ampcashbook.component.html'
})

export class AmpcashbookComponent implements OnInit {
    ampcashbookList = [];
    ampcashbookData = {};
    approveData = [];
    ajaxCount: number;
    bankList = [];
    bankList1 = [];
    importedList = [];
    file;
    id: number;
    downloadUrl: string = environment.api.downloadbase;

    constructor(
        private BanksetupaccountService: BanksetupaccountService,
        private AmpcashbookService: AmpcashbookService,
        private BankService: BankService,
        private httpClient: HttpClient,
        private route: ActivatedRoute,
        private router: Router,

    ) { }

    ngOnInit() {
        //Bank dropdown
        this.ajaxCount = 0;
        this.ajaxCount++;
        this.BankService.getCashBankItems().subscribe(
            data => {
                this.ajaxCount--;
                this.bankList = data['result']['data'];
            }
        );
        // company bank 
        this.ajaxCount++;
        this.BanksetupaccountService.getActiveCompanyBank().subscribe(
            data => {
                this.ajaxCount--;
                this.bankList1 = data['result']['data'];
            }, error => {
                console.log(error);
            });
    }
    dateComparison() {

        let fromDate = Date.parse(this.ampcashbookData['fromDate']);
        let toDate = Date.parse(this.ampcashbookData['toDate']);
        if (fromDate > toDate) {
            alert("From Date cannot be greater than To Date !");
            this.ampcashbookData['toDate'] = "";
        }

    }
    fileSelected(event) {
        this.file = event.target.files[0];
        console.log(this.file);
    }

    addAmpcashbook() {

        let fd = new FormData();
        fd.append('file', this.file);

        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.httpClient.post(this.downloadUrl, fd)
            .subscribe(
                data => {
                    this.ampcashbookData['file'] = data['name'];
                    this.AmpcashbookService.insertAmpcashbookItems(this.ampcashbookData).subscribe(
                        data => {
                            this.ampcashbookList = data['result']['result'];
                            console.log(this.ampcashbookList);
                            this.importedList = data['result']['file_data'];
                        }, error => {
                            console.log(error);
                        });
                });
    }
    CashApprovalData() {
        let approvecasharrayList = [];
        for (var i = 0; i < this.ampcashbookList.length; i++) {
            if (this.ampcashbookList[i].cashapprovalstatus == true) {
                approvecasharrayList.push(this.ampcashbookList[i].f096fid);
            }
        }
        if (approvecasharrayList.length < 1) {
            alert("Select atleast one Cash Book to Approve");
            return false;
        }
        var confirmPop = confirm("Do you want to Approve?");
        if (confirmPop == false) {
            return false;
        }
        var approvalObject = {};
        approvalObject['id'] = approvecasharrayList;
        approvalObject['status'] = '1';
        this.AmpcashbookService.updateApproval(approvalObject).subscribe(
            data => {
                alert("Approved Successfully");
                this.AmpcashbookService.insertAmpcashbookItems(this.ampcashbookData).subscribe(
                    data => {
                        this.ampcashbookList = data['result']['result'];
                        this.importedList = data['result']['file_data'];
                    }, error => {
                        console.log(error);
                    });
            }, error => {
                console.log(error);
            });
    }
}