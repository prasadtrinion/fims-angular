import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PremiseComponent } from './premise/premise.component';
import { PremiseFormComponent } from './premise/premiseform/premiseform.component';

import { CategoriesComponent } from './categories/categories.component';
import { CategoriesFormComponent } from './categories/categoriesform/categoriesform.component';

import { RecurringsetupcustomerComponent } from './recurringsetupcustomer/recurringsetupcustomer.component';
import { RecurringsetupcustomerFormComponent } from './recurringsetupcustomer/recurringsetupcustomerform/recurringsetupcustomerform.component';
import { ReceiptnoninvoiceComponent } from './receiptnoninvoice/receiptnoninvoice.component';

import { UtilityComponent } from './utility/utility.component';
import { UtilityFormComponent } from './utility/utilityform/utilityform.component';

import { OnlinepaymenttypeComponent } from './onlinepaymenttype/onlinepaymenttype.component';
import { OnlinepaymenttypeFormComponent } from './onlinepaymenttype/onlinepaymenttypeform/onlinepaymenttypeform.component';

import { MaininvoiceComponent } from './maininvoice/maininvoice.component';
import { MaininvoiceformComponent } from './maininvoice/maininvoiceform/maininvoiceform.component';
import { MaininvoiceapprovalComponent } from './maininvoiceapproval/maininvoiceapproval.component';

import { CustomerComponent } from './customer/customer.component';
import { CustomerFormComponent } from './customer/customerform/customerform.component';

import { RequestinvoiceComponent } from './requestinvoice/requestinvoice.component';
import { RequestinvoiceformComponent } from './requestinvoice/requestinvoiceform/requestinvoiceform.component';
import { RequestinvoiceapprovalComponent } from './requestinvoiceapproval/requestinvoiceapproval.component';

import { ReceiptnoninvoicelistComponent } from './receiptnoninvoice/receiptnoninvoicelist/receiptnoninvoicelist.component';
import { ReceiptnoninvapprovallistComponent } from './recepitnoninvapprovallist/recepitnoninvapprovallist.component';

import { VoucherComponent } from './voucher/voucher.component';
import { VoucherlistComponent } from './voucher/voucherlist/voucherlist.component';
import { VoucherapprovallistComponent } from './voucherapprovallist/voucherapprovallist.component';
import { VoucherapprovalFormComponent } from './voucherapprovallist/voucherapprovalform/voucherapprovalform.component';

import { ReceiptformComponent } from './receipt/receiptform/receiptform.component';
import { NewreceiptinvoiceComponent } from './receipt/newreceiptinvoice/newreceiptinvoice.component';
import { NewreceiptnoninvoiceComponent } from './receipt/newreceiptnoninvoice/newreceiptnoninvoice.component';
import { NewreceiptlistComponent } from './receipt/newreceiptlist/newreceiptlist.component';

import { UtilitybillComponent } from './utilitybill/utilitybill.component';
import { UtilitybillFormComponent } from '../accountsrecivable/utilitybill/utilitybillform/utilitybillform.component';

import { DebtorComponent } from './debtor/debtor.component';
import { DebtorlistComponent } from './debtor/debtorlist/debtorlist.component';
import { DebtorapprovalComponent } from './debtorapproval/debtorapproval.component';

import { CreditnoteComponent } from './creditnote/creditnote.component';
import { CreditnoteformComponent } from './creditnote/creditnoteform/creditnoteform.component';
import { CreditnoteapprovalComponent } from './creditnoteapproval/creditnoteapproval.component';

import { UtilityratesetupComponent } from "./utilityratesetup/utilityratesetup.component";
import { UtilityratesetupFormComponent } from './utilityratesetup/utilityratesetupform/utilityratesetupform.component';

import { DiscountComponent } from './discount/discount.component';
import { DiscountformComponent } from './discount/discountform/discountform.component';

import { DebitnoteComponent } from './debitnote/debitnote.component';
import { DebitnoteformComponent } from './debitnote/debitnoteform/debitnoteform.component';
import { DebitnoteapprovalComponent } from './debitnoteapproval/debitnoteapproval.component';
import { VouchergenerationComponent } from './vouchergeneration/vouchergeneration.component';
import { VouchergenerationFormComponent } from './vouchergeneration/vouchergenerationform/vouchergenerationform.component';
import { RevenueitemtypeComponent } from "./revenueitemtype/revenueitemtype.component";
import { RevenueItemTypeFormComponent } from "./revenueitemtype/revenueitemtypeform/revenueitemtypeform.component";

import { PremisetypeFormComponent } from '../accountsrecivable/premisetype/premisetypeform/premisetypeform.component';
import { PremisetypeComponent } from '../accountsrecivable/premisetype/premisetype.component';
import { RevenuesetupComponent } from "./revenuesetup/revenuesetup.component";
import { RevenuesetupFormComponent } from "./revenuesetup/revenuesetupform/revenuesetupform.component";

import { BatchgenerateComponent } from './batchgenerate/batchgenerate.component';
import { BatchgenerateFormComponent } from './batchgenerate/batchgenerateform/batchgenerateform.component';
import { DiscountapprovalComponent } from './discountapproval/discountapproval.component';

import { StudentdiscountComponent  } from '../accountsrecivable/studentdiscount/studentdiscount.component';
import { StudentdiscountFormComponent } from '../accountsrecivable/studentdiscount/studentdiscountform/studentdiscountform.component';

import { SponsorcreditnoteComponent } from "../accountsrecivable/sponsorcreditnote/sponsorcreditnote.component";
import { SponsorcreditnoteFormComponent } from "../accountsrecivable/sponsorcreditnote/sponsorcreditnoteform/sponsorcreditnoteform.component";

import { SponsordebitnoteComponent } from "../accountsrecivable/sponsordebitnote/sponsordebitnote.component";
import { SponsordebitnoteFormComponent } from "../accountsrecivable/sponsordebitnote/sponsordebitnoteform/sponsordebitnoteform.component";

import { TempbankfileapproveComponent  } from './tempbankfileapprove/tempbankfileapprove.component';

import { AdvancepaymentComponent  } from './advancepayment/advancepayment.component';
import { KnockoffComponent  } from './knockoff/knockoff.component';

import { OnlinePaymentComponent } from './onlinepayment/onlinepayment.component';
import { OnlinePaymentFormComponent } from './onlinepayment/onlinepaymentform/onlinepaymentform.component';
import { OnlinePaymentViewComponent } from './onlinepayment/onlinepaymentview.component';
import { OnlineKnockOffComponent  } from './onlineknockoff/onlineknockoff.component';

import{ RevenueComponent } from './revenue/revenue.component'
import { RevenueFormComponent } from "./revenue/revenueform/revenueform.component";

import { SponsorCreditapprovalComponent } from "./sponsorcreditapproval/sponsorcreditapproval";
import { SponsorDebitapprovalComponent } from "./sponsordebitapproval/sponsordebitapproval.component";

import { ReceiptapprovalComponent } from "./receiptapproval/receiptapproval.component";
import { AmpcashbookComponent } from './ampcashbook/ampcashbook.component'
import { MmpcashbookComponent } from './mmpcashbook/mmpcashbook.component'

import { DebtorCategoryFormComponent } from "./debtorcategories/debtorcategoriesform/debtorcategoriesform.component";
import { DebtorCategoriesComponent } from "./debtorcategories/debtorcategories.component";

import { RevenueTypeFormComponent } from "./revenuetype/revenuetypeform/revenuetypeform.component";
import { RevenuetypeComponent } from "./revenuetype/revenuetype.component";
const routes: Routes = [
  { path: 'premise', component: PremiseComponent },
  { path: 'addnewpremise', component: PremiseFormComponent },
  { path: 'editpremise/:id', component: PremiseFormComponent },
  { path: 'mmpcashbook', component: MmpcashbookComponent },

  { path: 'ampcashbook', component: AmpcashbookComponent },
  { path: 'studentdiscount', component: StudentdiscountComponent},
  { path: 'addnewstudentdiscount', component: StudentdiscountFormComponent},
  { path: 'editstudentdiscount/:id', component: StudentdiscountFormComponent},


  { path: 'receiptinvoice', component: NewreceiptinvoiceComponent },
  { path: 'receiptnoninvoice', component: NewreceiptnoninvoiceComponent },
  { path: 'receiptlist', component: NewreceiptlistComponent },
  { path: 'editreceiptlist/:id', component: NewreceiptinvoiceComponent },  

  { path: 'vouchergeneration', component: VouchergenerationComponent },
  { path: 'addvouchergeneration', component: VouchergenerationFormComponent },
  { path: 'editvouchergeneration/:id', component: VouchergenerationFormComponent },

  { path: 'utilityratesetup', component: UtilityratesetupComponent },
  { path: 'addnewutilityratesetup', component: UtilityratesetupFormComponent },
  { path: 'editutilityratesetup/:id', component: UtilityratesetupFormComponent },

  { path: 'premisetype', component: PremisetypeComponent },
  { path: 'addnewpremisetype', component: PremisetypeFormComponent },
  { path: 'editpremisetype/:id', component: PremisetypeFormComponent },

  { path : 'discountapproval', component :DiscountapprovalComponent},

  { path: 'categories', component: CategoriesComponent },
  { path: 'addnewcategories', component: CategoriesFormComponent },
  { path: 'editcategories/:id', component: CategoriesFormComponent },

  { path: 'recurringsetupcustomer', component: RecurringsetupcustomerComponent },
  { path: 'addnewrecurringsetupcustomer', component: RecurringsetupcustomerFormComponent },
  { path: 'editrecurringsetupcustomer/:id', component: RecurringsetupcustomerFormComponent },

  { path: 'utility', component: UtilityComponent },
  { path: 'addnewutility', component: UtilityFormComponent },
  { path: 'editutility/:id', component: UtilityFormComponent },

  { path: 'voucherapproval', component: VoucherapprovallistComponent },
  { path: 'editvoucherapproval/:id', component: VoucherapprovalFormComponent },


  { path: 'voucherlist', component: VoucherlistComponent },
  { path: 'receiptnoninvoice', component: ReceiptnoninvoiceComponent },
  { path: 'receiptnoninvapprovallist', component: ReceiptnoninvapprovallistComponent },
  { path: 'receiptnoninvoicelist', component: ReceiptnoninvoicelistComponent },


  { path: 'debitnote/:id', component: VoucherComponent},
  { path: 'debitnote/:id/:editId', component: VoucherComponent},
  { path: 'debitnotelist/:id', component: VoucherlistComponent},

  { path: 'creditnote/:id', component: VoucherComponent},
  { path: 'creditnotelist/:id', component: VoucherlistComponent},
  { path: 'creditnote/:id/:editId', component: VoucherComponent},

  { path: 'debtor/:id', component: DebtorComponent },
  { path: 'creditor/:id', component: DebtorComponent },
  { path: 'debtorlist/:id', component: DebtorlistComponent },
  { path: 'creditorlist/:id', component: DebtorlistComponent },
  { path: 'debtorapproval/:id', component: DebtorapprovalComponent },
  { path: 'creditorapproval/:id', component: DebtorapprovalComponent },
  { path: 'receiptnoninvoice', component: ReceiptnoninvoiceComponent },

  { path: 'creditnote', component: CreditnoteComponent},
  { path: 'addnewcreditnote', component: CreditnoteformComponent},
  { path: 'editcreditnote/:id', component: CreditnoteformComponent},
  { path: 'creditnoteapproval', component: CreditnoteapprovalComponent},
  { path: 'editcreditnoteapproval/:id', component: CreditnoteformComponent },
  { path: 'viewcreditnote/:id/:idview', component: CreditnoteformComponent },


  { path: 'debitnote', component: DebitnoteComponent},
  { path: 'addnewdebitnote', component: DebitnoteformComponent},
  { path: 'editdebitnote/:id', component: DebitnoteformComponent},
  { path: 'debitnoteapproval', component: DebitnoteapprovalComponent},
  { path: 'editdebitnoteapproval/:id', component: DebitnoteformComponent },
  { path: 'viewdebit/:id/:viewId', component:DebitnoteformComponent},

  { path: 'paymentvoucher/:id', component: VoucherComponent},
  { path: 'paymentvoucherlist/:id', component: VoucherlistComponent},
  { path: 'paymentvoucherapproval/:id', component: VoucherapprovallistComponent },

  { path: 'receipt', component: NewreceiptinvoiceComponent },
  { path: 'addnewreceipt', component: ReceiptformComponent },
  { path: 'editreceipt/:id', component: NewreceiptinvoiceComponent },

  { path: 'receiptapproval', component: ReceiptapprovalComponent },
  { path: 'viewreceipt/:id/:idview', component: NewreceiptinvoiceComponent },
  
  { path: 'maininvoice', component: MaininvoiceComponent },
  { path: 'addnewmaininvoice', component: MaininvoiceformComponent },
  { path: 'editmaininvoice/:id', component: MaininvoiceformComponent },
  { path: 'maininvoiceapproval', component: MaininvoiceapprovalComponent },
  { path: 'editmaininvoiceapproval/:id', component: MaininvoiceformComponent },
  { path: 'viewinvoice/:id/:viewid', component: MaininvoiceformComponent },


  { path: 'requestinvoice', component: RequestinvoiceComponent },
  { path: 'addnewrequestinvoice', component: RequestinvoiceformComponent },
  { path: 'editrequestinvoice/:id', component: RequestinvoiceformComponent },
  { path: 'requestinvoiceapproval', component: RequestinvoiceapprovalComponent },
  { path: 'viewrequestinvoice/:id/:viewid', component: RequestinvoiceformComponent },

  { path: 'discount', component: DiscountComponent},
  { path: 'addnewdiscount', component: DiscountformComponent},
  { path: 'editdiscount/:id', component: DiscountformComponent},
  { path: 'viewdiscount/:id/:viewid', component:DiscountformComponent},
  
  { path: 'customer', component: CustomerComponent },
  { path: 'addnewcustomer', component: CustomerFormComponent },
  { path: 'editcustomer/:id', component: CustomerFormComponent },

  { path: 'utilitybill', component: UtilitybillComponent },
  { path: 'addnewutilitybill', component: UtilitybillFormComponent },
  { path: 'editutilitybill/:id', component: UtilitybillFormComponent },

  { path: 'revenueitemtype', component: RevenueitemtypeComponent },
  { path: 'addnewrevenueitemtype', component: RevenueItemTypeFormComponent },
  { path: 'editrevenueitemtype/:id', component: RevenueItemTypeFormComponent },

  { path: 'onlinepaymenttype', component: OnlinepaymenttypeComponent },
  { path: 'addnewonlinepaymenttype', component: OnlinepaymenttypeFormComponent },
  { path: 'editonlinepaymenttype/:id', component: OnlinepaymenttypeFormComponent },

  { path: 'sponsorcreditnote', component: SponsorcreditnoteComponent },
  { path: 'addnewsponsorcreditnote', component: SponsorcreditnoteFormComponent },
  { path: 'editsponsorcreditnote/:id', component: SponsorcreditnoteFormComponent },

  { path: 'sponsordebitnote', component: SponsordebitnoteComponent },
  { path: 'addnewsponsordebitnote', component: SponsordebitnoteFormComponent },
  { path: 'editsponsordebitnote/:id', component: SponsordebitnoteFormComponent },

  { path: 'sponsorcreditnoteapproval', component: SponsorCreditapprovalComponent },
  { path: 'viewsponsorcreditnote/:id/:idview', component: SponsorcreditnoteFormComponent },

  { path: 'sponsordebitnoteapproval', component: SponsorDebitapprovalComponent },
  { path: 'viewsponsordebitnote/:id/:idview', component: SponsordebitnoteFormComponent },

  { path: 'revenuesetup', component: RevenuesetupComponent },
  { path: 'addnewrevenuesetup', component: RevenuesetupFormComponent },
  { path: 'editrevenuesetup/:id', component: RevenuesetupFormComponent },

  { path: 'batchgenerate', component: BatchgenerateComponent },
  { path: 'addnewbatchgenerate', component: BatchgenerateFormComponent },
  { path: 'editbatchgenerate/:id', component: BatchgenerateFormComponent },

  { path: 'tempbankfileapprove', component: TempbankfileapproveComponent },
  { path: 'advancepayment', component: AdvancepaymentComponent },
  { path: 'viewadvancepayment/:idview', component: AdvancepaymentComponent },
  { path: 'knockoff', component: KnockoffComponent },

  { path: 'onlinebankpayment', component: OnlinePaymentComponent },
  { path: 'onlineknockoffpayment', component: OnlineKnockOffComponent },
  { path: 'addnewbankpayment', component: OnlinePaymentFormComponent },
  { path: 'editbankpayment/:id', component: OnlinePaymentFormComponent },
  { path: 'viewbankpayment/:id/:date', component: OnlinePaymentViewComponent },

  { path: 'revenue', component: RevenueComponent },
  { path: 'addnewrevenue', component: RevenueFormComponent },
  { path: 'editrevenue/:id', component: RevenueFormComponent },

  { path: 'debtorcategory', component: DebtorCategoriesComponent },
  { path: 'adddebtorcategory', component: DebtorCategoryFormComponent },
  { path: 'editdebtorcategory/:id', component: DebtorCategoryFormComponent },

  { path: 'revenuetype', component: RevenuetypeComponent },
  { path: 'addnewrevenuetype', component: RevenueTypeFormComponent },
  { path: 'editrevenuetype/:id', component: RevenueTypeFormComponent },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],

})
export class AccountsrecivableRoutingModule {

}