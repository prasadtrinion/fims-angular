import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { DiscountService } from '../service/discount.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'DiscountComponent',
    templateUrl: 'discount.component.html'
})

export class DiscountComponent implements OnInit {
    discountList = [];
    discountData = {};
    id: number;
    title = '';

    constructor(

        private DiscountService: DiscountService,
        private spinner: NgxSpinnerService,
        private route: ActivatedRoute,
        private router: Router
    ) { }

    ngOnInit() {
        this.spinner.show();
        this.DiscountService.getAllItems().subscribe(
            data => {
                this.spinner.hide();
                let activityData = [];
                activityData = data['data']['data'];
                console.log("asdf");
                for (var i = 0; i < activityData.length; i++) {
                    if (activityData[i]['f024fapprovalStatus'] == 0) {
                        activityData[i]['f024fapprovalStatus'] = 'Pending';
                    } else if (activityData[i]['f024fapprovalStatus'] == 1) {
                        activityData[i]['f024fapprovalStatus'] = 'Approved';
                    } else {
                        activityData[i]['f024fapprovalStatus'] = 'Rejected';
                    }
                }
                this.discountList = activityData;
            }, error => {
                console.log(error);
            });
    }
}