import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { CustomerService } from '../../../accountsrecivable/service/customer.service';
import { StudentService } from '../../../studentfinance/service/student.service';
import { SponsorService } from '../../../studentfinance/service/sponsor.service';
import { ActivatedRoute, Router } from '@angular/router';
import { DiscountService } from '../../service/discount.service';
import { MaininvoiceService } from '../../service/maininvoice.service';
import { UserService } from '../../../generalsetup/service/user.service';
import { RevenuesetupService } from '../../service/revenuesetup.service'
import { TaxsetupcodeService } from '../../../generalledger/service/taxsetupcode.service'
import { FeeitemService } from '../../../studentfinance/service/feeitem.service';
import { FundService } from '../../../generalsetup/service/fund.service';
import { AccountcodeService } from '../../../generalsetup/service/accountcode.service'
import { ActivitycodeService } from '../../../generalsetup/service/activitycode.service';
import { DepartmentService } from '../../../generalsetup/service/department.service';
import { StaffService } from "../../service/staff.service";
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'DiscountformComponent',
    templateUrl: 'discountform.component.html'
})

export class DiscountformComponent implements OnInit {
    discountList = [];
    discountHeaderData = {};
    customerList = [];
    maininvoiceList = [];
    maininvoiceData = {};
    customerData = {};
    itemList = [];
    userData = {};
    userList = [];
    studentList = [];
    sponserList = [];
    feeItemList = [];
    title = '';
    editId: number;
    discountData = {};
    newDiscountList = [];
    saveDataObject = {};
    debitNotelist = [];
    accountcodeList = [];
    maininvoiceListBasedOnInvoiceId = [];
    DeptList = [];
    activitycodeList = [];
    fundList = [];
    creditnoteApprovalList = [];
    creditnoteApprovalData = {};
    approveDarrayList = [];
    invoiceapprovalData = [];
    id: number;
    viewid: number;
    showCrReadonly: boolean;
    showDrReadonly: boolean;
    debit: number;
    credit: number;
    cash: number;
    payment: number;
    type: string;
    valueDate: string;
    UserId: number;
    ajaxCount: number;
    taxcodeList = [];
    discountListData = [];
    invoicetype = [];
    smartStaffList = [];
    viewDisabled: boolean;
    saveBtnDisable: boolean;
    constructor(

        private RevenuesetupService: RevenuesetupService,
        private DiscountService: DiscountService,
        private UserService: UserService,
        private route: ActivatedRoute,
        private router: Router,
        private StudentService: StudentService,
        private SponsorService: SponsorService,
        private MaininvoiceService: MaininvoiceService,
        private CustomerService: CustomerService,
        private TaxsetupcodeService: TaxsetupcodeService,
        private FeeitemService: FeeitemService,
        private FundService: FundService,
        private AccountcodeService: AccountcodeService,
        private ActivitycodeService: ActivitycodeService,
        private DepartmentService: DepartmentService,
        private StaffService: StaffService,
        private spinner: NgxSpinnerService,
    ) { }


    getType() {
        let typeObject = {};
        typeObject['type'] = this.discountHeaderData['f024ftype'];
        this.type = this.discountHeaderData['f024ftype'];
        console.log(this.type);
        console.log(typeObject);
        this.getDiscountNumber();

    }

    ngDoCheck() {
        const change = this.ajaxCount;
        this.route.paramMap.subscribe((data) => this.viewid = + data.get('viewid'));
        // console.log(this.ajaxCount);
        if (this.ajaxCount == 0) {
            this.editFunction();
            this.spinner.hide();
            this.ajaxCount = 10;
        }
        if (this.ajaxCount == 0 && this.debitNotelist.length > 0) {
            this.ajaxCount = 20;
            console.log("asdfsdf");
            // this.showIcons();
            //console.log(this.newbudgetactivitiesList[0]);
            if (this.debitNotelist[0]['f024fapprovalStatus'] == 1 || this.debitNotelist[0]['f024fapprovalStatus'] == 2) {
                this.saveBtnDisable = true;
                this.viewDisabled = true;

                $("#target input,select").prop("disabled", true);
                $("#target1 input,select").prop("disabled", true);
            }
        }
        if (this.ajaxCount == 0 && this.debitNotelist.length > 0) {
            this.ajaxCount = 20;
            if (this.viewid > 1) {
                this.saveBtnDisable = true;
                this.viewDisabled = true;

                $("#target input,select").prop("disabled", true);
                $("#target1 input,select").prop("disabled", true);
            }
        }
    }
    editFunction() {
        this.ajaxCount++;
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.route.paramMap.subscribe((data) => this.viewid = + data.get('viewid'));

        this.editId = this.id;
        var arrayLength = '';
        if (this.editId > 0) {
            this.DiscountService.getItemsDetail(this.editId).subscribe(
                data => {
                    this.ajaxCount--;
                    console.log(this.id);
                    this.discountHeaderData = data['result'][0];
                    this.newDiscountList = data['result'];
                    this.discountHeaderData['invoiceDate'] = data['result'][0]['invoiceDate'],
                        this.discountHeaderData["f024ftype"] = data['result'][0]['f024ftype'],
                        this.type = data['result'][0]['f024ftype'],
                        this.discountHeaderData['f024fdate'] = data['result'][0]['f024fdate'],
                        this.discountHeaderData['f031fidInvoice'] = data['result'][0]['f031fidInvoice']
                    this.discountHeaderData['f024fdescription'] = data['result'][0]['f024fdescription'],
                        this.discountHeaderData['f024freferenceNumber'] = data['result'][0]['f024freferenceNumber'],
                        this.discountHeaderData['f024ftotalAmount'] = data['result'][0]['f024ftotalAmount'],
                        // this.discountHeaderData['f031fvoucherNumber'] = data['result'][0]['f031fvoucherNumber'],
                        this.discountHeaderData['f024fidCustomer'] = data['result'][0]['f024fidCustomer'];
                    this.discountHeaderData['f024fdiscountType'] = data['result'][0]['f024fdiscountType'];

                    this.getInvoiceList();
                    var balanceInvoiceAmount = 0;
                    for (var i = 0; i < this.newDiscountList.length; i++) {
                        console.log(this.newDiscountList[i]);
                        var creditnoteObject = {};
                        creditnoteObject['f072fidItem'] = this.newDiscountList[i]['f025fidItem'];
                        creditnoteObject['f072fdebitFundCode'] = this.newDiscountList[i]['f025fcreditFundCode'];
                        creditnoteObject['f072fdebitAccountCode'] = this.newDiscountList[i]['f025fcreditAccountCode'];
                        creditnoteObject['f072fdebitActivityCode'] = this.newDiscountList[i]['f025fcreditActivityCode'];
                        creditnoteObject['f072fdebitDepartmentCode'] = this.newDiscountList[i]['f025fcreditDepartmentCode'];

                        creditnoteObject['f072fcreditFundCode'] = this.newDiscountList[i]['f025fdebitFundCode'];
                        creditnoteObject['f072fcreditAccountCode'] = this.newDiscountList[i]['f025fdebitAccountCode'];
                        creditnoteObject['f072fcreditActivityCode'] = this.newDiscountList[i]['f025fdebitActivityCode'];
                        creditnoteObject['f072fcreditDepartmentCode'] = this.newDiscountList[i]['f025fdebitDepartmentCode'];

                        creditnoteObject['f072fquantity'] = this.newDiscountList[i]['f025fquantity'];
                        creditnoteObject['f072fprice'] = this.newDiscountList[i]['f025fprice'];
                        creditnoteObject['f072fgstCode'] = parseInt(this.newDiscountList[i]['f025ftaxCode']);
                        creditnoteObject['f072fgstValue'] = this.newDiscountList[i]['f025fgstValue'];
                        creditnoteObject['f072ftotal'] = this.newDiscountList[i]['f025ftotal'];
                        creditnoteObject['f025fbalanceAmount'] = this.newDiscountList[i]['f025fbalanceAmount'];
                        creditnoteObject['f025fdiscountAmount'] = this.newDiscountList[i]['f025fdiscountAmount'];
                        // creditnoteObject['f025fdiscountPercentage'] = this.newDiscountList[i]['f025fdiscountPercentage'];
                        // balanceInvoiceAmount = (this.ConvertToFloat(this.newDiscountList[i]['f025ftotal'])) - (this.ConvertToFloat(this.newDiscountList[i]['f025fdiscountAmount']));
                        // creditnoteObject['f025fbalanceAmount'] = balanceInvoiceAmount;

                        this.maininvoiceListBasedOnInvoiceId.push(creditnoteObject);
                    }

                    if (data['result'][0]['f024fapprovalStatus'] == 1 || data['result'][0]['f024fapprovalStatus'] == 2) {
                        this.viewDisabled = true;

                        $("#target input,select").prop("disabled", true);
                        $("#target1 input,select").prop("disabled", true);
                    }
                    if (this.viewid > 1) {
                        this.saveBtnDisable = true;
                        this.viewDisabled = true;
                        $("#target input,select").prop("disabled", true);
                        $("#target1 input,select").prop("disabled", true);
                    }
                }, error => {
                    console.log(error);
                });
        }
    }


    getInvoiceList() {
        this.ajaxCount = 0;
        //Invoice dropdown
        this.ajaxCount++;
        this.UserId = this.discountHeaderData['f024fidCustomer'];
        let invoiceObj = {};
        switch (this.type) {
            case 'DCSTA':
                invoiceObj['type'] = "STA";
                break;
            case 'DCSTD':
                invoiceObj['type'] = "STD";
                break;
            case 'DCOR':
                invoiceObj['type'] = "OR";
                break;
            case 'DCSP':
                invoiceObj['type'] = "SP";
                break;
        }
        invoiceObj['UserId'] = this.UserId;
        console.log(this.UserId);

        this.DiscountService.getInvoiceItems(invoiceObj).subscribe(
            data => {
                console.log(data);

                this.maininvoiceList = data['result']['data'];
                // if(this.maininvoiceList.isEmpty()){
                //     alert("No Invoice");
                // }
            }
        );
    }
    getDiscountNumber() {

        let typeObject = {};
        typeObject['type'] = this.discountHeaderData['f024ftype'];
        this.DiscountService.getDiscountNoteNumber(typeObject).subscribe(
            newData => {
                console.log(newData);
                this.discountHeaderData['f024freferenceNumber'] = newData['number'];
            }
        );
    }

    ngOnInit() {
        this.ajaxCount = 0
        this.spinner.show();
        this.viewDisabled = false;
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        this.route.paramMap.subscribe((data) => this.viewid = + data.get('viewid'));
        this.valueDate = new Date().toISOString().substr(0, 10);
        console.log(this.valueDate);
        this.discountHeaderData['f024fdate'] = this.valueDate;
        let invoicetypeobj = {};
        invoicetypeobj['name'] = 'Student';
        invoicetypeobj['type'] = 'DCSTD';
        this.invoicetype.push(invoicetypeobj);
        invoicetypeobj = {};
        invoicetypeobj['name'] = 'Staff';
        invoicetypeobj['type'] = 'DCSTA';

        this.invoicetype.push(invoicetypeobj);
        // invoicetypeobj = {};
        // invoicetypeobj['name'] = 'Sponser';
        // invoicetypeobj['type'] = 'DCSP';

        // this.invoicetype.push(invoicetypeobj);
        invoicetypeobj = {};
        invoicetypeobj['name'] = 'Other Receivables';
        invoicetypeobj['type'] = 'DCOR';

        this.invoicetype.push(invoicetypeobj);

        this.DiscountService.getDiscountItems().subscribe(
            data => {
                this.discountListData = data['result'];
                console.log(this.discountListData);
            }, error => {
                console.log(error);
            });

        //Revenue Item dropdown
        this.ajaxCount++;
        this.RevenuesetupService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.itemList = data['result'];
            }
        );
        //Student dropdown
        this.ajaxCount++;
        this.StudentService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                console.log(this.ajaxCount);

                this.studentList = data['result']['data'];
            }
        );

        //Sponser dropdown
        this.ajaxCount++;
        this.SponsorService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                console.log(this.ajaxCount);

                this.sponserList = data['result']['data'];
            }
        );

        //Customer dropdown
        this.ajaxCount++;
        this.CustomerService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.customerList = data['result']['data'];
            }
        );
        //Department dropdown
        this.ajaxCount++;
        this.DepartmentService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.DeptList = data['result']['data'];
            }
        );
        //  staff dropdown
        this.ajaxCount++;
        this.StaffService.getItems().subscribe(
            data => {
                console.log(data);
                this.ajaxCount--;
                this.smartStaffList = data['result']['data'];

                console.log(this.ajaxCount);
            }
        );

        //Activity dropdown
        this.ajaxCount++;
        this.ActivitycodeService.getItemsByLevel().subscribe(
            data => {
                this.ajaxCount--;
                this.activitycodeList = data['result']['data'];
            }
        );


        //Account dropdown
        this.ajaxCount++;
        this.AccountcodeService.getItemsByLevel().subscribe(
            data => {
                this.ajaxCount--;
                this.accountcodeList = data['result']['data'];
            }
        );



        //Fund dropdown
        this.ajaxCount++;
        this.FundService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.fundList = data['result']['data'];
            }
        );

        //User dropdown
        this.ajaxCount++;
        this.UserService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.userList = data['result']['data'];
            }
        );
        //FeeItem Dropdown
        this.ajaxCount++;
        this.FeeitemService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.feeItemList = data['result']['data'];
            }
        );
        //Taxcode dropdown
        this.ajaxCount++;
        this.TaxsetupcodeService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.taxcodeList = data['result']['data'];
            }
        );


        // this.ajaxCount++;
        // this.DiscountService.getDiscountItems().subscribe(
        //     data => {
        //         this.ajaxCount--;
        //         this.discountList = data['result'];
        //         console.log(this.discountList);
        //     }
        // );

    }
    ConvertToFloat(val) {
        return parseFloat(val);
    }
    // getBalanceAmount() {
    //     var balanceInvoiceAmount = 0;
    //     for (var i = 0; i < this.maininvoiceListBasedOnInvoiceId.length; i++) {
    //         balanceInvoiceAmount = this.ConvertToFloat(this.maininvoiceListBasedOnInvoiceId[i]['f072ftotal']);
    //         this.maininvoiceListBasedOnInvoiceId[i]['f025fbalanceAmount'] = balanceInvoiceAmount;
    //     }
    // }
    getInvoiceDetails() {
        let invid = 0;
        invid = this.discountHeaderData['f024fidInvoice'];
        console.log(invid);
        this.MaininvoiceService.getInvoiceDetailsById(invid).subscribe(
            data => {
                console.log(data);
                this.maininvoiceListBasedOnInvoiceId = data['result'];
                for (var i = 0; i < this.maininvoiceListBasedOnInvoiceId.length; i++) {
                    this.maininvoiceListBasedOnInvoiceId[i]['f025fidInvoiceDetail'] = this.maininvoiceListBasedOnInvoiceId[i]['f072fid'];
                    this.maininvoiceListBasedOnInvoiceId[i]['f072fgstCode'] = parseInt(this.maininvoiceListBasedOnInvoiceId[i]['f072fgstCode']);
                    this.maininvoiceListBasedOnInvoiceId[i]['f025fbalanceAmount'] = this.ConvertToFloat(this.maininvoiceListBasedOnInvoiceId[i]['f072fbalanceAmount']).toFixed(2);
                    // this.getBalanceAmount();
                }
                this.discountHeaderData['f024ftotalAmount'] = parseFloat(data['result'][0]['f071finvoiceTotal']).toFixed(2);
                this.discountHeaderData['invoiceDate'] = data['result'][0]['f071finvoiceDate'];
            }
        );
    }
    disableFieldsOfAP() {
        this.showCrReadonly = false;
        this.showDrReadonly = false;
        for (var i = 0; i < this.maininvoiceListBasedOnInvoiceId.length; i++) {
            if (this.maininvoiceListBasedOnInvoiceId[i]['f025fdiscountPercentage'] == undefined ||
                this.maininvoiceListBasedOnInvoiceId[i]['f025fdiscountPercentage'] == 0 ||
                this.maininvoiceListBasedOnInvoiceId[i]['f025fdiscountPercentage'] == '') {
                this.showDrReadonly = true;
            }
            if (this.maininvoiceListBasedOnInvoiceId[i]['f025fdiscountAmount'] == undefined ||
                this.maininvoiceListBasedOnInvoiceId[i]['f025fdiscountAmount'] == 0 ||
                this.maininvoiceListBasedOnInvoiceId[i]['f025fdiscountAmount'] == '') {
                this.showCrReadonly = true;
            }
        }
        if (this.showCrReadonly == true && this.showDrReadonly == true) {
            this.showCrReadonly = false;
            this.showDrReadonly = false;
        }
        console.log(this.showCrReadonly);
        console.log(this.showDrReadonly);
    }


    saveData() {
        if (this.discountHeaderData['f024ftype'] == undefined) {
            alert('Select Customer Type');
            return false;
        }
        if (this.discountHeaderData['f024fdiscountType'] == undefined) {
            alert('Select Discount Type');
            return false;
        }
        if (this.discountHeaderData['f024fidCustomer'] == undefined) {
            if (this.type == 'DCSTD') {
                alert("Select Student")
                return false;
            }
            if (this.type == 'DCOR') {
                alert("Select Customer")
                return false;
            }
            if (this.type == 'DCSTA') {
                alert("Select Staff")
                return false;
            }
            if (this.type == 'DCSP') {
                alert("Select Sponsor")
                return false;
            }
        }
        if (this.discountHeaderData['f024fidInvoice'] == undefined) {
            alert('Select Invoice Number');
            return false;
        }
        if (this.discountHeaderData['f024fdescription'] == undefined) {
            alert('Enter Description');
            return false;
        }
        // for (var i = 0; i < this.maininvoiceListBasedOnInvoiceId.length; i++) {

        // }
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.saveDataObject = {};
        this.saveDataObject['f024fid'] = this.discountHeaderData['f024fid'];
        this.saveDataObject["f024fidInvoice"] = this.discountHeaderData['f024fidInvoice'],
            this.saveDataObject["f024fidCustomer"] = this.discountHeaderData['f024fidCustomer'],
            this.saveDataObject["f024freferenceNumber"] = this.discountHeaderData['f024freferenceNumber'],
            this.saveDataObject["f024fdate"] = this.discountHeaderData['f024fdate'],
            this.saveDataObject["f024fdescription"] = this.discountHeaderData['f024fdescription'],
            this.saveDataObject["f024fdiscountNumber"] = this.discountHeaderData['f024fdiscountNumber'],
            this.saveDataObject["f024fapprovalStatus"] = 0,
            this.saveDataObject['f024ftype'] = this.discountHeaderData['f024ftype'];
        this.saveDataObject['f024fdiscountType'] = this.discountHeaderData['f024fdiscountType'];
        this.saveDataObject["f024ftotalAmount"] = parseFloat(this.discountHeaderData['f024ftotalAmount']).toFixed(2);



        for (var i = 0; i < this.maininvoiceListBasedOnInvoiceId.length; i++) {
            var debitnoteObject = {}
            if (this.id > 0) {
                debitnoteObject['f025fid'] = this.maininvoiceListBasedOnInvoiceId[i]['f025fid'];
            }
            debitnoteObject['f025fidInvoiceDetail'] = this.maininvoiceListBasedOnInvoiceId[i]['f072fid'];
            debitnoteObject['f025fidItem'] = this.maininvoiceListBasedOnInvoiceId[i]['f072fidItem'];
            debitnoteObject['f025fdebitFundCode'] = this.maininvoiceListBasedOnInvoiceId[i]['f072fdebitFundCode'];
            debitnoteObject['f025fdebitAccountCode'] = this.maininvoiceListBasedOnInvoiceId[i]['f072fdebitAccountCode'];
            debitnoteObject['f025fdebitActivityCode'] = this.maininvoiceListBasedOnInvoiceId[i]['f072fdebitActivityCode'];
            debitnoteObject['f025fdebitDepartmentCode'] = this.maininvoiceListBasedOnInvoiceId[i]['f072fdebitDepartmentCode'];

            debitnoteObject['f025fcreditFundCode'] = this.maininvoiceListBasedOnInvoiceId[i]['f072fcreditFundCode'];
            debitnoteObject['f025fcreditAccountCode'] = this.maininvoiceListBasedOnInvoiceId[i]['f072fcreditAccountCode'];
            debitnoteObject['f025fcreditActivityCode'] = this.maininvoiceListBasedOnInvoiceId[i]['f072fcreditActivityCode'];
            debitnoteObject['f025fcreditDepartmentCode'] = this.maininvoiceListBasedOnInvoiceId[i]['f072fcreditDepartmentCode'];
            debitnoteObject['f025fquantity'] = this.maininvoiceListBasedOnInvoiceId[i]['f072fquantity'];
            debitnoteObject['f025fprice'] = parseFloat(this.maininvoiceListBasedOnInvoiceId[i]['f072fprice']).toFixed(2);
            debitnoteObject['f025ftaxCode'] = this.maininvoiceListBasedOnInvoiceId[i]['f072fgstCode'];
            debitnoteObject['f025fgstValue'] = this.maininvoiceListBasedOnInvoiceId[i]['f072fgstValue'];
            debitnoteObject['f025ftotal'] = parseFloat(this.maininvoiceListBasedOnInvoiceId[i]['f072ftotal']).toFixed(2);
            // debitnoteObject['f025fbalanceAmount'] = parseFloat(this.maininvoiceListBasedOnInvoiceId[i]['f025fbalanceAmount']).toFixed(2);

            // debitnoteObject['f025fdiscountPercentage'] = this.maininvoiceListBasedOnInvoiceId[i]['f025fdiscountPercentage'];
            debitnoteObject['f025fdiscountAmount'] = parseFloat(this.maininvoiceListBasedOnInvoiceId[i]['f025fdiscountAmount']).toFixed(2);
            debitnoteObject['f025fbalanceAmount'] = this.ConvertToFloat(this.maininvoiceListBasedOnInvoiceId[i]['f025fbalanceAmount'] - this.maininvoiceListBasedOnInvoiceId[i]['f025fdiscountAmount']).toFixed(2);


            this.debitNotelist.push(debitnoteObject);
        }
        this.saveDataObject["discount-details"] = this.debitNotelist;



        console.log(this.saveDataObject);
        // console.log(this.maininvoiceListBasedOnInvoiceId);

        // this.saveDataObject["discount-details"] = this.newDiscountList,

        // console.log(this.saveDataObject);
        if (this.id > 0) {
            this.DiscountService.insertDiscountItems(this.saveDataObject).subscribe(
                data => {
                    this.discountHeaderData = data['result'];

                    this.router.navigate(['/accountsrecivable/discount']);
                    alert("Discount has been Updated Succcessfully");

                    console.log(this.discountHeaderData);
                }, error => {
                    console.log(error);
                });
        } else {
            this.DiscountService.insertDiscountItems(this.saveDataObject).subscribe(
                data => {
                    this.discountHeaderData = data['result'];

                    this.router.navigate(['/accountsrecivable/discount']);
                    alert("Discount has been Added Succcessfully");

                    console.log(this.discountHeaderData);
                }, error => {
                    console.log(error);
                });
        }
    }
    ConvertToInt(val) {
        return parseInt(val);
    }


    deleteVoucher(object) {
        var cnf = confirm("Do you really want to delete");
        if (cnf == true) {
            console.log(this.newDiscountList);
            var index = this.newDiscountList.indexOf(object);;
            if (index > -1) {
                this.newDiscountList.splice(index, 1);
            }
        }
    }

    addOpeningbalance() {

        var dataofCurrentRow = this.discountData;

        this.newDiscountList.push(dataofCurrentRow);
        console.log(this.newDiscountList);
        this.discountData = {};

    }
    onlyNumberKey(event) {
        //console.log(event);
        if (event.charCode == 46) {

        } else {
            return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
        }
    }
    maximize() {
        $("#myModal").addClass("modal");
        $("#overlay1").show();
        $(".maximize-icon").addClass("hidden");
        $(".minimize-icon").removeClass("hidden");
        $(".modal-backdrop").show();

    }
    minimize() {
        $("#myModal").removeClass("modal");
        $("#overlay1").hide();
        $(".maximize-icon").removeClass("hidden");
        $(".minimize-icon").addClass("hidden");
        $(".modal-backdrop").hide();
        $('#myModal').show();
    }
    getList() {
        this.spinner.show();
        this.DiscountService.getItems(0).subscribe(
            data => {
                this.spinner.hide();
                this.discountList = data['result'];
                for (var i = 0; i < this.discountList.length; i++) {
                    this.discountList['f024fapprovalStatus'] = false;
                }
            }, error => {
                console.log(error);
            });
    }
    ApprovalListData() {
        console.log(this.discountList);
        this.approveDarrayList = [];
        for (var i = 0; i < this.discountList.length; i++) {
            if (this.discountList[i].f024fapprovalStatus == true) {
                this.approveDarrayList.push(this.discountList[i].f024fid);
            }
        }
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.approveDarrayList.push(this.id);
        var confirmPop = confirm("Do you want to Approve?");
        if (confirmPop == false) {
            return false;
        }
        var approvalObject = {};
        approvalObject['id'] = this.approveDarrayList;
        approvalObject['status'] = '1';
        approvalObject['reason'] = '';
        this.DiscountService.updateApproval(approvalObject).subscribe(
            data => {
                this.getList();
                this.invoiceapprovalData['reason'] = '';
                alert("Approved Successfully");
                this.router.navigate(['/accountsrecivable/discountapproval']);
            }, error => {
                console.log(error);
            });

    }
    regectApprovalListData() {
        this.approveDarrayList = [];
        for (var i = 0; i < this.discountList.length; i++) {
            if (this.discountList[i].f024fapprovalStatus == true) {
                this.approveDarrayList.push(this.discountList[i].f024fid);
            }
        }
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.approveDarrayList.push(this.id);

        if (this.invoiceapprovalData['reason'] == undefined) {
            alert('Enter the Remarks');
            return false;
        }
        var confirmPop = confirm("Do you want to Reject?");
        if (confirmPop == false) {
            return false;
        }
        var rejectObject = {}
        rejectObject['id'] = this.approveDarrayList;
        rejectObject['status'] = '2';
        rejectObject['reason'] = this.invoiceapprovalData['reason'];
        this.DiscountService.updateApproval(rejectObject).subscribe(
            data => {
                this.getList();
                this.invoiceapprovalData['reason'] = '';
                this.router.navigate(['/accountsrecivable/discountapproval']);
                alert("Rejected Successfully");
            }, error => {
                console.log(error);
            });
    }
}