import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { VoucherapprovalService } from './../service/voucherapproval.service';
import { ActivatedRoute, Router } from '@angular/router';


@Component({
    selector: 'VoucherapprovallistComponent',
    templateUrl: 'voucherapprovallist.component.html'
})

export class VoucherapprovallistComponent implements OnInit {
    voucherapprovalList =  [];
    voucherapprovalData = {};
    id: number;
    title: string;
    type: string;
    selectAllCheckbox:false;

    constructor(
        
        private VoucherapprovalService: VoucherapprovalService,
        private route: ActivatedRoute
    ) { }

    ngOnInit() { 
        this.getList();

    }
    getList(){
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        
        this.VoucherapprovalService.getItems().subscribe(
            data => {
                
                this.voucherapprovalList = data['result'];
        }, error => {
            console.log(error);
        });
    }
    voucherApprovalListData() {
        let approveDarrayList = [];
        console.log(this.voucherapprovalList);
            for (var i = 0; i < this.voucherapprovalList.length; i++) {
                if(this.voucherapprovalList[i].f071fstatus==true) {
                    approveDarrayList.push(this.voucherapprovalList[i].f071fid);
                }
            }
            var approvalObject = {};
            approvalObject['id'] = approveDarrayList;
    
            this.VoucherapprovalService.updateApproval(approvalObject).subscribe(
                data => {
                    this.getList();
                }, error => {
                console.log(error);
            });
    
        }
    selectAll() {
        console.log(this.selectAllCheckbox);
        for (var i = 0; i < this.voucherapprovalList.length; i++) {
                this.voucherapprovalList[i].f071fstatus = this.selectAllCheckbox;
        }
        //console.log(this.budgetcostcenterapprovalList);
      }
    
}