import {Injector} from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { TempbankfileapprovalService } from '../service/tempbankfileapproval.service'
import { AlertService } from './../../_services/alert.service';
@Component({
    selector: 'Tempbankfileapprove',
    templateUrl: 'tempbankfileapprove.component.html'
})

export class TempbankfileapproveComponent implements OnInit {
   
    tempbankapproveList =  [];
    tempbankapproveData = {};
    selectAllCheckbox = true;

    constructor(
        
        private TempbankfileapprovalService: TempbankfileapprovalService,
        private AlertService: AlertService
    ) { }

    ngOnInit() {    
       this.getListData();
    }
    getListData(){
        this.TempbankfileapprovalService.getItemsDetail().subscribe(
            data => {
                this.tempbankapproveList = data['result']['data'];
                console.log(this.tempbankapproveList);
        }, error => {
            console.log(error);
        });
    }
    
    tempbankapproveListData() {
        console.log(this.tempbankapproveList);
        var approvaloneIds = [];
        for (var i = 0; i < this.tempbankapproveList.length; i++) {
            if(this.tempbankapproveList[i].f078fapprovedStatus==true) {
                approvaloneIds.push(this.tempbankapproveList[i]);
            }
        }
        var approvaloneUpdate = {};
        approvaloneUpdate['id'] = approvaloneIds;

        this.TempbankfileapprovalService.updateTempbankfileapprovalItems(approvaloneUpdate).subscribe(
            data => {
                this.AlertService.success(" Updated Sucessfully ! ");

                this.getListData();
        }, error => {
            console.log(error);
        });
    }


    // rejectmomentapprovalListData(){
    //     console.log(this.tempbankapprovalList);
    //     var rejectoneIds = [];
    //     for (var i = 0; i < this.tempbankapprovalList.length; i++) {
    //         if(this.tempbankapprovalList[i].f017fapprovedStatus==true) {
    //             rejectoneIds.push(this.tempbankapprovalList[i].f070fid);
    //         }
    //     }
    //     var rejectoneUpdate = {};
    //     rejectoneUpdate['id'] = rejectoneIds;
    //     rejectoneUpdate['reason'] = this.tempbankapprovalData['f063fapprove1Reson'];
    //     this.TempbankfileapprovalService.rejectTempbankfileapproval(rejectoneUpdate).subscribe(
    //         data => {
    //             this.getListData();
    //     }, error => {
    //         console.log(error);
    //     });
    
    // }
    selectAll() {
        console.log("asdf");
        console.log(this.selectAllCheckbox);
        for (var i = 0; i < this.tempbankapproveList.length; i++) {
                this.tempbankapproveList[i].f017fapprovedStatus = this.selectAllCheckbox;
        }
        console.log(this.tempbankapproveList);
      }

    }
