import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { UtilityService } from '../../service/utility.service';
import { UtilitybillService } from '../../service/utilitybill.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UtilityratesetupService } from '../../service/utilityratesetup.service';
import { PremiseService } from '../../service/premise.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'UtilitybillFormComponent',
    templateUrl: 'utilitybillform.component.html'
})

export class UtilitybillFormComponent implements OnInit {
    utilityform: FormGroup;
    utilitybillList = [];
    utilitybillData = {};
    utilityList = [];
    premiseList = [];
    multiplyingUnit: number;
    premiseData = {};
    utilityrateList = [];
    getpremiseList = [];
    calculatedAmount: number;
    calculatedAmountDisplay: string;
    calculationAmount: number;
    pendingUnit: number;
    lastReadingValue: number;
    totalUnitUsed: number;
    ajaxCount: number;

    id: number;
    constructor(
        private UtilityService: UtilityService,
        private UtilitybillService: UtilitybillService,
        private route: ActivatedRoute,
        private UtilityratesetupService: UtilityratesetupService,
        private router: Router,
        private PremiseService: PremiseService,
        private spinner: NgxSpinnerService,
    ) { }


    ngDoCheck() {
        const change = this.ajaxCount;
        console.log(this.ajaxCount);
        if (this.ajaxCount == 0) {
            this.editFunction();
            this.spinner.hide();
            this.ajaxCount = 10;
        }
    }

    editFunction() {
        this.ajaxCount++;
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.UtilitybillService.getItemsDetail(this.id).subscribe(
                data => {
                    this.ajaxCount--;
                    this.utilitybillData = data['result'][0];

                    this.utilitybillData['f078fidPremise'] = parseInt(this.utilitybillData['f078fidPremise']);
                    this.utilitybillData['f078fidUtility'] = parseInt(this.utilitybillData['f078fidUtility']);

                    this.utilitybillData['f078fstatus'] = parseInt(this.utilitybillData['f078fstatus']);
                    console.log(this.utilitybillData);
                }, error => {
                    console.log(error);
                });
        }

    }

    ngOnInit() {
        this.ajaxCount = 0;
        this.spinner.show();
        this.ajaxCount++;
        this.PremiseService.getActiveItems().subscribe(
            data => {
                this.ajaxCount--;
                this.premiseList = data['result']['data'];
            }, error => {
                console.log(error);
            });
        //utility dropdown
        this.ajaxCount++;
        this.UtilityService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.utilityrateList = data['result']['data'];
                console.log(this.utilityrateList);
            }, error => {
                console.log(error);
            });
        //utility rate setup dropdown
        this.ajaxCount++;
        this.UtilityratesetupService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.utilityList = data['result'];
                
                console.log(this.utilityList);
            }, error => {
                console.log(error);
            });



        //this.utilitybillData['f078fidUtility'] = '';

        this.utilityform = new FormGroup({
            utility: new FormControl('', Validators.required),

        });

        this.utilitybillData['f078fstatus'] = 1;

        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        // if(this.id>0) {
        //     this.ajaxCount++;
        //     this.UtilityService.getItems().subscribe(
        //         data => {
        //             this.ajaxCount--;
        //             this.utilityList = data['result']['data'];
        //         }, error => {
        //             console.log(error);
        //         });
        // } 

    }

    ShowUtilityList() {
        let id = this.utilitybillData['f078fidPremise'];
        this.UtilityService.getUtiltityItems(id).subscribe(
            data => {
                this.utilityrateList = data['result'];
            }, error => {
                console.log(error);
            });
    }

    getLastReading() {
        this.ajaxCount++;
        let itemId = this.utilitybillData['f078fidUtility'];
        this.UtilitybillService.getPremiseItems(itemId).subscribe(
            data => {
                this.ajaxCount--;
                this.getpremiseList = data['result'][0];
                this.utilitybillData['f078fidPremise'] = data['result'][0]['premiseName']
            }, error => {
                console.log(error);
            });

        for (var j = 0; j < this.utilityrateList.length; j++) {
            if (this.utilityrateList[j]['f078fid'] == this.utilitybillData['f078fidUtility']) {
                this.lastReadingValue = parseInt(this.utilityrateList[j]['f078flastReading']);
                this.utilitybillData['f078ffromDate'] = this.utilityrateList[j]['f078fdate'];
            }
        }
        this.utilitybillData['f078flastReading'] = this.lastReadingValue;

        // this.getTotalUnit();
    }

    getToDate(){
        this.utilitybillData['f078ftoDate']=this.utilitybillData['f078finitialReading'];
    }
    getTotalUnit() {
        var currentReading = parseInt(this.utilitybillData['f078fcurrentReadingValue']);
        if (this.lastReadingValue > currentReading) {
            alert("Current Reading Value cannot be less than Last Reading Value");
            this.utilitybillData['f078fcurrentReadingValue'] = '';
            return;
        } else {
            this.totalUnitUsed = currentReading - this.lastReadingValue;
            this.utilitybillData['f078funits'] = this.totalUnitUsed;
            this.findAmount();
        }
    }

    GetSortOrder(prop) {  
        return function(a, b) {  
            if (a[prop] > b[prop]) {  
                return 1;  
            } else if (a[prop] < b[prop]) {  
                return -1;  
            }  
            return 0;  
        }  
    }  
      
    

    findAmount() {
        this.calculatedAmount = 0;
        let number = this.totalUnitUsed;
        this.pendingUnit = 0;
        this.calculationAmount = 0;
        var calculatedAmount = 0;
        var loopgenerated = 1;
        this.multiplyingUnit = 0;
        this.utilityList.sort(this.GetSortOrder("f078fid")); //Pass the attribute to be sorted on  
    
        console.log(this.utilityList);

        for (var i = 0; i < this.utilityList.length; i++) {
            //    if( (parseFloat(number) >= parseFloat(this.utilityrateList[i]['f078fminUnit']))  &&
            //    (parseFloat(number) <= parseFloat(this.utilityrateList[i]['f078fmaxUnit']))) {
            //     this.calculatedAmount = parseFloat(this.utilityrateList[i]['f078famount'])*parseFloat(number);
            //         break;
            //    }

            let minunit = this.utilityList[i]['f078fminUnit'];
            let maxunit = this.utilityList[i]['f078fmaxUnit'];
            let amount = this.utilityList[i]['f078famount'];
            console.log('i = ' + i);
            console.log('minunit = ' + minunit);
            console.log('maxunit = ' + maxunit);
            if (number >= (parseFloat(maxunit) - parseFloat(minunit) + 1)) {
                this.pendingUnit = number - (parseFloat(maxunit) - parseFloat(minunit) + 1);
                this.multiplyingUnit = parseFloat(maxunit) - parseFloat(minunit)+1;
                number = this.pendingUnit;
            } else {
                this.multiplyingUnit = number;
                this.pendingUnit = number;
                loopgenerated = 0;
            }

            // number = parseInt(number) - (pendingUnit);
            console.log('number = ' + number);
            console.log('Multi = ' + this.multiplyingUnit);
            this.calculationAmount = this.calculationAmount + (parseFloat(amount) * (this.multiplyingUnit));
            console.log('Calculated amount = ' + this.calculationAmount);

            if (loopgenerated == 0) {
                break;
            }
        }
        console.log(this.calculationAmount);
        this.utilitybillData['f078fcalculatedAmount'] = this.calculationAmount;//this.calculatedAmount.toFixed(2);
        // this.calculatedAmountDisplay = this.calculatedAmount.toFixed(2);
    }
    dateComparison() {

        let fromDate = Date.parse(this.utilitybillData['f078ffromDate']);
        let toDate = Date.parse(this.utilitybillData['f078finitialReading']);
        if (fromDate > toDate) {
            alert("Reading Date cannot be Less than From Date !");
            this.utilitybillData['f078finitialReading'] = "";
        }

    }
    addUtility() {
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        this.utilitybillData['f078totalUsage'] = this.utilitybillData['f078funits'];
        this.utilitybillData['f078famount'] = this.utilitybillData['f078fcalculatedAmount'];
        var utilityFromDate = new Date(this.utilitybillData['f078ffromDate']);
        var fromSeconds = utilityFromDate.getTime() / 1000;

        var utilityToDate = new Date(this.utilitybillData['f078ftoDate']);
        var toSeconds = utilityToDate.getTime() / 1000;

        if (toSeconds < fromSeconds) {
            alert('From Date cannot be greater than todate');
            return;
        }
        if (parseInt(this.utilitybillData['f078fcurrentReadingValue']) < parseInt(this.utilitybillData['f078foldReadingValue'])) {
            alert("Current Reading Value cannot be less than Last Reading Value");
            return false;
        }
       
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.UtilitybillService.updateUtilitybillItems(this.utilitybillData, this.id).subscribe(
                data => {
                    this.router.navigate(['accountsrecivable/utilitybill']);
                    alert("Utility Bill has been Updated Successfully");
                }, error => {
                    console.log(error);
                });


        } else {

            this.UtilitybillService.insertUtilitybillItems(this.utilitybillData).subscribe(
                data => {
                    this.router.navigate(['accountsrecivable/utilitybill']);
                    alert("Utility Bill has been Added Successfully");

                }, error => {
                    console.log(error);
                });
        }
    }
    onlyNumberKey(event) {
        //console.log(event);
        if (event.charCode == 46) {

        } else {
            return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
        }
    }
}