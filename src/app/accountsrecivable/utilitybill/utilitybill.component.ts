import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UtilitybillService } from '../service/utilitybill.service';
import { faSearch, faEdit } from '@fortawesome/free-solid-svg-icons';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'UtilitybillComponent',
    templateUrl: 'utilitybill.component.html'
})

export class UtilitybillComponent implements OnInit {
    utilitybillList =  [];
    utilitybillData = {};
    faSearch = faSearch;
    faEdit = faEdit;
    id: number;

    constructor(
        private UtilitybillService: UtilitybillService,
        private spinner: NgxSpinnerService,

    ) { }

    ngOnInit() { 
        this.spinner.show();        
        this.UtilitybillService.getItems().subscribe(
            data => {
                this.spinner.hide();
                this.utilitybillList = data['result']['data'];
        }, error => {
            console.log(error);
        });
    }
    
    generateBill(id) {
        console.log(id);
        let utilityObjectForInvoice = {};
        utilityObjectForInvoice['utilityId'] = id;
        this.UtilitybillService.generateBillForUtility(utilityObjectForInvoice).subscribe(
            data => {
                alert('The Invoice has been generated');
        }, error => {
            console.log(error);
        });
    }

}