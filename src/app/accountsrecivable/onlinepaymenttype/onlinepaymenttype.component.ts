import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { OnlinepaymenttypeService } from '../service/onlinepaymenttype.service'
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'Onlinepaymenttype',
    templateUrl: 'onlinepaymenttype.component.html'
})

export class OnlinepaymenttypeComponent implements OnInit {
    onlinepaymenttypeList =  [];
    onlinepaymenttypeData = {};
    id: number;

    constructor(
        
        private OnlinepaymenttypeService: OnlinepaymenttypeService,
        private spinner: NgxSpinnerService,

    ) { }

    ngOnInit() { 
        this.spinner.show();        
        this.OnlinepaymenttypeService.getItems().subscribe(
            data => {
                this.spinner.hide();
                this.onlinepaymenttypeList = data['result']['data'];
        }, error => {
            console.log(error);
        });
    }
}