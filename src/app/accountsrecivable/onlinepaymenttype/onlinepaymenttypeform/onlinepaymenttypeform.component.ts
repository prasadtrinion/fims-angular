import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { OnlinepaymenttypeService } from '../../service/onlinepaymenttype.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { DefinationmsService } from '../../../studentfinance/service/definationms.service';

@Component({
    selector: 'OnlinepaymenttypeFormComponent',
    templateUrl: 'onlinepaymenttypeform.component.html'
})

export class OnlinepaymenttypeFormComponent implements OnInit {
    Onlinepaymenttypeform: FormGroup;
    onlinepaymenttypeList = [];
    onlinepaymenttypeData = {};
    ajaxCount: number;
    id: number;
    constructor(
        private OnlinepaymenttypeService: OnlinepaymenttypeService,
        private spinner: NgxSpinnerService,
        private route: ActivatedRoute,
        private router: Router,
        private DefinationmsService: DefinationmsService,

    ) { } 
    ngDoCheck() {
        if (this.ajaxCount == 0) {
            this.spinner.hide();
        }
    }
    ngOnInit() {
        this.ajaxCount = 0
        this.spinner.show();
        this.onlinepaymenttypeData['f018fstatus'] = 1;
        this.Onlinepaymenttypeform = new FormGroup({
            Onlinepaymenttype: new FormControl('', Validators.required),

        });
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.ajaxCount++;
            this.OnlinepaymenttypeService.getItemsDetail(this.id).subscribe(
                data => {
                    this.ajaxCount--;
                    this.onlinepaymenttypeData = data['result'][0];
                    this.onlinepaymenttypeData['f018fstatus'] = parseInt(this.onlinepaymenttypeData['f018fstatus']);

                }, error => {
                    console.log(error);

                });
        }
    }
    addOnlinepaymenttype() {
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        if (this.id > 0) {
            this.OnlinepaymenttypeService.updateOnlinepaymenttypeItems(this.onlinepaymenttypeData, this.id).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert('Payment Type Code Already Exist');
                    }
                    else {
                        this.router.navigate(['accountsrecivable/onlinepaymenttype']);
                        alert("Payment Type Setup has been Updated Successfully");
                    }
                }, error => {
                    console.log(error);
                    alert('Payment Type Code Already Exist');
                });
        } else {
            this.OnlinepaymenttypeService.insertOnlinepaymenttypeItems(this.onlinepaymenttypeData).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert('Payment Type Code Already Exist');
                    }
                    else {
                        this.router.navigate(['accountsrecivable/onlinepaymenttype']);
                        alert("Payment Type Setup has been Added Successfully");
                    }

                }, error => {
                    console.log(error);
                });
        }
    }
    checkDuplication() {
        let duplicationObj = {};
        if (this.id > 0) {
            duplicationObj['id'] = this.id;
        }
        duplicationObj['key'] = 'Payment Type';
        duplicationObj['paymentType'] = this.onlinepaymenttypeData['f018fcode'];
        if(this.onlinepaymenttypeData['f018fcode'] != undefined && this.onlinepaymenttypeData['f018fcode'] != ""){
            this.DefinationmsService.checkDuplication(duplicationObj).subscribe(
                data => {
                    if (parseInt(data['result']) == 1) {
                        alert("Payment Type Already Exist");
                        this.onlinepaymenttypeData['f018fcode'] = '';
                    }
                }, error => {
                    console.log(error);
                });
            }
    }
}