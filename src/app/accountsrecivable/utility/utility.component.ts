import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UtilityService } from '../service/utility.service'
import { faSearch, faEdit } from '@fortawesome/free-solid-svg-icons';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'Utility',
    templateUrl: 'utility.component.html'
})

export class UtilityComponent implements OnInit {
    utilityList = [];
    utilityData = {};
    faSearch = faSearch;
    faEdit = faEdit;
    id: number;

    constructor(
        private UtilityService: UtilityService,
        private spinner: NgxSpinnerService,

    ) { }

    ngOnInit() {
        this.spinner.show();
        this.UtilityService.getItems().subscribe(
            data => {
                this.spinner.hide();
                this.utilityList = data['result']['data'];
            }, error => {
                console.log(error);
            });
    }

    addUtility() {
        console.log(this.utilityData);
        this.UtilityService.insertUtilityItems(this.utilityData).subscribe(
            data => {
                this.utilityList = data['todos'];
            }, error => {
                console.log(error);
            });

    }
}