import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { UtilityService } from '../../service/utility.service'
import { PremiseService } from '../../service/premise.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { FundService } from '../../../generalsetup/service/fund.service'
import { ActivitycodeService } from '../../../generalsetup/service/activitycode.service'
import { AccountcodeService } from '../../../generalsetup/service/accountcode.service'
import { DepartmentService } from '../../../generalsetup/service/department.service'
import { RevenuesetupService } from '../../service/revenuesetup.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { DefinationmsService } from '../../../studentfinance/service/definationms.service';

@Component({
    selector: 'UtilityFormComponent',
    templateUrl: 'utilityform.component.html'
})

export class UtilityFormComponent implements OnInit {
    utilityform: FormGroup;
    utilityList = [];
    utilityData = {};
    premiseList = [];
    premiseData = {};
    customerList = [];
    customerData = [];
    categoriesList = [];
    DeptList = [];
    deptData = [];
    fundList = [];
    fundData = {};
    accountcodeList = [];
    accountcodeData = {};
    activitycodeList = [];
    activitycodeData = {};
    taxcodeList = [];
    revenuesetupData = {};
    revenuesetupList = [];
    invoiceList = [];
    revenuesetupDataAdded = [];
    revenueAddedList = [];
    id: number;
    ajaxCount: number;

    constructor(
        private UtilityService: UtilityService,
        private PremiseService: PremiseService,
        private route: ActivatedRoute,
        private router: Router,
        private ActivitycodeService: ActivitycodeService,
        private AccountcodeService: AccountcodeService,
        private DepartmentService: DepartmentService,
        private FundService: FundService,
        private RevenuesetupService: RevenuesetupService,
        private spinner: NgxSpinnerService,
        private DefinationmsService: DefinationmsService,

    ) { }
    ngDoCheck() {
        const change = this.ajaxCount;
        console.log(this.ajaxCount);
        if (this.ajaxCount == 0) {
            this.editFunction();
            this.spinner.hide();
            this.ajaxCount = 10;
        }
    }
    editFunction() {
        this.ajaxCount++;
        if (this.id > 0) {
            this.UtilityService.getItemsDetail(this.id).subscribe(
                data => {
                    this.ajaxCount--;
                    this.utilityData = data['result'][0];
                    this.utilityData['f078fpremise'] = parseInt(this.utilityData['f078fpremise']);
                    this.utilityData['f078fstatus'] = parseInt(this.utilityData['f078fstatus']);
                    console.log(this.utilityData);
                }, error => {
                    console.log(error);
                });
        }
    }

    ngOnInit() {
        this.ajaxCount = 0
        this.spinner.show();
        this.ajaxCount++;
        this.RevenuesetupService.getActiveItems().subscribe(
            data => {
                this.ajaxCount--;
                this.revenuesetupList = data['result'];
            }, error => {
                console.log(error);
            });

        this.utilityform = new FormGroup({
            utility: new FormControl('', Validators.required),

        });
        this.utilityData['f078fstatus'] = 1;
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        this.ajaxCount++;
        this.FundService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.fundList = data['result']['data'];
            }, error => {
                console.log(error);
            });
        // activity dropdown
        this.ajaxCount++;
        this.ActivitycodeService.getItemsByLevel().subscribe(
            data => {
                this.ajaxCount--;
                this.activitycodeList = data['result']['data'];
            }, error => {
                console.log(error);
            });
        // account dropdown
        this.ajaxCount++;
        this.AccountcodeService.getItemsByLevel().subscribe(
            data => {
                this.ajaxCount--;
                this.accountcodeList = data['result']['data'];
                for (var i = 0; i < this.accountcodeList.length; i++) {
                    if (this.accountcodeList[i]['f059fid'] == '1046') {
                        this.accountcodeList[i]['f059fcompleteCode'] = '00000';
                    }
                }
            }, error => {
                console.log(error);
            });

        // department dropdown
        this.ajaxCount++;
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.DepartmentService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.DeptList = data['result']['data'];
            }, error => {
                console.log(error);
            });

        // premise dropdown 
        this.ajaxCount++;
        this.PremiseService.getActiveItems().subscribe(
            data => {
                this.ajaxCount--;
                this.premiseList = data['result']['data'];
            }, error => {
                console.log(error);
            });

        console.log(this.id);



        console.log(this.id);
    }


    addUtility() {
        if (this.utilityData['f078fdebitFund'] == undefined) {
            alert("Select Debit Fund");
            return false;
        }
        if (this.utilityData['f078fdebitActivity'] == undefined) {
            alert("Select Debit Activity Code");
            return false;
        }
        if (this.utilityData['f078fdebitDepartment'] == undefined) {
            alert("Select Debit Cost Center");
            return false;
        }
        if (this.utilityData['f078fdebitAccount'] == undefined) {
            alert("Select Debit Account Code");
            return false;
        }
        if (this.utilityData['f078fcreditFund'] == undefined) {
            alert("Select Credit Fund");
            return false;
        }
        if (this.utilityData['f078fcreditActivity'] == undefined) {
            alert("Select Credit Activity Code");
            return false;
        }
        if (this.utilityData['f078fcreditDepartment'] == undefined) {
            alert("Select Credit Cost Center");
            return false;
        }
        if (this.utilityData['f078fcreditAccount'] == undefined) {
            alert("Select Account Code");
            return false;
        }

        if (this.utilityData['f078flastReading'] < this.utilityData['f078finitialReading']) {
            alert("Last Meter Reading should greater than Intial Meter Reading");
            return false;
        }
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        if (this.utilityData['f078fdebitAccount'] == this.utilityData['f078fcreditAccount']) {
            alert("Debit GL and Credit GL Account Code Cannot be Same");
            return false;
        }
        // if(parseInt(this.utilityData['f078finitialReading'])>parseInt(this.utilityData['f078flastReading'])) {
        //     alert("Initial Reading cannot be greater than Last Reading");
        //     return false;
        // }
        //if(this.utilityData['f078finitialReading'])
        this.utilityData['f078fidNumber'] = 0;
        //    this.utilityData['f042fupdatedBy']=1;
        //    this.utilityData['f042fcreatedBy']=1;

        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.UtilityService.updateUtilityItems(this.utilityData, this.id).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert("Premise Already Exists");
                        return false;
                    }
                    else {
                        this.router.navigate(['accountsrecivable/utility']);
                        alert("Utility Setup has been Updated Successfully");
                    }
                }, error => {
                    console.log(error);
                    alert("Premise Already Exists");
                });


        } else {
            this.UtilityService.insertUtilityItems(this.utilityData).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert("Premise Already Exists");
                        return false;
                    }
                    else {
                        this.router.navigate(['accountsrecivable/utility']);
                        alert("Utility Setup has been Added Successfully");
                    }
                }, error => {
                    console.log(error);
                });

        }

    }
    onlyNumberKey(event) {
        //console.log(event);
        if (event.charCode == 46) {

        } else {
            return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
        }
    }
    maximize() {
        $("#myModal").addClass("modal");
        $("#overlay1").show();
        $(".maximize-icon").addClass("hidden");
        $(".minimize-icon").removeClass("hidden");
        $(".modal-backdrop").show();

    }
    minimize() {
        $("#myModal").removeClass("modal");
        $("#overlay1").hide();
        $(".maximize-icon").removeClass("hidden");
        $(".minimize-icon").addClass("hidden");
        $(".modal-backdrop").hide();
        $('#myModal').show();

    }
    checkDuplication() {
        let duplicationObj = {};
        if (this.id > 0) {
            duplicationObj['id'] = this.id;
        }
        duplicationObj['key'] = 'Utility';
        duplicationObj['meter'] = this.utilityData['f078fdescription'];
        if(this.utilityData['f078fdescription'] != undefined && this.utilityData['f078fdescription'] != ""){
            this.DefinationmsService.checkDuplication(duplicationObj).subscribe(
                data => {
                    if (parseInt(data['result']) == 1) {
                        alert("Meter ID Already Exist");
                        this.utilityData['f078fdescription'] = '';
                    }
                }, error => {
                    console.log(error);
                });
            }
    }
}