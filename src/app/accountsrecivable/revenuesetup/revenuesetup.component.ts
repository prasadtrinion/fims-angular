import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { RevenuesetupService } from '../service/revenuesetup.service'
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'Revenuesetup',
    templateUrl: 'revenuesetup.component.html'
})

export class RevenuesetupComponent implements OnInit {
    revenuesetupList =  [];
    revenuesetupData = {};
    id: number;

    constructor(
        
        private RevenuesetupService: RevenuesetupService,
        private spinner: NgxSpinnerService

    ) { }

    ngOnInit() { 
        this.spinner.show();                        
        this.RevenuesetupService.getItems().subscribe(
            data => {
                this.spinner.hide();
                this.revenuesetupList = data['result'];
        }, error => {
            console.log(error);
        });
    }
}