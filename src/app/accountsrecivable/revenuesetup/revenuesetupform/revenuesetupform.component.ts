import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { RevenuesetupService } from '../../service/revenuesetup.service';
import { FundService } from '../../../generalsetup/service/fund.service'
import { ActivitycodeService } from '../../../generalsetup/service/activitycode.service'
import { AccountcodeService } from '../../../generalsetup/service/accountcode.service'
import { DepartmentService } from '../../../generalsetup/service/department.service'
import { CategoriesService } from '../../service/categories.service';
import { TaxsetupcodeService } from '../../../generalledger/service/taxsetupcode.service'
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { DefinationmsService } from '../../../studentfinance/service/definationms.service';

@Component({
    selector: 'RevenuesetupFormComponent',
    templateUrl: 'revenuesetupform.component.html'
})

export class RevenuesetupFormComponent implements OnInit {
    revenuesetupform: FormGroup;
    revenuesetupList = [];
    revenuesetupdata = {};
    DeptList = [];
    deptData = [];
    fundList = [];
    fundData = {};
    accountcodeList = [];
    accountcodeData = {};
    activitycodeList = [];
    prefixcodeList = [];
    activitycodeData = {};
    categoriesList = [];
    taxcodeList = [];
    ajaxCount: number;
    editDisabled: boolean;
    id: number;
    constructor(
        private RevenuesetupService: RevenuesetupService,
        private ActivitycodeService: ActivitycodeService,
        private AccountcodeService: AccountcodeService,
        private DepartmentService: DepartmentService,
        private FundService: FundService,
        private CategoriesService: CategoriesService,
        private TaxsetupcodeService: TaxsetupcodeService,
        private spinner: NgxSpinnerService,
        private route: ActivatedRoute,
        private router: Router,
        private DefinationmsService: DefinationmsService,

    ) { }

    ngDoCheck() {
        const change = this.ajaxCount;
        if (this.ajaxCount == 0) {
            this.editFunction();
            this.spinner.hide();
            this.ajaxCount = 10;
        }
    }

    editFunction() {
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.ajaxCount++;
        if (this.id > 0) {
            this.RevenuesetupService.getItemsDetail(this.id).subscribe(
                data => {
                    this.ajaxCount--;
                    this.revenuesetupdata = data['result'][0];
                    this.revenuesetupdata['f085fstatus'] = parseInt(this.revenuesetupdata['f085fstatus']);
                    this.revenuesetupdata['f085fidRevenueCategory'] = parseInt(this.revenuesetupdata['f085fidRevenueCategory']);
                    this.editDisabled = true;
                }, error => {
                    console.log(error);
                });
        }

    }


    ngOnInit() {
        this.ajaxCount = 0
        this.spinner.show();
        this.editDisabled = false;
        this.revenuesetupdata['f085fstatus'] = 1;
        this.ajaxCount = 0;
        this.ajaxCount++;
        //revenue category
        this.CategoriesService.getActiveItems().subscribe(
            data => {
                this.ajaxCount--;
                this.categoriesList = data['result'];
            }, error => {
                console.log(error);
            });
        // fund dropdown
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.FundService.getItems().subscribe(
            data => {
                this.fundList = data['result']['data'];
            }, error => {
                console.log(error);
            });

        // activity dropdown
        this.ActivitycodeService.getItemsByLevel().subscribe(
            data => {
                this.activitycodeList = data['result']['data'];
            }, error => {
                console.log(error);
            });

        // department dropdown
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.DepartmentService.getItems().subscribe(
            data => {
                this.DeptList = data['result']['data'];
            }, error => {
                console.log(error);
            });

        //Taxcode dropdown
        this.ajaxCount++;
        this.TaxsetupcodeService.getActiveItems().subscribe(
            data => {
                this.ajaxCount--;
                this.taxcodeList = data['result']['data'];
            }, error => {
                console.log(error);
            });

        //Accountcode dropdown
        this.ajaxCount++;
        this.AccountcodeService.getItemsByLevel().subscribe(
            data => {
                this.ajaxCount--;
                this.accountcodeList = data['result']['data'];
            }, error => {
                console.log(error);
            });


        this.revenuesetupform = new FormGroup({
            revenuesetup: new FormControl('', Validators.required),

        });
    }
    getPrefixCode() {
        //Prefix Code dropdown
        this.ajaxCount++;
        let itemId = this.revenuesetupdata['f085fidRevenueCategory'];
        this.CategoriesService.getPrefixCode(itemId).subscribe(
            data => {
                this.ajaxCount--;
                this.prefixcodeList = data['result'];
                this.revenuesetupdata['f085fidPreficCode'] = data['result']['f073fcode']
            }, error => {
                console.log(error);
            });
    }
    addRevenuesetup() {
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        if (this.id > 0) {
            this.RevenuesetupService.updateRevenuesetupItems(this.revenuesetupdata, this.id).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert('Revenue Setup Code Already Exist');
                    }
                    else {
                        this.router.navigate(['accountsrecivable/revenuesetup']);
                        alert("Revenue Setup has been Updated Successfully");
                    }
                }, error => {
                    console.log(error);
                    alert('Revenue Setup Code Already Exist');
                });


        } else {
            this.RevenuesetupService.insertRevenuesetupItems(this.revenuesetupdata).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert('Revenue Setup Code Already Exist');
                    }
                    else {
                        this.router.navigate(['accountsrecivable/revenuesetup']);
                        alert("Revenue Setup has been Added Successfully");
                    }

                }, error => {
                    console.log(error);
                });

        }

    }
    checkDuplication() {
        let duplicationObj = {};
        if (this.id > 0) {
            duplicationObj['id'] = this.id;
        }
        duplicationObj['key'] = 'Revenue Setup';
        duplicationObj['revenueSetup'] = this.revenuesetupdata['f085fname'];
        duplicationObj['revenueCategory'] = this.revenuesetupdata['f085fidRevenueCategory'];
        if(this.revenuesetupdata['f085fidRevenueCategory'] != undefined && this.revenuesetupdata['f085fidRevenueCategory'] != "" && this.revenuesetupdata['f085fname'] != undefined && this.revenuesetupdata['f085fname'] != ""){

            this.DefinationmsService.checkDuplication(duplicationObj).subscribe(
                data => {
                    if (parseInt(data['result']) == 1) {
                        alert("Revenue Category Already Exist");
                        this.revenuesetupdata['f085fname'] = '';
                    }
                }, error => {
                    console.log(error);
                });
            }
        
    }
}