import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { DiscountService } from '../../../studentfinance/service/discount.service'
import { FeeitemService } from '../../../studentfinance/service/feeitem.service'
// import { TaxsetupcodeService } from '../../../generalledger/service/taxsetupcode.service'
import { RevenuesetupService } from '../../service/revenuesetup.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CategoriesService } from '../../service/categories.service';
import { CustomerService } from '../../../accountsrecivable/service/customer.service';
import { StudentService } from '../../../studentfinance/service/student.service';
import { StaffService } from "../../service/staff.service";
import { FundService } from '../../../generalsetup/service/fund.service';
import { ActivitycodeService } from '../../../generalsetup/service/activitycode.service';
import { AccountcodeService } from '../../../generalsetup/service/accountcode.service';
import { DepartmentService } from '../../../generalsetup/service/department.service'
import { NgxSpinnerService } from 'ngx-spinner';
import { DefinationmsService } from '../../../studentfinance/service/definationms.service';

@Component({
    selector: 'StudentdiscountFormComponent',
    templateUrl: 'studentdiscountform.component.html'
})

export class StudentdiscountFormComponent implements OnInit {
    discountform: FormGroup;
    discountList = [];
    discountData = {};
    feeitemList = [];
    fdiscount = [];
    invoicetype = [];
    studentList = [];
    customerList = [];
    famount = [];
    smartStaffList = [];
    datetype = [];
    categoriesList = [];
    codeList = [];
    DeptList = [];
    fundList = [];
    feeItemList = [];
    accountcodeList = [];
    activitycodeList = [];
    id: number;
    type: string;
    valueDate: string;
    ajaxCount: number;
    constructor(
        private DiscountService: DiscountService,
        private FeeitemService: FeeitemService,
        private CategoriesService: CategoriesService,
        private RevenuesetupService: RevenuesetupService,
        private CustomerService: CustomerService,
        private StudentService: StudentService,
        private StaffService: StaffService,
        private FundService: FundService,
        private ActivitycodeService: ActivitycodeService,
        private AccountcodeService: AccountcodeService,
        private DepartmentService: DepartmentService,
        private spinner: NgxSpinnerService,
        private route: ActivatedRoute,
        private router: Router,
        private DefinationmsService: DefinationmsService,


    ) { }
    ngDoCheck() {
        if (this.ajaxCount == 0) {
            this.spinner.hide();
        }
    }
    ngOnInit() {
        this.ajaxCount = 0;
        this.spinner.show();
        this.discountData['f046fdiscountRateType'] = 1;
        this.discountData['f046fstatus'] = 1;

        let discounttypeobj = {};
        discounttypeobj['id'] = '1';
        discounttypeobj['name'] = 'Auto';
        this.fdiscount.push(discounttypeobj);
        discounttypeobj = {};
        discounttypeobj['id'] = '2';
        discounttypeobj['name'] = 'Manual';
        this.fdiscount.push(discounttypeobj);

        let invoicetypeobj = {};
        invoicetypeobj['name'] = 'Other Receivables';
        invoicetypeobj['type'] = 'OR';
        this.invoicetype.push(invoicetypeobj);

        this.discountData['f046fcustomerType'] = invoicetypeobj['type'];


        let datetypeobj = {};
        datetypeobj['name'] = '2019-January';
        datetypeobj['id'] = '2019-01';
        this.datetype.push(datetypeobj);
        datetypeobj = {};
        datetypeobj['name'] = '2019-February';
        datetypeobj['id'] = '2019-02';

        this.datetype.push(datetypeobj);
        // datetypeobj = {};
        // datetypeobj['name'] = '2019-March';
        // this.datetype.push(datetypeobj);
        // datetypeobj = {};
        // datetypeobj['name'] = '2019-April';
        // this.datetype.push(datetypeobj);
        // datetypeobj = {};
        // datetypeobj['name'] = '2019-May';
        // this.datetype.push(datetypeobj);
        // datetypeobj = {};
        // datetypeobj['name'] = '2019-June';
        // this.datetype.push(datetypeobj);
        // datetypeobj = {};
        // datetypeobj['name'] = '2019-July';
        // this.datetype.push(datetypeobj);
        // datetypeobj = {};
        // datetypeobj['name'] = '2019-August';
        // this.datetype.push(datetypeobj);
        // datetypeobj = {};
        // datetypeobj['name'] = '2019-September';
        // this.datetype.push(datetypeobj);
        // datetypeobj = {};
        // datetypeobj['name'] = '2019-October';
        // this.datetype.push(datetypeobj);
        // datetypeobj = {};
        // datetypeobj['name'] = '2019-November';
        // this.datetype.push(datetypeobj);
        // datetypeobj = {};
        // datetypeobj['name'] = '2019-December';
        // this.datetype.push(datetypeobj);

        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        // this.discountData['f046fdiscountGenerationDate'] = new Date().toISOString().substr(0, 10);
        // this.discountData['f046foutstandingAs'] = new Date().toISOString().substr(0, 10);

        // var m_names = ['January', 'February', 'March',
        //     'April', 'May', 'June', 'July',
        //     'August', 'September', 'October', 'November', 'December'];
        // var d = new Date();
        // var n = m_names[d.getMonth()+d.getMonth()];
        // this.discountData['f046foutstandingAs'] = n;
        // console.log(this.valueDate);

        // var date = new Date();
        // var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
        // this.discountData['f046fdiscountGenDate'] = lastDay;

        // var lastDayWithSlashes = (lastDay.getDate()) + '/' + (lastDay.getMonth() + 1) + '/' + lastDay.getFullYear();

        //Student dropdown
        this.ajaxCount++;
        this.StudentService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                console.log(this.ajaxCount);

                this.studentList = data['result']['data'];
            }
        );

        //Customer dropdown
        this.ajaxCount++;
        this.CustomerService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.customerList = data['result']['data'];
            }
        );
        //  staff dropdown
        this.ajaxCount++;
        this.StaffService.getItems().subscribe(
            data => {
                console.log(data);
                this.ajaxCount--;
                this.smartStaffList = data['result']['data'];

                console.log(this.ajaxCount);
            }
        );
        //fee dropdown
        this.FeeitemService.getItems().subscribe(
            data => {
                this.feeitemList = data['result']['data'];
            }, error => {
                console.log(error);
            });
        //revenue category
        this.CategoriesService.getActiveItems().subscribe(
            data => {
                this.ajaxCount--;
                this.categoriesList = data['result'];
            }, error => {
                console.log(error);
            });
        // fund dropdown
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.FundService.getItems().subscribe(
            data => {
                this.fundList = data['result']['data'];
            }, error => {
                console.log(error);
            });

        // activity dropdown
        this.ActivitycodeService.getItemsByLevel().subscribe(
            data => {
                this.activitycodeList = data['result']['data'];
            }, error => {
                console.log(error);
            });
        // account dropdown
        this.AccountcodeService.getItemsByLevel().subscribe(
            data => {
                this.accountcodeList = data['result']['data'];
            }, error => {
                console.log(error);
            });

        // department dropdown
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.DepartmentService.getItems().subscribe(
            data => {
                this.DeptList = data['result']['data'];
            }, error => {
                console.log(error);
            });
        this.discountform = new FormGroup({
            discount: new FormControl('', Validators.required)
        });
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));


        console.log(this.id);

        if (this.id > 0) {
            this.ajaxCount++;
            this.DiscountService.getItemsDetail(this.id).subscribe(
                data => {
                    this.ajaxCount--;
                    this.discountData = data['result'][0];
                    // converting from string to int for radio button default selection
                    this.discountData['f046fstatus'] = parseInt(data['result'][0]['f046fstatus']);
                    // this.discountData['f046fdiscountGenerationDate'] = parseInt(data['result'][0]['f046fdiscountGenerationDate']);
                    this.discountData['f046fdiscountRateType'] = parseInt(data['result'][0]['f046fdiscountRateType']);

                    this.discountData['f046foutstandingAs'] = data['result'][0]['f046foutstandingAs'];
                    this.discountData['f046fidRevenueCode'] = parseInt(data['result'][0]['f046fidRevenueCode']);
                    this.getPrefixCode();
                    this.validateDate();
                    console.log(this.discountData);
                }, error => {
                    console.log(error);
                });
        }


        console.log(this.id);
    }

    validateDate() {
        var  yearmonth = this.discountData['f046foutstandingAs'];
        var month = yearmonth.split('-');
        console.log(month);

        // console
        var newdate = new Date(this.discountData['f046fdiscountGenerationDate']);
       
        var dd = newdate.getDate();
        console.log(dd);
        console.log(newdate.getMonth());
        var mm = newdate.getMonth()+1;
        console.log(mm);
        var y = newdate.getFullYear();

        if(parseInt(month[0])>y) {
            alert('Please provide the proper Discount Generation Date');
            this.discountData['f046fdiscountGenerationDate']='';
        }

        if(parseInt(month[0])==y) {
            if(parseInt(month[1])<mm) {

            } else {
                alert('Please provide the proper Discount Generation Date');
                this.discountData['f046fdiscountGenerationDate']='';

            }
        }

    }
    getPrefixCode() {
        //Prefix Code dropdown
        this.ajaxCount++;
        let itemId = this.discountData['f046fidRevenueCategory'];
        this.RevenuesetupService.getRevenueByCategoryItems(itemId).subscribe(
            data => {
                this.ajaxCount--;
                this.codeList = data['result'];
            }, error => {
                console.log(error);
            });
    }
    getType() {
        let typeObject = {};
        typeObject['type'] = this.discountData['f046fcustomerType'];
        this.type = this.discountData['f046fcustomerType'];
        return this.type;
    }

    ConvertToFloat(val) {
        return parseFloat(val);
    }
    addDiscount() {
        console.log(this.discountData);
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        // this.discountData['f046famount'] = this.ConvertToFloat(this.discountData['f046famount']).toFixed(2);


        if (this.id > 0) {
            this.DiscountService.updatediscountItems(this.discountData, this.id).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert('Code Already Exist');
                    }
                    else {
                        this.router.navigate(['accountsrecivable/studentdiscount']);
                        alert("Discount has been Updated Successfully");
                    }
                }, error => {
                    alert('Code Already Exist');
                    console.log(error);
                });

        } else {
            this.DiscountService.insertdiscountItems(this.discountData).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert('Code Already Exist');
                    }
                    else {
                        this.router.navigate(['accountsrecivable/studentdiscount']);
                        alert("Discount has been Added Successfully");
                    }
                }, error => {
                    console.log(error);
                });

        }

    }
    onlyNumberKey(event) {
        //console.log(event);
        if (event.charCode == 46) {

        } else {
            return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
        }
    }
    checkDuplication() {
        let duplicationObj = {};
        if (this.id > 0) {
            duplicationObj['id'] = this.id;
        }
        duplicationObj['key'] = 'Student Discount';
        duplicationObj['discountType'] = this.discountData['f046fdiscountType'];
        if(this.discountData['f046fdiscountType'] != undefined && this.discountData['f046fdiscountType'] != ""){
            this.DefinationmsService.checkDuplication(duplicationObj).subscribe(
                data => {
                    if (parseInt(data['result']) == 1) {
                        alert("Discount Type Already Exist");
                        this.discountData['f046fdiscountType'] = '';
                    }
                }, error => {
                    console.log(error);
                });
            }
    }
}