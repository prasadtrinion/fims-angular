import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DiscountService } from '../../studentfinance/service/discount.service'
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'StudentdiscountComponent',
    templateUrl: 'studentdiscount.component.html'
})

export class StudentdiscountComponent implements OnInit {
    discountList =  [];
    discountData = {};
    id: number;

    constructor(
        
        private DiscountService: DiscountService,
        private spinner: NgxSpinnerService,

    ) { }

    ngOnInit() { 
        this.spinner.show();                                
        this.DiscountService.getItems().subscribe(
            data => {
                this.spinner.hide();
                // this.discountList = data['result']['data'];
                let activityData = [];
                    activityData = data['result'];
                    for (var i = 0; i < activityData.length; i++) {
                        if (activityData[i]['f046fcustomerType'] == 'STA') {
                            activityData[i]['f046fcustomerType'] = 'Staff';

                        } else if (activityData[i]['f046fcustomerType'] == 'STD') {
                            activityData[i]['f046fcustomerType'] = 'Student';
                        }
                        else {
                            if (activityData[i]['f046fcustomerType'] == 'OR') {
                                activityData[i]['f046fcustomerType'] = 'Other Receivables';
                            }
                        }
                    }
                    this.discountList = activityData;
        }, error => {
            console.log(error);
        });
    }

    addDiscount(){
           console.log(this.discountData);
        this.DiscountService.insertdiscountItems(this.discountData).subscribe(
            data => {
                this.discountList = data['todos'];
            }, error => {
                console.log(error);
            });

    }
}