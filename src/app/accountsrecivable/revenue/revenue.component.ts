import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { RevenueService } from '../service/revenue.service'
import { faSearch, faEdit } from '@fortawesome/free-solid-svg-icons';
import { NgxSpinnerService } from 'ngx-spinner';


@Component({
    selector: 'RevenueComponent',
    templateUrl: 'revenue.component.html'
})

export class RevenueComponent implements OnInit {
    revenueList =  [];
    revenueData = {};
    faSearch = faSearch;
    faEdit = faEdit;
    
    id: number;

    constructor(
        
        private RevenueService: RevenueService,
        private spinner: NgxSpinnerService,

    ) { }
    ngOnInit() { 
        this.spinner.show();                
        this.RevenueService.getItems().subscribe(
            data => {
                this.spinner.hide();
                this.revenueList = data['result'];
        }, error => {
            console.log(error);
        });
    }
}