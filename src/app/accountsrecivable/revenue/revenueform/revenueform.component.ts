import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { RevenueService } from '../../service/revenue.service'
import { ActivatedRoute, Router } from '@angular/router';
import { FundService } from '../../../generalsetup/service/fund.service';
import { ActivitycodeService } from '../../../generalsetup/service/activitycode.service';
import { AccountcodeService } from '../../../generalsetup/service/accountcode.service';
import { DepartmentService } from '../../../generalsetup/service/department.service';
import { FeeitemService } from '../../../studentfinance/service/feeitem.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'RevenueFormComponent',
    templateUrl: 'revenueform.component.html'
})

export class RevenueFormComponent implements OnInit {
    revenueList = [];
    revenueData = {};
    DeptList = [];
    deptData = [];
    fundList = [];
    fundData = {};
    feeItemList = [];
    accountcodeList = [];
    accountcodeData = {};
    activitycodeList = [];
    activitycodeData = {};
    id: number;
    ajaxCount: number;
    constructor(
        private RevenueService: RevenueService,
        private ActivitycodeService: ActivitycodeService,
        private AccountcodeService: AccountcodeService,
        private DepartmentService: DepartmentService,
        private FundService: FundService,
        private route: ActivatedRoute,
        private FeeitemService: FeeitemService,
        private spinner: NgxSpinnerService,
        private router: Router
    ) { }
    ngDoCheck() {
        if (this.ajaxCount == 0) {
            this.spinner.hide();
        }
    }
    ngOnInit() {
        this.ajaxCount = 0
        this.spinner.show();
        this.revenueData['f085fstatus'] = 1;

        // fund dropdown
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.FundService.getItems().subscribe(
            data => {
                this.fundList = data['result']['data'];
            }, error => {
                console.log(error);
            });

        // Fee Item dropdown
        this.FeeitemService.getItems().subscribe(
            data => {
                this.feeItemList = data['result']['data'];
            }, error => {
                console.log(error);
            });
        // activity dropdown
        this.ActivitycodeService.getItemsByLevel().subscribe(
            data => {
                this.activitycodeList = data['result']['data'];
            }, error => {
                console.log(error);
            });
        // account dropdown
        this.AccountcodeService.getItemsByLevel().subscribe(
            data => {
                this.accountcodeList = data['result']['data'];
            }, error => {
                console.log(error);
            });

        // department dropdown
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.DepartmentService.getItems().subscribe(
            data => {
                this.DeptList = data['result']['data'];
            }, error => {
                console.log(error);
            });

        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.ajaxCount++;
            this.RevenueService.getItemsDetail(this.id).subscribe(
                data => {
                    this.ajaxCount--;
                    this.revenueData = data['result'][0];
                    this.revenueData['f085fstatus'] = parseInt(this.revenueData['f085fstatus']);
                }, error => {
                    console.log(error);
                });
        }
    }

    addRevenueDetails() {
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.RevenueService.updateRevenueItems(this.revenueData, this.id).subscribe(
                data => {
                    this.router.navigate(['accountsrecivable/revenue']);
                    alert("Revenue has been Updated Sucessfully !!");
                }, error => {
                    console.log(error);
                });


        } else {
            this.RevenueService.insertRevenueItems(this.revenueData).subscribe(
                data => {
                    this.router.navigate(['accountsrecivable/revenue']);
                    alert("Revenue has been Added Sucessfully !!");


                }, error => {
                    console.log(error);
                });

        }

    }
}