import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CategoriesService } from '../service/categories.service'
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'Categories',
    templateUrl: 'categories.component.html'
})

export class CategoriesComponent implements OnInit {
    categoriesList =  [];
    categoriesData = {};
    id: number;

    constructor(
        
        private CategoriesService: CategoriesService,
        private spinner: NgxSpinnerService

    ) { }
    ngOnInit() { 
        this.spinner.show();                
        this.CategoriesService.getItems().subscribe(
            data => {
                this.spinner.hide();
                this.categoriesList = data['result']['data'];
        }, error => {
            console.log(error);
        });
    }
}