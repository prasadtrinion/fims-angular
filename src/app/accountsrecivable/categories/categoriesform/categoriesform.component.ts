import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { CategoriesService } from '../../service/categories.service'
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { TaxsetupcodeService } from '../../../generalledger/service/taxsetupcode.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { AccountcodeService } from '../../../generalsetup/service/accountcode.service';
import { DefinationmsService } from '../../../studentfinance/service/definationms.service';

@Component({
    selector: 'CategoriesFormComponent',
    templateUrl: 'categoriesform.component.html'
})

export class CategoriesFormComponent implements OnInit {
    categoriesform: FormGroup;
    categoriesList = [];
    categoriesData = {};
    taxcodeList = [];
    accountcodeList = [];
    id: number;
    ajaxCount: number;
    constructor(
        private CategoriesService: CategoriesService,
        private TaxsetupcodeService: TaxsetupcodeService,
        private spinner: NgxSpinnerService,
        private AccountcodeService: AccountcodeService,
        private route: ActivatedRoute,
        private router: Router,
        private DefinationmsService: DefinationmsService,

    ) { }
    ngDoCheck() {
        if (this.ajaxCount == 0) {
            this.spinner.hide();
        }
    }
    ngOnInit() {
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.ajaxCount = 0
        this.spinner.show();
        this.categoriesform = new FormGroup({
            categories: new FormControl('', Validators.required),
        });

        //TaxCode Service
        this.categoriesData['f073fstatus'] = 1;
        this.ajaxCount++;
        this.TaxsetupcodeService.getActiveItems().subscribe(
            data => {
                this.ajaxCount--;
                this.taxcodeList = data['result']['data'];
            }, error => {
                console.log(error);
            });

        //Account dropdown
        this.ajaxCount++;
        this.AccountcodeService.getItemsByLevel().subscribe(
            data => {
                this.ajaxCount--;
                this.accountcodeList = data['result']['data'];
            }
        );

        if (this.id > 0) {
            this.ajaxCount++;
            this.CategoriesService.getItemsDetail(this.id).subscribe(
                data => {
                    this.ajaxCount--;
                    this.categoriesData = data['result'];
                    this.categoriesData['f073ftaxCode'] = parseInt(this.categoriesData['f073ftaxCode']);
                    this.categoriesData['f073fstatus'] = parseInt(this.categoriesData['f073fstatus']);
                }, error => {
                    console.log(error);
                });
        }
    }
    addCategories() {
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.CategoriesService.updateCategoriesItems(this.categoriesData, this.id).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert("Prefix Code already exist");
                    } else {
                        this.router.navigate(['accountsrecivable/categories']);
                        alert("Revenue Category has been Updated Successfully");
                    }
                }, error => {
                    console.log(error);
                    alert(' Prefix Code Already Exist');

                });
        } else {
            this.CategoriesService.insertCategoriesItems(this.categoriesData).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert("Prefix Code already exist");
                    } else {
                        this.router.navigate(['accountsrecivable/categories']);
                        alert("Revenue Category has been Added Successfully");
                    }
                }, error => {
                    console.log(error);
                    alert("Prefix Code already exist");
                });
        }
    }
    onlyNumberKey(event) {
        console.log(event);
        return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
    }
    checkDuplication() {
        let duplicationObj = {};
        if (this.id > 0) {
            duplicationObj['id'] = this.id;
        }
        duplicationObj['key'] = 'Revenue Category';
        duplicationObj['revenueCategory'] = this.categoriesData['f073fcategoryName'];
        if(this.categoriesData['f073fcategoryName'] != undefined && this.categoriesData['f073fcategoryName'] != ""){

            this.DefinationmsService.checkDuplication(duplicationObj).subscribe(
                data => {
                    if (parseInt(data['result']) == 1) {
                        alert("Revenue Category Already Exist");
                        this.categoriesData['f073fcategoryName'] = '';
                    }
                }, error => {
                    console.log(error);
                });
            }
        
    }
    checkDuplication1() {
        let duplicationObj = {};
        if (this.id > 0) {
            duplicationObj['id'] = this.id;
        }
        duplicationObj['key'] = 'Revenue Prefix';
        duplicationObj['revenuePrefix'] = this.categoriesData['f073fcode'];
        if(this.categoriesData['f073fcode'] != undefined && this.categoriesData['f073fcode'] != ""){
            this.DefinationmsService.checkDuplication(duplicationObj).subscribe(
                data => {
                    if (parseInt(data['result']) == 1) {
                        alert("Prefix Code Already Exist");
                        this.categoriesData['f073fcode'] = '';
                    }
                }, error => {
                    console.log(error);
                });
            }
    }
}