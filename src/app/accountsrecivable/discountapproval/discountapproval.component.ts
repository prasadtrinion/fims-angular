import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Injector } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { faSearch, faEdit } from '@fortawesome/free-solid-svg-icons';
import { DiscountService } from '../service/discount.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'DiscountapprovalComponent',
    templateUrl: 'discountapproval.component.html'
})

export class DiscountapprovalComponent implements OnInit {
    creditnoteApprovalList = [];
    creditnoteApprovalData = {};
    discountList = [];
    approveDarrayList = [];
    invoiceapprovalData = [];
    id: number;
    selectAllCheckbox: false;
    type: string;
    title = '';
    faSearch = faSearch;
    faEdit = faEdit;
    constructor(
        private route: ActivatedRoute,
        private DiscountService: DiscountService,
        private spinner: NgxSpinnerService,
        private router: Router
    ) { }
    ngOnInit() {
        this.getList();
    }
    getList() {
        this.spinner.show();
        this.DiscountService.getItems(0).subscribe(
            data => {
                this.spinner.hide();
                this.discountList = data['result'];
                for (var i = 0; i < this.discountList.length; i++) {
                    this.discountList['f024fapprovalStatus'] = false;
                }
            }, error => {
                console.log(error);
            });
    }
    ApprovalListData() {
        console.log(this.discountList);
        this.approveDarrayList = [];
        for (var i = 0; i < this.discountList.length; i++) {
            if (this.discountList[i].f024fapprovalStatus == true) {
                this.approveDarrayList.push(this.discountList[i].f024fid);
            }
        }
        if (this.approveDarrayList.length < 1) {
            alert("Select atleast one Discount Note to Approve");
            return false;
        }
        var confirmPop = confirm("Do you want to Approve?");
        if (confirmPop == false) {
            return false;
        }
        var approvalObject = {};
        approvalObject['id'] = this.approveDarrayList;
        approvalObject['status'] = '1';
        approvalObject['reason'] = '';
        this.DiscountService.updateApproval(approvalObject).subscribe(
            data => {
                this.getList();
                this.invoiceapprovalData['reason'] = '';
                alert("Approved Successfully");
            }, error => {
                console.log(error);
            });

    }
    regectApprovalListData() {
        this.approveDarrayList = [];
        for (var i = 0; i < this.discountList.length; i++) {
            if (this.discountList[i].f024fapprovalStatus == true) {
                this.approveDarrayList.push(this.discountList[i].f024fid);
            }
        }
        if (this.approveDarrayList.length < 1) {
            alert("Select atleast one Discount Note to Reject");
            return false;
        }
      
        if (this.invoiceapprovalData['reason'] == undefined) {
            alert('Enter the Description');
            return false;
        }
        var confirmPop = confirm("Do you want to Reject?");
        if (confirmPop == false) {
            return false;
        }
        var rejectObject = {}
        rejectObject['id'] = this.approveDarrayList;
        rejectObject['status'] = '2';
        rejectObject['reason'] = this.invoiceapprovalData['reason'];
        this.DiscountService.updateApproval(rejectObject).subscribe(
            data => {
                this.getList();
                this.invoiceapprovalData['reason'] = '';
                alert("Rejected Successfully");
            }, error => {
                console.log(error);
            });
    }
    selectAll() {
        console.log(this.selectAllCheckbox);
        for (var i = 0; i < this.creditnoteApprovalList.length; i++) {
            this.creditnoteApprovalList[i].f024fapprovalStatus = this.selectAllCheckbox;
        }
    }
}