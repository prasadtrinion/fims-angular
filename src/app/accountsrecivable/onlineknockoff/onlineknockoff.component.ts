import {Injector} from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { TempbankfileapprovalService } from '../service/tempbankfileapproval.service'
import { AlertService } from './../../_services/alert.service';
@Component({
    selector: 'OnlineKnockOff',
    templateUrl: 'onlineknockoff.component.html'
})

export class OnlineKnockOffComponent implements OnInit {
   
    tempbankapproveList =  [];
    tempbankapproveData = {};
    selectAllCheckbox = true;

    constructor(
        
        private TempbankfileapprovalService: TempbankfileapprovalService,
        private AlertService: AlertService
    ) { }

    ngOnInit() {    
       this.getListData();
    }
    getListData(){
        this.TempbankfileapprovalService.getItemsDetail1().subscribe(
            data => {
                this.tempbankapproveList = data['result']['data'];
                console.log(this.tempbankapproveList);
        }, error => {
            console.log(error);
        });
    }
    tempbankapproveListData() {
        console.log(this.tempbankapproveList);
        var approvaloneIds = [];
        for (var i = 0; i < this.tempbankapproveList.length; i++) {
            if(this.tempbankapproveList[i].f078fapprovedStatus==true) {
                approvaloneIds.push(this.tempbankapproveList[i]);
            }
        }
        var approvaloneUpdate = {};
        approvaloneUpdate['id'] = approvaloneIds;

        this.TempbankfileapprovalService.knockoff(approvaloneUpdate).subscribe(
            data => {
                this.AlertService.success(" Updated Sucessfully ! ");

                this.getListData();
        }, error => {
            console.log(error);
        });
    }
    selectAll() {
        console.log("asdf");
        console.log(this.selectAllCheckbox);
        for (var i = 0; i < this.tempbankapproveList.length; i++) {
                this.tempbankapproveList[i].f017fapprovedStatus = this.selectAllCheckbox;
        }
        console.log(this.tempbankapproveList);
      }

    }
