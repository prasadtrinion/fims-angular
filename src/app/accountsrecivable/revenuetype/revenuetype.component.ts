import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { RevenueTypeService } from "../service/revenuetype.service";

@Component({
    selector: 'Revenuetype',
    templateUrl: 'revenuetype.component.html'
})

export class RevenuetypeComponent implements OnInit {
    revenuetypeList =  [];
    revenuetypeData = {};
    id: number;

    constructor(
        private RevenueTypeService: RevenueTypeService,
        private spinner: NgxSpinnerService,        

    ) { }

    ngOnInit() { 
        this.spinner.show();                                                        
        this.RevenueTypeService.getItems().subscribe(
            data => {
                this.spinner.hide();
                this.revenuetypeList = data['result'];
        }, error => {
            console.log(error);
        });
    }

}