import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RevenueTypeService } from "../../service/revenuetype.service";
import { CategoriesService } from "../../service/categories.service";
import { NgxSpinnerService } from 'ngx-spinner';
import { DefinationmsService } from '../../../studentfinance/service/definationms.service';

@Component({
    selector: 'RevenueTypeFormComponent',
    templateUrl: 'revenuetypeform.component.html'
})

export class RevenueTypeFormComponent implements OnInit {
    revenuetypeList = [];
    revenuetypeData = {};
    categoryList = [];
    id: number;
    ajaxCount: number;
    constructor(
        private RevenueTypeService: RevenueTypeService,
        private CategoriesService: CategoriesService,
        private spinner: NgxSpinnerService,
        private route: ActivatedRoute,
        private router: Router,
        private DefinationmsService: DefinationmsService,

    ) { }
    ngDoCheck() {
        const change = this.ajaxCount;
        if (this.ajaxCount == 0) {
            this.spinner.hide();
        }
    }
    ngOnInit() {
        this.ajaxCount = 0;
        this.spinner.show();
        this.revenuetypeData['f132fstatus'] = 1;
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        //Revenue Category
        this.CategoriesService.getActiveItems().subscribe(
            data => {
                this.categoryList = data['result'];
            }, error => {
                console.log(error);
            }
        )

        if (this.id > 0) {
            this.ajaxCount++;
            this.RevenueTypeService.getItemsDetail(this.id).subscribe(
                data => {
                    this.ajaxCount--;
                    this.revenuetypeData = data['result'][0];
                    this.revenuetypeData['f132fstatus'] = parseInt(data['result'][0]['f132fstatus']);
                }, error => {
                    console.log(error);
                });
        }


        console.log(this.id);
    }

    addCurrencyratesetup() {
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.RevenueTypeService.updaterevenuetypeItems(this.revenuetypeData, this.id).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert('Revenue Name Already Exist');
                    }
                    else {
                        this.router.navigate(['accountsrecivable/revenuetype']);
                        alert("Revenue Type has been Updated Sucessfully !!");
                    }

                }, error => {
                    console.log(error);
                    alert('Revenue Name Already Exist');
                });


        } else {
            this.RevenueTypeService.insertrevenuetypeItems(this.revenuetypeData).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert('Revenue Name Already Exist');
                    }
                    else {
                        this.router.navigate(['accountsrecivable/revenuetype']);
                        alert("Revenue Type has been Added Sucessfully !!");
                    }
                }, error => {
                    console.log(error);
                });

        }

    }
    onlyNumberKey(event) {
        console.log(event);
        return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
    }
    checkDuplication() {
        let duplicationObj = {};
        if (this.id > 0) {
            duplicationObj['id'] = this.id;
        }
        duplicationObj['key'] = 'Revenue Code';
        duplicationObj['revenueCode'] = this.revenuetypeData['f132ftypeCode'];
        if(this.revenuetypeData['f132ftypeCode'] != undefined && this.revenuetypeData['f132ftypeCode'] != ""){
            this.DefinationmsService.checkDuplication(duplicationObj).subscribe(
                data => {
                    if (parseInt(data['result']) == 1) {
                        alert("Revenue Type Code Already Exist");
                        this.revenuetypeData['f132ftypeCode'] = '';
                    }
                }, error => {
                    console.log(error);
                });
            }
    }
    checkDuplication1() {
        let duplicationObj = {};
        if (this.id > 0) {
            duplicationObj['id'] = this.id;
        }
        duplicationObj['key'] = 'Revenue Type';
        duplicationObj['revenueType'] = this.revenuetypeData['f132frevenueName'];
        if(this.revenuetypeData['f132frevenueName'] != undefined && this.revenuetypeData['f132frevenueName'] != ""){
            this.DefinationmsService.checkDuplication(duplicationObj).subscribe(
                data => {
                    if (parseInt(data['result']) == 1) {
                        alert("Revenue Name Already Exist");
                        this.revenuetypeData['f132frevenueName'] = '';
                    }
                }, error => {
                    console.log(error);
                });
            }
    }
}