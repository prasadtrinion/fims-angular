import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Injector } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AdvancepaymentService } from '../service/advancepayment.service'
import { StudentService } from '../../studentfinance/service/student.service';
import { CustomerService } from '../../accountsrecivable/service/customer.service';
import { faSearch, faEdit, faThumbsDown } from '@fortawesome/free-solid-svg-icons';
import { CreditnoteService } from '../service/creditnote.service';

@Component({
    selector: 'AdvancepaymentComponent',
    templateUrl: 'advancepayment.component.html'
})

export class AdvancepaymentComponent implements OnInit {
    advancePaymentList = [];
    creditnoteApprovalData = {};
    advancePaymentData = {};
    studentList = [];
    maininvoiceList = [];
    customerList = [];
    advancePaymentListNew = [];
    invoiceList = [];
    paymenttype = [];
    id: number;
    selectAllCheckbox: false;
    type: string;
    title = '';
    faSearch = faSearch;
    faEdit = faEdit;
    idview: number;
    UserId: number;
    StudentId:number;
    ajaxCount: number;
    showFlag: boolean;
    constructor(
        private AdvancepaymentService: AdvancepaymentService,
        private StudentService: StudentService,
        private CustomerService: CustomerService,
        private CreditnoteService: CreditnoteService,
        private route: ActivatedRoute,
        private router: Router
    ) { }
    ngOnInit() {

        //Student dropdown
        this.ajaxCount++;
        this.StudentService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                console.log(this.ajaxCount);

                this.studentList = data['result']['data'];
            }
        );
        //Customer dropdown
        this.ajaxCount++;
        this.CustomerService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.customerList = data['result']['data'];
            }
        );
        let stafftypeobj = {};
        stafftypeobj['name'] = 'Student';
        stafftypeobj['type'] = 'KSTD';
        this.paymenttype.push(stafftypeobj);
        stafftypeobj = {};
        stafftypeobj['name'] = 'Customer';
        stafftypeobj['type'] = 'KCUS';
        this.paymenttype.push(stafftypeobj);


        this.showFlag = false;
        this.getList();
        this.route.paramMap.subscribe((data) => this.idview = + data.get('idview'));
        if (this.idview > 0) {
            this.showFlag = true;
        }

    }
    getType() {
        let typeObject = {};
        typeObject['type'] = this.advancePaymentData['f031ftype'];
        this.type = this.advancePaymentData['f031ftype']
        console.log(typeObject);
        return this.type;
    }
    getInvoiceList() {
        this.UserId = this.advancePaymentData['f031fidCustomer'];
        // this.StudentId = this.advancePaymentData['f031fidStudent'];
        let invoiceObj = {};
        switch (this.type) {
            case 'KSTD':
                invoiceObj['type'] = "STD";
                break;
            case 'KCUS':
                invoiceObj['type'] = "OR";
                break;
        }
        invoiceObj['UserId'] = this.UserId;
        // invoiceObj['UserId'] = this.StudentId;
        this.CreditnoteService.getInvoiceItems1(invoiceObj).subscribe(
            data => {
                this.maininvoiceList = data['result']['data'];
            }
        );
    }

    hidestudentCustomer() {
        console.log(this.advancePaymentData['f031fidStudent']);
        this.advancePaymentList = [];
        this.studentList = [];
        this.customerList = [];

        if (this.advancePaymentData['f031type'] == 'Student') {
            for (var i = 0; i < this.advancePaymentListNew.length; i++) {
                if (this.advancePaymentListNew[i]['f020ftype'] == 'Student') {
                    this.studentList.push(this.advancePaymentListNew[i]);
                    if (this.advancePaymentData['f031fidStudent'] != undefined)
                        this.advancePaymentList.push(this.advancePaymentListNew[i])
                    this.getStudentInvoices(this.advancePaymentData['f031fidStudent']);
                }
            }
        }
        if (this.advancePaymentData['f031type'] != 'Student') {
            for (var i = 0; i < this.advancePaymentListNew.length; i++) {
                if (this.advancePaymentListNew[i]['f020ftype'] == 'Customer') {
                    this.customerList.push(this.advancePaymentListNew[i]);
                    if (this.advancePaymentData['f031fidCustomer'] != undefined)
                        this.advancePaymentList.push(this.advancePaymentListNew[i])
                }
            }
        }
    }
    getList() {
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        this.AdvancepaymentService.getItems().subscribe(
            data => {
                console.log(data['result']);
                this.advancePaymentListNew = data['result']['data'];
            }, error => {
                console.log(error);
            });
    }
    knockOffInvoice() {
        let knockOffarrayList = [];
        for (var i = 0; i < this.maininvoiceList.length; i++) {
            if (this.maininvoiceList[i].status == true) {
                knockOffarrayList.push(this.maininvoiceList[i].f071fid);
            }
        }
        if (knockOffarrayList.length < 1) {
            alert("Select atleast one Invoice Number to Knock Off");
            return false;
        }
        var confirmPop = confirm("Do you want to Knock Off Invoice?");
        if (confirmPop == false) {
            return false;
        }
        var knockodffObject = {};
        knockodffObject['id'] = knockOffarrayList;
        this.AdvancepaymentService.updateKnockOff(knockodffObject).subscribe(
            data => {
                this.getInvoiceList();
            }, error => {
                console.log(error);
            });

    }
    getStudentInvoices(id) {
        this.AdvancepaymentService.getInvoices(id).subscribe(
            data => {
                this.invoiceList = data['result']['data'];
            }, error => {
                console.log(error);
            });
    }
}