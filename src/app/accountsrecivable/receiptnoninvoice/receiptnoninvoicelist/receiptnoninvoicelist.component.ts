import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ReceiptnoninvoiceService } from '../../service/receiptnoninvoice.service';


@Component({
    selector: 'ReceiptnoninvoicelistComponent',
    templateUrl: 'receiptnoninvoicelist.component.html'
})

export class ReceiptnoninvoicelistComponent implements OnInit {
    receiptnoninvoicelistList =  [];
    receiptnoninvoicelistData = {};
    id: number;

    constructor(
        
        private ReceiptnoninvoiceService: ReceiptnoninvoiceService

    ) { }

    ngOnInit() { 
        
        this.ReceiptnoninvoiceService.getItems().subscribe(
            data => {
                this.receiptnoninvoicelistList = data['result']['data'];
        }, error => {
            console.log(error);
        });
    }
}