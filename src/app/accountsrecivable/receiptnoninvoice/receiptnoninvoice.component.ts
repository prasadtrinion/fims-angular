import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { CustomerService } from '../../accountsrecivable/service/customer.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ReceiptnoninvoiceService } from '../service/receiptnoninvoice.service'
import { GlcodegenerationService } from '../../generalledger/service/glcodegeneration.service'
import { ItemService } from '../service/item.service'
@Component({
    selector: 'ReceiptnoninvoiceComponent',
    templateUrl: 'receiptnoninvoice.component.html'
})

export class ReceiptnoninvoiceComponent implements OnInit {
    receiptnoninvoiceList = [];
    receiptnoninvoiceData = {};
    customerList = [];
    customerData = {};
    glcodeList = [];
    itemList = [];
    glcodeData = {};
    id: number;
    receiptnoninvoiceDataheader = {};
    ajaxCount : number;
    constructor(
 
        private GlcodegenerationService: GlcodegenerationService,
        private ItemService: ItemService,
        private ReceiptnoninvoiceService: ReceiptnoninvoiceService,
        private CustomerService: CustomerService,
        private route: ActivatedRoute,
        private router: Router
    ) { }

    ngOnChanges(abc) {
        console.log(this.ajaxCount);
    }

    
    ngOnInit() {
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.ajaxCount = 0;

        //item dropdown
        this.ajaxCount++;
        this.ItemService.getItems().subscribe(
            data=>{
                this.itemList = data['result'];
                this.CustomerService.getItems().subscribe(
                    data=>{
                        this.customerList = data['result']['data'];
                        this.GlcodegenerationService.getItems().subscribe(
                            data => {
                                this.glcodeList = data['result']['data'];
                                if(this.id>0) {
                                    this.ReceiptnoninvoiceService.getItemsDetail(this.id).subscribe(
                                        data => {
                                            this.receiptnoninvoiceList = data['result'];
                                            this.receiptnoninvoiceDataheader['f071fapprovedBy'] = data['result'][0].f071fapprovedBy;
                                            this.receiptnoninvoiceDataheader['f071finvoiceDate'] = data['result'][0].f071finvoiceDate;
                                            this.receiptnoninvoiceDataheader['f071finvoiceNumber'] = data['result'][0].f071finvoiceNumber;
                                            this.receiptnoninvoiceDataheader['f071finvoiceTotal'] = data['result'][0].f071finvoiceTotal;
                                            this.receiptnoninvoiceDataheader['f071finvoiceType'] = data['result'][0].f071finvoiceType;
                                            this.receiptnoninvoiceDataheader['f071fidCustomer'] = data['result'][0].f071fidCustomer;
                                            this.receiptnoninvoiceDataheader['f071fid'] = data['result'][0].f071fid;

                                            console.log(this.receiptnoninvoiceList);
                                        }, error => {
                                            console.log(error);
                                        });
                                }
                                
                            }
                        );
                    }
                );

            }
        );

        
    }
    indexTracker(index: number, value: any) {
        return index;
      }
    addeceiptnoninvoice() {
        console.log(this.receiptnoninvoiceList);
       
    }
    ConvertToInt(val) {
        return parseInt(val);
    }
    
    saveReceiptnoninvoice(){
        let receiptnoninvoiceObject = {};
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if(this.id>0) {
            receiptnoninvoiceObject['f071fid'] = this.id;
        }
        receiptnoninvoiceObject['f071finvoiceNumber'] = this.receiptnoninvoiceDataheader['f071finvoiceNumber'];
        receiptnoninvoiceObject['f071finvoiceType'] = this.receiptnoninvoiceDataheader['f071finvoiceType'];
        receiptnoninvoiceObject['f071fstatus'] = 0;
        receiptnoninvoiceObject['f071finvoiceDate'] = this.receiptnoninvoiceDataheader['f071finvoiceDate'];
        receiptnoninvoiceObject['f071finvoiceTotal'] = this.receiptnoninvoiceDataheader['f071finvoiceTotal'];
        receiptnoninvoiceObject['f071fidCustomer'] = this.receiptnoninvoiceDataheader['f071fidCustomer'];
        receiptnoninvoiceObject['invoice-details'] = this.receiptnoninvoiceList;

    }
    
    addreceiptnoninvoicelist() {
       console.log(this.receiptnoninvoiceData);
        if(this.receiptnoninvoiceData['f072fidItem']==undefined){
            alert('Select Item');
            return false; 
        }
        if(this.receiptnoninvoiceData['f072fglcode']==undefined){
            alert('Select GL Code');
            return false;
        }
        if(this.receiptnoninvoiceData['f072fquantity']==undefined){
            alert('Enter Quantity');
            return false;
        }

        var totalamount = (this.ConvertToInt(this.receiptnoninvoiceData['f072fquantity']))*(this.ConvertToInt(this.receiptnoninvoiceData['f072fprice']));
        var gstamount = (this.ConvertToInt(this.receiptnoninvoiceData['f072fgst']))/100*(totalamount);
        this.receiptnoninvoiceData['f072ftotal'] = gstamount + totalamount;
        var dataofCurrentRow = this.receiptnoninvoiceData;
        this.receiptnoninvoiceData = {};
        this.receiptnoninvoiceList.push(dataofCurrentRow);
        console.log(this.receiptnoninvoiceList);
    }

}