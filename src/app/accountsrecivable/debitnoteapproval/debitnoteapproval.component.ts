import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Injector } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DebitnoteapprovalService } from '../service/debitnoteapproval.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'DebitnoteapprovalComponent',
    templateUrl: 'debitnoteapproval.component.html'
})

export class DebitnoteapprovalComponent implements OnInit {
    debitnoteApprovalList = [];
    debitnoteApprovalData = {};
    id: number;
    selectAllCheckbox: false;
    type: string;
    title = '';

    constructor(

        private DebitnoteapprovalService: DebitnoteapprovalService,
        private spinner: NgxSpinnerService,
        private route: ActivatedRoute,
        private router: Router

    ) { }
    ngOnInit() {
        this.getList();
    }
    getList() {
        this.spinner.show();                                
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.DebitnoteapprovalService.getItems().subscribe(
            data => {
                this.spinner.hide();
                // this.debitnoteApprovalList = data['result'];
                let activeData = [];
                activeData = data['result'];
                for (var i = 0; i < activeData.length; i++) {
                    if (activeData[i]['f071fstatus'] == 0) {
                        activeData[i]['f071fstatus'] = 'Pending';
                    } else if (activeData[i]['f071fstatus'] == 1) {
                        activeData[i]['f071fstatus'] = 'Approved';
                    } else {
                        activeData[i]['f071fstatus'] = 'Rejected';
                    }
                    if (activeData[i]['f031ftype'] == 'DSTA') {
                        activeData[i]['f031ftype'] = 'Staff';

                    } else if (activeData[i]['f031ftype'] == 'DSTD') {
                        activeData[i]['f031ftype'] = 'Student';
                    }
                    else {
                        if (activeData[i]['f031ftype'] == 'DOR') {
                            activeData[i]['f031ftype'] = 'Other Receivables';
                        }
                    }
                }
                this.debitnoteApprovalList = activeData;

            }, error => {
                console.log(error);
            });
    }
    ApprovalListData() {
        let approveDarrayList = [];

        for (var i = 0; i < this.debitnoteApprovalList.length; i++) {
            if (this.debitnoteApprovalList[i].f031fstatus == true) {
                approveDarrayList.push(this.debitnoteApprovalList[i].f031fid);
            }
        }
        if (approveDarrayList.length < 1) {
            alert("Select atleast one Debit Note to Approve");
            return false;
        }
        var confirmPop = confirm("Do you want to Approve?");
        if (confirmPop == false) {
            return false;
        }
        var approvalObject = {};
        approvalObject['id'] = approveDarrayList;
        approvalObject['status'] = '1';
        approvalObject['reason'] = '';

        this.DebitnoteapprovalService.updateApproval(approvalObject).subscribe(
            data => {
                this.getList();
                this.debitnoteApprovalData['reason'] = '';
                alert("Approved Successfully");
            }, error => {
                console.log(error);
            });

    }
    regectdebitorApprovalListData() {
        let approveDarrayList = [];
        for (var i = 0; i < this.debitnoteApprovalList.length; i++) {
            if (this.debitnoteApprovalList[i].f031fstatus == true) {
                approveDarrayList.push(this.debitnoteApprovalList[i].f031fid);
            }
        }
        if (approveDarrayList.length < 1) {
            alert("Select atleast one Debit Note to Reject");
            return false;
        }
        if (this.debitnoteApprovalData['reason'] == undefined) {
            alert('Enter the Description');
            return false;
        }
        var confirmPop = confirm("Do you want to Reject?");
        if (confirmPop == false) {
            return false;
        }
        var debitnoteDataObject = {}
        debitnoteDataObject['id'] = approveDarrayList;
        debitnoteDataObject['status'] = '2';
        debitnoteDataObject['reason'] = this.debitnoteApprovalData['reason'];
        this.DebitnoteapprovalService.updateApproval(debitnoteDataObject).subscribe(
            data => {
                this.getList();
                this.debitnoteApprovalData['reason'] = '';
                alert("Rejected Successfully");
            }, error => {
                console.log(error);
            });
    }
}