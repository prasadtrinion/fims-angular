import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { CustomerService } from '../../../accountsrecivable/service/customer.service'
import { ActivatedRoute, Router } from '@angular/router';
import { DebitnoteapprovalService } from '../../service/debitnoteapproval.service';
import { ItemService } from '../../service/item.service';
import { MaininvoiceService } from '../../service/maininvoice.service';
import { GlcodegenerationService } from '../../../generalledger/service/glcodegeneration.service';
import { UserService } from '../../../generalsetup/service/user.service';
@Component({
    selector: 'DebitnoteapprovalformComponent',
    templateUrl: 'debitnoteapprovalform.component.html'
})

export class DebitnoteapprovalformComponent implements OnInit {
    debitnoteData = {};
    customerList = [];
    maininvoiceList = [];
    maininvoiceData = {};
    customerData = {};
    glcodeList = [];
    glcodeData = {};
    itemList = [];
    userData = {};
    userList = [];
    debitNotelist = [];
    title = '';
    viewId: number;
    openingbalanceData = {};
    newOpeningBalanceList = [];
    saveDataObject = {};
    ajaxCount:number;
    id: number;
    showCrReadonly: boolean;
    showDrReadonly: boolean;
    balance: number;
    cnamount: number;
    newbal: number;
    cash: number;
    debitnoteAddedAmount: number;
    payment: number;
    type: string;
    valueDate: string;
    saveBtnDisable: boolean;
    viewDisabled: boolean;
    constructor(

        private GlcodegenerationService: GlcodegenerationService,
        private DebitnoteService: DebitnoteapprovalService,
        private ItemService: ItemService,
        private MaininvoiceService: MaininvoiceService,
        private CustomerService: CustomerService,
        private UserService: UserService,
        private route: ActivatedRoute,
        private router: Router
    ) { }

    ngOnInit() {
        this.debitnoteAddedAmount = 0;
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        this.route.paramMap.subscribe((data) => this.viewId = + data.get('viewId'));

        this.valueDate = new Date().toISOString().substr(0, 10);
        console.log(this.valueDate);
        this.debitnoteData['f031fdate'] = this.valueDate;

        //invoice dropdown
        this.MaininvoiceService.getItems().subscribe(
            data => {
                this.maininvoiceList = data['result']['data'];
            }
        );
        //user dropdown
        this.UserService.getItems().subscribe(
            data => {
                this.userList = data['result']['data'];
            }
        );
        //item dropdown

        this.ItemService.getItems().subscribe(
            data => {
                this.itemList = data['result'];
                this.GlcodegenerationService.getItems().subscribe(
                    data => {
                        this.glcodeList = data['result']['data'];
                        this.CustomerService.getItems().subscribe(
                            data => {
                                this.customerList = data['result']['data'];
                                if (this.viewId > 0) {
                                    this.DebitnoteService.getItemsDetail(this.viewId).subscribe(
                                        data => {
                                            console.log(this.viewId);
                                            this.debitnoteData = data['result'];
                                            this.debitNotelist = data['result'];

                                            this.debitnoteData['f031fbalance'] = data['result'][0]['f031fbalance'],
                                                this.debitnoteData['f031fdate'] = data['result'][0]['f031fdate'],
                                                this.debitnoteData['f031fidInvoice'] = data['result'][0]['f031fidInvoice']
                                            this.debitnoteData['f031fnarration'] = data['result'][0]['f031fnarration'],
                                                this.debitnoteData['f031freferenceNumber'] = data['result'][0]['f031freferenceNumber'],
                                                this.debitnoteData['f031ftotalAmount'] = data['result'][0]['f031ftotalAmount'],
                                                this.debitnoteData['f031fvoucherNumber'] = data['result'][0]['f031fvoucherNumber'],
                                                this.debitnoteData['f031fidInvoice'] = data['result'][0]['f071fidCustomer']
                                            this.maininvoiceList = [];

                                            for (var i = 0; i < this.debitNotelist.length; i++) {
                                                console.log(this.debitNotelist[i]);
                                                var debitnoteObject = {};
                                                debitnoteObject['f072fidItem'] = this.debitNotelist[i]['f032fidItem'];
                                                debitnoteObject['f072fidDebitGlcode'] = this.debitNotelist[i]['f032fidDebitGlcode'];
                                                debitnoteObject['f072fidCreditGlcode'] = this.debitNotelist[i]['f032fidCreditGlcode'];
                                                debitnoteObject['f072fquantity'] = this.debitNotelist[i]['f032fquantity'];
                                                debitnoteObject['f072fprice'] = this.debitNotelist[i]['f032fprice'];
                                                debitnoteObject['f072fgstCode'] = this.debitNotelist[i]['f032fgstCode'];
                                                debitnoteObject['f072fgstValue'] = this.debitNotelist[i]['f032fgstValue'];
                                                debitnoteObject['f072ftotal'] = this.debitNotelist[i]['f032ftotal'];
                                                debitnoteObject['f032ftoDnAmount'] = this.debitNotelist[i]['f032ftoDnAmount'];

                                                this.maininvoiceList.push(debitnoteObject);
                                            }
                                           
                                            console.log(this.maininvoiceList);

                                        }, error => {
                                            console.log(error);
                                        });
                                }
                            }
                        );
                    }
                );
            }
        );

    }

    getInvoiceDetails() {
        let invid = 0;
        invid = this.debitnoteData['f031fidInvoice'];
        console.log(invid);


        this.MaininvoiceService.getInvoiceDetailsById(invid).subscribe(
            data => {
                console.log(data);
                this.maininvoiceList = data['result'];
                for (var i = 0; i < this.maininvoiceList.length; i++) {
                    this.maininvoiceList[i]['f032ftoDnAmount'] = 0;
                }
                this.debitnoteData['f031fcustomer'] = data['result'][0]['f071fidCustomer'],
                    this.debitnoteData['f031ftotalAmount'] = data['result'][0]['f071finvoiceTotal'],
                    this.debitnoteData['invoiceDate'] = data['result'][0]['f071finvoiceDate'],
                    this.debitnoteData['f031fbalance'] = data['result'][0]['f071fbalance']

            }
        );
    }

    addDebitAmount() {


        this.debitnoteAddedAmount = 0;
        for (var i = 0; i < this.maininvoiceList.length; i++) {
            this.debitnoteAddedAmount = this.ConvertToFloat(this.debitnoteAddedAmount) + this.ConvertToFloat(this.maininvoiceList[i]['f032ftoDnAmount']);

            if (this.debitnoteAddedAmount > this.debitnoteData['f031fbalance']) {
                alert("asdf");
            }

        }
        this.debitnoteData['f031fbalance'] = this.ConvertToFloat(this.debitnoteData['f031ftotalAmount']) - this.ConvertToFloat(this.debitnoteAddedAmount);
    }

    saveData() {
        this.saveDataObject["f031fidInvoice"] = this.debitnoteData['f031fidInvoice'],
            this.saveDataObject["f031ftotalAmount"] = this.debitnoteData['f031ftotalAmount'],
            this.saveDataObject["f031fbalance"] = this.debitnoteData['f031fbalance'],
            this.saveDataObject["f031freferenceNumber"] = this.debitnoteData['f031freferenceNumber'],
            this.saveDataObject["f031fdate"] = this.debitnoteData['f031fdate'],
            this.saveDataObject["f031fvoucherNumber"] = this.debitnoteData['f031fvoucherNumber'],
            this.saveDataObject["f031fnarration"] = this.debitnoteData['f031fnarration'],
            this.saveDataObject["f031fstatus"] = 0

        for (var i = 0; i < this.maininvoiceList.length; i++) {
            var creditnoteObject = {}
            creditnoteObject['f032fidItem'] = this.maininvoiceList[i]['f072fidItem'];
            creditnoteObject['f032fidDebitGlcode'] = this.maininvoiceList[i]['f072fidCreditGlcode'];
            creditnoteObject['f032fidCreditGlcode'] = this.maininvoiceList[i]['f072fidDebitGlcode'];
            creditnoteObject['f032fquantity'] = this.maininvoiceList[i]['f072fquantity'];
            creditnoteObject['f032fprice'] = this.maininvoiceList[i]['f072fprice'];
            creditnoteObject['f032fgstCode'] = this.maininvoiceList[i]['f072fgstCode'];
            creditnoteObject['f032fgstValue'] = this.maininvoiceList[i]['f072fgstValue'];
            creditnoteObject['f032ftotal'] = this.maininvoiceList[i]['f072ftotal'];
            creditnoteObject['f032ftoDnAmount'] = this.maininvoiceList[i]['f032ftoDnAmount'];

            this.debitNotelist.push(creditnoteObject);
        }
        this.saveDataObject["details"] = this.debitNotelist;
        console.log(this.saveDataObject);
    }
    ConvertToFloat(val) {
        return parseFloat(val);
    }
}