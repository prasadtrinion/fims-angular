import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Injector } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AdvancepaymentService } from '../service/advancepayment.service'
import { StudentService } from '../../studentfinance/service/student.service';
import { CustomerService } from '../../accountsrecivable/service/customer.service';
import { faSearch, faEdit } from '@fortawesome/free-solid-svg-icons';
import { CreditnoteService } from '../service/creditnote.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'KnockoffComponent',
    templateUrl: 'knockoff.component.html'
})

export class KnockoffComponent implements OnInit {
    advancePaymentList = [];
    creditnoteApprovalData = {};
    advancePaymentData = {};
    studentList = [];
    customerList = [];
    advancePaymentListNew = [];
    invoiceList = [];
    paymenttype = [];
    maininvoiceList = [];
    id: number;
    selectAllCheckbox: false;
    type: string;
    title = '';
    faSearch = faSearch;
    faEdit = faEdit;
    idview: number;
    showFlag: boolean;
    ajaxCount: number;
    UserId: number;
    StudentId: number;
    constructor(
        private AdvancepaymentService: AdvancepaymentService,
        private StudentService: StudentService,
        private CustomerService: CustomerService,
        private CreditnoteService: CreditnoteService,
        private spinner: NgxSpinnerService,
        private route: ActivatedRoute,
        private router: Router
    ) { }
    ngOnInit() {

        //Student dropdown
        this.ajaxCount++;
        this.StudentService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                console.log(this.ajaxCount);

                this.studentList = data['result']['data'];
            }
        );
        //Customer dropdown
        this.ajaxCount++;
        this.CustomerService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.customerList = data['result']['data'];
            }
        );
        let stafftypeobj = {};
        stafftypeobj['name'] = 'Student';
        stafftypeobj['type'] = 'KSTD';
        this.paymenttype.push(stafftypeobj);
        stafftypeobj = {};
        stafftypeobj['name'] = 'Customer';
        stafftypeobj['type'] = 'KCUS';
        this.paymenttype.push(stafftypeobj);
        this.showFlag = true;
        this.getList();
        this.route.paramMap.subscribe((data) => this.idview = + data.get('idview'));
        // if(this.idview>0) {
        //    this.showFlag = true;
        // }

    }
    getType() {
        let typeObject = {};
        typeObject['type'] = this.advancePaymentData['f031ftype'];
        this.type = this.advancePaymentData['f031ftype']
        console.log(typeObject);
        return this.type;
    }
    getInvoiceList() {
        this.UserId = this.advancePaymentData['f031fidCustomer'];
        // this.StudentId = this.advancePaymentData['f031fidCustomer'];
        let invoiceObj = {};
        switch (this.type) {
            case 'KSTD':
                invoiceObj['type'] = "STD";
                break;
            case 'KCUS':
                invoiceObj['type'] = "OR";
                break;
        }
        invoiceObj['UserId'] = this.UserId;
        // invoiceObj['UserId'] = this.StudentId;
        this.spinner.show();
        this.CreditnoteService.getInvoiceItems(invoiceObj).subscribe(
            data => {
                this.spinner.hide();
                console.log(data);
                this.maininvoiceList = data['result']['data'];
            }
        );
    }
    getList() {
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        this.AdvancepaymentService.getItems().subscribe(
            data => {
                console.log(data['result']);
                this.advancePaymentListNew = data['result']['data'];
                console.log(this.studentList);
                console.log(this.customerList);
            }, error => {
                console.log(error);
            });
    }
    getStudentInvoices(id) {
        this.AdvancepaymentService.getInvoices(id).subscribe(
            data => {
                this.invoiceList = data['result']['data'];
            }, error => {
                console.log(error);
            });
    }
    refundKnockOffInvoice() {
        let knockOffarrayList = [];
        console.log(this.maininvoiceList);
        for (var i = 0; i < this.maininvoiceList.length; i++) {
            if (this.maininvoiceList[i].status == true) {
                knockOffarrayList.push(this.maininvoiceList[i].f071fid);
            }
        }
        if (knockOffarrayList.length < 1) {
            alert("Select atleast one Invoice Number to Knock off");
            return false;
        }
        var confirmPop = confirm("Do you want to Knock Off Invoice?");
        if (confirmPop == false) {
            return false;
        }
        var knockodffObject = {};
        knockodffObject['id'] = knockOffarrayList;
        this.AdvancepaymentService.updateKnockOff(knockodffObject).subscribe(
            data => {
                this.getInvoiceList();
            }, error => {
                console.log(error);
            });
    }
}