import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Injector } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DebtorapprovalService } from '../service/debtorapproval.service'


@Component({
    selector: 'Debtorapproval',
    templateUrl: 'debtorapproval.component.html'
})

export class DebtorapprovalComponent implements OnInit {
    debtorapprovalList = [];
    debtorapprovalData = {};
    id: number;
    selectAllCheckbox: false;
    type: string;
    title = '';

    constructor(

        private DebtorapprovalService: DebtorapprovalService,
        private route: ActivatedRoute,

    ) { }
    ngOnInit() {
        this.getList();
    }
    getList() {
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        if (this.id == 0) {
            this.type = 'debtor';
            this.title = 'Debtor Approval';
        } else {
            this.type = 'creditor';
            this.title = 'Creditor Approval';
        }
        this.DebtorapprovalService.getItems(this.type).subscribe(
            data => {
                this.debtorapprovalList = data['result'];
            }, error => {
                console.log(error);
            });
    }
    debtorApprovalListData() {
        let approveDarrayList = [];
        console.log(this.debtorapprovalList);
        for (var i = 0; i < this.debtorapprovalList.length; i++) {
            if (this.debtorapprovalList[i].f079fstatus == true) {
                approveDarrayList.push(this.debtorapprovalList[i].f079fid);
            }
        }
        var approvalObject = {};
        approvalObject['id'] = approveDarrayList;

        this.DebtorapprovalService.updatedebtorApproval(approvalObject).subscribe(
            data => {
                this.getList();
            }, error => {
                console.log(error);
            });
    }
}