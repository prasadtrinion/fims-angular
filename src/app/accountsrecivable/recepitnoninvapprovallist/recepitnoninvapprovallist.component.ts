import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Injector } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ReceiptnoninvapprovallistService } from '../service/recepitnoninvapprovallist.service'


@Component({
    selector: 'Recepitnoninvapprovallist',
    templateUrl: 'recepitnoninvapprovallist.component.html'
})

export class ReceiptnoninvapprovallistComponent implements OnInit {
    receiptnoninvapprovallistList = [];
    receiptnoninvapprovallistData = {};
    id: number;
    selectAllCheckbox:false;

    constructor(
        
        private ReceiptnoninvapprovallistService: ReceiptnoninvapprovallistService,
        private route: ActivatedRoute,
        private router: Router

    ) { }
    ngOnInit() { 
    }
    ReceiptnoninvapprovallistData() {
    let approveDarrayList = [];
    console.log(this.receiptnoninvapprovallistList);
        for (var i = 0; i < this.receiptnoninvapprovallistList.length; i++) {
            if(this.receiptnoninvapprovallistList[i].f071fstatus==true) {
                approveDarrayList.push(this.receiptnoninvapprovallistList[i].f071fid);
            }
        }
        var approvalObject = {};
        approvalObject['id'] = approveDarrayList;
    
        this.ReceiptnoninvapprovallistService.updatereceiptnoninvapprovallistItems(approvalObject).subscribe(
            data => {
             
            }, error => {
            console.log(error);
        });

    }
    selectAll() {
        console.log(this.selectAllCheckbox);
        for (var i = 0; i < this.receiptnoninvapprovallistList.length; i++) {
                this.receiptnoninvapprovallistList[i].f071fstatus = this.selectAllCheckbox;
        }
      }

}