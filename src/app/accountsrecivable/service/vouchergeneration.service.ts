import { Injectable } from '@angular/core';
import { HttpClientModule,HttpHeaders, HttpClient} from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({ 'X-Auth-Client': 'qazwsx123',
    'Content-Type': 'application/json' })
    
  };
  
@Injectable()

export class VouchergenerationService {
    url: string = environment.api.base + environment.api.endPoints.voucher;
    urlREferenceNumber : string = environment.api.base + environment.api.endPoints.generateNumber;

    constructor(private httpClient: HttpClient) { 
        
    }
    
    getItems() {
        return this.httpClient.get(this.url,httpOptions);
    }

    getDetail(id) {
        return this.httpClient.get(this.url+'/'+id,httpOptions);
    }

    insertVoucherItems(voucherData): Observable<any> {
        return this.httpClient.post(this.url, voucherData,httpOptions);
    }
    getVoucherNumber(data) {
        return this.httpClient.post(this.urlREferenceNumber, data,httpOptions);

    }
    updateVoucherItems(voucherData,id): Observable<any> {
        return this.httpClient.put(this.url+'/'+id, voucherData,httpOptions);
       }
}