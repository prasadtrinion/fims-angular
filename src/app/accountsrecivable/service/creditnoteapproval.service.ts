import { Injectable } from '@angular/core';
import { HttpClientModule, HttpHeaders, HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({
        'X-Auth-Client': 'qazwsx123',
        'Content-Type': 'application/json'
    })

};

@Injectable()

export class CreditnoteapprovalService {

    creditNote:string = environment.api.base + environment.api.endPoints.creditNote;
    creditNoteurl: string = environment.api.base + environment.api.endPoints.approvedCreditNotes;
    creditNoteApproveurl: string = environment.api.base + environment.api.endPoints.creditNoteApprove;

    constructor(private httpClient: HttpClient) {
    }
    getItems() {
            return this.httpClient.get(this.creditNoteurl + '/' + 0, httpOptions);
    }
    getItemsDetail(id) {
        return this.httpClient.get(this.creditNote+'/' + id, httpOptions);
    }
    getInvoiceDetailsById(id) {
        return this.httpClient.get(this.creditNote+'/'+id,httpOptions);
}

    updateApproval(approvalData): Observable<any> {
            return this.httpClient.post(this.creditNoteApproveurl, approvalData, httpOptions);
    }


}