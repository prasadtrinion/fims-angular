import { Injectable } from '@angular/core';
import { HttpClientModule,HttpHeaders, HttpClient} from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({ 'X-Auth-Client': 'qazwsx123',
    'Content-Type': 'application/json' })
    
  };
  
@Injectable()

export class SponsorcreditnoteService {
    url: string = environment.api.base + environment.api.endPoints.sponsorCN;
    crediturl: string = environment.api.base + environment.api.endPoints.creditNote;
    sponsorHasBillsUrl : string = environment.api.base + environment.api.endPoints.sponsorHasBillsList;
    getCreditnoteNumberUrl : string = environment.api.base + environment.api.endPoints.generateNumber;
    getInvoiceListBasedOnUser : string = environment.api.base + environment.api.endPoints.customerMasterInvoices;
    constructor(private httpClient: HttpClient) { 
        
    }
    getInvoiceItems(data){
        return this.httpClient.post(this.getInvoiceListBasedOnUser,data,httpOptions);

    }
    getCreditnoteNumber(data) {
        return this.httpClient.post(this.getCreditnoteNumberUrl, data,httpOptions);

    }
    getsponsorHasBillsList(spId){
        return this.httpClient.get(this.sponsorHasBillsUrl+'/'+spId,httpOptions);
    }
    
    getItems() {
        return this.httpClient.get(this.url,httpOptions);
    }

    getItemsDetail(id) {
        return this.httpClient.get(this.url+'/'+id,httpOptions);
    }

    insertSponsorcreditItems(sponsorcreditData): Observable<any> {
        return this.httpClient.post(this.url, sponsorcreditData,httpOptions);
    }

    updateSponsorcreditItems(sponsorcreditData,id): Observable<any> {
        return this.httpClient.put(this.url+'/'+id, sponsorcreditData,httpOptions);
       }
}