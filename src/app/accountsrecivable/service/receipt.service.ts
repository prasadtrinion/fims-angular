import { Injectable } from '@angular/core';
import { HttpClientModule,HttpHeaders, HttpClient} from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({ 'X-Auth-Client': 'qazwsx123',
    'Content-Type': 'application/json' })
    
  };
  
@Injectable()

export class ReceiptService {
    url: string = environment.api.base + environment.api.endPoints.receipt;
    getReferenceNumberURL : string = environment.api.base + environment.api.endPoints.generateNumber;
    tempspnsorinvURL : string = environment.api.base + environment.api.endPoints.tempspnsorinv;
    getInvoiceListBasedOnUser : string = environment.api.base + environment.api.endPoints.customerMasterInvoiceDetails;
    sponsorHasBillsUrl : string = environment.api.base + environment.api.endPoints.customerMasterInvoiceDetails;
    vendorReceipts : string = environment.api.base + environment.api.endPoints.vendorReceipts;
    ActiveReceiptsurl : string = environment.api.base + environment.api.endPoints.receipts;
    constructor(private httpClient: HttpClient) { 
        
    } 

    getReceiptNumber(data){

        return this.httpClient.post(this.getReferenceNumberURL, data,httpOptions);
    }
    getInvoiceItems(data){
        return this.httpClient.post(this.getInvoiceListBasedOnUser,data,httpOptions);

    }
    getItems() {
        return this.httpClient.get(this.url,httpOptions);
    }
    getActiveItems() {
        return this.httpClient.get(this.ActiveReceiptsurl+'/'+1,httpOptions);
    }
    getsponsorHasBillsList(spId){
        return this.httpClient.get(this.sponsorHasBillsUrl+'/'+spId,httpOptions);
    }

    getsponsorHasBillsListApi(spId){
        return this.httpClient.get(this.getInvoiceListBasedOnUser+'/'+spId,httpOptions);

    }

    getItemsDetail(id) {
        return this.httpClient.get(this.url+'/'+id,httpOptions);
    }
    getVendorItemsDetail(id) {
        return this.httpClient.get(this.vendorReceipts+'/'+id,httpOptions);
    }

    insertReceiptItems(utilityData): Observable<any> {
        return this.httpClient.post(this.url, utilityData,httpOptions);
    }

    updateReceiptItems(utilityData,id): Observable<any> {
        return this.httpClient.put(this.url+'/'+id, utilityData,httpOptions);
       }
}