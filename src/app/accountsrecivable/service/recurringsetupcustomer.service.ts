import { Injectable } from '@angular/core';
import { HttpClientModule, HttpHeaders, HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({
        'X-Auth-Client': 'qazwsx123',
        'Content-Type': 'application/json'
    })

};

@Injectable()

export class RecurringsetupcustomerService {
    url: string = environment.api.base + environment.api.endPoints.recurringsetupcustomer;
    deleteurl: string = environment.api.base + environment.api.endPoints.deleteRecurringSetupDetails;
    urlgenerateRecurringSetupInvoice: string = environment.api.base + environment.api.endPoints.generateRecurringSetupInvoice
    constructor(private httpClient: HttpClient) {

    }

    getItems() {
        return this.httpClient.get(this.url, httpOptions);
    }
    deleteItems(deleteObject) {
        return this.httpClient.post(this.deleteurl,deleteObject,httpOptions);
    }
    deleteDataItems(deleteObject) {
        return this.httpClient.post(this.deleteurl,deleteObject,httpOptions);
    }
    generateBillForUtility(recurringsetupcustomerData): Observable<any> {
        return this.httpClient.post(this.urlgenerateRecurringSetupInvoice, recurringsetupcustomerData, httpOptions);
    }
    getItemsDetail(id) {
        return this.httpClient.get(this.url + '/' + id, httpOptions);
    }

    insertRecurringsetupcustomerItems(recurringsetupcustomerData): Observable<any> {
        return this.httpClient.post(this.url, recurringsetupcustomerData, httpOptions);
    }

    updateRecurringsetupcustomerItems(recurringsetupcustomerData, id): Observable<any> {
        return this.httpClient.put(this.url + '/' + id, recurringsetupcustomerData, httpOptions);
    }
}