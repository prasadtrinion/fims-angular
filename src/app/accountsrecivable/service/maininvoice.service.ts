import { Injectable } from '@angular/core';
import { HttpClientModule, HttpHeaders, HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({
        'X-Auth-Client': 'qazwsx123',
        'Content-Type': 'application/json'
    })

};

@Injectable()

export class MaininvoiceService {
    masterInvoiceurl: string = environment.api.base + environment.api.endPoints.masterInvoice;
    masterInvoiceApproveurl: string = environment.api.base + environment.api.endPoints.masterInvoiceApprove;
    downloadInvoiceUrl: string = environment.api.base + environment.api.endPoints.invoiceReport;
    generateStudentBillUrl: string = environment.api.base + environment.api.endPoints.generateStudentBill;
    deleteUrl: string = environment.api.base + environment.api.endPoints.deleteMasterInvoiceDetails;
    getInvoiceUrl: string = environment.api.base + environment.api.endPoints.requestInvoiceByType;
    getMasterInvoiceListUrl: string = environment.api.base + environment.api.endPoints.getMasterInvoiceList;

    constructor(private httpClient: HttpClient) {

    }
    getItems() {
        return this.httpClient.get(this.masterInvoiceurl, httpOptions);
    }
    getMasterInvoiceItems() {
        return this.httpClient.get(this.getMasterInvoiceListUrl, httpOptions);
    }
    getInvoiceItems(invoiceObj) {
        return this.httpClient.post(this.getInvoiceUrl,invoiceObj,httpOptions);
    }
    deleteItems(deleteObject) {
        return this.httpClient.post(this.deleteUrl, deleteObject, httpOptions);
    }
    getItemsDetail(id) {
        return this.httpClient.get(this.masterInvoiceurl + '/' + id, httpOptions);
    }

    getBatchDetailsById(spid) {
        return this.httpClient.get(this.generateStudentBillUrl + '/' + spid, httpOptions);

    }

    getInvoiceDetailsById(id) {
        return this.httpClient.get(this.masterInvoiceurl + '/' + id, httpOptions);
    }
    downloadInvoice(invoiceData): Observable<any> {
        return this.httpClient.post(this.downloadInvoiceUrl, invoiceData, httpOptions);
    }

    insertMaininvoiceItems(invoiceData): Observable<any> {
        return this.httpClient.post(this.masterInvoiceurl, invoiceData, httpOptions);
    }

    updateInvoiceItems(invoiceData, id): Observable<any> {
        return this.httpClient.put(this.masterInvoiceurl + '/' + id, invoiceData, httpOptions);
    }
}