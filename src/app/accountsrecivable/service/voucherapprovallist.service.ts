import { Injectable } from '@angular/core';
import { HttpClientModule, HttpHeaders, HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({
        'X-Auth-Client': 'qazwsx123',
        'Content-Type': 'application/json'
    })

};

@Injectable()

export class VoucherapprovallistService {
    urlvoucher: string = environment.api.base + environment.api.endPoints.voucher;
    urlvoucherapproval: string = environment.api.base + environment.api.endPoints.voucherapprovallist;
    constructor(private httpClient: HttpClient) {

    }

    getItems(type) {
        if (type == 'credit') {
            return this.httpClient.get(this.urlvoucher + '/' + 1 + '/' + 0, httpOptions);

        } 
        if (type == 'debit') {
            return this.httpClient.get(this.urlvoucher + '/' + 2 + '/' + 0, httpOptions);

        }
        if (type == 'cash') {
            return this.httpClient.get(this.urlvoucher + '/' + 3 + '/' + 0, httpOptions);

        }
        if (type == 'payment') {
            return this.httpClient.get(this.urlvoucher + '/' + 4 + '/' + 0, httpOptions);

        }
    }

    getItemsDetail(id) {
        return this.httpClient.get(this.urlvoucher + '/' + id, httpOptions);
    }

    insertvoucherapprovallistItems(voucherapprovallistData): Observable<any> {
        return this.httpClient.post(this.urlvoucher, voucherapprovallistData, httpOptions);
    }

    updateVoucherApproval(voucherapprovallistData): Observable<any> {
        return this.httpClient.put(this.urlvoucherapproval, voucherapprovallistData, httpOptions);
    }
}