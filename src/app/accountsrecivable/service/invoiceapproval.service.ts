import { Injectable } from '@angular/core';
import { HttpClientModule, HttpHeaders, HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({
        'X-Auth-Client': 'qazwsx123',
        'Content-Type': 'application/json'
    })

};

@Injectable()

export class InvoiceapprovalService {
    masterInvoicesurl: string = environment.api.base + environment.api.endPoints.masterInvoices;
    invoiceApprovalurl: string = environment.api.base + environment.api.endPoints.requestInvoiceApprove;
    invoiceApprove: string = environment.api.base + environment.api.endPoints.invoiceApprove;
    requestInvoiceApprove: string = environment.api.base + environment.api.endPoints.requestInvoiceApprove;

    constructor(private httpClient: HttpClient) {
    }
    getItems() {
            return this.httpClient.get(this.masterInvoicesurl+'/'+1 + '/' + 0, httpOptions);
    }
    getItemsDetail(id) {
    }
    updateInvoiceApproval(invoiceapprovalData,type): Observable<any> {
        if (type == 'requestinvoice') {

            return this.httpClient.post(this.requestInvoiceApprove, invoiceapprovalData, httpOptions);
        } else {
            return this.httpClient.post(this.requestInvoiceApprove, invoiceapprovalData, httpOptions);

        }
    }
}