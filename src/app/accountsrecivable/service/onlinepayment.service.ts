import { Injectable } from '@angular/core';
import { HttpClientModule,HttpHeaders, HttpClient} from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({ 'X-Auth-Client': 'qazwsx123',
    'Content-Type': 'application/json' })
    
  };
  
@Injectable()

export class OnlinePaymentService {
    url: string = environment.api.base + environment.api.endPoints.onlineBankPayment;
    constructor(private httpClient: HttpClient) {    
    }
    getItems() {
        return this.httpClient.get(this.url,httpOptions);
    }
    getItemsByGroup(date,bank) {
        return this.httpClient.get(this.url+'/'+bank+'/'+date,httpOptions);
    }

    getItemsDetail(id) {
        return this.httpClient.get(this.url+'/'+id,httpOptions);
    }

    insertPaymentItems(paymentData): Observable<any> {
        return this.httpClient.post(this.url, paymentData,httpOptions);
    }

    updatePaymentItems(paymentData,id): Observable<any> {
        return this.httpClient.put(this.url+'/'+id, paymentData,httpOptions);
       }
}