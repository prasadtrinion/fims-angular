import { Injectable } from '@angular/core';
import { HttpClientModule, HttpHeaders, HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({
        'X-Auth-Client': 'qazwsx123',
        'Content-Type': 'application/json'
    })

};

@Injectable()

export class VoucherapprovalService {

    approvedVouchersurl:string = environment.api.base + environment.api.endPoints.approvedVouchers;
    voucherApproveurl: string = environment.api.base + environment.api.endPoints.voucherapprovallist;
    url: string = environment.api.base + environment.api.endPoints.voucher;


    constructor(private httpClient: HttpClient) {
    }
    getItems() {
            return this.httpClient.get(this.approvedVouchersurl + '/' + 0, httpOptions);
    }
    getItemsDetail(id) {
        return this.httpClient.get(this.url+'/' + id, httpOptions);
    }
    getInvoiceDetailsById(id) {
        return this.httpClient.get(this.approvedVouchersurl+'/'+id,httpOptions);
}

    updateApproval(approvalData): Observable<any> {
            return this.httpClient.post(this.voucherApproveurl, approvalData, httpOptions);
    }


}