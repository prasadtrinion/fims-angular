import { Injectable } from '@angular/core';
import { HttpClientModule, HttpHeaders, HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({
        'X-Auth-Client': 'qazwsx123',
        'Content-Type': 'application/json'
    })

};

@Injectable()

export class DiscountService {
    url: string = environment.api.base + environment.api.endPoints.discountNote;
    urldiscountApproval: string = environment.api.base + environment.api.endPoints.discountApproval;
    urlapproveDiscountNote: string = environment.api.base + environment.api.endPoints.urlapproveDiscountNote;

    discounturlnumbergeneration: string = environment.api.base + environment.api.endPoints.discountNumberGeneration;
    getInvoiceListBasedOnUser: string = environment.api.base + environment.api.endPoints.customerMasterInvoices;
    getlistofDiscountUrl: string = environment.api.base + environment.api.endPoints.studentDiscount

    constructor(private httpClient: HttpClient) {

    }
    getInvoiceItems(data) {
        return this.httpClient.post(this.getInvoiceListBasedOnUser, data, httpOptions);

    }
    getDiscountNoteNumber(data) {
        return this.httpClient.post(this.discounturlnumbergeneration, data, httpOptions);

    }
    getDiscountShows() {

    }

    getItems(id) {

        return this.httpClient.get(this.urldiscountApproval + '/' + id, httpOptions);
    }

    getAllItems() {

        return this.httpClient.get(this.url, httpOptions);
    }
    getDiscountItems() {

        return this.httpClient.get(this.getlistofDiscountUrl, httpOptions);
    }
    getDiscountDetailsItems(id) {

        return this.httpClient.get(this.getlistofDiscountUrl+'/'+id, httpOptions);
    }
    getItemsDetail(id) {

        return this.httpClient.get(this.url + '/' + id, httpOptions);
    }

    insertDiscountItems(discountData): Observable<any> {

        return this.httpClient.post(this.url, discountData, httpOptions);
    }

    updateApproval(discountData): Observable<any> {

        return this.httpClient.put(this.urlapproveDiscountNote, discountData, httpOptions);
    }
}