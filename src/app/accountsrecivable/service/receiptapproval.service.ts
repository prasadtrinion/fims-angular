import { Injectable } from '@angular/core';
import { HttpClientModule, HttpHeaders, HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({
        'X-Auth-Client': 'qazwsx123',
        'Content-Type': 'application/json'
    })

};

@Injectable()

export class ReceiptapprovalService {

    url: string = environment.api.base + environment.api.endPoints.receiptapproval;
    approveurl: string = environment.api.base + environment.api.endPoints.approveReceipt;
    constructor(private httpClient: HttpClient) {
    }
    getItems() {
        return this.httpClient.get(this.url, httpOptions);
    }
    getItemsDetail(id) {
        return this.httpClient.get(this.url + '/' + id, httpOptions);
    }
    getInvoiceDetailsById(id) {
        return this.httpClient.get(this.url + '/' + id, httpOptions);
    }

    updateApproval(approvalData): Observable<any> {
        return this.httpClient.post(this.approveurl, approvalData, httpOptions);
    }


}