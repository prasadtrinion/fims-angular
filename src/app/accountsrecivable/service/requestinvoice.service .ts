import { Injectable } from '@angular/core';
import { HttpClientModule, HttpHeaders, HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({
        'X-Auth-Client': 'qazwsx123',
        'Content-Type': 'application/json'
    })

};

@Injectable()

export class RequestinvoiceService {
    requestInvoiceurl: string = environment.api.base + environment.api.endPoints.requestInvoice;
    requestInvoiceApproveurl: string = environment.api.base + environment.api.endPoints.requestInvoiceApprove;
    deleteurl: string = environment.api.base + environment.api.endPoints.deleteRequestInvoiceDetails;

    constructor(private httpClient: HttpClient) {

    }

    getItems() {
        return this.httpClient.get(this.requestInvoiceurl, httpOptions);
    }
    deleteItems(deleteObject) {
        return this.httpClient.post(this.deleteurl, deleteObject, httpOptions);
    }
    getItemsDetail(id) {
        return this.httpClient.get(this.requestInvoiceurl + '/' + id, httpOptions);
    }

    getApproveItems() {
        return this.httpClient.get(this.requestInvoiceurl + '/' + 1 + '/' + 1, httpOptions);

    }

    getInvoiceDetailsById(id) {
        return this.httpClient.get(this.requestInvoiceurl + '/' + id, httpOptions);
    }

    insertRequestinvoiceItems(requestinvoiceData): Observable<any> {
        return this.httpClient.post(this.requestInvoiceurl, requestinvoiceData, httpOptions);
    }

    updateRequestInvoiceItems(requestinvoiceData, id): Observable<any> {
        return this.httpClient.put(this.requestInvoiceurl + '/' + id, requestinvoiceData, httpOptions);
    }
}