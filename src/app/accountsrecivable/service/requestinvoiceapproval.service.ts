import { Injectable } from '@angular/core';
import { HttpClientModule, HttpHeaders, HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({
        'X-Auth-Client': 'qazwsx123',
        'Content-Type': 'application/json'
    })

};

@Injectable()

export class RequestinvoiceapprovalService {
    requestInvoiceurl: string = environment.api.base + environment.api.endPoints.requestInvoice;
    requestInvoiceApproveurl: string = environment.api.base + environment.api.endPoints.requestInvoiceApprove;


    constructor(private httpClient: HttpClient) {
    }
    getItems() {
            return this.httpClient.get(this.requestInvoiceurl + '/' + 1 + '/' + 0, httpOptions);
    }
    getItemsDetail(id) {
        return this.httpClient.get(this.requestInvoiceurl+'/' + id, httpOptions);
    }
    getInvoiceDetailsById(id) {
        return this.httpClient.get(this.requestInvoiceurl+'/'+id,httpOptions);
}

    updaterequestInvoiceApproval(requestinvoiceapprovalData): Observable<any> {
            return this.httpClient.post(this.requestInvoiceApproveurl, requestinvoiceapprovalData, httpOptions);
    }


}