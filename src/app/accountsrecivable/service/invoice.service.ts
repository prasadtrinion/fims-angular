import { Injectable } from '@angular/core';
import { HttpClientModule, HttpHeaders, HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({
        'X-Auth-Client': 'qazwsx123',
        'Content-Type': 'application/json'
    })

};

@Injectable()

export class InvoiceService {
    requestInvoiceListurl: string = environment.api.base + environment.api.endPoints.masterInvoices;
    masterInvoiceListurl: string = environment.api.base + environment.api.endPoints.masterInvoices;

    constructor(private httpClient: HttpClient) {

    }
    getItems(type) {

        if (type == 'requestinvoice') {
            return this.httpClient.get(this.requestInvoiceListurl + '/' + 1 + '/' + 2, httpOptions);

        } else {
            return this.httpClient.get(this.masterInvoiceListurl + '/' + 1 + '/' + 2, httpOptions);

        }
    }
    getitem() {
        return this.httpClient.get(this.masterInvoiceListurl + '/' + 1 + '/' + 2, httpOptions);
    }
    getItemsDetail(id) {
        return this.httpClient.get(this.requestInvoiceListurl + '/' + id, httpOptions);
    }
    getInvoiceDetailsById(id, type) {

        if (type == '0') {
            return this.httpClient.get(this.requestInvoiceListurl + '/' + id, httpOptions);

        } else {
            return this.httpClient.get(this.masterInvoiceListurl + '/' + id, httpOptions);

        }
    }
    insertinvoiceItems(invoiceData): Observable<any> {
        return this.httpClient.post(this.requestInvoiceListurl, invoiceData, httpOptions);
    }
    updateInvoiceItems(invoiceData): Observable<any> {
        return this.httpClient.put(this.requestInvoiceListurl, invoiceData, httpOptions);
    }
}