import { Injectable } from '@angular/core';
import { HttpClientModule, HttpHeaders, HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({
        'X-Auth-Client': 'qazwsx123',
        'Content-Type': 'application/json'
    })

};

@Injectable()

export class DebtorapprovalService {
    url: string = environment.api.base + environment.api.endPoints.debtor;
    creditorDebtorApproveurl: string = environment.api.base + environment.api.endPoints.creditorDebtorApprove;
    constructor(private httpClient: HttpClient) {

    }

    getItems(type) {
        if (type == 'debtor') {
            return this.httpClient.get(this.url + '/' + 1 + '/' + 0, httpOptions);

        } else {
            return this.httpClient.get(this.url + '/' + 2 + '/' + 0, httpOptions);

        }
    }

    getItemsDetail(id) {
        return this.httpClient.get(this.url + '/' + id, httpOptions);
    }

    insertdebtorapprovallistItems(debtorapprovalData): Observable<any> {
        return this.httpClient.post(this.creditorDebtorApproveurl, debtorapprovalData, httpOptions);
    }

    updatedebtorApproval(debtoreapprovalData): Observable<any> {
        return this.httpClient.post(this.creditorDebtorApproveurl, debtoreapprovalData, httpOptions);
    }

}