import { Injectable } from '@angular/core';
import { HttpClientModule,HttpHeaders, HttpClient} from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({ 'X-Auth-Client': 'qazwsx123',
    'Content-Type': 'application/json' })
    
  };
  
@Injectable()

export class SponsordebitnoteService {
    url: string = environment.api.base + environment.api.endPoints.sponsorDN;
    Debiturl: string = environment.api.base + environment.api.endPoints.debitNote;
    getDebitnoteNumberUrl : string = environment.api.base + environment.api.endPoints.generateNumber;
    sponsorHasBillsUrl : string = environment.api.base + environment.api.endPoints.sponsorHasBillsList;
    getCreditnoteNumberUrl : string = environment.api.base + environment.api.endPoints.generateNumber;
    getInvoiceListBasedOnUser : string = environment.api.base + environment.api.endPoints.customerMasterInvoices;
    constructor(private httpClient: HttpClient) { 
        
    }
    getInvoiceItems(data){
        return this.httpClient.post(this.getInvoiceListBasedOnUser,data,httpOptions);

    }
    getDebitnoteNumber(data) {
        return this.httpClient.post(this.getDebitnoteNumberUrl, data,httpOptions);

    }
    getsponsorHasBillsList(spId){
        return this.httpClient.get(this.sponsorHasBillsUrl+'/'+spId,httpOptions);
    }
    
    getItems() {
        return this.httpClient.get(this.url,httpOptions);
    }

    getItemsDetail(id) {
        return this.httpClient.get(this.url+'/'+id,httpOptions);
    }

    insertSponsordebitItems(sponsordebitData): Observable<any> {
        return this.httpClient.post(this.url, sponsordebitData,httpOptions);
    }

    updateSponsordebitItems(sponsordebitData,id): Observable<any> {
        return this.httpClient.put(this.Debiturl+'/'+id, sponsordebitData,httpOptions);
       }
}