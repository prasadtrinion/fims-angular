    import { Injectable } from '@angular/core';
    import { HttpClientModule,HttpHeaders, HttpClient} from '@angular/common/http';
    import { catchError } from 'rxjs/operators';
    import { HttpModule } from '@angular/http';
    import { Observable } from 'rxjs/Observable';
    import { environment } from '../../../environments/environment';
    const httpOptions = {
        headers: new HttpHeaders({ 'X-Auth-Client': 'qazwsx123',
        'Content-Type': 'application/json' })
        
      };
      
    @Injectable()
    
    export class DebitnoteService {
        url: string = environment.api.base + environment.api.endPoints.debitNote;
        getDebitnoteNumberUrl : string = environment.api.base + environment.api.endPoints.generateNumber;
        getInvoiceListBasedOnUser : string = environment.api.base + environment.api.endPoints.customerMasterInvoices;

    
        constructor(private httpClient: HttpClient) { 
            
        }
        
        getInvoiceItems(data){
            return this.httpClient.post(this.getInvoiceListBasedOnUser,data,httpOptions);
    
        }
        getItems() {
                return this.httpClient.get(this.url,httpOptions);
        }
        
        getDebitnoteNumber(data) {
            return this.httpClient.post(this.getDebitnoteNumberUrl, data,httpOptions);
    
        }
        getItemsDetail(id) {
            return this.httpClient.get(this.url+'/'+id,httpOptions);
        }
    
        insertdebitNoteItems(debitnoteData): Observable<any> {
            return this.httpClient.post(this.url, debitnoteData,httpOptions);
        }
    
        updatedebitNoteItems(debitnoteData,id): Observable<any> {
            return this.httpClient.put(this.url+'/'+id, debitnoteData,httpOptions);
           }
    }