import { Injectable } from '@angular/core';
import { HttpClientModule,HttpHeaders, HttpClient} from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({ 'X-Auth-Client': 'qazwsx123',
    'Content-Type': 'application/json' })
    
  };
  
@Injectable()

export class CreditnoteService {
    url: string = environment.api.base + environment.api.endPoints.creditNote;
    getCreditnoteNumberUrl : string = environment.api.base + environment.api.endPoints.generateNumber;
    getInvoiceListBasedOnUser : string = environment.api.base + environment.api.endPoints.customerMasterInvoices;
    getInvoiceListBasedOnUser1 : string = environment.api.base + environment.api.endPoints.customerMasterInvoices1;
   // approvalurl: string = environment.api.base + environment.api.endPoints.creditNote;

    constructor(private httpClient: HttpClient) { 
        
    }
    
    getInvoiceItems(data){
        return this.httpClient.post(this.getInvoiceListBasedOnUser,data,httpOptions);

    }
    getInvoiceItems1(data){
        return this.httpClient.post(this.getInvoiceListBasedOnUser1,data,httpOptions);
    }
    getItems() {
            return this.httpClient.get(this.url,httpOptions);
    }
    
    getCreditnoteNumber(data) {
        return this.httpClient.post(this.getCreditnoteNumberUrl, data,httpOptions);

    }
    getItemsDetail(id) {
        return this.httpClient.get(this.url+'/'+id,httpOptions);
    }

    insertcreditNoteItems(credintnoteData): Observable<any> {
        return this.httpClient.post(this.url, credintnoteData,httpOptions);
    }

    updatecreditNoteItems(credintnoteData,id): Observable<any> {
        return this.httpClient.put(this.url+'/'+id, credintnoteData,httpOptions);
       }
}