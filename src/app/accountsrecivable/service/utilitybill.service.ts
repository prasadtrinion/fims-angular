import { Injectable } from '@angular/core';
import { HttpClientModule, HttpHeaders, HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({
        'X-Auth-Client': 'qazwsx123',
        'Content-Type': 'application/json'
    })
};

@Injectable()

export class UtilitybillService {
    url: string = environment.api.base + environment.api.endPoints.utilityBill;
    getPremiseCodeurl: string = environment.api.base + environment.api.endPoints.utility;
    urlgenerateUtilityInvoice: string = environment.api.base + environment.api.endPoints.generateUtilityInvoice;
    constructor(private httpClient: HttpClient) {

    }

    generateBillForUtility(utilityData): Observable<any> {
        return this.httpClient.post(this.urlgenerateUtilityInvoice, utilityData, httpOptions);
    }

    getItems() {
        return this.httpClient.get(this.url, httpOptions);
    }

    getPremiseItems(id) {
        return this.httpClient.get(this.getPremiseCodeurl + '/' + id, httpOptions);
    }

    getItemsDetail(id) {
        return this.httpClient.get(this.url + '/' + id, httpOptions);
    }

    insertUtilitybillItems(utilityData): Observable<any> {
        return this.httpClient.post(this.url, utilityData, httpOptions);
    }

    updateUtilitybillItems(utilityData, id): Observable<any> {
        return this.httpClient.put(this.url + '/' + id, utilityData, httpOptions);
    }
}