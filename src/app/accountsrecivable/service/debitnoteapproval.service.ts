import { Injectable } from '@angular/core';
import { HttpClientModule, HttpHeaders, HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({
        'X-Auth-Client': 'qazwsx123',
        'Content-Type': 'application/json'
    })

};

@Injectable()

export class DebitnoteapprovalService {

    debitNote:string = environment.api.base + environment.api.endPoints.debitNote;
    debitNoteurl: string = environment.api.base + environment.api.endPoints.approvedDebitNotes;
    debitNoteApproveurl: string = environment.api.base + environment.api.endPoints.debitNoteApprove;

    constructor(private httpClient: HttpClient) {
    }
    getItems() {
            return this.httpClient.get(this.debitNoteurl + '/' + 0, httpOptions);
    }
    getItemsDetail(id) {
        return this.httpClient.get(this.debitNote+'/' + id, httpOptions);
    }
    getInvoiceDetailsById(id) {
        return this.httpClient.get(this.debitNote+'/'+id,httpOptions);
}

    updateApproval(approvalData): Observable<any> {
            return this.httpClient.post(this.debitNoteApproveurl, approvalData, httpOptions);
    }


}