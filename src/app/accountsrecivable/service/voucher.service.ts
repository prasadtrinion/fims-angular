import { Injectable } from '@angular/core';
import { HttpClientModule,HttpHeaders, HttpClient} from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({ 'X-Auth-Client': 'qazwsx123',
    'Content-Type': 'application/json' })
    
  };
  
@Injectable()

export class VoucherService {
    url: string = environment.api.base + environment.api.endPoints.voucher;
    urlREferenceNumber : string = environment.api.base + environment.api.endPoints.generateNumber;
    constructor(private httpClient: HttpClient) { 
        
    }
    
    getItems(type) {
        if(type=='credit') {
            return this.httpClient.get(this.url+'/'+1+'/'+2,httpOptions);

        } 
        if(type=='debit') {
            return this.httpClient.get(this.url+'/'+2+'/'+2,httpOptions);

        }
        if(type=='cash') {
            return this.httpClient.get(this.url+'/'+3+'/'+2,httpOptions);

        }
        if(type=='payment') {
            return this.httpClient.get(this.url+'/'+4+'/'+2,httpOptions);

        }
    }
    getInvoiceNumber(data) {
        return this.httpClient.post(this.urlREferenceNumber, data,httpOptions);

    }
    getItemsDetail(id) {
        return this.httpClient.get(this.url+'/'+id,httpOptions);
    }

    insertvoucherItems(voucherData): Observable<any> {
        return this.httpClient.post(this.url, voucherData,httpOptions);
    }

    updatevoucherItems(voucherData,id): Observable<any> {
        return this.httpClient.put(this.url+'/'+id, voucherData,httpOptions);
       }
}