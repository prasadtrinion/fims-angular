import { Injectable } from '@angular/core';
import { HttpClientModule, HttpHeaders, HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({
        'X-Auth-Client': 'qazwsx123',
        'Content-Type': 'application/json'
    })

};

@Injectable()

export class MaininvoiceapprovalService {
    masterInvoiceurl: string = environment.api.base + environment.api.endPoints.masterInvoice;
    masterInvoiceApproveurl: string = environment.api.base + environment.api.endPoints.masterInvoiceApprove;

    constructor(private httpClient: HttpClient) {
    }
    getItems() {
        return this.httpClient.get(this.masterInvoiceurl + '/' + 1 + '/' + 0, httpOptions);
    }
    getItemsDetail(id) {
        return this.httpClient.get(this.masterInvoiceurl + '/' + id, httpOptions);
    }
    getInvoiceDetailsById(id) {
        return this.httpClient.get(this.masterInvoiceurl + '/' + id, httpOptions);
    }

    updateInvoiceApproval(invoiceapprovalData): Observable<any> {
        return this.httpClient.post(this.masterInvoiceApproveurl, invoiceapprovalData, httpOptions);
    }
}