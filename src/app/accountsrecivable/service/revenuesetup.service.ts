import { Injectable } from '@angular/core';
import { HttpClientModule,HttpHeaders, HttpClient} from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({ 'X-Auth-Client': 'qazwsx123',
    'Content-Type': 'application/json' })
    
  };
  
@Injectable()

export class RevenuesetupService {
    url: string = environment.api.base + environment.api.endPoints.revenuesetup;
    Approvalurl: string = environment.api.base + environment.api.endPoints.RevenueApproveList;
    RevenueByCategoryurl: string = environment.api.base + environment.api.endPoints.getRevenueByCategory;

    constructor(private httpClient: HttpClient) { 
        
    }
    getActiveItems() {
        return this.httpClient.get(this.Approvalurl,httpOptions);
    }
    getItems() {
        return this.httpClient.get(this.url,httpOptions);
    }

    getItemsDetail(id) {
        return this.httpClient.get(this.url+'/'+id,httpOptions);
    }
    getRevenueByCategoryItems(id) {
        return this.httpClient.get(this.RevenueByCategoryurl+'/'+id,httpOptions);
    }
    insertRevenuesetupItems(revenuesetupData): Observable<any> {
        return this.httpClient.post(this.url, revenuesetupData,httpOptions);
    }

    updateRevenuesetupItems(revenuesetupData,id): Observable<any> {
        return this.httpClient.put(this.url+'/'+id, revenuesetupData,httpOptions);
       }
}