import { Injectable } from '@angular/core';
import { HttpClientModule,HttpHeaders, HttpClient} from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({ 'X-Auth-Client': 'qazwsx123',
    'Content-Type': 'application/json' })
    
  };
  
@Injectable()

export class AdvancepaymentService {
    knockoffurl: string = environment.api.base + environment.api.endPoints.knockoff;
    updateknockoffurl: string = environment.api.base + environment.api.endPoints.paidInvoice;
    advancePaymenturl: string = environment.api.base + environment.api.endPoints.advancePayment;
    studenturl: string = environment.api.base + environment.api.endPoints.studentinvoices;
    constructor(private httpClient: HttpClient) { 
        
    }
    
    getItems() {
        return this.httpClient.get(this.advancePaymenturl,httpOptions);
    }

    getInvoices(id) {
        return this.httpClient.get(this.studenturl+'/'+id,httpOptions);

    }
    updateKnockOff(advancePaymentData): Observable<any> {
        return this.httpClient.post(this.updateknockoffurl, advancePaymentData, httpOptions);
}
    updateBalance(categoriesData): Observable<any> {
        return this.httpClient.post(this.knockoffurl, categoriesData,httpOptions);
       }

    
   
}