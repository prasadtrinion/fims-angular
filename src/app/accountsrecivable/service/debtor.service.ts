import { Injectable } from '@angular/core';
import { HttpClientModule,HttpHeaders, HttpClient} from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({ 'X-Auth-Client': 'qazwsx123',
    'Content-Type': 'application/json' })
    
  };
  
@Injectable()

export class DebtorService {
    urldebtor: string = environment.api.base + environment.api.endPoints.debtor;
    urlcreditorDebtordebtor: string = environment.api.base + environment.api.endPoints.creditorDebtordebtor;

    constructor(private httpClient: HttpClient) { 
        
    }
    
    getItems(type) { 
        if(type=='debtor'){
            return this.httpClient.get(this.urldebtor+'/'+2+'/'+2,httpOptions);
        } else {
            return this.httpClient.get(this.urldebtor+'/'+1+'/'+2,httpOptions);
        }
    }

    getItemsDetail(id) {
        return this.httpClient.get(this.urldebtor+'/'+id,httpOptions);
    }

    insertdebtorItems(debtorData): Observable<any> {
        return this.httpClient.post(this.urlcreditorDebtordebtor, debtorData,httpOptions);
    }

    updatedebtorItems(debtorData,id): Observable<any> {
        return this.httpClient.put(this.urldebtor+'/'+id, debtorData,httpOptions);
       }
}