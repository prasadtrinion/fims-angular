import { Injectable } from '@angular/core';
import { HttpClientModule,HttpHeaders, HttpClient} from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({ 'X-Auth-Client': 'qazwsx123',
    'Content-Type': 'application/json' })
    
  };
  
@Injectable()

export class UtilityService {
    url: string = environment.api.base + environment.api.endPoints.utility;
    urlpremise: string = environment.api.base + environment.api.endPoints.utilityPremise;

    constructor(private httpClient: HttpClient) { 
        
    }
    
    getItems() {
        return this.httpClient.get(this.url,httpOptions);
    }

    getItemsDetail(id) {
        return this.httpClient.get(this.url+'/'+id,httpOptions);
    }

    getUtiltityItems(id) {
        return this.httpClient.get(this.urlpremise+'/'+id,httpOptions);
    }

    insertUtilityItems(utilityData): Observable<any> {
        return this.httpClient.post(this.url, utilityData,httpOptions);
    }

    updateUtilityItems(utilityData,id): Observable<any> {
        return this.httpClient.put(this.url+'/'+id, utilityData,httpOptions);
       }
}