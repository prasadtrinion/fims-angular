import { Injectable } from '@angular/core';
import { HttpClientModule,HttpHeaders, HttpClient} from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({ 'X-Auth-Client': 'qazwsx123',
    'Content-Type': 'application/json' })
    
  };
@Injectable()

export class TempbankfileapprovalService {

     url: string = environment.api.base + environment.api.endPoints.onlineBankPayment
     url1: string = environment.api.base + environment.api.endPoints.onlineBankPaymentByApprove
     approvalPuturl: string = environment.api.base + environment.api.endPoints.onlineBankPaymentApprove
     approveoneurl: string = environment.api.base + environment.api.endPoints.sellrejectOne
     knockoffUrl: string = environment.api.base + environment.api.endPoints.knockOnlinePayments
    constructor(private httpClient: HttpClient) {    
    }
    getItems() {
        return this.httpClient.get(this.url+'/',httpOptions);
    }   
    getItemsDetail() {
        return this.httpClient.get(this.url1+'/0', httpOptions);
    }
    getItemsDetail1() {
        return this.httpClient.get(this.url1+'/1', httpOptions);
    }
    insertTempbankfileapprovalItems(bankfileapprovalData): Observable<any> {
        return this.httpClient.post(this.url, bankfileapprovalData,httpOptions);
    }
    updateTempbankfileapprovalItems(bankfileapprovalData): Observable<any> {
        return this.httpClient.post(this.approvalPuturl, bankfileapprovalData,httpOptions);
    }
    knockoff(bankfileapprovalData): Observable<any> {
        return this.httpClient.post(this.knockoffUrl, bankfileapprovalData,httpOptions);
    }
}