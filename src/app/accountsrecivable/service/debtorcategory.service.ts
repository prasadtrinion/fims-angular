import { Injectable } from '@angular/core';
import { HttpClientModule, HttpHeaders, HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({
        'X-Auth-Client': 'qazwsx123',
        'Content-Type': 'application/json'
    })

};

@Injectable()

export class DebtorCategoryService {
    url: string = environment.api.base + environment.api.endPoints.debtorCategory;
    Activeurl: string = environment.api.base + environment.api.endPoints.getActiveDebtorCategory;

    constructor(private httpClient: HttpClient) {

    }

    getItems() {
        return this.httpClient.get(this.url, httpOptions);
    }
    getActiveItems() {
        return this.httpClient.get(this.Activeurl + '/' + 1, httpOptions);
    }
    getItemsDetail(id) {
        return this.httpClient.get(this.url + '/' + id, httpOptions);
    }

    insertPremiseItems(premiseData): Observable<any> {
        return this.httpClient.post(this.url, premiseData, httpOptions);
    }

    updatePremiseItems(premiseData, id): Observable<any> {
        return this.httpClient.post(this.url + '/' + id, premiseData, httpOptions);
    }
}