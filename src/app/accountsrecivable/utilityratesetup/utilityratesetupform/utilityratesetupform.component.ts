import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { UtilityratesetupService } from '../../service/utilityratesetup.service';
import { GlcodegenerationService } from '../../../generalledger/service/glcodegeneration.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UtilityService } from '../../service/utility.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'UtilityratesetupFormComponent',
    templateUrl: 'utilityratesetupform.component.html'
})

export class UtilityratesetupFormComponent implements OnInit {
    utilityratesetupform: FormGroup;
    utilityList = [];
    utilityData = {};
    utilityrateList = [];
    utilityrateData = {};
    ajaxCount: number;
    id: number;
    currentid:number;
    constructor(
        private UtilityratesetupService:UtilityratesetupService,
        private spinner: NgxSpinnerService,
        private route: ActivatedRoute,
        private router: Router,
        private utilityService:UtilityService
        
    ) { }

    ngDoCheck() {
        if (this.ajaxCount == 0) {
            this.spinner.hide();
        }
    }
    ngOnInit() {
        this.ajaxCount = 0
        this.spinner.show();
        this.utilityrateData['f078fstatus'] = 1;
        this.utilityData['f078fdescription'] = '';

         this.utilityratesetupform = new FormGroup({
            utilityratesetup: new FormControl('', Validators.required),
            
        });
        this.utilityrateData['f078fstatus'] = 1;
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        
        this.UtilityratesetupService.getItems().subscribe(
            data => {
                this.spinner.hide();
                this.utilityrateList = data['result'];
                console.log(this.utilityrateList);
        }, error => {
            console.log(error);
        });
        if (this.id > 0) {
            this.ajaxCount++;
            this.UtilityratesetupService.getItemsDetail(this.id).subscribe(
                data => {
            this.ajaxCount--;                    
                    this.utilityrateData = data['result'][0];
                  
                }, error => {
                    console.log(error);
                });
        }


        console.log(this.id);
    }
    getBills(){
        let f078feffectiveDate = Date.parse(this.utilityrateData['f078feffectiveDate']);
        console.log(this.utilityrateList);
        this.currentid = 0;
        this.utilityrateData['f078fminUnit'] = 1;
      if(this.utilityrateList)
      {
        for(var i=0;i<this.utilityrateList.length;i++) {
            if(Date.parse(this.utilityrateList[i]['f078feffectiveDate'])==Date.parse(this.utilityrateData['f078feffectiveDate'])) {
                console.log(this.currentid);
                console.log(this.utilityrateList[i]['f078fid']);
               if(this.utilityrateList[i]['f078fid']>this.currentid) {
                this.currentid = this.utilityrateList[i]['f078fid'];
                this.utilityrateData['f078fminUnit'] = this.utilityrateList[i]['f078fmaxUnit'] + 1;
               }
            }
        }
      } 
      
    }

    addUtilityratesetup() {
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        console.log(this.utilityrateData);
        if(parseInt(this.utilityrateData['f078fminUnit'])>parseInt(this.utilityrateData['f078fmaxUnit'])){
            alert("Minimum Unit cannot be greater than Maximum Unit");
            return false;
        }
     
        this.utilityrateData['f078fidUtility'] = 14;
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        if (this.id > 0) {
            this.UtilityratesetupService.updateUtilityratesetupItems(this.utilityrateData, this.id).subscribe(
                data => {
                    this.router.navigate(['accountsrecivable/utilityratesetup']);
                    alert("Utility Rate Setup has been Updated Successfully");
                }, error => {
                    console.log(error);
                });


        } else {
            this.UtilityratesetupService.insertUtilityratesetupItems(this.utilityrateData).subscribe(
                data => {
                    this.router.navigate(['accountsrecivable/utilityratesetup']);
                    alert("Utility Rate Setup has been Added Successfully");

                }, error => {
                    console.log(error);
                });

        }

    }
    onlyNumberKey(event) {
        //console.log(event);
        if (event.charCode == 46) {

        } else {
            return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
        }
    }
}