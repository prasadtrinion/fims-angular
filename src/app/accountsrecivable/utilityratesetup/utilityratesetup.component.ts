import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UtilityratesetupService } from '../service/utilityratesetup.service';
import { faSearch, faEdit } from '@fortawesome/free-solid-svg-icons';
import { NgxSpinnerService } from 'ngx-spinner';
@Component({
    selector: 'Utilityratesetup',
    templateUrl: 'utilityratesetup.component.html'
})

export class UtilityratesetupComponent implements OnInit {
    utilityrateList = [];
    utilityrateData = {};
    faSearch = faSearch;
    faEdit = faEdit;
    id: number;

    constructor(
        private UtilityratesetupService: UtilityratesetupService,
        private spinner: NgxSpinnerService,

    ) { }

    ngOnInit() { 
        this.spinner.show();                
        this.UtilityratesetupService.getItems().subscribe(
            data => {
                this.spinner.hide();
                this.utilityrateList = data['result'];
        }, error => {
            console.log(error);
        });
    }

  

}
