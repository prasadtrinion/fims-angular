import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { CustomerService } from '../../../accountsrecivable/service/customer.service';
import { StudentService } from '../../../studentfinance/service/student.service';
import { SponsorService } from '../../../studentfinance/service/sponsor.service';
import { ActivatedRoute, Router } from '@angular/router';
import { CreditnoteService } from '../../service/creditnote.service';
import { MaininvoiceService } from '../../service/maininvoice.service';
import { UserService } from '../../../generalsetup/service/user.service';
import { TaxsetupcodeService } from '../../../generalledger/service/taxsetupcode.service'
import { RevenuesetupService } from '../../service/revenuesetup.service'
import { FeeitemService } from '../../../studentfinance/service/feeitem.service';
import { FundService } from '../../../generalsetup/service/fund.service';
import { AccountcodeService } from '../../../generalsetup/service/accountcode.service'
import { ActivitycodeService } from '../../../generalsetup/service/activitycode.service';
import { DepartmentService } from '../../../generalsetup/service/department.service';
import { StaffService } from "../../service/staff.service";
import { RevenueTypeService } from "../../service/revenuetype.service";
import { CreditnoteapprovalService } from '../../service/creditnoteapproval.service'
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'CreditnoteformComponent',
    templateUrl: 'creditnoteform.component.html',
    styleUrls: ['./creditnoteform.component.css']
})

export class CreditnoteformComponent implements OnInit {
    creditnoteData = {};
    customerList = [];
    studentList = [];
    sponserList = [];
    taxcodeList = [];
    maininvoiceList = [];
    maininvoiceListBasedOnInvoiceId = [];
    maininvoiceData = {};
    customerData = {};
    glcodeList = [];
    glcodeData = {};
    invoicetype = [];
    itemList = [];
    userData = {};
    userList = [];
    creditNotelist = [];
    title = '';
    editId: number;
    openingbalanceData = {};
    newOpeningBalanceList = [];
    saveDataObject = {};
    feeItemList = [];
    id: number;
    smartStaffList = [];
    revenueitemList = [];
    creditnoteApprovalList = [];
    creditnoteApprovalData = {};
    invoiceapprovalData = [];
    revenuetypeList = [];
    showCrReadonly: boolean;
    showDrReadonly: boolean;
    balance: number;
    cnamount: number;
    newbal: number;
    cash: number;
    creditnoteAddedAmount: number;
    payment: number;
    type: string;
    valueDate: string;
    balanceAmount: string;
    ajaxCount: number;
    UserId: number;
    DeptList = [];
    accountcodeList = [];
    fundList = [];
    activitycodeList = [];
    viewDisabled: boolean;
    editDisabled: boolean;
    saveBtnDisable: boolean;
    idview: number;
    constructor(
        private RevenuesetupService: RevenuesetupService,
        private CreditnoteService: CreditnoteService,
        private TaxsetupcodeService: TaxsetupcodeService,
        private StudentService: StudentService,
        private SponsorService: SponsorService,
        private MaininvoiceService: MaininvoiceService,
        private CustomerService: CustomerService,
        private UserService: UserService,
        private FeeitemService: FeeitemService,
        private route: ActivatedRoute,
        private router: Router,
        private DepartmentService: DepartmentService,
        private ActivitycodeService: ActivitycodeService,
        private AccountcodeService: AccountcodeService,
        private FundService: FundService,
        private CreditnoteapprovalService: CreditnoteapprovalService,
        private StaffService: StaffService,
        private RevenueTypeService: RevenueTypeService,
        private spinner: NgxSpinnerService,
    ) { }



    ngDoCheck() {
        const change = this.ajaxCount;
        console.log(this.ajaxCount);
        if (this.ajaxCount == 0) {
            this.editFunction();
            this.spinner.hide();
            this.ajaxCount = 10;
        }
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.route.paramMap.subscribe((data) => this.idview = + data.get('idview'));
        if (this.ajaxCount == 0 && this.creditNotelist.length > 0) {
            this.ajaxCount = 20;
            console.log("asdfsdf");

            if (this.creditNotelist[0]['f031fstatus'] == 1 || this.creditNotelist[0]['f031fstatus'] == 2) {
                this.saveBtnDisable = true;
                this.viewDisabled = true;
                $("#target input,select").prop("disabled", true);
                $("#target1 input,select").prop("disabled", true);
            }
        }
        if (this.ajaxCount == 0 && this.creditNotelist.length > 0) {
            this.ajaxCount = 20;
            if (this.idview > 1) {
                this.saveBtnDisable = true;
                this.viewDisabled = true;
                $("#target input,select").prop("disabled", true);
                $("#target1 input,select").prop("disabled", true);
            }
        }
    }

    editFunction() {
        this.ajaxCount++;
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.route.paramMap.subscribe((data) => this.idview = + data.get('idview'));
        if (this.id > 0) {
            this.ajaxCount--;
            this.CreditnoteService.getItemsDetail(this.id).subscribe(
                data => {
                    this.creditnoteData = data['result'];
                    this.creditNotelist = data['result'];

                    // this.creditnoteData['f031fbalance'] = data['result'][0]['f031fbalance'],
                    this.creditnoteData["f031ftype"] = data['result'][0]['f031ftype'],
                        this.type = data['result'][0]['f031ftype'],
                        this.creditnoteData['f031fdate'] = data['result'][0]['f031fdate'],
                        this.creditnoteData['f031fidInvoice'] = data['result'][0]['f031fidInvoice']
                    this.creditnoteData['f031fdescription'] = data['result'][0]['f031fdescription'],
                        this.creditnoteData['f031freferenceNumber'] = data['result'][0]['f031freferenceNumber'],
                        this.creditnoteData['f031ftotalAmount'] = data['result'][0]['f031ftotalAmount'],
                        this.creditnoteData['f031frevenueType'] = parseInt(data['result'][0]['f031frevenueType']),
                        this.creditnoteData['f031fvoucherNumber'] = data['result'][0]['f031fvoucherNumber'],
                        this.creditnoteData['f031fidCustomer'] = data['result'][0]['f031fidCustomer']
                    var balanceInvoiceAmount = 0;

                    for (var i = 0; i < this.creditNotelist.length; i++) {
                        console.log(this.creditNotelist[i]);
                        var creditnoteObject = {};
                        creditnoteObject['f032fid'] = this.creditNotelist[i]['f032fid'];
                        creditnoteObject['f072fidItem'] = this.creditNotelist[i]['f032fidItem'];
                        creditnoteObject['f072fdebitFundCode'] = this.creditNotelist[i]['f032fcreditFundCode'];
                        creditnoteObject['f072fdebitAccountCode'] = this.creditNotelist[i]['f032fcreditAccountCode'];
                        creditnoteObject['f072fdebitActivityCode'] = this.creditNotelist[i]['f032fcreditActivityCode'];
                        creditnoteObject['f072fdebitDepartmentCode'] = this.creditNotelist[i]['f032fcreditDepartmentCode'];

                        creditnoteObject['f072fcreditFundCode'] = this.creditNotelist[i]['f032fdebitFundCode'];
                        creditnoteObject['f072fcreditAccountCode'] = this.creditNotelist[i]['f032fdebitAccountCode'];
                        creditnoteObject['f072fcreditActivityCode'] = this.creditNotelist[i]['f032fdebitActivityCode'];
                        creditnoteObject['f072fcreditDepartmentCode'] = this.creditNotelist[i]['f032fdebitDepartmentCode'];

                        creditnoteObject['f072fquantity'] = this.creditNotelist[i]['f032fquantity'];
                        creditnoteObject['f072fprice'] = this.creditNotelist[i]['f032fprice'];
                        creditnoteObject['f072fgstCode'] = parseInt(this.creditNotelist[i]['f032fgstCode']);
                        creditnoteObject['f072fgstValue'] = this.creditNotelist[i]['f032fgstValue'];
                        creditnoteObject['f072ftotal'] = this.creditNotelist[i]['f032ftotal'];
                        creditnoteObject['f032fbalanceAmount'] = this.creditNotelist[i]['f032fbalanceAmount'];

                        creditnoteObject['f032ftoCnAmount'] = this.creditNotelist[i]['f032ftoCnAmount'];

                        // balanceInvoiceAmount = (this.ConvertToFloat(this.creditNotelist[i]['f032ftotal'])) - (this.ConvertToFloat(this.creditNotelist[i]['f032ftoCnAmount']));
                        // creditnoteObject['f032fbalanceAmount'] = balanceInvoiceAmount;          

                        this.maininvoiceListBasedOnInvoiceId.push(creditnoteObject);

                    }
                    // var balanceInvoiceAmount = 0;
                    // for (var i = 0; i < this.creditNotelist.length; i++) {
                    //     balanceInvoiceAmount = ((this.ConvertToFloat(this.creditNotelist[i]['f072fgstValue'])) + (this.ConvertToFloat(this.creditNotelist[i]['f072ftotal']))) - (this.ConvertToFloat(this.creditNotelist[i]['f032ftoCnAmount']));
                    //     creditnoteObject['f032ftoCnAmount'] = balanceInvoiceAmount;            
                    // }

                    if (data['result'][0]['f031fstatus'] == 1 || this.creditNotelist[0]['f031fstatus'] == 2) {
                        this.viewDisabled = true;
                        this.editDisabled = true
                        $("#target input,select").prop("disabled", true);
                        $("#target1 input,select").prop("disabled", true);
                    }
                    if (this.idview > 1) {
                        this.saveBtnDisable = true;
                        this.viewDisabled = true;
                        this.editDisabled = true;
                        $("#target input,select").prop("disabled", true);
                        $("#target1 input,select").prop("disabled", true);
                    }
                    if (this.id > 0) {
                        this.editDisabled = true;
                    }
                }, error => {
                    console.log(error);
                });
        }
    }
    ngOnInit() {
        this.ajaxCount = 0
        this.spinner.show();
        this.creditnoteAddedAmount = 0;
        //  this.maininvoiceList=[];
        this.viewDisabled = false;
        this.editDisabled = false;
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        this.route.paramMap.subscribe((data) => this.idview = + data.get('idview'));

        this.valueDate = new Date().toISOString().substr(0, 10);
        this.creditnoteData['f031fdate'] = this.valueDate;
        this.ajaxCount = 0;

        let invoicetypeobj = {};
        invoicetypeobj['name'] = 'Student';
        invoicetypeobj['type'] = 'CSTD';
        this.invoicetype.push(invoicetypeobj);
        invoicetypeobj = {};
        invoicetypeobj['name'] = 'Staff';
        invoicetypeobj['type'] = 'CSTA';

        this.invoicetype.push(invoicetypeobj);

        invoicetypeobj = {};
        invoicetypeobj['name'] = 'Other Receivables';
        invoicetypeobj['type'] = 'COR';

        this.invoicetype.push(invoicetypeobj);

        //Invoice dropdown
        if (this.id > 0) {
            this.ajaxCount++;
            this.MaininvoiceService.getItems().subscribe(
                data => {
                    this.ajaxCount--;
                    this.maininvoiceList = data['result']['data'];
                }
            );
        }
        //Department dropdown
        this.ajaxCount++;
        this.DepartmentService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.DeptList = data['result']['data'];
            }
        );
        //Activity dropdown
        this.ajaxCount++;
        this.ActivitycodeService.getItemsByLevel().subscribe(
            data => {
                this.ajaxCount--;
                this.activitycodeList = data['result']['data'];
            }
        );
        //Account dropdown
        this.ajaxCount++;
        this.AccountcodeService.getItemsByLevel().subscribe(
            data => {
                this.ajaxCount--;
                this.accountcodeList = data['result']['data'];
            }
        );
        //Fund dropdown
        this.ajaxCount++;
        this.FundService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.fundList = data['result']['data'];
            }
        );
        //  staff dropdown
        this.ajaxCount++;
        this.StaffService.getItems().subscribe(
            data => {
                console.log(data);
                this.ajaxCount--;
                this.smartStaffList = data['result']['data'];

                console.log(this.ajaxCount);
            }
        );
        //User dropdown
        this.ajaxCount++;
        this.UserService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.userList = data['result']['data'];
            }
        );
        //Item dropdown
        this.ajaxCount++;
        this.RevenuesetupService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.itemList = data['result'];
            }
        );
        //revenue type dropdown
        this.ajaxCount++;
        this.RevenueTypeService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.revenuetypeList = data['result'];
            }, error => {
                console.log(error);
            });
        //FeeItem Dropdown
        this.ajaxCount++;
        this.FeeitemService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.feeItemList = data['result']['data'];
            }
        );
        //Taxcode dropdown
        this.ajaxCount++;
        this.TaxsetupcodeService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.taxcodeList = data['result']['data'];
            }
        );

        //Student dropdown
        this.ajaxCount++;
        this.StudentService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                console.log(this.ajaxCount);

                this.studentList = data['result']['data'];
            }
        );
        //Sponser dropdown
        this.ajaxCount++;
        this.SponsorService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                console.log(this.ajaxCount);

                this.sponserList = data['result']['data'];
            }
        );

        //Customer dropdown
        this.ajaxCount++;
        this.CustomerService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.customerList = data['result']['data'];
            });
    }
    getType() {
        let typeObject = {};
        typeObject['type'] = this.creditnoteData['f031ftype'];
        this.type = this.creditnoteData['f031ftype']
        console.log(typeObject);
        this.getCreditNoteNumber();
        return this.type;
    }

    // getBalanceAmount() {
    //     var balanceInvoiceAmount = 0;
    //     for (var i = 0; i < this.maininvoiceListBasedOnInvoiceId.length; i++) {
    //         balanceInvoiceAmount = this.ConvertToFloat(this.maininvoiceListBasedOnInvoiceId[i]['f072ftotal']);
    //         this.maininvoiceListBasedOnInvoiceId[i]['f032fbalanceAmount'] = balanceInvoiceAmount;

    //     }
    // }

    getCreditNoteNumber() {

        let typeObject = {};
        typeObject['type'] = this.creditnoteData['f031ftype'];
        this.CreditnoteService.getCreditnoteNumber(typeObject).subscribe(
            newData => {
                console.log(newData);
                this.creditnoteData['f031freferenceNumber'] = newData['number'];
            }
        );
    }
    getInvoiceList() {
        this.ajaxCount = 0;
        //Invoice dropdown
        this.ajaxCount++;
        this.UserId = this.creditnoteData['f031fidCustomer'];
        let invoiceObj = {};
        switch (this.type) {
            case 'CSTA':
                invoiceObj['type'] = "STA";
                break;
            case 'CSTD':
                invoiceObj['type'] = "STD";
                break;
            case 'COR':
                invoiceObj['type'] = "OR";
                break;
        }
        invoiceObj['UserId'] = this.UserId;
        console.log(this.UserId);

        this.CreditnoteService.getInvoiceItems(invoiceObj).subscribe(
            data => {
                this.ajaxCount--;
                console.log(data);

                this.maininvoiceList = data['result']['data'];
                // if(this.maininvoiceList.isEmpty()){
                //     alert("No Invoice");
                // }
            }
        );
    }
    // getglcodeList() {
    //     for (var i = 0; i < this.maininvoiceListBasedOnInvoiceId.length; i++) {
    //         let itemId = this.maininvoiceListBasedOnInvoiceId[i]['f072fidItem'];
    //         // this.ajaxCount++;
    //         this.RevenuesetupService.getItemsDetail(itemId).subscribe(
    //             data => {
    //                 // this.ajaxCount--;
    //                 this.revenueitemList = data['result'][0];
    //             }
    //         );
    //         this.maininvoiceListBasedOnInvoiceId[i]['f072fcreditFundCode'] = this.itemList['f085ffund'];
    //         this.maininvoiceListBasedOnInvoiceId[i]['f072fcreditActivityCode'] = this.itemList['f085factivity'];
    //         this.maininvoiceListBasedOnInvoiceId[i]['f072fcreditDepartmentCode'] = this.itemList['f085fdepartment'];
    //         this.maininvoiceListBasedOnInvoiceId[i]['f072fcreditAccountCode'] = this.itemList['f085faccountId'];
    //     }

    // }
    getInvoiceDetails() {
        let invid = 0;
        invid = this.creditnoteData['f031fidInvoice'];
        this.MaininvoiceService.getInvoiceDetailsById(invid).subscribe(
            data => {
                this.maininvoiceListBasedOnInvoiceId = data['result'];
                this.creditnoteData['f031frevenueType'] = data['result'][0]['f071fbillType'];
                this.creditnoteData['f031fdescription'] = data['result'][0]['f071fdescription'];

                for (var i = 0; i < this.maininvoiceListBasedOnInvoiceId.length; i++) {
                    this.maininvoiceListBasedOnInvoiceId[i]['f072fgstCode'] = parseInt(this.maininvoiceListBasedOnInvoiceId[i]['f072fgstCode']);
                    this.maininvoiceListBasedOnInvoiceId[i]['f072fidItem'] = parseInt(this.maininvoiceListBasedOnInvoiceId[i]['f072fidItem']);
                    // this.getBalanceAmount();
                    this.maininvoiceListBasedOnInvoiceId[i]['f032fbalanceAmount'] = this.ConvertToFloat(this.maininvoiceListBasedOnInvoiceId[i]['f072fbalanceAmount']).toFixed(2);
                }
                this.creditnoteData['f031ftotalAmount'] = this.ConvertToFloat(data['result'][0]['f071finvoiceTotal']).toFixed(2);
                this.creditnoteData['invoiceDate'] = data['result'][0]['f071finvoiceDate'];
            }
        );
    }

    addCreditAmount() {
        this.creditnoteAddedAmount = 0;
        this.creditnoteData['f032ftoCnAmount'] = 0;
        for (var i = 0; i < this.maininvoiceListBasedOnInvoiceId.length; i++) {
            this.creditnoteAddedAmount = this.ConvertToFloat(this.creditnoteAddedAmount) + this.ConvertToFloat(this.maininvoiceListBasedOnInvoiceId[i]['f032ftoCnAmount']);

            if (this.creditnoteAddedAmount > this.creditnoteData['f031fbalance']) {
            }
        }
        this.creditnoteData['f031fbalance'] = this.ConvertToFloat(this.creditnoteData['f031ftotalAmount']) - this.ConvertToFloat(this.creditnoteAddedAmount);
    }

    saveData() {
        if (this.creditnoteData['f031ftype'] == undefined) {
            alert('Select Customer Type');
            return false;
        }
        if (this.creditnoteData['f031fidCustomer'] == undefined) {
            if (this.type == 'CSTD') {
                alert("Select Student Name")
                return false;
            }
            if (this.type == 'COR') {
                alert("Select Debtor Name")
                return false;
            }
            if (this.type == 'CSTA') {
                alert("Select Staff")
                return false;
            }
        }
        if (this.creditnoteData['f031fidInvoice'] == undefined) {
            alert('Select Invoice Number');
            return false;
        }
        if (this.creditnoteData['f031fdescription'] == undefined) {
            alert('Enter Description');
            return false;
        }
        for (var i = 0; i < this.maininvoiceListBasedOnInvoiceId.length; i++) {
            if (this.maininvoiceListBasedOnInvoiceId[i]['f032ftoCnAmount'] == undefined) {
                alert('Enter Credit Note Amount(RM)');
                return false;
            }
            if (this.maininvoiceListBasedOnInvoiceId[i]['f072ftotal'] < this.maininvoiceListBasedOnInvoiceId[i]['f032ftoCnAmount']) {
                alert('Credit Note amount should not greater than total invoice');
                return false;
            }
            // if (this.maininvoiceListBasedOnInvoiceId[i]['f032ftoCnAmount'] > this.maininvoiceListBasedOnInvoiceId[i]['f032fbalanceAmount']) {
            //     alert('Credit Note amount should not greater than Balance');
            //     return false;
            // }
        }
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.saveDataObject = {};
        if (this.id > 0) {
            this.saveDataObject['f031fid'] = this.id;
        }
        var saveDetails = [];
        this.saveDataObject["f031ftype"] = this.creditnoteData['f031ftype'];
        this.saveDataObject["f031fidInvoice"] = this.creditnoteData['f031fidInvoice'];
        this.saveDataObject["f031ftotalAmount"] = this.ConvertToFloat(this.creditnoteData['f031ftotalAmount']).toFixed(2);
        this.saveDataObject["f031freferenceNumber"] = this.creditnoteData['f031freferenceNumber'];
        this.saveDataObject["f031fdate"] = this.creditnoteData['f031fdate'];
        this.saveDataObject["f031fvoucherNumber"] = this.creditnoteData['f031fvoucherNumber'];
        this.saveDataObject["f031fnarration"] = this.creditnoteData['f031fnarration'];
        this.saveDataObject["f031fdescription"] = this.creditnoteData['f031fdescription'];
        this.saveDataObject["f031fidCustomer"] = this.creditnoteData['f031fidCustomer'];
        this.saveDataObject["f031frevenueType"] = this.creditnoteData['f031frevenueType'];
        this.saveDataObject["f031fstatus"] = 0;
        for (var i = 0; i < this.maininvoiceListBasedOnInvoiceId.length; i++) {
            var creditnoteObject = {}

            creditnoteObject['f032fid'] = this.maininvoiceListBasedOnInvoiceId[i]['f032fid'];
            creditnoteObject['f032fidInvoiceDetail'] = this.maininvoiceListBasedOnInvoiceId[i]['f072fid'];
            creditnoteObject['f032fidItem'] = this.maininvoiceListBasedOnInvoiceId[i]['f072fidItem'];
            creditnoteObject['f032fdebitFundCode'] = this.maininvoiceListBasedOnInvoiceId[i]['f072fdebitFundCode'];
            creditnoteObject['f032fdebitAccountCode'] = this.maininvoiceListBasedOnInvoiceId[i]['f072fdebitAccountCode'];
            creditnoteObject['f032fdebitActivityCode'] = this.maininvoiceListBasedOnInvoiceId[i]['f072fdebitActivityCode'];
            creditnoteObject['f032fdebitDepartmentCode'] = this.maininvoiceListBasedOnInvoiceId[i]['f072fdebitDepartmentCode'];

            creditnoteObject['f032fcreditFundCode'] = this.maininvoiceListBasedOnInvoiceId[i]['f072fcreditFundCode'];
            creditnoteObject['f032fcreditAccountCode'] = this.maininvoiceListBasedOnInvoiceId[i]['f072fcreditAccountCode'];
            creditnoteObject['f032fcreditActivityCode'] = this.maininvoiceListBasedOnInvoiceId[i]['f072fcreditActivityCode'];
            creditnoteObject['f032fcreditDepartmentCode'] = this.maininvoiceListBasedOnInvoiceId[i]['f072fcreditDepartmentCode'];
            creditnoteObject['f032fquantity'] = this.maininvoiceListBasedOnInvoiceId[i]['f072fquantity'];
            creditnoteObject['f032fprice'] = this.ConvertToFloat(this.maininvoiceListBasedOnInvoiceId[i]['f072fprice']).toFixed(2);
            creditnoteObject['f032fgstCode'] = this.maininvoiceListBasedOnInvoiceId[i]['f072fgstCode'];
            creditnoteObject['f032fgstValue'] = this.maininvoiceListBasedOnInvoiceId[i]['f072fgstValue'];
            creditnoteObject['f032ftotal'] = this.ConvertToFloat(this.maininvoiceListBasedOnInvoiceId[i]['f072ftotal']).toFixed(2);
            creditnoteObject['f032ftoCnAmount'] = this.ConvertToFloat(this.maininvoiceListBasedOnInvoiceId[i]['f032ftoCnAmount']).toFixed(2);
            creditnoteObject['f032fbalanceAmount'] = this.ConvertToFloat(this.maininvoiceListBasedOnInvoiceId[i]['f032fbalanceAmount'] - this.maininvoiceListBasedOnInvoiceId[i]['f032ftoCnAmount']).toFixed(2);
            // creditnoteObject['balanceAmount'] = this.saveDataObject['f031fbalance'];
            saveDetails.push(creditnoteObject);
            // this.maininvoiceListBasedOnInvoiceId[i]['f032fidItem'] = parseInt(this.maininvoiceListBasedOnInvoiceId[i]['f032fidItem']).toString();

        }


        this.saveDataObject["details"] = saveDetails;
        console.log(this.saveDataObject);
        if (this.id > 0) {
            this.CreditnoteService.insertcreditNoteItems(this.saveDataObject).subscribe(
                data => {
                    this.creditnoteData = data['result'];

                    this.router.navigate(['/accountsrecivable/creditnote']);
                    alert("Credit Note Entry has been Updated Succcessfully");

                    console.log(this.creditnoteData);
                }, error => {
                    console.log(error);
                });
        } else {
            this.CreditnoteService.insertcreditNoteItems(this.saveDataObject).subscribe(
                data => {
                    this.creditnoteData = data['result'];

                    this.router.navigate(['/accountsrecivable/creditnote']);
                    alert("Credit Note Entry has been Added Succcessfully");

                    console.log(this.creditnoteData);
                }, error => {
                    console.log(error);
                });
        }
    }
    ConvertToFloat(val) {
        return parseFloat(val);
    }
    onlyNumberKey(event) {
        //console.log(event);
        if (event.charCode == 46) {

        } else {
            return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
        }
    }
    maximize() {
        $("#myModal").addClass("modal");
        $("#overlay1").show();
        $(".maximize-icon").addClass("hidden");
        $(".minimize-icon").removeClass("hidden");
        $(".modal-backdrop").show();

    }
    minimize() {
        $("#myModal").removeClass("modal");
        $("#overlay1").hide();
        $(".maximize-icon").removeClass("hidden");
        $(".minimize-icon").addClass("hidden");
        $(".modal-backdrop").hide();
        $('#myModal').show();

    }
    getList() {
        this.spinner.show();
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        this.CreditnoteapprovalService.getItems().subscribe(
            data => {
                this.spinner.hide();
                this.creditnoteApprovalList = data['result'];
            }, error => {
                console.log(error);
            });
    }
    ApprovalListData() {
        let approveDarrayList = [];
        for (var i = 0; i < this.creditnoteApprovalList.length; i++) {
            if (this.creditnoteApprovalList[i].f031fstatus == true) {
                approveDarrayList.push(this.creditnoteApprovalList[i].f031fid);
            }
        }
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        approveDarrayList.push(this.id);
        var confirmPop = confirm("Do you want to Approve?");
        if (confirmPop == false) {
            return false;
        }
        var approvalObject = {};
        approvalObject['id'] = approveDarrayList;
        approvalObject['status'] = '1';
        approvalObject['reason'] = '';

        this.CreditnoteapprovalService.updateApproval(approvalObject).subscribe(
            data => {
                this.getList();
                alert("Approved Successfully");
                this.router.navigate(['/accountsrecivable/creditnoteapproval']);
                this.invoiceapprovalData['reason'] = '';
            }, error => {
                console.log(error);
            });

    }
    regectJournalApprovalListData() {
        let approveDarrayList = [];
        for (var i = 0; i < this.creditnoteApprovalList.length; i++) {
            if (this.creditnoteApprovalList[i].f031fstatus == true) {
                approveDarrayList.push(this.creditnoteApprovalList[i].f031fid);
            }
        }
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        approveDarrayList.push(this.id);

        if (this.invoiceapprovalData['reason'] == undefined) {
            alert('Enter the Remarks');
            return false;
        }
        var confirmPop = confirm("Do you want to Reject?");
        if (confirmPop == false) {
            return false;
        }
        var creditnoteDataObject = {}
        creditnoteDataObject['id'] = approveDarrayList;
        creditnoteDataObject['status'] = '2';
        creditnoteDataObject['reason'] = this.invoiceapprovalData['reason'];
        this.CreditnoteapprovalService.updateApproval(creditnoteDataObject).subscribe(
            data => {
                this.getList();
                this.router.navigate(['/accountsrecivable/creditnoteapproval']);
                this.invoiceapprovalData['reason'] = '';
                alert("Rejected Successfully");
            }, error => {
                console.log(error);
            });
    }
}