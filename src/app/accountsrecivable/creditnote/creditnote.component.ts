import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CreditnoteService } from '../service/creditnote.service';
import { faSearch, faEdit } from '@fortawesome/free-solid-svg-icons';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'CreditnoteComponent',
    templateUrl: 'creditnote.component.html'
})

export class CreditnoteComponent implements OnInit {
    creditnoteList = [];
    creditnoteData = {};
    id: number;
    title = '';
    faSearch = faSearch;
    faEdit = faEdit;

    constructor(

        private CreditnoteService: CreditnoteService,
        private route: ActivatedRoute,
        private spinner: NgxSpinnerService,
        private router: Router
    ) { }

    ngOnInit() {

        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        console.log(this.id);
        this.spinner.show();
        if (this.id == 0) {
            this.CreditnoteService.getItems().subscribe(
                data => {
                    this.spinner.hide();
                    let activityData = [];
                    activityData = data['data'];
                    for (var i = 0; i < activityData.length; i++) {
                        if (activityData[i]['f031fstatus'] == 0) {
                            activityData[i]['f031fstatus'] = 'Pending';
                        } else if (activityData[i]['f031fstatus'] == 1) {
                            activityData[i]['f031fstatus'] = 'Approved';
                        } else {
                            activityData[i]['f031fstatus'] = 'Rejected';
                        }
                        if (activityData[i]['f031ftype'] == 'CSTA') {
                            activityData[i]['f031ftype'] = 'Staff';

                        } else if (activityData[i]['f031ftype'] == 'CSTD') {
                            activityData[i]['f031ftype'] = 'Student';
                        }
                        else {
                            if (activityData[i]['f031ftype'] == 'COR') {
                                activityData[i]['f031ftype'] = 'Other Receivables';
                            }
                        }
                    }
                    console.log(this.creditnoteData);
                    this.creditnoteList = activityData;
                }, error => {
                    console.log(error);
                });
        }
    }
    getName(name) {
        var text = '';
        switch (name) {
            case 'CSTD': text = "Student"; break;
            case 'CSTA': text = "Staff"; break;
            case 'COR': text = "Other Recevables"; break;
        }
        return text;
    }
}