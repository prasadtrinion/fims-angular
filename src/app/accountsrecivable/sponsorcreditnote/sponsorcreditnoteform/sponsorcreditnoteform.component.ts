
import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { CustomerService } from '../../../accountsrecivable/service/customer.service';
import { StudentService } from '../../../studentfinance/service/student.service';
import { ActivatedRoute, Router } from '@angular/router';
import { SponsorService } from "../../../studentfinance/service/sponsor.service";
import { ItemService } from '../../service/item.service';
import { MaininvoiceService } from '../../service/maininvoice.service';
import { GlcodegenerationService } from '../../../generalledger/service/glcodegeneration.service';
import { UserService } from '../../../generalsetup/service/user.service';
import { TaxsetupcodeService } from '../../../generalledger/service/taxsetupcode.service'
import { isEmpty } from 'rxjs/operators';
import { RevenuesetupService } from '../../service/revenuesetup.service'
import { FeeitemService } from '../../../studentfinance/service/feeitem.service';
import { FundService } from '../../../generalsetup/service/fund.service';
import { AccountcodeService } from '../../../generalsetup/service/accountcode.service'
import { ActivitycodeService } from '../../../generalsetup/service/activitycode.service';
import { DepartmentService } from '../../../generalsetup/service/department.service';
import { SponsorcreditnoteService } from "../../service/sponsorcreditnote.service";
import { StaffService } from "../../service/staff.service";
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'SponsorcreditnoteFormComponent',
    templateUrl: 'sponsorcreditnoteform.component.html',
    // styleUrls: ['./sponsorcreditnoteform.component.css']
})

export class SponsorcreditnoteFormComponent implements OnInit {
    creditnoteData = {};
    customerList = [];
    studentList = [];
    sponserList = [];
    taxcodeList = [];
    maininvoiceList = [];
    maininvoiceListBasedOnInvoiceId = [];
    maininvoiceData = {};
    customerData = {};
    glcodeList = [];
    glcodeData = {};
    invoicetype = [];
    itemList = [];
    userData = {};
    userList = [];
    creditNotelist = [];
    title = '';
    editId: number;
    openingbalanceData = {};
    newOpeningBalanceList = [];
    saveDataObject = {};
    feeItemList = [];
    id: number;
    smartStaffList = [];
    showCrReadonly: boolean;
    showDrReadonly: boolean;
    balance: number;
    cnamount: number;
    newbal: number;
    cash: number;
    creditnoteAddedAmount: number;
    payment: number;
    type: string;
    valueDate: string;
    ajaxCount: number;
    UserId: number;
    DeptList = [];
    accountcodeList = [];
    fundList = [];
    activitycodeList = [];
    viewDisabled: boolean;
    editDisabled: boolean;
    saveBtnDisable: boolean;
    idview: number;
    constructor(

        private GlcodegenerationService: GlcodegenerationService,
        private RevenuesetupService: RevenuesetupService,
        private SponsorcreditnoteService: SponsorcreditnoteService,
        private TaxsetupcodeService: TaxsetupcodeService,
        private ItemService: ItemService,
        private StudentService: StudentService,
        private SponsorService: SponsorService,
        private MaininvoiceService: MaininvoiceService,
        private CustomerService: CustomerService,
        private UserService: UserService,
        private FeeitemService: FeeitemService,
        private route: ActivatedRoute,
        private router: Router,
        private DepartmentService: DepartmentService,
        private ActivitycodeService: ActivitycodeService,
        private AccountcodeService: AccountcodeService,
        private FundService: FundService,
        private StaffService: StaffService,
        private spinner: NgxSpinnerService,
    ) { }



    ngDoCheck() {
        const change = this.ajaxCount;
        console.log(this.ajaxCount);
        // if (this.ajaxCount == 0) {
        //     this.ajaxCount = 10;
        // }
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.route.paramMap.subscribe((data) => this.idview = + data.get('idview'));
        if (this.ajaxCount == 0 && this.creditNotelist.length > 0) {
            this.ajaxCount = 20;
            this.spinner.hide();
            // console.log("asdfsdf");
            // this.showIcons();
            //console.log(this.newbudgetactivitiesList[0]);
            if (this.creditNotelist[0]['f061fstatus'] == 1 || this.creditNotelist[0]['f061fstatus'] == 2) {
                this.saveBtnDisable = true;
                this.viewDisabled = true;
                $("#target input,select").prop("disabled", true);
                $("#target1 input,select").prop("disabled", true);
            }
        }
        if (this.ajaxCount == 0 && this.creditNotelist.length > 0) {
            this.ajaxCount = 20;
            if (this.idview > 1) {
                this.saveBtnDisable = true;
                this.viewDisabled = true;
                $("#target input,select").prop("disabled", true);
                $("#target1 input,select").prop("disabled", true);
            }
        }
    }

    editFunction() {
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.route.paramMap.subscribe((data) => this.idview = + data.get('idview'));
        if (this.id > 0) {
            this.ajaxCount++;
            this.SponsorcreditnoteService.getItemsDetail(this.id).subscribe(
                data => {
                    this.ajaxCount--;
                    this.creditnoteData = data['result'];
                    this.creditNotelist = data['result'];

                    // this.creditnoteData['f061fbalance'] = data['result'][0]['f071fbalance'],
                    this.creditnoteData["f061ftype"] = data['result'][0]['f061ftype'],
                        this.type = data['result'][0]['f061ftype'],
                        this.creditnoteData['f061fdate'] = data['result'][0]['f061fdate'],
                        this.creditnoteData['f061fidInvoice'] = data['result'][0]['f061fidInvoice']
                    this.creditnoteData['f061fdescription'] = data['result'][0]['f061fdescription'],
                        // this.creditnoteData['f031fnarration'] = data['result'][0]['f031fdescription'],
                        this.creditnoteData['f061freferenceNumber'] = data['result'][0]['f061freferenceNumber'],
                        this.creditnoteData['f061ftotalAmount'] = data['result'][0]['f061ftotalAmount'],
                        this.creditnoteData['f061fvoucherNumber'] = data['result'][0]['f061fvoucherNumber'],
                        this.creditnoteData['f061fidCustomer'] = data['result'][0]['f061fidCustomer']
                    this.getInvoiceList();

                    for (var i = 0; i < this.creditNotelist.length; i++) {
                        // console.log(this.creditNotelist[i]);
                        var creditnoteObject = {};
                        creditnoteObject['f062fid'] = this.creditNotelist[i]['f062fid'];
                        creditnoteObject['f072fidItem'] = this.creditNotelist[i]['f062fidItem'];
                        creditnoteObject['f072fdebitFundCode'] = this.creditNotelist[i]['f062fdebitFundCode'];
                        creditnoteObject['f072fdebitActivityCode'] = this.creditNotelist[i]['f062fdebitActivityCode'];
                        creditnoteObject['f072fdebitDepartmentCode'] = this.creditNotelist[i]['f062fdebitDepartmentCode'];
                        creditnoteObject['f072fdebitAccountCode'] = this.creditNotelist[i]['f062fdebitAccountCode'];
                        creditnoteObject['f072fcreditFundCode'] = this.creditNotelist[i]['f062fcreditFundCode'];
                        creditnoteObject['f072fcreditActivityCode'] = this.creditNotelist[i]['f062fcreditActivityCode'];
                        creditnoteObject['f072fcreditDepartmentCode'] = this.creditNotelist[i]['f062fcreditDepartmentCode'];
                        creditnoteObject['f072fcreditAccountCode'] = this.creditNotelist[i]['f062fcreditAccountCode'];
                        creditnoteObject['f072fquantity'] = this.creditNotelist[i]['f062fquantity'];
                        creditnoteObject['f072fprice'] = this.creditNotelist[i]['f062fprice'];
                        creditnoteObject['f072fgstCode'] = parseInt(this.creditNotelist[i]['f062fgstCode']);
                        creditnoteObject['f072fgstValue'] = this.creditNotelist[i]['f062fgstValue'];
                        creditnoteObject['f072ftotal'] = this.creditNotelist[i]['f062ftotal'];
                        creditnoteObject['f072ftaxAmount'] = this.creditNotelist[i]['f062ftaxAmount'];
                        creditnoteObject['f072ftotalExc'] = this.creditNotelist[i]['f062ftotalExc'];
                        creditnoteObject['f062ftoCnAmount'] = this.creditNotelist[i]['f062ftoCnAmount'];

                        this.maininvoiceListBasedOnInvoiceId.push(creditnoteObject);

                    }
                    console.log('In Edit');
                    console.log(this.maininvoiceListBasedOnInvoiceId);
                    if (data['result'][0]['f061fstatus'] == 1 || this.creditNotelist[0]['f061fstatus'] == 2) {
                        this.viewDisabled = true;
                        this.editDisabled = true
                        $("#target input,select").prop("disabled", true);
                        $("#target1 input,select").prop("disabled", true);
                    }
                    if (this.idview > 1) {
                        this.saveBtnDisable = true;
                        this.viewDisabled = true;
                        this.editDisabled = true;
                        $("#target input,select").prop("disabled", true);
                        $("#target1 input,select").prop("disabled", true);
                    }
                    if (this.id > 0) {
                        this.editDisabled = true;
                    }
                }, error => {
                    console.log(error);
                });
        }
    }


    ngOnInit() {
        this.ajaxCount = 0
        this.spinner.show();
        this.creditnoteAddedAmount = 0;
        //  this.maininvoiceList=[];
        this.viewDisabled = false;
        this.editDisabled = false;
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        this.route.paramMap.subscribe((data) => this.idview = + data.get('idview'));

        this.valueDate = new Date().toISOString().substr(0, 10);
        console.log(this.valueDate);
        this.creditnoteData['f061fdate'] = this.valueDate;
        this.ajaxCount = 0;
        console.log(this.id);
        this.editFunction();

        let invoicetypeobj = {};
        invoicetypeobj['name'] = 'Sponser';
        invoicetypeobj['type'] = 'CSP';
        this.invoicetype.push(invoicetypeobj);
        this.creditnoteData['f061ftype'] = invoicetypeobj['name'];
        //Invoice dropdown
        if (this.id > 0) {
            this.ajaxCount++;
            this.MaininvoiceService.getItems().subscribe(
                data => {
                    this.ajaxCount--;
                    this.maininvoiceList = data['result']['data'];
                }
            );
        }
        //Department dropdown
        this.ajaxCount++;
        this.DepartmentService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.DeptList = data['result']['data'];
            }
        );


        //Activity dropdown
        this.ajaxCount++;
        this.ActivitycodeService.getItemsByLevel().subscribe(
            data => {
                this.ajaxCount--;
                this.activitycodeList = data['result']['data'];
            }
        );


        //Account dropdown
        this.ajaxCount++;
        this.AccountcodeService.getItemsByLevel().subscribe(
            data => {
                this.ajaxCount--;
                this.accountcodeList = data['result']['data'];
            }
        );



        //Fund dropdown
        this.ajaxCount++;
        this.FundService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.fundList = data['result']['data'];
            }
        );
        //  staff dropdown
        this.ajaxCount++;
        this.StaffService.getItems().subscribe(
            data => {
                console.log(data);
                this.ajaxCount--;
                this.smartStaffList = data['result']['data'];

                // console.log(this.ajaxCount);
            }
        );
        //User dropdown
        this.ajaxCount++;
        this.UserService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.userList = data['result']['data'];
            }
        );

        //Item dropdown
        this.ajaxCount++;
        this.RevenuesetupService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.itemList = data['result'];
            }
        );
        //FeeItem Dropdown
        this.ajaxCount++;
        this.FeeitemService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.feeItemList = data['result']['data'];
            }
        );
        //Taxcode dropdown
        this.ajaxCount++;
        this.TaxsetupcodeService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.taxcodeList = data['result']['data'];
            }
        );

        //Student dropdown
        this.ajaxCount++;
        this.StudentService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                console.log(this.ajaxCount);

                this.studentList = data['result']['data'];
            }
        );

        //Sponser dropdown
        this.ajaxCount++;
        this.SponsorService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                // console.log(this.ajaxCount);

                this.sponserList = data['result']['data'];
            }
        );

        //Customer dropdown
        this.ajaxCount++;
        this.CustomerService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.customerList = data['result']['data'];
            }
        );
        this.spinner.hide();

    }

    getType() {
        let typeObject = {};
        typeObject['type'] = this.creditnoteData['f061ftype'];
        this.type = this.creditnoteData['f061ftype']
        // console.log(typeObject);
        this.getCreditNoteNumber();
        return this.type;
    }

    getCreditNoteNumber() {

        let typeObject = {};
        typeObject['type'] = this.creditnoteData['f061ftype'];
        this.SponsorcreditnoteService.getCreditnoteNumber(typeObject).subscribe(
            newData => {
                // console.log(newData);
                this.creditnoteData['f061freferenceNumber'] = newData['number'];
            }
        );
    }

    getInvoiceList() {

        this.ajaxCount = 0;
        //Invoice dropdown
        this.ajaxCount++;
        this.UserId = this.creditnoteData['f061fidCustomer'];
        let invoiceObj = {};
        invoiceObj['type'] = "SP";
        invoiceObj['UserId'] = this.UserId;
        console.log(invoiceObj);

        this.SponsorcreditnoteService.getInvoiceItems(invoiceObj).subscribe(
            data => {
                this.ajaxCount--;
                // console.log(data);
                this.maininvoiceList = data['result']['data'];
            }
        );
    }

    getInvoiceDetails() {
        let invid = 0;
        invid = this.creditnoteData['f061fidInvoice'];
        this.MaininvoiceService.getInvoiceDetailsById(invid).subscribe(
            data => {
                this.maininvoiceListBasedOnInvoiceId = data['result'];
                for (var i = 0; i < this.maininvoiceListBasedOnInvoiceId.length; i++) {
                    this.maininvoiceListBasedOnInvoiceId[i]['f072fgstCode'] = parseInt(this.maininvoiceListBasedOnInvoiceId[i]['f072fgstCode']);
                }
                // console.log(this.maininvoiceListBasedOnInvoiceId);
                // this.creditnoteData['f031fidCustomer'] = this.ConvertToFloat(data['result'][0]['f071fidCustomer']).toFixed(2);
                this.creditnoteData['f061ftotalAmount'] = this.ConvertToFloat(data['result'][0]['f071finvoiceTotal']).toFixed(2);
                this.creditnoteData['invoiceDate'] = data['result'][0]['f072finvoiceDate'];
                this.creditnoteData['f061fbalance'] = this.ConvertToFloat(data['result'][0]['f071fbalance']).toFixed(2);
                // this.creditnoteData['f031fdescription'] = this.ConvertToFloat(data['result'][0]['f071fdescription']).toFixed(2);
            }
        );
    }

    addCreditAmount() {
        this.creditnoteAddedAmount = 0;
        this.creditnoteData['f062ftoCnAmount'] = 0;
        for (var i = 0; i < this.maininvoiceListBasedOnInvoiceId.length; i++) {
            this.creditnoteAddedAmount = this.ConvertToFloat(this.creditnoteAddedAmount) + this.ConvertToFloat(this.maininvoiceListBasedOnInvoiceId[i]['f062ftoCnAmount']);

            if (this.creditnoteAddedAmount > this.creditnoteData['f061fbalance']) {
            }

        }

        this.creditnoteData['f061fbalance'] = this.ConvertToFloat(this.creditnoteData['f061ftotalAmount']) - this.ConvertToFloat(this.creditnoteAddedAmount);

    }

    saveData() {
        if (this.creditnoteData['f061ftype'] == undefined) {
            alert('Select Invoice Type');
            return false;
        }
        if (this.creditnoteData['f061fidCustomer'] == undefined) {
            alert("Select Sponsor")
            return false;
        }
        if (this.creditnoteData['f061fidInvoice'] == undefined) {
            alert('Select Invoice Number');
            return false;
        }
        if (this.creditnoteData['f061fdescription'] == undefined) {
            alert('Enter Description');
            return false;
        }
        // if (this.creditnoteData['f062ftoCnAmount'] == undefined) {
        //     alert('Enter Amount(RM)');
        //     return false;
        // }
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.saveDataObject = {};
        if (this.id > 0) {
            this.saveDataObject['f061fid'] = this.id;
        }
        var saveDetails = [];
        this.saveDataObject["f061ftype"] = this.creditnoteData['f061ftype'];
        this.saveDataObject["f061fidInvoice"] = this.creditnoteData['f061fidInvoice'];
        this.saveDataObject["f061ftotalAmount"] = this.ConvertToFloat(this.creditnoteData['f061ftotalAmount']).toFixed(2);
        //this.saveDataObject["f031fbalance"] = this.creditnoteData['f031fbalance'];
        this.saveDataObject["f061freferenceNumber"] = this.creditnoteData['f061freferenceNumber'];
        this.saveDataObject["f061fdate"] = this.creditnoteData['f061fdate'];
        this.saveDataObject["f061fvoucherNumber"] = this.creditnoteData['f061fvoucherNumber'];
        this.saveDataObject["f061fnarration"] = this.creditnoteData['f061fnarration'];
        this.saveDataObject["f061fdescription"] = this.creditnoteData['f061fdescription'];
        this.saveDataObject["f061fidCustomer"] = this.creditnoteData['f061fidCustomer'];
        this.saveDataObject["f061fstatus"] = 0;
        this.saveDataObject['f061ftype'] = 'CSP';
        for (var i = 0; i < this.maininvoiceListBasedOnInvoiceId.length; i++) {
            var creditnoteObject = {}

            creditnoteObject['f062fid'] = this.maininvoiceListBasedOnInvoiceId[i]['f062fid'];

            creditnoteObject['f062fidItem'] = this.maininvoiceListBasedOnInvoiceId[i]['f072fidItem'];
            creditnoteObject['f062fdebitFundCode'] = this.maininvoiceListBasedOnInvoiceId[i]['f072fdebitFundCode'];
            creditnoteObject['f062fdebitAccountCode'] = this.maininvoiceListBasedOnInvoiceId[i]['f072fdebitAccountCode'];
            creditnoteObject['f062fdebitActivityCode'] = this.maininvoiceListBasedOnInvoiceId[i]['f072fdebitActivityCode'];
            creditnoteObject['f062fdebitDepartmentCode'] = this.maininvoiceListBasedOnInvoiceId[i]['f072fdebitDepartmentCode'];

            creditnoteObject['f062fcreditFundCode'] = this.maininvoiceListBasedOnInvoiceId[i]['f072fcreditFundCode'];
            creditnoteObject['f062fcreditAccountCode'] = this.maininvoiceListBasedOnInvoiceId[i]['f072fcreditAccountCode'];
            creditnoteObject['f062fcreditActivityCode'] = this.maininvoiceListBasedOnInvoiceId[i]['f072fcreditActivityCode'];
            creditnoteObject['f062fcreditDepartmentCode'] = this.maininvoiceListBasedOnInvoiceId[i]['f072fcreditDepartmentCode'];
            creditnoteObject['f062fquantity'] = this.maininvoiceListBasedOnInvoiceId[i]['f072fquantity'];
            creditnoteObject['f062fprice'] = this.ConvertToFloat(this.maininvoiceListBasedOnInvoiceId[i]['f072fprice']).toFixed(2);
            creditnoteObject['f062fgstCode'] = this.maininvoiceListBasedOnInvoiceId[i]['f072fgstCode'];
            creditnoteObject['f062ftotalExc'] = this.maininvoiceListBasedOnInvoiceId[i]['f072ftotalExc'];
            creditnoteObject['f062ftaxAmount'] = this.ConvertToFloat(this.maininvoiceListBasedOnInvoiceId[i]['f072ftaxAmount']).toFixed(2);
            creditnoteObject['f062ftotal'] = this.ConvertToFloat(this.maininvoiceListBasedOnInvoiceId[i]['f072ftotal']).toFixed(2);
            creditnoteObject['f062fgstValue'] = this.ConvertToFloat(this.maininvoiceListBasedOnInvoiceId[i]['f072fgstValue']).toFixed(2);
            creditnoteObject['f062ftoCnAmount'] = this.ConvertToFloat(this.maininvoiceListBasedOnInvoiceId[i]['f062ftoCnAmount']).toFixed(2);

            saveDetails.push(creditnoteObject);
            // this.maininvoiceListBasedOnInvoiceId[i]['f062fidItem'] = parseInt(this.maininvoiceListBasedOnInvoiceId[i]['f062fidItem']).toString();

        }
        this.saveDataObject["details"] = saveDetails;
        console.log(this.saveDataObject);
        if (this.id > 0) {
            this.SponsorcreditnoteService.insertSponsorcreditItems(this.saveDataObject).subscribe(
                data => {
                    this.creditnoteData = data['result'];

                    this.router.navigate(['/accountsrecivable/sponsorcreditnote']);
                    alert("Sponsor Credit Note has been Updated Succcessfully");

                    console.log(this.creditnoteData);
                }, error => {
                    console.log(error);
                });
        } else {
            this.SponsorcreditnoteService.insertSponsorcreditItems(this.saveDataObject).subscribe(
                data => {
                    this.creditnoteData = data['result'];

                    this.router.navigate(['/accountsrecivable/sponsorcreditnote']);
                    alert("Sponsor Credit Note has been Added Succcessfully");

                    console.log(this.creditnoteData);
                }, error => {
                    console.log(error);
                });
        }
    }
    ConvertToFloat(val) {
        return parseFloat(val);
    }
    onlyNumberKey(event) {
        //console.log(event);
        if (event.charCode == 46) {

        } else {
            return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
        }
    }
    maximize() {
        $("#myModal").addClass("modal");
        $("#overlay1").show();
        $(".maximize-icon").addClass("hidden");
        $(".minimize-icon").removeClass("hidden");
        $(".modal-backdrop").show();

    }
    minimize() {
        $("#myModal").removeClass("modal");
        $("#overlay1").hide();
        $(".maximize-icon").removeClass("hidden");
        $(".minimize-icon").addClass("hidden");
        $(".modal-backdrop").hide();
        $('#myModal').show();

    }

}