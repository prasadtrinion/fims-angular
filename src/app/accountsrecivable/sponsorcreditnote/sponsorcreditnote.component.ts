import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SponsorcreditnoteService } from '../service/sponsorcreditnote.service'
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'Sponsorcreditnote',
    templateUrl: 'sponsorcreditnote.component.html'
})
export class SponsorcreditnoteComponent implements OnInit {
    sponsorcreditList = [];
    id: number;
    constructor(
        private SponsorcreditnoteService: SponsorcreditnoteService,
        private spinner: NgxSpinnerService,

    ) { }

    ngOnInit() {
        this.spinner.show();                                
        this.SponsorcreditnoteService.getItems().subscribe(
            data => {
                this.spinner.hide();
                let activityData = [];
                activityData = data['data'];
                console.log("asdf");
                for (var i = 0; i < activityData.length; i++) {
                    if (activityData[i]['f061fstatus'] == 0) {
                        activityData[i]['f061fstatus'] = 'Pending';
                    } else if (activityData[i]['f061fstatus'] == 1) {
                        activityData[i]['f061fstatus'] = 'Approved';
                    } else {
                        activityData[i]['f061fstatus'] = 'Rejected';
                    }
                }
                this.sponsorcreditList = activityData;

            }, error => {
                console.log(error);
            });
    }
    
}