import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { BatchgenerateService } from '../service/batchgenerate.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'Batchgenerate',
    templateUrl: 'batchgenerate.component.html'
})

export class BatchgenerateComponent implements OnInit {
    batchgenerateList =  [];
    batchgenerateData = {};
    id: number;
    constructor(  
        private BatchgenerateService: BatchgenerateService,
        private spinner: NgxSpinnerService,

    ) { }
    ngOnInit() { 
        this.spinner.show();        
        this.BatchgenerateService.getItems().subscribe(
            data => {
                this.spinner.hide();
                this.batchgenerateList = data['result']['data'];
        }, error => {
            console.log(error);
        });
    }
}