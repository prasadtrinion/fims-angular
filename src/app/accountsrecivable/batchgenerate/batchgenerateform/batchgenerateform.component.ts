import { Injector } from '@angular/core';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BatchgenerateService } from '../../service/batchgenerate.service'
import { ReceiptService } from '../../service/receipt.service'
import { BanksetupaccountService } from '../../../generalsetup/service/banksetupaccount.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { OnlinepaymenttypeService } from "../../service/onlinepaymenttype.service";

@Component({
    selector: 'BatchgenerateFormComponent',
    templateUrl: 'batchgenerateform.component.html'
})

export class BatchgenerateFormComponent implements OnInit {
    batchform: FormGroup;
    batchList = [];
    batchData = {};
    batchDataheader = {};
    receiptsList = [];
    banksetupaccountList = [];
    paymentList =[];
    ajaxCount: number;
    showLastRow: boolean;
    showLastRowMinus: boolean;
    showLastRowPlus: boolean;
    listshowLastRowMinus: boolean;
    listshowLastRowPlus: boolean;
    id: number;
    viewDisabled: boolean;
    saveBtnDisable: boolean;
    constructor(
        private BatchgenerateService: BatchgenerateService,
        private ReceiptService: ReceiptService,
        private BanksetupaccountService: BanksetupaccountService,
        private OnlinepaymenttypeService: OnlinepaymenttypeService,
        private spinner: NgxSpinnerService,
        private route: ActivatedRoute,
        private router: Router
    ) { }

    showIcons() {
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        console.log(this.batchList.length);
        console.log("Show last row" + this.showLastRow);
        console.log("showLastRowMinus" + this.showLastRowMinus);
        console.log("showLastRowPlus" + this.showLastRowPlus);
        console.log("listshowLastRowMinus" + this.listshowLastRowMinus);
        console.log("listshowLastRowPlus" + this.listshowLastRowPlus);

        if (this.batchList.length > 0) {
            this.showLastRowMinus = true;
            this.listshowLastRowMinus = true;
        }

        if (this.batchList.length > 1) {
            if (this.showLastRow == false) {
                this.listshowLastRowPlus = false;
                this.listshowLastRowMinus = true;
                this.showLastRowPlus = true;

            } else {
                this.listshowLastRowPlus = true;
                this.listshowLastRowMinus = false;

            }
        }



        if (this.batchList.length == 1 && this.showLastRow == false) {
            this.listshowLastRowMinus = true;
            this.listshowLastRowPlus = false;

        }
        if (this.batchList.length == 1 && this.showLastRow == true) {
            this.listshowLastRowMinus = false;
            this.listshowLastRowPlus = true;

        }
        if (this.batchList.length == 0 && this.showLastRow == false) {
            this.showLastRowMinus = false;
            this.showLastRowPlus = true;
        }
        if (this.batchList.length == 0 && this.showLastRow == true) {
            this.showLastRowMinus = true;
            this.showLastRowPlus = false;
        }
        if (this.id > 0) {
            this.listshowLastRowPlus = false;
        }

    }
    showNewRow() {
        this.showLastRow = false;
        this.showIcons();
    }
    deletemodule() {
        this.showLastRow = true;
        this.showIcons();
    }

    getReceipts() {
        var batchObj = {};
        batchObj['idBank'] = this.batchDataheader['f050fpaymentBank'];
        batchObj['date'] = this.batchDataheader['f050fdate'];
        this.BatchgenerateService.getReceipts(batchObj).subscribe(
            data => {
                this.receiptsList = data['result']['data'];
            }, error => {
                console.log(error);
            });
    }
    ngDoCheck() {
        if (this.ajaxCount == 0) {
            this.spinner.hide();
        }
    }
    ngOnInit() {
        this.ajaxCount = 0
        this.spinner.show();
        this.viewDisabled = false;
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id < 1) {
            this.showLastRow = false;
            this.showLastRowMinus = false;
            this.showLastRowPlus = true;
        } else {
            this.showLastRow = true;

        }

        // Receipt 
        // this.ajaxCount++;
        // this.ReceiptService.getActiveItems().subscribe(
        //     data => {
        //         this.ajaxCount--;
        //         console.log(this.ajaxCount);
        //         this.receiptsList = data['result']['data'];
        //     }
        // );
        //Payment dropdown
        this.ajaxCount++;
        this.OnlinepaymenttypeService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.paymentList = data['result']['data'];
            }
        );
        //company uum bank dropdown
        this.ajaxCount++;
        this.BanksetupaccountService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                console.log(this.ajaxCount);
                this.banksetupaccountList = data['result']['data'];
            }
        );
        if (this.id > 0) {
            this.ajaxCount++;
            this.BatchgenerateService.getItemsDetail(this.id).subscribe(
                data => {
                    this.ajaxCount--;
                    this.batchDataheader = data['result'][0];
                    this.batchDataheader['f050fpaymentBank'] = data['result'][0]['f050fpaymentBank'];
                    this.batchDataheader['f050fdate'] = data['result'][0]['f050fdate'];
                    this.batchDataheader['f050fbatchNumber'] = data['result'][0]['f050fbatchNumber'];
                    this.batchDataheader['f050fdate'] = data['result'][0]['f050fdate'];

                    this.batchList = data['result'];
                    this.showIcons();
                    this.saveBtnDisable = true;
                    this.viewDisabled = true;
                    $("#target input,select").prop("disabled", true);
                    $("#target1 input,select").prop("disabled", true);
                }, error => {
                    console.log(error);
                });
        }
        this.showIcons();

    }

    savebatchgenerate() {
        if (this.batchDataheader['f050fdate'] == undefined) {
            alert("Select Date");
            return false;
        }
        if (this.batchDataheader['f050fpaymentBank'] == undefined) {
            alert("Select Bank");
            return false;
        }
        if (this.batchDataheader['f050fpaymentType'] == undefined) {
            alert("Enter Payment");
            return false;
        }
        if (this.showLastRow == false) {
            var addactivities = this.addbatchgeneratelist();
            if (addactivities == false) {
                return false;
            }
        }
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }

        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        console.log(this.id);
        if (this.id > 0) {
            this.batchDataheader['f050fid'] = this.id;
        }
        this.batchDataheader['f050fstatus'] = 0;
        this.batchDataheader['batch-details'] = this.batchList;
        if (this.id > 0) {
            this.BatchgenerateService.updateBatchgenerateItems(this.batchDataheader, this.id).subscribe(
                data => {
                    this.router.navigate(['accountsrecivable/batchgenerate']);
                }, error => {
                    console.log(error);
                });
        } else {
            this.BatchgenerateService.insertBatchgenerateItems(this.batchDataheader).subscribe(
                data => {
                    this.router.navigate(['accountsrecivable/batchgenerate']);
                    alert("Batch Number Generate has been Added Successfully");
                }, error => {
                    console.log(error);
                });
        }
    }
    deletebatchgenerate(object) {
        var cnf = confirm("Do you really want to delete");
        if (cnf == true) {
            var index = this.batchList.indexOf(object);
            if (index > -1) {
                this.batchList.splice(index, 1);
            }
            this.showIcons();
        }
    }
    addbatchgeneratelist() {
        if (this.batchData['f051fidReceipt'] == undefined) {
            alert("Select Receipt");
            return false;
        }
        var dataofCurrentRow = this.batchData;
        this.batchData = {};
        this.batchList.push(dataofCurrentRow);
        this.showIcons();
    }
}