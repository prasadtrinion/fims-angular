import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { RequestinvoiceService } from '../service/requestinvoice.service '
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'RequestinvoiceComponent',
    templateUrl: 'requestinvoice.component.html'
})

export class RequestinvoiceComponent implements OnInit {
    requestinvoiceList = [];
    requestinvoiceData = {};
    id: number;
    title: string;
    type: string;

    constructor(

        private RequestinvoiceService: RequestinvoiceService,
        private spinner: NgxSpinnerService,
        private route: ActivatedRoute
    ) { }

    ngOnInit() {
        this.spinner.show();
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.RequestinvoiceService.getItems().subscribe(
            data => {
                this.spinner.hide();
                let activeData = [];
                activeData = data['result']['data'];
                for (var i = 0; i < activeData.length; i++) {
                    if (activeData[i]['f071fstatus'] == 0) {
                        activeData[i]['f071fstatus'] = 'Pending';
                    } else if (activeData[i]['f071fstatus'] == 1) {
                        activeData[i]['f071fstatus'] = 'Approved';
                    } else {
                        activeData[i]['f071fstatus'] = 'Rejected';
                    }
                    if (activeData[i]['f071finvoiceType'] == 'STA') {
                        activeData[i]['f071finvoiceType'] = 'Staff';

                    } else if (activeData[i]['f071finvoiceType'] == 'STD') {
                        activeData[i]['f071finvoiceType'] = 'Student';
                    }
                    else {
                        if (activeData[i]['f071finvoiceType'] == 'OR') {
                            activeData[i]['f071finvoiceType'] = 'Other Receivables';
                        }
                    }
                }
                this.requestinvoiceList = activeData;
            }, error => {
                console.log(error);
            });
    }

}