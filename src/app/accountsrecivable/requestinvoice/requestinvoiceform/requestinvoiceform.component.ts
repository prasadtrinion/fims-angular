import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { VoucherService } from '../../service/voucher.service';
import { RequestinvoiceService } from '../../service/requestinvoice.service ';
import { CustomerService } from '../../../accountsrecivable/service/customer.service';
import { GlcodegenerationService } from '../../../generalledger/service/glcodegeneration.service';
import { TaxsetupcodeService } from '../../../generalledger/service/taxsetupcode.service'
import { ItemService } from '../../service/item.service';
import { FundService } from '../../../generalsetup/service/fund.service';
import { AccountcodeService } from '../../../generalsetup/service/accountcode.service';
import { ActivitycodeService } from '../../../generalsetup/service/activitycode.service';
import { DepartmentService } from '../../../generalsetup/service/department.service';
import { RevenuesetupService } from '../../service/revenuesetup.service'
import { FeeitemService } from '../../../studentfinance/service/feeitem.service';
import { RequestinvoiceapprovalService } from '../../service/requestinvoiceapproval.service'
import { NgxSpinnerService } from 'ngx-spinner';
import { StudentService } from '../../../studentfinance/service/student.service';
import { SponsorService } from '../../../studentfinance/service/sponsor.service';
import { StaffService } from "../../service/staff.service";
import { RevenueTypeService } from "../../service/revenuetype.service";

@Component({
    selector: 'RequestinvoiceformComponent',
    templateUrl: 'requestinvoiceform.component.html',
    styleUrls: ['./requestinvoiceform.component.css']

})

export class RequestinvoiceformComponent implements OnInit {
    requestinvoiceList = [];
    requestinvoiceData = {};
    requestinvoiceDataheader = {};
    glcodeList = [];
    itemList = [];
    customerList = [];
    taxcodeList = [];
    DeptList = [];
    accountcodeList = [];
    fundList = [];
    Invoice = [];
    Bill = [];
    activitycodeList = [];
    taxcodepercenList = [];
    deleteList = [];
    requestinvoiceApprovalList = [];
    requestinvoiceApprovalData = {};
    customertype = [];
    invoiceapprovalData = [];
    sponserList = [];
    studentList = [];
    smartStaffList = [];
    revenuetypeList = [];
    type: string;
    id: number;
    valueDate: string;
    ajaxCount: number;
    gstValue: number;
    Amount: number;
    taxAmount: number;
    totalAmount: number;
    viewid: number;
    showLastRow: boolean;
    showLastRowMinus: boolean;
    showLastRowPlus: boolean;
    listshowLastRowMinus: boolean;
    listshowLastRowPlus: boolean;
    saveBtnDisable: boolean;
    viewDisabled: boolean;
    editDisabled: boolean;
    idview: number;
    constructor(
        private GlcodegenerationService: GlcodegenerationService,
        private RequestinvoiceService: RequestinvoiceService,
        private DepartmentService: DepartmentService,
        private ActivitycodeService: ActivitycodeService,
        private AccountcodeService: AccountcodeService,
        private FundService: FundService,
        private VoucherService: VoucherService,
        private TaxsetupcodeService: TaxsetupcodeService,
        private CustomerService: CustomerService,
        private StaffService: StaffService,
        private StudentService: StudentService,
        private SponsorService: SponsorService,
        private ItemService: ItemService,
        private route: ActivatedRoute,
        private RevenuesetupService: RevenuesetupService,
        private RequestinvoiceapprovalService: RequestinvoiceapprovalService,
        private router: Router,
        private FeeitemService: FeeitemService,
        private RevenueTypeService: RevenueTypeService,
        private spinner: NgxSpinnerService,

    ) { }

    ngDoCheck() {
        this.valueDate = new Date().toISOString().substr(0, 10);
        this.requestinvoiceDataheader['f071finvoiceDate'] = this.valueDate;
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.route.paramMap.subscribe((data) => this.viewid = + data.get('viewid'));
        if (this.ajaxCount == 0 && this.requestinvoiceList.length > 0) {
            this.ajaxCount = 20;
            console.log("asdfsdf");
            this.showIcons();
            if (this.requestinvoiceList[0]['f071fstatus'] == 1 || this.requestinvoiceList[0]['f071fstatus'] == 2) {
                this.saveBtnDisable = true;
                this.viewDisabled = true;
                this.editDisabled = true;
                $("#target input,select").prop("disabled", true);
                $("#target1 input,select").prop("disabled", true);
            }
        }
        if (this.ajaxCount == 0 && this.requestinvoiceList.length > 0) {
            this.ajaxCount = 20;
            if (this.viewid > 1) {
                this.saveBtnDisable = true;
                this.viewDisabled = true;
                this.editDisabled = true;
                $("#target input,select").prop("disabled", true);
                $("#target1 input,select").prop("disabled", true);
            }
        }
        if (this.ajaxCount == 0) {
            this.editFunction();
            this.spinner.hide();
            this.ajaxCount = 10;
        }
    }
    deletemodule() {


        this.showLastRow = true;
        this.showIcons();
    }

    showIcons() {
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.requestinvoiceList.length > 0) {
            this.showLastRowMinus = true;
            this.listshowLastRowMinus = true;
            this.showLastRowPlus = true;
        }

        if (this.requestinvoiceList.length > 1) {
            if (this.showLastRow == false) {
                this.listshowLastRowPlus = false;
                this.listshowLastRowMinus = true;
                this.showLastRowPlus = true;

            } else {
                this.listshowLastRowPlus = true;
                this.listshowLastRowMinus = false;

            }
        }
        if (this.requestinvoiceList.length == 1 && this.showLastRow == false) {
            this.listshowLastRowMinus = true;
            this.listshowLastRowPlus = false;

        }
        if (this.requestinvoiceList.length == 1 && this.showLastRow == true) {
            this.listshowLastRowMinus = false;
            this.listshowLastRowPlus = true;

        }
        if (this.requestinvoiceList.length == 0 && this.showLastRow == false) {
            this.showLastRowMinus = false;
            this.showLastRowPlus = true;
        }
        if (this.requestinvoiceList.length == 0 && this.showLastRow == true) {
            this.showLastRowMinus = true;
            this.showLastRowPlus = false;
        }
        console.log(this.requestinvoiceList.length);
        console.log("Show last row" + this.showLastRow);
        console.log("showLastRowMinus" + this.showLastRowMinus);
        console.log("showLastRowPlus" + this.showLastRowPlus);
        console.log("listshowLastRowMinus" + this.listshowLastRowMinus);
        console.log("listshowLastRowPlus" + this.listshowLastRowPlus);

        if (this.requestinvoiceList.length > 0) {
            if (this.viewDisabled == true || this.requestinvoiceList[0]['f071fstatus'] == 1 || this.requestinvoiceList[0]['f071fstatus'] == 2) {
                this.listshowLastRowPlus = false;
            }
        }
        else {
            if (this.viewDisabled == true) {
                this.listshowLastRowPlus = false;
            }
        }
    }
    showNewRow() {
        var confirmPop = confirm("Do you want to Add?");
        if (confirmPop == false) {
            return false;
        }
        this.showLastRow = false;
        this.showIcons();
    }
    editFunction() {
        this.ajaxCount++;
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.route.paramMap.subscribe((data) => this.viewid = + data.get('viewid'));

        if (this.id > 0) {
            this.RequestinvoiceService.getInvoiceDetailsById(this.id).subscribe(
                data => {
                    this.ajaxCount--;
                    this.requestinvoiceList = data['result'];
                    this.requestinvoiceDataheader['f071fapprovedBy'] = data['result'][0]['f071fapprovedBy'];
                    this.requestinvoiceDataheader['f071finvoiceDate'] = data['result'][0]['f071finvoiceDate'];
                    this.requestinvoiceDataheader['f071finvoiceNumber'] = data['result'][0]['f071finvoiceNumber'];
                    this.requestinvoiceDataheader['f071finvoiceTotal'] = data['result'][0]['f071finvoiceTotal'];
                    this.requestinvoiceDataheader['f071finvoiceType'] = data['result'][0]['f071finvoiceType'];
                    this.requestinvoiceDataheader['f071fidCustomer'] = data['result'][0]['f071fidCustomer'];
                    this.requestinvoiceDataheader['f071fbillType'] = data['result'][0]['f071fbillType'];
                    this.requestinvoiceDataheader['f071frevenueType'] = data['result'][0]['f071frevenueType'];
                    this.requestinvoiceDataheader['f071fdescription'] = data['result'][0]['f071fdescription'];
                    this.requestinvoiceDataheader['f071fid'] = data['result'][0]['f071fid'];

                    // this.requestinvoiceData['f072fgstCode'] = parseInt(this.requestinvoiceData['f072fgstCode']);
                    for (var i = 0; i < this.requestinvoiceList.length; i++) {
                        this.requestinvoiceList[i]['f072fgstCode'] = parseInt(this.requestinvoiceList[i]['f072fgstCode']);
                    }
                    console.log(this.requestinvoiceList);
                    if (data['result'][0]['f071fstatus'] == 1 || data['result'][0]['f071fstatus'] == 2) {
                        this.viewDisabled = true;
                        this.editDisabled = true;
                        setTimeout(
                            function () {
                                $("#target input,select").prop("disabled", true);
                                $("#target1 input,select").prop("disabled", true);
                            }, 2000);

                    }
                    if (this.viewid == 2) {
                        this.viewDisabled = true;
                        this.editDisabled = true;
                        $("#target input,select").prop("disabled", true);
                        $("#target1 input,select").prop("disabled", true);
                    }
                    if (this.id > 0) {

                        this.editDisabled = true;
                    }
                    this.onBlurMethod();
                    this.showIcons();
                    this.getGstvalue();
                }, error => {
                    console.log(error);
                });
        }
    }
    autoSelect() {
        this.requestinvoiceData['f072fcreditFundCode'] = this.requestinvoiceData['f072fdebitFundCode'];


        this.requestinvoiceData['f072fcreditActivityCode'] = this.requestinvoiceData['f072fdebitActivityCode'];

        this.requestinvoiceData['f072fcreditDepartmentCode'] = this.requestinvoiceData['f072fdebitDepartmentCode'];

    }
    ngOnInit() {
        this.ajaxCount = 0
        this.spinner.show();
        this.viewDisabled = false;
        this.editDisabled = false;
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id < 1) {
            this.showLastRow = false;
            this.showLastRowMinus = false;
            this.showLastRowPlus = true;
        } else {
            this.showLastRow = true;

        }
        let invoicetypeobj = {};
        invoicetypeobj['name'] = 'Student';
        invoicetypeobj['type'] = 'STD';
        this.customertype.push(invoicetypeobj);
        invoicetypeobj = {};
        invoicetypeobj['name'] = 'Staff';
        invoicetypeobj['type'] = 'STA';
        this.customertype.push(invoicetypeobj);
        // invoicetypeobj = {};
        // invoicetypeobj['name'] = 'Sponsor';
        // invoicetypeobj['type'] = 'SP';
        // this.customertype.push(invoicetypeobj);
        invoicetypeobj = {};
        invoicetypeobj['name'] = 'Other Receivables';
        invoicetypeobj['type'] = 'OR';
        this.customertype.push(invoicetypeobj);
        console.log(this.id);

        let month = {};
        month['name'] = 'Payment';
        month['type'] = 'PI';
        this.Invoice.push(month);
        month = {};
        month['name'] = 'Credit Card Payment';
        month['type'] = 'CI';
        this.Invoice.push(month);
        month = {};
        month['name'] = 'Invoice from PTJ';
        month['type'] = 'KI';
        this.Invoice.push(month);

        let billType = {};
        billType['name'] = 'General Bill';
        this.Bill.push(billType);
        billType = {};
        billType['name'] = 'Utility Type';
        this.Bill.push(billType);
        this.ajaxCount = 0;
        //item dropdown
        // this.ajaxCount++;
        // this.FeeitemService.getItems().subscribe(
        //     data => {
        //         this.ajaxCount--;
        //         this.itemList = data['result']['data'];
        //     }
        // );

        //revenue type dropdown
        this.RevenueTypeService.getItems().subscribe(
            data => {
                this.revenuetypeList = data['result'];
            }, error => {
                console.log(error);
            });

        //Customer dropdown
        this.ajaxCount++;
        this.CustomerService.getActiveItems().subscribe(
            data => {
                this.ajaxCount--;
                this.customerList = data['result']['data'];
            }
        );

        //Glcode dropdown
        this.ajaxCount++;
        this.GlcodegenerationService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.glcodeList = data['result']['data'];
            }
        );


        //Department dropdown
        this.ajaxCount++;
        this.DepartmentService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.DeptList = data['result']['data'];
            }
        );


        //Activity dropdown
        this.ajaxCount++;
        this.ActivitycodeService.getItemsByLevel().subscribe(
            data => {
                this.ajaxCount--;
                this.activitycodeList = data['result']['data'];
            }
        );


        //Account dropdown
        this.ajaxCount++;
        this.AccountcodeService.getItemsByLevel().subscribe(
            data => {
                this.ajaxCount--;
                this.accountcodeList = data['result']['data'];
            }
        );



        //Fund dropdown
        this.ajaxCount++;
        this.FundService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.fundList = data['result']['data'];
            }
        );


        //Tax code dropdown
        this.ajaxCount++;
        this.TaxsetupcodeService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.taxcodeList = data['result']['data'];
            }
        );
        //item dropdown
        this.ajaxCount++;
        this.RevenuesetupService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                console.log(this.ajaxCount);

                this.itemList = data['result'];
            }
        );

        //Student dropdown
        this.ajaxCount++;
        this.StudentService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                console.log(this.ajaxCount);

                this.studentList = data['result']['data'];
            }
        );

        // Sponser dropdown
        this.ajaxCount++;
        this.SponsorService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                console.log(this.ajaxCount);

                this.sponserList = data['result']['data'];
            }
        );
        //  staff dropdown
        this.ajaxCount++;
        this.StaffService.getItems().subscribe(
            data => {
                console.log(data);
                this.ajaxCount--;
                this.smartStaffList = data['result']['data'];

                console.log(this.ajaxCount);
            }
        );
        this.ajaxCount++;
        this.CustomerService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                console.log(this.ajaxCount);

                this.customerList = data['result']['data'];
            }
        );
        this.showIcons();
        this.onBlurMethod();
    }
    getglcode() {
        let itemId = this.requestinvoiceData['f072fidItem'];
        console.log(itemId);
        this.ajaxCount++;
        this.RevenuesetupService.getItemsDetail(itemId).subscribe(
            data => {
                this.ajaxCount--;
                this.itemList = data['result'][0];
                this.requestinvoiceData['f072fcreditFundCode'] = data['result'][0]['f085ffund'];
                this.requestinvoiceData['f072fcreditActivityCode'] = data['result'][0]['f085factivity'];
                this.requestinvoiceData['f072fcreditDepartmentCode'] = data['result'][0]['f085fdepartment'];
                this.requestinvoiceData['f072fcreditAccountCode'] = data['result'][0]['f085faccountId'];
            }
        );
    }
    getglcodeList() {
        for (var i = 0; i < this.requestinvoiceList.length; i++) {
            let itemId = this.requestinvoiceList[i]['f072fidItem'];
            // this.ajaxCount++;
            this.RevenuesetupService.getItemsDetail(itemId).subscribe(
                data => {
                    // this.ajaxCount--;
                    this.itemList = data['result'][0];
                }
            );
            this.requestinvoiceList[i]['f072fcreditFundCode'] = this.itemList['f085ffund'];
            this.requestinvoiceList[i]['f072fcreditActivityCode'] = this.itemList['f085factivity'];
            this.requestinvoiceList[i]['f072fcreditDepartmentCode'] = this.itemList['f085fdepartment'];
            this.requestinvoiceList[i]['f072fcreditAccountCode'] = this.itemList['f085faccountId'];
        }

    }
    getType() {
        let typeObject = {};
        typeObject['type'] = this.requestinvoiceDataheader['f071finvoiceType'];
        this.type = this.requestinvoiceDataheader['f071finvoiceType']
        console.log(typeObject);
        return this.type;
    }
    getPercentage() {
        let percenId = this.requestinvoiceData['f072fgstValue'];
        console.log(percenId);
        this.TaxsetupcodeService.getItemsDetail(percenId).subscribe(
            data => {
                this.taxcodepercenList = data['result'];

                this.requestinvoiceData['f072fgstValue'] = data['result'][0]['f081fpercentage'];
            }
        );
    }
    getInvoiceNumber() {
        let typeObject = {};
        typeObject['type'] = this.requestinvoiceDataheader['f071finvoiceType'];
        this.VoucherService.getInvoiceNumber(typeObject).subscribe(
            data => {
                console.log(data);
                this.requestinvoiceDataheader['f071finvoiceNumber'] = data['number'];
                this.valueDate = new Date().toISOString().substr(0, 10);
                console.log(this.valueDate);
                this.requestinvoiceDataheader['f071finvoiceDate'] = this.valueDate;
            }
        );
    }

    ConvertToFloat(val) {
        return parseFloat(val);
    }

    saveRequestinvoice() {
        // if (this.requestinvoiceDataheader['f071finvoiceType'] == undefined) {
        //     alert("Select Request Invoice Type");
        //     return false;
        // }
        if (this.requestinvoiceDataheader['f071finvoiceDate'] == undefined) {
            alert("Select Request Invoice Date");
            return false;
        }
        // if (this.requestinvoiceDataheader['f071fidCustomer'] == undefined) {
        //     alert("Select Debtor Name");
        //     return false;
        // }
        // if (this.requestinvoiceDataheader['f071fbillType'] == undefined) {
        //     alert("Select Bill Type");
        //     return false;
        // }
        if (this.showLastRow == false) {
            var addactivities = this.addrequestinvoicelist(1);
            if (addactivities == false) {
                return false;
            }

        }
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        let invoiceObject = {};
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        console.log(this.id);
        if (this.id > 0) {
            invoiceObject['f071fid'] = this.id;
        }
        // if (this.requestinvoiceList.length > 0) {

        // } else {
        //     alert("Please add one invoice details");
        //     return false;
        // }
        invoiceObject['f071finvoiceNumber'] = this.requestinvoiceDataheader['f071finvoiceNumber'];
        invoiceObject['f071finvoiceType'] = this.requestinvoiceDataheader['f071finvoiceType'];
        invoiceObject['f071finvoiceDate'] = this.requestinvoiceDataheader['f071finvoiceDate'];
        invoiceObject['f071finvoiceTotal'] = this.ConvertToFloat(this.requestinvoiceDataheader['f071finvoiceTotal']).toFixed(2);
        invoiceObject['f071fidCustomer'] = this.requestinvoiceDataheader['f071fidCustomer'];
        // invoiceObject['f071fbillType'] = this.requestinvoiceDataheader['f071fbillType'];
        invoiceObject['f071frevenueType'] = this.requestinvoiceDataheader['f071frevenueType'];
        invoiceObject['f071fdescription'] = this.requestinvoiceDataheader['f071fdescription'];
        invoiceObject['f071finvoiceFrom'] = 2;
        invoiceObject['f071fdescription'] = "Description";
        invoiceObject['f071fstatus'] = 0;
        invoiceObject['f071fisRcp'] = 1;
        invoiceObject['invoice-details'] = this.requestinvoiceList;
        for (var i = 0; i < invoiceObject['invoice-details'].length; i++) {
            invoiceObject['invoice-details'][i]['f072ftotal'] = this.ConvertToFloat(invoiceObject['invoice-details'][i]['f072ftotal']).toFixed(2);
            invoiceObject['invoice-details'][i]['f072fprice'] = this.ConvertToFloat(invoiceObject['invoice-details'][i]['f072fprice']).toFixed(2);
            invoiceObject['invoice-details'][i]['f072ftotalExc'] = this.ConvertToFloat(invoiceObject['invoice-details'][i]['f072ftotalExc']).toFixed(2);
            invoiceObject['invoice-details'][i]['f072ftaxAmount'] = this.ConvertToFloat(invoiceObject['invoice-details'][i]['f072ftaxAmount']).toFixed(2);
        }
        invoiceObject['invoice-details'] = this.requestinvoiceList;

        if (this.id > 0) {
            this.RequestinvoiceService.updateRequestInvoiceItems(invoiceObject, this.id).subscribe(
                data => {
                    this.router.navigate(['accountsrecivable/requestinvoice']);
                    alert("Request Invoice has been Updated Successfully");
                }, error => {
                    console.log(error);
                });
        } else {
            this.RequestinvoiceService.insertRequestinvoiceItems(invoiceObject).subscribe(
                data => {
                    this.router.navigate(['accountsrecivable/requestinvoice']);
                    alert("Request Invoice has been Added Successfully");
                }, error => {
                    console.log(error);
                });
        }
        this.onBlurMethod();

    }
    getGstvalue() {
        let taxId = this.requestinvoiceData['f072fgstCode'];
        let quantity = this.requestinvoiceData['f072fquantity'];
        let price = this.requestinvoiceData['f072fprice'];
        //console.log(this.taxcodeList);
        var taxSelectedObject = {};
        for (var k = 0; k < this.taxcodeList.length; k++) {
            if (this.taxcodeList[k]['f081fid'] == taxId) {
                taxSelectedObject = this.taxcodeList[k];
            }
        }


        this.gstValue = taxSelectedObject['f081fpercentage'];
        this.requestinvoiceData['f072fgstValue'] = this.gstValue;
        this.Amount = (this.ConvertToFloat(price)) * (this.ConvertToFloat(quantity));
        this.taxAmount = (this.ConvertToFloat(this.gstValue)) / 100 * this.ConvertToFloat((this.Amount));

        let totalAm = ((this.taxAmount) + (this.Amount));
        // this.purchaserequistionentryData['f088total'] = this.Amount.toFixed(2);
        this.requestinvoiceData['f072ftaxAmount'] = this.taxAmount.toFixed(2);
        this.requestinvoiceData['f072ftotal'] = totalAm.toFixed(2);



    }
    onBlurMethod() {
        console.log(this.requestinvoiceList);
        var finaltotal = 0;
        for (var i = 0; i < this.requestinvoiceList.length; i++) {
            var totalamount = (this.ConvertToFloat(this.requestinvoiceList[i]['f072fquantity'])) * (this.ConvertToFloat(this.requestinvoiceList[i]['f072fprice']));
            console.log(totalamount);
            var gstamount = (this.ConvertToFloat(this.requestinvoiceList[i]['f072fgstValue']) / 100 * (totalamount));
            console.log(gstamount);
            this.requestinvoiceList[i]['f072ftotalExc'] = totalamount;
            this.requestinvoiceList[i]['f072ftotal'] = gstamount + totalamount;
            console.log(finaltotal);
            finaltotal = finaltotal + this.requestinvoiceList[i]['f072ftotal'];
            console.log(finaltotal);
        }
        this.requestinvoiceDataheader['f071finvoiceTotal'] = (this.ConvertToFloat(finaltotal)).toFixed(2);
    }
    amountprefill() {
        // console.log(this.purchaserequistionentryList);
        var finaltotal = 0;
        if (this.requestinvoiceData['f072fprice'] != undefined) {
            var totalamount = (this.ConvertToFloat(this.requestinvoiceData['f072fquantity'])) * (this.ConvertToFloat(this.requestinvoiceData['f072fprice']));
            console.log(totalamount);
            var gstamount = (this.ConvertToFloat(this.requestinvoiceData['f072fgstValue']) / 100 * (totalamount));
            console.log(gstamount);
            this.requestinvoiceData['f072ftotalExc'] = totalamount;
            // this.purchaserequistionentryDataheader['f088ftotalAmount'] = totalamount;
            if (this.requestinvoiceData['f072fgstCode'] != undefined) {
                this.getGstvalue();
            }
        }
    }
    listamountprefill() {
        // console.log(this.purchaserequistionentryList);
        for (let i = 0; i < this.requestinvoiceList.length; i++) {
            if (this.requestinvoiceList[i]['f072fprice'] != undefined) {
                var totalamount = (this.ConvertToFloat(this.requestinvoiceList[i]['f072fquantity'])) * (this.ConvertToFloat(this.requestinvoiceList[i]['f072fprice']));
                console.log(totalamount);
                var gstamount = (this.ConvertToFloat(this.requestinvoiceList[i]['f072fgstValue']) / 100 * (totalamount));
                console.log(gstamount);
                this.requestinvoiceList[i]['f072ftotalExc'] = totalamount;
                // this.purchaserequistionentryDataheader['f088ftotalAmount'] = totalamount;
                if (this.requestinvoiceList[i]['f072fgstCode'] != undefined) {
                    let taxId = this.requestinvoiceList[i]['f072fgstCode'];
                    let quantity = this.requestinvoiceList[i]['f072fquantity'];
                    let price = this.requestinvoiceList[i]['f072fprice'];
                    //console.log(this.taxcodeList);
                    var taxSelectedObject = {};
                    for (var k = 0; k < this.taxcodeList.length; k++) {
                        if (this.taxcodeList[k]['f081fid'] == taxId) {
                            taxSelectedObject = this.taxcodeList[k];
                        }
                    }


                    this.gstValue = taxSelectedObject['f081fpercentage'];
                    this.requestinvoiceList[i]['f072fgstValue'] = this.gstValue;
                    this.Amount = (this.ConvertToFloat(price)) * (this.ConvertToFloat(quantity));
                    this.taxAmount = (this.ConvertToFloat(this.gstValue)) / 100 * this.ConvertToFloat((this.Amount));

                    let totalAm = ((this.taxAmount) + (this.Amount));
                    // this.purchaserequistionentryData['f088total'] = this.Amount.toFixed(2);
                    this.requestinvoiceList[i]['f072ftaxAmount'] = this.taxAmount.toFixed(2);
                    this.requestinvoiceList[i]['f072ftotal'] = totalAm.toFixed(2);
                }
            }
        }

    }
    // amountprefill() {
    deleteRequestinvoice(object) {
        var cnf = confirm("Do you really want to delete");
        if (cnf == true) {
            console.log(this.requestinvoiceList);
            var index = this.requestinvoiceList.indexOf(object);
            this.deleteList.push(this.requestinvoiceList[index]['f072fid']);
            let deleteObject = {};
            deleteObject['id'] = this.deleteList;
            this.RequestinvoiceService.deleteItems(deleteObject).subscribe(
                data => {
                }, error => {
                    console.log(error);
                });
            if (index > -1) {
                this.requestinvoiceList.splice(index, 1);
            }
            this.onBlurMethod();
            this.showIcons();
        }
    }

    addrequestinvoicelist(type) {

        if (this.requestinvoiceData['f072fidItem'] == undefined) {
            alert("Select Fee Item");
            return false;
        }
        if (this.requestinvoiceData['f072fdebitFundCode'] == undefined) {
            alert("Select Debit Fund");
            return false;
        }
        if (this.requestinvoiceData['f072fdebitActivityCode'] == undefined) {
            alert("Select Debit Activity Code");
            return false;
        }
        if (this.requestinvoiceData['f072fdebitDepartmentCode'] == undefined) {
            alert("Select Debit Department Code");
            return false;
        }
        if (this.requestinvoiceData['f072fdebitAccountCode'] == undefined) {
            alert("Select Debit Account Code");
            return false;
        }
        if (this.requestinvoiceData['f072fcreditFundCode'] == undefined) {
            alert("Select Credit Fund");
            return false;
        }
        if (this.requestinvoiceData['f072fcreditActivityCode'] == undefined) {
            alert("Select Credit Activity Code");
            return false;
        }

        if (this.requestinvoiceData['f072fcreditDepartmentCode'] == undefined) {
            alert("Select Credit Department Code");
            return false;
        } if (this.requestinvoiceData['f072fcreditAccountCode'] == undefined) {
            alert("Select Credit Account Code");
            return false;
        }
        if (this.requestinvoiceData['f072fquantity'] == undefined) {
            alert("Enter Quantity");
            return false;
        }
        if (this.requestinvoiceData['f072fprice'] == undefined) {
            alert("Enter Price");
            return false;
        }
        if (this.requestinvoiceData['f072fgstCode'] == undefined) {
            alert("Select Tax Code");
            return false;
        }
        // if (this.requestinvoiceData['f072fdebitFundCode'] ==this.requestinvoiceData['f072fcreditFundCode']) {
        //     alert("Debit GL and Credit GL Fund Cannot be Same");
        //     return false;
        // }
        // if (this.requestinvoiceData['f072fdebitActivityCode'] ==this.requestinvoiceData['f072fcreditActivityCode']) {
        //     alert("Debit GL and Credit GL Activity Code Cannot be Same");
        //     return false;
        // }
        // if (this.requestinvoiceData['f072fdebitDepartmentCode'] ==this.requestinvoiceData['f072fcreditDepartmentCode']) {
        //     alert("Debit GL and Credit GL Department Cannot be Same");
        //     return false;
        // }
        if (this.requestinvoiceData['f072fdebitAccountCode'] == this.requestinvoiceData['f072fcreditAccountCode']) {
            alert("Debit GL and Credit GL Account Code Cannot be Same");
            return false;
        }
        var confirmPop = confirm("Do you want to Add?");
        if (confirmPop == false) {
            return false;
        }

        console.log(this.requestinvoiceData);

        var dataofCurrentRow = this.requestinvoiceData;
        this.requestinvoiceData = {};
        this.requestinvoiceList.push(dataofCurrentRow);
        this.onBlurMethod();
        this.showIcons();
        this.onBlurMethod();
        this.amountprefill();
    }
    onlyNumberKey(event) {
        //console.log(event);
        if (event.charCode == 46) {

        } else {
            return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
        }
    }
    maximize() {
        $("#myModal").addClass("modal");
        $("#overlay1").show();
        $(".maximize-icon").addClass("hidden");
        $(".minimize-icon").removeClass("hidden");
        $(".modal-backdrop").show();

    }
    minimize() {
        $("#myModal").removeClass("modal");
        $("#overlay1").hide();
        $(".maximize-icon").removeClass("hidden");
        $(".minimize-icon").addClass("hidden");
        $(".modal-backdrop").hide();
        $('#myModal').show();

    }
    getList() {
        this.ajaxCount++;
        this.RequestinvoiceapprovalService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.requestinvoiceApprovalList = data['result'];

            }, error => {
                console.log(error);
            });
    }
    invoiceApprovalListData() {
        let approveDarrayList = [];
        for (var i = 0; i < this.requestinvoiceApprovalList.length; i++) {
            if (this.requestinvoiceApprovalList[i].f071fstatus == true) {
                approveDarrayList.push(this.requestinvoiceApprovalList[i].f071fid);
            }
        }
        var confirmPop = confirm("Do you want to Approve?");
        if (confirmPop == false) {
            return false;
        }
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        approveDarrayList.push(this.id);
        var approvalObject = {};
        approvalObject['id'] = approveDarrayList;
        approvalObject['status'] = '1';
        approvalObject['reason'] = '';

        this.RequestinvoiceapprovalService.updaterequestInvoiceApproval(approvalObject).subscribe(
            data => {
                this.invoiceapprovalData['reason'] = '';
                this.getList();
                alert("Request Invoice has been Approved Successfully");
                this.router.navigate(['accountsrecivable/requestinvoiceapproval']);
            }, error => {
                console.log(error);
            });

    }
    regectJournalApprovalListData() {
        console.log("867");
        let approveDarrayList = [];
        for (var i = 0; i < this.requestinvoiceApprovalList.length; i++) {
            if (this.requestinvoiceApprovalList[i].f071fstatus == true) {
                approveDarrayList.push(this.requestinvoiceApprovalList[i].f071fid);
            }
        }

            if (this.invoiceapprovalData['reason'] == undefined) {
                alert('Enter the Remarks');
                return false;
            }
            var confirmPop = confirm("Do you want to Reject?");
            if (confirmPop == false) {
                return false;
            }
            this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
            approveDarrayList.push(this.id);
            
            var budgetActivityDataObject = {}
            budgetActivityDataObject['id'] = approveDarrayList;
            budgetActivityDataObject['status'] = '2';
            budgetActivityDataObject['reason'] = this.invoiceapprovalData['reason'];


            this.RequestinvoiceapprovalService.updaterequestInvoiceApproval(budgetActivityDataObject).subscribe(
                data => {
                    this.getList();
                    this.invoiceapprovalData['reason'] = '';
                    alert("Request Invoice has been Rejected Successfully");
                    this.router.navigate(['accountsrecivable/requestinvoiceapproval']);
                }, error => {
                    console.log(error);
                });


        
    }
}