import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HttpClient, HTTP_INTERCEPTORS } from '@angular/common/http';
import { QueryBuilderModule } from "angular2-query-builder";
import { MaterialModule } from '../material/material.module';
import { DataTableModule } from 'angular-6-datatable';

import { PremiseComponent } from './premise/premise.component';
import { PremiseFormComponent } from './premise/premiseform/premiseform.component';

import { CategoriesComponent } from './categories/categories.component';
import { CategoriesFormComponent } from './categories/categoriesform/categoriesform.component';

import { RecurringsetupcustomerComponent } from './recurringsetupcustomer/recurringsetupcustomer.component';
import { RecurringsetupcustomerFormComponent } from './recurringsetupcustomer/recurringsetupcustomerform/recurringsetupcustomerform.component';

import { UtilityComponent } from './utility/utility.component';
import { UtilityFormComponent } from './utility/utilityform/utilityform.component';

import { CustomerComponent } from './customer/customer.component';
import { CustomerFormComponent } from './customer/customerform/customerform.component';

import { OnlinepaymenttypeComponent } from './onlinepaymenttype/onlinepaymenttype.component';
import { OnlinepaymenttypeFormComponent } from './onlinepaymenttype/onlinepaymenttypeform/onlinepaymenttypeform.component';

import { OnlinePaymentComponent } from './onlinepayment/onlinepayment.component';
import { OnlinePaymentViewComponent } from './onlinepayment/onlinepaymentview.component';
import { OnlinePaymentFormComponent } from './onlinepayment/onlinepaymentform/onlinepaymentform.component';

import { MaininvoiceComponent } from './maininvoice/maininvoice.component';
import { MaininvoiceformComponent } from './maininvoice/maininvoiceform/maininvoiceform.component';
import { MaininvoiceapprovalComponent } from './maininvoiceapproval/maininvoiceapproval.component';
import { MaininvoiceapprovalformComponent } from './maininvoiceapproval/maininvoiceapprovalform/maininvoiceapprovalform.component';

import { RequestinvoiceComponent } from './requestinvoice/requestinvoice.component';
import { RequestinvoiceformComponent } from './requestinvoice/requestinvoiceform/requestinvoiceform.component';
import { RequestinvoiceapprovalComponent } from './requestinvoiceapproval/requestinvoiceapproval.component';
import { RequestinvoiceapprovalformComponent } from './requestinvoiceapproval/requestinvoiceapprovalform/requestinvoiceapprovalform.component';

import { DebitnoteComponent } from './debitnote/debitnote.component';
import { DebitnoteformComponent } from './debitnote/debitnoteform/debitnoteform.component';
import { DebitnoteapprovalComponent } from './debitnoteapproval/debitnoteapproval.component';
import { DebitnoteapprovalformComponent } from './debitnoteapproval/debitnoteapprovalform/debitnoteapprovalform.component';

import { CreditnoteComponent } from './creditnote/creditnote.component';
import { CreditnoteformComponent } from './creditnote/creditnoteform/creditnoteform.component';
import { CreditnoteapprovalComponent } from './creditnoteapproval/creditnoteapproval.component';
import { CreditnoteapprovalformComponent } from './creditnoteapproval/creditnoteapprovalform/creditnoteapprovalform.component';

import { DiscountComponent } from './discount/discount.component';
import { DiscountformComponent } from './discount/discountform/discountform.component';

import { VoucherlistComponent } from './voucher/voucherlist/voucherlist.component';
import { VoucherapprovallistComponent } from './voucherapprovallist/voucherapprovallist.component';
import { VoucherapprovalFormComponent } from './voucherapprovallist/voucherapprovalform/voucherapprovalform.component';

import { ReceiptnoninvoiceComponent } from './receiptnoninvoice/receiptnoninvoice.component';
import { ReceiptnoninvapprovallistComponent } from './recepitnoninvapprovallist/recepitnoninvapprovallist.component';
import { ReceiptnoninvoicelistComponent } from './receiptnoninvoice/receiptnoninvoicelist/receiptnoninvoicelist.component';
import { VoucherComponent } from './voucher/voucher.component';

import { DebtorComponent } from './debtor/debtor.component';
import { DebtorlistComponent } from './debtor/debtorlist/debtorlist.component';
import { DebtorapprovalComponent } from './debtorapproval/debtorapproval.component';

import { DiscountapprovalComponent } from './discountapproval/discountapproval.component';

import { ReceiptComponent } from './receipt/receipt.component';
import { ReceiptformComponent } from './receipt/receiptform/receiptform.component';
import { NewreceiptinvoiceComponent } from './receipt/newreceiptinvoice/newreceiptinvoice.component';
import { NewreceiptnoninvoiceComponent } from './receipt/newreceiptnoninvoice/newreceiptnoninvoice.component';
import { NewreceiptlistComponent } from './receipt/newreceiptlist/newreceiptlist.component';

import { ReceiptapprovalComponent } from "./receiptapproval/receiptapproval.component";
import { ReceiptapprovalService } from "./service/receiptapproval.service";

import { VouchergenerationComponent } from './vouchergeneration/vouchergeneration.component';
import { VouchergenerationFormComponent } from './vouchergeneration/vouchergenerationform/vouchergenerationform.component';

import { PremisetypeFormComponent } from '../accountsrecivable/premisetype/premisetypeform/premisetypeform.component';
import { PremisetypeComponent } from '../accountsrecivable/premisetype/premisetype.component';

import { RevenueitemtypeComponent } from "./revenueitemtype/revenueitemtype.component";
import { RevenueItemTypeFormComponent } from "./revenueitemtype/revenueitemtypeform/revenueitemtypeform.component";

import { UtilityratesetupComponent } from "./utilityratesetup/utilityratesetup.component";
import { UtilityratesetupFormComponent } from './utilityratesetup/utilityratesetupform/utilityratesetupform.component';
import { RevenuesetupComponent } from "./revenuesetup/revenuesetup.component";
import { RevenuesetupFormComponent } from "./revenuesetup/revenuesetupform/revenuesetupform.component";

import { UtilitybillComponent } from './utilitybill/utilitybill.component';
import { UtilitybillFormComponent } from '../accountsrecivable/utilitybill/utilitybillform/utilitybillform.component';

import { BatchgenerateComponent } from './batchgenerate/batchgenerate.component';
import { BatchgenerateFormComponent } from './batchgenerate/batchgenerateform/batchgenerateform.component';
import { BatchgenerateService } from './service/batchgenerate.service';

import { StudentdiscountComponent } from '../accountsrecivable/studentdiscount/studentdiscount.component';
import { StudentdiscountFormComponent } from '../accountsrecivable/studentdiscount/studentdiscountform/studentdiscountform.component';

import { SponsorcreditnoteComponent } from "../accountsrecivable/sponsorcreditnote/sponsorcreditnote.component";
import { SponsorcreditnoteFormComponent } from "../accountsrecivable/sponsorcreditnote/sponsorcreditnoteform/sponsorcreditnoteform.component";
import { SponsorcreditnoteService } from "./service/sponsorcreditnote.service";

import { SponsorCreditapprovalComponent } from "./sponsorcreditapproval/sponsorcreditapproval";
import { SponsorCreditnoteapprovalService } from "./service/sponsorcreditapproval.service";

import { SponsordebitnoteComponent } from "../accountsrecivable/sponsordebitnote/sponsordebitnote.component";
import { SponsordebitnoteFormComponent } from "../accountsrecivable/sponsordebitnote/sponsordebitnoteform/sponsordebitnoteform.component";
import { SponsordebitnoteService } from "./service/sponsordebitnote.service";

import { SponsorDebitapprovalComponent } from "./sponsordebitapproval/sponsordebitapproval.component";
import { SponsorDebitnoteapprovalService } from "./service/sponsordebitapproval.service";

import { CategoriesService } from './service/categories.service';
import { VoucherService } from './service/voucher.service'
import { CountryService } from './../generalsetup/service/country.service';
import { StateService } from './../generalsetup/service/state.service';
import { GlcodegenerationService } from '../generalledger/service/glcodegeneration.service'
import { CustomerService } from '../accountsrecivable/service/customer.service';
import { PremiseService } from './service/premise.service';
import { UtilityService } from './service/utility.service';
import { VoucherapprovallistService } from './service/voucherapprovallist.service';
import { MaininvoiceService } from './service/maininvoice.service';
import { RequestinvoiceService } from './service/requestinvoice.service ';
import { MaininvoiceapprovalService } from './service/maininvoiceapproval.service';
import { DebtorService } from './service/debtor.service';
import { DebtorapprovalService } from './service/debtorapproval.service';
import { ReceiptnoninvoiceService } from './service/receiptnoninvoice.service';
import { ReceiptService } from './service/receipt.service';
import { ReceiptnoninvapprovallistService } from './service/recepitnoninvapprovallist.service';
import { RequestinvoiceapprovalService } from './service/requestinvoiceapproval.service';
import { CreditnoteService } from './service/creditnote.service';
import { CreditnoteapprovalService } from './service/creditnoteapproval.service';
import { PaymentvoucherService } from './../accountspayable/service/paymentvoucher.service';
import { ClaimcustomerService } from './../accountspayable/service/claimcustomer.service';
import { VoucherapprovalService } from './../accountsrecivable/service/voucherapproval.service';
import { PremisetypeService } from '../accountsrecivable/service/premisetype.service';
import { RevenueitemService } from "./service/revenueitem.service";
import { UtilityratesetupService } from './service/utilityratesetup.service';
import { UtilitybillService } from '../accountsrecivable/service/utilitybill.service';
import { FundService } from '../generalsetup/service/fund.service';
import { AccountcodeService } from '../generalsetup/service/accountcode.service';
import { ActivitycodeService } from '../generalsetup/service/activitycode.service';
import { DepartmentService } from '../generalsetup/service/department.service';

import { OnlinepaymenttypeService } from './service/onlinepaymenttype.service';
import { RevenuesetupService } from './service/revenuesetup.service'

import { DiscountService } from './service/discount.service';
import { DebitnoteService } from './service/debitnote.service';
import { DebitnoteapprovalService } from './service/debitnoteapproval.service';
import { VouchergenerationService } from './service/vouchergeneration.service';
import { SupplierregistrationService } from '../procurement/service/supplierregistration.service';

import { TempbankfileapproveComponent } from './tempbankfileapprove/tempbankfileapprove.component';
import { OnlineKnockOffComponent } from './onlineknockoff/onlineknockoff.component';
import { TempbankfileapprovalService } from './service/tempbankfileapproval.service';

import { AdvancepaymentComponent } from './advancepayment/advancepayment.component';
import { AdvancepaymentService } from './service/advancepayment.service';
import { KnockoffComponent } from './knockoff/knockoff.component';

import { RevenueComponent } from './revenue/revenue.component'
import { RevenueFormComponent } from "./revenue/revenueform/revenueform.component";
import { RevenueService } from "./service/revenue.service"

import { StaffService } from "./service/staff.service";

import { ItemService } from './service/item.service';
import { UserService } from '../generalsetup/service/user.service';
import { RecurringsetupcustomerService } from './service/recurringsetupcustomer.service';
import { OnlinePaymentService } from './service/onlinepayment.service';

import { AmpcashbookComponent } from './ampcashbook/ampcashbook.component'
import { AmpcashbookService } from './service/ampcashbook.service';

import { MmpcashbookComponent } from './mmpcashbook/mmpcashbook.component'
import { MmpcashbookService } from './service/mmpcashbook.service';

import { DebtorCategoryFormComponent } from "./debtorcategories/debtorcategoriesform/debtorcategoriesform.component";
import { DebtorCategoriesComponent } from "./debtorcategories/debtorcategories.component";
import { DebtorCategoryService } from "./service/debtorcategory.service";

import { RevenueTypeService } from "./service/revenuetype.service";
import { RevenueTypeFormComponent } from "./revenuetype/revenuetypeform/revenuetypeform.component";
import { RevenuetypeComponent } from "./revenuetype/revenuetype.component";

import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { AccountsrecivableRoutingModule } from './accountsrecivable.router.module';

import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { JwtInterceptor } from '../_helpers/jwt.interceptors';

import { NgSelectModule } from '@ng-select/ng-select';
import { SharedModule } from "../shared/shared.module";


@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        MaterialModule,
        AccountsrecivableRoutingModule,
        Ng2SearchPipeModule,
        HttpClientModule,
        QueryBuilderModule,
        DataTableModule,
        FontAwesomeModule,
        NgSelectModule,
        SharedModule,

    ],
    exports: [],
    declarations: [
        AmpcashbookComponent,
        MmpcashbookComponent,
        PremiseComponent,
        KnockoffComponent,
        PremiseFormComponent,
        CategoriesComponent,
        CategoriesFormComponent,
        RecurringsetupcustomerComponent,
        RecurringsetupcustomerFormComponent,
        UtilityComponent,
        UtilityFormComponent,
        MaininvoiceComponent,
        MaininvoiceapprovalComponent,
        DebtorComponent,
        ReceiptnoninvoiceComponent,
        VoucherComponent,
        MaininvoiceformComponent,
        MaininvoiceapprovalformComponent,
        DebtorlistComponent,
        DebtorapprovalComponent,
        VoucherapprovallistComponent,
        VoucherlistComponent,
        ReceiptnoninvoicelistComponent,
        ReceiptnoninvapprovallistComponent,
        ReceiptformComponent,
        ReceiptComponent,
        RequestinvoiceformComponent,
        RequestinvoiceComponent,
        RequestinvoiceapprovalformComponent,
        RequestinvoiceapprovalComponent,
        CreditnoteComponent,
        CreditnoteformComponent,
        DiscountComponent,
        DiscountformComponent,
        DebitnoteComponent,
        DebitnoteformComponent,
        CreditnoteapprovalformComponent,
        CreditnoteapprovalComponent,
        CustomerComponent,
        CustomerFormComponent,
        DebitnoteapprovalformComponent,
        DebitnoteapprovalComponent,
        NewreceiptinvoiceComponent,
        NewreceiptnoninvoiceComponent,
        NewreceiptlistComponent,
        VouchergenerationComponent,
        VouchergenerationFormComponent,
        VoucherapprovalFormComponent,
        PremisetypeFormComponent,
        PremisetypeComponent,
        RevenueitemtypeComponent,
        RevenueItemTypeFormComponent,
        UtilityratesetupFormComponent,
        UtilityratesetupComponent,
        UtilitybillFormComponent,
        UtilitybillComponent,
        OnlinepaymenttypeComponent,
        OnlinepaymenttypeFormComponent,
        OnlinePaymentComponent,
        OnlinePaymentFormComponent,
        RevenuesetupFormComponent,
        RevenuesetupComponent,
        BatchgenerateFormComponent,
        BatchgenerateComponent,
        DiscountapprovalComponent,
        StudentdiscountFormComponent,
        StudentdiscountComponent,
        TempbankfileapproveComponent,
        SponsorcreditnoteComponent,
        SponsorcreditnoteFormComponent,
        SponsordebitnoteComponent,
        SponsordebitnoteFormComponent,
        AdvancepaymentComponent,
        OnlinePaymentViewComponent,
        OnlineKnockOffComponent,
        RevenueFormComponent,
        RevenueComponent,
        SponsorCreditapprovalComponent,
        SponsorDebitapprovalComponent,
        ReceiptapprovalComponent,
        DebtorCategoriesComponent,
        DebtorCategoryFormComponent,
        RevenuetypeComponent,
        RevenueTypeFormComponent

    ],
    providers: [
        AmpcashbookService,
        MmpcashbookService,
        CategoriesService,
        PremiseService,
        AdvancepaymentService,
        RecurringsetupcustomerService,
        UtilityService,
        CountryService,
        CustomerService,
        StateService,
        MaininvoiceService,
        GlcodegenerationService,
        MaininvoiceapprovalService,
        ItemService,
        DebtorService,
        DebtorapprovalService,
        ReceiptnoninvoiceService,
        ItemService,
        VoucherService,
        UserService,
        VoucherapprovallistService,
        ReceiptnoninvapprovallistService,
        ReceiptService,
        RequestinvoiceService,
        RequestinvoiceapprovalService,
        CreditnoteService,
        DiscountService,
        DebitnoteService,
        CreditnoteapprovalService,
        CustomerService,
        DebitnoteapprovalService,
        VouchergenerationService,
        PaymentvoucherService,
        ClaimcustomerService,
        VoucherapprovalService,
        PremisetypeService,
        RevenueitemService,
        UtilityratesetupService,
        OnlinepaymenttypeService,
        RevenuesetupService,
        UtilitybillService,
        DepartmentService,
        FundService,
        AccountcodeService,
        ActivitycodeService,
        SupplierregistrationService,
        BatchgenerateService,
        SponsorcreditnoteService,
        SponsordebitnoteService,
        TempbankfileapprovalService,
        SponsorCreditnoteapprovalService,
        RevenueService,
        StaffService,
        SponsorDebitnoteapprovalService,
        ReceiptapprovalService,
        DebtorCategoryService,
        RevenueTypeService,
        OnlinePaymentService,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: JwtInterceptor,
            multi: true
        }

    ],
})
export class AccountsrecivableModule { }
