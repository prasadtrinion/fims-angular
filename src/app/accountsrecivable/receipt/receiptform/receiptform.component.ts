import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { ReceiptService } from '../../service/receipt.service'
import { ActivatedRoute, Router } from '@angular/router';


@Component({
    selector: 'ReceiptformComponent',
    templateUrl: 'receiptform.component.html'
})

export class ReceiptformComponent implements OnInit {
    invoiceList = [];
    invoiceData = {};
    customerList = [];
    glcodeList = [];
    itemList = [];
    invoiceDataheader = {};
    id: number;
    ajaxCount : number;
    valueDate: string;
    type:number;
    title: string;

    constructor(
        private InvoiceService: ReceiptService,
       
        private route: ActivatedRoute,
        private router: Router
    ) { }

    ngOnChanges(abc) {
        console.log(this.ajaxCount);
    }

    
    ngOnInit() {
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        this.route.paramMap.subscribe((data) => this.type = + data.get('type'));
        console.log(this.id);
                if (this.id==0){
                    this.title='Request Invoice';
                    //this.type='requestinvoice';
                }        
                if (this.id==1){
                    
                    //this.type='invoice';
                    this.title='Invoice';
                }



        // this.ajaxCount = 0;
        // //item dropdown
        // this.ajaxCount++;
        // this.ItemService.getItems().subscribe(
        //     data=>{
        //         this.itemList = data['result'];
        //         this.CustomerService.getItems().subscribe(
        //             data=>{
        //                 this.customerList = data['result']['data'];
        //                 this.GlcodegenerationService.getItems().subscribe(
        //                     data => {
        //                         this.glcodeList = data['result']['data'];
        //                         if(this.id>0) {
        //                             this.InvoiceService.getInvoiceDetailsById(this.id,this.type).subscribe(
        //                                 data => {
        //                                     this.invoiceList = data['result'];
        //                                     this.invoiceDataheader['f071fapprovedBy'] = data['result'][0].f071fapprovedBy;
        //                                     this.invoiceDataheader['f071finvoiceDate'] = data['result'][0].f071finvoiceDate;
        //                                     this.invoiceDataheader['f071finvoiceNumber'] = data['result'][0].f071finvoiceNumber;
        //                                     this.invoiceDataheader['f071finvoiceTotal'] = data['result'][0].f071finvoiceTotal;
        //                                     this.invoiceDataheader['f071finvoiceType'] = data['result'][0].f071finvoiceType;
        //                                     this.invoiceDataheader['f071fidCustomer'] = data['result'][0].f071fidCustomer;
        //                                     this.invoiceDataheader['f071fid'] = data['result'][0].f071fid;

        //                                     console.log(this.invoiceList);
        //                                 }, error => {
        //                                     console.log(error);
        //                                 });
        //                         }
                                
        //                     }
        //                 );
        //             }
        //         );

        //     }
        // );

        
    }
    indexTracker(index: number, value: any) {
        return index;
      }
    addinvoice() {
        console.log(this.invoiceList);
       
    }
    ConvertToInt(val) {
        return parseFloat(val);
    }
    
    // getInoiceNumber() {
    //     let typeObject = {};
    //     typeObject['type'] = this.invoiceDataheader['f071finvoiceType'];
    //     this.VoucherService.getInvoiceNumber(typeObject).subscribe(
    //         data => {
    //             console.log(data);
    //             this.invoiceDataheader['f071finvoiceNumber'] = data['number'];
    //             this.valueDate = new Date().toISOString().substr(0, 10);
    //             console.log(this.valueDate);
    //             this.invoiceDataheader['f071finvoiceDate'] = this.valueDate;
    //         }
    //     );
    // }
    saveInvoice(){


        let invoiceObject = {};
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if(this.id>0) {
            invoiceObject['f071fid'] = this.id;
        }
        invoiceObject['f071finvoiceNumber'] = this.invoiceDataheader['f071finvoiceNumber'];
        invoiceObject['f071finvoiceType'] = this.invoiceDataheader['f071finvoiceType'];
        invoiceObject['f071fstatus'] = 0;
        invoiceObject['f071fisRcp'] = 1;
        invoiceObject['f071finvoiceDate'] = this.invoiceDataheader['f071finvoiceDate'];
        invoiceObject['f071finvoiceTotal'] = this.invoiceDataheader['f071finvoiceTotal'];
        invoiceObject['f071fidCustomer'] = this.invoiceDataheader['f071fidCustomer'];
        invoiceObject['invoice-details'] = this.invoiceList;

        // if(this.id>0) {
        //     this.InvoiceService.updateInvoiceItems(invoiceObject).subscribe(
        //         data => {
        //             this.router.navigate(['accountsrecivable/requestinvoicelist/0']);
    
        //         }, error => {
        //             console.log(error);
        //         });
        // } else {
        //     this.InvoiceService.insertinvoiceItems(invoiceObject).subscribe(
        //         data => {
        //             this.router.navigate(['accountsrecivable/requestinvoicelist/0']);
    
        //         }, error => {
        //             console.log(error);
        //         });
        // }
       


    }

    onBlurMethod() {
        console.log(this.invoiceList);
        var finaltotal = 0;
        for(var i=0;i<this.invoiceList.length;i++) {
            var totalamount = (this.ConvertToInt(this.invoiceList[i]['f072fquantity']))*(this.ConvertToInt(this.invoiceList[i]['f072fprice']));
            var gstamount = (this.ConvertToInt(this.invoiceList[i]['f072fgst']))/100*(totalamount);
            this.invoiceList[i]['f072ftotal'] = gstamount + totalamount;
            finaltotal = finaltotal + this.invoiceList[i]['f072ftotal'];
        }

        this.invoiceDataheader['f071finvoiceTotal'] = finaltotal;
    }

    deleteInvoice(object) {
        var cnf = confirm("Do you really want to delete");
        if(cnf==true){
            console.log(this.invoiceList);
            var index = this.invoiceList.indexOf(object);;
            if (index > -1) {
                this.invoiceList.splice(index, 1);
            }
            this.onBlurMethod();
        }
    }
    

    addinvoicelist() {
       console.log(this.invoiceData);
        if(this.invoiceData['f072fidItem']==undefined){
            alert('Select Item');
            return false;
        }
        if(this.invoiceData['f072fglcode']==undefined){
            alert('Select GL Code');
            return false;
        }
        if(this.invoiceData['f072fquantity']==undefined){
            alert('Enter Quantity');
            return false;
        }

        var totalamount = (this.ConvertToInt(this.invoiceData['f072fquantity']))*(this.ConvertToInt(this.invoiceData['f072fprice']));
        var gstamount = (this.ConvertToInt(this.invoiceData['f072fgst']))/100*(totalamount);
        this.invoiceData['f072ftotal'] = gstamount + totalamount;
        //this.invoiceList.push(totalamount);
        var dataofCurrentRow = this.invoiceData;
        this.invoiceData = {};
        this.invoiceList.push(dataofCurrentRow);
        this.onBlurMethod();
        console.log(this.invoiceList);
    }

}