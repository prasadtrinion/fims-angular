import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { CustomerService } from '../../../accountsrecivable/service/customer.service'
import { ActivatedRoute, Router } from '@angular/router';
import { MaininvoiceService } from '../../service/maininvoice.service';
import { ReceiptService } from '../../service/receipt.service';
import { SponsorService } from '../../../studentfinance/service/sponsor.service';
import { TaxsetupcodeService } from '../../../generalledger/service/taxsetupcode.service'
import { ItemService } from '../../service/item.service';
import { FeeitemService } from '../../../studentfinance/service/feeitem.service';
import { StudentService } from '../../../studentfinance/service/student.service';
import { FundService } from '../../../generalsetup/service/fund.service';
import { AccountcodeService } from '../../../generalsetup/service/accountcode.service'
import { ActivitycodeService } from '../../../generalsetup/service/activitycode.service';
import { DepartmentService } from '../../../generalsetup/service/department.service';
import { SupplierregistrationService } from '../../../procurement/service/supplierregistration.service';
import { BanksetupaccountService } from '../../../generalsetup/service/banksetupaccount.service';
import { StaffService } from "../../service/staff.service";
import { OnlinepaymenttypeService } from "../../service/onlinepaymenttype.service";
import { ReceiptapprovalService } from '../../service/receiptapproval.service'
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'NewreceiptinvoiceComponent',
    templateUrl: 'newreceiptinvoice.component.html'
})

export class NewreceiptinvoiceComponent implements OnInit {

    itemList = [];

    customerList = [];
    studentList = [];
    supplierList = [];
    companyBankList = [];
    taxcodeList = [];
    feeItemList = [];
    DeptList = [];
    accountcodeList = [];
    fundList = [];
    activitycodeList = [];
    sponserList = [];
    paymenttype = [];
    inovoiceList = [];
    receiptDataheader = {};
    receiptPaymentInfoList = [];
    receiptPaymentInfoData = {};
    receiptInvoiceList = [];
    receiptInvoiceData = {};
    receiptNonInvoiceList = [];
    receiptNonInvoiceData = {};
    receiptDetailsList = [];
    banktype = [];
    maininvoiceList = [];
    receiptInvoiceList1 = [];
    maininvoiceData = {};
    customerData = {};
    invoiceListByUser = [];
    userData = {};
    userList = [];
    fpayeetype = [];
    receiptList = [];
    creditNotelist = [];
    title = '';
    finalAmount: number;
    nonInvoiceAmount: number;
    editId: number;
    openingbalanceData = {};
    newOpeningBalanceList = [];
    saveDataObject = {};
    smartStaffList = [];
    paymentList = [];
    creditnoteApprovalList = [];
    creditnoteApprovalData = {};
    invoiceapprovalData = [];
    gstValue: number;
    Amount: number;
    taxAmount: number;
    totalAmount: number;
    id: number;
    UserId: number;
    type: string;
    valueDate: string;
    ajaxCount: number;
    showLastRow: boolean;
    showLastRowMinus: boolean;
    showLastRowPlus: boolean;
    listshowLastRowMinus: boolean;
    listshowLastRowPlus: boolean;
    showLastRow1: boolean;
    showLastRowMinus1: boolean;
    showLastRowPlus1: boolean;
    listshowLastRowMinus1: boolean;
    listshowLastRowPlus1: boolean;
    showLastRow2: boolean;
    showLastRowMinus2: boolean;
    showLastRowPlus2: boolean;
    listshowLastRowMinus2: boolean;
    listshowLastRowPlus2: boolean;
    viewDisabled: boolean;
    editDisabled: boolean;
    saveBtnDisable: boolean;
    idview: number;
    receiptamount = 0;
    paymentamount = 0;
    invoiceamount = 0;
    noninvoiceamount = 0;
    showinvoice = 0;
    constructor(

        private ReceiptService: ReceiptService,
        private TaxsetupcodeService: TaxsetupcodeService,
        private CustomerService: CustomerService,
        private DepartmentService: DepartmentService,
        private ActivitycodeService: ActivitycodeService,
        private AccountcodeService: AccountcodeService,
        private FundService: FundService,
        private ItemService: ItemService,
        private SponsorService: SponsorService,
        private FeeitemService: FeeitemService,
        private StudentService: StudentService,
        private SupplierregistrationService: SupplierregistrationService,
        private BanksetupaccountService: BanksetupaccountService,
        private spinner: NgxSpinnerService,
        private MaininvoiceService: MaininvoiceService,
        private OnlinepaymenttypeService: OnlinepaymenttypeService,
        private ReceiptapprovalService: ReceiptapprovalService,
        private route: ActivatedRoute,
        private router: Router,
        private StaffService: StaffService,

    ) { }
    checkAmount() {
        this.receiptamount = this.receiptDataheader['f082ftotalAmount'];
        if (this.receiptamount != this.paymentamount) {
            alert("Receipt Amount Not equal to Payment Amount");
            return false;
        }
        // if(this.paymentamount != (this.invoiceamount + this.nonInvoiceAmount)){
        //     alert("Payment Amount Not equal to sum of invoice and non invoice");
        //     return false;
        // }
        return true;
    }
    increaseReceiptAmount(amount) {
        if (amount != undefined && amount != '') {
            this.paymentamount += parseFloat(amount);

        }
    }
    decreaseReceiptAmount(amount) {
        // let pay = this.receiptPaymentInfoData['f083famount'];
        if (amount != undefined && amount != '') {

            this.paymentamount -= parseFloat(amount);
        }
    }
    increaseNonInvoiceAmount(amount) {
        if (amount != undefined && amount != '') {
            this.noninvoiceamount += parseFloat(amount);

        }
    }
    decreaseNonInvoiceAmount(amount) {
        // let pay = this.receiptPaymentInfoData['f083famount'];
        if (amount != undefined && amount != '') {

            this.noninvoiceamount -= parseFloat(amount);
        }
    }
    increaseInvoiceAmount(amount) {
        if (amount != undefined && amount != '') {
            this.invoiceamount += parseFloat(amount);

        }
    }
    decreaseInvoiceAmount(amount) {
        // let pay = this.receiptPaymentInfoData['f083famount'];
        if (amount != undefined && amount != '') {
            this.invoiceamount -= parseFloat(amount);
        }
    }
    ngDoCheck() {

        const change = this.ajaxCount;
        this.route.paramMap.subscribe((data) => this.idview = + data.get('idview'));
        console.log(this.ajaxCount);
        if (this.ajaxCount == 0 && this.receiptList.length > 0) {
            this.spinner.hide();
            this.ajaxCount = 20;
            if (this.idview > 1) {
                this.saveBtnDisable = true;
                this.viewDisabled = true;
                $("#target input,select").prop("disabled", true);
                $("#target1 input,select").prop("disabled", true);
                $("#target2 input,select").prop("disabled", true);
                $("#target3 input,select").prop("disabled", true);

            }
        }
    }
    showIcons() {
        // this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.receiptPaymentInfoList.length > 0) {
            this.showLastRowMinus = true;
            this.showLastRowPlus = true;
            this.listshowLastRowMinus = true;
        }

        if (this.receiptPaymentInfoList.length > 1) {
            if (this.showLastRow == false) {
                this.listshowLastRowPlus = false;
                this.listshowLastRowMinus = true;
                this.showLastRowPlus = true;

            } else {
                this.listshowLastRowPlus = true;
                this.listshowLastRowMinus = false;

            }
        }
        if (this.receiptPaymentInfoList.length == 1 && this.showLastRow == false) {
            this.listshowLastRowMinus = true;
            this.listshowLastRowPlus = false;

        }
        if (this.receiptPaymentInfoList.length == 1 && this.showLastRow == true) {
            this.listshowLastRowMinus = false;
            this.listshowLastRowPlus = true;

        }
        if (this.receiptPaymentInfoList.length == 0 && this.showLastRow == false) {
            this.showLastRowMinus = false;
            this.showLastRowPlus = true;
        }
        if (this.receiptPaymentInfoList.length == 0 && this.showLastRow == true) {
            this.showLastRowMinus = true;
            this.showLastRowPlus = false;
        }
    }
    deletemodule() {
        this.showLastRow = true;
        this.showIcons();
    }
    showNewRow() {
        var confirmPop = confirm("Do you want to Add?");
        if (confirmPop == false) {
            return false;
        }
        this.showLastRow = false;
        this.showIcons();
    }

    showIcons1() {
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        if (this.receiptInvoiceList.length > 0) {
            this.showLastRowMinus1 = true;
            this.showLastRowPlus1 = true;
            this.listshowLastRowMinus1 = true;
        }

        if (this.receiptInvoiceList.length > 1) {
            if (this.showLastRow1 == false) {
                this.listshowLastRowPlus1 = false;
                this.listshowLastRowMinus1 = true;
                this.showLastRowPlus1 = true;

            } else {
                this.listshowLastRowPlus1 = true;
                this.listshowLastRowMinus1 = false;

            }
        }
        if (this.receiptInvoiceList.length == 1 && this.showLastRow1 == false) {
            this.listshowLastRowMinus1 = true;
            this.listshowLastRowPlus1 = false;

        }
        if (this.receiptInvoiceList.length == 1 && this.showLastRow1 == true) {
            this.listshowLastRowMinus1 = false;
            this.listshowLastRowPlus1 = true;

        }
        if (this.receiptInvoiceList.length == 0 && this.showLastRow1 == false) {
            this.showLastRowMinus1 = false;
            this.showLastRowPlus1 = true;
        }
        if (this.receiptInvoiceList.length == 0 && this.showLastRow1 == true) {
            this.showLastRowMinus1 = true;
            this.showLastRowPlus1 = false;
        }
        console.log(this.receiptInvoiceList.length);
        console.log("Show last row" + this.showLastRow1);
        console.log("showLastRowMinus" + this.showLastRowMinus1)
        console.log("showLastRowPlus" + this.showLastRowPlus1);
        console.log("listshowLastRowMinus" + this.listshowLastRowMinus1);
        console.log("listshowLastRowPlus" + this.listshowLastRowPlus1);
    }
    showNewRow1() {
        var confirmPop = confirm("Do you want to Add?");
        if (confirmPop == false) {
            return false;
        }
        this.showLastRow1 = false;
        this.showIcons1();
    }
    deletemodule1() {
        this.showLastRow1 = true;
        this.showIcons1();
    }

    showIcons2() {
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.receiptNonInvoiceList.length > 0) {
            this.showLastRowMinus2 = true;
            this.showLastRowPlus2 = true;
            this.listshowLastRowMinus2 = true;
        }

        if (this.receiptNonInvoiceList.length > 1) {
            if (this.showLastRow2 == false) {
                this.listshowLastRowPlus2 = false;
                this.listshowLastRowMinus2 = true;
                this.showLastRowPlus2 = true;

            } else {
                this.listshowLastRowPlus2 = true;
                this.listshowLastRowMinus2 = false;

            }
        }
        if (this.receiptNonInvoiceList.length == 1 && this.showLastRow2 == false) {
            this.listshowLastRowMinus2 = true;
            this.listshowLastRowPlus2 = false;

        }
        if (this.receiptNonInvoiceList.length == 1 && this.showLastRow2 == true) {
            this.listshowLastRowMinus2 = false;
            this.listshowLastRowPlus2 = true;

        }
        if (this.receiptNonInvoiceList.length == 0 && this.showLastRow2 == false) {
            this.showLastRowMinus2 = false;
            this.showLastRowPlus2 = true;
        }
        if (this.receiptNonInvoiceList.length == 0 && this.showLastRow2 == true) {
            this.showLastRowMinus2 = true;
            this.showLastRowPlus2 = false;
        }
        console.log(this.receiptNonInvoiceList.length);
        console.log("Show last row" + this.showLastRow2);
        console.log("showLastRowMinus" + this.showLastRowMinus2);
        console.log("showLastRowPlus" + this.showLastRowPlus2);
        console.log("listshowLastRowMinus" + this.listshowLastRowMinus2);
        console.log("listshowLastRowPlus" + this.listshowLastRowPlus2);
    }
    showNewRow2() {
        var confirmPop = confirm("Do you want to Add?");
        if (confirmPop == false) {
            return false;
        }
        this.showLastRow2 = false;
        this.showIcons2();
    }
    deletemodule2() {
        var confirmPop = confirm("Do you want to Delete?");
        if (confirmPop == false) {
            return false;
        }
        this.showLastRow2 = true;
        this.showIcons2();
    }
    editFunction() {
        this.ajaxCount++;
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.route.paramMap.subscribe((data) => this.idview = + data.get('idview'));
        if (this.id > 0) {
            this.ReceiptService.getItemsDetail(this.id).subscribe(
                data => {
                    this.ajaxCount--;
                    this.receiptDetailsList = data['result'];
                    this.receiptDataheader['f082freceiptNumber'] = data['result'].f082freceiptNumber;
                    this.receiptDataheader['f082fpayeeType'] = data['result'].f082fpayeeType;
                    this.receiptDataheader['f082fidCustomer'] = data['result'].f082fidCustomer;
                    this.receiptDataheader['f082fdate'] = data['result'].f082fdate;
                    this.receiptDataheader['f082fpaymentBank'] = data['result'].f082fpaymentBank;
                    this.receiptDataheader['f082ftotalAmount'] = data['result'].f082ftotalAmount;
                    this.receiptDataheader['f082fidInvoice'] = data['result'].f082fidInvoice;
                    this.receiptDataheader['f082fdescription'] = data['result'].f082fdescription;
                    this.getType();
                    this.getInvoiceList();
                    this.getsponsorHasBillsList();
                    this.receiptPaymentInfoList = data['result']['receipt-payments'];
                    this.receiptInvoiceList = data['result']['receipt-invoice'];
                    this.receiptNonInvoiceList = data['result']['receipt-non-invoice'];
                    this.showIcons();
                    this.showIcons1();
                    this.showIcons2();
                    if (data['result']['approvalStatus'] == 1 || data['result']['approvalStatus'] == 2) {
                        this.viewDisabled = true;
                        $("#target input,select").prop("disabled", true);
                        $("#target1 input,select").prop("disabled", true);
                        $("#target2 input,select").prop("disabled", true);
                        $("#target3 input,select").prop("disabled", true);

                    }
                    if (this.idview > 1) {
                        this.saveBtnDisable = true;
                        this.viewDisabled = true;
                        $("#target input,select").prop("disabled", true);
                        $("#target1 input,select").prop("disabled", true);
                        $("#target2 input,select").prop("disabled", true);
                        $("#target3 input,select").prop("disabled", true);

                    }
                    if (this.viewDisabled == true || data['result']['approvalStatus'] == 1 || data['result']['approvalStatus'] == 2) {
                        this.listshowLastRowPlus = false;
                        this.listshowLastRowPlus1 = false;
                        this.listshowLastRowPlus2 = false;

                    }
                    this.showinvoice = 1;
                    // this.showinvoice = 0;
                    this.editDisabled = true;
                    this.getAmount();
                    this.getAmount1();
                }, error => {
                    console.log(error);
                });
        }
    }

    ngOnInit() {
        this.ajaxCount = 0
        this.spinner.show();
        this.valueDate = new Date().toISOString().substr(0, 10);
        this.receiptDataheader['f082fdate'] = this.valueDate;
        this.viewDisabled = false;
        this.editDisabled = false;
        this.route.paramMap.subscribe((data) => this.idview = + data.get('idview'));
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id < 1) {
            this.showLastRow = false;
            this.showLastRowMinus = false;
            this.showLastRowPlus = true;
        } else {
            this.showLastRow = true;

        }
        if (this.id < 1) {
            this.
                showLastRow1 = false;
            this.showLastRowMinus1 = false;
            this.showLastRowPlus1 = true;
        } else {
            this.showLastRow1 = true;

        }
        if (this.id < 1) {
            this.showLastRow2 = false;
            this.showLastRowMinus2 = false;
            this.showLastRowPlus2 = true;
        } else {
            this.showLastRow2 = true;

        }
        let payeetype = {};
        payeetype['name'] = 'Student';
        payeetype['type'] = 'RSTD';
        this.fpayeetype.push(payeetype);
        payeetype = {};
        payeetype['name'] = 'Sponser';
        payeetype['type'] = 'RSP';
        this.fpayeetype.push(payeetype);
        payeetype = {};
        payeetype['name'] = 'Other Receivables';
        payeetype['type'] = 'ROR';
        this.fpayeetype.push(payeetype);
        payeetype = {};
        payeetype['name'] = 'Staff';
        payeetype['type'] = 'RSTA';
        this.fpayeetype.push(payeetype);

        this.route.paramMap.subscribe((data) => this.editId = + data.get('editId'));
        this.editFunction();

        let paymenttypeobj = {};
        paymenttypeobj['name'] = 'Resident';
        this.paymenttype.push(paymenttypeobj);
        paymenttypeobj = {};
        paymenttypeobj['name'] = 'Non-Resident';
        this.paymenttype.push(paymenttypeobj);

        let banktypeobj = {};
        banktypeobj['name'] = 'CIMB';
        this.banktype.push(banktypeobj);
        banktypeobj = {};
        banktypeobj['name'] = 'OUM';
        this.banktype.push(banktypeobj);
        //item dropdown
        this.ajaxCount++;
        this.ItemService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.itemList = data['result'];
            }
        );
        //Payment dropdown
        this.ajaxCount++;
        this.OnlinepaymenttypeService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.paymentList = data['result']['data'];
            }
        );
        //Sponser dropdown
        this.ajaxCount++;
        this.SponsorService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.sponserList = data['result']['data'];
            }
        );
        this.ajaxCount++;
        this.StaffService.getItems().subscribe(
            data => {
                console.log(data);
                this.ajaxCount--;
                this.smartStaffList = data['result']['data'];

                console.log(this.ajaxCount);
            }
        );
        //companyBank dropdown
        this.ajaxCount++;
        this.BanksetupaccountService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.companyBankList = data['result']['data'];
            }
        );
        //Student dropdown
        this.ajaxCount++;
        this.StudentService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.studentList = data['result']['data'];
            }
        );
        //Supplier dropdown
        this.ajaxCount++;
        this.SupplierregistrationService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.supplierList = data['result']['data'];
            }
        );
        //Customer dropdown
        this.ajaxCount++;
        this.CustomerService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.customerList = data['result']['data'];
            }
        );
        //Feeitem dropdown
        this.ajaxCount++;
        this.FeeitemService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.feeItemList = data['result']['data'];
            }
        );
        //Department dropdown
        this.ajaxCount++;
        this.DepartmentService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.DeptList = data['result']['data'];
            }
        );
        //Activity dropdown
        this.ajaxCount++;
        this.ActivitycodeService.getItemsByLevel().subscribe(
            data => {
                this.ajaxCount--;
                this.activitycodeList = data['result']['data'];
            }
        );
        //Account dropdown
        this.ajaxCount++;
        this.AccountcodeService.getItemsByLevel().subscribe(
            data => {
                this.ajaxCount--;
                this.accountcodeList = data['result']['data'];
            }
        );
        //Fund dropdown
        this.ajaxCount++;
        this.FundService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.fundList = data['result']['data'];
            }
        );
        //Taxcode dropdown
        this.ajaxCount++;
        this.TaxsetupcodeService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.spinner.hide();

                this.taxcodeList = data['result']['data'];
            });
        this.showIcons();
        this.showIcons1();
        this.showIcons2();
    }
    getType() {
        this.type = this.receiptDataheader['f082fpayeeType']
        return this.type;
    }
    getInvoiceAmount() {
        let id_invoice = this.receiptInvoiceData['f084fidInvoice'];
        this.MaininvoiceService.getItemsDetail(id_invoice).subscribe(
            data => {
                this.receiptInvoiceData['f084ftotal'] = data['result'][0]['f071finvoiceTotal'];
            }
        );
    }
    getInvoiceNumber() {
        let typeObject = {};
        // typeObject['type'] = this.receiptDataheader['f082fpayeeType'];
        typeObject['type'] = "RCP";
        this.type = this.receiptDataheader['f082fpayeeType']
        console.log(typeObject);


        this.ReceiptService.getReceiptNumber(typeObject).subscribe(
            data => {
                this.receiptDataheader['f082freceiptNumber'] = data['number'];
                this.valueDate = new Date().toISOString().substr(0, 10);
                this.receiptDataheader['f082fdate'] = this.valueDate;
            }
        );
        return this.type;
    }
    getsponsorHasBillsList() {
        this.UserId = this.receiptDataheader['f082fidCustomer'];
        console.log(this.UserId);

        this.ReceiptService.getsponsorHasBillsListApi(this.UserId).subscribe(
            data => {
                this.ajaxCount--;
                console.log(data);

                this.maininvoiceList = data['result']['data'];
                console.log(this.maininvoiceList);

            }
        );
    }
    getInvoiceList() {
        this.ajaxCount = 0;
        //Invoice dropdown
        this.ajaxCount++;
        this.UserId = this.receiptDataheader['f082fidCustomer'];
        let invoiceObj = {};
        switch (this.type) {
            case 'RSTA':
                invoiceObj['type'] = "STA";
                break;
            case 'RSTD':
                invoiceObj['type'] = "STD";
                break;
            case 'ROR':
                invoiceObj['type'] = "OR";
                break;
            case 'RSP':
                invoiceObj['type'] = "SP";
                break;
        }
        invoiceObj['UserId'] = this.UserId;

        this.ReceiptService.getInvoiceItems(invoiceObj).subscribe(
            data => {
                this.ajaxCount--;
                console.log(data);
                this.finalAmount = 0;
                this.maininvoiceList = data['result']['data'];

                if (this.id <= 0) {
                    this.receiptInvoiceList = data['result']['data'];

                    for (var i = 0; i < this.receiptInvoiceList.length; i++) {
                        this.receiptInvoiceList[i]['f084fidInvoice'] = this.receiptInvoiceList[i]['f071fid'];
                        this.receiptInvoiceList[i]['invoiceNumber'] = this.receiptInvoiceList[i]['f071finvoiceNumber'];
                        this.receiptInvoiceList[i]['f084fidDetails'] = this.receiptInvoiceList[i]['f072fid'];
                        this.receiptInvoiceList[i]['f084fcreditFundCode'] = this.receiptInvoiceList[i]['f072fcreditFundCode'];
                        this.receiptInvoiceList[i]['f084fcreditActivityCode'] = this.receiptInvoiceList[i]['f072fcreditActivityCode'];
                        this.receiptInvoiceList[i]['f084fcreditDepartmentCode'] = this.receiptInvoiceList[i]['f072fcreditDepartmentCode'];
                        this.receiptInvoiceList[i]['f084fcreditAccountCode'] = this.receiptInvoiceList[i]['f072fcreditAccountCode'];
                            this.receiptInvoiceList[i]['balance'] = this.receiptInvoiceList[i]['f072fbalanceAmount'];
                            this.receiptInvoiceList[i]['f084ftotal'] = this.receiptInvoiceList[i]['f072fbalanceAmount'];
                            this.finalAmount += parseFloat(this.receiptInvoiceList[i]['f084ftotal']);
                    }
                }

                else {
                    this.receiptInvoiceList1 = data['result']['data'];
                    for (var i = 0; i < this.receiptInvoiceList.length; i++) {
                        this.receiptInvoiceList[i]['balance'] = this.receiptInvoiceList1[i]['f072fbalanceAmount'];
                    }
                }
                // if(this.maininvoiceList.isEmpty()){
                //     alert("No Invoice");
                // }
            }
        );
    }
    getBatchDetails() {
        for (var i = 0; i < this.maininvoiceList.length; i++) {

            let spId = 0;
            spId = this.receiptInvoiceData['f084fidInvoice'];
            console.log(spId);

            this.MaininvoiceService.getBatchDetailsById(spId).subscribe(
                data => {
                    this.inovoiceList = data['result'][0];
                    this.receiptInvoiceData['f084ftotal'] = data['result'][0]['f071finvoiceTotal'],

                        this.receiptInvoiceData['f084fcreditActivityCode'] = data['result'][0]['f072fcreditActivityCode'],
                        this.receiptInvoiceData['f084fcreditFundCode'] = data['result'][0]['f072fcreditFundCode'],
                        this.receiptInvoiceData['f084fcreditDepartmentCode'] = data['result'][0]['f072fcreditDepartmentCode'],
                        this.receiptInvoiceData['f084fcreditAccountCode'] = data['result'][0]['f072fcreditAccountCode']
                    $("#target input").prop('disabled', true);
                }
            );
        }
    }
    getInvoiceDetails() {
        for (var i = 0; i < this.maininvoiceList.length; i++) {

            let invId = 0;
            if (this.receiptDataheader['f082fpayeeType'] == 'RSPO') {
                invId = this.maininvoiceList[i]['invoiceId'];

            } else {
                invId = this.receiptInvoiceData['f084fidInvoice'];
            }
            console.log(invId);
            this.MaininvoiceService.getInvoiceDetailsById(invId).subscribe(
                data => {
                    this.inovoiceList = data['result'][0];
                    this.receiptInvoiceData['f084ftotal'] = data['result'][0]['f071finvoiceTotal'],
                        this.receiptInvoiceData['f084fcreditActivityCode'] = data['result'][0]['f072fcreditActivityCode'],
                        this.receiptInvoiceData['f084fcreditFundCode'] = data['result'][0]['f072fcreditFundCode'],
                        this.receiptInvoiceData['f084fcreditDepartmentCode'] = data['result'][0]['f072fcreditDepartmentCode'],
                        this.receiptInvoiceData['f084fcreditAccountCode'] = data['result'][0]['f072fcreditAccountCode']
                    $("#target input").prop('disabled', true);
                }
            );
        }
    }

    ConvertToFloat(val) {

        return parseFloat(val);
    }

    getGstvalue() {
        let taxId = this.receiptNonInvoiceData['f084fidTaxCode'];
        // let quantity = this.receiptNonInvoiceData['f084fquantity'];
        // let price = this.receiptNonInvoiceData['f084fprice'];
        console.log(this.taxcodeList);
        var taxSelectedObject = {};
        for (var k = 0; k < this.taxcodeList.length; k++) {
            if (this.taxcodeList[k]['f081fid'] == taxId) {
                taxSelectedObject = this.taxcodeList[k];
            }
        }
        this.gstValue = taxSelectedObject['f081fpercentage'];
        this.receiptNonInvoiceData['f084fgstValue'] = this.gstValue;
        this.Amount = this.ConvertToFloat(this.receiptNonInvoiceData['f084ftotalExc']);
        this.taxAmount = ((this.ConvertToFloat(this.Amount) / 100) * this.ConvertToFloat(this.gstValue));
        let totalAm = ((this.taxAmount) + (this.Amount));
        this.receiptNonInvoiceData['f084ftotalExc'] = this.Amount.toFixed(2);
        this.receiptNonInvoiceData['f084ftaxAmount'] = this.taxAmount.toFixed(2);
        this.receiptNonInvoiceData['f084ftotal'] = totalAm.toFixed(2);

    }
    onBlurMethod() {
        var finaltotal = 0;
        for (var i = 0; i < this.receiptPaymentInfoList.length; i++) {
            var totalamount = this.ConvertToFloat(this.receiptNonInvoiceData['f084ftotalExc']);
            console.log(totalamount);
            var gstamount = (this.ConvertToFloat(this.receiptPaymentInfoList[i]['f084fgstValue']) / 100 * (totalamount));
            console.log(gstamount);
            this.receiptPaymentInfoList[i]['f084ftotalExc'] = totalamount;
            this.receiptPaymentInfoList[i]['f084ftaxAmount'] = gstamount + totalamount;
            console.log(finaltotal);
            finaltotal = finaltotal + this.receiptPaymentInfoList[i]['f084ftaxAmount'];
            console.log(finaltotal);
        }
        // this.purchaserequistionentryDataheader['f088ftotalAmount'] = this.ConvertToFloat(finaltotal.toFixed(2));
    }
    amountprefill() {
        var finaltotal = 0;
        if (this.receiptNonInvoiceData['f084fprice'] != undefined) {
            var totalamount = this.ConvertToFloat(this.receiptNonInvoiceData['f084ftotalExc']);
            console.log(totalamount);
            var gstamount = (this.ConvertToFloat(this.receiptNonInvoiceData['f084fgstValue']) / 100 * (totalamount));
            console.log(gstamount);
            this.receiptNonInvoiceData['f084ftotalExc'] = totalamount;
            if (this.receiptNonInvoiceData['f084fidTaxCode'] != undefined) {
                this.getGstvalue();
            }
        }
    }
    listamountprefill() {
        for (let i = 0; i < this.receiptPaymentInfoList.length; i++) {
            if (this.receiptPaymentInfoList[i]['f084fprice'] != undefined) {
                var totalamount = (this.ConvertToFloat(this.receiptPaymentInfoList[i]['f084fquantity'])) * (this.ConvertToFloat(this.receiptPaymentInfoList[i]['f084fprice']));
                console.log(totalamount);
                var gstamount = (this.ConvertToFloat(this.receiptPaymentInfoList[i]['f084fgstValue']) / 100 * (totalamount));
                console.log(gstamount);
                this.receiptPaymentInfoList[i]['f084ftotalExc'] = totalamount;
                if (this.receiptPaymentInfoList[i]['f084fidTaxCode'] != undefined) {
                    let taxId = this.receiptPaymentInfoList[i]['f084fidTaxCode'];
                    let quantity = this.receiptPaymentInfoList[i]['f084fquantity'];
                    let price = this.receiptPaymentInfoList[i]['f084fprice'];
                    var taxSelectedObject = {};
                    for (var k = 0; k < this.taxcodeList.length; k++) {
                        if (this.taxcodeList[k]['f084fid'] == taxId) {
                            taxSelectedObject = this.taxcodeList[k];
                        }
                    }
                    this.gstValue = taxSelectedObject['f084fgstValue'];
                    this.receiptPaymentInfoList[i]['f084fgstValue'] = this.gstValue;
                    this.Amount = (this.ConvertToFloat(price)) * (this.ConvertToFloat(quantity));
                    this.taxAmount = (this.ConvertToFloat(this.gstValue)) / 100 * this.ConvertToFloat((this.Amount));

                    let totalAm = ((this.taxAmount) + (this.Amount));
                    this.receiptPaymentInfoList[i]['f084ftaxAmount'] = this.taxAmount.toFixed(2);
                    this.receiptPaymentInfoList[i]['f084ftotal'] = totalAm.toFixed(2);
                }
            }
        }
    }
    getAmount() {
        this.finalAmount = 0;
        for (var i = 0; i < this.receiptInvoiceList.length; i++) {
            this.finalAmount = this.finalAmount + parseFloat(this.receiptInvoiceList[i]['f084ftotal']);
        }
    }
    getAmount1() {
        this.nonInvoiceAmount = 0;
        for (var i = 0; i < this.receiptNonInvoiceList.length; i++) {
            this.nonInvoiceAmount = this.nonInvoiceAmount + parseFloat(this.receiptNonInvoiceList[i]['f084ftotal']);
        }
    }
    deleteReceiptInvoiceList(object) {
        var cnf = confirm("Do you really want to delete");
        if (cnf == true) {
            console.log(this.receiptInvoiceList);
            let amount = object['f084ftotal'];
            // this.decreaseInvoiceAmount(amount);
            var index = this.receiptInvoiceList.indexOf(object);;
            if (index > -1) {
                this.receiptInvoiceList.splice(index, 1);
            }
            this.onBlurMethod();
            this.showIcons1();
            this.getAmount();
        }
    }

    addreceiptInvoiceData() {
        console.log(this.receiptInvoiceData);
        // if (this.receiptInvoiceData['f084fidInvoice'] == undefined) {
        //     alert('Select Payment Type');
        //     return false;
        // }
        // if (this.receiptInvoiceData['f084fcreditFundCode'] == undefined) {
        //     alert('Select Credit Fund');
        //     return false;
        // }
        // if (this.receiptInvoiceData['f084fcreditActivityCode'] == undefined) {
        //     alert('Select Credit Activity Code');
        //     return false;
        // }
        // if (this.receiptInvoiceData['f084fcreditDepartmentCode'] == undefined) {
        //     alert('Select Credit Deparment Code');
        //     return false;
        // }
        // if (this.receiptInvoiceData['f084fcreditAccountCode'] == undefined) {
        //     alert('Select Credit Account Code');
        //     return false;
        // }
        // if (this.receiptInvoiceData['f084ftotal'] == undefined) {
        //     alert('Enter Amount(RM)');
        //     return false;
        // }
        var confirmPop = confirm("Do you want to Add?");
        if (confirmPop == false) {
            return false;
        }
        // this.increaseInvoiceAmount();
        var dataofCurrentRow = this.receiptInvoiceData;
        this.receiptInvoiceData = {};
        this.receiptInvoiceList.push(dataofCurrentRow);
        this.showIcons1();
        this.getAmount();
    }
    deleteReceiptNonInvoice(object) {
        var cnf = confirm("Do you really want to delete");
        if (cnf == true) {
            console.log(this.receiptNonInvoiceList);
            let amount = object['f084ftotal'];
            // this.decreaseNonInvoiceAmount(amount);
            var index = this.receiptNonInvoiceList.indexOf(object);;
            if (index > -1) {
                this.receiptNonInvoiceList.splice(index, 1);
            }
            this.showIcons2();
            this.getAmount1();
        }
    }
    addReceiptNonInvoicelist() {
        console.log(this.receiptNonInvoiceData);
        if (this.receiptNonInvoiceData['f084fidItem'] == undefined) {
            alert('Select Item');
            return false;
        }
        if (this.receiptNonInvoiceData['f084fcreditFundCode'] == undefined) {
            alert('Select Credit Fund');
            return false;
        }
        if (this.receiptNonInvoiceData['f084fcreditActivityCode'] == undefined) {
            alert('Select Credit Activity Code');
            return false;
        }
        if (this.receiptNonInvoiceData['f084fcreditDepartmentCode'] == undefined) {
            alert('Select Credit Department Code');
            return false;
        }
        if (this.receiptNonInvoiceData['f084fcreditAccountCode'] == undefined) {
            alert('Select Credit Account Code');
            return false;
        }
        // if (this.receiptNonInvoiceData['f084fquantity'] == undefined) {
        //     alert('Enter Quantity');
        //     return false;
        // }
        // if (this.receiptNonInvoiceData['f084fprice'] == undefined) {
        //     alert('Enter Price');
        //     return false;
        // }

        var confirmPop = confirm("Do you want to Add?");
        if (confirmPop == false) {
            return false;
        }
        // this.increaseNonInvoiceAmount();
        var dataofCurrentRow = this.receiptNonInvoiceData;
        this.receiptNonInvoiceData = {};
        this.receiptNonInvoiceList.push(dataofCurrentRow);
        // this.onBlurMethod();
        this.showIcons2();
        this.getAmount1();
    }

    deletePaymentInfo(object) {
        var cnf = confirm("Do you really want to delete");
        if (cnf == true) {
            console.log(this.receiptPaymentInfoList);
            console.log(object);
            let amount = object['f083famount'];
            // this.decreaseReceiptAmount(amount);
            var index = this.receiptPaymentInfoList.indexOf(object);;
            if (index > -1) {
                this.receiptPaymentInfoList.splice(index, 1);
            }
            this.showIcons();
        }
    }
    addPaymentInfo() {

        console.log(this.receiptPaymentInfoData);
        // if (this.receiptPaymentInfoData['f083fidPayment'] == undefined) {
        //     alert('Select Payment Type');
        //     return false;
        // }
        if (this.receiptPaymentInfoData['f083fidTerminal'] == undefined) {
            alert('Enter Terminal ID');
            return false;
        }
        if (this.receiptPaymentInfoData['f083freferenceNumber'] == undefined) {
            alert('Enter Reference Number');
            return false;
        }
        if (this.receiptPaymentInfoData['f083fidBank'] == undefined) {
            alert('Select Bank');
            return false;
        }
        if (this.receiptPaymentInfoData['f083famount'] == undefined) {
            alert('Enter Amount(RM)');
            return false;
        }
        var confirmPop = confirm("Do you want to Add?");
        if (confirmPop == false) {
            return false;
        }
        // this.increaseReceiptAmount();
        var dataofCurrentRow = this.receiptPaymentInfoData;
        this.receiptPaymentInfoData = {};
        this.receiptPaymentInfoList.push(dataofCurrentRow);
        this.showIcons();
    }
    saveReceipt() {
        let save = this.checkAmount();
        if (save == false) {
            return false;
        }
        let receiptObject = {};
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        console.log(this.id);
        if (this.showLastRow == false) {
            var addactivities = this.addPaymentInfo();
            if (addactivities == false) {
                return false;
            }
        }
        if (this.showLastRow1 == false) {
            var addactivities = this.addreceiptInvoiceData();
            if (addactivities == false) {
                return false;
            }
        }
        if (this.showLastRow2 == false) {
            var addactivities = this.addReceiptNonInvoicelist();
            if (addactivities == false) {
                return false;
            }
        }
        if (this.receiptDataheader['f082fpayeeType'] == undefined) {
            alert('Select Payee Type');
            return false;
        }
        if (this.receiptDataheader['f082fdate'] == undefined) {
            alert('Select Date');
            return false;
        }
        if (this.receiptDataheader['f082fpaymentBank'] == undefined) {
            alert('Select Payment Bank');
            return false;
        }
        if (this.receiptDataheader['f082fdescription'] == undefined) {
            alert('Enter Description');
            return false;
        }

        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        if (this.id > 0) {
            receiptObject['f081fid'] = this.id;
        }

        let totalAmount = 0;
        for (var j = 0; j < this.receiptPaymentInfoList.length; j++) {
            totalAmount = totalAmount + parseFloat(this.receiptPaymentInfoList[j]['f083famount']);
        }
        receiptObject['f082freceiptNumber'] = this.receiptDataheader['f082freceiptNumber'];
        receiptObject['f082fpayeeType'] = this.receiptDataheader['f082fpayeeType'];
        receiptObject['f082fidCustomer'] = this.receiptDataheader['f082fidCustomer'];
        receiptObject['f082fdate'] = this.receiptDataheader['f082fdate'];
        receiptObject['f082fpaymentBank'] = this.receiptDataheader['f082fpaymentBank'];
        receiptObject['f082ftotalAmount'] = totalAmount.toFixed(2);
        receiptObject['f082fidInvoice'] = 1;//this.receiptDataheader['f082fidInvoice'];
        receiptObject['f082fdescription'] = this.receiptDataheader["f082fdescription"];

        receiptObject['receipt-payments'] = this.receiptPaymentInfoList;
        receiptObject['receipt-invoice'] = this.receiptInvoiceList;
        receiptObject['receipt-non-invoice'] = this.receiptNonInvoiceList;
        // receiptObject['receipt-non-invoice'] = this.receiptPaymentInfoList['f084fdebitFundCode'] = 11;
        // receiptObject['receipt-non-invoice'] = this.receiptPaymentInfoList['f084fdebitAccountCode'] = 11;
        // receiptObject['receipt-non-invoice'] = this.receiptPaymentInfoList['f084fdebitActivityCode'] = 11;
        // receiptObject['receipt-non-invoice'] = this.receiptPaymentInfoList['f084fdebitDepartmentCode'] = 11;

        if (this.id > 0) {
            receiptObject['f082fid'] = this.id;
        }
        this.ReceiptService.insertReceiptItems(receiptObject).subscribe(
            data => {
                this.router.navigate(['accountsrecivable/receiptlist']);
                if (this.id > 0) {
                    alert("Receipt has been Updated Sucessfully")
                } else {
                    alert("Receipt has been Added Sucessfully");
                }
            }, error => {
                console.log(error);
            });
    }
    onlyNumberKey(event) {
        //console.log(event);
        if (event.charCode == 46) {

        } else {
            return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
        }
    }
    maximize() {
        $("#myModal").addClass("modal");
        $("#overlay1").show();
        $(".maximize-icon").addClass("hidden");
        $(".minimize-icon").removeClass("hidden");
        $(".modal-backdrop").show();

    }
    minimize() {
        $("#myModal").removeClass("modal");
        $("#overlay1").hide();
        $(".maximize-icon").removeClass("hidden");
        $(".minimize-icon").addClass("hidden");
        $(".modal-backdrop").hide();
        $('#myModal').show();

    }
    getList() {
        this.spinner.show();
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        this.ReceiptapprovalService.getItems().subscribe(
            data => {
                this.spinner.hide();
                this.creditnoteApprovalList = data['result']['data'];
            }, error => {
                console.log(error);
            });
    }
    ApprovalListData() {
        let approveDarrayList = [];
        console.log(this.creditnoteApprovalList);
        for (var i = 0; i < this.creditnoteApprovalList.length; i++) {
            if (this.creditnoteApprovalList[i].approvalStatus == true) {
                approveDarrayList.push(this.creditnoteApprovalList[i].f082fid);
            }
        }
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        approveDarrayList.push(this.id);
        var confirmPop = confirm("Do you want to Approve?");
        if (confirmPop == false) {
            return false;
        }
        var approvalObject = {};
        approvalObject['id'] = approveDarrayList;
        approvalObject['status'] = '1';
        approvalObject['reason'] = '';

        this.ReceiptapprovalService.updateApproval(approvalObject).subscribe(
            data => {
                this.getList();
                alert("Approved Successfully");
                this.router.navigate(['/accountsrecivable/receiptapproval']);
                this.invoiceapprovalData['reason'] = '';
            }, error => {
                console.log(error);
            });

    }
    regectJournalApprovalListData() {
        let approveDarrayList = [];
        console.log(this.creditnoteApprovalList);
        for (var i = 0; i < this.creditnoteApprovalList.length; i++) {
            if (this.creditnoteApprovalList[i].approvalStatus == true) {
                approveDarrayList.push(this.creditnoteApprovalList[i].f082fid);
            }
        }
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        approveDarrayList.push(this.id);

        if (this.invoiceapprovalData['reason'] == undefined) {
            alert('Enter the Remarks');
            return false;
        }
        var confirmPop = confirm("Do you want to Reject?");
        if (confirmPop == false) {
            return false;
        }
        var creditnoteDataObject = {}
        creditnoteDataObject['id'] = approveDarrayList;
        creditnoteDataObject['status'] = '2';
        creditnoteDataObject['reason'] = this.invoiceapprovalData['reason'];
        this.ReceiptapprovalService.updateApproval(creditnoteDataObject).subscribe(
            data => {
                this.getList();
                this.invoiceapprovalData['reason'] = '';
                alert("Rejected Successfully");
                this.router.navigate(['/accountsrecivable/receiptapproval']);
            }, error => {
                console.log(error);
            });
    }
    showInvoice() {
        if (this.id > 0) {
            this.showinvoice = 1;
        }
        else {

            let save = this.checkAmount();
            if (save == false) {
                return false;
            }
            else {
                this.showinvoice = 1;
            }
        }


    }
    hideInvoice() {

        this.showinvoice = 0;
    }
}