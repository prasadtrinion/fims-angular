import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ReceiptService } from '../../service/receipt.service';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';


@Component({
    selector: 'NewreceiptlistComponent',
    templateUrl: 'newreceiptlist.component.html'
})

export class NewreceiptlistComponent implements OnInit {
    receiptList =  [];
    receiptData = {};
    id: number;
    title: string;
    type: string;

    constructor(
        
        private ReceiptService: ReceiptService,
        private route: ActivatedRoute,
        private spinner: NgxSpinnerService,
    ) { }

    ngOnInit() { 
        this.spinner.show();
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.ReceiptService.getItems().subscribe(
            data => {
                this.spinner.hide();
                let activityData = [];
                activityData = data['result']['data'];
                console.log("asdf");
                for (var i = 0; i < activityData.length; i++) {
                    if (activityData[i]['approvalStatus'] == 0) {
                        activityData[i]['approvalStatus'] = 'Pending';
                    } else if (activityData[i]['approvalStatus'] == 1) {
                        activityData[i]['approvalStatus'] = 'Approved';
                    } else {
                        activityData[i]['approvalStatus'] = 'Rejected';
                    }
                }
                this.receiptList = activityData;
        }, error => {
            console.log(error);
        });
    }

    
}