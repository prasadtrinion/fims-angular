import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ReceiptService } from './../service/receipt.service'
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
@Component({
    selector: 'ReceiptComponent',
    templateUrl: 'receipt.component.html'
})

export class ReceiptComponent implements OnInit {
    invoiceList = [];
    invoiceData = {};
    id: number;
    title: string;
    type: string;

    constructor(
        private ReceiptService: ReceiptService,
        private route: ActivatedRoute,
        private spinner: NgxSpinnerService,
    ) { }

    ngOnInit() {
        this.spinner.show();
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        console.log(this.id);
        if (this.id == 0) {
            this.title = 'Request Invoice';
            this.type = 'requestinvoice';
        }
        if (this.id == 1) {

            this.type = 'invoice';
            this.title = 'Invoice';
        }
        this.ReceiptService.getItems().subscribe(
            data => {
                this.spinner.hide();
                this.invoiceList = data['result'];
            }, error => {
                console.log(error);
            });
    }

    addPremise() {
        console.log(this.invoiceData);
        this.ReceiptService.insertReceiptItems(this.invoiceData).subscribe(
            data => {
                this.invoiceList = data['todos'];
            }, error => {
                console.log(error);
            });

    }
}