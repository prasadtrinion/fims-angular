import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Injector } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ReceiptapprovalService } from '../service/receiptapproval.service'
import { faSearch, faEdit } from '@fortawesome/free-solid-svg-icons';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'ReceiptapprovalComponent',
    templateUrl: 'receiptapproval.component.html'
})

export class ReceiptapprovalComponent implements OnInit {
    creditnoteApprovalList = [];
    creditnoteApprovalData = {};
    invoiceapprovalData=[];
    id: number;
    selectAllCheckbox:false;
    type : string;
    title ='';
    faSearch = faSearch;
    faEdit = faEdit;
    constructor(        
        private ReceiptapprovalService: ReceiptapprovalService,
        private spinner: NgxSpinnerService,
        private route: ActivatedRoute,
        private router: Router
    ) { }
    ngOnInit() { 
        this.getList();
    }
    getList() {
        this.spinner.show();
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        this.ReceiptapprovalService.getItems().subscribe(
            data => {
                this.spinner.hide();
                this.creditnoteApprovalList = data['result']['data'];
        }, error => {
            console.log(error);
        });
    }
    ApprovalListData() {
    let approveDarrayList = [];
    console.log(this.creditnoteApprovalList);
        for (var i = 0; i < this.creditnoteApprovalList.length; i++) {
            if(this.creditnoteApprovalList[i].approvalStatus==true) {
                approveDarrayList.push(this.creditnoteApprovalList[i].f082fid);
            }
        }
        if(approveDarrayList.length<1) {
            alert("Select atleast one Receipt to Approve");
            return false;
        }
        var confirmPop = confirm("Do you want to Approve?");
        if (confirmPop == false) {
            return false;
        }
        var approvalObject = {};
        approvalObject['id'] = approveDarrayList;
        approvalObject['status'] = '1';
        approvalObject['reason']='';

        this.ReceiptapprovalService.updateApproval(approvalObject).subscribe(
            data => {
                this.getList();
                alert("Approved Successfully");
                this.invoiceapprovalData['reason']='';
            }, error => {
            console.log(error);
        });

    }
    regectJournalApprovalListData() {
        let approveDarrayList = [];
    console.log(this.creditnoteApprovalList);
        for (var i = 0; i < this.creditnoteApprovalList.length; i++) {
            if(this.creditnoteApprovalList[i].approvalStatus==true) {
                approveDarrayList.push(this.creditnoteApprovalList[i].f082fid);
            }
        }
        if(approveDarrayList.length<1) {
            alert("Select atleast one Receipt to Reject");
            return false;
        }
       
        if (this.invoiceapprovalData['reason'] == undefined) {
            alert('Enter the Description');
            return false;
        }
        var confirmPop = confirm("Do you want to Reject?");
        if (confirmPop == false) {
            return false;
        }
        var creditnoteDataObject = {}
        creditnoteDataObject['id'] = approveDarrayList;
        creditnoteDataObject['status'] = '2';
        creditnoteDataObject['reason'] = this.invoiceapprovalData['reason'];
        this.ReceiptapprovalService.updateApproval(creditnoteDataObject).subscribe(
            data => {
                this.getList();
                this.invoiceapprovalData['reason']='';
                alert("Rejected Successfully");
            }, error => {
                console.log(error);
            });
    }
    selectAll() {
        console.log(this.selectAllCheckbox);
        for (var i = 0; i < this.creditnoteApprovalList.length; i++) {
                this.creditnoteApprovalList[i].approvalStatus = this.selectAllCheckbox;
        }
      }

}