import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CustomerService } from '../../accountsrecivable/service/customer.service';
import { GlcodegenerationService } from '../../generalledger/service/glcodegeneration.service'
import { ItemService } from '../service/item.service';
import { DebtorService } from '../service/debtor.service';
@Component({
    selector: 'DebtorComponent',
    templateUrl: 'debtor.component.html'
})

export class DebtorComponent implements OnInit {
    invoiceList = [];
    invoiceData = {};
    debtorList = [];
    debtorData = {};
    customerList = [];
    customerData = {};
    glcodeList = [];
    glcodeData = {};
    itemList = [];
    itemData = {};
    invoiceDataheader = {};
    id: number;
    editId:number;
    constructor(
        private DebtorService: DebtorService,
        private ItemService: ItemService,
        private GlcodegenerationService: GlcodegenerationService,
        private CustomerService: CustomerService,
        private route: ActivatedRoute,
        private router: Router
    ) { }

    ngOnInit() {
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.route.paramMap.subscribe((data) => this.editId = + data.get('editId'));

        //item dropdown

        this.ItemService.getItems().subscribe(
            data=>{
                this.itemList = data['result'];
                this.GlcodegenerationService.getItems().subscribe(
                    data => {
                        this.glcodeList = data['result']['data'];
                        this.CustomerService.getItems().subscribe(
                            data => {
                                this.customerList = data['result']['data'];
                            }
                        );
                    }
                );
            }
        );

    }

    saveInvoice(){

        let invoiceObject = {};
        invoiceObject['f079fcustomer'] = this.invoiceDataheader['f079fcustomer'];
        invoiceObject['f079fdate'] =this.invoiceDataheader['f079fdate'];
        invoiceObject['f079fadvancePayment'] =this.invoiceDataheader['f079fadvancePayment'];
        invoiceObject['f079fapprovedBy'] =this.invoiceDataheader['f079fapprovedBy'];
        invoiceObject['f079ftotalAmount'] =this.invoiceDataheader['f079ftotalAmount'];
        invoiceObject['f079fstatus'] =0;
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        console.log(this.id);
        if(this.id==0) {
            invoiceObject['f079fcrDrFlag'] =1;

        } else {
            invoiceObject['f079fcrDrFlag'] =2;

        }

        invoiceObject['details'] = this.invoiceList;
        console.log(invoiceObject);
        this.DebtorService.insertdebtorItems(invoiceObject).subscribe(
            data => {
            }, error => {
                console.log(error);
            });


    }
    addinvoicelist() {
       console.log(this.invoiceData);
        var dataofCurrentRow = this.invoiceData;
        this.invoiceData = {};
        this.invoiceList.push(dataofCurrentRow);
        console.log(this.invoiceList);
    }


}