import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DebtorService } from '../../service/debtor.service'
import { ActivatedRoute, Router } from '@angular/router';


@Component({
    selector: 'DebtorlistComponent',
    templateUrl: 'debtorlist.component.html'
})

export class DebtorlistComponent implements OnInit {
    debtorList =  [];
    debtorData = {};
    id: number;
    title:string;

    constructor(
        
        private DebtorService: DebtorService,
        private route: ActivatedRoute,

    ) { }

    ngOnInit() { 
        let type = '';

        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if(this.id==0) {
            this.title = "Debtor";
            type = "debtor";
           
        }
        if(this.id==1) {
            type="creditor";
            this.title = "Creditor";
        }
        console.log("asdfsadf");
            this.DebtorService.getItems(type).subscribe(
                data => {
                    this.debtorList = data['result'];
                    console.log(this.debtorList);
            }, error => {
                console.log(error);
            });
    }
}