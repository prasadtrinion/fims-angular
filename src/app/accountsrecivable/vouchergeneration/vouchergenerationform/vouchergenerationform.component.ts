import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { VoucherService } from '../../service/voucher.service';
import { VouchergenerationService } from '../../service/vouchergeneration.service';
import { CustomerService } from '../../../accountsrecivable/service/customer.service';
import { GlcodegenerationService } from '../../../generalledger/service/glcodegeneration.service';
import { TaxsetupcodeService } from '../../../generalledger/service/taxsetupcode.service';
import { ItemService } from '../../service/item.service';
import { AlertService } from '../../../_services/alert.service';
import { DefinationmsService } from '../../../studentfinance/service/definationms.service';
import { ClaimcustomerService } from '../../../accountspayable/service/claimcustomer.service';
import { StudentService } from '../../../studentfinance/service/student.service';
import { SponsorService } from '../../../studentfinance/service/sponsor.service';
import { FeeitemService } from '../../../studentfinance/service/feeitem.service';



@Component({
    selector: 'VouchergenerationFormComponent',
    templateUrl: 'vouchergenerationform.component.html'
})

export class VouchergenerationFormComponent implements OnInit {
    voucherGenerationList = [];
    voucherGenerationData = {};
    voucherGenerationDataheader = {};
    paymenttypeList = [];
    claimCustomerList = [];
    glcodeList = [];
    itemList = [];
    customerList = [];
    taxcodeList = [];
    typeList = [];
    studentList = [];
    sponserList = [];
    feeitemList = [];
    id: number;
    valueDate: string;
    ajaxCount: number;
    gstValue: number;
    Amount: number;
    taxAmount: number;
    totalAmount: number;
    typeId: string;
    payType: string;

    constructor(

        private GlcodegenerationService: GlcodegenerationService,
        private VouchergenerationService: VouchergenerationService,
        private ClaimcustomerService: ClaimcustomerService,
        private DefinationmsService: DefinationmsService,
        private VoucherService: VoucherService,
        private StudentService: StudentService,
        private TaxsetupcodeService: TaxsetupcodeService,
        private SponsorService: SponsorService,
        private CustomerService: CustomerService,
        private ItemService: ItemService,
        private FeeitemService: FeeitemService,
        private alertService: AlertService,
        private route: ActivatedRoute,
        private router: Router

    ) { }



    ngOnInit() {
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        console.log(this.id);

        this.getVoucherNumber();

        this.ajaxCount = 0;

        this.StudentService.getItems().subscribe(
            data => {
                this.studentList = data['result']['data'];
                console.log(this.studentList);
            }
        );

        this.SponsorService.getItems().subscribe(
            data => {
                this.sponserList = data['result']['data'];
            }
        );

        this.FeeitemService.getItems().subscribe(
            data => {
                this.feeitemList = data['result']['data'];
            }
        );

        this.ajaxCount++;
        // payment type dropdown
        this.DefinationmsService.getPaymenttype('PaymentVoucherType').subscribe(
            data => {
                this.paymenttypeList = data['result'];
                this.ClaimcustomerService.getItems().subscribe(
                    data => {
                        this.claimCustomerList = data['result']['data'];
                        this.ItemService.getItems().subscribe(
                            data => {
                                this.itemList = data['result'];
                                this.CustomerService.getItems().subscribe(
                                    data => {
                                        this.customerList = data['result']['data'];
                                        this.GlcodegenerationService.getItems().subscribe(
                                            data => {
                                                this.glcodeList = data['result']['data'];
                                                this.TaxsetupcodeService.getItems().subscribe(
                                                    data => {
                                                        this.taxcodeList = data['result']['data'];
                                                        if (this.id > 0) {
                                                            this.VouchergenerationService.getDetail(this.id).subscribe(
                                                                data => {
                                                                    this.voucherGenerationList = data['result'];
                                                                    this.voucherGenerationDataheader['f071fvoucherNumber'] = data['result'][0].f071fvoucherNumber;
                                                                    this.voucherGenerationDataheader['f071fvoucherType'] = data['result'][0].f071fvoucherType;
                                                                    this.voucherGenerationDataheader['f071fidCustomer'] = data['result'][0].f071fidCustomer;
                                                                    this.voucherGenerationDataheader['f071fvoucherTotal'] = data['result'][0].f071fvoucherTotal;
                                                                    this.voucherGenerationDataheader['f071fvoucherDate'] = data['result'][0].f071fvoucherDate;
                                                                    this.voucherGenerationDataheader['f071fid'] = data['result'][0].f071fid;
                                                                    this.customerEnable();
                                                                }, error => {
                                                                    console.log(error);
                                                                });
                                                        }
                                                    }
                                                );
                                            }
                                        );
                                    }
                                );

                            }
                        );
                    }
                );
            }
        );
        // ClaimCustomer dropdown


    }

    customerEnable() {

        this.typeId = this.voucherGenerationDataheader['f071fvoucherType'];
        console.log(this.typeId);
        this.DefinationmsService.getItemsDetail(this.typeId).subscribe(
            data => {
                this.typeList = data['result'];
                this.payType = data['result'].f011fdefinitionCode;
                return this.payType;
            }, error => {
                console.log(error);
            });

    }
    getVoucherNumber() {
        let typeObject = {};
        if (this.payType = "Student") {
            alert("student");
            typeObject['type'] = "Student";
        }
        else if (this.payType = "Sponser") {
            typeObject['type'] = "Sponser";
        }
        else if (this.payType = "Other Receivables") {
            typeObject['type'] = "Other Receivables";
        } else {

            typeObject['type'] = "Other Receivables";


            this.VouchergenerationService.getVoucherNumber(typeObject).subscribe(
                data => {
                    this.voucherGenerationDataheader['f071fvoucherNumber'] = data['number'];
                    this.valueDate = new Date().toISOString().substr(0, 10);
                    this.voucherGenerationDataheader['f071fvoucherDate'] = this.valueDate;
                }
            );
        }
    }

    ConvertToFloat(val) {
        return parseFloat(val);
    }

    saveVoucher() {

        let voucherObject = {};
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        console.log(this.id);
        if (this.id > 0) {
            voucherObject['f071fid'] = this.id;
        }
        voucherObject['f071fvoucherNumber'] = this.voucherGenerationDataheader['f071fvoucherNumber'];
        voucherObject['f071fvoucherType'] = this.voucherGenerationDataheader['f071fvoucherType'];
        voucherObject['f071fidCustomer'] = this.voucherGenerationDataheader['f071fidCustomer'];
        voucherObject['f071fvoucherTotal'] = this.voucherGenerationDataheader['f071fvoucherTotal'];
        voucherObject['f071fvoucherDate'] = this.voucherGenerationDataheader['f071fvoucherDate'];
        voucherObject['f071fstatus'] = 0;
        voucherObject['voucher-details'] = this.voucherGenerationList;

        console.log(this.voucherGenerationList);
        if (this.id > 0) {
            this.VouchergenerationService.updateVoucherItems(voucherObject, this.id).subscribe(
                data => {

                    this.router.navigate(['accountsrecivable/vouchergeneration']);
                    this.alertService.success(" Updated Sucessfully ! ");

                }, error => {
                    console.log(error);
                });
        } else {
            this.VouchergenerationService.insertVoucherItems(voucherObject).subscribe(
                data => {


                    this.router.navigate(['accountsrecivable/vouchergeneration']);
                    this.alertService.success(" Added Sucessfully ! ");

                }, error => {
                    console.log(error);
                });
        }



    }


    getGstvalue() {
        let taxId = this.voucherGenerationData['f072fgstCode'];
        let quantity = this.voucherGenerationData['f072fquantity'];
        let price = this.voucherGenerationData['f072fprice'];
        //console.log(this.taxcodeList);
        var taxSelectedObject = {};
        for (var k = 0; k < this.taxcodeList.length; k++) {
            if (this.taxcodeList[k]['f081fid'] == taxId) {
                taxSelectedObject = this.taxcodeList[k];
            }
        }


        this.gstValue = taxSelectedObject['f081fpercentage'];
        this.voucherGenerationData['f072fgstValue'] = this.gstValue;
        this.Amount = (this.ConvertToFloat(price)) * (this.ConvertToFloat(quantity));
        this.taxAmount = (this.ConvertToFloat(this.gstValue)) / 100 * this.ConvertToFloat((this.Amount));

        let totalAm = ((this.taxAmount) + (this.Amount));
        this.voucherGenerationData['exTotalAmount'] = this.Amount.toFixed(2);
        this.voucherGenerationData['taxAmount'] = this.taxAmount.toFixed(2);
        this.voucherGenerationData['f072ftotal'] = totalAm.toFixed(2);



    }

    onBlurMethod() {
        console.log(this.voucherGenerationList);
        var finaltotal = 0;
        for (var i = 0; i < this.voucherGenerationList.length; i++) {
            var totalamount = (this.ConvertToFloat(this.voucherGenerationList[i]['f072fquantity'])) * (this.ConvertToFloat(this.voucherGenerationList[i]['f072fprice']));
            var gstamount = (this.ConvertToFloat(this.gstValue) / 100 * (totalamount));
            this.voucherGenerationList[i]['f072ftotal'] = gstamount + totalamount;
            //console.log(gstamount);
            //console.log(totalamount)
            finaltotal = finaltotal + this.voucherGenerationList[i]['f072ftotal'];
        }

        this.voucherGenerationDataheader['f071fvoucherTotal'] = this.ConvertToFloat(finaltotal.toFixed(2));
    }

    deleteVoucher(object) {
        var cnf = confirm("Do you really want to delete");
        if (cnf == true) {
            console.log(this.voucherGenerationList);
            var index = this.voucherGenerationList.indexOf(object);;
            if (index > -1) {
                this.voucherGenerationList.splice(index, 1);
            }
            this.onBlurMethod();
        }
    }

    addvoucherlist() {
        console.log(this.voucherGenerationData);

        var dataofCurrentRow = this.voucherGenerationData;
        this.voucherGenerationData = {};
        this.voucherGenerationList.push(dataofCurrentRow);
        this.onBlurMethod();
    }
}