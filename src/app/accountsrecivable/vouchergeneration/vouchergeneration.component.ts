import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { VouchergenerationService } from './../service/vouchergeneration.service';
import { ActivatedRoute, Router } from '@angular/router';


@Component({
    selector: 'VouchergenerationComponent',
    templateUrl: 'vouchergeneration.component.html'
})

export class VouchergenerationComponent implements OnInit {
    voucherGenerationList =  [];
    voucherGenerationData = {};
    id: number;
    title: string;
    type: string;

    constructor(
        
        private VouchergenerationService: VouchergenerationService,
        private route: ActivatedRoute
    ) { }

    ngOnInit() { 
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        
        this.VouchergenerationService.getItems().subscribe(
            data => {
                
                this.voucherGenerationList = data['result']['data'];
        }, error => {
            console.log(error);
        });
    }

    
}