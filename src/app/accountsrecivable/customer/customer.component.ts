import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CustomerService } from '../service/customer.service'
import { NgxSpinnerService } from 'ngx-spinner';
@Component({
    selector: 'Customer',
    templateUrl: 'customer.component.html'
})

export class CustomerComponent implements OnInit {
    customerList = [];
    customerData = {};
    id: number;

    constructor(
        private CustomerService: CustomerService,
        private spinner: NgxSpinnerService,

    ) { }
    ngOnInit() {
        this.spinner.show();
        this.CustomerService.getItems().subscribe(
            data => {
                this.spinner.hide();
                this.customerList = data['result']['data'];
            }, error => {
                console.log(error);
            });
    }
}