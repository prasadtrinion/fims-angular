import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { CustomerService } from '../../service/customer.service'
import { CountryService } from '../../../generalsetup/service/country.service'
import { StateService } from '../../../generalsetup/service/state.service'
import { SupplierregistrationService } from '../../../procurement/service/supplierregistration.service';
import { BankService } from '../../../generalsetup/service/bank.service';
import { DebtorCategoryService } from "../../service/debtorcategory.service";
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { DefinationmsService } from '../../../studentfinance/service/definationms.service';

@Component({
    selector: 'CustomerFormComponent',
    templateUrl: 'customerform.component.html'
})

export class CustomerFormComponent implements OnInit {
    customerList = [];
    customerData = {};
    countryList = [];
    countryData = {};
    stateList = [];
    stateData = [];
    debtorcategoryList=[];
    supplierList = [];
    bankList = [];
    id: number;
    editDisbled: boolean;
    ajaxCount: number;
    vehicleConditionDiv: boolean;
    constructor(
        private CustomerService: CustomerService,
        private CountryService: CountryService,
        private StateService: StateService,
        private route: ActivatedRoute,
        private BankService: BankService,
        private DebtorCategoryService: DebtorCategoryService,
        private router: Router,
        private spinner: NgxSpinnerService,
        private SupplierregistrationService: SupplierregistrationService,
        private DefinationmsService: DefinationmsService,

    ) { }
    ngDoCheck() {
        if (this.ajaxCount == 0) {
            this.spinner.hide();
        }
    }
    ngOnInit() {
        this.editDisbled = false;
        this.ajaxCount = 0
        this.spinner.show();
        this.customerData['f021fstatus'] = 1;
        this.customerData['f021fsupplier'] = 1;
        this.customerData['f021fidTaxFree'] = 1;
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        // country dropdown
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.CountryService.getActiveCountryList().subscribe(
            data => {
                this.countryList = data['result']['data'];
            }, error => {
                console.log(error);
            });
        this.SupplierregistrationService.getActiveItems().subscribe(
            data => {
                this.supplierList = data['result']['data'];
            }
        );
        //banksetup account dropdown
        this.BankService.getActiveBank().subscribe(
            data => {
                this.bankList = data['result']['data'];
            }, error => {
                console.log(error);
            });
        // state dropdown
        // this.StateService.getActiveStateList().subscribe(
        //     data => {
        //         this.stateList = data['result']['data'];
        //     }, error => {
        //         console.log(error);
        //     });
        //categories of debtor dropdown
        this.DebtorCategoryService.getActiveItems().subscribe(
            data => {
                this.debtorcategoryList = data['result'];

            }, error => {
                console.log(error);
            });

        if (this.id > 0) {
            this.ajaxCount++;
            this.CustomerService.getItemsDetail(this.id).subscribe(
                data => {
                    this.ajaxCount--;
                    this.customerData = data['result'];
                    if (this.customerData['f021fsupplier'] == true) {
                        this.customerData['f021fsupplier'] = 1;
                    } else {
                        this.customerData['f021fsupplier'] = 0;
                    }
                    if (this.customerData['f021fidTaxFree'] == true) {
                        this.customerData['f021fidTaxFree'] = 1;
                    } else {
                        this.customerData['f021fidTaxFree'] = 0;
                    }
                    if (data['result'].f021fstatus === true) {
                        this.customerData['f021fstatus'] = 1;
                    } else {
                        this.customerData['f021fstatus'] = 0;
                    }
                    this.onCountryChange();
                    this.editDisbled = true;
                    // this.customerData['f021fidSupplier'] = parseInt(this.customerData['f021fidSupplier']).toString();
                }, error => {
                    console.log(error);
                });
        }
    }
    onCountryChange() {
        if (this.customerData['f021fcountry'] == undefined) {
            this.customerData['f021fcountry'] = 0;
        }
        this.StateService.getStates(this.customerData['f021fcountry']).subscribe(
            data => {
                this.stateList = data['result'];
                let other = {
                    'f012fid': "1",
                    "f012fshortName": "SH",
                    "f012fstateName": "Others"
                };
                this.stateList.push(other);
            }
        );
    }
    addCustomer() {
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.CustomerService.updateCustomerItems(this.customerData, this.id).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert(' Debtor Code Already Exist');
                    }
                    else {
                        this.router.navigate(['accountsrecivable/customer']);
                        alert("Debtor Profile has been Updated Successfully");
                    }
                }, error => {
                    console.log(error);
                    alert(' Debtor Code Already Exist');
                });
        } else {
            this.CustomerService.insertCustomerItems(this.customerData).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert(' Debtor Code Already Exist');
                    }
                    else {
                        this.router.navigate(['accountsrecivable/customer']);
                        alert("Debtor Profile has been Added Successfully");
                    }

                }, error => {
                    console.log(error);
                });

        }

    }
    onlyNumberKey(event) {
        if (event.charCode == 46) {

        } else {
            return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
        }
    }
    checkDuplication() {
        let duplicationObj = {};
        if (this.id > 0) {
            duplicationObj['id'] = this.id;
        }
        duplicationObj['key'] = 'Debtor IC';
        duplicationObj['ic'] = this.customerData['f021flastName'];
        if(this.customerData['f021flastName'] != undefined && this.customerData['f021flastName'] != ""){
            this.DefinationmsService.checkDuplication(duplicationObj).subscribe(
                data => {
                    if (parseInt(data['result']) == 1) {
                        alert("IC Already Exist");
                        this.customerData['f021flastName'] = '';
                    }
                }, error => {
                    console.log(error);
                });
            }
    }
    checkDuplication1() {
        let duplicationObj = {};
        if (this.id > 0) {
            duplicationObj['id'] = this.id;
        }
        duplicationObj['key'] = 'Debtor Email';
        duplicationObj['email'] = this.customerData['f021femailId'];
        if(this.customerData['f021femailId'] != undefined && this.customerData['f021femailId'] != ""){
            this.DefinationmsService.checkDuplication(duplicationObj).subscribe(
                data => {
                    if (parseInt(data['result']) == 1) {
                        alert("Email Already Exist");
                        this.customerData['f021femailId'] = '';
                    }
                }, error => {
                    console.log(error);
                });
            }
    }
}