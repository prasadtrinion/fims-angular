import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { CustomerService } from '../../../accountsrecivable/service/customer.service'
import { ActivatedRoute, Router } from '@angular/router';
import { CreditnoteapprovalService } from '../../service/creditnoteapproval.service';
import { ItemService } from '../../service/item.service';
import { MaininvoiceService } from '../../service/maininvoice.service';
import { GlcodegenerationService } from '../../../generalledger/service/glcodegeneration.service';
import { UserService } from '../../../generalsetup/service/user.service';
@Component({
    selector: 'CreditnoteapprovalformComponent',
    templateUrl: 'creditnoteapprovalform.component.html'
})

export class CreditnoteapprovalformComponent implements OnInit {
    creditnoteData = {};
    customerList = [];
    maininvoiceList = [];
    maininvoiceData = {};
    customerData = {};
    glcodeList = [];
    glcodeData = {};
    itemList = [];
    userData = {};
    userList = [];
    creditNotelist = [];
    title = '';
    editId: number;
    openingbalanceData = {};
    newOpeningBalanceList = [];
    saveDataObject = {};

    id: number;
    showCrReadonly: boolean;
    showDrReadonly: boolean;
    balance: number;
    cnamount: number;
    newbal: number;
    cash: number;
    creditnoteAddedAmount: number;
    payment: number;
    type: string;
    valueDate: string;

    constructor(

        private GlcodegenerationService: GlcodegenerationService,
        private CreditnoteService: CreditnoteapprovalService,
        private ItemService: ItemService,
        private MaininvoiceService: MaininvoiceService,
        private CustomerService: CustomerService,
        private UserService: UserService,
        private route: ActivatedRoute,
        private router: Router
    ) { }

    ngOnInit() {
        this.creditnoteAddedAmount = 0;
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        this.route.paramMap.subscribe((data) => this.editId = + data.get('editId'));

        this.valueDate = new Date().toISOString().substr(0, 10);
        console.log(this.valueDate);
        this.creditnoteData['f031fdate'] = this.valueDate;

        //invoice dropdown
        this.MaininvoiceService.getItems().subscribe(
            data => {
                this.maininvoiceList = data['result']['data'];
            }
        );
        //user dropdown
        this.UserService.getItems().subscribe(
            data => {
                this.userList = data['result']['data'];
            }
        );
        //item dropdown

        this.ItemService.getItems().subscribe(
            data => {
                this.itemList = data['result'];
                this.GlcodegenerationService.getItems().subscribe(
                    data => {
                        this.glcodeList = data['result']['data'];
                        this.CustomerService.getItems().subscribe(
                            data => {
                                this.customerList = data['result']['data'];
                                if (this.editId > 0) {
                                    this.CreditnoteService.getItemsDetail(this.editId).subscribe(
                                        data => {
                                            console.log(this.editId);
                                            this.creditnoteData = data['result'];
                                            this.creditNotelist = data['result'];

                                            this.creditnoteData['f031fbalance'] = data['result'][0]['f031fbalance'],
                                                this.creditnoteData['f031fdate'] = data['result'][0]['f031fdate'],
                                                this.creditnoteData['f031fidInvoice'] = data['result'][0]['f031fidInvoice']
                                            this.creditnoteData['f031fnarration'] = data['result'][0]['f031fnarration'],
                                                this.creditnoteData['f031freferenceNumber'] = data['result'][0]['f031freferenceNumber'],
                                                this.creditnoteData['f031ftotalAmount'] = data['result'][0]['f031ftotalAmount'],
                                                this.creditnoteData['f031fvoucherNumber'] = data['result'][0]['f031fvoucherNumber'],
                                                this.creditnoteData['f031fidInvoice'] = data['result'][0]['f071fidCustomer']
                                            this.maininvoiceList = [];

                                            for (var i = 0; i < this.creditNotelist.length; i++) {
                                                console.log(this.creditNotelist[i]);
                                                var creditnoteObject = {};
                                                creditnoteObject['f072fidItem'] = this.creditNotelist[i]['f032fidItem'];
                                                creditnoteObject['f072fidDebitGlcode'] = this.creditNotelist[i]['f032fidDebitGlcode'];
                                                creditnoteObject['f072fidCreditGlcode'] = this.creditNotelist[i]['f032fidCreditGlcode'];
                                                creditnoteObject['f072fquantity'] = this.creditNotelist[i]['f032fquantity'];
                                                creditnoteObject['f072fprice'] = this.creditNotelist[i]['f032fprice'];
                                                creditnoteObject['f072fgstCode'] = this.creditNotelist[i]['f032fgstCode'];
                                                creditnoteObject['f072fgstValue'] = this.creditNotelist[i]['f032fgstValue'];
                                                creditnoteObject['f072ftotal'] = this.creditNotelist[i]['f032ftotal'];
                                                creditnoteObject['f032ftoCnAmount'] = this.creditNotelist[i]['f032ftoCnAmount'];

                                                this.maininvoiceList.push(creditnoteObject);
                                            }

                                            console.log(this.maininvoiceList);

                                        }, error => {
                                            console.log(error);
                                        });
                                }
                            }
                        );
                    }
                );
            }
        );

    }
    getInvoiceDetails() {
        let invid = 0;
        invid = this.creditnoteData['f031fidInvoice'];
        console.log(invid);
        this.MaininvoiceService.getInvoiceDetailsById(invid).subscribe(
            data => {
                console.log(data);
                this.maininvoiceList = data['result'];
                for (var i = 0; i < this.maininvoiceList.length; i++) {
                    this.maininvoiceList[i]['f032ftoCnAmount'] = 0;
                }
                this.creditnoteData['f031fcustomer'] = data['result'][0]['f071fidCustomer'],
                    this.creditnoteData['f031ftotalAmount'] = data['result'][0]['f071finvoiceTotal'],
                    this.creditnoteData['invoiceDate'] = data['result'][0]['f071finvoiceDate'],
                    this.creditnoteData['f031fbalance'] = data['result'][0]['f071fbalance']

            }
        );
    }
    addCreditAmount() {
        this.creditnoteAddedAmount = 0;
        for (var i = 0; i < this.maininvoiceList.length; i++) {
            this.creditnoteAddedAmount = this.ConvertToFloat(this.creditnoteAddedAmount) + this.ConvertToFloat(this.maininvoiceList[i]['f032ftoCnAmount']);

            if (this.creditnoteAddedAmount > this.creditnoteData['f031fbalance']) {
                alert("asdf");
            }
        }
        this.creditnoteData['f031fbalance'] = this.ConvertToFloat(this.creditnoteData['f031ftotalAmount']) - this.ConvertToFloat(this.creditnoteAddedAmount);
    }

    saveData() {
            this.saveDataObject["f031fidInvoice"] = this.creditnoteData['f031fidInvoice'],
            this.saveDataObject["f031ftotalAmount"] = this.creditnoteData['f031ftotalAmount'],
            this.saveDataObject["f031fbalance"] = this.creditnoteData['f031fbalance'],
            this.saveDataObject["f031freferenceNumber"] = this.creditnoteData['f031freferenceNumber'],
            this.saveDataObject["f031fdate"] = this.creditnoteData['f031fdate'],
            this.saveDataObject["f031fvoucherNumber"] = this.creditnoteData['f031fvoucherNumber'],
            this.saveDataObject["f031fnarration"] = this.creditnoteData['f031fnarration'],
            this.saveDataObject["f031fstatus"] = 0

        for (var i = 0; i < this.maininvoiceList.length; i++) {
            var creditnoteObject = {}
            creditnoteObject['f032fidItem'] = this.maininvoiceList[i]['f072fidItem'];
            creditnoteObject['f032fidDebitGlcode'] = this.maininvoiceList[i]['f072fidCreditGlcode'];
            creditnoteObject['f032fidCreditGlcode'] = this.maininvoiceList[i]['f072fidDebitGlcode'];
            creditnoteObject['f032fquantity'] = this.maininvoiceList[i]['f072fquantity'];
            creditnoteObject['f032fprice'] = this.maininvoiceList[i]['f072fprice'];
            creditnoteObject['f032fgstCode'] = this.maininvoiceList[i]['f072fgstCode'];
            creditnoteObject['f032fgstValue'] = this.maininvoiceList[i]['f072fgstValue'];
            creditnoteObject['f032ftotal'] = this.maininvoiceList[i]['f072ftotal'];
            creditnoteObject['f032ftoCnAmount'] = this.maininvoiceList[i]['f032ftoCnAmount'];

            this.creditNotelist.push(creditnoteObject);
        }
        this.saveDataObject["details"] = this.creditNotelist;
        console.log(this.saveDataObject);

    }
    ConvertToFloat(val) {
        return parseFloat(val);
    }



}