import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Injector } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CreditnoteapprovalService } from '../service/creditnoteapproval.service'
import { faSearch, faEdit } from '@fortawesome/free-solid-svg-icons';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'CreditnoteapprovalComponent',
    templateUrl: 'creditnoteapproval.component.html'
})

export class CreditnoteapprovalComponent implements OnInit {
    creditnoteApprovalList = [];
    creditnoteApprovalData = {};
    invoiceapprovalData = [];
    id: number;
    selectAllCheckbox: false;
    type: string;
    title = '';
    faSearch = faSearch;
    faEdit = faEdit;
    constructor(
        private CreditnoteapprovalService: CreditnoteapprovalService,
        private spinner: NgxSpinnerService,
        private route: ActivatedRoute,
        private router: Router
    ) { }
    ngOnInit() {
        this.getList();
    }
    getList() {
        this.spinner.show();                                
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        this.CreditnoteapprovalService.getItems().subscribe(
            data => {
                this.spinner.hide();
                let activeData = [];
                activeData = data['result'];
                for (var i = 0; i < activeData.length; i++) {
                    if (activeData[i]['f071fstatus'] == 0) {
                        activeData[i]['f071fstatus'] = 'Pending';
                    } else if (activeData[i]['f071fstatus'] == 1) {
                        activeData[i]['f071fstatus'] = 'Approved';
                    } else {
                        activeData[i]['f071fstatus'] = 'Rejected';
                    }
                    if (activeData[i]['f071finvoiceType'] == 'CSTA') {
                        activeData[i]['f071finvoiceType'] = 'Staff';

                    } else if (activeData[i]['f071finvoiceType'] == 'CSTD') {
                        activeData[i]['f071finvoiceType'] = 'Student';
                    }
                    else {
                        if (activeData[i]['f071finvoiceType'] == 'COR') {
                            activeData[i]['f071finvoiceType'] = 'Other Receivables';
                        }
                    }
                }
                this.creditnoteApprovalList = activeData;
            }, error => {
                console.log(error);
            });
    }
    ApprovalListData() {
        let approveDarrayList = [];
        for (var i = 0; i < this.creditnoteApprovalList.length; i++) {
            if (this.creditnoteApprovalList[i].f031fstatus == true) {
                approveDarrayList.push(this.creditnoteApprovalList[i].f031fid);
            }
        }
        if (approveDarrayList.length < 1) {
            alert("Select atleast one Credit Note to Approve");
            return false;
        }
        var confirmPop = confirm("Do you want to Approve?");
        if (confirmPop == false) {
            return false;
        }
        var approvalObject = {};
        approvalObject['id'] = approveDarrayList;
        approvalObject['status'] = '1';
        approvalObject['reason'] = '';

        this.CreditnoteapprovalService.updateApproval(approvalObject).subscribe(
            data => {
                this.getList();
                alert("Approved Successfully");
                this.invoiceapprovalData['reason'] = '';
            }, error => {
                console.log(error);
            });

    }
    regectJournalApprovalListData() {
        let approveDarrayList = [];
        for (var i = 0; i < this.creditnoteApprovalList.length; i++) {
            if (this.creditnoteApprovalList[i].f031fstatus == true) {
                approveDarrayList.push(this.creditnoteApprovalList[i].f031fid);
            }
        }
        if (approveDarrayList.length < 1) {
            alert("Select atleast one Credit Note to Reject");
            return false;
        }
        if (this.invoiceapprovalData['reason'] == undefined) {
            alert('Enter the Description');
            return false;
        }
        var confirmPop = confirm("Do you want to Reject?");
        if (confirmPop == false) {
            return false;
        }
        var creditnoteDataObject = {}
        creditnoteDataObject['id'] = approveDarrayList;
        creditnoteDataObject['status'] = '2';
        creditnoteDataObject['reason'] = this.invoiceapprovalData['reason'];
        this.CreditnoteapprovalService.updateApproval(creditnoteDataObject).subscribe(
            data => {
                this.getList();
                this.invoiceapprovalData['reason'] = '';
                alert("Rejected Successfully");
            }, error => {
                console.log(error);
            });
    }
}