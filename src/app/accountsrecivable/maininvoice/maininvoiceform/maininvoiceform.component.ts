import { Injector } from '@angular/core';
import { Component, OnInit, ViewEncapsulation  } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { VoucherService } from '../../service/voucher.service';
import { MaininvoiceService } from '../../service/maininvoice.service';
import { CustomerService } from '../../../accountsrecivable/service/customer.service';
import { GlcodegenerationService } from '../../../generalledger/service/glcodegeneration.service';
import { TaxsetupcodeService } from '../../../generalledger/service/taxsetupcode.service'
import { FeeitemService } from '../../../studentfinance/service/feeitem.service';
import { StudentService } from '../../../studentfinance/service/student.service';
import { SponsorService } from '../../../studentfinance/service/sponsor.service';
import { FundService } from '../../../generalsetup/service/fund.service';
import { AccountcodeService } from '../../../generalsetup/service/accountcode.service'
import { ActivitycodeService } from '../../../generalsetup/service/activitycode.service';
import { DepartmentService } from '../../../generalsetup/service/department.service';
import { RevenuesetupService } from '../../service/revenuesetup.service';
import { MaininvoiceapprovalService } from '../../service/maininvoiceapproval.service';
import { RevenueTypeService } from "../../service/revenuetype.service";
import { RequestinvoiceService } from "../../service/requestinvoice.service ";
import { count } from 'rxjs/operators';
import { StaffService } from "../../service/staff.service";
import { IfStmt } from '@angular/compiler';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'MaininvoiceformComponent',
    templateUrl: 'maininvoiceform.component.html',
    //encapsulation: ViewEncapsulation.None,
    styles: [`
    .dark-modal .modal-content {
      background-color: #292b2c;
      color: white;
    }
    .dark-modal .close {
      color: white;
    }
    .light-blue-backdrop {
      background-color: #5cb3fd;
    }
  `]
})

export class MaininvoiceformComponent implements OnInit {
    invoiceList = [];
    invoiceData = {};
    invoiceDataheader = {};
    glcodeList = [];
    itemList = [];
    deleteList = [];
    customerList = [];
    studentList = [];
    sponserList = [];
    smartStaffList = [];
    taxcodeList = [];
    feeItemList = [];
    DeptList = [];
    accountcodeList = [];
    fundList = [];
    activitycodeList = [];
    invoicetype = [];
    billtype = [];
    maininvoiceApprovalList = [];
    maininvoiceApprovalData = {};
    invoiceapprovalData = {};
    glCodeList = [];
    revenuetypeList = [];
    customertype = [];
    requestInvoiceList = [];
    maininvoiceList = [];
    InvoiceListBasedonId = [];
    UserId: number;
    id: number;
    viewid: number;
    valueDate: string;
    ajaxCount: number;
    gstValue: number;
    Amount: number;
    taxAmount: number;
    totalAmount: number;
    type: string;
    saveBtnDisable: boolean;
    viewDisabled: boolean;
    editDisabled: boolean;
    showLastRow: boolean;
    showLastRowMinus: boolean;
    showLastRowPlus: boolean;
    listshowLastRowMinus: boolean;
    listshowLastRowPlus: boolean;
    invoicetypeDisabled: boolean;
    closeResult: string;
    constructor(
        private GlcodegenerationService: GlcodegenerationService,
        private MaininvoiceService: MaininvoiceService,
        private VoucherService: VoucherService,
        private TaxsetupcodeService: TaxsetupcodeService,
        private CustomerService: CustomerService,
        private DepartmentService: DepartmentService,
        private ActivitycodeService: ActivitycodeService,
        private AccountcodeService: AccountcodeService,
        private RevenuesetupService: RevenuesetupService,
        private MaininvoiceapprovalService: MaininvoiceapprovalService,
        private FundService: FundService,
        private FeeitemService: FeeitemService,
        private StudentService: StudentService,
        private SponsorService: SponsorService,
        private StaffService: StaffService,
        private RevenueTypeService: RevenueTypeService,
        private RequestinvoiceService: RequestinvoiceService,
        private spinner: NgxSpinnerService,
        private route: ActivatedRoute,
        private router: Router,
        private modalService: NgbModal

    ) { }
    
    openAddNewModal(content){
        this.modalService.open(content, {
            backdropClass: 'custom-backdrop',
            windowClass: 'custom-modal',
            size: 'lg'
        });
    }

    ngDoCheck() {
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.route.paramMap.subscribe((data) => this.viewid = + data.get('viewid'));
        console.log(this.viewid);
        const change = this.ajaxCount;
        console.log(this.ajaxCount);
        if (this.ajaxCount == 0 && this.invoiceList.length > 0) {
            this.ajaxCount = 20;
            console.log("asdfsdf");
            this.showIcons();
            if (this.invoiceList[0]['f071fstatus'] == 1 || this.invoiceList[0]['f071fstatus'] == 2) {
                this.saveBtnDisable = true;
                this.viewDisabled = true;
                this.editDisabled = true;
                $("#target input,select").prop("disabled", true);
                $("#target1 input,select").prop("disabled", true);
            }
        }
        if (this.ajaxCount == 0 && this.invoiceList.length > 0) {
            this.ajaxCount = 20;
            if (this.viewid > 1) {
                this.saveBtnDisable = true;
                this.viewDisabled = true;
                this.listshowLastRowPlus = false;
                this.editDisabled = true;
                $("#target input,select").prop("disabled", true);
                $("#target1 input,select").prop("disabled", true);
            }
        }
        // if (this.ajaxCount == 0) {
        //     this.editFunction();
        //     this.spinner.hide();
        //     this.ajaxCount = 10;
        // }
    }
    deletemodule() {
        this.showLastRow = true;
        this.showIcons();
    }

    showIcons() {
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.route.paramMap.subscribe((data) => this.viewid = + data.get('viewid'));

        console.log(this.invoiceList.length);
        console.log("Show last row" + this.showLastRow);
        console.log("showLastRowMinus" + this.showLastRowMinus);
        console.log("showLastRowPlus" + this.showLastRowPlus);
        console.log("listshowLastRowMinus" + this.listshowLastRowMinus);
        console.log("listshowLastRowPlus" + this.listshowLastRowPlus);

        if (this.invoiceList.length > 0) {
            this.showLastRowMinus = true;
            this.showLastRowPlus = true;
            this.listshowLastRowMinus = true;
        }

        if (this.invoiceList.length > 1) {
            if (this.showLastRow == false) {
                this.listshowLastRowPlus = false;
                this.listshowLastRowMinus = true;
                this.showLastRowPlus = true;

            } else {
                this.listshowLastRowPlus = true;
                this.listshowLastRowMinus = false;

            }
        }
        if (this.invoiceList.length == 1 && this.showLastRow == false) {
            this.listshowLastRowMinus = true;
            this.listshowLastRowPlus = false;

        }
        if (this.invoiceList.length == 1 && this.showLastRow == true) {
            this.listshowLastRowMinus = false;
            this.listshowLastRowPlus = true;

        }
        if (this.invoiceList.length == 0 && this.showLastRow == false) {
            this.showLastRowMinus = false;
            this.showLastRowPlus = true;
        }
        if (this.invoiceList.length == 0 && this.showLastRow == true) {
            this.showLastRowMinus = true;
            this.showLastRowPlus = false;
        }
        if (this.invoiceList.length > 0) {
            if (this.viewDisabled == true || this.invoiceList[0]['f071fstatus'] == 1 || this.invoiceList[0]['f071fstatus'] == 2) {
                this.listshowLastRowPlus = false;
            }
        }
        else {
            if (this.viewDisabled == true) {
                this.listshowLastRowPlus = false;
            }
        }
    }
    showNewRow() {
        var confirmPop = confirm("Do you want to Add?");
        if (confirmPop == false) {
            return false;
        }
        this.showLastRow = false;
        this.showIcons();
    }
    editFunction() {
        this.ajaxCount++;
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.route.paramMap.subscribe((data) => this.viewid = + data.get('viewid'));
        if (this.id > 0) {
            this.MaininvoiceService.getInvoiceDetailsById(this.id).subscribe(
                data => {
                    this.ajaxCount--;
                    this.invoiceList = data['result'];
                    this.invoiceDataheader['f071ftype'] = parseInt(data['result'][0].f071ftype);
                    this.invoiceDataheader['f071fapprovedBy'] = data['result'][0].f071fapprovedBy;
                    this.invoiceDataheader['f071finvoiceDate'] = data['result'][0].f071finvoiceDate;
                    this.invoiceDataheader['f071fidRequestInvoice'] = data['result'][0].f071fidRequestInvoice;
                    this.invoiceDataheader['f071finvoiceTotal'] = data['result'][0].f071finvoiceTotal;
                    this.invoiceDataheader['f071finvoiceType'] = data['result'][0].f071finvoiceType;
                    this.invoiceDataheader['f071fdescription'] = data['result'][0].f071fdescription;
                    this.invoiceDataheader['f071finvoiceNumber'] = data['result'][0].f071finvoiceNumber;
                    this.invoiceDataheader['f071fbillType'] = data['result'][0].f071fbillType;
                    this.type = data['result'][0]['f071finvoiceType'];

                    if (this.invoiceDataheader['f071finvoiceType'] == 'KI' ||
                        this.invoiceDataheader['f071finvoiceType'] == 'CI' ||
                        this.invoiceDataheader['f071finvoiceType'] == 'PI') {
                        this.invoiceDataheader['f071finvoiceType'] = 'OR';
                    }
                    this.invoiceDataheader['f071fidCustomer'] = data['result'][0].f071fidCustomer;
                    this.invoiceDataheader['f071fbillType'] = data['result'][0].f071fbillType;

                    this.invoiceDataheader['f071fid'] = data['result'][0].f071fid;
                    this.invoiceDataheader['f072fidItem'] = parseInt(this.invoiceDataheader['f072fidItem']).toString();

                    for (var i = 0; i < this.invoiceList.length; i++) {
                        this.invoiceList[i]['f072fgstCode'] = parseInt(this.invoiceList[i]['f072fgstCode']);
                    }
                    if (data['result'][0]['f071fstatus'] == 1 || data['result'][0]['f071fstatus'] == 2) {
                        this.viewDisabled = true;
                        this.editDisabled = true;
                        $("#target input,select").prop("disabled", true);
                        $("#target1 input,select").prop("disabled", true);
                    }
                    if (this.viewid == 2) {
                        this.viewDisabled = true;
                        this.editDisabled = true;
                        $("#target input,select").prop("disabled", true);
                        $("#target1 input,select").prop("disabled", true);
                    }
                    if (this.id > 0) {
                        this.editDisabled = true;
                    }
                    this.showIcons();
                    this.onBlurMethod();
                    this.getInvoiceList();
                    console.log(this.invoiceList);
                }, error => {
                    console.log(error);
                });

        }
    }

    ngOnInit() {
        this.invoiceDataheader['f071ftype'] = 1;
        this.ajaxCount = 0
        // this.spinner.show();
        this.viewDisabled = false;
        this.editDisabled = false;
        let invoicetypeobj = {};
        invoicetypeobj['name'] = 'Student';
        invoicetypeobj['type'] = 'STD';
        this.invoicetype.push(invoicetypeobj);
        invoicetypeobj = {};
        invoicetypeobj['name'] = 'Staff';
        invoicetypeobj['type'] = 'STA';
        this.invoicetype.push(invoicetypeobj);
        invoicetypeobj = {};
        // invoicetypeobj['name'] = 'Sponser';
        // invoicetypeobj['type'] = 'SP';
        // this.invoicetype.push(invoicetypeobj);
        invoicetypeobj = {};
        invoicetypeobj['name'] = 'Other Receivables';
        invoicetypeobj['type'] = 'OR';
        this.invoicetype.push(invoicetypeobj);

        // let custtypeobj = {};
        // invoicetypeobj['name'] = 'Student';
        // invoicetypeobj['type'] = 'STD';
        // this.customertype.push(custtypeobj);
        // invoicetypeobj = {};
        // invoicetypeobj['name'] = 'Staff';
        // invoicetypeobj['type'] = 'STA';
        // this.customertype.push(custtypeobj);
        // invoicetypeobj = {};
        // invoicetypeobj['name'] = 'Sponsor';
        // invoicetypeobj['type'] = 'SP';
        // this.customertype.push(custtypeobj);
        invoicetypeobj = {};

        let billtypeobj = {};
        billtypeobj['name'] = 'Student';
        this.billtype.push(billtypeobj);
        this.viewDisabled = false;
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.route.paramMap.subscribe((data) => this.viewid = + data.get('viewid'));
        if (this.id < 1) {
            this.showLastRow = false;
            this.showLastRowMinus = false;
            this.showLastRowPlus = true;
        } else {
            this.showLastRow = true;

        }
        if(this.id > 0){
            this.editFunction();
        }
        console.log(this.id);

        //revenue type dropdown
        this.RevenueTypeService.getItems().subscribe(
            data => {
                this.revenuetypeList = data['result'];
            }, error => {
                console.log(error);
            });
        //revenue type dropdown
        // this.RequestinvoiceService.getApproveItems().subscribe(
        //     data => {
        //         this.requestInvoiceList = data['result'];
        //     }, error => {
        //         console.log(error);
        //     });
        this.ajaxCount = 0;
        //item dropdown
        this.ajaxCount++;
        this.RevenuesetupService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                console.log(this.ajaxCount);

                this.itemList = data['result'];
            }
        );

        //Student dropdown
        this.ajaxCount++;
        this.StudentService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                console.log(this.ajaxCount);

                this.studentList = data['result']['data'];
            }
        );

        // Sponser dropdown
        this.ajaxCount++;
        this.SponsorService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                console.log(this.ajaxCount);

                this.sponserList = data['result']['data'];
            }
        );
        //  staff dropdown
        this.ajaxCount++;
        this.StaffService.getItems().subscribe(
            data => {
                console.log(data);
                this.ajaxCount--;
                this.smartStaffList = data['result']['data'];

                console.log(this.ajaxCount);
            }
        );
        this.ajaxCount++;
        this.CustomerService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                console.log(this.ajaxCount);

                this.customerList = data['result']['data'];
            }
        );
        console.log(this.ajaxCount);

        this.ajaxCount++;
        this.FeeitemService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.feeItemList = data['result']['data'];
            }
        );
        this.ajaxCount++;
        this.GlcodegenerationService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                console.log(this.ajaxCount);

                this.glcodeList = data['result']['data'];
            }
        );
        //Department dropdown
        this.ajaxCount++;
        this.DepartmentService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.DeptList = data['result']['data'];
            }
        );
        //Activity dropdown
        this.ajaxCount++;
        this.ActivitycodeService.getItemsByLevel().subscribe(
            data => {
                this.ajaxCount--;
                this.activitycodeList = data['result']['data'];
            }
        );
        //Account dropdown
        this.ajaxCount++;
        this.AccountcodeService.getItemsByLevel().subscribe(
            data => {
                this.ajaxCount--;
                this.accountcodeList = data['result']['data'];
            }
        );
        //Fund dropdown
        this.ajaxCount++;
        this.FundService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.fundList = data['result']['data'];
            }
        );

        this.ajaxCount++;
        this.TaxsetupcodeService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.taxcodeList = data['result']['data'];
            });
        this.valueDate = new Date().toISOString().substr(0, 10);
        this.invoiceDataheader['f071finvoiceDate'] = this.valueDate;
        this.showIcons();
    }

    getInvoiceList() {
        this.ajaxCount = 0;
        //Invoice dropdown
        this.ajaxCount++;
        this.UserId = this.invoiceDataheader['f071fidCustomer'];
        let invoiceObj = {};
        switch (this.type) {
            case 'STA':
                invoiceObj['type'] = "STA";
                break;
            case 'STD':
                invoiceObj['type'] = "STD";
                break;
            case 'OR':
                invoiceObj['type'] = "OR";
                break;
        }
        invoiceObj['userId'] = this.UserId;
        console.log(invoiceObj);
        this.MaininvoiceService.getInvoiceItems(invoiceObj).subscribe(
            data => {
                this.ajaxCount--;
                console.log(data);
                this.maininvoiceList = data['result']['data'];

            }
        );
    }

    getPurchaseOrderDetails() {


        let PurchaseOrderId = this.invoiceDataheader['f071fidRequestInvoice'];
        this.invoiceList = [];
        this.RequestinvoiceService.getItemsDetail(PurchaseOrderId).subscribe(
            data => {
                this.InvoiceListBasedonId = data['result'];
                this.invoiceDataheader['f071finvoiceTotal'] = data['result'][0]['f071finvoiceTotal'];
                this.invoiceDataheader['f071finvoiceDate'] = data['result'][0]['f071finvoiceDate'];
                this.invoiceDataheader['f071fbillType'] = data['result'][0]['f071frevenueType'];
                this.invoiceDataheader['f071fdescription'] = data['result'][0]['f071fdescription'];
                this.invoiceDataheader['f071finvoiceNumber'] = data['result'][0]['f071finvoiceNumber'];
                // this.getGlList();

                for (var i = 0; i < this.InvoiceListBasedonId.length; i++) {
                    var purchaseOrderObject = {}

                    purchaseOrderObject['f072fidItem'] = this.InvoiceListBasedonId[i]['f072fidItem'];
                    purchaseOrderObject['f072fdebitFundCode'] = this.InvoiceListBasedonId[i]['f072fdebitFundCode'];
                    purchaseOrderObject['f072fdebitActivityCode'] = this.InvoiceListBasedonId[i]['f072fdebitActivityCode'];
                    purchaseOrderObject['f072fdebitDepartmentCode'] = this.InvoiceListBasedonId[i]['f072fdebitDepartmentCode'];
                    purchaseOrderObject['f072fdebitAccountCode'] = this.InvoiceListBasedonId[i]['f072fdebitAccountCode'];
                    purchaseOrderObject['f072fcreditFundCode'] = this.InvoiceListBasedonId[i]['f072fcreditFundCode'];
                    purchaseOrderObject['f072fcreditActivityCode'] = this.InvoiceListBasedonId[i]['f072fcreditActivityCode'];
                    purchaseOrderObject['f072fcreditDepartmentCode'] = this.InvoiceListBasedonId[i]['f072fcreditDepartmentCode'];
                    purchaseOrderObject['f072fcreditAccountCode'] = this.InvoiceListBasedonId[i]['f072fcreditAccountCode'];
                    purchaseOrderObject['f072fquantity'] = this.InvoiceListBasedonId[i]['f072fquantity'];
                    purchaseOrderObject['f072fprice'] = this.ConvertToFloat(this.InvoiceListBasedonId[i]['f072fprice']).toFixed(2);
                    purchaseOrderObject['f072ftotalExc'] = this.InvoiceListBasedonId[i]['f072ftotalExc'];
                    purchaseOrderObject['f072fgstCode'] = parseInt(this.InvoiceListBasedonId[i]['f072fgstCode']);
                    purchaseOrderObject['f072fgstValue'] = this.ConvertToFloat(this.InvoiceListBasedonId[i]['f072fgstValue']).toFixed(2);
                    purchaseOrderObject['f072ftaxAmount'] = this.ConvertToFloat(this.InvoiceListBasedonId[i]['f072ftaxAmount']).toFixed(2);;
                    purchaseOrderObject['f072ftotal'] = this.ConvertToFloat(this.InvoiceListBasedonId[i]['f072ftotal']).toFixed(2);

                    // purchaseOrderObject['f034ftotalIncTax'] = this.ConvertToFloat(this.purchaseRequisitionListBasedonId[i]['f088ftotalIncTax']).toFixed(2);

                    this.invoiceList.push(purchaseOrderObject);
                }
                this.showLastRow = true;
                this.showIcons();
            }
        );
    }

    getglcode() {
        let itemId = this.invoiceData['f072fidItem'];
        console.log(itemId);
        this.ajaxCount++;
        this.RevenuesetupService.getItemsDetail(itemId).subscribe(
            data => {
                this.ajaxCount--;
                this.glCodeList = data['result'][0];
                this.invoiceData['f072fcreditFundCode'] = data['result'][0]['f085ffund'];
                this.invoiceData['f072fcreditActivityCode'] = data['result'][0]['f085factivity'];
                this.invoiceData['f072fcreditDepartmentCode'] = data['result'][0]['f085fdepartment'];
                this.invoiceData['f072fcreditAccountCode'] = data['result'][0]['f085faccountId'];
            }
        );
    }
    getglcodeList() {
        for (var i = 0; i < this.invoiceList.length; i++) {
            let itemId = this.invoiceList[i]['f072fidItem'];
            // this.ajaxCount++;
            this.RevenuesetupService.getItemsDetail(itemId).subscribe(
                data => {
                    // this.ajaxCount--;
                    this.glCodeList = data['result'][0];
                }
            );
            this.invoiceList[i]['f072fcreditFundCode'] = this.itemList['f085ffund'];
            this.invoiceList[i]['f072fcreditActivityCode'] = this.itemList['f085factivity'];
            this.invoiceList[i]['f072fcreditDepartmentCode'] = this.itemList['f085fdepartment'];
            this.invoiceList[i]['f072fcreditAccountCode'] = this.itemList['f085faccountId'];
        }

    }
    getType() {
        let typeObject = {};
        typeObject['type'] = this.invoiceDataheader['f071finvoiceType'];
        this.type = this.invoiceDataheader['f071finvoiceType']
        console.log(typeObject);
        return this.type;
    }
    getPercentage() {
        let percenId = this.invoiceData['f072fgstCode'];
        console.log(percenId);
        this.TaxsetupcodeService.getItemsDetail(percenId).subscribe(
            data => {
                this.invoiceList = data['result'];

                this.invoiceData['f072fgstValue'] = data['result'][0]['f081fpercentage'];
            }
        );
    }
    getInvoiceNumber() {
        let typeObject = {};
        typeObject['type'] = this.invoiceDataheader['f071finvoiceType'];
        this.type = this.invoiceDataheader['f071finvoiceType'];
        console.log(typeObject);

        this.VoucherService.getInvoiceNumber(typeObject).subscribe(
            data => {
                this.invoiceDataheader['f071finvoiceNumber'] = data['number'];
                this.valueDate = new Date().toISOString().substr(0, 10);
                this.invoiceDataheader['f071finvoiceDate'] = this.valueDate;
            }
        );
        return this.type;
    }

    ConvertToFloat(val) {
        return parseFloat(val);
    }

    autoSelect() {
        this.invoiceList['f072fcreditFundCode'] = this.invoiceList['f072fdebitFundCode'];


        this.invoiceList['f072fcreditActivityCode'] = this.invoiceList['f072fdebitActivityCode'];

        this.invoiceList['f072fcreditDepartmentCode'] = this.invoiceList['f072fdebitDepartmentCode'];

    }

    onBlurMethod() {
        console.log(this.invoiceList);
        var finaltotal = 0;
        for (var i = 0; i < this.invoiceList.length; i++) {
            var totalamount = (this.ConvertToFloat(this.invoiceList[i]['f072fquantity'])) * (this.ConvertToFloat(this.invoiceList[i]['f072fprice']));
            var gstamount = ((this.ConvertToFloat(totalamount) / 100) * this.ConvertToFloat(this.invoiceList[i]['f072fgstValue']));//this.ConvertToFloat(this.gstValue) / 100 * 
            this.invoiceList[i]['f072ftotal'] = (gstamount + totalamount);
            console.log(gstamount);
            console.log(totalamount)
            finaltotal = finaltotal + this.invoiceList[i]['f072ftotal'];
            this.invoiceList[i]['f072ftaxAmount'] = gstamount.toFixed(2);
            this.invoiceList[i]['f072ftotalExc'] = totalamount.toFixed(2);
        }

        this.invoiceDataheader['f071finvoiceTotal'] = (this.ConvertToFloat(finaltotal)).toFixed(2);
    }
    saveInvoice() {
        if (this.invoiceDataheader['f071finvoiceType'] == undefined) {
            alert("Select Customer Type");
            return false;
        }
        if (this.invoiceDataheader['f071fidCustomer'] == undefined) {
            if (this.type == 'STD') {
                alert("Select Student")
                return false;
            }
            if (this.type == 'OR') {
                alert("Select Customer")
                return false;
            }
            if (this.type == 'STA') {
                alert("Select Staff")
                return false;
            }
            if (this.type == 'SP') {
                alert("Select Sponsor")
                return false;
            }
        }

        if (this.invoiceDataheader['f071finvoiceDate'] == undefined) {
            alert('Select Invoice Date');
            return false;
        }
        if (this.invoiceDataheader['f071fbillType'] == undefined) {
            alert('Select Revenue Type');
            return false;
        }
        if (this.showLastRow == false) {
            var addactivities = this.addinvoicelist(1);
            if (addactivities == false) {
                return false;
            }

        }
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        let invoiceObject = {};
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        console.log(this.id);
        
        if (this.invoiceList.length > 0) {

        } else {
            alert("Please add one invoice details");
            return false;
        }
        invoiceObject['f071fid'] = this.invoiceDataheader['f071fid'];;
        invoiceObject['f071fidRequestInvoice'] = this.invoiceDataheader['f071fidRequestInvoice'];
        invoiceObject['f071finvoiceType'] = this.invoiceDataheader['f071finvoiceType'];
        invoiceObject['f071finvoiceDate'] = this.invoiceDataheader['f071finvoiceDate'];
        invoiceObject['f071finvoiceTotal'] = this.ConvertToFloat(this.invoiceDataheader['f071finvoiceTotal']).toFixed(2);
        invoiceObject['f071fidCustomer'] = this.invoiceDataheader['f071fidCustomer'];
        invoiceObject['f071fbillType'] = this.invoiceDataheader['f071fbillType'];
        invoiceObject['f071ftype'] = this.invoiceDataheader['f071ftype'];
        invoiceObject['f071finvoiceNumber'] = this.invoiceDataheader['f071finvoiceNumber'];
        invoiceObject['f071fdescription'] = "Description";
        invoiceObject['f071finvoiceFrom'] = 2;
        invoiceObject['f071fstatus'] = 0;
        invoiceObject['f071fisRcp'] = 1;
        invoiceObject['invoice-details'] = this.invoiceList;
        for (var i = 0; i < invoiceObject['invoice-details'].length; i++) {
            if (this.id > 0) {
                invoiceObject['f072fid'] = invoiceObject['invoice-details'][i]['f072fid'];
            }
            invoiceObject['invoice-details'][i]['f072ftotal'] = this.ConvertToFloat(invoiceObject['invoice-details'][i]['f072ftotal']).toFixed(2);
            invoiceObject['invoice-details'][i]['f072fprice'] = this.ConvertToFloat(invoiceObject['invoice-details'][i]['f072fprice']).toFixed(2);
            invoiceObject['invoice-details'][i]['f072ftotalExc'] = this.ConvertToFloat(invoiceObject['invoice-details'][i]['f072ftotalExc']).toFixed(2);
            invoiceObject['invoice-details'][i]['f072ftaxAmount'] = this.ConvertToFloat(invoiceObject['invoice-details'][i]['f072ftaxAmount']).toFixed(2); 
        }
        if (this.id > 0) {
            this.MaininvoiceService.updateInvoiceItems(invoiceObject, this.id).subscribe(
                data => {

                    this.router.navigate(['accountsrecivable/maininvoice']);
                    alert("Invoice Entry has been Updated Sucessfully ! ");

                }, error => {
                    console.log(error);
                });
        } else {
            this.MaininvoiceService.insertMaininvoiceItems(invoiceObject).subscribe(
                data => {


                    this.router.navigate(['accountsrecivable/maininvoice']);
                    alert("Invoice Entry has been Added Sucessfully ! ");

                }, error => {
                    console.log(error);
                });
        }
    }

    getGstvalue() {
        let taxId = this.invoiceData['f072fgstCode'];
        let quantity = this.invoiceData['f072fquantity'];
        let price = this.invoiceData['f072fprice'];
        //console.log(this.taxcodeList);
        var taxSelectedObject = {};
        for (var k = 0; k < this.taxcodeList.length; k++) {
            if (this.taxcodeList[k]['f081fid'] == taxId) {
                taxSelectedObject = this.taxcodeList[k];
            }
        }


        this.gstValue = taxSelectedObject['f081fpercentage'];
        this.invoiceData['f072fgstValue'] = this.gstValue;
        this.Amount = (this.ConvertToFloat(price)) * (this.ConvertToFloat(quantity));
        this.taxAmount = (this.ConvertToFloat(this.gstValue)) / 100 * this.ConvertToFloat((this.Amount));

        let totalAm = ((this.taxAmount) + (this.Amount));
        this.invoiceData['f072ftotalExc'] = this.Amount.toFixed(2);
        this.invoiceData['f072ftaxAmount'] = this.taxAmount.toFixed(2);
        this.invoiceData['f072ftotal'] = totalAm.toFixed(2);
    }
    deleteInvoice(object) {
        var cnf = confirm("Do you really want to delete");
        if (cnf == true) {
            console.log(this.invoiceList);
            var index = this.invoiceList.indexOf(object);
            this.deleteList.push(this.invoiceList[index]['f072fid']);
            let deleteObject = {};
            deleteObject['id'] = this.deleteList;
            this.MaininvoiceService.deleteItems(deleteObject).subscribe(
                data => {
                }, error => {
                    console.log(error);
                });
            if (index > -1) {
                this.invoiceList.splice(index, 1);
            }
            this.onBlurMethod();
            this.showIcons();
        }
    }
    addinvoicelist(type) {

        if (this.invoiceData['f072fidItem'] == undefined) {
            alert('Select Fee Item');
            return false;
        }
        if (this.invoiceData['f072fdebitFundCode'] == undefined) {
            alert('Select Debit Fund');
            return false;
        }
        if (this.invoiceData['f072fdebitActivityCode'] == undefined) {
            alert('Select Debit Activity Code');
            return false;
        }
        if (this.invoiceData['f072fdebitDepartmentCode'] == undefined) {
            alert('Select Debit Department');
            return false;
        }
        if (this.invoiceData['f072fdebitAccountCode'] == undefined) {
            alert('Select Debit Account Code');
            return false;
        }
        if (this.invoiceData['f072fcreditFundCode'] == undefined) {
            alert('Select Credit Fund');
            return false;
        }
        if (this.invoiceData['f072fcreditActivityCode'] == undefined) {
            alert('Select Credit Activity Code');
            return false;
        }
        if (this.invoiceData['f072fcreditDepartmentCode'] == undefined) {
            alert('Select Credit Department');
            return false;
        }
        if (this.invoiceData['f072fcreditAccountCode'] == undefined) {
            alert('Select Credit Account Code');
            return false;
        }
        if (this.invoiceData['f072fquantity'] == undefined) {
            alert('Enter Quantity');
            return false;
        }
        if (this.invoiceData['f072fprice'] == undefined) {
            alert('Enter Price');
            return false;
        }
        if (this.invoiceData['f072fgstCode'] == undefined) {
            alert('Select Tax Code');
            return false;
        }
        if (this.invoiceData['f072fdebitAccountCode'] == this.invoiceData['f072fcreditAccountCode']) {
            alert("Debit GL and Credit GL Account Code Cannot be Same");
            return false;
        }

        console.log(type);
        if (type == 1) {

        } else {

            var confirmPop = confirm("Do you want to Add?");
            if (confirmPop == false) {
                return false;
            }
        }
        console.log(this.invoiceData);

        var dataofCurrentRow = this.invoiceData;
        this.invoiceData = {};
        this.invoiceList.push(dataofCurrentRow);
        this.onBlurMethod();
        this.showIcons();
    }
    onlyNumberKey(event) {
        //console.log(event);
        if (event.charCode == 46) {
        } else {
            return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
        }
    }
    maximize() {
        $("#myModal").addClass("modal");
        $("#overlay1").show();
        $(".maximize-icon").addClass("hidden");
        $(".minimize-icon").removeClass("hidden");
        $(".modal-backdrop").show();

    }
    minimize() {
        $("#myModal").removeClass("modal");
        $("#overlay1").hide();
        $(".maximize-icon").removeClass("hidden");
        $(".minimize-icon").addClass("hidden");
        $(".modal-backdrop").hide();
        $('#myModal').show();

    }
    getList() {
        this.ajaxCount++;
        this.MaininvoiceapprovalService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.maininvoiceApprovalList = data['result'];

            }, error => {
                console.log(error);
            });
    }
    invoiceApprovalListData() {
        let approveDarrayList = [];
        console.log(this.maininvoiceApprovalList);
        for (var i = 0; i < this.maininvoiceApprovalList.length; i++) {
            if (this.maininvoiceApprovalList[i].f071fstatus == true) {
                approveDarrayList.push(this.maininvoiceApprovalList[i].f071fid);
            }
        }
        var confirmPop = confirm("Do you want to Approve?");
        if (confirmPop == false) {
            return false;
        }
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        approveDarrayList.push(this.id);
        var approvalObject = {};
        approvalObject['id'] = approveDarrayList;
        approvalObject['status'] = '1';
        approvalObject['reason'] = '';
        this.MaininvoiceapprovalService.updateInvoiceApproval(approvalObject).subscribe(
            data => {
                this.getList();
                this.invoiceapprovalData['reason'] = '';
                this.router.navigate(['accountsrecivable/maininvoiceapproval']);
                alert("Approved Successfully");
            }, error => {
                console.log(error);
            });

    }
    regectJournalApprovalListData() {
        let approveDarrayList = [];
        console.log(this.maininvoiceApprovalList);
        for (var i = 0; i < this.maininvoiceApprovalList.length; i++) {
            if (this.maininvoiceApprovalList[i].f071fstatus == true) {
                approveDarrayList.push(this.maininvoiceApprovalList[i].f071fid);
            }
        }
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        approveDarrayList.push(this.id);
        if (this.invoiceapprovalData['reason'] == undefined) {
            alert('Enter the Remarks');
            return false;
        }
        var confirmPop = confirm("Do you want to Reject?");
        if (confirmPop == false) {
            return false;
        }
        var budgetActivityDataObject = {}
        budgetActivityDataObject['id'] = approveDarrayList;
        budgetActivityDataObject['status'] = '2';
        budgetActivityDataObject['reason'] = this.invoiceapprovalData['reason'];


        this.MaininvoiceapprovalService.updateInvoiceApproval(budgetActivityDataObject).subscribe(
            data => {
                this.getList();
                this.invoiceapprovalData['reason'] = '';
                this.router.navigate(['accountsrecivable/maininvoiceapproval']);
                alert("Rejected Successfully");
            }, error => {
                console.log(error);
            });
    }
}