import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MaininvoiceService } from '../service/maininvoice.service'
import { ActivatedRoute, Router } from '@angular/router';
import { faSearch, faEdit } from '@fortawesome/free-solid-svg-icons';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'MaininvoiceComponent',
    templateUrl: 'maininvoice.component.html'
})

export class MaininvoiceComponent implements OnInit {
    invoiceList = [];
    invoiceData = {};
    faSearch = faSearch;
    faEdit = faEdit;
    id: number;
    title: string;
    type: string;
    constructor(
        private MaininvoiceService: MaininvoiceService,
        private spinner: NgxSpinnerService,
        private route: ActivatedRoute
    ) { }

    ngOnInit() {
        this.spinner.show();                        
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.MaininvoiceService.getMasterInvoiceItems().subscribe(
            data => {
                this.spinner.hide();
                this.invoiceList = data['result']['data'];
                for (var i = 0; i < this.invoiceList.length; i++) {
                    this.invoiceList[i]['f071finvoiceType'] = this.getName(this.invoiceList[i]['f071finvoiceType']);
                }
                let activityData = [];
                activityData = data['result']['data'];
                for (var i = 0; i < activityData.length; i++) {
                    if (activityData[i]['f071fstatus'] == 0) {
                        activityData[i]['f071fstatus'] = 'Pending';
                    } else if (activityData[i]['f071fstatus'] == 1) {
                        activityData[i]['f071fstatus'] = 'Approved';
                    } else {
                        activityData[i]['f071fstatus'] = 'Rejected';
                    }
                    if (activityData[i]['f071finvoiceType'] == 'STA') {
                        activityData[i]['f071finvoiceType'] = 'Staff';

                    } else if (activityData[i]['f071finvoiceType'] == 'STD') {
                        activityData[i]['f071finvoiceType'] = 'Student';
                    }
                    else {
                        if (activityData[i]['f071finvoiceType'] == 'OR') {
                            activityData[i]['f071finvoiceType'] = 'Other Receivables';
                        }
                    }
                }
                this.invoiceList = activityData;
            }, error => {
                console.log(error);
            });
    }

    getName(name) {
        var text = '';
        switch (name) {
            case 'SP': text = "Sponsor"; break;
            case 'STD': text = "Student"; break;
            case 'STA': text = "Staff"; break;
            case 'Credit Card Payment': text = "Credit Card Payment"; break;
            case 'Payment': text = "Payment"; break;
            case 'Invoice from PTJ': text = "Invoice from PTJ"; break;
            case 'OR': text = "Other Recevables"; break;
        }
        return text;
    }
    downloadInvoice(id) {
        console.log(id);

        let invObject = {};
        invObject['id'] = id;
        invObject['download'] = 'yes';
        invObject['type'] = 'pdf';
        console.log(invObject);
        this.MaininvoiceService.downloadInvoice(invObject).subscribe(
            data => {
                console.log(data['name']);
                window.open('http://fims.mtcsbdeveloper.asia/download/' + data['name']);
            }, error => {
                console.log(error);
            });
    }
    addMaininvoice() {
        console.log(this.invoiceData);
        this.MaininvoiceService.insertMaininvoiceItems(this.invoiceData).subscribe(
            data => {
                this.invoiceList = data['todos'];
            }, error => {
                console.log(error);
            });
    }
}