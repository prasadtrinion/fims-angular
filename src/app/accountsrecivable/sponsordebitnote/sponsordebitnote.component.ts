import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SponsordebitnoteService } from '../service/sponsordebitnote.service'
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'Sponsordebitnote',
    templateUrl: 'sponsordebitnote.component.html'
})
export class SponsordebitnoteComponent implements OnInit {
    sponsordebitList = [];
    SponsordebitData = {};
    id: number;
    constructor(
        private SponsordebitnoteService: SponsordebitnoteService,
        private spinner: NgxSpinnerService,
    ) { }

    ngOnInit() {
        this.spinner.show();                                
        this.SponsordebitnoteService.getItems().subscribe(
            data => {
                this.spinner.hide();
                let activityData = [];
                activityData = data['data'];
                console.log("asdf");
                for (var i = 0; i < activityData.length; i++) {
                    if (activityData[i]['f063fstatus'] == 0) {
                        activityData[i]['f063fstatus'] = 'Pending';
                    } else if (activityData[i]['f063fstatus'] == 1) {
                        activityData[i]['f063fstatus'] = 'Approved';
                    } else {
                        activityData[i]['f063fstatus'] = 'Rejected';
                    }
                }
                this.sponsordebitList = activityData;
            }, error => {
                console.log(error);
            });
    }
   
}