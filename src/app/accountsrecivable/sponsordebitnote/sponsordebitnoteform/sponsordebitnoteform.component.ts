
import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { CustomerService } from '../../../accountsrecivable/service/customer.service';
import { StudentService } from '../../../studentfinance/service/student.service';
import { ActivatedRoute, Router } from '@angular/router';
import { SponsorService } from "../../../studentfinance/service/sponsor.service";
import { ItemService } from '../../service/item.service';
import { MaininvoiceService } from '../../service/maininvoice.service';
import { GlcodegenerationService } from '../../../generalledger/service/glcodegeneration.service';
import { UserService } from '../../../generalsetup/service/user.service';
import { TaxsetupcodeService } from '../../../generalledger/service/taxsetupcode.service'
import { isEmpty } from 'rxjs/operators';
import { RevenuesetupService } from '../../service/revenuesetup.service'
import { FeeitemService } from '../../../studentfinance/service/feeitem.service';
import { FundService } from '../../../generalsetup/service/fund.service';
import { AccountcodeService } from '../../../generalsetup/service/accountcode.service'
import { ActivitycodeService } from '../../../generalsetup/service/activitycode.service';
import { DepartmentService } from '../../../generalsetup/service/department.service';
import { SponsordebitnoteService } from "../../service/sponsordebitnote.service";
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'SponsordebitnoteFormComponent',
    templateUrl: 'sponsordebitnoteform.component.html'
})

export class SponsordebitnoteFormComponent implements OnInit {
    debitnoteData = {};
    customerList = [];
    studentList = [];
    sponserList = [];
    taxcodeList = [];
    maininvoiceList = [];
    maininvoiceListBasedOnInvoiceId = [];
    maininvoiceData = {};
    customerData = {};
    glcodeList = [];
    glcodeData = {};
    itemList = [];
    userData = {};
    userList = [];
    debitNotelist = [];
    fundList = [];
    saveBtnDisable: boolean;
    viewDisabled: boolean;
    editDisabled: boolean;
    title = '';
    viewId: number;
    openingbalanceData = {};
    newOpeningBalanceList = [];
    saveDataObject = {};
    invoicetypeList = [];
    DeptList = [];
    activitycodeList = [];
    accountcodeList = [];
    feeItemList = [];
    id: number;
    showCrReadonly: boolean;
    showDrReadonly: boolean;
    balance: number;
    cnamount: number;
    newbal: number;
    cash: number;
    debitnoteAddedAmount: number;
    payment: number;
    type: string;
    valueDate: string;
    ajaxCount: number;
    UserId: number;
    idview:number;
    smartStaffList = [];
    constructor(

        private GlcodegenerationService: GlcodegenerationService,
        private RevenuesetupService: RevenuesetupService,
        private SponsordebitnoteService: SponsordebitnoteService,
        private TaxsetupcodeService: TaxsetupcodeService,
        private ItemService: ItemService,
        private StudentService: StudentService,
        private SponsorService: SponsorService,
        private MaininvoiceService: MaininvoiceService,
        private CustomerService: CustomerService,
        private UserService: UserService,
        private FundService: FundService,
        private FeeitemService: FeeitemService,
        private AccountcodeService: AccountcodeService,
        private ActivitycodeService: ActivitycodeService,
        private DepartmentService: DepartmentService,
        private spinner: NgxSpinnerService,
        private route: ActivatedRoute,
        private router: Router
    ) { }



    ngDoCheck() {
        const change = this.ajaxCount;
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.saveBtnDisable = false;
        this.route.paramMap.subscribe((data) => this.idview = + data.get('idview'));
        // console.log(this.ajaxCount);
        if (this.ajaxCount == 0) {
            this.editFunction();
            this.spinner.hide();
            this.ajaxCount = 10;
        }

        if (this.ajaxCount == 0 && this.debitNotelist.length > 0) {
            this.ajaxCount = 20;
            console.log("asdfsdf");
            // this.showIcons();
            //console.log(this.newbudgetactivitiesList[0]);
            if (this.debitNotelist[0]['f063fstatus'] == 1 || this.debitNotelist[0]['f063fstatus'] == 2) {
                this.saveBtnDisable = true;
                this.viewDisabled = true;
                this.editDisabled = true;
                $("#target input,select").prop("disabled", true);
                $("#target1 input,select").prop("disabled", true);
            }
        }
        if (this.ajaxCount == 0 && this.debitNotelist.length > 0) {
            this.ajaxCount = 20;
            if (this.idview > 1) {
                this.saveBtnDisable = true;
                this.viewDisabled = true;
                this.editDisabled = true;
                $("#target input,select").prop("disabled", true);
                $("#target1 input,select").prop("disabled", true);
            }
        }
    }

    editFunction() {
        this.ajaxCount++;
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.route.paramMap.subscribe((data) => this.idview = + data.get('idview'));

        if (this.id > 0) {
            this.SponsordebitnoteService.getItemsDetail(this.id).subscribe(
                data => {
                    this.ajaxCount--;
                    console.log(this.id);
                    this.debitnoteData = data['result'];
                    this.debitNotelist = data['result'];


                    this.debitnoteData['f063fbalance'] = data['result'][0]['f071fbalance'],
                    this.debitnoteData["f063ftype"] = data['result'][0]['f063ftype'],
                    this.type = data['result'][0]['f063ftype'],
                    this.debitnoteData['f063fdate'] = data['result'][0]['f063fdate'],
                    this.debitnoteData['f063fidInvoice'] = data['result'][0]['f063fidInvoice']
                    this.debitnoteData['f063fdescription'] = data['result'][0]['f063fdescription'],
                    // this.creditnoteData[f031fnarration'] = data['result'][0]['f031fdescription'],
                    this.debitnoteData['f063freferenceNumber'] = data['result'][0]['f063freferenceNumber'],
                    
                    this.debitnoteData['f063ftotalAmount'] = data['result'][0]['f063ftotalAmount'],
                    this.debitnoteData['f063fvoucherNumber'] = data['result'][0]['f063fvoucherNumber'],
                    this.debitnoteData['f063fidCustomer'] = data['result'][0]['f063fidCustomer']
                    this.getInvoiceList();
                    // this.getInvoiceDetails();
                    for (var i = 0; i < this.debitNotelist.length; i++) {
                        console.log(this.debitNotelist[i]);
                        var creditnoteObject = {};
                        creditnoteObject['f064fid'] = this.debitNotelist[i]['f064fid'];
                        creditnoteObject['f072fidItem'] = this.debitNotelist[i]['f064fidItem'];
                        creditnoteObject['f072fdebitFundCode'] = this.debitNotelist[i]['f064fdebitFundCode'];
                        creditnoteObject['f072fdebitAccountCode'] = this.debitNotelist[i]['f064fdebitAccountCode'];
                        creditnoteObject['f072fdebitActivityCode'] = this.debitNotelist[i]['f064fdebitActivityCode'];
                        creditnoteObject['f072fdebitDepartmentCode'] = this.debitNotelist[i]['f064fdebitDepartmentCode'];

                        creditnoteObject['f072fcreditFundCode'] = this.debitNotelist[i]['f064fcreditFundCode'];
                        creditnoteObject['f072fcreditAccountCode'] = this.debitNotelist[i]['f064fcreditAccountCode'];
                        creditnoteObject['f072fcreditActivityCode'] = (this.debitNotelist[i]['f064fcreditActivityCode']);
                        creditnoteObject['f072fcreditDepartmentCode'] = this.debitNotelist[i]['f064fcreditDepartmentCode'];

                        creditnoteObject['f072fquantity'] = this.debitNotelist[i]['f064fquantity'];
                        creditnoteObject['f072fprice'] = this.ConvertToFloat(this.debitNotelist[i]['f064fprice'].toFixed(2));
                        creditnoteObject['f072fgstCode'] = parseInt(this.debitNotelist[i]['f064fgstCode']);
                        creditnoteObject['f072fgstValue'] = this.debitNotelist[i]['f064fgstValue'];
                        creditnoteObject['f072ftotal'] = this.debitNotelist[i]['f064ftotal'];
                        creditnoteObject['f072ftaxAmount'] = this.debitNotelist[i]['f064ftaxAmount'];
                        creditnoteObject['f072ftotalExc'] = this.debitNotelist[i]['f064ftotalExc'];
                        creditnoteObject['f064ftoDnAmount'] = this.debitNotelist[i]['f064ftoDnAmount'];

                        this.maininvoiceListBasedOnInvoiceId.push(creditnoteObject);
                    }
                    if (data['result'][0]['f063fstatus'] == 1 || this.debitNotelist[0]['f063fstatus'] == 2) {
                        this.viewDisabled = true;
                        this.editDisabled = true;
                        $("#target input,select").prop("disabled", true);
                        $("#target1 input,select").prop("disabled", true);
                    }
                    if (this.idview > 1) {
                        this.saveBtnDisable = true;
                        this.viewDisabled = true;
                        this.editDisabled = true;
                        $("#target input,select").prop("disabled", true);
                        $("#target1 input,select").prop("disabled", true);
                    }
                }, error => {
                    console.log(error);
                });
        }
    }


    ngOnInit() {
        this.viewDisabled = false;
        this.editDisabled = false;
        this.debitnoteAddedAmount = 0;
        this.ajaxCount = 0;
        this.spinner.show();
        //  this.maininvoiceList=[];

        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        this.route.paramMap.subscribe((data) => this.viewId = + data.get('viewId'));

        this.valueDate = new Date().toISOString().substr(0, 10);
        console.log(this.valueDate);
        this.debitnoteData['f063fdate'] = this.valueDate;
        let invoicetypeobj = {};
        invoicetypeobj['name'] = 'Sponsor';
        invoicetypeobj['type'] = 'DSP';
        this.invoicetypeList.push(invoicetypeobj);
        this.debitnoteData['f063ftype'] = invoicetypeobj['name'];        // invoicetypeobj = {};


        // this.ajaxCount = 0;
        // //Invoice dropdown
        // this.ajaxCount++;
        // this.MaininvoiceService.getItems().subscribe(
        //     data => {
        //         this.ajaxCount--;
        //         this.maininvoiceList = data['result']['data'];
        //     }
        // );

        //User dropdown
        this.ajaxCount++;
        this.UserService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.userList = data['result']['data'];
            }
        );


        //Item dropdown
        this.ajaxCount++;
        this.RevenuesetupService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.itemList = data['result'];
            }
        );
        //Department dropdown
        this.ajaxCount++;
        this.DepartmentService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.DeptList = data['result']['data'];
            }
        );


        //Activity dropdown
        this.ajaxCount++;
        this.ActivitycodeService.getItemsByLevel().subscribe(
            data => {
                this.ajaxCount--;
                this.activitycodeList = data['result']['data'];
            }
        );
        //Account dropdown
        this.ajaxCount++;
        this.AccountcodeService.getItemsByLevel().subscribe(
            data => {
                this.ajaxCount--;
                this.accountcodeList = data['result']['data'];
            }
        );



        //Fund dropdown
        this.ajaxCount++;
        this.FundService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.fundList = data['result']['data'];
            }
        );


        //FeeItem Dropdown
        this.ajaxCount++;
        this.FeeitemService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.feeItemList = data['result']['data'];
            }
        );

        //Taxcode dropdown
        this.ajaxCount++;
        this.TaxsetupcodeService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.taxcodeList = data['result']['data'];
            }
        );


        //Student dropdown
        this.ajaxCount++;
        this.StudentService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                console.log(this.ajaxCount);

                this.studentList = data['result']['data'];
            }
        );

        //Sponser dropdown
        this.ajaxCount++;
        this.SponsorService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                console.log(this.ajaxCount);

                this.sponserList = data['result']['data'];
            }
        );

        //Customer dropdown
        this.ajaxCount++;
        this.CustomerService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.customerList = data['result']['data'];
            }
        );


    }

    getType() {
        let typeObject = {};
        typeObject['type'] = this.debitnoteData['f063ftype'];
        this.type = this.debitnoteData['f063ftype']
        console.log(typeObject);
        this.getDebitNoteNumber();
        return this.type;
    }

    getDebitNoteNumber() {

        let typeObject = {};
        typeObject['type'] = this.debitnoteData['f063ftype'];
        this.SponsordebitnoteService.getDebitnoteNumber(typeObject).subscribe(
            newData => {
                console.log(newData);
                this.debitnoteData['f063freferenceNumber'] = newData['number'];
            }
        );
    }

    getInvoiceList() {
        this.ajaxCount = 0;
        //Invoice dropdown
        this.ajaxCount++;
        this.UserId = this.debitnoteData['f063fidCustomer'];
        let invoiceObj = {};
        invoiceObj['type'] = "SP";
        invoiceObj['UserId'] = this.UserId;
        console.log(invoiceObj);
        this.SponsordebitnoteService.getInvoiceItems(invoiceObj).subscribe(
            data => {

                this.maininvoiceList = data['result']['data'];

            }
        );
    }

    getInvoiceDetails() {
        let invid = 0;
        invid = this.debitnoteData['f063fidInvoice'];
        this.MaininvoiceService.getInvoiceDetailsById(invid).subscribe(
            data => {
                this.maininvoiceListBasedOnInvoiceId = data['result'];
                for (var i = 0; i < this.maininvoiceListBasedOnInvoiceId.length; i++) {
                    this.maininvoiceListBasedOnInvoiceId[i]['f072fgstCode'] = parseInt(this.maininvoiceListBasedOnInvoiceId[i]['f072fgstCode']);
                }
                console.log(this.maininvoiceListBasedOnInvoiceId);
                // this.creditnoteData['f031fidCustomer'] = this.ConvertToFloat(data['result'][0]['f071fidCustomer']).toFixed(2);
                this.debitnoteData['f063ftotalAmount'] = this.ConvertToFloat(data['result'][0]['f071finvoiceTotal']).toFixed(2);
                this.debitnoteData['invoiceDate'] = data['result'][0]['f072finvoiceDate'];
                this.debitnoteData['f063fbalance'] = this.ConvertToFloat(data['result'][0]['f071fbalance']).toFixed(2);
                // this.creditnoteData['f031fdescription'] = this.ConvertToFloat(data['result'][0]['f071fdescription']).toFixed(2);
            }
        )
    }
    subDebitAmount() {


        this.debitnoteAddedAmount = 0;
        this.debitnoteData['f064ftoDnAmount'] = 0;
        for (var i = 0; i < this.maininvoiceListBasedOnInvoiceId.length; i++) {
            this.debitnoteAddedAmount = this.ConvertToFloat(this.debitnoteAddedAmount) + this.ConvertToFloat(this.maininvoiceListBasedOnInvoiceId[i]['f064ftoDnAmount']);

            if (this.debitnoteAddedAmount > this.debitnoteData['f063fbalance']) {
            }

        }

        this.debitnoteData['f031fbalance'] = this.ConvertToFloat(this.debitnoteData['f063ftotalAmount']) + this.ConvertToFloat(this.debitnoteAddedAmount);

    }

    saveData() {
        if (this.debitnoteData['f063ftype'] == undefined) {
            alert('Select Invoice Type');
            return false;
        }
        if (this.debitnoteData['f063fidCustomer'] == undefined) {
            alert("Select Sponsor")
            return false;
        }
        if (this.debitnoteData['f063fidInvoice'] == undefined) {
            alert('Select Invoice Number');
            return false;
        }
        if (this.debitnoteData['f063fdescription'] == undefined) {
            alert('Enter Desccription');
            return false;
        }
        // if (this.maininvoiceListBasedOnInvoiceId['f032ftoDnAmount'] == undefined) {
        //     alert('Enter To Debit Amount(RM)');
        //     return false;
        // }
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        this.saveDataObject = {};
        if (this.id > 0) {
            this.saveDataObject['f063fid'] = this.id;
        }
        var saveDetails = [];
        this.saveDataObject["f063ftype"] = this.debitnoteData['f063ftype'];
        this.saveDataObject["f063fidInvoice"] = this.debitnoteData['f063fidInvoice'];
        this.saveDataObject["f063ftotalAmount"] = this.ConvertToFloat(this.debitnoteData['f063ftotalAmount']).toFixed(2);
        //this.saveDataObject["f031fbalance"] = this.creditnoteData['f031fbalance'];
        this.saveDataObject["f063freferenceNumber"] = this.debitnoteData['f063freferenceNumber'];
        this.saveDataObject["f063fdate"] = this.debitnoteData['f063fdate'];
        this.saveDataObject["f063fvoucherNumber"] = this.debitnoteData['f063fvoucherNumber'];
        this.saveDataObject["f063fnarration"] = this.debitnoteData['f063fnarration'];
        this.saveDataObject["f063fdescription"] = this.debitnoteData['f063fdescription'];
        this.saveDataObject["f063fidCustomer"] = this.debitnoteData['f063fidCustomer'];
        this.saveDataObject["f063fstatus"] = 0;
        this.saveDataObject['f063ftype'] = 'DSP';
        for (var i = 0; i < this.maininvoiceListBasedOnInvoiceId.length; i++) {
            var creditnoteObject = {}

            creditnoteObject['f064fid'] = this.maininvoiceListBasedOnInvoiceId[i]['f064fid'];

            creditnoteObject['f064fidItem'] = this.maininvoiceListBasedOnInvoiceId[i]['f072fidItem'];
            creditnoteObject['f064fdebitFundCode'] = this.maininvoiceListBasedOnInvoiceId[i]['f072fdebitFundCode'];
            creditnoteObject['f064fdebitAccountCode'] = this.maininvoiceListBasedOnInvoiceId[i]['f072fdebitAccountCode'];
            creditnoteObject['f064fdebitActivityCode'] = this.maininvoiceListBasedOnInvoiceId[i]['f072fdebitActivityCode'];
            creditnoteObject['f064fdebitDepartmentCode'] = this.maininvoiceListBasedOnInvoiceId[i]['f072fdebitDepartmentCode'];

            creditnoteObject['f064fcreditFundCode'] = this.maininvoiceListBasedOnInvoiceId[i]['f072fcreditFundCode'];
            creditnoteObject['f064fcreditAccountCode'] = this.maininvoiceListBasedOnInvoiceId[i]['f072fcreditAccountCode'];
            creditnoteObject['f064fcreditActivityCode'] = this.maininvoiceListBasedOnInvoiceId[i]['f072fcreditActivityCode'];
            creditnoteObject['f064fcreditDepartmentCode'] = this.maininvoiceListBasedOnInvoiceId[i]['f072fcreditDepartmentCode'];
            creditnoteObject['f064fquantity'] = this.maininvoiceListBasedOnInvoiceId[i]['f072fquantity'];
            creditnoteObject['f064fprice'] = this.ConvertToFloat(this.maininvoiceListBasedOnInvoiceId[i]['f072fprice']).toFixed(2);
            creditnoteObject['f064fgstCode'] = this.maininvoiceListBasedOnInvoiceId[i]['f072fgstCode'];

            creditnoteObject['f064ftotalExc'] = this.maininvoiceListBasedOnInvoiceId[i]['f072ftotalExc'];
            creditnoteObject['f064ftaxAmount'] = this.maininvoiceListBasedOnInvoiceId[i]['f072ftaxAmount'];
            creditnoteObject['f064ftotal'] = this.ConvertToFloat(this.maininvoiceListBasedOnInvoiceId[i]['f072ftotal']).toFixed(2);
            creditnoteObject['f064ftoDnAmount'] = this.ConvertToFloat(this.maininvoiceListBasedOnInvoiceId[i]['f064ftoDnAmount']).toFixed(2);

            saveDetails.push(creditnoteObject);
            // this.maininvoiceListBasedOnInvoiceId[i]['f032fidItem'] = parseInt(this.maininvoiceListBasedOnInvoiceId[i]['f032fidItem']).toString();

        }
        this.saveDataObject["details"] = saveDetails;
        console.log(this.saveDataObject);
        if (this.id > 0) {
            this.SponsordebitnoteService.insertSponsordebitItems(this.saveDataObject).subscribe(
                data => {
                    this.debitnoteData = data['result'];

                    this.router.navigate(['/accountsrecivable/sponsordebitnote']);
                    alert("Sponsor Debit Note has been Updated Succcessfully");

                    console.log(this.debitnoteData);
                }, error => {
                    console.log(error);
                });
        } else {
            this.SponsordebitnoteService.insertSponsordebitItems(this.saveDataObject).subscribe(
                data => {
                    this.debitnoteData = data['result'];

                    this.router.navigate(['/accountsrecivable/sponsordebitnote']);
                    alert("Sponsor Debit Note has been Added Succcessfully");

                    console.log(this.debitnoteData);
                }, error => {
                    console.log(error);
                });
        }

    }
    ConvertToFloat(val) {
        return parseFloat(val);
    }
    onlyNumberKey(event) {
        //console.log(event);
        if (event.charCode == 46) {

        } else {
            return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
        }
    }
    maximize() {
        $("#myModal").addClass("modal");
        $("#overlay1").show();
        $(".maximize-icon").addClass("hidden");
        $(".minimize-icon").removeClass("hidden");
        $(".modal-backdrop").show();

    }
    minimize() {
        $("#myModal").removeClass("modal");
        $("#overlay1").hide();
        $(".maximize-icon").removeClass("hidden");
        $(".minimize-icon").addClass("hidden");
        $(".modal-backdrop").hide();
        $('#myModal').show();

    }

}