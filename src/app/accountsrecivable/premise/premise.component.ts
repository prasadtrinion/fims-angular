import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { PremiseService } from '../service/premise.service'
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'Premise',
    templateUrl: 'premise.component.html'
})

export class PremiseComponent implements OnInit {
    premiseList =  [];
    premiseData = {};
    id: number;

    constructor(
        private PremiseService: PremiseService,
        private spinner: NgxSpinnerService,

    ) { }

    ngOnInit() { 
        this.spinner.show();        
        this.PremiseService.getItems().subscribe(
            data => {
                this.spinner.hide();
                this.premiseList = data['result']['data'];
        }, error => {
            console.log(error);
        });
    }
}