import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { PremiseService } from '../../service/premise.service';
import { ActivatedRoute, Router } from '@angular/router';
import { CountryService } from '../../../generalsetup/service/country.service';
import { PremisetypeService } from '../../service/premisetype.service';
import { StateService } from '../../../generalsetup/service/state.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { DefinationmsService } from '../../../studentfinance/service/definationms.service';

@Component({
    selector: 'PremiseFormComponent',
    templateUrl: 'premiseform.component.html'
})

export class PremiseFormComponent implements OnInit {
    premiseform: FormGroup;
    premiseList = [];
    premiseData = {};
    countryList = [];
    countryData = {};
    premiseTypeList = [];
    stateList = [];
    ajaxcount = 0;
    stateData = [];
    ajaxCount : number;
    id: number;
    constructor(
        private PremiseService: PremiseService,
        private CountryService: CountryService,
        private PremisetypeService: PremisetypeService,
        private spinner: NgxSpinnerService,
        private StateService: StateService,
        private route: ActivatedRoute,
        private router: Router,
        private DefinationmsService: DefinationmsService,

    ) { }
    ngDoCheck() {
        if (this.ajaxCount == 0) {
            this.spinner.hide();
        }
    }
    ngOnInit() {
        this.ajaxCount = 0
        this.spinner.show();
        this.premiseData['f076fstatus'] = 1;

        this.PremisetypeService.getActiveItems().subscribe(
            data => {
                this.premiseTypeList = data['result'];
            }, error => {
                console.log(error);
            });


        // country dropdown
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.CountryService.getActiveCountryList().subscribe(
            data => {
                this.countryList = data['result']['data'];
            }, error => {
                console.log(error);
            });

        // state dropdown
        // this.StateService.getActiveStateList().subscribe(
        //     data => {
        //         this.stateList = data['result']['data'];
        //     }, error => {
        //         console.log(error);
        //     });


        if (this.id > 0) {
            this.ajaxCount++;
            this.PremiseService.getItemsDetail(this.id).subscribe(
                data => {
            this.ajaxCount--;
                    //console.log(data['result'][0]);
                    this.premiseData = data['result'][0];
                    this.premiseData['f076fstatus'] = parseInt(this.premiseData['f076fstatus']);
                    this.premiseData['f076fstate'] = parseInt(this.premiseData['f076fstate']);
                    console.log(this.premiseData['f076fstatus']);
                    this.onCountryChange();
                }, error => {
                    console.log(error);
                });
        }


        console.log(this.id);
    }
    onCountryChange(){
        this.ajaxcount ++;
        if(this.premiseData['f076fcountry'] == undefined){
            this.premiseData['f076fcountry'] = 0;
        }
        this.StateService.getStates(this.premiseData['f076fcountry']).subscribe(
            data=>{
                this.ajaxcount --;
                    this.stateList = data['result'];
                    let other = {
                        'f012fid' : "1",
                        "f012fshortName":"SH",
                        "f012fstateName":"Others"

                    };
                    this.stateList.push(other);

            }
        );
    }

    addPremise() {
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        console.log(this.premiseData);
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        if (this.id > 0) {
            this.PremiseService.updatePremiseItems(this.premiseData, this.id).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert('Name Already Exist');
                    }
                    else {
                        this.router.navigate(['accountsrecivable/premise']);
                        alert("Premise Setup has been Updated Successfully");
                    }
                }, error => {
                    console.log(error);
                    alert('Name Already Exist');
                });


        } else {
            this.PremiseService.insertPremiseItems(this.premiseData).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert('Name Already Exist');
                    }
                    else {
                        this.router.navigate(['accountsrecivable/premise']);
                        alert("Premise Setup has been Added Successfully");
                    }
                }, error => {
                    console.log(error);
                });

        }

    }
    onlyNumberKey(event) {
        //console.log(event);
        if (event.charCode == 46) {

        } else {
            return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
        }
    }
    checkDuplication() {
        let duplicationObj = {};
        if (this.id > 0) {
            duplicationObj['id'] = this.id;
        }
        duplicationObj['key'] = 'Premise Setup';
        duplicationObj['premiseName'] = this.premiseData['f076fname'];
        duplicationObj['premiseType'] = this.premiseData['f076fidPremiseType'];
        if(this.premiseData['f076fidPremiseType'] != undefined && this.premiseData['f076fidPremiseType'] != "" && this.premiseData['f076fname'] != undefined && this.premiseData['f076fname'] != ""){
            this.DefinationmsService.checkDuplication(duplicationObj).subscribe(
                data => {
                    if (parseInt(data['result']) == 1) {
                        alert("Premise Name Already Exist");
                        this.premiseData['f076fname'] = '';
                    }
                }, error => {
                    console.log(error);
                });
            }
    }
}