import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { DebitnoteService } from '../service/debitnote.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'DebitnoteComponent',
    templateUrl: 'debitnote.component.html'
})

export class DebitnoteComponent implements OnInit {
    debitnoteList = [];
    debitnoteData = {};
    id: number;
    title = '';

    constructor(

        private DebitnoteService: DebitnoteService,
        private spinner: NgxSpinnerService,
        private route: ActivatedRoute,
        private router: Router
    ) { }

    ngOnInit() {
        this.spinner.show();
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id == 0) {
            this.DebitnoteService.getItems().subscribe(
                data => {
                    this.spinner.hide();
                    let activityData = [];
                    activityData = data['data'];
                    for (var i = 0; i < activityData.length; i++) {
                        if (activityData[i]['f031fstatus'] == 0) {
                            activityData[i]['f031fstatus'] = 'Pending';
                        } else if (activityData[i]['f031fstatus'] == 1) {
                            activityData[i]['f031fstatus'] = 'Approved';
                        } else {
                            activityData[i]['f031fstatus'] = 'Rejected';
                        }
                        if (activityData[i]['f031ftype'] == 'DSTA') {
                            activityData[i]['f031ftype'] = 'Staff';

                        } else if (activityData[i]['f031ftype'] == 'DSTD') {
                            activityData[i]['f031ftype'] = 'Student';
                        }
                        else {
                            if (activityData[i]['f031ftype'] == 'DOR') {
                                activityData[i]['f031ftype'] = 'Other Receivables';
                            }
                        }
                    }
                    this.debitnoteList = activityData;

                }, error => {
                    console.log(error);
                });
        }
    }
    getName(name) {
        var text = '';
        switch (name) {
            case 'DSTD': text = "Student"; break;
            case 'DSTA': text = "Staff"; break;
            case 'DSP': text = "Sponsor"; break;
            case 'DDOC': text = "Document"; break;
            case 'DOR': text = "Other Recevables"; break;
        }
        return text;
    }
}