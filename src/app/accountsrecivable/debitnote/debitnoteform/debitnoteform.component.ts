import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { CustomerService } from '../../../accountsrecivable/service/customer.service';
import { StudentService } from '../../../studentfinance/service/student.service';
import { SponsorService } from '../../../studentfinance/service/sponsor.service';
import { ActivatedRoute, Router } from '@angular/router';
import { DebitnoteService } from '../../service/debitnote.service';
import { MaininvoiceService } from '../../service/maininvoice.service';
import { UserService } from '../../../generalsetup/service/user.service';
import { TaxsetupcodeService } from '../../../generalledger/service/taxsetupcode.service'
import { isEmpty } from 'rxjs/operators';
import { RevenuesetupService } from '../../service/revenuesetup.service';
import { FeeitemService } from '../../../studentfinance/service/feeitem.service';
import { FundService } from '../../../generalsetup/service/fund.service';
import { AccountcodeService } from '../../../generalsetup/service/accountcode.service'
import { ActivitycodeService } from '../../../generalsetup/service/activitycode.service';
import { DepartmentService } from '../../../generalsetup/service/department.service';
import { StaffService } from "../../service/staff.service";
import { DebitnoteapprovalService } from '../../service/debitnoteapproval.service';
import { RevenueTypeService } from "../../service/revenuetype.service";
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'DebitnoteformComponent',
    templateUrl: 'debitnoteform.component.html',
    styleUrls: ['./debitnoteform.component.css']
})

export class DebitnoteformComponent implements OnInit {
    debitnoteData = {};
    customerList = [];
    revenuetypeList = [];
    studentList = [];
    sponserList = [];
    taxcodeList = [];
    maininvoiceList = [];
    maininvoiceListBasedOnInvoiceId = [];
    maininvoiceData = {};
    customerData = {};
    glcodeList = [];
    glcodeData = {};
    itemList = [];
    userData = {};
    userList = [];
    debitNotelist = [];
    fundList = [];
    saveBtnDisable: boolean;
    viewDisabled: boolean;
    editDisabled: boolean;
    title = '';
    viewId: number;
    openingbalanceData = {};
    newOpeningBalanceList = [];
    saveDataObject = {};
    invoicetypeList = [];
    DeptList = [];
    activitycodeList = [];
    accountcodeList = [];
    feeItemList = [];
    revenueitemList = [];
    debitnoteApprovalList = [];
    debitnoteApprovalData = {};
    id: number;
    showCrReadonly: boolean;
    showDrReadonly: boolean;
    balance: number;
    cnamount: number;
    newbal: number;
    cash: number;
    debitnoteAddedAmount: number;
    payment: number;
    type: string;
    valueDate: string;
    ajaxCount: number;
    UserId: number;
    smartStaffList = [];
    constructor(

        private RevenuesetupService: RevenuesetupService,
        private RevenueTypeService: RevenueTypeService,
        private DebitnoteService: DebitnoteService,
        private TaxsetupcodeService: TaxsetupcodeService,
        private StudentService: StudentService,
        private SponsorService: SponsorService,
        private MaininvoiceService: MaininvoiceService,
        private CustomerService: CustomerService,
        private UserService: UserService,
        private FundService: FundService,
        private FeeitemService: FeeitemService,
        private AccountcodeService: AccountcodeService,
        private ActivitycodeService: ActivitycodeService,
        private DepartmentService: DepartmentService,
        private StaffService: StaffService,
        private DebitnoteapprovalService: DebitnoteapprovalService,
        private spinner: NgxSpinnerService,
        private route: ActivatedRoute,
        private router: Router
    ) { }



    ngDoCheck() {
        const change = this.ajaxCount;
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.saveBtnDisable = false;
        this.route.paramMap.subscribe((data) => this.viewId = + data.get('viewId'));
        if (this.ajaxCount == 0) {
            this.editFunction();
            this.spinner.hide();
            this.ajaxCount = 10;
        }

        if (this.ajaxCount == 0 && this.debitNotelist.length > 0) {
            this.ajaxCount = 20;
            if (this.debitNotelist[0]['f031fstatus'] == 1 || this.debitNotelist[0]['f031fstatus'] == 2) {
                this.saveBtnDisable = true;
                this.viewDisabled = true;
                this.editDisabled = true;
                $("#target input,select").prop("disabled", true);
                $("#target1 input,select").prop("disabled", true);
            }
        }
        if (this.ajaxCount == 0 && this.debitNotelist.length > 0) {
            this.ajaxCount = 20;
            if (this.viewId > 1) {
                this.saveBtnDisable = true;
                this.viewDisabled = true;
                this.editDisabled = true;
                $("#target input,select").prop("disabled", true);
                $("#target1 input,select").prop("disabled", true);
            }
        }
    }

    editFunction() {
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.route.paramMap.subscribe((data) => this.viewId = + data.get('viewId'));

        if (this.id > 0) {
            this.ajaxCount++;
            this.DebitnoteService.getItemsDetail(this.id).subscribe(
                data => {
                    this.ajaxCount--;
                    this.debitnoteData = data['result'];
                    this.debitNotelist = data['result'];

                    this.debitnoteData["f031ftype"] = data['result'][0]['f031ftype'],
                        this.type = data['result'][0]['f031ftype'],
                        this.debitnoteData['f031fdate'] = data['result'][0]['f031fdate'],
                        this.debitnoteData['f031fidInvoice'] = data['result'][0]['f031fidInvoice']
                    this.debitnoteData['f031fdescription'] = data['result'][0]['f031fdescription'],
                        this.debitnoteData['f031fnarration'] = data['result'][0]['f031fdescription'],
                        this.debitnoteData["f031frevenueType"] = data['result'][0]['f031frevenueType'];
                    this.debitnoteData['f031freferenceNumber'] = data['result'][0]['f031freferenceNumber'],
                        this.debitnoteData['f031ftotalAmount'] = data['result'][0]['f031ftotalAmount'],
                        this.debitnoteData['f031fvoucherNumber'] = data['result'][0]['f031fvoucherNumber'],
                        this.debitnoteData['f031fidCustomer'] = data['result'][0]['f031fidCustomer']
                    this.getInvoiceList();
                    var balanceInvoiceAmount = 0;
                    for (var i = 0; i < this.debitNotelist.length; i++) {
                        var creditnoteObject = {};
                        creditnoteObject['f032fid'] = this.debitNotelist[i]['f032fid'];
                        creditnoteObject['f072fidItem'] = parseInt(this.debitNotelist[i]['f032fidItem']);
                        creditnoteObject['f072fdebitFundCode'] = this.debitNotelist[i]['f032fcreditFundCode'];
                        creditnoteObject['f072fdebitAccountCode'] = this.debitNotelist[i]['f032fcreditAccountCode'];
                        creditnoteObject['f072fdebitActivityCode'] = this.debitNotelist[i]['f032fcreditActivityCode'];
                        creditnoteObject['f072fdebitDepartmentCode'] = this.debitNotelist[i]['f032fcreditDepartmentCode'];

                        creditnoteObject['f072fcreditFundCode'] = this.debitNotelist[i]['f032fdebitFundCode'];
                        creditnoteObject['f072fcreditAccountCode'] = this.debitNotelist[i]['f032fdebitAccountCode'];
                        creditnoteObject['f072fcreditActivityCode'] = this.debitNotelist[i]['f032fdebitActivityCode'];
                        creditnoteObject['f072fcreditDepartmentCode'] = this.debitNotelist[i]['f032fdebitDepartmentCode'];

                        creditnoteObject['f072fquantity'] = this.debitNotelist[i]['f032fquantity'];
                        creditnoteObject['f072fprice'] = this.debitNotelist[i]['f032fprice'];
                        creditnoteObject['f072fgstCode'] = parseInt(this.debitNotelist[i]['f032fgstCode']);
                        creditnoteObject['f072fgstValue'] = this.debitNotelist[i]['f032fgstValue'];
                        creditnoteObject['f072ftotal'] = this.debitNotelist[i]['f032ftotal'];
                        creditnoteObject['f032fbalanceAmount'] = this.debitNotelist[i]['f032fbalanceAmount'],
                            creditnoteObject['f032ftoDnAmount'] = this.debitNotelist[i]['f032ftoDnAmount'];

                        // balanceInvoiceAmount = (this.ConvertToFloat(this.debitNotelist[i]['f032ftotal'])) + (this.ConvertToFloat(this.debitNotelist[i]['f032ftoDnAmount']));
                        // creditnoteObject['f032fbalanceAmount'] = balanceInvoiceAmount; 
                        this.maininvoiceListBasedOnInvoiceId.push(creditnoteObject);
                    }
                    if (data['result'][0]['f031fstatus'] == 1 || this.debitNotelist[0]['f031fstatus'] == 2) {
                        this.viewDisabled = true;
                        this.editDisabled = true;
                        $("#target input,select").prop("disabled", true);
                        $("#target1 input,select").prop("disabled", true);
                    }
                    if (this.viewId > 1) {
                        this.saveBtnDisable = true;
                        this.viewDisabled = true;
                        this.editDisabled = true;
                        $("#target input,select").prop("disabled", true);
                        $("#target1 input,select").prop("disabled", true);
                    }
                }, error => {
                    console.log(error);
                });
        }
    }


    ngOnInit() {
        this.ajaxCount = 0
        this.spinner.show();
        this.viewDisabled = false;
        this.editDisabled = false;
        this.debitnoteAddedAmount = 0;
        //  this.maininvoiceList=[];

        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        this.route.paramMap.subscribe((data) => this.viewId = + data.get('viewId'));

        this.valueDate = new Date().toISOString().substr(0, 10);
        this.debitnoteData['f031fdate'] = this.valueDate;
        let invoicetypeobj = {};
        invoicetypeobj['name'] = 'Student';
        invoicetypeobj['type'] = 'DSTD';
        this.invoicetypeList.push(invoicetypeobj);
        invoicetypeobj = {};
        invoicetypeobj['name'] = 'Staff';
        invoicetypeobj['type'] = 'DSTA';
        this.invoicetypeList.push(invoicetypeobj);
        invoicetypeobj = {};
        invoicetypeobj['name'] = 'Other Receivables';
        invoicetypeobj['type'] = 'DOR';
        this.invoicetypeList.push(invoicetypeobj);
        //User dropdown
        this.ajaxCount++;
        this.UserService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.userList = data['result']['data'];
            }
        );
        //revenue type dropdown
        this.RevenueTypeService.getItems().subscribe(
            data => {
                this.revenuetypeList = data['result'];
            }, error => {
                console.log(error);
            });
        //Item dropdown
        this.ajaxCount++;
        this.RevenuesetupService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.itemList = data['result'];
            }
        );
        //Department dropdown
        this.ajaxCount++;
        this.DepartmentService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.DeptList = data['result']['data'];
            }
        );
        //Activity dropdown
        this.ajaxCount++;
        this.ActivitycodeService.getItemsByLevel().subscribe(
            data => {
                this.ajaxCount--;
                this.activitycodeList = data['result']['data'];
            }
        );
        //  staff dropdown
        this.ajaxCount++;
        this.StaffService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.smartStaffList = data['result']['data'];
            }
        );

        //Account dropdown
        this.ajaxCount++;
        this.AccountcodeService.getItemsByLevel().subscribe(
            data => {
                this.ajaxCount--;
                this.accountcodeList = data['result']['data'];
            }
        );
        //Fund dropdown
        this.ajaxCount++;
        this.FundService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.fundList = data['result']['data'];
            }
        );
        //FeeItem Dropdown
        this.ajaxCount++;
        this.FeeitemService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.feeItemList = data['result']['data'];
            }
        );
        //Taxcode dropdown
        this.ajaxCount++;
        this.TaxsetupcodeService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.taxcodeList = data['result']['data'];
            }
        );
        //Student dropdown
        this.ajaxCount++;
        this.StudentService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.studentList = data['result']['data'];
            }
        );

        //Sponser dropdown
        this.ajaxCount++;
        this.SponsorService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.sponserList = data['result']['data'];
            }
        );

        //Customer dropdown
        this.ajaxCount++;
        this.CustomerService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.customerList = data['result']['data'];
            });

        //Revenue iTem dropdown
        //Revenue Item
        this.RevenuesetupService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.revenueitemList = data['result'];
            }, error => {
                console.log(error);
            });
    }
    getType() {
        let typeObject = {};
        typeObject['type'] = this.debitnoteData['f031ftype'];
        this.type = this.debitnoteData['f031ftype'];
        this.getDebitNoteNumber();
        return this.type;
    }
    getDebitNoteNumber() {
        let typeObject = {};
        typeObject['type'] = this.debitnoteData['f031ftype'];
        this.DebitnoteService.getDebitnoteNumber(typeObject).subscribe(
            newData => {
                this.debitnoteData['f031freferenceNumber'] = newData['number'];
            }
        );
    }
    getInvoiceList() {
        // this.ajaxCount = 0this.ajaxCount = 0;
        this.ajaxCount++;
        this.UserId = this.debitnoteData['f031fidCustomer'];
        let invoiceObj = {};
        switch (this.type) {
            case 'DSTA':
                invoiceObj['type'] = "STA";
                break;
            case 'DSTD':
                invoiceObj['type'] = "STD";
                break;
            case 'DOR':
                invoiceObj['type'] = "OR";
                break;
        }
        invoiceObj['UserId'] = this.UserId;
        this.DebitnoteService.getInvoiceItems(invoiceObj).subscribe(
            data => {

                this.maininvoiceList = data['result']['data'];

            }
        );
    }
    getInvoiceDetails() {
        let invid = 0;
        invid = this.debitnoteData['f031fidInvoice'];
        this.MaininvoiceService.getInvoiceDetailsById(invid).subscribe(
            data => {
                this.maininvoiceListBasedOnInvoiceId = data['result'];
                this.debitnoteData['f031frevenueType'] = data['result'][0]['f071fbillType'];
                this.debitnoteData['f031fdescription'] = data['result'][0]['f071fdescription'];
                for (var i = 0; i < this.maininvoiceListBasedOnInvoiceId.length; i++) {
                    this.maininvoiceListBasedOnInvoiceId[i]['f032fidInvoiceDetail'] = this.maininvoiceListBasedOnInvoiceId[i]['f072fid'];
                    this.maininvoiceListBasedOnInvoiceId[i]['f072fgstCode'] = parseInt(this.maininvoiceListBasedOnInvoiceId[i]['f072fgstCode']);
                    this.maininvoiceListBasedOnInvoiceId[i]['f072fidItem'] = parseInt(this.maininvoiceListBasedOnInvoiceId[i]['f072fidItem']);
                    this.maininvoiceListBasedOnInvoiceId[i]['f032fbalanceAmount'] = this.ConvertToFloat(this.maininvoiceListBasedOnInvoiceId[i]['f072fbalanceAmount']).toFixed(2);
                    // this.getBalanceAmount();
                }
                this.debitnoteData['f031ftotalAmount'] = parseFloat(data['result'][0]['f071finvoiceTotal']).toFixed(2);
                this.debitnoteData['invoiceDate'] = data['result'][0]['f071finvoiceDate']
                // this.debitnoteData['f031fbalance'] = parseFloat(data['result'][0]['f071fbalance']).toFixed(2);
            }
        );
    }

    saveData() {
        if (this.debitnoteData['f031ftype'] == undefined) {
            alert('Select Invoice Type');
            return false;
        }
        if (this.debitnoteData['f031fidCustomer'] == undefined) {
            if (this.type == 'DSTD') {
                alert("Select Student")
                return false;
            }
            if (this.type == 'DOR') {
                alert("Select Customer")
                return false;
            }
            if (this.type == 'DSTA') {
                alert("Select Staff")
                return false;
            }
        }
        if (this.debitnoteData['f031fidInvoice'] == undefined) {
            alert('Select Invoice Number');
            return false;
        }
        if (this.debitnoteData['f031fdescription'] == undefined) {
            alert('Enter Description');
            return false;
        }
        for (var i = 0; i < this.maininvoiceListBasedOnInvoiceId.length; i++) {
            if (this.maininvoiceListBasedOnInvoiceId[i]['f032ftoDnAmount'] == undefined) {
                alert('Enter Debit Note Amount(RM)');
                return false;
            }
            if (this.maininvoiceListBasedOnInvoiceId[i]['f072ftotal'] < this.maininvoiceListBasedOnInvoiceId[i]['f032ftoDnAmount']) {
                alert('Debit Note amount should not greater than total invoice');
                return false;
            }
            // if (this.maininvoiceListBasedOnInvoiceId[i]['f032ftoDnAmount'] > this.maininvoiceListBasedOnInvoiceId[i]['f032fbalanceAmount']) {
            //     alert('Debit Note amount should not greater than Balance Amount');
            //     return false;
            // }
        }
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        this.saveDataObject = {};

        var saveDetails = [];
        if(this.id>0){
        this.saveDataObject['f031fid'] = this.debitnoteData['f031fid'];
        }
        this.saveDataObject["f031ftype"] = this.debitnoteData['f031ftype'];
        this.saveDataObject["f031fidInvoice"] = this.debitnoteData['f031fidInvoice'];
        this.saveDataObject["f031ftotalAmount"] = this.ConvertToFloat(this.debitnoteData['f031ftotalAmount']).toFixed(2);
        this.saveDataObject["f031freferenceNumber"] = this.debitnoteData['f031freferenceNumber'];
        this.saveDataObject["f031fdate"] = this.debitnoteData['f031fdate'];
        this.saveDataObject["f031fvoucherNumber"] = this.debitnoteData['f031fvoucherNumber'];
        this.saveDataObject["f031frevenueType"] = this.debitnoteData['f031frevenueType'];
        this.saveDataObject["f031fnarration"] = this.debitnoteData['f031fnarration'];
        this.saveDataObject["f031fdescription"] = this.debitnoteData['f031fdescription'];
        this.saveDataObject["f031fidCustomer"] = this.debitnoteData['f031fidCustomer'];
        this.saveDataObject["f031fstatus"] = 0;
        var balanceInvoiceAmount = 0;
        for (var i = 0; i < this.maininvoiceListBasedOnInvoiceId.length; i++) {
            var creditnoteObject = {}
            if (this.id > 0) {
                creditnoteObject['f032fid'] = this.maininvoiceListBasedOnInvoiceId[i]['f032fid'];
            }
            creditnoteObject['f032fidInvoiceDetail'] = this.maininvoiceListBasedOnInvoiceId[i]['f072fid'];
            creditnoteObject['f032fidItem'] = this.maininvoiceListBasedOnInvoiceId[i]['f072fidItem'];
            creditnoteObject['f032fdebitFundCode'] = this.maininvoiceListBasedOnInvoiceId[i]['f072fdebitFundCode'];
            creditnoteObject['f032fdebitAccountCode'] = this.maininvoiceListBasedOnInvoiceId[i]['f072fdebitAccountCode'];
            creditnoteObject['f032fdebitActivityCode'] = this.maininvoiceListBasedOnInvoiceId[i]['f072fdebitActivityCode'];
            creditnoteObject['f032fdebitDepartmentCode'] = this.maininvoiceListBasedOnInvoiceId[i]['f072fdebitDepartmentCode'];

            creditnoteObject['f032fcreditFundCode'] = this.maininvoiceListBasedOnInvoiceId[i]['f072fcreditFundCode'];
            creditnoteObject['f032fcreditAccountCode'] = this.maininvoiceListBasedOnInvoiceId[i]['f072fcreditAccountCode'];
            creditnoteObject['f032fcreditActivityCode'] = this.maininvoiceListBasedOnInvoiceId[i]['f072fcreditActivityCode'];
            creditnoteObject['f032fcreditDepartmentCode'] = this.maininvoiceListBasedOnInvoiceId[i]['f072fcreditDepartmentCode'];
            creditnoteObject['f032fquantity'] = this.maininvoiceListBasedOnInvoiceId[i]['f072fquantity'];
            creditnoteObject['f032fprice'] = this.ConvertToFloat(this.maininvoiceListBasedOnInvoiceId[i]['f072fprice']).toFixed(2);
            creditnoteObject['f032fgstCode'] = this.maininvoiceListBasedOnInvoiceId[i]['f072fgstCode'];
            creditnoteObject['f032fgstValue'] = this.maininvoiceListBasedOnInvoiceId[i]['f072fgstValue'];
            creditnoteObject['f032ftotal'] = this.ConvertToFloat(this.maininvoiceListBasedOnInvoiceId[i]['f072ftotal']).toFixed(2);
            creditnoteObject['f032ftoDnAmount'] = this.ConvertToFloat(this.maininvoiceListBasedOnInvoiceId[i]['f032ftoDnAmount']).toFixed(2);
            creditnoteObject['f032fbalanceAmount'] = this.ConvertToFloat(this.ConvertToFloat(this.maininvoiceListBasedOnInvoiceId[i]['f032fbalanceAmount']) - this.ConvertToFloat(this.maininvoiceListBasedOnInvoiceId[i]['f032ftoDnAmount'])).toFixed(2);

            // creditnoteObject['f032fbalanceAmount'] = this.maininvoiceListBasedOnInvoiceId[i]['f032ftoDnAmount']+this.ConvertToFloat(this.maininvoiceListBasedOnInvoiceId[i]['f032fbalanceAmount']).toFixed(2);
            saveDetails.push(creditnoteObject);

        }
        this.saveDataObject["details"] = saveDetails;
        if (this.id > 0) {
            this.DebitnoteService.insertdebitNoteItems(this.saveDataObject).subscribe(
                data => {
                    this.debitnoteData = data['result'];

                    this.router.navigate(['/accountsrecivable/debitnote']);
                    alert("Debit Note Entry has been Updated Succcessfully");
                }, error => {
                    console.log(error);
                });
        } else {
            this.DebitnoteService.insertdebitNoteItems(this.saveDataObject).subscribe(
                data => {
                    this.debitnoteData = data['result'];

                    this.router.navigate(['/accountsrecivable/debitnote']);
                    alert("Debit Note Entry has been Added Succcessfully");
                }, error => {
                    console.log(error);
                });
        }
    }
    ConvertToFloat(val) {
        return parseFloat(val);
    }
    onlyNumberKey(event) {
        if (event.charCode == 46) {

        } else {
            return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
        }
    }
    maximize() {
        $("#myModal").addClass("modal");
        $("#overlay1").show();
        $(".maximize-icon").addClass("hidden");
        $(".minimize-icon").removeClass("hidden");
        $(".modal-backdrop").show();

    }
    minimize() {
        $("#myModal").removeClass("modal");
        $("#overlay1").hide();
        $(".maximize-icon").removeClass("hidden");
        $(".minimize-icon").addClass("hidden");
        $(".modal-backdrop").hide();
        $('#myModal').show();

    }
    getList() {
        this.spinner.show();
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.DebitnoteapprovalService.getItems().subscribe(
            data => {
                this.spinner.hide();
                this.debitnoteApprovalList = data['result'];

            }, error => {
                console.log(error);
            });
    }
    ApprovalListData() {
        let approveDarrayList = [];

        for (var i = 0; i < this.debitnoteApprovalList.length; i++) {
            if (this.debitnoteApprovalList[i].f031fstatus == true) {
                approveDarrayList.push(this.debitnoteApprovalList[i].f031fid);
            }
        }
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        approveDarrayList.push(this.id);
        var confirmPop = confirm("Do you want to Approve?");
        if (confirmPop == false) {
            return false;
        }
        var approvalObject = {};
        approvalObject['id'] = approveDarrayList;
        approvalObject['status'] = '1';
        approvalObject['reason'] = '';

        this.DebitnoteapprovalService.updateApproval(approvalObject).subscribe(
            data => {
                this.getList();
                this.debitnoteApprovalData['reason'] = '';
                this.router.navigate(['/accountsrecivable/debitnoteapproval']);
                alert("Approved Successfully");
            }, error => {
                console.log(error);
            });

    }
    regectdebitorApprovalListData() {
        let approveDarrayList = [];
        for (var i = 0; i < this.debitnoteApprovalList.length; i++) {
            if (this.debitnoteApprovalList[i].f031fstatus == true) {
                approveDarrayList.push(this.debitnoteApprovalList[i].f031fid);
            }
        }
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        approveDarrayList.push(this.id);
        if (this.debitnoteApprovalData['reason'] == undefined) {
            alert('Enter the Description');
            return false;
        }
        var confirmPop = confirm("Do you want to Reject?");
        if (confirmPop == false) {
            return false;
        }
        var debitnoteDataObject = {}
        debitnoteDataObject['id'] = approveDarrayList;
        debitnoteDataObject['status'] = '2';
        debitnoteDataObject['reason'] = this.debitnoteApprovalData['reason'];
        this.DebitnoteapprovalService.updateApproval(debitnoteDataObject).subscribe(
            data => {
                this.getList();
                this.debitnoteApprovalData['reason'] = '';
                this.router.navigate(['/accountsrecivable/debitnoteapproval']);
                alert("Rejected Successfully");
            }, error => {
                console.log(error);
            });
    }
}