import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { OnlinePaymentService } from '../service/onlinepayment.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
    selector: 'OnlinePaymentView',
    templateUrl: 'onlinepaymentview.component.html'
})

export class OnlinePaymentViewComponent implements OnInit {
    paymentList =  [];
    id: number;
    date;
    bank;

    constructor(
        
        private OnlinePaymentService: OnlinePaymentService,
        private route: ActivatedRoute,
        private router: Router

    ) { }

    ngOnInit() { 
        this.route.paramMap.subscribe((data) => this.bank = + data.get('id'));

        this.route.paramMap.subscribe((data) => this.date =  data.get('date'));
        console.log(this.date);
        this.OnlinePaymentService.getItemsByGroup(this.date,this.bank).subscribe(
            data => {
                this.paymentList = data['result'];
        }, error => {
            console.log(error);
        });
    }
}