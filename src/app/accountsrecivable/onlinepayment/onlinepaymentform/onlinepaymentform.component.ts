import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { PremiseService } from '../../service/premise.service';
import { ActivatedRoute, Router } from '@angular/router';
import { BankService } from '../../../generalsetup/service/bank.service';
import { OnlinePaymentService } from '../../service/onlinepayment.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { HttpClientModule, HttpHeaders, HttpClient } from '@angular/common/http';
import { environment } from "../../../../environments/environment";
@Component({
    selector: 'OnlinePaymentComponent',
    templateUrl: 'onlinepaymentform.component.html'
})

export class OnlinePaymentFormComponent implements OnInit {
    premiseform: FormGroup;
    paymentList = [];
    paymentData = {};
    bankList = [];
    typeList = [];
    bankList1 = [];
    file: File;
    fileData: any;
    id: number;
    downloadUrl: string = environment.api.downloadbase;
    constructor(
        private OnlinePaymentService: OnlinePaymentService,
        private BankService: BankService,
        private route: ActivatedRoute,
        private router: Router,
        private httpClient: HttpClient
    ) { }

    ngOnInit() {

        let typeObject = {};
        typeObject['id'] = 1;
        typeObject['name'] = 'Bill Payment';
        this.typeList.push(typeObject);
        typeObject = {};
        typeObject['id'] = 2;
        typeObject['name'] = "Bill Presentment";
        this.typeList.push(typeObject);
        typeObject = {};
        typeObject['id'] = 3;
        typeObject['name'] = "MIGS";
        this.typeList.push(typeObject);
        typeObject = {};
        typeObject['id'] = 4;
        typeObject['name'] = "FPX";
        this.typeList.push(typeObject);

        let bankObject = {};
        bankObject['id'] = 1;
        bankObject['name'] = 'Malayan Banking Berhad';
        this.bankList1.push(bankObject);
        bankObject = {};
        bankObject['id'] = 2;
        bankObject['name'] = 'CIMB Bank Berhad';
        this.bankList1.push(bankObject);
        bankObject = {};
        bankObject['id'] = 3;
        bankObject['name'] = 'Bank Islam Malaysia';
        this.bankList1.push(bankObject);

        this.OnlinePaymentService.getItems().subscribe(
            data => {
                this.paymentList = data['result']['data'];
            }, error => {
                console.log(error);
            });


        // country dropdown
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.BankService.getItems().subscribe(
            data => {
                this.bankList = data['result']['data'];
            }, error => {
                console.log(error);
            });

        if (this.id > 0) {
            this.OnlinePaymentService.getItemsDetail(this.id).subscribe(
                data => {
                    this.paymentData = data['result'][0];
                    this.paymentData['f078fstatus'] = parseInt(this.paymentData['f078fstatus']);

                }, error => {
                    console.log(error);
                });
        }

        console.log(this.paymentData);

    }

    addPayment() {
        console.log(this.paymentData);
        let fd = new FormData();
        fd.append('file', this.file);
        this.httpClient.post(this.downloadUrl, fd)
            .subscribe(
                data => {
                    this.paymentData['file'] = data['name'];

                    this.OnlinePaymentService.insertPaymentItems(this.paymentData).subscribe(
                        data => {
                            this.router.navigate(['accountsrecivable/onlinebankpayment']);

                        }, error => {
                            console.log(error);
                        });
                }, error => {
                    console.log(error);
                });
    }
    // fileSelected(event) {
    //     this.file = event.target.files[0];
    //     let fileReader = new FileReader();
    //     fileReader.onload = (e) => {
    //         this.paymentData['fileData'] = fileReader.result;
    //         this.paymentData['f078ffileName'] = this.file.name;
    //     }
    //     fileReader.readAsText(this.file);

    // }
    fileSelected(event) {

        this.file = event.target.files[0];
        console.log(this.file);
    }
    setbank() {
        if (this.paymentData['f078ftype'] == 'MIGS' || this.paymentData['f078ftype'] == 'FPX') {
            this.paymentData['f078fidBank'] = 6;
        }
        else {
            this.paymentData['f078fidBank'] = '';
        }
    }
}