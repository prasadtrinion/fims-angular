import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { OnlinePaymentService } from '../service/onlinepayment.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'OnlinePayment',
    templateUrl: 'onlinepayment.component.html'
})

export class OnlinePaymentComponent implements OnInit {
    paymentList =  [];
    id: number;

    constructor(
        
        private OnlinePaymentService: OnlinePaymentService,
        private spinner:NgxSpinnerService

    ) { }

    ngOnInit() { 
        this.spinner.show();
        this.OnlinePaymentService.getItems().subscribe(
            data => {
                this.spinner.hide();
                this.paymentList = data['result']['data'];
        }, error => {
            console.log(error);
        });
    }
}