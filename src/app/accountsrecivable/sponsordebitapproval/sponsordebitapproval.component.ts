import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Injector } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SponsorDebitnoteapprovalService } from '../service/sponsordebitapproval.service'
import { faSearch, faEdit } from '@fortawesome/free-solid-svg-icons';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'SponsorDebitapprovalComponent',
    templateUrl: 'sponsordebitapproval.component.html'
})

export class SponsorDebitapprovalComponent implements OnInit {
    creditnoteApprovalList = [];
    creditnoteApprovalData = {};
    invoiceapprovalData=[];
    id: number;
    selectAllCheckbox:false;
    type : string;
    title ='';
    faSearch = faSearch;
    faEdit = faEdit;
    constructor(        
        private CreditnoteapprovalService: SponsorDebitnoteapprovalService,
        private route: ActivatedRoute,
        private spinner: NgxSpinnerService,
        private router: Router
    ) { }
    ngOnInit() { 
        this.getList();
    }
    getList() {
        this.spinner.show();                                
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        this.CreditnoteapprovalService.getItems().subscribe(
            data => {
                this.spinner.hide();
                this.creditnoteApprovalList = data['result'];
        }, error => {
            console.log(error);
        });
    }
    ApprovalListData() {
    let approveDarrayList = [];
    console.log(this.creditnoteApprovalList);
        for (var i = 0; i < this.creditnoteApprovalList.length; i++) {
            if(this.creditnoteApprovalList[i].ApprovalStatus==true) {
                approveDarrayList.push(this.creditnoteApprovalList[i].f063fid);
            }
        }
        if(approveDarrayList.length<1) {
            alert("Select atleast one Sponsor Debit Note to Approve");
            return false;
        }
        var confirmPop = confirm("Do you want to Approve?");
        if (confirmPop == false) {
            return false;
        }
        var approvalObject = {};
        approvalObject['id'] = approveDarrayList;
        approvalObject['status'] = '1';
        approvalObject['reason']='';

        this.CreditnoteapprovalService.updateApproval(approvalObject).subscribe(
            data => {
                this.getList();
                alert("Approved Successfully");
                this.invoiceapprovalData['reason']='';
            }, error => {
            console.log(error);
        });

    }
    regectJournalApprovalListData() {
        let approveDarrayList = [];
    console.log(this.creditnoteApprovalList);
        for (var i = 0; i < this.creditnoteApprovalList.length; i++) {
            if(this.creditnoteApprovalList[i].ApprovalStatus==true) {
                approveDarrayList.push(this.creditnoteApprovalList[i].f063fid);
            }
        }
        if(approveDarrayList.length<1) {
            alert("Select atleast one Sponsor Debit Note to Reject");
            return false;
        }
        // if(this.budgetactivityapprovalList.length<2) {
        //     alert("Please select atleast one cost center to Reject");
        //     return false;
        // }
        if (this.invoiceapprovalData['reason'] == undefined) {
            alert('Enter the Description');
            return false;
        }
        var confirmPop = confirm("Do you want to Reject?");
        if (confirmPop == false) {
            return false;
        }
        var creditnoteDataObject = {}
        creditnoteDataObject['id'] = approveDarrayList;
        creditnoteDataObject['status'] = '2';
        creditnoteDataObject['reason'] = this.invoiceapprovalData['reason'];
        this.CreditnoteapprovalService.updateApproval(creditnoteDataObject).subscribe(
            data => {
                this.getList();
                this.invoiceapprovalData['reason']='';
                alert("Rejected Successfully");
            }, error => {
                console.log(error);
            });


    }
    selectAll() {
        console.log(this.selectAllCheckbox);
        for (var i = 0; i < this.creditnoteApprovalList.length; i++) {
                this.creditnoteApprovalList[i].f031fstatus = this.selectAllCheckbox;
        }
        //console.log(this.budgetcostcenterapprovalList);
      }

}