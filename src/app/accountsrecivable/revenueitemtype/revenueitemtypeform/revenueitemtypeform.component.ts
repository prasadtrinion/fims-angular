import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { RevenueitemService } from '../../service/revenueitem.service';
import { ActivatedRoute, Router } from '@angular/router';
import { GlcodegenerationService } from '../../../generalledger/service/glcodegeneration.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
@Component({
    selector: 'RevenueItemTypeFormComponent',
    templateUrl: 'revenueitemtypeform.component.html'
})

export class RevenueItemTypeFormComponent implements OnInit {
    revenueitemform: FormGroup;
    revenueitemData = {};
    revenueitemList = [];
    glCodeList = [];
    

    id: number;
    constructor(
        private RevenueitemService: RevenueitemService,
        private GlcodeService: GlcodegenerationService,
        private route: ActivatedRoute,
        private router: Router
    ) { }

    ngOnInit() {
        this.revenueitemData['f036fstatus'] = 1;
        this.revenueitemData['f036fidGlcode']='';
        

        this.revenueitemform = new FormGroup({
            premise: new FormControl('', Validators.required),
            
        });

        this.GlcodeService.getItems().subscribe(
            data=>{
                this.glCodeList = data['result']['data'];
            }
        );
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        console.log(this.id);

        if (this.id > 0) {
            this.RevenueitemService.getItemsDetail(this.id).subscribe(
                data => {
                    this.revenueitemData = data['result'][0];
                    this.revenueitemData['f036fstatus'] = parseInt(this.revenueitemData['f036fstatus']);
                    
                }, error => {
                    console.log(error);
                });
        }
        console.log(this.id);
    }

    addRevenueitem() {
        console.log(this.revenueitemData);
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.RevenueitemService.updateRevenueItems(this.revenueitemData, this.id).subscribe(
                data => {
                    this.router.navigate(['accountsrecivable/revenueitemtype']);
                }, error => {
                    console.log(error);
                });
        } else {
            this.RevenueitemService.insertRevenueItems(this.revenueitemData).subscribe(
                data => {
                    this.router.navigate(['accountsrecivable/revenueitemtype']);
                    alert("data added")
                }, error => {
                    console.log(error);
                });
        }
    }
}