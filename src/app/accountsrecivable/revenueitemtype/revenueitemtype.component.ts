import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { RevenueitemService } from '../service/revenueitem.service';


@Component({
    selector: 'Revenueitemtype',
    templateUrl: 'revenueitemtype.component.html'
})

export class RevenueitemtypeComponent implements OnInit {
    revenueitemList =  [];
    revenueitemData = {};
    id: number;

    constructor(
        
        private RevenueitemService: RevenueitemService

    ) { }

    ngOnInit() { 
        this.RevenueitemService.getItems().subscribe(
            data => {
                this.revenueitemList = data['result']['data'];
        }, error => {
            console.log(error);
        });
    } 
}