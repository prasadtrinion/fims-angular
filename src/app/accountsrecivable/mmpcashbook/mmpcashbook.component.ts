import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MmpcashbookService } from '../service/mmpcashbook.service'
import { BankService } from '../../generalsetup/service/bank.service'
import { HttpClientModule, HttpHeaders, HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { AmpcashbookService } from "../service/ampcashbook.service";
import { BanksetupaccountService } from '../../generalsetup/service/banksetupaccount.service';


@Component({
    selector: 'Mmpcashbook',
    templateUrl: 'mmpcashbook.component.html'
})

export class MmpcashbookComponent implements OnInit {
    mmpcashbookList = [];
    mmpcashbookData = {};
    bankList = [];
    bankList1 = [];
    ajaxCount: number;
    file;
    id: number;

    constructor(
        private MmpcashbookService: MmpcashbookService,
        private AmpcashbookService: AmpcashbookService,
        private BanksetupaccountService: BanksetupaccountService,
        private BankService: BankService,
        private httpClient: HttpClient,
        private route: ActivatedRoute,
        private router: Router,

    ) { }

    ngOnInit() {
        //Bank dropdown
        this.ajaxCount = 0;
        this.ajaxCount++;
        this.BankService.getCashBankItems().subscribe(
            data => {
                this.ajaxCount--;
                this.bankList = data['result']['data'];
            }
        );
         // company bank 
         this.BanksetupaccountService.getActiveCompanyBank().subscribe(
            data => {
                this.bankList1 = data['result']['data'];
            }, error => {
                console.log(error);
            });
    }
    dateComparison() {

        let fromDate = Date.parse(this.mmpcashbookData['fromDate']);
        let toDate = Date.parse(this.mmpcashbookData['toDate']);
        if (fromDate > toDate) {
            alert("From Date cannot be greater than To Date !");
            this.mmpcashbookData['toDate'] = "";
        }

    }
    fileSelected(event) {

        this.file = event.target.files[0];
        console.log(this.file);

    }
    addMmpcashbook() {
             this.mmpcashbookData['bank'] = this.mmpcashbookData['bank'];
             console.log("asfasdf");
                this.MmpcashbookService.insertMmpcashbookItems(this.mmpcashbookData).subscribe(
                    data => {
                        this.mmpcashbookList = data['result'];
                    }, error => {
                        console.log(error);
                    });
    }
    CashApprovalData() {
        let approvemmparrayList = [];
        console.log(this.mmpcashbookList);
        for (var i = 0; i < this.mmpcashbookList.length; i++) {
            if (this.mmpcashbookList[i].approvalstatus == true) {
                approvemmparrayList.push(this.mmpcashbookList[i].f096fid);
            }
        }
        if (approvemmparrayList.length < 1) {
            alert("Select atleast one Cash Book to Approve");
            return false;
        }
        var confirmPop = confirm("Do you want to Approve?");
        if (confirmPop == false) {
            return false;
        }
        var approvalObject = {};
        approvalObject['id'] = approvemmparrayList;
        approvalObject['status'] = '1';
        this.AmpcashbookService.updateApproval(approvalObject).subscribe(
            data => {
                alert("Approved Successfully");
                this.MmpcashbookService.insertMmpcashbookItems(this.mmpcashbookData).subscribe(
                    data => {
                        this.mmpcashbookList = data['result'];
                    }, error => {
                        console.log(error);
                    });
            }, error => {
                console.log(error);
            });
    }
}