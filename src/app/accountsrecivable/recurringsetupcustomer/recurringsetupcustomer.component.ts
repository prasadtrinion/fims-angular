import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { RecurringsetupcustomerService } from '../service/recurringsetupcustomer.service'
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'recurringsetupcustomer',
    templateUrl: 'recurringsetupcustomer.component.html'
})

export class RecurringsetupcustomerComponent implements OnInit {
    recurringsetupcustomerList =  [];
    recurringsetupcustomerData = {};
    id: number;

    constructor(
        private RecurringsetupcustomerService: RecurringsetupcustomerService,
        private spinner: NgxSpinnerService,

    ) { }

    generateBill(id) {
        console.log(id);
        let utilityObjectForInvoice = {};
        utilityObjectForInvoice['id'] = id;
        this.RecurringsetupcustomerService.generateBillForUtility(utilityObjectForInvoice).subscribe(
            data => {
                alert('The Invoice has been generated');
        }, error => {
            console.log(error);
        });
    }
    ngOnInit() { 
        this.spinner.show();                        
        this.RecurringsetupcustomerService.getItems().subscribe(
            data => {
                this.spinner.hide();
                this.recurringsetupcustomerList = data['result']['data'];
        }, error => {
            console.log(error);
        });
    }

    addRecurringsetupcustomer(){
           console.log(this.recurringsetupcustomerData);
        this.RecurringsetupcustomerService.insertRecurringsetupcustomerItems(this.recurringsetupcustomerData).subscribe(
            data => { 
                this.recurringsetupcustomerList = data['todos'];
            }, error => {
                console.log(error);
            });

    }
}