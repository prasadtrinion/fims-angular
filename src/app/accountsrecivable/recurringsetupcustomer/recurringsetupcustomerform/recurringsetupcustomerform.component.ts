import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { RecurringsetupcustomerService } from '../../service/recurringsetupcustomer.service'
import { ActivatedRoute, Router } from '@angular/router';
import { CustomerService } from '../../../accountsrecivable/service/customer.service';
import { PremiseService } from '../../service/premise.service'
import { CategoriesService } from '../../service/categories.service'
import { FormGroup, FormControl, Validators } from '@angular/forms'
import { FundService } from '../../../generalsetup/service/fund.service';
import { ActivitycodeService } from '../../../generalsetup/service/activitycode.service';
import { AccountcodeService } from '../../../generalsetup/service/accountcode.service';
import { DepartmentService } from '../../../generalsetup/service/department.service';
import { RevenuesetupService } from '../../service/revenuesetup.service';
import { TaxsetupcodeService } from '../../../generalledger/service/taxsetupcode.service'
import { PremisetypeService } from "../../service/premisetype.service";
import { StudentService } from '../../../studentfinance/service/student.service';
import { OnlinepaymenttypeService } from "../../service/onlinepaymenttype.service";
import { StaffService } from "../../service/staff.service";
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'RecurringsetupcustomerFormComponent',
    templateUrl: 'recurringsetupcustomerform.component.html',
    styleUrls: ['./recurringsetupcustomerform.component.css']

})

export class RecurringsetupcustomerFormComponent implements OnInit {
    recurringsetupcustomerform: FormGroup;
    recurringsetupcustomerList = [];
    numberList = [];
    itemList = [];
    recurringsetupcustomerData = {};
    customerList = [];
    customerData = [];
    premiseList = [];
    categoriesList = [];
    premiseData = {};
    DeptList = [];
    Debtortype = [];
    deptData = [];
    fundList = [];
    paymentList = [];
    fundData = {};
    deleteList = [];
    taxcodepercenList = [];
    accountcodeList = [];
    accountcodeData = {};
    activitycodeList = [];
    activitycodeData = {};
    taxcodeList = [];
    revenuesetupData = {};
    revenuesetupList = [];
    invoiceList = [];
    revenuesetupDataAdded = [];
    revenueAddedList = [];
    premiseCategoryList = [];
    premiseBasedonCatList = [];
    smartStaffList = [];
    studentList = [];
    Paymenttype = [];
    deleteDataList = [];
    type: string;
    totalAmount: number;
    showLastRow: boolean;
    showLastRowMinus: boolean;
    showLastRowPlus: boolean;
    listshowLastRowMinus: boolean;
    listshowLastRowPlus: boolean;
    id: number;
    ajaxCount: number;
    finaltotl: number;
    constructor(
        private CustomerService: CustomerService,
        private StudentService: StudentService,
        private StaffService: StaffService,
        private TaxsetupcodeService: TaxsetupcodeService,
        private RevenuesetupService: RevenuesetupService,
        private RecurringsetupcustomerService: RecurringsetupcustomerService,
        private PremiseService: PremiseService,
        private CategoriesService: CategoriesService,
        private route: ActivatedRoute,
        private router: Router,
        private ActivitycodeService: ActivitycodeService,
        private AccountcodeService: AccountcodeService,
        private DepartmentService: DepartmentService,
        private FundService: FundService,
        private PremisetypeService: PremisetypeService,
        private OnlinepaymenttypeService: OnlinepaymenttypeService,
        private spinner: NgxSpinnerService,

    ) { }
    ngDoCheck() {
        const change = this.ajaxCount;
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.ajaxCount == 0) {
            this.editFunction();
            this.spinner.hide();
            this.ajaxCount = 10;
        }
        // this.route.paramMap.subscribe((data) => this.id = + data.get('idview'));
        if (this.ajaxCount == 0 && this.revenuesetupDataAdded.length > 0) {
            this.ajaxCount = 20;
            console.log("asdfsdf");
            this.showIcons();
        }
        // console.log(this.ajaxCount);

    }
    deletemodule() {
        this.showLastRow = true;
        this.showIcons();
        this.revenuesetupData = '';
    }

    prefillLastDate(){
        var date = new Date(this.recurringsetupcustomerData['f077feffectiveDate']);
        var newdate = new Date(date);
        newdate.setDate(newdate.getDate());
        var dd = newdate.getDate();
        console.log(dd);
        console.log(newdate.getMonth());
        var mm = newdate.getMonth() + (parseInt(this.recurringsetupcustomerData['f077fperiod'])+1);
        console.log(mm);
        var y = newdate.getFullYear()
        this.recurringsetupcustomerData['f077fendDate'] = new Date(y+'-'+mm+'-'+dd).toISOString().substr(0, 10);
    }

    showIcons() {
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        console.log(this.revenuesetupDataAdded.length);
        console.log("Show last row" + this.showLastRow);
        console.log("showLastRowMinus" + this.showLastRowMinus);
        console.log("showLastRowPlus" + this.showLastRowPlus);
        console.log("listshowLastRowMinus" + this.listshowLastRowMinus);
        console.log("listshowLastRowPlus" + this.listshowLastRowPlus);

        if (this.revenuesetupDataAdded.length > 0) {
            this.showLastRowMinus = true;
            this.showLastRowPlus = true;
            this.listshowLastRowMinus = true;
        }

        if (this.revenuesetupDataAdded.length > 1) {
            if (this.showLastRow == false) {
                this.listshowLastRowPlus = false;
                this.listshowLastRowMinus = true;
                this.showLastRowPlus = true;

            } else {
                this.listshowLastRowPlus = true;
                this.listshowLastRowMinus = false;

            }
        }
        if (this.revenuesetupDataAdded.length == 1 && this.showLastRow == false) {
            this.listshowLastRowMinus = true;
            this.listshowLastRowPlus = false;

        }
        if (this.revenuesetupDataAdded.length == 1 && this.showLastRow == true) {
            this.listshowLastRowMinus = false;
            this.listshowLastRowPlus = true;

        }
        if (this.revenuesetupDataAdded.length == 0 && this.showLastRow == false) {
            this.showLastRowMinus = false;
            this.showLastRowPlus = true;
        }
        if (this.revenuesetupDataAdded.length == 0 && this.showLastRow == true) {
            this.showLastRowMinus = true;
            this.showLastRowPlus = false;
        }
        //  if(this.journalList.length>0){
        //     if(this.viewDisabled==true || this.journalList[0]['f017fapprovedStatus']==1 || this.journalList[0]['f017fapprovedStatus'] == 2) {
        //         this.listshowLastRowPlus = false;
        //     }
        // }
        // else{
        //     if(this.viewDisabled==true) {
        //         this.listshowLastRowPlus = false;
        //     }
        // }
    }
    showNewRow() {
        var confirmPop = confirm("Do you want to Add?");
        if (confirmPop == false) {
            return false;
        }
        this.showLastRow = false;
        this.showIcons();
    }
    editFunction() {
        this.ajaxCount++;
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        // this.route.paramMap.subscribe((data) => this.idview = + data.get('idview'));
        if (this.id > 0) {
            this.RecurringsetupcustomerService.getItemsDetail(this.id).subscribe(
                data => {
                    this.ajaxCount--;
                    this.revenuesetupDataAdded = data['result'];
                    //this.recurringsetupcustomerData = data['result'][0];
                    this.recurringsetupcustomerData['f077fcustomer'] = data['result'][0]['f077fcustomer'];
                    this.recurringsetupcustomerData['f077fcontractNumber'] = data['result'][0]['f077fcontractNumber'];
                    this.recurringsetupcustomerData['f077fdebitorType'] = data['result'][0]['f077fdebitorType'];
                    this.recurringsetupcustomerData['f077fdiscountEntitlement'] = parseInt(data['result'][0]['f077fdiscountEntitlement']);
                    this.recurringsetupcustomerData['f077feffectiveDate'] = data['result'][0]['f077feffectiveDate'];
                    this.recurringsetupcustomerData['f077fidPremiseCategory'] = data['result'][0]['f077fidPremiseCategory'];
                    this.recurringsetupcustomerData['f077fpremise'] = data['result'][0]['f077fpremise'];
                    this.recurringsetupcustomerData['f077fpaymentMethod'] = data['result'][0]['f077fpaymentMethod'];

                    this.recurringsetupcustomerData['f077fendDate'] = data['result'][0]['f077fendDate'];
                    this.recurringsetupcustomerData['f077fpayment'] = data['result'][0]['f077fpayment'];
                    this.recurringsetupcustomerData['f077fperiod'] = data['result'][0]['f077fperiod'];
                    this.recurringsetupcustomerData['f077fpremise'] = data['result'][0]['f077fpremise'];
                    this.recurringsetupcustomerData['f077freferenceNumber'] = data['result'][0]['f077freferenceNumber'];
                    this.recurringsetupcustomerData['f077fcategory'] = data['result'][0]['f077fcategory'];

                    this.recurringsetupcustomerData['f077fdateInvoice'] = parseInt(data['result'][0]['f077fdateInvoice']);

                    this.recurringsetupcustomerData['f077fstatus'] = parseInt(this.recurringsetupcustomerData['f077fstatus']);

                    console.log(this.recurringsetupcustomerData);
                    this.showIcons();
                    this.getPremiseBasedonCategory();
                }, error => {
                    console.log(error);
                });
        }
        console.log(this.id);
    }
    ngOnInit() {
        this.ajaxCount = 0
        this.spinner.show();
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        // this.editFunction();

        if (this.id < 1) {
            this.showLastRow = false;
            this.showLastRowMinus = false;
            this.showLastRowPlus = true;
        } else {
            this.showLastRow = true;

        }
        //recurringsetupcustomerData.f077fdiscountEntitlement
        this.recurringsetupcustomerData['f077fdiscountEntitlement'] = 1;
        // this.recurringsetupcustomerData['f077fcustomer']='';
        // this.recurringsetupcustomerData['f077fpremise']='';
        // this.recurringsetupcustomerData['f077fcategory']='';

        let debtortypeobj = {};
        debtortypeobj['name'] = 'Student';
        debtortypeobj['type'] = 'STD';
        this.Debtortype.push(debtortypeobj);
        debtortypeobj = {};
        debtortypeobj['name'] = 'Staff';
        debtortypeobj['type'] = 'STA';
        this.Debtortype.push(debtortypeobj);
        debtortypeobj = {};
        debtortypeobj['name'] = 'Others';
        debtortypeobj['type'] = 'OR';
        this.Debtortype.push(debtortypeobj);


        let paymenttypeobj = {};
        paymenttypeobj['name'] = 'Cheque';
        this.Paymenttype.push(paymenttypeobj);
        paymenttypeobj = {};
        paymenttypeobj['name'] = 'Cash';
        this.Paymenttype.push(paymenttypeobj);
        //Student dropdown
        this.ajaxCount++;
        this.StudentService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                console.log(this.ajaxCount);

                this.studentList = data['result']['data'];
            }
        );

        //  staff dropdown
        this.ajaxCount++;
        this.StaffService.getItems().subscribe(
            data => {
                console.log(data);
                this.ajaxCount--;
                this.smartStaffList = data['result']['data'];
            }
        );
        //Payment dropdown
        this.ajaxCount++;
        this.OnlinepaymenttypeService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.paymentList = data['result']['data'];
            }
        );

        for (var i = 1; i < 30; i++) {
            let number = {};
            if (i > 28) {
                number['key'] = 0;
                number['value'] = 'Last day of month';
            } else {
                number['key'] = i;
                number['value'] = i;
            }
            this.numberList.push(number);
        }

        // let newnumber = {};
        // newnumber['key'] = 50;
        // newnumber['value'] = 'Last Day of Month';

        // this.numberList.push(newnumber);
        this.ajaxCount++;
        this.RevenuesetupService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.revenuesetupList = data['result'];
            }, error => {
                console.log(error);
            });


        this.recurringsetupcustomerform = new FormGroup({
            recurringsetupcustomer: new FormControl('', Validators.required),

        });
        this.recurringsetupcustomerData['f077fstatus'] = 1;
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        //Premise Category
        this.ajaxCount++;
        this.PremisetypeService.getActiveItems().subscribe(
            data => {
                this.ajaxCount--;
                this.premiseCategoryList = data['result'];
            }, error => {
                console.log(error);
            });
        // premise dropdown 
        // this.ajaxCount++;
        // this.PremiseService.getActiveItems().subscribe(
        //     data => {
        //         this.ajaxCount--;
        //         this.premiseList = data['result']['data'];
        //     }, error => {
        //         console.log(error);
        //     });

        //Fund Dropdown
        this.ajaxCount++;
        this.FundService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.fundList = data['result']['data'];
            }, error => {
                console.log(error);
            });
        // activity dropdown
        this.ajaxCount++;
        this.ActivitycodeService.getItemsByLevel().subscribe(
            data => {
                this.ajaxCount--;
                console.log(data);
                this.activitycodeList = data['result']['data'];
            }, error => {
                console.log(error);
            });
        // account dropdown
        this.ajaxCount++;
        this.AccountcodeService.getItemsByLevel().subscribe(
            data => {
                this.ajaxCount--;
                this.accountcodeList = data['result']['data'];
                for (var i = 0; i < this.accountcodeList.length; i++) {
                    if (this.accountcodeList[i]['f059fid'] == '1046') {
                        this.accountcodeList[i]['f059fcompleteCode'] = '00000';
                    }
                }
            }, error => {
                console.log(error);
            });

        // department dropdown
        this.ajaxCount++;
        this.DepartmentService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.DeptList = data['result']['data'];
            }, error => {
                console.log(error);
            });


        // customer dropdown
        this.ajaxCount++;
        this.CustomerService.getActiveItems().subscribe(
            data => {
                this.ajaxCount--;
                this.customerList = data['result']['data'];
            }, error => {
                console.log(error);
            });
        //TaxCode Dropdown
        this.ajaxCount++;
        this.TaxsetupcodeService.getActiveItems().subscribe(
            data => {
                this.ajaxCount--;
                this.taxcodeList = data['result']['data'];
            });

        // category dropdown
        this.ajaxCount++;
        this.CategoriesService.getActiveItems().subscribe(
            data => {
                this.ajaxCount--;
                this.categoriesList = data['result'];
            }, error => {
                console.log(error);
            });
        console.log(this.id);
        this.showIcons();

    }

    getPremiseBasedonCategory() {
        let itemId = this.recurringsetupcustomerData['f077fidPremiseCategory'];
        this.ajaxCount--;
        this.PremiseService.getItemsDetail(itemId).subscribe(
            data => {
                this.ajaxCount--;
                this.premiseBasedonCatList = data['result'];
            }, error => {
                console.log(error);
            });
    }

    getType() {
        let typeObject = {};
        typeObject['type'] = this.recurringsetupcustomerData['f077fdebitorType'];
        this.type = this.recurringsetupcustomerData['f077fdebitorType']
        console.log(typeObject);
        return this.type;
    }
    // getpercentage() {
    //     let percentageCode = this.revenuesetupData['f077fgstCode'];
    //     for (var i = 0; i < this.taxcodeList.length; i++) {
    //         if (this.taxcodeList[i]['f081fid'] == this.revenuesetupData['f077fgstCode']) {
    //             this.revenuesetupData['f077fgstPercentage'] = this.taxcodeList[i]['f081fpercentage'];
    //         }
    //     }
    // }
    getpercentage() {
        let percenId = this.revenuesetupData['f077ftaxCode'];
        console.log(percenId);
        this.ajaxCount++;
        this.TaxsetupcodeService.getItemsDetail(percenId).subscribe(
            data => {
                this.ajaxCount--;
                this.taxcodepercenList = data['result'];

                this.revenuesetupData['f077fgstPercentage'] = data['result'][0]['f081fpercentage'];
            }
        );
    }
    assignCode() {
        this.revenuesetupData['f077fcrFund'] = this.revenuesetupData['f077fdrFund'];
        this.revenuesetupData['f077fcrActivity'] = this.revenuesetupData['f077fdrActivity'];
        this.revenuesetupData['f077fcrDepartment'] = this.revenuesetupData['f077fdrDepartment'];
    }
    deleteRecurring(object) {

        var cnf = confirm("Do you really want to delete");
        if (cnf == true) {
            console.log(this.revenuesetupDataAdded);
            var index = this.revenuesetupDataAdded.indexOf(object);
            this.deleteList.push(this.revenuesetupDataAdded[index]['f077fidDetails']);
            let deleteObject = {};
            deleteObject['id'] = this.deleteList
            this.RecurringsetupcustomerService.deleteItems(deleteObject).subscribe(
                data => {
                }, error => {
                    console.log(error);
                })
            if (index > -1) {
                this.revenuesetupDataAdded.splice(index, 1);
            }
            this.showIcons();
            this.getAmount();

        }

    }
    ConvertToFloat(val) {
        return parseFloat(val);
    }
    getglcode() {
        let itemId = this.revenuesetupData['f077ffeeSetup'];
        console.log(itemId);
        this.ajaxCount++;
        this.RevenuesetupService.getItemsDetail(itemId).subscribe(
            data => {
                this.ajaxCount--;
                this.itemList = data['result'][0];
                this.revenuesetupData['f077fcrFund'] = data['result'][0]['f085ffund'];
                this.revenuesetupData['f077fcrActivity'] = data['result'][0]['f085factivity'];
                this.revenuesetupData['f077fcrDepartment'] = data['result'][0]['f085fdepartment'];
                this.revenuesetupData['f077fcrAccount'] = data['result'][0]['f085faccountId'];
            }
        );
    }
    getglcodeList() {
        for (var i = 0; i < this.revenuesetupDataAdded.length; i++) {
            let itemId = this.revenuesetupDataAdded[i]['f077ffeeSetup'];
            // this.ajaxCount++;
            this.RevenuesetupService.getItemsDetail(itemId).subscribe(
                data => {
                    // this.ajaxCount--;
                    this.itemList = data['result'][0];
                }
            );
            this.revenuesetupDataAdded[i]['f077fdrFund'] = this.itemList['f085ffund'];
            this.revenuesetupDataAdded[i]['f077fdrActivity'] = this.itemList['f085factivity'];
            this.revenuesetupDataAdded[i]['f077fdrDepartment'] = this.itemList['f085fdepartment'];
            this.revenuesetupDataAdded[i]['f077fdrAccount'] = this.itemList['f085faccountId'];
            console.log(this.revenuesetupDataAdded[i]);
            console.log(this.itemList);
            // this.itemList = [];
        }

    }
    getAmount() {

        let finaltotl = 0;
        for (var i = 0; i < this.revenuesetupDataAdded.length; i++) {
            finaltotl = finaltotl + this.ConvertToFloat(this.revenuesetupDataAdded[i]['f077famount']);
        }
        this.recurringsetupcustomerData['f077fpayment'] = finaltotl.toFixed(2);

    }

    addinvoicelist() {
        console.log(this.revenuesetupData);
        console.log(this.revenuesetupData);
        var returnresult = this.validation(this.revenuesetupData);
        console.log(returnresult);

        if (!returnresult) {
            return;
        }
        this.totalAmount = 0;
        this.revenuesetupDataAdded.push(this.revenuesetupData);
        for (var j = 0; j < this.revenuesetupDataAdded.length; j++) {
            this.totalAmount = this.totalAmount + parseFloat(this.revenuesetupDataAdded[j]['f077famount'])
        }
        this.recurringsetupcustomerData['f077fpayment'] = this.totalAmount;
        this.revenuesetupData = {};
        //this.revenuesetupData['']
    }
    getFields() {
        this.revenuesetupData['f077ffeeSetup'];

        for (var i = 0; i < this.revenuesetupList.length; i++) {
            if (this.revenuesetupList[i]['f085fid'] == this.revenuesetupData['f077ffeeSetup']) {
                this.revenuesetupData['f077fcrAccount'] = this.revenuesetupList[i]['masterAccountCode'];
                this.revenuesetupData['f077fgstCode'] = this.revenuesetupList[i]['f085fidTaxCode'];
                this.getpercentage();
            }
        }

    }

    formatNumber() {
        let number = this.revenuesetupData['f077famount'];
        number = parseFloat(number).toFixed(2)
        var withCommas = Number(number).toLocaleString('en');
        return withCommas;

    }

    validation(revenueData) {
        console.log(revenueData);
        if (revenueData['f077fcrFund'] != revenueData['f077fdrFund']) {
            alert("Debit GL and Credit GL Fund Cannot be different");
            return false;
        }
        // else if (revenueData['f077fcrActivity'] != revenueData['f077fdrActivity']) {
        //     alert("Debit GL and Credit GL Activity Code Cannot be different");
        //     return false;
        // }
        else if (revenueData['f077fcrDepartment'] != revenueData['f077fdrDepartment']) {
            alert("Debit GL and Credit GL Department Cannot be different");
            return false;
        }
        // else if (revenueData['f077fdrAccount'] == revenueData['f077fcrAccount']) {
        //     alert("Debit GL and Credit GL Account Cannot be Same");
        //     return false;
        // } 
        else {
            return true;
        }


    }

    addRecurringsetupcustomer() {
        // this.addinvoicelist();
        if (this.recurringsetupcustomerData['f077fdebitorType'] == undefined) {
            alert('Select Type of Debtor');
            return false;
        }
        // if (this.recurringsetupcustomerData['f077fpremise'] == undefined) {
        //     alert('Select Premise Setup');
        //     return false;
        // }
        // if (this.recurringsetupcustomerData['f077fcustomer'] == undefined) {
        //     alert('Select Debtor Name');
        //     return false;
        // }
        if (this.recurringsetupcustomerData['f077fcontractNumber'] == undefined) {
            alert('Enter Contract Reference');
            return false;
        }
        if (this.recurringsetupcustomerData['f077fperiod'] == undefined) {
            alert('Enter Period in Months');
            return false;
        }
        if (this.recurringsetupcustomerData['f077fdateInvoice'] == undefined) {
            alert('Select Invoice Generation Day');
            return false;
        }
        if (this.recurringsetupcustomerData['f077feffectiveDate'] == undefined) {
            alert('Select Effective Date');
            return false;
        }
        if (this.recurringsetupcustomerData['f077fendDate'] == undefined) {
            alert('Select Last Date');
            return false;
        }
        if (this.showLastRow == false) {
            var addactivities = this.addrecurring();
            if (addactivities == false) {
                return false;
            }

        }

        // if (this.revenuesetupData['f077fcrDepartment'] != this.revenuesetupData['f077fdrDepartment']) {
        //     alert("Debit GL and Credit GL Department Cannot be different");
        //     return false;
        // }
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        console.log(this.recurringsetupcustomerData);

        var utilityFromDate = new Date(this.recurringsetupcustomerData['f077feffectiveDate']);
        var fromSeconds = utilityFromDate.getTime() / 1000;

        var utilityToDate = new Date(this.recurringsetupcustomerData['f077enddate']);
        var toSeconds = utilityToDate.getTime() / 1000;

        if (toSeconds < fromSeconds) {
            alert('Effective Date cannot be greater than End Date');
            return;
        }



        //    if(!this.validation(this.recurringsetupcustomerData)) {
        //      return;
        //    }
        this.recurringsetupcustomerData['f077faddress'] = '0';
        //    this.recurringsetupcustomerData['f077fcontractNumber'] = '0';
        this.recurringsetupcustomerData['recurring-details'] = this.revenuesetupDataAdded;
        //    this.recurringsetupcustomerData['f042fcreatedBy']=1;
        for (var i = 0; i < this.revenuesetupDataAdded.length; i++) {
            if (!this.validation(this.revenuesetupDataAdded[i])) {
                return;
            }
        }
        this.recurringsetupcustomerData['f077fpayment'] = this.ConvertToFloat(this.recurringsetupcustomerData['f077fpayment']);
        for (var i = 0; i < this.revenuesetupDataAdded.length; i++) {
            this.revenuesetupDataAdded[i]['f077famount'] = this.ConvertToFloat(this.revenuesetupDataAdded[i]['f077famount']).toFixed(2);
        }
        this.recurringsetupcustomerData['f077fidPremiseCategory']=this.recurringsetupcustomerData['f077fidPremiseCategory'];
        this.recurringsetupcustomerData['f077fpaymentMethod']=this.recurringsetupcustomerData['f077fpaymentMethod'];

        console.log(this.revenuesetupData);
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.RecurringsetupcustomerService.updateRecurringsetupcustomerItems(this.recurringsetupcustomerData, this.id).subscribe(
                data => {
                    this.router.navigate(['accountsrecivable/recurringsetupcustomer']);
                    alert("Recurring Setup Customer has been Updated Successfully");
                }, error => {
                    console.log(error);
                });


        } else {
            this.RecurringsetupcustomerService.insertRecurringsetupcustomerItems(this.recurringsetupcustomerData).subscribe(
                data => {
                    this.router.navigate(['accountsrecivable/recurringsetupcustomer']);
                    alert("Recurring Setup Customer has been Added Successfully");

                }, error => {
                    console.log(error);
                });

        }

    }
    addrecurring() {

        if (this.revenuesetupData['f077ffeeSetup'] == undefined) {
            alert('Select Fee Item');
            return false;
        }
        if (this.revenuesetupData['f077fdrFund'] == undefined) {
            alert('Select Debit Fund');
            return false;
        }
        if (this.revenuesetupData['f077fdrActivity'] == undefined) {
            alert('Select Debit Activity Code');
            return false;
        }
        if (this.revenuesetupData['f077fdrDepartment'] == undefined) {
            alert('Select Debit Cost Center');
            return false;
        }
        if (this.revenuesetupData['f077fdrAccount'] == undefined) {
            alert('Select Debit Account Code');
            return false;
        }
        if (this.revenuesetupData['f077fcrFund'] == undefined) {
            alert('Enter Credit Fund');
            return false;
        }
        if (this.revenuesetupData['f077fcrActivity'] == undefined) {
            alert('Select Credit Activity Code');
            return false;
        }
        if (this.revenuesetupData['f077fcrDepartment'] == undefined) {
            alert('Select Credit Cost Center');
            return false;
        }
        if (this.revenuesetupData['f077fcrAccount'] == undefined) {
            alert('Select Credit Account Code');
            return false;
        }
        if (this.revenuesetupData['f077ftaxCode'] == undefined) {
            alert('Select Tax Code');
            return false;
        }
        if (this.revenuesetupData['f077famount'] == undefined) {
            alert('Enter Amount');
            return false;
        }
        if (this.revenuesetupData['f077fdrAccount'] == this.revenuesetupData['f077fcrAccount']) {
            alert("Debit GL and Credit GL Account Code Cannot be same");
            return false;
        }
        if (this.revenuesetupData['f077fdrFund'] != this.revenuesetupData['f077fcrFund']) {
            alert("Debit GL and Credit GL Fund Should be same");
            return false;
        }
        if (this.revenuesetupData['f077fdrActivity'] != this.revenuesetupData['f077fcrActivity']) {
            alert("Debit GL and Credit GL Activity Code Should be same");
            return false;
        }
        if (this.revenuesetupData['f077fdrDepartment'] != this.revenuesetupData['f077fcrDepartment']) {
            alert("Debit GL and Credit GL Department Should be same");
            return false;
        }
        var confirmPop = confirm("Do you want to Add?");
        if (confirmPop == false) {
            return false;
        }
        var dataofCurrentRow = this.revenuesetupData;
        this.revenuesetupData = {};
        this.revenuesetupDataAdded.push(dataofCurrentRow);
        this.showIcons();
        this.getAmount();
    }
    onlyNumberKey(event) {
        //console.log(event);
        if (event.charCode == 46) {

        } else {
            return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
        }
    }
    maximize() {
        $("#myModal").addClass("modal");
        $("#overlay1").show();
        $(".maximize-icon").addClass("hidden");
        $(".minimize-icon").removeClass("hidden");
        $(".modal-backdrop").show();

    }
    minimize() {
        $("#myModal").removeClass("modal");
        $("#overlay1").hide();
        $(".maximize-icon").removeClass("hidden");
        $(".minimize-icon").addClass("hidden");
        $(".modal-backdrop").hide();
        $('#myModal').show();

    }
}