import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { CustomerService } from '../../accountsrecivable/service/customer.service';
import { ActivatedRoute, Router } from '@angular/router';
import { VoucherService } from '../service/voucher.service';
import { ItemService } from '../service/item.service';
import { InvoiceService } from '../service/invoice.service';
import { GlcodegenerationService } from '../../generalledger/service/glcodegeneration.service';
import { UserService } from '../../generalsetup/service/user.service';
@Component({
    selector: 'Voucher',
    templateUrl: 'voucher.component.html'
})

export class VoucherComponent implements OnInit {
    voucherList = [];
    voucherData = {};
    customerList = [];
    invoiceList = [];
    invoiceData = {};
    customerData = {};
    glcodeList = [];
    glcodeData = {};
    itemList = [];
    userData = {};
    userList = [];
    title = '';
    editId: number;
    openingbalanceData = {};
    newOpeningBalanceList = [];
    saveDataObject = {};

    id: number;
    showCrReadonly:boolean;
    showDrReadonly:boolean;
    debit: number;
    credit: number;
    cash: number;
    payment: number;
    type:string;
    valueDate: string;

    constructor(

        private GlcodegenerationService: GlcodegenerationService,
        private VoucherService: VoucherService,
        private ItemService: ItemService,
        private InvoiceService: InvoiceService,
        private CustomerService: CustomerService,
        private UserService: UserService,
        private route: ActivatedRoute,
        private router: Router
    ) { }

    ngOnInit() {
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        this.route.paramMap.subscribe((data) => this.editId = + data.get('editId'));

        this.valueDate = new Date().toISOString().substr(0, 10);
                console.log(this.valueDate);
                this.voucherData['f031fdate'] = this.valueDate;

        console.log(this.id);
        if (this.id == 0) {
            this.saveDataObject['f031fvoucherType'] = 'CN';
        }
        if (this.id == 1) {
            this.saveDataObject['f031fvoucherType'] = 'DN';
        }
        if (this.id == 2) {
            this.saveDataObject['f031fvoucherType'] = 'CSH';
        }
        if (this.id == 3) {
            this.saveDataObject['f031fvoucherType'] = 'PMT';
        }
        console.log("?asdfsad");
        
        var string='';
        
        if (this.id == 0) {
            this.title = "Credit Note";
            string = 'CN';
        }
        if (this.id == 1) {
            this.title = "Debit Note";
            string = 'DN';
        } 
        if (this.id == 2) {
            this.title = "Cash Voucher";
            string = 'CSH';
        }
        if (this.id == 3) {
            this.title = "Payment Voucher";
            string = 'PMT';
        }
        let typeObject = {};
        typeObject['type'] = string;
        this.VoucherService.getInvoiceNumber(typeObject).subscribe(
            newData => {
                console.log(newData);
                this.voucherData['f031fvoucherNumberss'] = newData['number'];
            }
        );
        //invoice dropdown
        this.type = "invoice";
        this.InvoiceService.getItems(this.type).subscribe(
            data => {
                console.log(data);
                this.invoiceList = data['result'];
            }
        );
        //user dropdown
        this.UserService.getItems().subscribe(
            data => {
                this.userList = data['result']['data'];
            }
        );
        //item dropdown

        this.ItemService.getItems().subscribe(
            data => {
                this.itemList = data['result'];
                this.GlcodegenerationService.getItems().subscribe(
                    data => {
                        this.glcodeList = data['result']['data'];
                        this.CustomerService.getItems().subscribe(
                            data => {
                                this.customerList = data['result']['data'];
                                if (this.editId > 0) {
                                    this.VoucherService.getItemsDetail(this.editId).subscribe(
                                        data => {
                                            this.voucherData = data['result'];
                                            console.log(this.voucherData);
                                            this.newOpeningBalanceList = data['result'];
                                            this.voucherData['f031fidInvoice'] = data['result'][0]['f031fidInvoice'],
                                                this.voucherData['f031ftotalAmount'] = data['result'][0]['f031ftotalAmount'],
                                                this.voucherData['f031fcustomer'] = data['result'][0]['f031fidCustomer']
                                                this.voucherData['f031fbalance'] = data['result'][0]['f031fbalance'],
                                                this.voucherData['f031freferenceNumber'] = data['result'][0]['f031freferenceNumber'],
                                                this.voucherData['f031fdate'] = data['result'][0]['f031fdate'],
                                                this.voucherData['f031fapproverId'] = data['result'][0]['f031fapproverId']
                                                // this.voucherData['f031fvoucherDays'] = data['result'][0]['']
                                        }, error => {
                                            console.log(error);
                                        });
                                }
                            }
                        );
                    }
                );
            }
        );

    }
    getInvoiceDetails() {
        let invid=0;
        invid = this.voucherData['f031fidInvoice'];
        console.log(invid);
        let type='invoice';
        console.log(type);

        this.InvoiceService.getInvoiceDetailsById(invid,type).subscribe(
            data => {
                console.log(data);
                this.invoiceList = data['result'];
                this.voucherData['f031fcustomer'] = data['result'][0]['f071fidCustomer']
                this.voucherData['f031ftotalAmount'] = data['result'][0]['f071finvoiceTotal']
                this.voucherData['invoiceDate'] = data['result'][0]['f071finvoiceDate']


            }
        );
    }
    
    disableFieldsOfDRCR() {
        this.showCrReadonly = false;
        this.showDrReadonly = false;

        console.log(this.openingbalanceData);
        if(this.openingbalanceData['f032fdrAmount']==undefined ||
           this.openingbalanceData['f032fdrAmount']==0 ||
           this.openingbalanceData['f032fdrAmount']=='') {
               this.showDrReadonly = true;
           }
           if(this.openingbalanceData['f032fcrAmount']==undefined ||
           this.openingbalanceData['f032fcrAmount']==0 ||
           this.openingbalanceData['f032fcrAmount']=='') {
               this.showCrReadonly = true;
           }

        if(this.showCrReadonly==true && this.showDrReadonly==true) {
            this.showCrReadonly = false;
            this.showDrReadonly = false;    
        }

           console.log(this.showCrReadonly);
           console.log(this.showDrReadonly);

    }

    saveData() {
        console.log(this.newOpeningBalanceList);


        if (this.credit != this.debit) {
            alert("Sum of Credit should be equal to Debit");
            return;
        }
        //alert("data can be saved");
        console.log(this.voucherData);
        this.saveDataObject["f031fidInvoice"] = this.voucherData['f031fidInvoice'],
            this.saveDataObject["f031ftotalAmount"] = this.voucherData['f031ftotalAmount'],
            this.saveDataObject["f031fbalance"] = this.voucherData['f031fbalance'],
            this.saveDataObject["f031freferenceNumber"] = this.voucherData['f031freferenceNumber'],
            this.saveDataObject["f031fdate"] = this.voucherData['f031fdate'],
            this.saveDataObject["f031fapproverId"] = this.voucherData['f031fapproverId'],
            this.saveDataObject["f031fvoucherNumber"] = this.voucherData['f031fvoucherNumberss'],
            console.log(this.voucherData['f031fvoucherNumberss']);
            console.log("asdf");
            this.saveDataObject["f031fvoucherDays"] = this.voucherData['f031fvoucherDays'],
            this.saveDataObject["f031fstatus"] = 0,
            this.saveDataObject["voucher-details"] = this.newOpeningBalanceList,

            console.log(this.saveDataObject);

        this.VoucherService.insertvoucherItems(this.saveDataObject).subscribe(
            data => {
                this.voucherData = data['result'];

                if (this.id == 0) {
                    this.router.navigate(['/accountsrecivable/creditnotelist/0']);
                }
                if (this.id == 1) {
                    this.router.navigate(['/accountsrecivable/debitnotelist/1']);
                } 
                if (this.id == 2) {
                    this.router.navigate(['/accountsrecivable/cashvoucherlist/2']);
                }
                if (this.id == 3) {
                    this.router.navigate(['/accountsrecivable/paymentvoucherlist/3']);
                }
                console.log(this.voucherData);
            }, error => {
                console.log(error);
            });
        

    }
    ConvertToInt(val) {
        return parseInt(val);
    }
    // onBlurMethod() {
    //     console.log(this.newOpeningBalanceList);
    //     var finaltotal = 0;
    //     for(var i=0;i<this.newOpeningBalanceList.length;i++) {
    //         var totalamount = (this.ConvertToInt(this.newOpeningBalanceList[i]['f072fquantity']))*(this.ConvertToInt(this.invoiceList[i]['f072fprice']));
    //         //var gstamount = (this.ConvertToInt(this.newOpeningBalanceList[i]['f072fgst']))/100*(totalamount);
    //         this.newOpeningBalanceList[i]['f072ftotal'] =  totalamount;
    //         finaltotal = finaltotal + this.newOpeningBalanceList[i]['f072ftotal'];
    //     }

    //     this.voucherData['f031ftotalAmount'] = finaltotal;
    // }

    deleteVoucher(object) {
        var cnf = confirm("Do you really want to delete");
        if(cnf==true){
            console.log(this.newOpeningBalanceList);
            var index = this.newOpeningBalanceList.indexOf(object);;
            if (index > -1) {
                this.newOpeningBalanceList.splice(index, 1);
            }
           // this.onBlurMethod();
        }
    }
    
    addOpeningbalance() {

        this.debit = 0;
        this.credit = 0;
        console.log(this.openingbalanceData);
        if (this.openingbalanceData['f032fcrAmount'] == undefined) {
            this.openingbalanceData['f032fcrAmount'] = 0;
        }
        if (this.openingbalanceData['f032fdrAmount'] == undefined) {
            this.openingbalanceData['f032fdrAmount'] = 0;
        }
        var dataofCurrentRow = this.openingbalanceData;

        this.newOpeningBalanceList.push(dataofCurrentRow);
        console.log(this.newOpeningBalanceList);
        this.openingbalanceData = {};

        for (var i = 0; i < this.newOpeningBalanceList.length; i++) {
            this.debit = (this.ConvertToInt(this.debit) + this.ConvertToInt(this.newOpeningBalanceList[i]['f032fdrAmount']));
            this.credit = (this.ConvertToInt(this.credit) + this.ConvertToInt(this.newOpeningBalanceList[i]['f032fcrAmount']));
        }
        console.log(this.debit);
        console.log(this.credit);
        this.showCrReadonly = false;
        this.showDrReadonly = false;
    }
}