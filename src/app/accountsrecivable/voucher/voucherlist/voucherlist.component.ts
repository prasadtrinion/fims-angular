import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { VoucherService } from '../../service/voucher.service';


@Component({
    selector: 'VoucherlistComponent',
    templateUrl: 'voucherlist.component.html'
})

export class VoucherlistComponent implements OnInit {
    voucherlistList =  [];
    voucherlistData = {};
    id: number;
    title='';

    constructor(
        
        private VoucherService: VoucherService,
        private route: ActivatedRoute,
        private router: Router
    ) { }

    ngOnInit() { 
        let type = '';

        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        console.log(this.id);
        if(this.id==0){
             type = 'credit';
            this.title="Credit Note";

        }
        if(this.id==1){
            type='debit';
            this.title="Debit Note";
        }
        if(this.id==2){
            type='cash';
            this.title="Cash Voucher";
        }
        if(this.id==3){
            type='payment';
            this.title="Payment Voucher";
        }
        
        this.VoucherService.getItems(type).subscribe(
            data => {
                this.voucherlistList = data['result'];
        }, error => {
            console.log(error);
        });
    }

    addVoucherlist(){
           console.log(this.voucherlistData);
        this.VoucherService.insertvoucherItems(this.voucherlistData).subscribe(
            data => { 
                this.voucherlistList = data['todos'];
            }, error => {
                console.log(error);
            });

    }
}