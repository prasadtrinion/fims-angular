import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Injector } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MaininvoiceapprovalService } from '../service/maininvoiceapproval.service'
import { NgxSpinnerService } from 'ngx-spinner';


@Component({
    selector: 'Maininvoiceapproval',
    templateUrl: 'maininvoiceapproval.component.html'
})

export class MaininvoiceapprovalComponent implements OnInit {
    maininvoiceApprovalList = [];
    maininvoiceApprovalData = {};
    invoiceapprovalData = {};
    id: number;
    selectAllCheckbox: false;
    type: string;
    title = '';

    constructor(

        private MaininvoiceapprovalService: MaininvoiceapprovalService,
        private spinner: NgxSpinnerService,
        private route: ActivatedRoute,
        private router: Router

    ) { }
    ngOnInit() {
        this.getList();
    }
    getList() {
        this.spinner.show();
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        this.MaininvoiceapprovalService.getItems().subscribe(
            data => {
                this.spinner.hide();
                let activeData = [];
                activeData = data['result'];
                for (var i = 0; i < activeData.length; i++) {
                    if (activeData[i]['f071fstatus'] == 0) {
                        activeData[i]['f071fstatus'] = 'Pending';
                    } else if (activeData[i]['f071fstatus'] == 1) {
                        activeData[i]['f071fstatus'] = 'Approved';
                    } else {
                        activeData[i]['f071fstatus'] = 'Rejected';
                    }
                    if (activeData[i]['f071finvoiceType'] == 'STA') {
                        activeData[i]['f071finvoiceType'] = 'Staff';

                    } else if (activeData[i]['f071finvoiceType'] == 'STD') {
                        activeData[i]['f071finvoiceType'] = 'Student';
                    }
                    else {
                        if (activeData[i]['f071finvoiceType'] == 'OR') {
                            activeData[i]['f071finvoiceType'] = 'Other Receivables';
                        }
                    }
                }
                this.maininvoiceApprovalList = activeData;
            }, error => {
                console.log(error);
            });
    }
    invoiceApprovalListData() {
        let approveDarrayList = [];
        console.log(this.maininvoiceApprovalList);
        for (var i = 0; i < this.maininvoiceApprovalList.length; i++) {
            if (this.maininvoiceApprovalList[i].f071fstatus == true) {
                approveDarrayList.push(this.maininvoiceApprovalList[i].f071fid);
            }
        }
        if (approveDarrayList.length < 1) {
            alert("Select atleast one Invoice  to Approve");
            return false;
        }
        var confirmPop = confirm("Do you want to Approve?");
        if (confirmPop == false) {
            return false;
        }
        var approvalObject = {};
        approvalObject['id'] = approveDarrayList;
        approvalObject['status'] = '1';
        approvalObject['reason'] = '';
        this.MaininvoiceapprovalService.updateInvoiceApproval(approvalObject).subscribe(
            data => {
                this.getList();
                this.invoiceapprovalData['reason'] = '';
                alert("Approved Successfully");
            }, error => {
                console.log(error);
            });

    }
    regectJournalApprovalListData() {
        let approveDarrayList = [];
        console.log(this.maininvoiceApprovalList);
        for (var i = 0; i < this.maininvoiceApprovalList.length; i++) {
            if (this.maininvoiceApprovalList[i].f071fstatus == true) {
                approveDarrayList.push(this.maininvoiceApprovalList[i].f071fid);
            }
        }
        if (approveDarrayList.length < 1) {
            alert("Select atleast one Invoice to Reject");
            return false;
        }

        if (this.invoiceapprovalData['reason'] == undefined) {
            alert('Enter the Description');
            return false;
        }
        var confirmPop = confirm("Do you want to Reject?");
        if (confirmPop == false) {
            return false;
        }
        var budgetActivityDataObject = {}
        budgetActivityDataObject['id'] = approveDarrayList;
        budgetActivityDataObject['status'] = '2';
        budgetActivityDataObject['reason'] = this.invoiceapprovalData['reason'];


        this.MaininvoiceapprovalService.updateInvoiceApproval(budgetActivityDataObject).subscribe(
            data => {
                this.getList();
                this.invoiceapprovalData['reason'] = '';
                alert("Rejected Successfully");
            }, error => {
                console.log(error);
            });
    }
    selectAll() {
        console.log(this.selectAllCheckbox);
        for (var i = 0; i < this.maininvoiceApprovalList.length; i++) {
            this.maininvoiceApprovalList[i].f071fstatus = this.selectAllCheckbox;
        }
    }

}