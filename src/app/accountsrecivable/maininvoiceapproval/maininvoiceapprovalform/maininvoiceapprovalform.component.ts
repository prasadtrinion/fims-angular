import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { VoucherService } from '../../service/voucher.service';
import { MaininvoiceapprovalService } from '../../service/maininvoiceapproval.service';
import { CustomerService } from '../../../accountsrecivable/service/customer.service';

import { GlcodegenerationService } from '../../../generalledger/service/glcodegeneration.service';
import { TaxsetupcodeService } from '../../../generalledger/service/taxsetupcode.service'
import { ItemService } from '../../service/item.service';



@Component({
    selector: 'MaininvoiceapprovalformComponent',
    templateUrl: 'maininvoiceapprovalform.component.html'
})

export class MaininvoiceapprovalformComponent implements OnInit {
    invoiceList = [];
    invoiceData = {};
    invoiceDataheader = {};
    glcodeList = [];
    itemList = [];
    customerList = [];
    taxcodeList = [];
    id: number;
    valueDate: string;
    ajaxCount: number;
    gstValue: number;
    Amount: number;
    taxAmount: number;
    totalAmount: number;

    constructor(
        private GlcodegenerationService: GlcodegenerationService,
        private MaininvoiceService: MaininvoiceapprovalService,
        private VoucherService: VoucherService,
        private TaxsetupcodeService: TaxsetupcodeService,
        private CustomerService: CustomerService,
        private ItemService: ItemService,
        private route: ActivatedRoute,
        private router: Router
    ) { }


    ngOnInit() {
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        console.log(this.id);

        this.getInvoiceNumber();

        this.ajaxCount = 0;
        //item dropdown
        this.ajaxCount++;
        this.ItemService.getItems().subscribe(
            data => {
                this.itemList = data['result'];
                this.CustomerService.getItems().subscribe(
                    data => {
                        this.customerList = data['result']['data'];
                        this.GlcodegenerationService.getItems().subscribe(
                            data => {
                                this.glcodeList = data['result']['data'];
                                this.TaxsetupcodeService.getItems().subscribe(
                                    data => {
                                        this.taxcodeList = data['result']['data'];
                                        if (this.id > 0) {
                                            this.MaininvoiceService.getInvoiceDetailsById(this.id).subscribe(
                                                data => {
                                                    this.invoiceList = data['result'];
                                                    this.invoiceDataheader['f071fapprovedBy'] = data['result'][0].f071fapprovedBy;
                                                    this.invoiceDataheader['f071finvoiceDate'] = data['result'][0].f071finvoiceDate;
                                                    this.invoiceDataheader['f071finvoiceNumber'] = data['result'][0].f071finvoiceNumber;
                                                    this.invoiceDataheader['f071finvoiceTotal'] = data['result'][0].f071finvoiceTotal;
                                                    this.invoiceDataheader['f071finvoiceType'] = data['result'][0].f071finvoiceType;
                                                    this.invoiceDataheader['f071fidCustomer'] = data['result'][0].f071fidCustomer;
                                                    this.invoiceDataheader['f071fid'] = data['result'][0].f071fid;

                                                    console.log(this.invoiceList);
                                                }, error => {
                                                    console.log(error);
                                                });
                                        }
                                    }
                                );
                            }
                        );
                    }
                );

            }
        );
    }

    getInvoiceNumber() {
        let typeObject = {};
        typeObject['type'] = "IV";
        this.VoucherService.getInvoiceNumber(typeObject).subscribe(
            data => {
                console.log(data);
                this.invoiceDataheader['f071finvoiceNumber'] = data['number'];
                this.valueDate = new Date().toISOString().substr(0, 10);
                console.log(this.valueDate);
                this.invoiceDataheader['f071finvoiceDate'] = this.valueDate;
            }
        );
    }

    ConvertToFloat(val) {
        return parseFloat(val);
    }

    saveInvoice() {

        let invoiceObject = {};
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        console.log(this.id);
        if (this.id > 0) {
            invoiceObject['f071fid'] = this.id;
        }
        invoiceObject['f071finvoiceNumber'] = this.invoiceDataheader['f071finvoiceNumber'];
        invoiceObject['f071finvoiceType'] = this.invoiceDataheader['f071finvoiceType'];
        invoiceObject['f071finvoiceDate'] = this.invoiceDataheader['f071finvoiceDate'];
        invoiceObject['f071finvoiceTotal'] = this.invoiceDataheader['f071finvoiceTotal'];
        invoiceObject['f071fidCustomer'] = this.invoiceDataheader['f071fidCustomer'];
        invoiceObject['f071finvoiceFrom'] = 2;
        invoiceObject['f071fstatus'] = 0;
        invoiceObject['f071fisRcp'] = 1;
        invoiceObject['invoice-details'] = this.invoiceList;
    }

    getGstvalue() {
        let taxId = this.invoiceData['f072fgstCode'];
        let quantity = this.invoiceData['f072fquantity'];
        let price = this.invoiceData['f072fprice'];
        //console.log(this.taxcodeList);
        var taxSelectedObject = {};
        for (var k = 0; k < this.taxcodeList.length; k++) {
            if (this.taxcodeList[k]['f081fid'] == taxId) {
                taxSelectedObject = this.taxcodeList[k];
            }
        }
        this.gstValue = taxSelectedObject['f081fpercentage'];
        this.Amount = (this.ConvertToFloat(price)) * (this.ConvertToFloat(quantity));
        this.taxAmount = (this.ConvertToFloat(this.gstValue)) / 100 * (this.Amount)
        this.invoiceData['f072fgstValue'] = this.taxAmount;
        this.invoiceData['f072ftotal'] = (this.taxAmount) + (this.Amount);

    }
    onBlurMethod() {
        console.log(this.invoiceList);
        var finaltotal = 0;
        for (var i = 0; i < this.invoiceList.length; i++) {
            var totalamount = (this.ConvertToFloat(this.invoiceList[i]['f072fquantity'])) * (this.ConvertToFloat(this.invoiceList[i]['f072fprice']));
            var gstamount = (this.ConvertToFloat(this.gstValue) / 100 * (totalamount));
            this.invoiceList[i]['f072ftotal'] = gstamount + totalamount;
            console.log(gstamount);
            console.log(totalamount)
            finaltotal = finaltotal + this.invoiceList[i]['f072ftotal'];
        }

        this.invoiceDataheader['f071finvoiceTotal'] = finaltotal;
    }
    deleteInvoice(object) {
        var cnf = confirm("Do you really want to delete");
        if (cnf == true) {
            console.log(this.invoiceList);
            var index = this.invoiceList.indexOf(object);;
            if (index > -1) {
                this.invoiceList.splice(index, 1);
            }
            this.onBlurMethod();
        }
    }
    addinvoicelist() {
        console.log(this.invoiceData);

        var dataofCurrentRow = this.invoiceData;
        this.invoiceData = {};
        this.invoiceList.push(dataofCurrentRow);
        this.onBlurMethod();
    }
}