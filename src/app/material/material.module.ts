import { NgModule } from '@angular/core';
import { 
  MatButtonModule,
  MatCheckboxModule,
  MatToolbarModule,
  MatInputModule,
  MatFormFieldModule,
  MatCardModule
} from '@angular/material';

const modules = [
  MatButtonModule,
  MatToolbarModule,  
  MatInputModule,
  MatFormFieldModule,
  MatCheckboxModule,
  MatCardModule
]

@NgModule({
  imports: [ modules ],
  exports: [ modules ]
})
export class MaterialModule { }