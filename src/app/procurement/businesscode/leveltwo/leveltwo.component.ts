import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BusinesscodeService } from '../../service/businesscode.service'

import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
    selector: 'LeveltwoComponent',
    templateUrl: 'leveltwo.component.html'
})

export class LeveltwoComponent implements OnInit {
    leveltwoList = [];
    leveltwoData = {};
    id: number;
    levelone:string;
    constructor(
        private BusinesscodeService: BusinesscodeService,
        private route: ActivatedRoute,
        private router: Router
    ) { }

    ngOnInit() {
        this.getList();
    }
    onBlurMethod(object) {
        console.log(object);
        sessionStorage.removeItem("natureleveltwoparent");
        sessionStorage.removeItem("natureleveltwocode");
        sessionStorage.setItem("natureleveltwoparent", object['f059fid']);
        sessionStorage.setItem("natureleveltwocode",object['f059fcode']);

    }

    getList() {
        this.levelone = sessionStorage.getItem("naturelevelonecode");
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        console.log(this.id);
        this.leveltwoData['f059frefCode'] = sessionStorage.getItem("natureleveloneparent");
        this.leveltwoData['f059fparentCode'] = sessionStorage.getItem("natureleveloneparent");
        this.leveltwoData['f059flevelStatus'] = 1;
        let parentId =sessionStorage.getItem("natureleveloneparent");
        let levelId = 1;
        this.leveltwoData['f059fstatus'] = 1;

        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        console.log(this.id);
       
            this.BusinesscodeService.getItemsDetail(parentId,levelId).subscribe(
                data => {
                    this.leveltwoList = data['result']['data'];
                }, error => {
                    console.log(error);
                });
    
        
        console.log(this.id);
    }
    saveLeveltwo() {
        let leveltwoObject = {};
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            leveltwoObject['f059fid'] = this.id;
        }
        leveltwoObject['data'] = this.leveltwoList;
        this.BusinesscodeService.insertBusinesscodeItems(leveltwoObject).subscribe(
            data => {
                this.getList();
                alert("Data has been updated");

            }, error => {
                console.log(error);
            });
    }

    deleteleveltwo(object)
     {
        var cnf = confirm("Do you really want to delete");
        if(cnf==true){
            console.log(this.leveltwoList);
            var index = this.leveltwoList.indexOf(object);;
            if (index > -1) {
                this.leveltwoList.splice(index, 1);
            }
        }
    }
    addLeveltwo()
    {
        console.log(this.leveltwoData);
        this.leveltwoData['f059fupdatedBy'] = 1;
        this.leveltwoData['f059fcreatedBy'] = 1;
        this.leveltwoData['f059fstatus'] = 1;
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.leveltwoData['f059frefCode'] = sessionStorage.getItem("natureleveloneparent");
        this.leveltwoData['f059fparentCode'] = sessionStorage.getItem("natureleveloneparent");
        this.leveltwoData['f059flevelStatus'] = 1;

        var dataofCurrentRow = this.leveltwoData;
        this.leveltwoList.push(dataofCurrentRow);
        console.log(this.leveltwoData);
        this.leveltwoData = {};
    }
}
