import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BusinesscodeService } from '../../service/businesscode.service'
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
    selector: 'LeveloneComponent',
    templateUrl: 'levelone.component.html'
})

export class LeveloneComponent implements OnInit {
    leveloneList = [];
    leveloneData = {};
    leveloneDataheader = {};
    categoryList = [];
    id: number;
    constructor(
        private BusinesscodeService: BusinesscodeService,
        private route: ActivatedRoute,
        private router: Router
    ) { }

    ngOnInit() {
        this.getList();
    }

    saveLevelone() {

        let leveloneObject = {};
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            leveloneObject['f059fid'] = this.id;
        }

        leveloneObject['data'] = this.leveloneList;

        for(var i=0;i<this.leveloneList.length;i++) {
            if(this.leveloneList[i]['f059fcode'].length!=2) {
                alert("Please add 2 digit Group");
                return false;
            }
        }
        this.BusinesscodeService.insertBusinesscodeItems(leveloneObject).subscribe(
            data => {

                this.getList();
                alert("Data has been updated");

            }, error => {
                console.log(error);
            });
    }

    getList() {
        this.leveloneData['f059fstatus'] = 1;
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        console.log(this.id);
        let parentId =0;
        let levelId = 0;

        this.BusinesscodeService.getItemsDetail(parentId,levelId).subscribe(
            data => {
                this.leveloneList = data['result']['data'];
            }, error => {
                console.log(error);
            });
        console.log(this.id);

    }

    onBlurMethod(object) {
        console.log(object);
        sessionStorage.removeItem("natureleveloneparent");
        sessionStorage.removeItem("naturelevelonecode");
        sessionStorage.removeItem("natureparentCode");
        sessionStorage.setItem("natureparentCode", object['f059fcode']);
        sessionStorage.setItem("natureleveloneparent", object['f059fid']);
        sessionStorage.setItem("naturelevelonecode",object['f059fcode']);
    }
    deletelevelone(object) {
        var cnf = confirm("Do you really want to delete");
        if (cnf == true) {
            console.log(this.leveloneList);
            var index = this.leveloneList.indexOf(object);;
            if (index > -1) {
                this.leveloneList.splice(index, 1);
            }
        }
    }
    addLevelone() {

        this.leveloneData['f059fupdatedBy'] = 1;
        this.leveloneData['f059fcreatedBy'] = 1;
        this.leveloneData['f059fparentCode'] = 0;
        this.leveloneData['f059frefCode'] = 0;
        this.leveloneData['f059fstatus'] = 0;
        this.leveloneData['f059flevelStatus'] = 0;
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        var dataofCurrentRow = this.leveloneData;
        this.leveloneList.push(dataofCurrentRow);
        console.log(this.leveloneData);
        this.leveloneData = {};

    }

}


