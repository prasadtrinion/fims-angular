import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BusinesscodeService } from '../../service/businesscode.service'

import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
    selector: 'LevelthreeComponent',
    templateUrl: 'levelthree.component.html'
})

export class LevelthreeComponent implements OnInit {
    levelthreeList = [];
    levelthreeData = {};
    id: number;
    levelone:string;
    leveltwo:string;
    constructor(
        private BusinesscodeService: BusinesscodeService,
        private route: ActivatedRoute,
        private router: Router
    ) { }

    ngOnInit() {
       
    this.getList();
    }

    getList(){

        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        console.log(this.id);

        console.log(this.id);
        this.levelthreeData['f059frefCode'] = sessionStorage.getItem("natureleveltwoparent");
        this.levelthreeData['f059fparentCode'] = sessionStorage.getItem("natureleveltwoparent");
        this.levelthreeData['f059flevelStatus'] = 2;
        let parentId =sessionStorage.getItem("natureleveltwoparent");

        this.levelone = sessionStorage.getItem("naturelevelonecode");
        this.leveltwo = sessionStorage.getItem("natureleveltwocode");
        this.levelthreeData['f059fstatus'] = 1;

        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        console.log(this.id);
        let levelId = 2;
       
            this.BusinesscodeService.getItemsDetail(parentId, levelId).subscribe(
                data => {
                    this.levelthreeList = data['result']['data'];
                }, error => {
                    console.log(error);
                });
        


        console.log(this.id);
    }
    saveLevelthree() {
        let levelthreeObject = {};
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            levelthreeObject['f059fid'] = this.id;
        }

        levelthreeObject['data'] = this.levelthreeList;
        for(var i=0;i<this.levelthreeList.length;i++) {
            if(this.levelthreeList[i]['f059fcode'].length!=3) {
                alert("Please add 3 digit Sub Group Category");
                return false;
            }
        }
        this.BusinesscodeService.insertBusinesscodeItems(levelthreeObject).subscribe(
            data => {
                this.getList();
                alert("Data has been updated");

            }, error => {
                console.log(error);
            });
    }

    deletelevelthree(object) {
        var cnf = confirm("Do you really want to delete");
        if (cnf == true) {
            console.log(this.levelthreeList);
            var index = this.levelthreeList.indexOf(object);;
            if (index > -1) {
                this.levelthreeList.splice(index, 1);
            }
        }
    }
    addLevelthree() {

        this.levelthreeData['f059fupdatedBy'] = 1;
        this.levelthreeData['f059fcreatedBy'] = 1;
        this.levelthreeData['f059fstatus'] = 2;
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.levelthreeData['f059frefCode'] = sessionStorage.getItem("natureleveltwoparent");
        this.levelthreeData['f059fparentCode'] = sessionStorage.getItem("natureleveltwoparent");
        this.levelthreeData['f059flevelStatus'] = 2;

        var dataofCurrentRow = this.levelthreeData;
        this.levelthreeList.push(dataofCurrentRow);
        console.log(this.levelthreeData);
        this.levelthreeData = {};

    }

}
