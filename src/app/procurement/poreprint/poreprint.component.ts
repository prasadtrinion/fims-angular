import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { PoReportapprovalService } from '../service/purchaseorderprint.service'
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { environment } from '../../../environments/environment';

@Component({
    selector: 'PurchaseOrderRePrint',
    templateUrl: 'poreprint.component.html'
})

export class PurchaseOrderRePrintComponent implements OnInit {

    poapprovalList = [];
    poapprovalData = {};
    approvalData = {};
    selectAllCheckbox = false;
    downloadUrl: string = environment.api.downloadbase;
    id: number;
    constructor(

        private PoReportapprovalService: PoReportapprovalService,
        private route: ActivatedRoute,
        private router: Router,
        private spinner: NgxSpinnerService
    ) { }

    ngOnInit() {
        this.getListData();
    }
    getListData() {
        this.spinner.show();
        this.PoReportapprovalService.getprintItems().subscribe(
            data => {
                this.spinner.hide();
                this.poapprovalList = data['result']['data'];
                // this.select();
        }, error => {
            console.log(error);
        });
    }
    addPrint(id) {
        this.route.paramMap.subscribe((data) => this.id);
        let newobj = {};
        newobj['id'] = id;
        newobj['type']=2;
        var confirmPop = confirm("Do you want to Re-Print?");
        if (confirmPop == false) {
            return false;
        } this.PoReportapprovalService.insertPoPrintItems(newobj).subscribe(
            data => {
                // alert('KWAP has been saved');
                window.open(this.downloadUrl + data['name']);
                this.router.navigate(['procurement/poreprint']);

            }, error => {
                console.log(error);
            });
    }

}
