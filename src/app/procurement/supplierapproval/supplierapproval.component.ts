import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SupplierapprovalService } from '../service/supplierapproval.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'SupplierapprovalComponent',
    templateUrl: 'supplierapproval.component.html'
})
export class SupplierapprovalComponent implements OnInit {
    supplierList = [];
    supplierData = {};
    approvalData = {};
    id: number;
    //selectAllCheckbox = true;
    constructor(
        private SupplierapprovalService: SupplierapprovalService,
        private spinner: NgxSpinnerService
    ) { }

    ngOnInit() {

        this.getList();
    }

    getList() {
        this.spinner.show();        
        this.SupplierapprovalService.getItems().subscribe(
            data => {
                this.spinner.hide();
                this.supplierList = data['result'];
            }, error => {
                console.log(error);
                this.spinner.hide();

            });
    }

    ApprovalListData() {
        let approveDarrayList = [];
        console.log(this.supplierList);
        for (var i = 0; i < this.supplierList.length; i++) {
            if (this.supplierList[i].f030fapprovalStatus == true) {
                approveDarrayList.push(this.supplierList[i].f030fid);
            }
        }
        if (approveDarrayList.length < 1) {
            alert("select atleast one Vendor to approve");
            return false;
        }
        var confirmPop = confirm("Do you want to Approve?");
        if (confirmPop == false) {
            return false;
        }
        var approvalObject = {};
        approvalObject['id'] = approveDarrayList;

        console.log(approvalObject);
        var budgetActivityDataObject = {}
        budgetActivityDataObject['id'] = approveDarrayList;
        budgetActivityDataObject['status'] = 1;
        budgetActivityDataObject['reason'] = '';
        this.SupplierapprovalService.updateSupplierApproval(budgetActivityDataObject).subscribe(
            data => {
                this.getList();
                this.approvalData['reason'] = '';
                alert("Approved Sucessfully !!");
            }, error => {
                console.log(error);
            });
    }

    regectApprovalListData() {
        let approveDarrayList = [];
        console.log(this.supplierList);
        for (var i = 0; i < this.supplierList.length; i++) {
            if (this.supplierList[i].f030fapprovalStatus == true) {
                approveDarrayList.push(this.supplierList[i].f030fid);
            }
        }
        if (approveDarrayList.length < 1) {
            alert("Select atleast one Vendor to Reject");
            return false;
        }
        if (this.approvalData['reason'] == undefined) {
            alert('Enter the Description');
            return false;
        }
        var confirmPop = confirm("Do you want to Reject?");
        if (confirmPop == false) {
            return false;
        }
        var budgetActivityDataObject = {}
        budgetActivityDataObject['id'] = approveDarrayList;
        budgetActivityDataObject['status'] = 2;
        budgetActivityDataObject['reason'] = this.approvalData['reason'];

        this.SupplierapprovalService.updateSupplierApproval(budgetActivityDataObject).subscribe(
            data => {
                this.getList();
                this.approvalData['reason'] = '';
                alert("Rejected Successfully");
            }, error => {
                console.log(error);
            });
    }
}