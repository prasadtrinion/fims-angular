import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { PoapprovalService } from '../service/poapproval.service'
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'Poapproval',
    templateUrl: 'poapproval.component.html'
})

export class PoapprovalComponent implements OnInit {

    poapprovalList = [];
    poapprovalData = {};
    approvalData = {};
    selectAllCheckbox = false;

    constructor(
        private PoapprovalService: PoapprovalService,
        private spinner: NgxSpinnerService
    ) { }

    ngOnInit() {
        this.getListData();
    }
    getListData() {
        this.spinner.show();
        this.PoapprovalService.getItems().subscribe(
            data => {
                this.spinner.hide();
                this.poapprovalList = data['result'];
                this.select();
            }, error => {
                console.log(error);
            });
    }

    PrapprovalListData() {
        console.log(this.poapprovalList);
        var approvaloneIds = [];
        for (var i = 0; i < this.poapprovalList.length; i++) {
            if (this.poapprovalList[i].f034fapprovalStatus == true) {
                approvaloneIds.push(this.poapprovalList[i].f034fid);
            }
        }
        if (approvaloneIds.length < 1) {
            alert("Please select atleast one cost center to Reject");
            return false;
        }
        var confirmPop = confirm("Do you want to Approve?");
        if (confirmPop == false) {
            return false;
        }
        var approvaloneUpdate = {};
        approvaloneUpdate['id'] = approvaloneIds;

        console.log(approvaloneIds);
        this.PoapprovalService.updatePoapprovalItems(approvaloneUpdate).subscribe(
            data => {

                this.getListData();
                alert(" Approved Sucessfully ! ");

            }, error => {
                console.log(error);
            });
    }
    regectApprovalListData() {
        console.log(this.poapprovalList);
        var approvaloneIds = [];
        for (var i = 0; i < this.poapprovalList.length; i++) {
            if (this.poapprovalList[i].f034fapprovalStatus == true) {
                approvaloneIds.push(this.poapprovalList[i].f034fid);
            }
        }
        if (approvaloneIds.length < 1) {
            alert("Please select atleast one cost center to Reject");
            return false;
        }
        if (this.approvalData['reason'] == undefined) {
            alert('Please Enter the Description');
            return false;
        }
        var confirmPop = confirm("Do you want to Reject?");
        if (confirmPop == false) {
            return false;
        }
        var budgetActivityDataObject = {}
        budgetActivityDataObject['id'] = approvaloneIds;
        budgetActivityDataObject['status'] = 2;
        budgetActivityDataObject['reason'] = this.approvalData['reason'];


        this.PoapprovalService.updatePoapprovalItems(budgetActivityDataObject).subscribe(
            data => {
                this.getListData();
                this.approvalData['reason'] = '';
                alert(" Rejected Sucessfully ! ");
            }, error => {
                console.log(error);
            });
    }
    select() {
        console.log(this.selectAllCheckbox);
        for (var i = 0; i < this.poapprovalList.length; i++) {
            this.poapprovalList[i].f034fapprovalStatus = this.selectAllCheckbox;
        }
        console.log(this.poapprovalList);
    }

}
