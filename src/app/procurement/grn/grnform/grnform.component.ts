import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { GrnService } from '../../service/grn.service';
import { TaxsetupcodeService } from '../../../generalledger/service/taxsetupcode.service'
import { ItemsetupService } from '../../../procurement/service/itemsetup.service';
import { DepartmentService } from '../../../generalsetup/service/department.service';
import { FinancialyearService } from '../../../generalledger/service/financialyear.service';
import { SupplierregistrationService } from '../../service/supplierregistration.service';
import { PurchaseorderService } from '../../service/purchaseorder.service'
import { FundService } from '../../../generalsetup/service/fund.service';
import { AccountcodeService } from '../../../generalsetup/service/accountcode.service';
import { ActivitycodeService } from '../../../generalsetup/service/activitycode.service';
import { PoapprovalService } from '../../service/poapproval.service';
import { PurchaserequistionentryService } from '../../service/purchaserequistionentry.service';
import { GrnapprovalService } from '../../service/grnapproval.service';
import { BudgetactivitiesService } from "../../../budget/service/budgetactivities.service";
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'GrnformComponent',
    templateUrl: 'grnform.component.html'
})

export class GrnformComponent implements OnInit {
    grnList = [];
    grnData = {};
    grnDataheader = {};
    itemList = [];
    taxcodeList = [];
    departmentList = [];
    costCenterFund = [];
    costCenterAccount = [];
    costCenterActivity = [];
    financialList = [];
    budgetcostcenterList = [];
    DeptList = [];
    accountcodeList = [];
    fundList = [];
    activitycodeList = [];
    poApprovedList = [];
    itemUnitList = [];
    supplierList = [];
    purchaseMethods = [];
    purchaseOrderList = [];
    orderList = [];
    purchaseOrderLists = [];
    poListBasedOnOrderTypeList = [];
    grnapprovalList = [];
    ratingList = [];
    approvalData = {};
    grnapprovalData = {};
    deleteList = [];
    costCenterList = [];
    typeItemList = [];
    orderTypeId = [];
    PoIdList = []
    id: number;
    idview: number;
    valueDate: string;
    ajaxCount: number;
    gstValue: number;
    Amount: number;
    taxAmount: number;
    totalAmount: number;
    type: string;
    showLastRow: boolean;
    showLastRowMinus: boolean;
    showLastRowPlus: boolean;
    listshowLastRowMinus: boolean;
    listshowLastRowPlus: boolean;
    viewDisabled: boolean;
    editDisabled: boolean;
    saveBtnDisable: boolean;
    constructor(

        private PurchaseorderService: PurchaseorderService,
        private PoapprovalService: PoapprovalService,
        private GrnService: GrnService,
        private TaxsetupcodeService: TaxsetupcodeService,
        private ItemService: ItemsetupService,
        private ActivitycodeService: ActivitycodeService,
        private AccountcodeService: AccountcodeService,
        private FundService: FundService,
        private DepartmentService: DepartmentService,
        private FinancialyearService: FinancialyearService,
        private SupplierregistrationService: SupplierregistrationService,
        private route: ActivatedRoute,
        private PurchaserequistionentryService: PurchaserequistionentryService,
        private BudgetactivitiesService: BudgetactivitiesService,
        private spinner: NgxSpinnerService,
        private router: Router,
        private GrnapprovalService: GrnapprovalService

    ) { }


    ngDoCheck() {
        this.showIcons();
        const change = this.ajaxCount;
        // console.log(this.ajaxCount);
        if (this.ajaxCount == 0) {
            this.editFunction();
            this.spinner.hide();
            this.ajaxCount = 10;
        }
        if (this.ajaxCount == 0 && this.grnList.length > 0) {
            this.ajaxCount = 20;
            console.log("asdfsdf");
            // this.showIcons();
            //console.log(this.newbudgetactivitiesList[0]);
            if (this.grnList[0]['f034fapprovalStatus'] == 1 || this.grnList[0]['f034fapprovalStatus'] == 2) {
                this.saveBtnDisable = true;
                this.viewDisabled = true;
                this.listshowLastRowPlus = false;

                $("#target input,select").prop("disabled", true);
                $("#target1 input,select").prop("disabled", true);
            }
        }
        if (this.ajaxCount == 0 && this.grnList.length > 0) {
            this.ajaxCount = 20;
            if (this.idview > 1) {
                this.saveBtnDisable = true;
                this.viewDisabled = true;
                this.listshowLastRowPlus = false;

                $("#target input,select").prop("disabled", true);
                $("#target1 input,select").prop("disabled", true);
            }
        }
    }

    editFunction() {
        this.ajaxCount++;
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.route.paramMap.subscribe((data) => this.idview = + data.get('idview'));
        if (this.id > 0) {
            this.GrnService.getItemsDetail(this.id).subscribe(
                data => {
                    this.ajaxCount--;
                    this.PurchaseorderService.getItems().subscribe(
                        data => {
                            this.ajaxCount--;
                            this.purchaseOrderList = data['result']['data'];
                        }, error => {
                            console.log(error);
                        });
                    this.grnList = data['result'];
                    this.grnDataheader['f034forderType'] = data['result'][0]['f034forderType'];
                    this.grnDataheader['f034forderDate'] = data['result'][0]['f034forderDate'];
                    this.grnDataheader['f034fidFinancialyear'] = data['result'][0]['f034fidFinancialyear'];
                    this.grnDataheader['f034fidSupplier'] = data['result'][0]['f034fidSupplier'];
                    this.grnDataheader['f034fdescription'] = data['result'][0]['f034fdescription'];
                    this.grnDataheader['f034freferenceNumber'] = data['result'][0]['f034freferenceNumber'];
                    this.grnDataheader['f034fidDepartment'] = data['result'][0]['f034fidDepartment'];
                    this.grnDataheader['f034fidPurchaseOrder'] = data['result'][0]['f034fidPurchaseOrder'];
                    this.grnDataheader['f034fexpiredDate'] = data['result'][0]['f034fexpiredDate'];
                    this.grnDataheader['f034ftotalAmount'] = data['result'][0]['f034ftotalAmount'];
                    this.grnDataheader['f034frating'] = data['result'][0]['f034frating'];

                    this.grnDataheader['f034fid'] = data['result'][0]['f034fid'];
                    if (data['result'][0]['f034fapprovalStatus'] == 1 || this.grnList[0]['f034fapprovalStatus'] == 2) {
                        this.viewDisabled = true;

                        $("#target input,select").prop("disabled", true);
                        $("#target1 input,select").prop("disabled", true);
                    }
                    if (this.idview == 2) {
                        this.viewDisabled = true;

                        $("#target input,select").prop("disabled", true);
                        $("#target1 input,select").prop("disabled", true);
                    }
                    this.editDisabled = true;
                    this.getAllpurchaseGlList();
                     this.getDepartment();
                    this.onBlurMethod();
                    this.getPurchaseId();
                    this.showIcons();
                }, error => {
                    console.log(error);
                });
        }
    }
    showIcons() {
        if (this.grnList.length > 0) {
            this.showLastRowPlus = true;
            this.showLastRowMinus = true;
            this.listshowLastRowMinus = true;
        }

        if (this.grnList.length > 1) {
            if (this.showLastRow == false) {
                this.listshowLastRowPlus = false;
                this.listshowLastRowMinus = true;
                this.showLastRowPlus = true;

            } else {
                this.listshowLastRowPlus = true;
                this.listshowLastRowMinus = false;

            }
        }
        if (this.grnList.length == 1 && this.showLastRow == false) {
            this.listshowLastRowMinus = true;
            this.listshowLastRowPlus = false;

        }
        if (this.grnList.length == 1 && this.showLastRow == true) {
            this.listshowLastRowMinus = false;
            this.listshowLastRowPlus = true;

        }
        if (this.grnList.length == 0 && this.showLastRow == false) {
            this.showLastRowMinus = false;
            this.showLastRowPlus = true;
        }
        if (this.grnList.length == 0 && this.showLastRow == true) {
            this.showLastRowMinus = true;
            this.showLastRowPlus = false;
        }
        if (this.idview == 2 || this.viewDisabled == true) {
            this.listshowLastRowPlus = false;
        }
    }
    deletemodule() {
        this.showLastRow = true;
        this.showIcons();
    }
    showNewRow() {
        var confirmPop = confirm("Do you want to Add?");
        if (confirmPop == false) {
            return false;
        }
        this.showLastRow = false;
        this.showIcons();
    }
    ngOnInit() {
        let typeofitemObject = {};
        typeofitemObject['id'] = 1;
        typeofitemObject['name'] = 'Assets';
        this.typeItemList.push(typeofitemObject);
        typeofitemObject = {};
        typeofitemObject['id'] = 2;
        typeofitemObject['name'] = 'Expense';
        this.typeItemList.push(typeofitemObject);
        typeofitemObject = {};
        typeofitemObject['id'] = 3;
        typeofitemObject['name'] = 'Inventory';
        this.typeItemList.push(typeofitemObject);

        this.ajaxCount = 0;
        this.spinner.show();
        this.viewDisabled = false;
        this.editDisabled = false;
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.route.paramMap.subscribe((data) => this.idview = + data.get('idview'));

        console.log(this.id);
        if (this.id < 1) {
            this.showLastRow = false;
            this.showLastRowMinus = false;
            this.showLastRowPlus = true;
        } else {
            this.showLastRow = true;

        }
        this.getGRNumber();

        // let ordertype = {};
        // ordertype['name'] = 'PO';
        // this.orderList.push(ordertype);
        // this.grnDataheader['f034forderType'] = ordertype['name'];

        let ordertype = {};
        ordertype['name'] = 'PO';
        this.orderList.push(ordertype);
        ordertype = {};
        ordertype['name'] = 'Warrant';
        this.orderList.push(ordertype);
        ordertype = {};
        ordertype['name'] = 'Direct Purchase';
        this.orderList.push(ordertype);
        ordertype = {};
        ordertype['name'] = 'Tender';
        this.orderList.push(ordertype);

        let ratetype = {};
        ratetype['name1'] = 'A';
        this.ratingList.push(ratetype);
        ratetype = {};
        ratetype['name1'] = 'B';
        this.ratingList.push(ratetype);
        ratetype = {};
        ratetype['name1'] = 'C';
        this.ratingList.push(ratetype);


        //purchase order dropdown
        this.ajaxCount++;
        this.PoapprovalService.getpoNotIn().subscribe(
            data => {
                this.ajaxCount--;
                this.purchaseOrderList = data['result'];
            }, error => {
                console.log(error);
            });
        //item dropdown
        this.ajaxCount++;
        this.ItemService.getAllItems().subscribe(
            data => {
                this.ajaxCount--;
                this.itemList = data['result']['data'];
                console.log(this.itemList);
            }
        );
        //Supplierregistration dropdown
        this.ajaxCount++;
        this.SupplierregistrationService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.supplierList = data['result']['data'];
            }
        );
        //Department dropdown
        this.ajaxCount++;
        this.DepartmentService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.DeptList = data['result']['data'];
            }
        );
        //Activity dropdown
        this.ajaxCount++;
        this.ActivitycodeService.getItemsByLevel().subscribe(
            data => {
                this.ajaxCount--;
                this.activitycodeList = data['result']['data'];
            }
        );

        //Account dropdown
        this.ajaxCount++;
        this.AccountcodeService.getItemsByLevel().subscribe(
            data => {
                this.ajaxCount--;
                this.accountcodeList = data['result']['data'];
            }
        );

        //Fund dropdown
        this.ajaxCount++;
        this.FundService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.fundList = data['result']['data'];
            }
        );

        //Financial year dropdown
        this.ajaxCount++;
        this.FinancialyearService.getActiveItems().subscribe(
            data => {
                this.ajaxCount--;
                this.financialList = data['result'];
            }
        );

        //Department dropdown
        this.ajaxCount++;
        this.DepartmentService.getZerodepartment().subscribe(
            data => {
                this.ajaxCount--;
                this.departmentList = data['result']['data'];
            }
        );
        this.ajaxCount++;
        this.TaxsetupcodeService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.taxcodeList = data['result']['data'];
            });
        this.onBlurMethod();
        this.showIcons();
    }

    getUOM() {
        let itemId = this.grnData['f034fidItem'];
        console.log(itemId);
        this.ItemService.getItemsDetail(itemId).subscribe(
            data => {
                this.itemUnitList = data['result'][0];
                console.log(data);
                this.grnData['f034funit'] = data['result'][0]['f029funit'];
            }
        );
    }
    getPoList() {
        let orderTypeId = this.grnDataheader['f034forderType'];
        console.log(orderTypeId);

        this.GrnService.getPoListByOrderType(orderTypeId).subscribe(
            data => {
                this.poListBasedOnOrderTypeList = data['result']['data'];
            }
        );
    }
    getDepartment() {
        var departmentId = this.grnDataheader['f034fidDepartment'];
        var departmentCode = departmentId.substring(0, 3);;

        this.BudgetactivitiesService.getCostCenterBasedOnDepartmentCode(departmentCode).subscribe(
            data => {
                this.costCenterList = data['result']['data'];
            }, error => {
                console.log(error);
            });
    }
    getGRNumber() {
        let typeObject = {};
        typeObject['type'] = "GR"
        console.log(typeObject);
        this.GrnService.getGRN(typeObject).subscribe(
            data => {
                console.log(data);
                this.grnDataheader['f034freferenceNumber'] = data['number'];
                this.valueDate = new Date().toISOString().substr(0, 10);
                this.grnDataheader['f034forderDate'] = this.valueDate;
            }
        );
        return this.type;
    }


    getPurchaseOrderDetails() {
        let PurchaseOrderId = this.grnDataheader['f034fidPurchaseOrder'];
        this.grnList = [];
        this.PurchaseorderService.getItemsDetail(PurchaseOrderId).subscribe(
            data => {
                let purchaseOrderListNew = data['result'];
                this.grnDataheader['f034forderType'] = data['result'][0]['f034forderType'];
                this.grnDataheader['f034fidFinancialyear'] = data['result'][0]['f034fidFinancialyear'];
                this.grnDataheader['f034fidSupplier'] = data['result'][0]['f034fidSupplier'];
                this.grnDataheader['f034fidDepartment'] = data['result'][0]['f034fdepartmentCode'];
                this.grnDataheader['f034fexpiredDate'] = data['result'][0]['f034forderDate'];
                this.grnDataheader['f034fdescription'] = data['result'][0]['f034fdescription'];
                this.grnDataheader['f034ftotalAmount'] = data['result'][0]['f034ftotalAmount'];
                console.log(this.grnDataheader);

                this.getAllpurchaseGlList();

                for (var i = 0; i < purchaseOrderListNew.length; i++) {
                    var purchaseOrderObject = {}

                    purchaseOrderObject['f034fidItem'] = purchaseOrderListNew[i]['f034fidItem'];
                    purchaseOrderObject['f034funit'] = purchaseOrderListNew[i]['f034funit'];
                    purchaseOrderObject['f034ffundCode'] = purchaseOrderListNew[i]['f034ffundCode'];
                    purchaseOrderObject['f034factivityCode'] = purchaseOrderListNew[i]['f034factivityCode'];
                    purchaseOrderObject['f034fdepartmentCode'] = purchaseOrderListNew[i]['f034fdepartmentCode'];
                    purchaseOrderObject['f034faccountCode'] = purchaseOrderListNew[i]['f034faccountCode'];
                    purchaseOrderObject['f034fbudgetFundCode'] = purchaseOrderListNew[i]['f034fbudgetFundCode'];
                    purchaseOrderObject['f034fbudgetActivityCode'] = purchaseOrderListNew[i]['f034fbudgetActivityCode'];
                    purchaseOrderObject['f034fbudgetDepartmentCode'] = purchaseOrderListNew[i]['f034fbudgetDepartmentCode'];
                    purchaseOrderObject['f034fbudgetAccountCode'] = purchaseOrderListNew[i]['f034fbudgetAccountCode'];
                    purchaseOrderObject['f034fsoCode'] = purchaseOrderListNew[i]['f034fsoCode'];
                    purchaseOrderObject['f034frequiredDate'] = purchaseOrderListNew[i]['f034frequiredDate'];
                    purchaseOrderObject['f034fquantity'] = purchaseOrderListNew[i]['f034fquantity'];
                    purchaseOrderObject['f034fprice'] = this.ConvertToFloat(purchaseOrderListNew[i]['f034fprice']).toFixed(2);
                    purchaseOrderObject['f034total'] = this.ConvertToFloat(purchaseOrderListNew[i]['f034total']).toFixed(2);
                    purchaseOrderObject['f034ftaxCode'] = purchaseOrderListNew[i]['f034ftaxCode'];
                    purchaseOrderObject['f034fpercentage'] = purchaseOrderListNew[i]['f034fpercentage'];
                    purchaseOrderObject['f034ftaxAmount'] = this.ConvertToFloat(purchaseOrderListNew[i]['f034ftaxAmount']).toFixed(2);
                    purchaseOrderObject['f034ftotalIncTax'] = this.ConvertToFloat(purchaseOrderListNew[i]['f034ftotalIncTax']).toFixed(2);
                    purchaseOrderObject['categoryName'] = purchaseOrderListNew[i]['categoryName'];
                    purchaseOrderObject['subCategoryName'] = purchaseOrderListNew[i]['subCategoryName'];
                    purchaseOrderObject['itemName'] = purchaseOrderListNew[i]['itemName'];
                    purchaseOrderObject['f034fitemType'] = purchaseOrderListNew[i]['f034fcategoryType'];
                    purchaseOrderObject['f034fitemCategory'] = purchaseOrderListNew[i]['f034fitemCategory'];
                    purchaseOrderObject['f034fitemSubCategory'] = purchaseOrderListNew[i]['f034fitemSubCategory'];
                    purchaseOrderObject['f034fassetCode'] = purchaseOrderListNew[i]['f034fassetCode'];
                    purchaseOrderObject['f034fbalanceQuantity'] = purchaseOrderListNew[i]['f034fbalanceQuantity'];
                    purchaseOrderObject['f034fbalanceQuantityOriginal'] = purchaseOrderListNew[i]['f034fbalanceQuantity'];
                    this.grnList.push(purchaseOrderObject);
                }
                this.showLastRow = true;
                this.showIcons();
                this.getDepartment();
                console.log(this.grnList);
            }
        );
    }
    ConvertToFloat(val) {
        return parseFloat(val);
    }

    getAllpurchaseGlList() {
        var idfinancialYear = this.grnDataheader['f034fidFinancialyear'];

        this.PurchaserequistionentryService.urlgetAllpurcahseGls(idfinancialYear).subscribe(
            data => {
                var depArray = data['result']['Department'];
                this.budgetcostcenterList = depArray;

                this.costCenterFund = data['result']['Fund'];

                this.costCenterAccount = data['result']['Account'];

                this.costCenterActivity = data['result']['Activity'];

                console.log(data);
            }, error => {
                console.log(error);
            }
        );
    }
    checkBalance(i){

        if(parseInt(this.grnList[i]['f034fquantityReceived'])>parseInt(this.grnList[i]['f034fquantity'])) {
            alert("Received quantity cannot be greater than Ordered Quantity");
            return false;
        } else {
            
            this.grnList[i]['f034fbalanceQuantity'] = parseInt(this.grnList[i]['f034fbalanceQuantityOriginal'])-(parseInt(this.grnList[i]['f034fquantityReceived']));
        }
        

    }
    saveGrn() {
        this.onBlurMethod();
        this.showIcons();
        if (this.grnDataheader['f034forderType'] == undefined) {
            alert('Select Order Type');
            return false;
        }
        if (this.grnDataheader['f034fidPurchaseOrder'] == undefined) {
            alert('Select Purchase Order Id');
            return false;
        }
        if (this.grnDataheader['f034fidDepartment'] == undefined) {
            alert('Select Cost Center');
            return false;
        }
        if (this.grnDataheader['f034fidFinancialyear'] == undefined) {
            alert('Select Financial Year');
            return false;
        }
        if (this.grnDataheader['f034fidSupplier'] == undefined) {
            alert('Select Vendor');
            return false;
        }
        if (this.grnDataheader['f034frating'] == undefined) {
            alert('Select Rating');
            return false;
        }
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        if (this.showLastRow == false) {
            var addactivities = this.addEntrylist();
            if (addactivities = false) {
                return false;
            }
        }

        let purchaseOrderObject = {};
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        console.log(this.id);
        if (this.id > 0) {
            purchaseOrderObject['f034fid'] = this.id;
        }

        if (this.grnList.length > 0) {

        } else {
            alert("Please add one details");
            return false;
        }

        for(var i=0;i<this.grnList.length;i++) {
            if(this.grnList[i]['f034fquantityReceived']==undefined || this.grnList[i]['f034fquantityReceived']==0 ) {
                alert("GRN Quantity Cannot be less than 0");
                return false;
            }
        }

        purchaseOrderObject['f034forderType'] = this.grnDataheader['f034forderType'];
        purchaseOrderObject['f034forderDate'] = this.grnDataheader['f034forderDate'];
        purchaseOrderObject['f034fexpiredDate'] = this.grnDataheader['f034fexpiredDate'];
        purchaseOrderObject['f034fidSupplier'] = this.grnDataheader['f034fidSupplier'];
        purchaseOrderObject['f034fidFinancialyear'] = this.grnDataheader['f034fidFinancialyear'];
        purchaseOrderObject['f034fdescription'] = this.grnDataheader['f034fdescription'];
        purchaseOrderObject['f034freferenceNumber'] = this.grnDataheader['f034freferenceNumber'];
        purchaseOrderObject['f034fidDepartment'] = this.grnDataheader['f034fidDepartment'];
        purchaseOrderObject['f034fidPurchaseOrder'] = this.grnDataheader['f034fidPurchaseOrder'];
        purchaseOrderObject['f034ftotalAmount'] = this.grnDataheader['f034ftotalAmount'];
        purchaseOrderObject['f034frating'] = this.grnDataheader['f034frating'];
        purchaseOrderObject['f034fstatus'] = 0;
        purchaseOrderObject['grn-details'] = this.grnList;
        console.log(purchaseOrderObject);


        this.GrnService.insertEntryItems(purchaseOrderObject).subscribe(
            data => {
                this.router.navigate(['procurement/grn']);
                if (this.id > 0) {
                    alert("GRN has been Updated Sucessfully ! ");
                }
                else {
                    alert("GRN has been Added Sucessfully ! ");
                }
            }, error => {
                console.log(error);
            });

    }
    getPurchaseId() {
        this.orderTypeId = this.grnDataheader['f034forderType'];
        // this.StudentId = this.advancePaymentData['f031fidCustomer'];
        let OrderTypeObj = {};
        switch (this.type) {
            case 'Warrant':
                OrderTypeObj['type'] = "Warrant";
                break;
            case 'PO':
                OrderTypeObj['type'] = "PO";
                break;
            case 'Direct Purchase':
                OrderTypeObj['type'] = "Direct Purchase";
                break;
            case 'Tender':
                OrderTypeObj['type'] = "Tender";
                break;
        }
        OrderTypeObj['orderType'] = this.orderTypeId;
        this.spinner.show();
        this.GrnService.getPurchaseOrderItems(OrderTypeObj).subscribe(
            data => {
                this.spinner.hide();
                console.log(data);
                this.PoIdList = data['result'];
            }
        );
    }
    getGstvalue() {
        let taxId = this.grnData['f034ftaxCode'];
        let quantity = this.grnData['f034fquantity'];
        let price = this.grnData['f034fprice'];
        console.log(taxId);
        console.log(this.taxcodeList);
        var taxSelectedObject = {};
        for (var k = 0; k < this.taxcodeList.length; k++) {
            if (this.taxcodeList[k]['f081fid'] == taxId) {
                taxSelectedObject = this.taxcodeList[k];
            }
        }

        this.gstValue = taxSelectedObject['f081fpercentage'];
        console.log('GST value' + this.gstValue);
        this.grnData['f034fpercentage'] = this.gstValue;
        this.Amount = (this.ConvertToFloat(price)) * (this.ConvertToFloat(quantity));
        this.taxAmount = (this.ConvertToFloat(this.gstValue)) / 100 * this.ConvertToFloat((this.Amount));

        let totalAm = ((this.taxAmount) + (this.Amount));
        this.grnData['f034total'] = this.Amount.toFixed(2);
        this.grnData['f034ftaxAmount'] = this.taxAmount.toFixed(2);
        this.grnData['f034ftotalIncTax'] = totalAm.toFixed(2);

        console.log(this.grnData);

    }
    onBlurMethod() {
        console.log(this.grnList);
        var finaltotal = 0;
        console.log(finaltotal);
        for (var i = 0; i < this.grnList.length; i++) {
            var totalamount = (this.ConvertToFloat(this.grnList[i]['f034fquantity'])) * (this.ConvertToFloat(this.grnList[i]['f034fprice']));
            console.log(totalamount);
            var gstamount = (this.ConvertToFloat(this.grnList[i]['f034fpercentage']) / 100 * (totalamount));
            console.log(gstamount);
            this.grnList[i]['f034total'] = totalamount;

            this.grnList[i]['f034ftotalIncTax'] = gstamount + totalamount;
            console.log(finaltotal);
            finaltotal = finaltotal + this.grnList[i]['f034ftotalIncTax'];
            console.log(finaltotal);
        }

        this.grnDataheader['f034ftotalAmount'] = this.ConvertToFloat(finaltotal.toFixed(2));
    }
    amountprefill() {
        var finaltotal = 0;
        if (this.grnData['f034fprice'] != undefined) {
            var totalamount = (this.ConvertToFloat(this.grnData['f034fquantity'])) * (this.ConvertToFloat(this.grnData['f034fprice']));
            console.log(totalamount);
            var gstamount = (this.ConvertToFloat(this.grnData['f034fpercentage']) / 100 * (totalamount));
            console.log(gstamount);
            this.grnData['f034total'] = totalamount;
        }
    }
    listamountprefill() {
        // console.log(this.purchaserequistionentryList);
        for (let i = 0; i < this.grnList.length; i++) {
            if (this.grnList[i]['f034fprice'] != undefined) {
                var totalamount = (this.ConvertToFloat(this.grnList[i]['f034fquantity'])) * (this.ConvertToFloat(this.grnList[i]['f034fprice']));
                console.log(totalamount);
                var gstamount = (this.ConvertToFloat(this.grnList[i]['f034fpercentage']) / 100 * (totalamount));
                console.log(gstamount);
                this.grnList[i]['f034total'] = totalamount;
                if (this.grnList[i]['f034ftaxCode'] != undefined) {
                    let taxId = this.grnList[i]['f034ftaxCode'];
                    let quantity = this.grnList[i]['f034fquantity'];
                    let price = this.grnList[i]['f034fprice'];
                    //console.log(this.taxcodeList);
                    var taxSelectedObject = {};
                    for (var k = 0; k < this.taxcodeList.length; k++) {
                        if (this.taxcodeList[k]['f034fid'] == taxId) {
                            taxSelectedObject = this.taxcodeList[k];
                        }
                    }
                    this.gstValue = taxSelectedObject['f081fpercentage'];
                    this.grnList[i]['f088fpercentage'] = this.gstValue;
                    this.Amount = (this.ConvertToFloat(price)) * (this.ConvertToFloat(quantity));
                    this.taxAmount = (this.ConvertToFloat(this.gstValue)) / 100 * this.ConvertToFloat((this.Amount));

                    let totalAm = ((this.taxAmount) + (this.Amount));
                    // this.purchaserequistionentryData['f088total'] = this.Amount.toFixed(2);
                    this.grnList[i]['f088ftaxAmount'] = this.taxAmount.toFixed(2);
                    this.grnList[i]['f088ftotalIncTax'] = totalAm.toFixed(2);
                }
            }
        }

    }
    deleteEntry(object) {
        var cnf = confirm("Do you really want to delete");
        if (cnf == true) {
            console.log(this.grnList);
            var index = this.grnList.indexOf(object);
            this.deleteList.push(this.grnList[index]['f034fidDetails']);
            let deleteObject = {};
            deleteObject['id'] = this.deleteList;
            this.GrnService.deleteItems(deleteObject).subscribe(
                data => {
                }, error => {
                    console.log(error);
                });
            if (index > -1) {
                this.grnList.splice(index, 1);
            }
            this.onBlurMethod();
            this.showIcons();
        }
    }

    addEntrylist() {
        if (this.grnData['f034ftypeOfItem'] == undefined) {
            alert('Select Type of Item');
            return false;
        }
        if (this.grnData['f034fidItem'] == undefined) {
            alert('Select Item');
            return false;
        }
        if (this.grnData['f034funit'] == undefined) {
            alert('Select Unit');
            return false;
        }
        if (this.grnData['f034ffundCode'] == undefined) {
            alert('Select GL Fund');
            return false;
        }
        if (this.grnData['f034factivityCode'] == undefined) {
            alert('Select GL Activity Code');
            return false;
        }
        if (this.grnData['f034fdepartmentCode'] == undefined) {
            alert('Select GL Cost Center Code');
            return false;
        }
        if (this.grnData['f034faccountCode'] == undefined) {
            alert('Select GL Account Code');
            return false;
        }
        if (this.grnData['f034fbudgetFundCode'] == undefined) {
            alert('Select Budget Fund');
            return false;
        }
        if (this.grnData['f034fbudgetActivityCode'] == undefined) {
            alert('Select Budget Activity Code');
            return false;
        }
        if (this.grnData['f034fbudgetDepartmentCode'] == undefined) {
            alert('Select Budget Cost Center Code');
            return false;
        }
        if (this.grnData['f034fbudgetAccountCode'] == undefined) {
            alert('Select Budget Account Code');
            return false;
        }
        if (this.grnData['f034fquantityReceived'] == undefined || this.grnData['f034fquantityReceived'] == 0) {
            alert('Enter the Quantity Received');
            return false;
        }
        if (this.grnData['f034frequiredDate'] == undefined) {
            alert('Select Required Date');
            return false;
        }
        if (this.grnData['f034fquantity'] == undefined) {
            alert('Enter the Quantity');
            return false;
        }
        var confirmPop = confirm("Do you want to Add?");
        if (confirmPop == false) {
            return false;
        }
        var dataofCurrentRow = this.grnData;
        this.grnData = {};
        this.grnList.push(dataofCurrentRow);
        this.onBlurMethod();
        this.amountprefill();
        this.showIcons();
        // }
        // }
    }
    onlyNumberKey(event) {
        //console.log(event);
        if (event.charCode == 46) {

        } else {
            return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
        }
    }
    getListData() {
        this.GrnapprovalService.getItemsDetail(0).subscribe(
            data => {
                this.grnapprovalList = data['result'];
            }, error => {
                console.log(error);
            });
    }
    maximize() {
        $("#myModal").addClass("modal");
        $("#overlay1").show();
        $(".maximize-icon").addClass("hidden");
        $(".minimize-icon").removeClass("hidden");
        $(".modal-backdrop").show();

    }
    minimize() {
        $("#myModal").removeClass("modal");
        $("#overlay1").hide();
        $(".maximize-icon").removeClass("hidden");
        $(".minimize-icon").addClass("hidden");
        $(".modal-backdrop").hide();
        $('#myModal').show();
    }
    GrnapprovalListData() {
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        console.log(this.grnapprovalList);
        var approvaloneIds = [];
        var approvaloneUpdate = {};

        var confirmPop = confirm("Do you want to Approve?");
        if (confirmPop == false) {
            return false;
        }
        approvaloneIds.push(this.id);
        approvaloneUpdate['id'] = approvaloneIds;
        approvaloneUpdate['status'] = 1;

        this.GrnapprovalService.updateGrnapprovalItems(approvaloneUpdate).subscribe(
            data => {
                alert("Approved Successfully");
                this.router.navigate(['procurement/grn']);
            }, error => {
                console.log(error);
            });
    }
    regectApprovalListData() {
        console.log(this.grnapprovalList);
        var approvaloneIds = [];

        if (this.approvalData['reason'] == undefined) {
            alert('Please Enter the Description');
            return false;
        }
        var confirmPop = confirm("Do you want to Reject?");
        if (confirmPop == false) {
            return false;
        }
        var budgetActivityDataObject = {};
        approvaloneIds.push(this.id);
        budgetActivityDataObject['id'] = approvaloneIds;
        budgetActivityDataObject['status'] = '2';
        budgetActivityDataObject['reason'] = this.approvalData['reason'];

        this.GrnapprovalService.updateGrnapprovalItems(budgetActivityDataObject).subscribe(
            data => {
                alert("Rejected Successfully");
                this.router.navigate(['procurement/grn']);
            }, error => {
                console.log(error);
            });
    }
}