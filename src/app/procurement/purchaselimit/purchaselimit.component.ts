import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { PurchaselimitService } from '../service/purchaselimit.service'
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'PurchaselimitComponent',
    templateUrl: 'purchaselimit.component.html'
})
export class PurchaselimitComponent implements OnInit {
    purchaseLimitList = [];
    purchaseLimitData = {};
    id: number;
    constructor(
        private PurchaselimitService: PurchaselimitService,
        private spinner: NgxSpinnerService
    ) { }

    ngOnInit() {
        this.spinner.show();
        this.PurchaselimitService.getItems().subscribe(
            data => {
                this.spinner.hide();
                this.purchaseLimitList = data['result']['data'];
            }, error => {
                console.log(error);
            });
    }
    
}