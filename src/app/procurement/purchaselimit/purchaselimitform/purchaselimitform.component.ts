import { Injector } from '@angular/core';
import { Component, OnInit, ViewChild } from '@angular/core';
import { OrdertypeService } from '../../service/ordertype.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ActivatedRoute, Router } from '@angular/router';
import { PurchaselimitService } from '../../service/purchaselimit.service'


@Component({
    selector: 'PurchaselimitFormComponent',
    templateUrl: 'purchaselimitform.component.html'
})

export class PurchaselimitFormComponent implements OnInit {

    purchaseLimitList = [];
    purchaseLimitData = {};
    purchaseMethods = [];
    orderTypeList = [];
    fordertype=[];
    ajaxCount: number;
    id: number;
    constructor(

        private PurchaselimitService: PurchaselimitService,
        private route: ActivatedRoute,
        private OrdertypeService: OrdertypeService,
        private spinner: NgxSpinnerService,
        private router: Router

    ) { }
    ngDoCheck() {
        const change = this.ajaxCount;
        if (this.ajaxCount == 0) {
            this.spinner.hide();
        }
    }
    ngOnInit() {
        let ordertypeobj = {};
        ordertypeobj['name'] = 'PO';
        this.fordertype.push(ordertypeobj);
        ordertypeobj = {};
        ordertypeobj['name'] = 'Direct Purchase';
        this.fordertype.push(ordertypeobj);
        ordertypeobj = {};
        ordertypeobj['name'] = 'Warrant';
        this.fordertype.push(ordertypeobj);
        ordertypeobj = {};
        ordertypeobj['name'] = 'Tender';
        this.fordertype.push(ordertypeobj);
        ordertypeobj = {};
        ordertypeobj['name'] = 'Quotation';
        this.fordertype.push(ordertypeobj);

        this.ajaxCount = 0;
        this.spinner.show();
        this.purchaseLimitData['f109fstatus'] = 1;
        let type = 'PurchaseMethod';
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        //order type dropdown
        this.ajaxCount++;
        this.OrdertypeService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.orderTypeList = data['result']['data'];

            }
        );
        if (this.id > 0) {
            this.ajaxCount++;
            this.PurchaselimitService.getItemsDetail(this.id).subscribe(
                data => {
                    this.ajaxCount--;
                    this.purchaseLimitData = data['result'][0];
                    this.purchaseLimitData['f109fstatus'] = parseInt(this.purchaseLimitData['f109fstatus']);

                    console.log(this.purchaseLimitData);
                }, error => {
                    console.log(error);
                });
        }
    }
    addPurchaseLimit() {
        console.log(this.purchaseLimitData);
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        if (parseInt(this.purchaseLimitData['f109fminLimit']) > parseInt(this.purchaseLimitData['f109fmaxLimit'])) {
            alert("Minimum Amount cannot be greater than Maximum Amount");
            return false;
        }
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.PurchaselimitService.updatePurchaselimitItems(this.purchaseLimitData, this.id).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert('Order Type Already Exist');
                        return false;
                    }
                    this.router.navigate(['procurement/purchaselimit']);
                    alert("Purchase Method has been Updated Successfully")
                }, error => {
                    console.log(error);
                    alert('Order Type Already Exist');
                });
        } else {

            this.PurchaselimitService.insertPurchaselimitItems(this.purchaseLimitData).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert('Order Type Already Exist');
                        return false;
                    }
                    this.router.navigate(['procurement/purchaselimit']);
                    alert("Purchase Method has been Added Successfully")

                }, error => {
                    console.log(error);
                });
        }
    }
    onlyNumberKey(event) {
        if (event.charCode == 46) {

        } else {
            return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
        }
    }
}