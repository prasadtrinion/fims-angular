import { Injector } from '@angular/core';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MofcategoryService } from '../../service/mofcategory.service';
import { AlertService } from '../../../_services/alert.service';

@Component({
    selector: 'MofcategoryFormComponent',
    templateUrl: 'mofcategoryform.component.html'
})

export class MofcategoryFormComponent implements OnInit {

    mofcategoryList = [];
    mofcategoryData = {};
    
    id: number;
    constructor(

        private MofcategoryService: MofcategoryService,
        private AlertService: AlertService,
        private route: ActivatedRoute,
        private router: Router

    ) { }

    ngOnInit() {
        this.mofcategoryData['f114fstatus'] = 1;

          this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        if (this.id > 0) {
        this.MofcategoryService.getItemsDetail(this.id).subscribe(
        data => {
        this.mofcategoryData = data['result'][0];
        this.mofcategoryData['f114fstatus'] = parseInt(this.mofcategoryData['f114fstatus']);

        console.log(this.mofcategoryData);
        }, error => {
        console.log(error);
        });
        }
    }
    addMofcategory() {
        console.log(this.mofcategoryData);
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.MofcategoryService.updateMofcategoryItems(this.mofcategoryData, this.id).subscribe(
                data => {
                    if(data['status'] == 409){
                        alert('Mof Category Already Exist');
                    }
                    else{
                    this.router.navigate(['procurement/mofcategory']);
                    this.AlertService.success("Updated Sucessfully !!")
                    alert(" Updated Sucessfully !!");
                    }
                }, error => {
                    alert('Mof Category Already Exist');
                    return false;
                    // console.log(error);
                });
        } else {
            this.MofcategoryService.insertMofcategoryItems(this.mofcategoryData).subscribe(
                data => {
                    if(data['status'] == 409){
                        alert('Mof Category Already Exist');
                    }
                    else{
                    this.router.navigate(['procurement/mofcategory']);
                    this.AlertService.success("Added Sucessfully !!")
                    alert(" Added Sucessfully !!");
                    }
                }, error => {
                    console.log(error);
                });
        }
    }

}