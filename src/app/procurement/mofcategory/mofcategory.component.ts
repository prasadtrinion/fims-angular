import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MofcategoryService } from '../service/mofcategory.service'

@Component({
    selector: 'Mofcategory',
    templateUrl: 'mofcategory.component.html'
})
export class MofcategoryComponent implements OnInit {
    mofcategoryList = [];
    mofcategoryData = {};
    id: number;
    constructor(
        private MofcategoryService: MofcategoryService
    ) { }

    ngOnInit() {

        this.MofcategoryService.getItems().subscribe(
            data => {
                this.mofcategoryList = data['result']['data'];
            }, error => {
                console.log(error);
            });
    }
}