import { Injectable } from '@angular/core';
import { HttpClientModule, HttpHeaders, HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({
        'X-Auth-Client': 'qazwsx123',
        'Content-Type': 'application/json'
    })

};

@Injectable()

export class PurchaserequistionentryService {
    purchaseRequisitionurl: string = environment.api.base + environment.api.endPoints.purchaseRequisition;
    urlREferenceNumber: string = environment.api.base + environment.api.endPoints.generateNumber;
    grnnurl: string = environment.api.base + environment.api.endPoints.grnAsset
    assetInformationurl: string = environment.api.base + environment.api.endPoints.assetInformation;
    urlgetpurchaseAllGls: string = environment.api.base + environment.api.endPoints.getAllPurchaseGlCode;
    urlDeleteItems: string = environment.api.base + environment.api.endPoints.deletePurchaseRequistionDetails;
    urlSocode: string = environment.api.base + environment.api.endPoints.socode;

    constructor(private httpClient: HttpClient) {

    }
    getItems() {
        return this.httpClient.get(this.purchaseRequisitionurl, httpOptions);
    }
    getSocodeItems() {
        return this.httpClient.get(this.urlSocode, httpOptions);
    }
    deleteItems(deleteObject) {
        return this.httpClient.post(this.urlDeleteItems, deleteObject, httpOptions);
    }
    postgrn(purchaseRequisitionData): Observable<any> {
        return this.httpClient.post(this.grnnurl, purchaseRequisitionData, httpOptions);
    }

    getItemsDetail(id) {
        return this.httpClient.get(this.purchaseRequisitionurl + '/' + id, httpOptions);
    }

    urlgetAllpurcahseGls(id) {
        return this.httpClient.get(this.urlgetpurchaseAllGls + '/' + id, httpOptions);
    }

    insertEntryItems(purchaseRequisitionData): Observable<any> {
        return this.httpClient.post(this.purchaseRequisitionurl, purchaseRequisitionData, httpOptions);
    }

    preAssetInformation(purchaseRequisitionData): Observable<any> {
        return this.httpClient.post(this.grnnurl, purchaseRequisitionData, httpOptions);
    }
    updateEntryItems(purchaseRequisitionData, id): Observable<any> {
        return this.httpClient.put(this.purchaseRequisitionurl + '/' + id, purchaseRequisitionData, httpOptions);
    }
    getPurchaseRequisitionNumber(data) {
        return this.httpClient.post(this.urlREferenceNumber, data, httpOptions);

    }
}