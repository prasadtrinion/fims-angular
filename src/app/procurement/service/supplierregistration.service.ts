import { Injectable } from '@angular/core';
import { HttpClientModule,HttpHeaders, HttpClient} from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({ 'X-Auth-Client': 'qazwsx123',
    'Content-Type': 'application/json' })
    
  };
  
@Injectable()

export class SupplierregistrationService {
    url: string = environment.api.base + environment.api.endPoints.vendor;
    Activeurl: string = environment.api.base + environment.api.endPoints.vendorActive;
    undefinedvendorcompanyinsuranceurl: string = environment.api.base + environment.api.endPoints.undefinedvendorcompanyinsurance;
    CodeUrl: string = environment.api.base + environment.api.endPoints.generateNumber;
    urlLicenseItems: string = environment.api.base + environment.api.endPoints.deleteLicenseDetails;
    urlContactItems: string = environment.api.base + environment.api.endPoints.deleteContactDetails;
    urlNaturalItems: string = environment.api.base + environment.api.endPoints.deleteNatureOfBusiness;
    urlpaytoProfileItems: string = environment.api.base + environment.api.endPoints.deletePaytoProfileDetails;
    urlMOfItems: string = environment.api.base + environment.api.endPoints.deletevendorMof;

    constructor(private httpClient: HttpClient) { 
        
    }
    getCode(type): Observable<any> {
        return this.httpClient.post(this.CodeUrl,type,httpOptions);
    }
    deleteContactItems(deleteObject){
        return this.httpClient.post(this.urlContactItems,deleteObject,httpOptions);          
    }
    deletePaytoProfileList(deleteObject){
        return this.httpClient.post(this.urlpaytoProfileItems,deleteObject,httpOptions);          
    }
    deleteLicenseItems(deleteObject){
        return this.httpClient.post(this.urlLicenseItems,deleteObject,httpOptions);          
    }
    deleteNaturalItems(deleteObject){
        return this.httpClient.post(this.urlNaturalItems,deleteObject,httpOptions);          
    }
    deleteMofItems(deleteObject){
        return this.httpClient.post(this.urlMOfItems,deleteObject,httpOptions);          
    }
    getActiveItems() {
        return this.httpClient.get(this.Activeurl,httpOptions);
    }
    getUndefinedVendorCompanyInsurance() {
        return this.httpClient.get(this.undefinedvendorcompanyinsuranceurl,httpOptions);
    }
    getItems() {
        return this.httpClient.get(this.url,httpOptions);
    }

    getItemsDetail(id) {
        return this.httpClient.get(this.url+'/'+id,httpOptions);
    }

    insertSupplierregistrationItems(supplierData): Observable<any> {
        return this.httpClient.post(this.url, supplierData,httpOptions);
    }

    updateSupplierregistrationItems(supplierData,id): Observable<any> {
        return this.httpClient.put(this.url+'/'+id, supplierData,httpOptions);
       }
}