import { Injectable } from '@angular/core';
import { HttpClientModule, HttpHeaders, HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({
        'X-Auth-Client': 'qazwsx123',
        'Content-Type': 'application/json'
    })

};
@Injectable()
export class GrnService {
    purchaseorderurl: string = environment.api.base + environment.api.endPoints.grn;
    grnurl: string = environment.api.base + environment.api.endPoints.grnNotIn;
    urlREferenceNumber: string = environment.api.base + environment.api.endPoints.generateNumber;
    Definationurl: string = environment.api.base + environment.api.endPoints.Definationurl;
    getListByOrderTypeUrl: string = environment.api.base + environment.api.endPoints.getListByOrderType;
    urlDeleteItems: string = environment.api.base + environment.api.endPoints.deleteGRNDetails;
    poIdUrl = environment.api.base + environment.api.endPoints.PONotInGrn;
    listOfGrnByStatusUrl = environment.api.base + environment.api.endPoints.listOfGrnByStatus;
    getGrnDetailsUrl = environment.api.base + environment.api.endPoints.getAssetPreRegistrationNotInAsset;

    constructor(private httpClient: HttpClient) {

    }
    deleteItems(deleteObject) {
        return this.httpClient.post(this.urlDeleteItems, deleteObject, httpOptions);
    }
    getPoListByOrderType(type) {
        return this.httpClient.get(this.getListByOrderTypeUrl + '/' + type, httpOptions);
    }
    getItems() {
        return this.httpClient.get(this.purchaseorderurl, httpOptions);
    }
    getPurchaseOrderItems(OrderTypeObj) {
        return this.httpClient.post(this.poIdUrl, OrderTypeObj,httpOptions);
    }
    getGrnDetails(id){
        return this.httpClient.get(this.getGrnDetailsUrl + '/' + id, httpOptions);

    }
    getGrnItems() {
        return this.httpClient.get(this.listOfGrnByStatusUrl+'/0', httpOptions);
    }
    getItemsDetail(id) {
        return this.httpClient.get(this.purchaseorderurl + '/' + id, httpOptions);
    }
    insertEntryItems(purchaseRequisitionData): Observable<any> {
        return this.httpClient.post(this.purchaseorderurl, purchaseRequisitionData, httpOptions);
    }
    updateEntryItems(purchaseRequisitionData, id): Observable<any> {
        return this.httpClient.put(this.purchaseorderurl + '/' + id, purchaseRequisitionData, httpOptions);
    }
    getGRN(data) {
        return this.httpClient.post(this.urlREferenceNumber, data, httpOptions);
    }
    getDefms(key) {
        return this.httpClient.get(this.Definationurl + '/' + key, httpOptions);
    }
}