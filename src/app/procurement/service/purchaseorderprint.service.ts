import { Injectable } from '@angular/core';
import { HttpClientModule,HttpHeaders, HttpClient} from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({ 'X-Auth-Client': 'qazwsx123',
    'Content-Type': 'application/json' })
    
  };
@Injectable()

export class PoReportapprovalService {

     url: string = environment.api.base + environment.api.endPoints.PoapprovalList  
     printurl: string = environment.api.base + environment.api.endPoints.poPrint;
     poprintlisturl: string = environment.api.base + environment.api.endPoints.purchaseOrderWarrantiesApproval;
     poprinturl: string = environment.api.base + environment.api.endPoints.getPurchaseOrderWarrant;

    constructor(private httpClient: HttpClient) {    
    }
    getItems() {
        return this.httpClient.get(this.url+'/'+1,httpOptions);
    } 
    getprintItems() {
        return this.httpClient.get(this.poprintlisturl,httpOptions);
    }
    getApprovedItems() {
        return this.httpClient.get(this.poprinturl,httpOptions);
    }   
    insertPoPrintItems(journalData): Observable<any> {
        return this.httpClient.post(this.printurl, journalData, httpOptions);

    }
}