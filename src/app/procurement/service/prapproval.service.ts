import { Injectable } from '@angular/core';
import { HttpClientModule,HttpHeaders, HttpClient} from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({ 'X-Auth-Client': 'qazwsx123',
    'Content-Type': 'application/json' })
    
  };
@Injectable()

export class PrapprovalService {

     url: string = environment.api.base + environment.api.endPoints.getPurchaseApprovedList 
     approvalPuturl: string = environment.api.base + environment.api.endPoints.purchaseOrderNotIn
     approvedreference: string = environment.api.base + environment.api.endPoints.purchaseRequisitionApprove
    referenceurl: string = environment.api.base + environment.api.endPoints.purchaseOrderWarrant;     
    balanceurl: string = environment.api.base + environment.api.endPoints.balancestatus;     
    warrantApprovalPRUrl: string = environment.api.base + environment.api.endPoints.warrantApprovalPRUrl;     
     
    constructor(private httpClient: HttpClient) {    
    }
    getItems() {
        return this.httpClient.get(this.url+'/'+0,httpOptions);
    } 
    getReferenceItems() {
        return this.httpClient.get(this.referenceurl,httpOptions);
    }  
    getApprovedItems() {
        return this.httpClient.get(this.approvalPuturl,httpOptions);
    } 
    getWarrantItems() {
        return this.httpClient.get(this.warrantApprovalPRUrl,httpOptions);
    }  
    getItemsDetail() {
        return this.httpClient.get(this.url, httpOptions);
    }
    insertPrapprovalItems(grnapprovalData): Observable<any> {
        return this.httpClient.post(this.url, grnapprovalData,httpOptions);
    }
    updatePrapprovalItems(grnapprovalData): Observable<any> {
        return this.httpClient.post(this.approvedreference, grnapprovalData,httpOptions);
    }
    getBalanceStatus(data){
        return this.httpClient.post(this.balanceurl, data,httpOptions);
    }
}