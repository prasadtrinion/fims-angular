import { Injectable } from '@angular/core';
import { HttpClientModule, HttpHeaders, HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({
        'X-Auth-Client': 'qazwsx123',
        'Content-Type': 'application/json'
    })

};

@Injectable()

export class TenderquatationService {
    url: string = environment.api.base + environment.api.endPoints.tender;
    urlgetListBasedOnTendor: string = environment.api.base + environment.api.endPoints.getListBasedOnTendor;
    urltenderAward: string = environment.api.base + environment.api.endPoints.shortlistedVendorsByTender;
    urltenderRegistration: string = environment.api.base + environment.api.endPoints.tenderSubmission;
    urltendershortlist: string = environment.api.base + environment.api.endPoints.getTendorDetails
    urltenderSuppliers: string = environment.api.base + environment.api.endPoints.tenderSuppliers;
    getCreditnoteNumberUrl: string = environment.api.base + environment.api.endPoints.generateNumber;
    getfinalshortlist: string = environment.api.base + environment.api.endPoints.shortlistVendors;
    agreementUrl: string = environment.api.base + environment.api.endPoints.agreement;

    getfinalawardList: string = environment.api.base + environment.api.endPoints.shortlistedVendorsByTender
    awardVendorsurl: string = environment.api.base + environment.api.endPoints.awardVendors
    tenderAwardUrl: string = environment.api.base + environment.api.endPoints.tenderAward
    newAwardsUrl: string = environment.api.base + environment.api.endPoints.newTenders;
    deleteItemsUrl: string = environment.api.base + environment.api.endPoints.deleteTenderSpec;
    deleteComiteeUrl: string = environment.api.base + environment.api.endPoints.deleteTenderComitee;
    deleteRemarksUrl: string = environment.api.base + environment.api.endPoints.deleteTenderRemarks;
    deleteVendorUrl: string = environment.api.base + environment.api.endPoints.deleteTenderSubmission;

    getTenderSubmissionUrl: string = environment.api.base + environment.api.endPoints.getTenderSubmission;
    getTenderShortlistedUrl: string = environment.api.base + environment.api.endPoints.getTenderShortlisted;
    getTenderAwardedUrl: string = environment.api.base + environment.api.endPoints.getTenderAwarded;

    preOfQuotationTenderUrl: string = environment.api.base + environment.api.endPoints.preOfQuotationTenderUrl;     

    constructor(private httpClient: HttpClient) {

    }

    insertTenderShortlist(tenderData): Observable<any> {
        return this.httpClient.post(this.urltenderSuppliers, tenderData, httpOptions);
    }
    getTenderAndQuotationRequisiontData(data) {
        return this.httpClient.post(this.preOfQuotationTenderUrl, data, httpOptions);
    }

    getItems() {
        return this.httpClient.get(this.url, httpOptions);
    }
    getAgreementItems() {
        return this.httpClient.get(this.agreementUrl, httpOptions);
    }
    deleteItems(deleteObject) {
        return this.httpClient.post(this.deleteItemsUrl, deleteObject, httpOptions);
    }
    deleteVendorItems(deleteObject) {
        return this.httpClient.post(this.deleteVendorUrl, deleteObject, httpOptions);
    }
    deleteCommiteeItems(deleteObject) {
        return this.httpClient.post(this.deleteComiteeUrl, deleteObject, httpOptions);
    }
    deleteRemarksItems(deleteObject) {
        return this.httpClient.post(this.deleteRemarksUrl, deleteObject, httpOptions);
    }
    getNewItems() {
        return this.httpClient.get(this.newAwardsUrl, httpOptions);
    }
    getNumber(data) {
        return this.httpClient.post(this.getCreditnoteNumberUrl, data, httpOptions);
    }

    downloadAgreement(id): Observable<any> {
        return this.httpClient.get(this.tenderAwardUrl + '/' + id, httpOptions);
    }
    getItemshotlist() {
        return this.httpClient.get(this.urltenderSuppliers, httpOptions);
    }
    getItemsFinalAwardTender() {
        return this.httpClient.get(environment.api.base + 'finalAwardTender', httpOptions);
    }

    getAllListBasedOnTendor(id) {
        return this.httpClient.get(this.urltenderRegistration + '/' + id, httpOptions);
    }
    getOnlySubmissions() {
        return this.httpClient.get(this.urltenderRegistration, httpOptions);
    }
    getAllAwardListBasedOnTendor(id) {
        return this.httpClient.get(this.urltenderAward + '/' + id, httpOptions);
    }
    getawardVendor(id) {
        return this.httpClient.get(this.getfinalawardList + '/' + id, httpOptions);
    }

    awardVendor(tenderData): Observable<any> {
        return this.httpClient.post(this.awardVendorsurl, tenderData, httpOptions);
    }

    getShortlistVendor(id) {
        return this.httpClient.get(this.urltendershortlist + '/' + id, httpOptions);
    }
    finalinsertTenderShortlist(tenderData): Observable<any> {
        return this.httpClient.post(this.getfinalshortlist, tenderData, httpOptions);
    }

    getItemsDetail(id) {
        return this.httpClient.get(this.url + '/' + id, httpOptions);
    }

    insertTenderquatationItems(tenderData): Observable<any> {
        return this.httpClient.post(this.url, tenderData, httpOptions);
    }

    insertTenderRefistrationItems(tenderData): Observable<any> {
        return this.httpClient.post(this.urltenderRegistration, tenderData, httpOptions);
    }
    getRegisteredItems() {
        return this.httpClient.get(this.urltenderRegistration, httpOptions);
    }
    getItemsRegistrationDetail(id) {
        return this.httpClient.get(this.urltenderRegistration + '/' + id, httpOptions);
    }

    updateTenderquatationItems(tenderData, id): Observable<any> {
        return this.httpClient.put(this.url + '/' + id, tenderData, httpOptions);
    }
    getTenderSubmissions() {
        return this.httpClient.get(this.getTenderSubmissionUrl + '/1', httpOptions);
    }
    getTenderShorlists() {
        return this.httpClient.get(this.getTenderShortlistedUrl + '/0', httpOptions);
    }
    getAwardShorlists() {
        return this.httpClient.get(this.getTenderAwardedUrl + '/0', httpOptions);
    }
    getTenderAwards() {
        return this.httpClient.get(this.getTenderAwardedUrl + '/1', httpOptions);
    }
}