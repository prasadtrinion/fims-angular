import { Injectable } from '@angular/core';
import { HttpClientModule,HttpHeaders, HttpClient} from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({ 'X-Auth-Client': 'qazwsx123',
    'Content-Type': 'application/json' })
    
  };
  
@Injectable()

export class LicenseService {
    url: string = environment.api.base + environment.api.endPoints.license;
    Activeurl: string = environment.api.base + environment.api.endPoints.getActiveLicense;
    constructor(private httpClient: HttpClient) { 
        
    }
    
    getItems() {
        return this.httpClient.get(this.url,httpOptions);
    }
    getActiveItems() {
        return this.httpClient.get(this.Activeurl+'/'+1,httpOptions);
    }
    getItemsDetail(id) {
        return this.httpClient.get(this.url+'/'+id,httpOptions);
    }

    insertLicenseItems(licenseData): Observable<any> {
        return this.httpClient.post(this.url, licenseData,httpOptions);
    }

    updateLicenseItems(itemsetupData,id): Observable<any> {
        return this.httpClient.put(this.url+'/'+id, itemsetupData,httpOptions);
       }
}