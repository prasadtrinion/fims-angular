import { Injectable } from '@angular/core';
import { HttpClientModule,HttpHeaders, HttpClient} from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({ 'X-Auth-Client': 'qazwsx123',
    'Content-Type': 'application/json' })
    
  };
@Injectable()

export class PoapprovalService {

     url: string = environment.api.base + environment.api.endPoints.PoapprovalList 
     approvalPuturl: string = environment.api.base + environment.api.endPoints.Poapprove
     poNotInurl: string = environment.api.base + environment.api.endPoints.PONotInGrn
    constructor(private httpClient: HttpClient) {    
    }
    getItems() {
        return this.httpClient.get(this.url+'/'+0,httpOptions);
    }
    getpoNotIn() {
        return this.httpClient.get(this.poNotInurl,httpOptions);
    }
    getApprovedItems() {
        return this.httpClient.get(this.url+'/'+1,httpOptions);
    }   
    getItemsDetail() {
        return this.httpClient.get(this.url, httpOptions);
    }
    insertPoapprovalItems(grnapprovalData): Observable<any> {
        return this.httpClient.post(this.url, grnapprovalData,httpOptions);
    }
    updatePoapprovalItems(grnapprovalData): Observable<any> {
        return this.httpClient.post(this.approvalPuturl, grnapprovalData,httpOptions);
    }
}