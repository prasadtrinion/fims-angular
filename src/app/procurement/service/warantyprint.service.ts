import { Injectable } from '@angular/core';
import { HttpClientModule,HttpHeaders, HttpClient} from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({ 'X-Auth-Client': 'qazwsx123',
    'Content-Type': 'application/json' })
    
  };
@Injectable()

export class WarrantyPrintService {
    url: string = environment.api.base + environment.api.endPoints.ApprovedPoWarranties;
    printurl: string = environment.api.base + environment.api.endPoints.warrantPrint;
    constructor(private httpClient: HttpClient) {    
    }
    getItems() {
        return this.httpClient.get(this.url+'/'+1,httpOptions);
    } 
    getApprovedItems() {
        return this.httpClient.get(this.url+'/'+1,httpOptions);
    }   
    insertPoPrintItems(journalData): Observable<any> {
        return this.httpClient.post(this.printurl, journalData, httpOptions);

    }
}