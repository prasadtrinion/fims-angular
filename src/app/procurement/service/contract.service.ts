import { Injectable } from '@angular/core';
import { HttpClientModule,HttpHeaders, HttpClient} from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({ 'X-Auth-Client': 'qazwsx123',
    'Content-Type': 'application/json' })
    
  };
  
@Injectable()

export class ContractService {
    url: string = environment.api.base + environment.api.endPoints.paymentAward;
    PaymentAwardurl: string = environment.api.base + environment.api.endPoints.awardedTender;
    Activeurl: string = environment.api.base + environment.api.endPoints.revenueActive;    
    constructor(private httpClient: HttpClient) { 
        
    }
    
    getItems() {
        return this.httpClient.get(this.url,httpOptions);
    }
    getPaymentAwardItems() {
        return this.httpClient.get(this.PaymentAwardurl,httpOptions);
    }
    getActiveItems() {
        return this.httpClient.get(this.Activeurl,httpOptions);
    }
    getItemsDetail(id) {
        return this.httpClient.get(this.url+'/'+id,httpOptions);
    }

    insertContractItems(PattycashallocationData): Observable<any> {
        return this.httpClient.post(this.url, PattycashallocationData,httpOptions);
    }

    updateContractItems(PattycashallocationData,id): Observable<any> {
        return this.httpClient.put(this.url+'/'+id, PattycashallocationData,httpOptions);
       }
}