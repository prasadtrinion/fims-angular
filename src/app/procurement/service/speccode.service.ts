import { Injectable } from '@angular/core';
import { HttpClientModule, HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({
        'X-Auth-Client': 'qazwsx123',
        'Content-Type': 'application/json'
    })

};
@Injectable()
export class SpecCodeService {
    url: string = environment.api.base + environment.api.endPoints.specCode;

    constructor(private httpClient: HttpClient) {

    }
    getItems() {
        return this.httpClient.get(this.url, httpOptions);
    }
    getItemsDetail(id) {
        return this.httpClient.get(this.url + '/' + id, httpOptions);
    }
    insertEntryItems(data): Observable<any> {
        return this.httpClient.post(this.url, data, httpOptions);
    }
    updateEntryItems(data, id): Observable<any> {
        return this.httpClient.put(this.url + '/' + id, data, httpOptions);
    }
    getPurchaseOrderNumber(data) {
        return this.httpClient.post(this.url, data, httpOptions);
    }
}