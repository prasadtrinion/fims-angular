import { Injectable } from '@angular/core';
import { HttpClientModule,HttpHeaders, HttpClient} from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({ 'X-Auth-Client': 'qazwsx123',
    'Content-Type': 'application/json' })
    
  };
  
@Injectable()

export class SupplierapprovalService {
    url: string = environment.api.base + environment.api.endPoints.vendor;

    CodeUrl: string = environment.api.base + environment.api.endPoints.generateNumber;
    vendorApprovalUrl : string =  environment.api.base + environment.api.endPoints.vendorApproval;
    getVendorApprovedListUrl : string = environment.api.base + environment.api.endPoints.getVendorApprovedList

    constructor(private httpClient: HttpClient) { 
        
    }
    getCode(type): Observable<any> {
        return this.httpClient.post(this.CodeUrl,type,httpOptions);
    }
    getItems() {
        return this.httpClient.get(this.getVendorApprovedListUrl+'/'+0,httpOptions);
    }

    getItemsDetail(id) {
        return this.httpClient.get(this.url+'/'+id,httpOptions);
    }

    insertSupplierregistrationItems(supplierData): Observable<any> {
        return this.httpClient.post(this.url, supplierData,httpOptions);
    }

    updateSupplierApproval(supplierData): Observable<any> {
        return this.httpClient.post(this.vendorApprovalUrl, supplierData,httpOptions);
       }
}