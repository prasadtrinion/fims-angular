import { Injectable } from '@angular/core';
import { HttpClientModule, HttpHeaders, HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({
        'X-Auth-Client': 'qazwsx123',
        'Content-Type': 'application/json'
    })

};

@Injectable()

export class WarrantapprovalService {
    warrantApprovalurl: string = environment.api.base + environment.api.endPoints.getWarrantActiveList;
    Approveurl: string = environment.api.base + environment.api.endPoints.approveWarrant;

    constructor(private httpClient: HttpClient) {
    }
    getItems() {
        return this.httpClient.get(this.warrantApprovalurl + '/' + 0, httpOptions);
    }
    updateWarrantApproval(invoiceapprovalData): Observable<any> {
        return this.httpClient.post(this.Approveurl, invoiceapprovalData, httpOptions);
    }
}