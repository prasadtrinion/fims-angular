import { Injectable } from '@angular/core';
import { HttpClientModule, HttpHeaders, HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({
        'X-Auth-Client': 'qazwsx123',
        'Content-Type': 'application/json'
    })

};

@Injectable()

export class PurchaseorderWarrantyService {
    purchaseorderurl: string = environment.api.base + environment.api.endPoints.purchaseorder;
    purchaseorderGeturl: string = environment.api.base + environment.api.endPoints.purchaseOrderWarranties;
    urlREferenceNumber: string = environment.api.base + environment.api.endPoints.generateNumber;
    Definationurl: string = environment.api.base + environment.api.endPoints.Definationurl;
    Deleteurl: string = environment.api.base + environment.api.endPoints.deletePurchaseOrderDetails;

    constructor(private httpClient: HttpClient) {

    }
    deleteItems(deleteObject){
        return this.httpClient.post(this.Deleteurl,deleteObject,httpOptions);          
    }
    getItems() {
        return this.httpClient.get(this.purchaseorderGeturl, httpOptions);
    }
    getSupplierItems(id) {
        return this.httpClient.get(this.purchaseorderurl + '/' + id, httpOptions);
    }


    getItemsDetail(id) {
        return this.httpClient.get(this.purchaseorderurl + '/' + id, httpOptions);
    }



    insertEntryItems(purchaseRequisitionData): Observable<any> {
        return this.httpClient.post(this.purchaseorderurl, purchaseRequisitionData, httpOptions);
    }

    updateEntryItems(purchaseRequisitionData, id): Observable<any> {
        return this.httpClient.put(this.purchaseorderurl + '/' + id, purchaseRequisitionData, httpOptions);
    }
    getPurchaseOrderNumber(data) {
        return this.httpClient.post(this.urlREferenceNumber, data, httpOptions);

    }
    getDefms(key) {
        return this.httpClient.get(this.Definationurl + '/' + key, httpOptions);
    }
}