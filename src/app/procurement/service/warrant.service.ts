import { Injectable } from '@angular/core';
import { HttpClientModule, HttpHeaders, HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({
        'X-Auth-Client': 'qazwsx123',
        'Content-Type': 'application/json'
    })

};

@Injectable()

export class WarrantService {
    url: string = environment.api.base + environment.api.endPoints.warrant;
    Activeurl: string = environment.api.base + environment.api.endPoints.getWarrantActiveList;
    deleteurl: string = environment.api.base + environment.api.endPoints.deleteWarrantDetails;
    constructor(private httpClient: HttpClient) {
    }
    getItems() {
        return this.httpClient.get(this.url, httpOptions);
    }
    getActiveItems() {
        return this.httpClient.get(this.url + '/' +1, httpOptions);
    }
    deleteitems(id) {
        return this.httpClient.get(this.deleteurl + '/' + id, httpOptions);
    }
    getItemsDetail(id) {
        return this.httpClient.get(this.url + '/' + id, httpOptions);
    }
    insertWarrantItems(manualgrnData): Observable<any> {
        return this.httpClient.post(this.url, manualgrnData, httpOptions);
    }

    updateWarrantItems(manualgrnData, id): Observable<any> {
        return this.httpClient.put(this.url + '/' + id, manualgrnData, httpOptions);
    }
}