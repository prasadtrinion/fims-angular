import { Injectable } from '@angular/core';
import { HttpClientModule, HttpHeaders, HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({
        'X-Auth-Client': 'qazwsx123',
        'Content-Type': 'application/json'
    })

};

@Injectable()

export class ItemsubcategoryService {
    url: string = environment.api.base + environment.api.endPoints.itemsubcategory;
    activeUrl: string = environment.api.base + environment.api.endPoints.activeItemsubcategory;
    subcategoriesUrl: string = environment.api.base + environment.api.endPoints.itemsubcategoriesbycategory;
    supplierItemUrl: string = environment.api.base + environment.api.endPoints.getCategoryItem;
    subcategoryBasedOnCategoryId: string = environment.api.base + environment.api.endPoints.getAssetSubCategoryByAssetCategory;
    procurementSubCategory : string = environment.api.base+environment.api.endPoints.getProcurmentSubCategoriesByProcuremntCategory

    getItemForAssetSubCategory : string = environment.api.base+environment.api.endPoints.getItemForAssetSubCategory;
    getItemForProcurementSubCategory : string = environment.api.base+environment.api.endPoints.getItemForProcurementSubCategory;

    constructor(private httpClient: HttpClient) {

    }

    getItems() {
        return this.httpClient.get(this.url, httpOptions);
    }
    getActiveItems() {
        return this.httpClient.get(this.activeUrl, httpOptions);
    }
    getSupplierItems() {
        return this.httpClient.get(this.supplierItemUrl, httpOptions);
    }
    getProcurmentSubCategoriesByProcuremntCategory(id) {
        return this.httpClient.get(this.procurementSubCategory + '/' + id, httpOptions);
    }
    assetgetSubCategoriesByCategory(id){
        return this.httpClient.get(this.subcategoryBasedOnCategoryId + '/' + id, httpOptions);
    }

    assetItemBySubCategoryId(id){
        return this.httpClient.get(this.getItemForAssetSubCategory + '/' + id, httpOptions);
    }

    getSubCategoriesByCategory(id) {
        return this.httpClient.get(this.procurementSubCategory + '/' + id, httpOptions);
    }

    procurementItemBySubCategoryId(id){
        return this.httpClient.get(this.getItemForProcurementSubCategory + '/' + id, httpOptions);
    }
    getItemsDetail(id) {
        return this.httpClient.get(this.url + '/' + id, httpOptions);
    }

    insertItemsubcategoryItems(itemsubcategoryData): Observable<any> {
        return this.httpClient.post(this.url, itemsubcategoryData, httpOptions);
    }

    updateItemsubcategoryItems(itemsubcategoryData, id): Observable<any> {
        return this.httpClient.put(this.url + '/' + id, itemsubcategoryData, httpOptions);
    }
}