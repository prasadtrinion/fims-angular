import { Injectable } from '@angular/core';
import { HttpClientModule, HttpHeaders, HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({
        'X-Auth-Client': 'qazwsx123',
        'Content-Type': 'application/json'
    })

};
@Injectable()

export class AssetApproverVerificationService {

    url: string = environment.api.base + environment.api.endPoints.grn;
    getExpensetemsUrl: string = environment.api.base + environment.api.endPoints.expenseGrns;
    addAssetUrl: string = environment.api.base + environment.api.endPoints.awaitingGrns;
    getExtendItemsurl: string = environment.api.base + environment.api.endPoints.assetLifeIncrease;
    getIncreaseItemsUrl: string = environment.api.base + environment.api.endPoints.assetAmountIncrease;
    constructor(private httpClient: HttpClient) {
    }
    getItems() {
        return this.httpClient.get(this.url, httpOptions);
    }
    addAssetItems(grnapprovalData) {
        return this.httpClient.post(this.addAssetUrl,grnapprovalData, httpOptions);
    }
    getExpensetems(grnapprovalData) {
        return this.httpClient.post(this.getExpensetemsUrl,grnapprovalData, httpOptions);
    }
    getExtendItems(grnapprovalData): Observable<any> {
        return this.httpClient.post(this.getExtendItemsurl, grnapprovalData, httpOptions);
    }
    getIncreaseItems(grnapprovalData): Observable<any> {
        return this.httpClient.post(this.getIncreaseItemsUrl, grnapprovalData, httpOptions);
    }
}