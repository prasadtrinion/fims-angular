import { Injectable } from '@angular/core';
import { HttpClientModule,HttpHeaders, HttpClient} from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({ 'X-Auth-Client': 'qazwsx123',
    'Content-Type': 'application/json' })
    
  };
  
@Injectable()

export class ItemsetupService {
    url: string = environment.api.base + environment.api.endPoints.itemsetup;
    allitemsurl: string = environment.api.base + environment.api.endPoints.allitems;
    ActiveListurl : string = environment.api.base + environment.api.endPoints.activeItem;

    constructor(private httpClient: HttpClient) { 
        
    }
    
    getItems() {
        return this.httpClient.get(this.url,httpOptions);
    }
    getAllItems() {
        return this.httpClient.get(this.allitemsurl,httpOptions);
    }
    getActiveItems() {
        return this.httpClient.get(this.ActiveListurl,httpOptions);
   }

    getItemsDetail(id) {
        return this.httpClient.get(this.url+'/'+id,httpOptions);
    }

    insertItemsetupItems(itemsetupData): Observable<any> {
        return this.httpClient.post(this.url, itemsetupData,httpOptions);
    }

    updateItemsetupItems(itemsetupData,id): Observable<any> {
        return this.httpClient.put(this.url+'/'+id, itemsetupData,httpOptions);
       }
}