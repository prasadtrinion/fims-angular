import { Injectable } from '@angular/core';
import { HttpClientModule, HttpHeaders, HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({
        'X-Auth-Client': 'qazwsx123',
        'Content-Type': 'application/json'
    })

};
@Injectable()

export class GrnapprovalService {

    url: string = environment.api.base + environment.api.endPoints.getGrnApprovalList
    grnUrl: string = environment.api.base + environment.api.endPoints.grn
    approvalPuturl: string = environment.api.base + environment.api.endPoints.GrnApproval
    detailApproveUrl: string = environment.api.base + environment.api.endPoints.GrnDetailApproval
    constructor(private httpClient: HttpClient) {
    }
    getItems() {
        return this.httpClient.get(this.url, httpOptions);
    }
    getItemsDetail(id) {
        return this.httpClient.get(this.url + '/' + id, httpOptions);
    }
    getlistItems() {
        return this.httpClient.get(this.url + '/' + 0, httpOptions);
    }
    insertGrnapprovalItems(grnapprovalData): Observable<any> {
        return this.httpClient.post(this.url, grnapprovalData, httpOptions);
    }
    updateGrnapprovalItems(grnapprovalData): Observable<any> {
        return this.httpClient.post(this.detailApproveUrl, grnapprovalData, httpOptions);
    }
}