import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SupplierregistrationService } from '../service/supplierregistration.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'Supplierregistration',
    templateUrl: 'supplierregistration.component.html'
})
export class SupplierregistrationComponent implements OnInit {
    supplierList = [];
    supplierData = {};
    id: number;
    constructor(
        private SupplierregistrationService: SupplierregistrationService,
        private spinner: NgxSpinnerService,
        

    ) { }

    ngOnInit() {
        this.spinner.show();        
        this.SupplierregistrationService.getItems().subscribe(
            data => {
                this.spinner.hide();
                let activityData = [];
                activityData = data['result']['data'];
                console.log("asdf");
                for (var i = 0; i < activityData.length; i++) {
                    if (activityData[i]['f030fapprovalStatus'] == 0) {
                        activityData[i]['f030fapprovalStatus'] = 'Pending';
                    } else if (activityData[i]['f030fapprovalStatus'] == 1) {
                        activityData[i]['f030fapprovalStatus'] = 'Approved';
                    } else {
                        activityData[i]['f030fapprovalStatus'] = 'Rejected';
                    }
                }
                this.supplierList = activityData;
            }, error => {
                console.log(error);
            });
    }
}