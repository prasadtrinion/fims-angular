import { Injector } from '@angular/core';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SupplierregistrationService } from '../../service/supplierregistration.service';
import { CountryService } from '../../../generalsetup/service/country.service';
import { StateService } from '../../../generalsetup/service/state.service';
import { LicenseService } from '../../service/license.service';
import { BankService } from '../../../generalsetup/service/bank.service';
import { AccountcodeService } from '../../../generalsetup/service/accountcode.service';
import { CustomerService } from "../../../accountsrecivable/service/customer.service";
import { ItemcategoryService } from '../../service/itemcategory.service';
import { ItemsubcategoryService } from '../../service/itemsubcategory.service';
import { HttpClientModule, HttpHeaders, HttpClient } from '@angular/common/http';
import { DefinationmsService } from '../../../studentfinance/service/definationms.service';
import { MofcategoryService } from "../../service/mofcategory.service";
import { environment } from '../../../../environments/environment';
import { NgxSpinnerService } from 'ngx-spinner';
import { INVALID } from '@angular/forms/src/model';
import { valid } from 'semver';

@Component({
    selector: 'SupplierregistrationFormComponent',
    templateUrl: 'supplierregistrationform.component.html'
})

export class SupplierregistrationFormComponent implements OnInit {

    vendorList = [];
    vendorData = {};
    mofDataList = [];
    contactDataList = [];
    gradeDataList = [];
    vendorLicenseData = {};
    NatureDataList = [];
    licenseDataList = [];
    accountcodeList = [];
    vendorgradeData = {};
    vendorDataHeader = {};
    vendorNatureData = {};
    countryList = [];
    vendormofData = {};
    ContactData = {};
    stateList = [];
    licenseList = [];
    bankList = [];
    deleteList = [];
    deleteList1 = [];
    deleteList2 = [];
    deleteList3 = [];
    debtorcodelist = [];
    paytoProfileList = [];
    paytoProfileData = {};
    itemcategoryList = [];
    itemsubcategoryList = [];
    statusList = [];
    vendorOwnerData = {};
    vendorOwnerList = [];
    raceList = [];
    idtypeList = [];
    mofcategoryList = [];
    ajaxcount = 0;
    showLastRow: boolean;
    showLastRowMinus: boolean;
    showLastRowPlus: boolean;
    listshowLastRowMinus: boolean;
    listshowLastRowPlus: boolean;
    showLastRow1: boolean;
    showLastRowMinus1: boolean;
    showLastRowPlus1: boolean;
    listshowLastRowMinus1: boolean;
    listshowLastRowPlus1: boolean;
    showLastRow2: boolean;
    showLastRowMinus2: boolean;
    showLastRowPlus2: boolean;
    listshowLastRowMinus2: boolean;
    listshowLastRowPlus2: boolean;
    showLastRow3: boolean;
    showLastRowMinus3: boolean;
    showLastRowPlus3: boolean;
    listshowLastRowMinus3: boolean;
    listshowLastRowPlus3: boolean;
    showLastRow4: boolean;
    showLastRowMinus4: boolean;
    showLastRowPlus4: boolean;
    listshowLastRowMinus4: boolean;
    listshowLastRowPlus4: boolean;
    showLastRow5: boolean;
    showLastRowMinus5: boolean;
    showLastRowPlus5: boolean;
    listshowLastRowMinus5: boolean;
    listshowLastRowPlus5: boolean;
    id: number;
    idview: number;
    ajaxCount: number;
    viewDisabled: boolean;
    saveBtnDisable: boolean;
    file: any;
    edit: boolean = false;
    downloadUrl: string = environment.api.filebase;
    constructor(

        private SupplierregistrationService: SupplierregistrationService,
        private CountryService: CountryService,
        private LicenseService: LicenseService,
        private StateService: StateService,
        private BankService: BankService,
        private AccountcodeService: AccountcodeService,
        private CustomerService: CustomerService,
        private ItemcategoryService: ItemcategoryService,
        private ItemsubcategoryService: ItemsubcategoryService,
        private DefinationmsService: DefinationmsService,
        private MofcategoryService: MofcategoryService,
        private route: ActivatedRoute,
        private spinner: NgxSpinnerService,
        private router: Router,
        private httpClient: HttpClient


    ) { }


    ngDoCheck() {

        this.route.paramMap.subscribe((data) => this.idview = + data.get('idview'));
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.ajaxCount == 0) {
            this.spinner.hide();
        }
    }
    showIcons() {
        // this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.contactDataList.length > 0) {
            this.showLastRowMinus = true;
            this.showLastRowPlus = true;
            this.listshowLastRowMinus = true;
        }

        if (this.contactDataList.length > 1) {
            if (this.showLastRow == false) {
                this.listshowLastRowPlus = false;
                this.listshowLastRowMinus = true;
                this.showLastRowPlus = true;

            } else {
                this.listshowLastRowPlus = true;
                this.listshowLastRowMinus = false;

            }
        }
        if (this.contactDataList.length == 1 && this.showLastRow == false) {
            this.listshowLastRowMinus = true;
            this.listshowLastRowPlus = false;

        }
        if (this.contactDataList.length == 1 && this.showLastRow == true) {
            this.listshowLastRowMinus = false;
            this.listshowLastRowPlus = true;

        }
        if (this.contactDataList.length == 0 && this.showLastRow == false) {
            this.showLastRowMinus = false;
            this.showLastRowPlus = true;
        }
        if (this.contactDataList.length == 0 && this.showLastRow == true) {
            this.showLastRowMinus = true;
            this.showLastRowPlus = false;
        }
        if (this.viewDisabled == true || this.idview == 2) {
            this.listshowLastRowPlus = false;
        }

        console.log(this.contactDataList.length);
        console.log("Show last row" + this.showLastRow);
        console.log("showLastRowMinus" + this.showLastRowMinus);
        console.log("showLastRowPlus" + this.showLastRowPlus);
        console.log("listshowLastRowMinus" + this.listshowLastRowMinus);
        console.log("listshowLastRowPlus" + this.listshowLastRowPlus);
    }
    deletemodule() {
        this.showLastRow = true;
        this.showIcons();
    }
    showNewRow() {
        var confirmPop = confirm("Do you want to Add?");
        if (confirmPop == false) {
            return false;
        }
        this.showLastRow = false;
        this.showIcons();
    }

    showIcons1() {
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        if (this.gradeDataList.length > 0) {
            this.showLastRowMinus1 = true;
            this.showLastRowPlus1 = true;
            this.listshowLastRowMinus1 = true;
        }

        if (this.gradeDataList.length > 1) {
            if (this.showLastRow1 == false) {
                this.listshowLastRowPlus1 = false;
                this.listshowLastRowMinus1 = true;
                this.showLastRowPlus1 = true;

            } else {
                this.listshowLastRowPlus1 = true;
                this.listshowLastRowMinus1 = false;

            }
        }
        if (this.gradeDataList.length == 1 && this.showLastRow1 == false) {
            this.listshowLastRowMinus1 = true;
            this.listshowLastRowPlus1 = false;

        }
        if (this.gradeDataList.length == 1 && this.showLastRow1 == true) {
            this.listshowLastRowMinus1 = false;
            this.listshowLastRowPlus1 = true;

        }
        if (this.gradeDataList.length == 0 && this.showLastRow1 == false) {
            this.showLastRowMinus1 = false;
            this.showLastRowPlus1 = true;
        }
        if (this.gradeDataList.length == 0 && this.showLastRow1 == true) {
            this.showLastRowMinus1 = true;
            this.showLastRowPlus1 = false;
        }
        if (this.viewDisabled == true || this.idview == 2) {
            this.listshowLastRowPlus1 = false;
        }
        console.log(this.gradeDataList.length);
        console.log("Show last row" + this.showLastRow1);
        console.log("showLastRowMinus" + this.showLastRowMinus1);
        console.log("showLastRowPlus" + this.showLastRowPlus1);
        console.log("listshowLastRowMinus" + this.listshowLastRowMinus1);
        console.log("listshowLastRowPlus" + this.listshowLastRowPlus1);
    }
    showNewRow1() {
        var confirmPop = confirm("Do you want to Add?");
        if (confirmPop == false) {
            return false;
        }
        this.showLastRow1 = false;
        this.showIcons1();
    }
    deletemodule1() {


        this.showLastRow1 = true;
        this.showIcons1();
    }

    showIcons2() {
        if (this.NatureDataList.length > 0) {
            this.showLastRowMinus2 = true;
            this.showLastRowPlus2 = true;
            this.listshowLastRowMinus2 = true;
        }

        if (this.NatureDataList.length > 1) {
            if (this.showLastRow2 == false) {
                this.listshowLastRowPlus2 = false;
                this.listshowLastRowMinus2 = true;
                this.showLastRowPlus2 = true;

            } else {
                this.listshowLastRowPlus2 = true;
                this.listshowLastRowMinus2 = false;

            }
        }
        if (this.NatureDataList.length == 1 && this.showLastRow2 == false) {
            this.listshowLastRowMinus2 = true;
            this.listshowLastRowPlus2 = false;

        }
        if (this.NatureDataList.length == 1 && this.showLastRow2 == true) {
            this.listshowLastRowMinus2 = false;
            this.listshowLastRowPlus2 = true;

        }
        if (this.NatureDataList.length == 0 && this.showLastRow2 == false) {
            this.showLastRowMinus2 = false;
            this.showLastRowPlus2 = true;
        }
        if (this.NatureDataList.length == 0 && this.showLastRow2 == true) {
            this.showLastRowMinus2 = true;
            this.showLastRowPlus2 = false;
        }
        if (this.viewDisabled == true || this.idview == 2) {
            this.listshowLastRowPlus2 = false;
        }
        console.log(this.NatureDataList.length);
        console.log("Show last row" + this.showLastRow2);
        console.log("showLastRowMinus" + this.showLastRowMinus2);
        console.log("showLastRowPlus" + this.showLastRowPlus2);
        console.log("listshowLastRowMinus" + this.listshowLastRowMinus2);
        console.log("listshowLastRowPlus" + this.listshowLastRowPlus2);
    }
    showNewRow2() {
        var confirmPop = confirm("Do you want to Add?");
        if (confirmPop == false) {
            return false;
        }
        this.showLastRow2 = false;
        this.showIcons2();
    }
    deletemodule2() {
        this.showLastRow2 = true;
        this.showIcons2();
    }
    showIcons3() {

        if (this.mofDataList.length > 0) {
            this.showLastRowMinus3 = true;
            this.showLastRowPlus3 = true;
            this.listshowLastRowMinus3 = true;
        }

        if (this.mofDataList.length > 1) {
            if (this.showLastRow3 == false) {
                this.listshowLastRowPlus3 = false;
                this.listshowLastRowMinus3 = true;
                this.showLastRowPlus3 = true;

            } else {
                this.listshowLastRowPlus3 = true;
                this.listshowLastRowMinus3 = false;

            }
        }
        if (this.mofDataList.length == 1 && this.showLastRow3 == false) {
            this.listshowLastRowMinus3 = true;
            this.listshowLastRowPlus3 = false;

        }
        if (this.mofDataList.length == 1 && this.showLastRow3 == true) {
            this.listshowLastRowMinus3 = false;
            this.listshowLastRowPlus3 = true;

        }
        if (this.mofDataList.length == 0 && this.showLastRow3 == false) {
            this.showLastRowMinus3 = false;
            this.showLastRowPlus3 = true;
        }
        if (this.mofDataList.length == 0 && this.showLastRow3 == true) {
            this.showLastRowMinus3 = true;
            this.showLastRowPlus3 = false;
        }
        if (this.viewDisabled == true || this.idview == 2) {
            this.listshowLastRowPlus3 = false;
        }
        console.log(this.mofDataList.length);
        console.log("Show last row" + this.showLastRow3);
        console.log("showLastRowMinus" + this.showLastRowMinus3);
        console.log("showLastRowPlus" + this.showLastRowPlus3);
        console.log("listshowLastRowMinus" + this.listshowLastRowMinus3);
        console.log("listshowLastRowPlus" + this.listshowLastRowPlus3);
    }
    showNewRow3() {
        var confirmPop = confirm("Do you want to Add?");
        if (confirmPop == false) {
            return false;
        }
        this.showLastRow3 = false;
        this.showIcons3();
    }
    deletemodule3() {
        this.showLastRow3 = true;
        this.showIcons3();
    }
    showIcons4() {

        if (this.paytoProfileList.length > 0) {
            this.showLastRowMinus4 = true;
            this.showLastRowPlus4 = true;
            this.listshowLastRowMinus4 = true;
        }

        if (this.paytoProfileList.length > 1) {
            if (this.showLastRow4 == false) {
                this.listshowLastRowPlus4 = false;
                this.listshowLastRowMinus4 = true;
                this.showLastRowPlus4 = true;

            } else {
                this.listshowLastRowPlus4 = true;
                this.listshowLastRowMinus4 = false;

            }
        }
        if (this.paytoProfileList.length == 1 && this.showLastRow4 == false) {
            this.listshowLastRowMinus4 = true;
            this.listshowLastRowPlus4 = false;

        }
        if (this.paytoProfileList.length == 1 && this.showLastRow4 == true) {
            this.listshowLastRowMinus4 = false;
            this.listshowLastRowPlus4 = true;

        }
        if (this.paytoProfileList.length == 0 && this.showLastRow4 == false) {
            this.showLastRowMinus4 = false;
            this.showLastRowPlus4 = true;
        }
        if (this.paytoProfileList.length == 0 && this.showLastRow4 == true) {
            this.showLastRowMinus4 = true;
            this.showLastRowPlus4 = false;
        }
        if (this.viewDisabled == true || this.idview == 2) {
            this.listshowLastRowPlus4 = false;
        }
    }
    showNewRow4() {
        var confirmPop = confirm("Do you want to Add?");
        if (confirmPop == false) {
            return false;
        }
        this.showLastRow4 = false;
        this.showIcons4();
    }
    deletemodule4() {
        this.showLastRow4 = true;
        this.showIcons4();
    }
    showIcons5() {

        if (this.vendorOwnerList.length > 0) {
            this.showLastRowMinus5 = true;
            this.showLastRowPlus5 = true;
            this.listshowLastRowMinus5 = true;
        }

        if (this.vendorOwnerList.length > 1) {
            if (this.showLastRow5 == false) {
                this.listshowLastRowPlus5 = false;
                this.listshowLastRowMinus5 = true;
                this.showLastRowPlus5 = true;

            } else {
                this.listshowLastRowPlus5 = true;
                this.listshowLastRowMinus5 = false;

            }
        }
        if (this.vendorOwnerList.length == 1 && this.showLastRow5 == false) {
            this.listshowLastRowMinus5 = true;
            this.listshowLastRowPlus5 = false;

        }
        if (this.vendorOwnerList.length == 1 && this.showLastRow5 == true) {
            this.listshowLastRowMinus5 = false;
            this.listshowLastRowPlus5 = true;

        }
        if (this.vendorOwnerList.length == 0 && this.showLastRow5 == false) {
            this.showLastRowMinus5 = false;
            this.showLastRowPlus5 = true;
        }
        if (this.vendorOwnerList.length == 0 && this.showLastRow5 == true) {
            this.showLastRowMinus5 = true;
            this.showLastRowPlus5 = false;
        }
        if (this.viewDisabled == true || this.idview == 2) {
            this.listshowLastRowPlus4 = false;
        }
    }
    showNewRow5() {
        var confirmPop = confirm("Do you want to Add?");
        if (confirmPop == false) {
            return false;
        }
        this.showLastRow5 = false;
        this.showIcons5();
    }
    deletemodule5() {
        this.showLastRow5 = true;
        this.showIcons5();
    }
    ngOnInit() {
        this.ajaxCount = 0;
        this.spinner.show();
        this.viewDisabled = false;
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.route.paramMap.subscribe((data) => this.idview = + data.get('idview'));
        this.DefinationmsService.getScheme('UserStatus').subscribe(
            data => {

                this.statusList = data['result'];
            }, error => {
                console.log(error);
            });
        this.DefinationmsService.getScheme('Race').subscribe(
            data => {

                this.raceList = data['result'];
            }, error => {
                console.log(error);
            });
        if (this.id > 0) {
            this.ajaxCount++;
            this.edit = true;
            console.log("id");
            this.SupplierregistrationService.getItemsDetail(this.id).subscribe(
                data => {
                    this.ajaxCount--;
                    this.licenseDataList = data['result'];
                    this.vendorDataHeader['f030fcompanyName'] = data['result']['f030fcompanyName'];
                    this.vendorDataHeader['f030fnumberReceipt'] = data['result']['f030fnumberReceipt'];
                    this.vendorDataHeader['f030fpayToProfile'] = data['result']['f030fpayToProfile'];
                    this.vendorDataHeader['f030faddress1'] = data['result']['f030faddress1'];
                    this.vendorDataHeader['f030faddress2'] = data['result']['f030faddress2'];
                    this.vendorDataHeader['f030faddress3'] = data['result']['f030faddress3'];
                    this.vendorDataHeader['f030femail'] = data['result']['f030femail'];
                    this.vendorDataHeader['f030faddress'] = data['result']['f030faddress'];
                    this.vendorDataHeader['f030fphone'] = data['result']['f030fphone'];
                    this.vendorDataHeader['f030fvendorCode'] = data['result']['f030fvendorCode'];
                    this.vendorDataHeader['f030fstartDate'] = data['result']['f030fstartDate'];
                    this.vendorDataHeader['f030fendDate'] = data['result']['f030fendDate'];
                    this.vendorDataHeader['f030fcity'] = data['result']['f030fcity'];
                    this.vendorDataHeader['f030fpostCode'] = data['result']['f030fpostCode'];
                    this.vendorDataHeader['f030fstate'] = data['result']['f030fstate'];
                    this.vendorDataHeader['f030fcountry'] = data['result']['f030fcountry'];
                    this.vendorDataHeader['f030fwebsite'] = data['result']['f030fwebsite'];
                    this.vendorDataHeader['f030fpayToProfile'] = data['result']['f030fpayToProfile'];
                    this.vendorDataHeader['f030fdebtorCode'] = parseInt(data['result']['f030fdebtorCode']);
                    this.vendorDataHeader['f030fidBank'] = data['result']['f030fidBank'];
                    this.vendorDataHeader['f030faccountNumber'] = data['result']['f030faccountNumber'];

                    this.vendorDataHeader['f030faccountCode'] = data['result']['f030faccountCode'];
                    this.vendorDataHeader['f030faccountNo'] = data['result']['f030faccountNo'];
                    this.vendorDataHeader['f030fcompanyRegNo'] = data['result']['f030fcompanyRegNo'];
                    this.vendorDataHeader['f030ffax'] = data['result']['f030ffax'];
                    this.vendorDataHeader['f030frace'] = data['result']['f030frace'];
                    this.vendorDataHeader['f030fuserStatus'] = data['result']['f030fuserStatus'];
                    this.contactDataList = data['result']['contact-details'];
                    this.gradeDataList = data['result']['license-details'];
                    this.NatureDataList = data['result']['nature-details'];
                    this.vendorNatureData['f113fcategory'] = parseInt(data['result']['nature-details'][0]['f113fcategory']);
                    this.vendorNatureData['f113fsubCategory'] = parseInt(data['result']['nature-details'][0]['f113fsubCategory']);
                    this.mofDataList = data['result']['mof-details'];
                    this.vendormofData['f115fmodeOfCategory'] = data['result']['mof-details'][0]['f115fmodeOfCategory'];
                    this.paytoProfileList = data['result']['payto-profile'];
                    this.vendorOwnerList = data['result']['owner-details'];
                    if (data['result']['f030fapprovalStatus'] == 1 || this.licenseDataList['f030fapprovalStatus'] == 2) {
                        this.viewDisabled = true;
                        this.listshowLastRowPlus = false;
                        this.listshowLastRowPlus1 = false;
                        this.listshowLastRowPlus2 = false;
                        this.listshowLastRowPlus3 = false;
                        this.listshowLastRowPlus5 = false;
                        $("#target input,select").prop("disabled", true);
                        $("#target1 input,select").prop("disabled", true);
                        $("#target2 input,select").prop("disabled", true);
                        $("#target3 input,select").prop("disabled", true);
                        $("#target4 input,select").prop("disabled", true);
                        $("#target5 input,select").prop("disabled", true);
                    }
                    if (this.idview == 2) {
                        this.viewDisabled = true;
                        this.listshowLastRowPlus1 = false;
                        this.listshowLastRowPlus2 = false;
                        this.listshowLastRowPlus = false;
                        this.listshowLastRowPlus3 = false;
                        this.listshowLastRowPlus5 = false;
                        $("#target input,select").prop("disabled", true);
                        $("#target1 input,select").prop("disabled", true);
                        $("#target2 input,select").prop("disabled", true);
                        $("#target3 input,select").prop("disabled", true);
                        $("#target4 input,select").prop("disabled", true);
                        $("#target5 input,select").prop("disabled", true);
                    }
                    this.showIcons();
                    this.showIcons1();
                    this.showIcons2();
                    this.showIcons3();
                    this.showIcons4();
                    this.showIcons5();
                    this.getSubCategories();
                }, error => {
                    console.log(error);
                });
        }
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id < 1) {
            this.showLastRow = false;
            this.showLastRowMinus = false;
            this.showLastRowPlus = true;
        } else {
            this.showLastRow = true;

        }
        if (this.id < 1) {
            this.
                showLastRow1 = false;
            this.showLastRowMinus1 = false;
            this.showLastRowPlus1 = true;
        } else {
            this.showLastRow1 = true;

        }
        if (this.id < 1) {
            this.showLastRow2 = false;
            this.showLastRowMinus2 = false;
            this.showLastRowPlus2 = true;
        } else {
            this.showLastRow2 = true;

        }
        if (this.id < 1) {
            this.showLastRow3 = false;
            this.showLastRowMinus3 = false;
            this.showLastRowPlus3 = true;
        } else {
            this.showLastRow3 = true;

        }
        if (this.id < 1) {
            this.showLastRow4 = false;
            this.showLastRowMinus4 = false;
            this.showLastRowPlus4 = true;
        } else {
            this.showLastRow4 = true;

        }
        if (this.id < 1) {
            this.showLastRow5 = false;
            this.showLastRowMinus5 = false;
            this.showLastRowPlus5 = true;
        } else {
            this.showLastRow5 = true;

        }

        let idtypeObj = {};
        idtypeObj['name'] = "IC Number";
        this.idtypeList.push(idtypeObj);
        idtypeObj = {};
        idtypeObj['name'] = "Passport";
        this.idtypeList.push(idtypeObj);

        this.getVendorCode();
        this.vendorLicenseData['f031fstatus'] = 1;
        this.vendorDataHeader['f030fstatus'] = 1;
        //country dropdown
        this.ajaxCount++;
        this.CountryService.getActiveCountryList().subscribe(
            data => {
                this.ajaxCount--;
                this.countryList = data['result']['data'];
            }
        );

        //state dropdown
        this.ajaxCount++;
        this.StateService.getActiveStateList().subscribe(
            data => {
                this.ajaxCount--;
                this.stateList = data['result']['data'];
            }
        );


        //License dropdown
        this.ajaxCount++;
        this.LicenseService.getActiveItems().subscribe(
            data => {
                this.ajaxCount--;
                this.licenseList = data['result'];
            }
        );

        //Bank dropdown
        this.ajaxCount++;
        this.BankService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.bankList = data['result']['data'];
            }
        );
        //Account dropdown
        this.ajaxCount++;
        this.AccountcodeService.getItemsByLevel().subscribe(
            data => {
                this.ajaxCount--;
                this.accountcodeList = data['result']['data'];
            }
        );
        //Debtor code dropdown
        this.ajaxCount++;
        this.CustomerService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.debtorcodelist = data['result']['data'];
            });
        //Category dropdown
        //item category
        this.ajaxCount++;
        this.ItemcategoryService.getActiveItems().subscribe(
            data => {
                this.ajaxCount--;
                this.itemcategoryList = data['result']['data'];
            }, error => {
                console.log(error);
            });

        //subCategory
        // this.ajaxCount++;
        // this.ItemsubcategoryService.getActiveItems().subscribe(
        //     data => {
        //         this.ajaxCount--;
        //         this.itemsubcategoryList = data['result']['data'];
        //     }, error => {
        //         console.log(error);
        //     });
        //MOF Category
        this.ajaxCount++;
        this.MofcategoryService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.mofcategoryList = data['result']['data'];
            }, error => {
                console.log(error);
            });

        this.showIcons();
        this.showIcons1();
        this.showIcons2();
        this.showIcons3();
    }
    deletecontactDataList(object) {
        var cnf = confirm("Do you really want to delete");
        if (cnf == true) {

            var index = this.contactDataList.indexOf(object);
            this.deleteList.push(this.contactDataList[index]['f112fid']);
            let deleteObject = {};
            deleteObject['id'] = this.deleteList;
            this.SupplierregistrationService.deleteContactItems(deleteObject).subscribe(
                data => {
                }, error => {
                    console.log(error);
                });
            if (index > -1) {
                this.contactDataList.splice(index, 1);
            }
            this.showIcons1();
        }
    }

    deletegradeDataList(object) {
        var cnf = confirm("Do you really want to delete");
        if (cnf == true) {
            var index = this.gradeDataList.indexOf(object);
            this.deleteList1.push(this.gradeDataList[index]['f031fid']);
            let deleteObject = {};
            deleteObject['id'] = this.deleteList1;
            this.ajaxCount++;
            this.SupplierregistrationService.deleteLicenseItems(deleteObject).subscribe(
                data => {
                    this.ajaxCount--;
                }, error => {
                    console.log(error);
                });
            if (index > -1) {
                this.gradeDataList.splice(index, 1);
            }
            this.showIcons1();
        }
    }
    getSubCategories() {
        //  Sub cAt
        this.ajaxCount++;
        this.ItemsubcategoryService.getSubCategoriesByCategory(this.vendorNatureData['f113fcategory']).subscribe(
            data => {
                this.ajaxCount--;
                console.log("asdfasdfsub cat");
                this.itemsubcategoryList = data['result'];
            }, error => {
                console.log(error);
            });

    }
    deleteNatureDataList(object) {
        var cnf = confirm("Do you really want to delete");
        if (cnf == true) {
            var index = this.NatureDataList.indexOf(object);
            this.deleteList2.push(this.NatureDataList[index]['f113fid']);
            let deleteObject = {};
            deleteObject['id'] = this.deleteList2;
            this.SupplierregistrationService.deleteNaturalItems(deleteObject).subscribe(
                data => {
                }, error => {
                    console.log(error);
                });
            if (index > -1) {
                this.NatureDataList.splice(index, 1);
            }
            this.showIcons2();
        }
    }
    deletemofDataList(object) {
        var cnf = confirm("Do you really want to delete");
        if (cnf == true) {
            var index = this.mofDataList.indexOf(object);
            this.deleteList3.push(this.mofDataList[index]['f115fid']);
            let deleteObject = {};
            deleteObject['id'] = this.deleteList3;
            this.SupplierregistrationService.deleteMofItems(deleteObject).subscribe(
                data => {
                }, error => {
                    console.log(error);
                });
            if (index > -1) {
                this.mofDataList.splice(index, 1);
            }
            this.showIcons3();
        }
    }
    deletePaytoProfileList(object) {
        var cnf = confirm("Do you really want to delete");
        if (cnf == true) {
            var index = this.mofDataList.indexOf(object);
            this.deleteList3.push(this.mofDataList[index]['f127fid']);
            let deleteObject = {};
            deleteObject['id'] = this.deleteList3;
            this.SupplierregistrationService.deletePaytoProfileList(deleteObject).subscribe(
                data => {
                }, error => {
                    console.log(error);
                });
            if (index > -1) {
                this.mofDataList.splice(index, 1);
            }
            this.showIcons4();
        }
    }
    deletevendorOwnerDataList(object) {
        var cnf = confirm("Do you really want to delete");
        if (cnf == true) {
            var index = this.vendorOwnerList.indexOf(object);
            this.deleteList1.push(this.vendorOwnerList[index]['f031fid']);
            let deleteObject = {};
            deleteObject['id'] = this.deleteList1;
            this.ajaxCount++;
            // this.SupplierregistrationService.deleteLicenseItems(deleteObject).subscribe(
            //     data => {
            //         this.ajaxCount--;
            //     }, error => {
            //         console.log(error);
            //     });
            if (index > -1) {
                this.vendorOwnerList.splice(index, 1);
            }
            this.showIcons5();
        }
    }
    getVendorCode() {

        let typeObject = {};
        typeObject['type'] = "Vendor"
        this.ajaxCount++;
        this.SupplierregistrationService.getCode(typeObject).subscribe(
            data => {
                this.ajaxCount--;
                this.vendorDataHeader['f030fvendorCode'] = data['number'];
            }
        );
    }
    ContactkeyPress(event) {
        const pattern = /[0-9\+\ ]/;
        let inputChar = String.fromCharCode(event.charCode);
        if (event.keyCode != 8 && !pattern.test(inputChar)) {
            event.preventDefault();
        }
        if (pattern == undefined) {
            alert("Enter valid Phone Number");
        }
    }
    EmailkeyPress(email) {
        var reg = /[^@\s]+@[^@\s]+\.[^@\s]+/;
        if (reg.test(email) == false) {
            alert('Invalid Email Address');
            this.vendorDataHeader['f030femail'] = '';
            return false;
        }
        return true;
    }
    dateComparison() {
        // var newdate = new Date();
        var today = new Date().getTime();
        var startdate = new Date(this.vendorDataHeader['f030fstartDate']).getTime();
        if (startdate > today) {
            alert("Start Date should less than Current Date");
            this.vendorDataHeader['f030fstartDate'] = '';
        }
    }

    saveVendorData() {
        if (this.vendorDataHeader['f030fcompanyRegNo'] == undefined || this.vendorDataHeader['f030fcompanyRegNo'] == '') {
            alert('Enter Company Registartion Number');
            return false;
        }
        if (this.vendorDataHeader['f030fcompanyRegNo'] == undefined || this.vendorDataHeader['f030fcompanyRegNo'] == '') {
            alert('Enter Company Registartion Number');
            return false;
        }
        if (this.vendorDataHeader['f030fcompanyName'] == undefined || this.vendorDataHeader['f030fcompanyName'] == '') {
            alert('Enter Vendor Name');
            return false;
        }
        if (this.vendorDataHeader['f030femail'] == undefined || this.vendorDataHeader['f030femail'] == '') {
            alert('Enter Email');
            return false;
        }
        if (this.vendorDataHeader['f030faddress1'] == undefined || this.vendorDataHeader['f030faddress1'] == '') {
            alert('Enter Address1');
            return false;
        }
        // if (this.vendorDataHeader['f030faddress2'] == undefined) {
        //     alert('Enter Address2');
        //     return false;
        // }
        // if (this.vendorDataHeader['f030faddress3'] == undefined) {
        //     alert('Enter Address3');
        //     return false;
        // }
        if (this.vendorDataHeader['f030fpostCode'] == undefined || this.vendorDataHeader['f030fpostCode'] == '') {
            alert('Enter Post Code');
            return false;
        }
        if (this.vendorDataHeader['f030fcountry'] == undefined || this.vendorDataHeader['f030fcountry'] == '') {
            alert('Select Country');
            return false;
        }
        if (this.vendorDataHeader['f030fstate'] == undefined || this.vendorDataHeader['f030fstate'] == '') {
            alert('Select State');
            return false;
        }
        if (this.vendorDataHeader['f030fcity'] == undefined || this.vendorDataHeader['f030fcity'] == '') {
            alert('Enter City');
            return false;
        }
        // if (this.vendorDataHeader['f030fwebsite'] == undefined) {
        //     alert('Enter Website');
        //     return false;
        // }
        if (this.vendorDataHeader['f030fstartDate'] == undefined || this.vendorDataHeader['f030fstartDate'] == '') {
            alert('Select Start Date');
            return false;
        }
        if (this.vendorDataHeader['f030fphone'] == undefined || this.vendorDataHeader['f030fphone'] == '') {
            alert('Enter Phone Number');
            return false;
        }

        if (this.vendorDataHeader['f030faccountNo'] == undefined || this.vendorDataHeader['f030faccountNo'] == '') {
            alert('Enter Account Number');
            return false;
        }
        if (this.vendorDataHeader['f030frace'] == undefined || this.vendorDataHeader['f030frace'] == '') {
            alert('Select Bumiputra Status');
            return false;
        }
        if (this.vendorDataHeader['f030fuserStatus'] == undefined || this.vendorDataHeader['f030fuserStatus'] == '') {
            alert('Select Status');
            return false;
        }

        // if (this.vendorDataHeader['f030fpayToProfile'] == undefined) {
        //     alert('Enter Pay to Profile');
        //     return false;
        // }

        if (this.vendorDataHeader['f030fidBank'] == undefined || this.vendorDataHeader['f030fidBank'] == '') {
            alert('Select Bank');
            return false;
        }

        if (this.edit == false) {
            if (this.file == undefined) {
                alert('Select a file to upload');
                return false;
            }
        }
        if (this.showLastRow == false) {
            var addactivities = this.addcontactDataList(1);
            if (addactivities == false) {
                return false;
            }
        }
        if (this.showLastRow1 == false) {
            var addactivities = this.addgradeDataList(2);
            if (addactivities == false) {
                return false;
            }
        }
        if (this.showLastRow2 == false) {
            var addactivities = this.addnaturalDataList(3);
            if (addactivities == false) {
                return false;
            }
        }
        if (this.showLastRow3 == false) {
            var addactivities = this.addmofDataList(4);
            if (addactivities == false) {
                return false;
            }
        }
        if (this.showLastRow4 == false) {
            var addactivities = this.addPaytoProfileList(5);
            if (addactivities == false) {
                return false;
            }
        }
        if (this.showLastRow5 == false) {
            var addactivities = this.addVendorOwnerDataList(6);
            if (addactivities == false) {
                return false;
            }
        }
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        let VendorObject = {};
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        console.log(this.id);

        VendorObject['f030faddress4'] = this.vendorDataHeader['f030faddress4'];
        VendorObject['f030faddress3'] = this.vendorDataHeader['f030faddress3'];
        VendorObject['f030faddress2'] = this.vendorDataHeader['f030faddress2'];
        VendorObject['f030faddress1'] = this.vendorDataHeader['f030faddress1'];
        VendorObject['f030fcompanyName'] = this.vendorDataHeader['f030fcompanyName'];
        VendorObject['f030femail'] = this.vendorDataHeader['f030femail'];
        VendorObject['f030faddress'] = this.vendorDataHeader['f030faddress'];
        VendorObject['f030fphone'] = this.vendorDataHeader['f030fphone'];
        VendorObject['f030fstartDate'] = this.vendorDataHeader['f030fstartDate'];
        VendorObject['f030fendDate'] = this.vendorDataHeader['f030fendDate'];
        VendorObject['f030fcity'] = this.vendorDataHeader['f030fcity'];
        VendorObject['f030fpostCode'] = this.vendorDataHeader['f030fpostCode'];
        VendorObject['f030fstate'] = this.vendorDataHeader['f030fstate'];
        VendorObject['f030fcountry'] = this.vendorDataHeader['f030fcountry'];
        VendorObject['f030fwebsite'] = this.vendorDataHeader['f030fwebsite'];
        VendorObject['f030fcontact1Name'] = this.vendorDataHeader['f030fcontact1Name'];
        VendorObject['f030fcontact1Email'] = this.vendorDataHeader['f030fcontact1Email'];
        VendorObject['f030fcontact1Phone'] = this.vendorDataHeader['f030fcontact1Phone'];
        VendorObject['f030fcontact2Name'] = this.vendorDataHeader['f030fcontact2Name'];
        VendorObject['f030fcontact2Email'] = this.vendorDataHeader['f030fcontact2Email'];
        VendorObject['f030fcontact2Phone'] = this.vendorDataHeader['f030fcontact2Phone'];
        VendorObject['f030fidBank'] = this.vendorDataHeader['f030fidBank'];
        VendorObject['f030ffax'] = this.vendorDataHeader['f030ffax'];
        VendorObject['f030fvendorCode'] = this.vendorDataHeader['f030fvendorCode'];
        VendorObject['f030fnumberReceipt'] = this.vendorDataHeader['f030fnumberReceipt'];
        VendorObject['f030fpayToProfile'] = this.vendorDataHeader['f030fpayToProfile'];
        VendorObject['f030fstartDate'] = this.vendorDataHeader['f030fstartDate'];
        VendorObject['f030fendDate'] = this.vendorDataHeader['f030fendDate'];
        VendorObject['f030fdebtorCode'] = this.vendorDataHeader['f030fdebtorCode'];
        VendorObject['f030faccountCode'] = this.vendorDataHeader['f030faccountCode'];
        VendorObject['f030faccountNo'] = this.vendorDataHeader['f030faccountNo'];
        VendorObject['f030fcompanyRegNo'] = this.vendorDataHeader['f030fcompanyRegNo'];
        VendorObject['f030frace'] = this.vendorDataHeader['f030frace'];
        VendorObject['f030fuserStatus'] = this.vendorDataHeader['f030fuserStatus'];
        //file upload
        VendorObject['f030fcompanyRegistration'] = "file";

        //license Details
        VendorObject['license-details'] = this.gradeDataList;
        VendorObject['contact-details'] = this.contactDataList;
        VendorObject['business-nature'] = this.NatureDataList;
        VendorObject['mof-details'] = this.mofDataList;
        VendorObject['payto-profile'] = this.paytoProfileList;
        VendorObject['owner-details'] = this.vendorOwnerList;
        console.log(VendorObject);
        if (this.id > 0) {
            this.SupplierregistrationService.updateSupplierregistrationItems(VendorObject, this.id).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert("Email Already Exist");
                        return false;
                    }
                    else {
                        this.router.navigate(['procurement/supplierregistration']);
                        alert("Vendor Registration has been Updated Sucessfully !!");
                    }

                }, error => {
                    console.log(error);
                    alert("Email Already Exist");
                });
        } else {
            let fd = new FormData();
            fd.append('file', this.file);

            this.httpClient.post(this.downloadUrl, fd)
                .subscribe(
                    data => {
                        VendorObject['f030faccountNumber'] = data['name'];

                        this.SupplierregistrationService.insertSupplierregistrationItems(VendorObject).subscribe(
                            data => {
                                if (data['status'] == 409) {
                                    alert("Email already Exist");
                                    return false;
                                }
                                else {
                                    this.router.navigate(['procurement/supplierregistration']);
                                    alert("Vendor Registration has been Added Sucessfully !!")
                                }
                            }, error => {
                                console.log(error);
                            });
                    },
                    err => {
                        console.log(err);
                    });
        }
    }
    fileSelected(event) {

        this.file = event.target.files[0];
        console.log(this.file);
    }

    onCountryChange() {
        this.ajaxCount++;
        if (this.vendorDataHeader['f030fcountry'] == undefined) {
            this.vendorDataHeader['f030fcountry'] = 0;
        }
        this.StateService.getStates(this.vendorDataHeader['f030fcountry']).subscribe(
            data => {
                this.ajaxCount--;
                this.stateList = data['result'];
                let other = {
                    'f012fid': "1",
                    "f012fshortName": "SH",
                    "f012fstateName": "Others"

                };
                this.stateList.push(other);

            }
        );
    }
    addcontactDataList(parameterId) {
        if (this.ContactData['f112fcontactPerson'] == undefined || this.ContactData['f112fcontactPerson'] == '') {
            alert('Enter Contact Person');
            return false;
        }
        if (this.ContactData['f112fidType'] == undefined || this.ContactData['f112fidType'] == '') {
            alert('Select ID Type');
            return false;
        }
        if (this.ContactData['f112ficNumber'] == undefined || this.ContactData['f112ficNumber'] == '') {
            alert('Enter IC Number/Passport');
            return false;
        }
        if (this.ContactData['f112femail'] == undefined || this.ContactData['f112femail'] == '') {
            alert('Enter Email');
            return false;
        }
        if (this.ContactData['f112fcontactNumber'] == undefined || this.ContactData['f112fcontactNumber'] == '') {
            alert('Enter Contact Number');
            return false;
        }
        if (parameterId != 1) {
            var confirmPop = confirm("Do you want to Add?");
            if (confirmPop == false) {
                return false;
            }
        }
        var dataofCurrentRow = this.ContactData;
        this.ContactData = {};
        this.contactDataList.push(dataofCurrentRow);
        console.log(this.contactDataList);
        this.showIcons();
    }
    addgradeDataList(parameterId) {
        if (this.vendorgradeData['f031fidLicense'] == undefined || this.vendorgradeData['f031fidLicense'] == '') {
            alert('Select Grade');
            return false;
        }
        if (this.vendorgradeData['f031fexpireDate'] == undefined || this.vendorgradeData['f031fexpireDate'] == '') {
            alert('Select Expiry Date');
            return false;
        }
        if (parameterId != 2) {
            var confirmPop = confirm("Do you want to Add?");
            if (confirmPop == false) {
                return false;
            }
        }
        var dataofCurrentRow = this.vendorgradeData;
        this.vendorgradeData = {};
        this.gradeDataList.push(dataofCurrentRow);
        this.showIcons1();
    }
    addnaturalDataList(parameterId) {
        if (this.vendorNatureData['f113fnatureOfBusiness'] == undefined || this.vendorNatureData['f113fnatureOfBusiness'] == '') {
            alert('Enter Natural of Business');
            return false;
        }
        if (this.vendorNatureData['f113fcategory'] == undefined || this.vendorNatureData['f113fcategory'] == '') {
            alert('Select Procurement Category');
            return false;
        }
        if (this.vendorNatureData['f113fsubCategory'] == undefined || this.vendorNatureData['f113fsubCategory'] == '') {
            alert('Select Procurement Sub Category');
            return false;
        }
        if (parameterId != 3) {
            var confirmPop = confirm("Do you want to Add?");
            if (confirmPop == false) {
                return false;
            }
        }
        var dataofCurrentRow = this.vendorNatureData;
        this.vendorNatureData = {};
        this.NatureDataList.push(dataofCurrentRow);
        this.showIcons2();
    }
    addmofDataList(parameterId) {
        if (this.vendormofData['f115fmodeOfCategory'] == undefined || this.vendormofData['f115fmodeOfCategory'] == '') {
            alert('Enter Mof Category');
            return false;
        }
        if (parameterId != 4) {
            var confirmPop = confirm("Do you want to Add?");
            if (confirmPop == false) {
                return false;
            }
        }
        var dataofCurrentRow = this.vendormofData;
        this.vendormofData = {};
        this.mofDataList.push(dataofCurrentRow);
        this.showIcons3();
    }
    addPaytoProfileList(parameterId) {
        if (this.paytoProfileData['f127fname'] == undefined || this.paytoProfileData['f127fname'] == '') {
            alert('Enter Pay to Profile Name');
            return false;
        }
        if (this.paytoProfileData['f127fbank'] == undefined || this.paytoProfileData['f127fbank'] == '') {
            alert('Select Bank');
            return false;
        }
        if (this.paytoProfileData['f127faccountNo'] == undefined || this.paytoProfileData['f127faccountNo'] == '') {
            alert('Enter Account Number');
            return false;
        }
        if (parameterId != 5) {
            var confirmPop = confirm("Do you want to Add?");
            if (confirmPop == false) {
                return false;
            }
        }
        var dataofCurrentRow = this.paytoProfileData;
        this.paytoProfileData = {};
        this.paytoProfileList.push(dataofCurrentRow);
        this.showIcons4();
    }
    addVendorOwnerDataList(parameterId) {
        if (this.vendorOwnerData['f031fownerName'] == undefined || this.vendorOwnerData['f031fownerName'] == '') {
            alert('Enter the Name');
            return false;
        }
        if (this.vendorOwnerData['f031fownerIc'] == undefined || this.vendorOwnerData['f031fownerIc'] == '') {
            alert('Enter the IC Number');
            return false;
        }
        if (this.vendorOwnerData['f031fownerDesignation'] == undefined || this.vendorOwnerData['f031fownerDesignation'] == '') {
            alert('Enter the Designation');
            return false;
        }
        if (parameterId != 6) {
            var confirmPop = confirm("Do you want to Add?");
            if (confirmPop == false) {
                return false;
            }
        }
        var dataofCurrentRow = this.vendorOwnerData;
        this.vendorOwnerData = {};
        this.vendorOwnerList.push(dataofCurrentRow);
        this.showIcons5();
    }
    onlyNumberKey(event) {
        if (event.charCode == 46) {

        } else {
            return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
        }
    }
    onlyNumberKey1(event) {
        console.log(event);
        return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
    }
}