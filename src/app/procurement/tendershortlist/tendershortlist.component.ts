import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { TenderquatationService } from '../service/tenderquotation.service'
import { faSearch, faEdit } from '@fortawesome/free-solid-svg-icons';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'TendershortlistComponent',
    templateUrl: 'tendershortlist.component.html'
})

export class TendershortlistComponent implements OnInit {
    tenderQuotationList =  [];
    tenderQuotationData = {};
    faSearch = faSearch;
    faEdit = faEdit;
    
    id: number;

    constructor(
        
        private TenderquatationService: TenderquatationService,
        private spinner: NgxSpinnerService

    ) { }

    ngOnInit() {
        this.spinner.show();
       
        this.TenderquatationService.getTenderSubmissions().subscribe(
            data => {
                this.spinner.hide();
                this.tenderQuotationList = data['result'];
                console.log(this.tenderQuotationData);
        }, error => {
            console.log(error);
        });
    }  
}