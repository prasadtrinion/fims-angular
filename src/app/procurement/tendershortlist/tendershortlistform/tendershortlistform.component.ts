import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { TenderquatationService } from '../../service/tenderquotation.service'
import { ActivatedRoute, Router } from '@angular/router';
import { AlertService } from '../../../_services/alert.service'
import { StaffService } from "../../../loan/service/staff.service";
import { SupplierregistrationService } from "../../../procurement/service/supplierregistration.service";
import { LicenseService } from '../../service/license.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'TendershortlistformComponent',
    templateUrl: 'tendershortlistform.component.html'
})

export class TendershortlistformComponent implements OnInit {
    tenderList = [];
    tenderData = {};
    categoryList = [];
    committListData = {};
    committList = [];
    shortList = []; 
    specData = {};
    smartStaffList = [];
    tenderQuotationList = [];
    shortListData = {};
    vendorList = [];
    supplierList = [];
    vendorListAward = [];
    generalspecData = {};
    licenseList = [];
    remarkList = [];
    id: number;
    ajaxCount : number;

    constructor(
        private TenderquatationService: TenderquatationService,
        private SupplierregistrationService: SupplierregistrationService,
        private route: ActivatedRoute,
        private AlertService: AlertService,
        private router: Router,
        private StaffService: StaffService,
        private LicenseService: LicenseService,
        private spinner:NgxSpinnerService,

    ) { }
    getVendorDetails() {
        console.log(this.specData);
        this.SupplierregistrationService.getItems().subscribe(
            data => {

                this.vendorList = data['result']['data'];
                for (var k = 0; k < this.vendorList.length; k++) {
                    this.vendorList[k]['statuscheckbox'] = false;
                }
                console.log(this.vendorList);
            }, error => {
                console.log(error);
            });

    }


    ngDoCheck() {
        const change = this.ajaxCount;
        if (this.ajaxCount == 0) {
        this.spinner.hide();            
        }
    }


    ngOnInit() {
        this.spinner.show();
        this.ajaxCount = 0;
        this.tenderData['f035fstatus'] = 1;
        this.tenderData['f035fidDepartment'] = '';
        this.tenderData['f035fidCategory'] = '';
        this.tenderData['f035ftype'] = '';
        this.ajaxCount++;
        this.TenderquatationService.getTenderShorlists().subscribe(
            data => {
        this.ajaxCount--;
                this.tenderQuotationList = data['result'];
                console.log(this.tenderQuotationList);
            }, error => {
                console.log(error);
            });
        this.SupplierregistrationService.getItems().subscribe(
            data => {

                this.vendorList = data['result']['data'];
                for (var k = 0; k < this.vendorList.length; k++) {
                    this.vendorList[k]['statuscheckbox'] = false;
                }
                console.log(this.vendorList);

            }, error => {
                console.log(error);
            });
        this.StaffService.getItems().subscribe(
            data => {
                this.smartStaffList = data['result']['data'];
                console.log(this.smartStaffList);
            }, error => {
                console.log(error);
            });

        this.LicenseService.getItems().subscribe(
            data => {
                this.licenseList = data['result']['data'];
            }, error => {
                console.log(error);
            });

        // categoryList dropdown
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        console.log(this.id);

        if (this.id > 0) {
        this.ajaxCount++;
            this.TenderquatationService.getItemsDetail(this.id).subscribe(
                data => {
        this.ajaxCount--;
                    this.tenderData = data['result'][0];
                    if (data['result'].f035fstatus === true) {
                        console.log(data);
                        this.tenderData['f035fstatus'] = 1;
                    } else {
                        this.tenderData['f035fstatus'] = 0;

                    }
                    console.log(this.tenderData);
                }, error => {
                    console.log(error);
                });
        }
        console.log(this.id);
    }


    showTenderItems() {
        console.log("asdfsdf");
        this.TenderquatationService.getAllListBasedOnTendor(this.specData['f050fidTendor']).subscribe(
            data => {
                console.log(data);

                this.specData['f050fstartDate'] = data['result']['vendor-details'][0]['f035fstartDate'].replace(" ", "T");
                this.specData['f050fendDate'] = data['result']['vendor-details'][0]['f035fendDate'].replace(" ", "T");
                this.specData['f050ftitle'] = data['result']['vendor-details'][0]['f035ftitle']
                this.specData['f050ftenderOpeningDate'] = data['result']['vendor-details'][0]['f035fopeningDate'].replace(" ", "T");

                this.committList = data['result']['commitee-details'];
                for (var i = 0; i < this.committList.length; i++) {
                    this.committList[i]['f051fidStaff'] = this.committList[i]['f051fidStaff'];
                }
                this.supplierList = data['result']['vendor-details'];
                this.remarkList = data['result']['remarks'];
                for (var j = 0; j < this.supplierList.length; j++) {
                    if (this.supplierList[j]['f050fisShortlisted'] == '1') {
                        this.supplierList[j]['statuscheckbox'] = true;
                    } else {
                        this.supplierList[j]['statuscheckbox'] = false;

                    }
                    this.supplierList[j]['f050fgrade'] = parseInt(this.supplierList[j]['f050fgrade']);
                }
                // console.log(this.tenderData);
            }, error => {
                console.log(error);
            });
    }

    addListCommittee() {
        this.shortList.push(this.shortListData);
        this.shortListData = {};

    }

    saveData() {
        if (this.specData['f050fidTendor'] == undefined) {
            alert('Select Reference Number ');
            return false;
        }
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        console.log(this.vendorList);
        var finalArray = [];
        for (var i = 0; i < this.supplierList.length; i++) {
            if (this.supplierList[i]['statuscheckbox'] == true) {
                finalArray.push(this.supplierList[i]['f050fid']);
            }
        }
        if (finalArray.length < 1) {
            alert("Select atleast one Vendor");
            return false;
        }
        var finalTen = {};
        finalTen['id'] = finalArray;
        finalTen['tendorId'] = this.specData['f050fidTendor'];

        this.tenderData['f035ftenderStatus'] = 0;
        this.TenderquatationService.finalinsertTenderShortlist(finalTen).subscribe(
            data => {
                alert("Vendor has been shorlisted successfully")

                //location.reload();
                this.supplierList = [];
                this.remarkList = [];
                this.committList = [];
                this.specData = {};

            }, error => {
                console.log(error);
            });
    }
    addtender() {
        console.log(this.tenderData);

    }
}