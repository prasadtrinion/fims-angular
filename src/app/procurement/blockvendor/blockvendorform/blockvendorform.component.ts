import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SupplierregistrationService } from '../../service/supplierregistration.service';
import { BlockvendorService } from "../../service/blockvendor.service";
import { NgxSpinnerService } from 'ngx-spinner';
@Component({
    selector: 'BlockvendorFormComponent',
    templateUrl: 'blockvendorform.component.html'
})

export class BlockvendorFormComponent implements OnInit {
    blockvendorList = [];
    blockvendorData = {};
    supplierList = [];
    supplierList1 = [];
    ajaxCount: number;
    editDisabled: boolean;
    staffList = [];
    id: number;

    constructor(
        private BlockvendorService: BlockvendorService,
        private SupplierregistrationService: SupplierregistrationService,
        private route: ActivatedRoute,
        private spinner: NgxSpinnerService,
        private router: Router,

    ) { }
    ngDoCheck() {
        const change = this.ajaxCount;
        if (this.ajaxCount == 0) {
            this.spinner.hide();
        }
    }
    ngOnInit() {
        this.editDisabled = false;
        this.blockvendorData['f111fstatus'] = 1;
        this.ajaxCount = 0
        this.spinner.show();
        // staff dropdown

        this.ajaxCount++;
        this.SupplierregistrationService.getActiveItems().subscribe(
            data => {
                this.ajaxCount--;
                console.log(this.ajaxCount);

                this.supplierList = data['result']['data'];
            }
        );
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        if (this.id > 0) {
            this.ajaxCount++;
            this.BlockvendorService.getItemsDetail(this.id).subscribe(
                data => {
                    this.ajaxCount--;
                    this.SupplierregistrationService.getItems().subscribe(
                        data => {
                            this.ajaxCount--;
                            console.log(this.ajaxCount);

                            this.supplierList = data['result']['data'];
                        }
                    );
                    this.blockvendorData = data['result'][0];
                    if (this.blockvendorData['f111freleaseDate'] == '1970-01-01') {
                        this.blockvendorData['f111freleaseDate'] = '';
                    }
                    this.blockvendorData['f111fstatus'] = parseInt(this.blockvendorData['f111fstatus']);
                    console.log(this.blockvendorData);
                    this.editDisabled=true;
                }, error => {
                    console.log(error);
                });
        }
    }
    ConvertToInt(val) {
        return parseInt(val);
    }
    dateComparison() {

        let startDate = Date.parse(this.blockvendorData['f111fblocklistFrom']);
        let endDate = Date.parse(this.blockvendorData['f111freleaseDate']);
        if (startDate > endDate) {
            alert("Blacklist From Date cannot be greater than Release Date !");
            this.blockvendorData['f111freleaseDate'] = "";
        }

    }
    addblockvendor() {
        console.log(this.blockvendorData);
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        if (this.id > 0) {
            this.BlockvendorService.updateBlockvendorItems(this.blockvendorData, this.id).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert('Blacklist Vendor Already Exist');
                    }
                    else {
                    }
                    this.router.navigate(['procurement/blockvendor']);
                    alert("Block Vendor has been Updated Successfully");
                }, error => {
                    console.log(error);
                });

        } else {
            this.BlockvendorService.insertBlockvendorItems(this.blockvendorData).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert("No duplication of Blacklist Vendor");
                    }
                    else {
                        this.router.navigate(['procurement/blockvendor']);
                        alert("Vendor has been Blacklisted");
                    }
                    this.router.navigate(['procurement/blockvendor']);

                }, error => {
                    console.log(error);
                });

        }
    }

}