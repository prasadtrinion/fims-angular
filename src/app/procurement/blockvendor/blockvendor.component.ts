import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { BlockvendorService } from "../service/blockvendor.service";
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'Blockvendor',
    templateUrl: 'blockvendor.component.html'
})

export class BlockvendorComponent implements OnInit {
    blacklistempListNew = [];
    blockvendorList = [];
    blockvendorData = {};
    id: number;
    title: string;
    type: string;

    constructor(
     
        private BlockvendorService: BlockvendorService,
        private spinner: NgxSpinnerService
    ) { }

    ngOnInit() {
        this.spinner.show();
        this.BlockvendorService.getItems().subscribe(
            data => {
                this.spinner.hide();
                this.blockvendorList = data['result']['data'];
            }, error => {
                console.log(error);
            });
    }  
}