import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { GrnService } from '../../service/grn.service';
import { TaxsetupcodeService } from '../../../generalledger/service/taxsetupcode.service'
import { ItemsetupService } from '../../../procurement/service/itemsetup.service';
import { AlertService } from '../../../_services/alert.service';
import { DepartmentService } from '../../../generalsetup/service/department.service';
import { FinancialyearService } from '../../../generalledger/service/financialyear.service';
import { SupplierregistrationService } from '../../service/supplierregistration.service';
import { PurchaseorderService } from '../../service/purchaseorder.service'
import { FundService } from '../../../generalsetup/service/fund.service';
import { AccountcodeService } from '../../../generalsetup/service/accountcode.service';
import { ActivitycodeService } from '../../../generalsetup/service/activitycode.service';
import { GrnapprovalService } from '../../service/grnapproval.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { PurchaserequistionentryService } from '../../service/purchaserequistionentry.service';
import { BudgetactivitiesService } from "../../../budget/service/budgetactivities.service";

@Component({
    selector: 'GrnapprovalformComponent',
    templateUrl: 'grnapprovalform.component.html'
})

export class GrnapprovalformComponent implements OnInit {
    grnList = [];
    grnData = {};
    grnDataheader = {};
    itemList = [];
    taxcodeList = [];
    departmentList = [];
    financialList = [];
    DeptList = [];
    accountcodeList = [];
    fundList = [];
    activitycodeList = [];
    itemUnitList = [];
    supplierList = [];
    purchaseMethods = [];
    purchaseOrderList = [];
    grnapprovalList = [];
    approvalData = [];
    budgetcostcenterList = [];
    costCenterFund = [];
    costCenterAccount = [];
    costCenterActivity = [];
    costCenterList = [];
    depList = [];
    typeItemList = [];
    id: number;
    valueDate: string;
    ajaxCount: number;
    gstValue: number;
    Amount: number;
    taxAmount: number;
    totalAmount: number;
    viewDisabled = false;
    type: string;

    constructor(

        private PurchaseorderService: PurchaseorderService,
        private GrnService: GrnService,
        private PurchaserequistionentryService: PurchaserequistionentryService,
        private GrnapprovalService: GrnapprovalService,
        private TaxsetupcodeService: TaxsetupcodeService,
        private ItemService: ItemsetupService,
        private ActivitycodeService: ActivitycodeService,
        private AccountcodeService: AccountcodeService,
        private FundService: FundService,
        private alertService: AlertService,
        private spinner: NgxSpinnerService,
        private DepartmentService: DepartmentService,
        private FinancialyearService: FinancialyearService,
        private SupplierregistrationService: SupplierregistrationService,
        private BudgetactivitiesService: BudgetactivitiesService,
        private route: ActivatedRoute,
        private router: Router

    ) { }
    ngDoCheck() {
        const change = this.ajaxCount;
        // console.log(this.ajaxCount);
        if (this.ajaxCount == 0) {
            this.editFunction();
            this.spinner.hide();
            this.ajaxCount = 10;
        }
    }
    getListData() {
        this.GrnapprovalService.getItemsDetail(0).subscribe(
            data => {
                this.grnapprovalList = data['result'];
            }, error => {
                console.log(error);
            });
    }

    approvalListData() {
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        console.log(this.grnList);
        let approvaloneIds = [];
       
        approvaloneIds.push(this.id);
            
       
        var confirmPop = confirm("Do you want to Approve?");
        if (confirmPop == false) {
            return false;
        }
        var approvaloneUpdate = {};
        approvaloneUpdate['id'] = approvaloneIds;
        approvaloneUpdate['status'] = 1;
        approvaloneUpdate['reason'] = '';
        console.log(approvaloneIds);
        this.GrnapprovalService.updateGrnapprovalItems(approvaloneUpdate).subscribe(
            data => {
                this.router.navigate(['procurement/grnapproval']);
                this.getListData();
                this.approvalData['reason'] = '';
                alert(" Approved Sucessfully ! ");

            }, error => {
                console.log(error);
            });
    }

    rejectListData() {

        var confirmPop = confirm("Do you want to Reject ?");
        if (confirmPop == false) {
            return false;
        }


        console.log(this.grnList);
        var rejectIds = [];
        for (var i = 0; i < this.grnList.length; i++) {
            if (this.grnList[i].f034fapprovalStatus == true) {
                rejectIds.push(this.grnList[i].f034fidDetails);
            }
        }

        // if (rejectIds.length < 1) {
        //     alert("Select atleast one Application to Reject");
        //     return false;
        // }

        var rejectUpdate = {};
        rejectUpdate['id'] = rejectIds;
        rejectUpdate['status'] = 2;
        rejectUpdate['reason'] = this.grnData['reason'];


        console.log(this.grnData['reason']);

        if (this.grnData['reason'] == '' || this.grnData['reason'] == undefined) {
            alert("Enter Description to Reject !");
        } else {

            this.GrnapprovalService.updateGrnapprovalItems(rejectUpdate).subscribe(
                data => {
                    alert(" Application Rejected ! ");
                    this.router.navigate(['procurement/grnapproval']);

                }, error => {
                    console.log(error);
                });
        }
    }
    editFunction() {
        this.ajaxCount++;
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        // this.route.paramMap.subscribe((data) => this.idview = + data.get('idview'));
        if (this.id > 0) {
            this.GrnService.getItemsDetail(this.id).subscribe(
                data => {
                    this.ajaxCount--;
                    this.PurchaseorderService.getItems().subscribe(
                        data => {
                            this.ajaxCount--;
                            this.purchaseOrderList = data['result']['data'];
                            // console.log(this.purchaseRequisitionList);
                        }, error => {
                            console.log(error);
                        });
                    this.grnList = data['result'];
                    this.grnDataheader['f034forderType'] = data['result'][0]['f034forderType'];
                    this.grnDataheader['f034forderDate'] = data['result'][0]['f034forderDate'];
                    this.grnDataheader['f034fidFinancialyear'] = data['result'][0]['f034fidFinancialyear'];
                    this.grnDataheader['f034fidSupplier'] = data['result'][0]['f034fidSupplier'];
                    this.grnDataheader['f034fdescription'] = data['result'][0]['f034fdescription'];
                    this.grnDataheader['f034freferenceNumber'] = data['result'][0]['f034freferenceNumber'];
                    this.grnDataheader['f034fidDepartment'] = data['result'][0]['f034fidDepartment'];
                    this.grnDataheader['f034fidPurchaseOrder'] = data['result'][0]['f034fidPurchaseOrder'];
                    this.grnDataheader['f034fexpiredDate'] = data['result'][0]['f034fexpiredDate'];
                    this.grnDataheader['f034ftotalAmount'] = data['result'][0]['f034ftotalAmount'];
                    this.grnDataheader['f034frating'] = data['result'][0]['f034frating'];

                    this.grnDataheader['f034fid'] = data['result'][0]['f034fid'];
                    if (data['result'][0]['f034fapprovalStatus'] == 1 || this.grnList[0]['f034fapprovalStatus'] == 2) {
                        this.viewDisabled = true;

                        $("#target input,select").prop("disabled", true);
                        $("#target1 input,select").prop("disabled", true);
                    }
                    this.getAllpurchaseGlList();
                    // console.log(this.grnList);
                    this.getDepartment();
                    this.onBlurMethod();
                }, error => {
                    console.log(error);
                });
        }
    }
    ngOnInit() {
        let typeofitemObject = {};
        typeofitemObject['id'] = 1;
        typeofitemObject['name'] = 'Assets';
        this.typeItemList.push(typeofitemObject);
        typeofitemObject = {};
        typeofitemObject['id'] = 2;
        typeofitemObject['name'] = 'Expense';
        this.typeItemList.push(typeofitemObject);
        typeofitemObject = {};
        typeofitemObject['id'] = 3;
        typeofitemObject['name'] = 'Inventory';
        this.typeItemList.push(typeofitemObject);
        this.ajaxCount = 0;
        this.spinner.show();
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        console.log(this.id);

        this.getGRNumber();
        this.ajaxCount = 0;
        //purchase order dropdown
        this.ajaxCount++;
        this.PurchaseorderService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.purchaseOrderList = data['result']['data'];
            }, error => {
                console.log(error);
            });
        //item dropdown
        this.ajaxCount++;
        this.ItemService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                console.log(this.ajaxCount);

                this.itemList = data['result']['data'];
                console.log(this.itemList);
            }
        );
        //Supplierregistration dropdown
        this.ajaxCount++;
        this.SupplierregistrationService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                console.log(this.ajaxCount);

                this.supplierList = data['result']['data'];
            }
        );

        //Department dropdown
        this.ajaxCount++;
        this.DepartmentService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.DeptList = data['result']['data'];
            }
        );

        //Activity dropdown
        this.ajaxCount++;
        this.ActivitycodeService.getItemsByLevel().subscribe(
            data => {
                this.ajaxCount--;
                this.activitycodeList = data['result']['data'];
            }
        );

        //Account dropdown
        this.ajaxCount++;
        this.AccountcodeService.getItemsByLevel().subscribe(
            data => {
                this.ajaxCount--;
                this.accountcodeList = data['result']['data'];
            }
        );

        //Fund dropdown
        this.ajaxCount++;
        this.FundService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.fundList = data['result']['data'];
            }
        );


        //Financial year dropdown
        this.ajaxCount++;
        this.FinancialyearService.getActiveItems().subscribe(
            data => {
                this.ajaxCount--;
                console.log(this.ajaxCount);

                this.financialList = data['result'];
            }
        );
        //Department dropdown
        this.ajaxCount++;
        this.DepartmentService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                console.log(this.ajaxCount);

                this.departmentList = data['result']['data'];
            }
        );
        //Department dropdown
        this.ajaxCount++;
        this.DepartmentService.getZerodepartment().subscribe(
            data => {
                this.ajaxCount--;
                console.log(this.ajaxCount);

                this.depList = data['result']['data'];
            }
        );
        console.log(this.grnList);
        this.ajaxCount++;
        this.TaxsetupcodeService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.taxcodeList = data['result']['data'];
            });
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            // this.spinner.show();
            this.ajaxCount++;
            this.GrnService.getItemsDetail(this.id).subscribe(
                data => {
                    this.ajaxCount--;
                    this.viewDisabled = true;
                    console.log(this.viewDisabled);
                    this.grnList = data['result'];
                    this.grnDataheader['f034forderType'] = data['result'][0].f034forderType;
                    this.grnDataheader['f034forderDate'] = data['result'][0].f034forderDate;
                    this.grnDataheader['f034fidFinancialyear'] = data['result'][0].f034fidFinancialyear;
                    this.grnDataheader['f034fidSupplier'] = data['result'][0].f034fidSupplier;
                    this.grnDataheader['f034fdescription'] = data['result'][0].f034fdescription;
                    this.grnDataheader['f034freferenceNumber'] = data['result'][0].f034freferenceNumber;
                    this.grnDataheader['f034fidDepartment'] = data['result'][0].f034fidDepartment;
                    this.grnDataheader['f034fidPurchaseOrder'] = data['result'][0].f034fidPurchaseOrder;
                    this.grnDataheader['f034fexpiredDate'] = data['result'][0].f034fexpiredDate;
                    this.grnDataheader['f034ftotalAmount'] = data['result'][0]['f034ftotalAmount'];
                    this.grnDataheader['f034fid'] = data['result'][0].f034fid;
                    setTimeout(function () {
                        $("#target input,ng-select").prop("disabled", true);
                    }, 500);

                    console.log(this.grnList);
                }, error => {
                    console.log(error);
                });
        }
    }
    getDepartment() {
        var departmentId = this.grnDataheader['f034fidDepartment'];
        var departmentCode = departmentId.substring(0, 3);;

        this.BudgetactivitiesService.getCostCenterBasedOnDepartmentCode(departmentCode).subscribe(
            data => {
                this.costCenterList = data['result']['data'];
            }, error => {
                console.log(error);
            });
    }
    getUOM() {
        let itemId = this.grnData['f034fidItem'];
        console.log(itemId);
        this.ItemService.getItemsDetail(itemId).subscribe(
            data => {
                this.itemUnitList = data['result'][0];
                console.log(data);
                this.grnData['f034funit'] = data['result'][0]['f029funit'];
            }
        );
    }
    getAllpurchaseGlList() {
        var idfinancialYear = this.grnDataheader['f034fidFinancialyear'];

        this.PurchaserequistionentryService.urlgetAllpurcahseGls(idfinancialYear).subscribe(
            data => {
                var depArray = data['result']['Department'];
                this.budgetcostcenterList = depArray;

                this.costCenterFund = data['result']['Fund'];

                this.costCenterAccount = data['result']['Account'];

                this.costCenterActivity = data['result']['Activity'];

                console.log(data);
            }, error => {
                console.log(error);
            }
        );
    }
    getGRNumber() {
        let typeObject = {};
        typeObject['type'] = "GR"
        console.log(typeObject);

        this.GrnService.getGRN(typeObject).subscribe(
            data => {
                console.log(data);
                this.grnDataheader['f034freferenceNumber'] = data['number'];
                this.valueDate = new Date().toISOString().substr(0, 10);
                this.grnDataheader['f034forderDate'] = this.valueDate;
            }
        );
        return this.type;
    }


    getPurchaseOrderDetails() {
        let PurchaseOrderId = this.grnDataheader['f034fidPurchaseOrder'];

        this.PurchaseorderService.getItemsDetail(PurchaseOrderId).subscribe(
            data => {
                this.purchaseOrderList = data['result'];

                this.grnDataheader['f034fidFinancialyear'] = data['result'][0]['f034fidFinancialyear'];
                this.grnDataheader['f034fidSupplier'] = data['result'][0]['f034fidSupplier'];
                this.grnDataheader['f034fidDepartment'] = data['result'][0]['f034fidDepartment'];
                this.grnDataheader['f034fexpiredDate'] = data['result'][0]['f034forderDate'];
                this.grnDataheader['f034fdescription'] = data['result'][0]['f034fdescription'];



                for (var i = 0; i < this.purchaseOrderList.length; i++) {
                    var purchaseOrderObject = {}

                    purchaseOrderObject['f034fidItem'] = this.purchaseOrderList[i]['f034fidItem'];
                    purchaseOrderObject['f034funit'] = this.purchaseOrderList[i]['f034funit'];
                    purchaseOrderObject['f034ffundCode'] = this.purchaseOrderList[i]['f034ffundCode'];
                    purchaseOrderObject['f034factivityCode'] = this.purchaseOrderList[i]['f034factivityCode'];
                    purchaseOrderObject['f034fdepartmentCode'] = this.purchaseOrderList[i]['f034fdepartmentCode'];
                    purchaseOrderObject['f034faccountCode'] = this.purchaseOrderList[i]['f034faccountCode'];
                    purchaseOrderObject['f034fbudgetFundCode'] = this.purchaseOrderList[i]['f034fbudgetFundCode'];
                    purchaseOrderObject['f034fbudgetActivityCode'] = this.purchaseOrderList[i]['f034fbudgetActivityCode'];
                    purchaseOrderObject['f034fbudgetDepartmentCode'] = this.purchaseOrderList[i]['f034fbudgetDepartmentCode'];
                    purchaseOrderObject['f034fbudgetAccountCode'] = this.purchaseOrderList[i]['f034fbudgetAccountCode'];
                    purchaseOrderObject['f034fsoCode'] = this.purchaseOrderList[i]['f034fsoCode'];
                    purchaseOrderObject['f034frequiredDate'] = this.purchaseOrderList[i]['f034frequiredDate'];
                    purchaseOrderObject['f034fquantity'] = this.purchaseOrderList[i]['f034fquantity'];
                    purchaseOrderObject['f034fprice'] = this.purchaseOrderList[i]['f034fprice'];
                    purchaseOrderObject['f034total'] = this.purchaseOrderList[i]['f034total'];
                    purchaseOrderObject['f034ftaxCode'] = this.purchaseOrderList[i]['f034ftaxCode'];
                    purchaseOrderObject['f034fpercentage'] = this.purchaseOrderList[i]['f034fpercentage'];
                    purchaseOrderObject['f034ftaxAmount'] = this.purchaseOrderList[i]['f034ftaxAmount'];
                    purchaseOrderObject['f034ftotalIncTax'] = this.purchaseOrderList[i]['f034ftotalIncTax'];
                    this.grnList.push(purchaseOrderObject);
                    this.getAllpurchaseGlList();
                    this.getDepartment();
                }
                console.log(this.grnList);
            }
        );
    }
    ConvertToFloat(val) {
        return parseFloat(val);
    }

    saveGrn() {

        let purchaseOrderObject = {};
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        console.log(this.id);
        if (this.id > 0) {
            purchaseOrderObject['f034fid'] = this.id;
        }
        purchaseOrderObject['f034forderType'] = this.grnDataheader['f034forderType'];
        purchaseOrderObject['f034forderDate'] = this.grnDataheader['f034forderDate'];
        purchaseOrderObject['f034fexpiredDate'] = this.grnDataheader['f034fexpiredDate'];
        purchaseOrderObject['f034fidSupplier'] = this.grnDataheader['f034fidSupplier'];
        purchaseOrderObject['f034fidFinancialyear'] = this.grnDataheader['f034fidFinancialyear'];
        purchaseOrderObject['f034fdescription'] = this.grnDataheader['f034fdescription'];
        purchaseOrderObject['f034freferenceNumber'] = this.grnDataheader['f034freferenceNumber'];
        purchaseOrderObject['f034fidDepartment'] = this.grnDataheader['f034fidDepartment'];
        purchaseOrderObject['f034fidPurchaseOrder'] = this.grnDataheader['f034fidPurchaseOrder'];

        purchaseOrderObject['f034fstatus'] = 0;
        purchaseOrderObject['grn-details'] = this.grnList;

        if (this.id > 0) {
            this.GrnService.updateEntryItems(purchaseOrderObject, this.id).subscribe(
                data => {

                    this.router.navigate(['procurement/grn']);
                    this.alertService.success(" Updated Sucessfully ! ");

                }, error => {
                    console.log(error);
                });
        } else {
            this.GrnService.insertEntryItems(purchaseOrderObject).subscribe(
                data => {


                    this.router.navigate(['procurement/grn']);
                    this.alertService.success(" Added Sucessfully ! ");

                }, error => {
                    console.log(error);
                });
        }
    }

    getGstvalue() {
        let taxId = this.grnData['f034ftaxCode'];
        let quantity = this.grnData['f034fquantity'];
        let price = this.grnData['f034fprice'];
        //console.log(this.taxcodeList);
        var taxSelectedObject = {};
        for (var k = 0; k < this.taxcodeList.length; k++) {
            if (this.taxcodeList[k]['f081fid'] == taxId) {
                taxSelectedObject = this.taxcodeList[k];
            }
        }
        this.gstValue = taxSelectedObject['f034fpercentage'];
        this.grnData['f034fpercentage'] = this.gstValue;
        this.Amount = (this.ConvertToFloat(price)) * (this.ConvertToFloat(quantity));
        this.taxAmount = (this.ConvertToFloat(this.gstValue)) / 100 * this.ConvertToFloat((this.Amount));

        let totalAm = ((this.taxAmount) + (this.Amount));
        this.grnData['f034total'] = this.Amount.toFixed(2);
        this.grnData['f034ftaxAmount'] = this.taxAmount.toFixed(2);
        this.grnData['f034ftotalIncTax'] = totalAm.toFixed(2);

        console.log(this.grnData);
    }

    onBlurMethod() {
        console.log(this.grnList);
        var finaltotal = 0;
        for (var i = 0; i < this.grnList.length; i++) {
            var totalamount = (this.ConvertToFloat(this.grnList[i]['f034fquantity'])) * (this.ConvertToFloat(this.grnList[i]['f034fprice']));
            var gstamount = (this.ConvertToFloat(this.gstValue) / 100 * (totalamount));
            this.grnList[i]['f034ftotal'] = gstamount + totalamount;
            console.log(gstamount);
            console.log(totalamount)
            finaltotal = finaltotal + this.grnList[i]['f034ftotal'];
        }

    }

    deleteEntry(object) {
        var cnf = confirm("Do you really want to delete");
        if (cnf == true) {
            console.log(this.grnList);
            var index = this.grnList.indexOf(object);;
            if (index > -1) {
                this.grnList.splice(index, 1);
            }
            this.onBlurMethod();
        }
    }

    addEntrylist() {
        console.log(this.grnData);

        var dataofCurrentRow = this.grnData;
        this.grnData = {};
        this.grnList.push(dataofCurrentRow);
        this.onBlurMethod();
    }
    maximize() {
        $("#myModal").addClass("modal");
        $("#overlay1").show();
        $(".maximize-icon").addClass("hidden");
        $(".minimize-icon").removeClass("hidden");
        $(".modal-backdrop").show();

    }
    minimize() {
        $("#myModal").removeClass("modal");
        $("#overlay1").hide();
        $(".maximize-icon").removeClass("hidden");
        $(".minimize-icon").addClass("hidden");
        $(".modal-backdrop").hide();
        $('#myModal').show();
    }
}