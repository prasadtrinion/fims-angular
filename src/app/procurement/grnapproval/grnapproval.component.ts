import {Injector} from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { GrnapprovalService } from '../service/grnapproval.service'
import { AlertService } from './../../_services/alert.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'Grnapproval',
    templateUrl: 'grnapproval.component.html'
})

export class GrnapprovalComponent implements OnInit {
   
    grnapprovalList =  [];
    approvalData = {};
    grnapprovalData = {};
    selectAllCheckbox = true;

    constructor(
        
        private GrnapprovalService: GrnapprovalService,
        private AlertService: AlertService,
        private spinner: NgxSpinnerService
        
    ) { }

    ngOnInit() {    
       this.getListData();
    }
    getListData(){
        this.spinner.show();
        this.GrnapprovalService.getlistItems().subscribe(
            data => {
                this.spinner.hide();
                this.grnapprovalList = data['result']['data'];
        }, error => {
            console.log(error);
        });
    }
    
    GrnapprovalListData() {
        console.log(this.grnapprovalList);
        var approvaloneIds = [];
        for (var i = 0; i < this.grnapprovalList.length; i++) {
            if(this.grnapprovalList[i].f034fapprovalStatus==true) {
                approvaloneIds.push(this.grnapprovalList[i].f034fid);
            }
        }
        var confirmPop = confirm("Do you want to Approve?");
        if (confirmPop == false) {
            return false;
        }
        var approvaloneUpdate = {};
        approvaloneUpdate['id'] = approvaloneIds;
        this.GrnapprovalService.updateGrnapprovalItems(approvaloneUpdate).subscribe(
            data => {
                this.getListData();
                alert("Approved Successfully");
        }, error => {
            console.log(error);
        });
    }
    regectApprovalListData() {
        console.log(this.grnapprovalList);
        var approvaloneIds = [];
        for (var i = 0; i < this.grnapprovalList.length; i++) {
            if(this.grnapprovalList[i].f034fapprovalStatus==true) {
                approvaloneIds.push(this.grnapprovalList[i].f034fid);
            }
        }
        if (approvaloneIds.length < 1) {
            alert("Please select atleast one cost center to Reject");
            return false;
        }
        if (this.approvalData['reason'] == undefined) {
            alert('Please Enter the Description');
            return false;
        }
        var confirmPop = confirm("Do you want to Reject?");
        if (confirmPop == false) {
            return false;
        }
        var budgetActivityDataObject = {}
        budgetActivityDataObject['id'] = approvaloneIds;
        budgetActivityDataObject['status'] = 2;
        budgetActivityDataObject['reason'] = this.approvalData['reason'];
        this.GrnapprovalService.updateGrnapprovalItems(budgetActivityDataObject).subscribe(
            data => {
                this.getListData()
                this.approvalData['reason']=""
                alert("Rejected Successfully");
        }, error => {
            console.log(error);
        });


    }
    selectAll() {
        console.log("asdf");
        console.log(this.selectAllCheckbox);
        for (var i = 0; i < this.grnapprovalList.length; i++) {
                this.grnapprovalList[i].f034fapprovalStatus = this.selectAllCheckbox;
        }
        console.log(this.grnapprovalList);
      }

    }
