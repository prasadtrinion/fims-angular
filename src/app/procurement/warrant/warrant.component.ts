import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { WarrantService } from "../service/warrant.service";
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'Warrant',
    templateUrl: 'warrant.component.html'
})
export class WarrantComponent implements OnInit {
    warrantList = [];
    id: number;
    constructor(
        private WarrantService: WarrantService,
        private spinner: NgxSpinnerService,
    ) { }
    ngOnInit() {
        this.spinner.show();
        this.WarrantService.getItems().subscribe(
            data => {
                this.spinner.hide();
                // this.warrantList = data['result']['data'];
                let activityData = [];
                activityData = data['result']['data'];
                console.log("asdf");
                for (var i = 0; i < activityData.length; i++) {
                    if (activityData[i]['f143fstatus'] == 0) {
                        activityData[i]['f143fstatus'] = 'Pending';
                    } else if (activityData[i]['f143fstatus'] == 1) {
                        activityData[i]['f143fstatus'] = 'Approved';
                    } else {
                        activityData[i]['f143fstatus'] = 'Rejected';
                    }
                }
                this.warrantList = activityData;
            }, error => {
                console.log(error);
            });
    }
}