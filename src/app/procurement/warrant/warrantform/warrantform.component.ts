import { Injector } from '@angular/core';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { StaffService } from "../../../loan/service/staff.service";
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { DepartmentService } from '../../../generalsetup/service/department.service';
import { WarrantService } from "../../service/warrant.service";
import { SupplierregistrationService } from '../../service/supplierregistration.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'WarrantFormComponent',
    templateUrl: 'warrantform.component.html'
})

export class WarrantFormComponent implements OnInit {
    disposalreqform: FormGroup;
    warrantList = [];
    deleteList = [];
    warrantData = {};
    warrantDataheader = {};
    staffList = [];
    supplierList = [];
    ajaxCount: number;
    id: number;
    idview: number;
    DeptList = [];
    accountcodeList = [];
    showLastRow: boolean;
    showLastRowMinus: boolean;
    showLastRowPlus: boolean;
    listshowLastRowMinus: boolean;
    listshowLastRowPlus: boolean;
    viewDisabled: boolean;
    constructor(
        private StaffService: StaffService,
        private WarrantService: WarrantService,
        private route: ActivatedRoute,
        private router: Router,
        private DepartmentService: DepartmentService,
        private SupplierregistrationService: SupplierregistrationService,
        private spinner: NgxSpinnerService,

    ) { }
    showIcons() {
        // this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.warrantList.length > 0) {
            this.showLastRowMinus = true;
            this.showLastRowPlus = true;
            this.listshowLastRowMinus = true;
        }

        if (this.warrantList.length > 1) {
            if (this.showLastRow == false) {
                this.listshowLastRowPlus = false;
                this.listshowLastRowMinus = true;
                this.showLastRowPlus = true;

            } else {
                this.listshowLastRowPlus = true;
                this.listshowLastRowMinus = false;

            }
        }
        if (this.warrantList.length == 1 && this.showLastRow == false) {
            this.listshowLastRowMinus = true;
            this.listshowLastRowPlus = false;

        }
        if (this.warrantList.length == 1 && this.showLastRow == true) {
            this.listshowLastRowMinus = false;
            this.listshowLastRowPlus = true;

        }
        if (this.warrantList.length == 0 && this.showLastRow == false) {
            this.showLastRowMinus = false;
            this.showLastRowPlus = true;
        }
        if (this.warrantList.length == 0 && this.showLastRow == true) {
            this.showLastRowMinus = true;
            this.showLastRowPlus = false;
        }
        if (this.viewDisabled == true ||this.idview==2|| this.warrantDataheader['f143fstatus'] == 1 || this.warrantDataheader['f143fstatus'] == 2) {
            this.listshowLastRowPlus = false;
        }
    }
    deletemodule() {
        this.showLastRow = true;
        this.showIcons();
    }
    showNewRow() {
        var confirmPop = confirm("Do you want to Add?");
        if (confirmPop == false) {
            return false;
        }
        this.showLastRow = false;
        this.showIcons();
    }
    ngDoCheck() {
        const change = this.ajaxCount;
        if (this.ajaxCount == 0) {
            this.editFunction();
            this.spinner.hide();
            this.ajaxCount = 10;
        }
    }
    editFunction() {
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.route.paramMap.subscribe((data) => this.idview = + data.get('idview'));
        if (this.id > 0) {
            this.ajaxCount++;
            this.WarrantService.getItemsDetail(this.id).subscribe(
                data => {
                    this.ajaxCount--;
                    this.warrantDataheader['f143freferenceNumber'] = data['result']['data'][0]['f143freferenceNumber'];
                    this.warrantDataheader['f143fidVendor'] = data['result']['data'][0]['f143fidVendor'];
                    this.warrantDataheader['f143fdepartment'] = data['result']['data'][0]['f143fdepartment'];
                    this.warrantDataheader['f143fdescription'] = data['result']['data'][0]['f143fdescription'];
                    this.warrantDataheader['f143ftotal'] = data['result']['data'][0]['f143ftotal'];
                    this.warrantList = data['result']['data'];
                    this.showIcons();
                    if (data['result']['data'][0]['f143fstatus'] == 1 || this.warrantList[0]['f143fstatus'] == 2) {
                        this.viewDisabled = true;

                        $("#target input,select").prop("disabled", true);
                        $("#target1 input,select").prop("disabled", true);
                    }
                    if (this.idview == 2) {
                        this.viewDisabled = true;

                        $("#target input,select").prop("disabled", true);
                        $("#target1 input,select").prop("disabled", true);
                    }
                }, error => {
                    console.log(error);
                });
        }
    }
    ngOnInit() {
        this.ajaxCount = 0
        this.viewDisabled = false;
        this.spinner.show();
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id < 1) {
            this.showLastRow = false;
            this.showLastRowMinus = false;
            this.showLastRowPlus = true;
        } else {
            this.showLastRow = true;

        }

        // staff dropdown
        this.ajaxCount++;
        this.StaffService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.staffList = data['result']['data'];
            }, error => {
                console.log(error);
            });

        //Department dropdown
        this.ajaxCount++;
        this.DepartmentService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.DeptList = data['result']['data'];
            }
        );
        //Supplierregistration dropdown
        this.ajaxCount++;
        this.SupplierregistrationService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                console.log(this.ajaxCount);

                this.supplierList = data['result']['data'];
            }
        );
        this.showIcons();
    }

    getAmount() {
        var finalAmount = 0;
        for (var i = 0; i < this.warrantList.length; i++) {
            finalAmount = finalAmount + parseFloat(this.warrantList[i]['f144famount']);
        }
        this.warrantDataheader['f143ftotal'] = this.ConvertToFloat(finalAmount).toFixed(2);
    }

    ConvertToFloat(val) {
        return parseFloat(val);
    }
    saveWarrant() {

        if (this.warrantDataheader['f143fidVendor'] == undefined) {
            alert("Select Vendor");
            return false;
        }
        if (this.warrantDataheader['f143fdepartment'] == undefined) {
            alert("Select Department");
            return false;
        }
        if (this.warrantDataheader['f143fdescription'] == undefined) {
            alert("Enter Description");
                return false;
        }
        if (this.showLastRow == false) {
            var addactivities = this.addWarrantList();
            if (addactivities = true) {
                return false;
            }
        }
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        let assetdisposalObject = {};
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            assetdisposalObject['f121fid'] = this.id;
        }
        assetdisposalObject['f143fidVendor'] = this.warrantDataheader['f143fidVendor'];
        assetdisposalObject['f143fdepartment'] = this.warrantDataheader['f143fdepartment'];
        assetdisposalObject['f143fdescription'] = this.warrantDataheader['f143fdescription'];
        assetdisposalObject['f143ftotal'] = this.ConvertToFloat(this.warrantDataheader['f143ftotal']).toFixed(2);
        assetdisposalObject['details'] = this.warrantList;
        for (var i = 0; i < assetdisposalObject['details'].length; i++) {
            assetdisposalObject['details'][i]['f144famount'] = this.ConvertToFloat(assetdisposalObject['details'][i]['f144famount']).toFixed(2);

        }
        this.WarrantService.insertWarrantItems(assetdisposalObject).subscribe(
            data => {
                this.router.navigate(['procurement/warrant']);
                if (this.id > 0) {
                    alert("Warrant has been Updated Sucessfully!");
                }
                else {
                    alert("Warrant has been Added Sucessfully!");
                }
            }, error => {
                console.log(error);
            });

    }
    deleteWarrant(object) {
        var cnf = confirm("Do you really want to delete");
        if (cnf == true) {
            var index = this.warrantList.indexOf(object);
            this.deleteList.push(this.warrantList[index]['f144fid']);
            this.WarrantService.deleteitems(this.warrantList[index]['f144fid']).subscribe(
                data => {
                }, error => {
                    console.log(error);
                });
            if (index > -1) {
                this.warrantList.splice(index, 1);
            }
            this.showIcons();
        }
    }

    addWarrantList() {

        if (this.warrantData['f144fidStaff'] == undefined) {
            alert("Select Staff");
            return false;
        }
        if (this.warrantData['f144fclass'] == undefined) {
            alert("Enter Class");
            return false;
        }
        if (this.warrantData['f144ffrom'] == undefined) {
            alert("Enter From");
            return false;
        }
        if (this.warrantData['f144fto'] == undefined) {
            alert("Enter To");
            return false;
        }
        if (this.warrantData['f144fdate'] == undefined) {
            alert("Select Date");
            return false;
        }
        if (this.warrantData['f144famount'] == undefined) {
            alert("Enter Amount");
            return false;
        }
        var confirmPop = confirm("Do you want to Add?");
        if (confirmPop == false) {
            return false;
        }
        var dataofCurrentRow = this.warrantData;
        this.warrantData = {};
        this.warrantList.push(dataofCurrentRow);
        this.showIcons();
        this.getAmount();
    }
    onlyNumberKey(event) {
        if (event.charCode == 46) {

        } else {
            return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
        }
    }
}
