import { Injector } from '@angular/core';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ItemsubcategoryService } from '../../service/itemsubcategory.service';
import { ItemcategoryService } from '../../service/itemcategory.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'ItemsubcategoryFormComponent',
    templateUrl: 'itemsubcategoryform.component.html'
})

export class ItemsubcategoryFormComponent implements OnInit {

    itemsubcategoryList = [];
    itemsubcategoryData = {};
    itemcategoryList = [];
    ajaxCount: number;
    id: number;
    constructor(

        private ItemsubcategoryService: ItemsubcategoryService,
        private ItemcategoryService: ItemcategoryService,
        private spinner: NgxSpinnerService,
        private route: ActivatedRoute,
        private router: Router

    ) { }
    ngDoCheck() {
        const change = this.ajaxCount;
        if (this.ajaxCount == 0) {
            this.spinner.hide();
        }
    }
    ngOnInit() {
        this.itemsubcategoryData['f028fstatus'] = 1;
        this.itemsubcategoryData['f027fcategoryName'];
        this.ajaxCount = 0;
        this.spinner.show();

        //category dropdown
        this.ajaxCount++;
        this.ItemcategoryService.getActiveItems().subscribe(
            data => {
                this.ajaxCount--;
                this.itemcategoryList = data['result']['data'];
            }, error => {
                console.log(error);
            });

        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        // this.spinner.show();
        if (this.id > 0) {
            this.ajaxCount++;
            this.ItemsubcategoryService.getItemsDetail(this.id).subscribe(
                data => {
                    this.ajaxCount--;
                    // this.spinner.hide();
                    this.itemsubcategoryData = data['result'][0];
                    this.itemsubcategoryData['f028fstatus'] = parseInt(this.itemsubcategoryData['f028fstatus']);

                    console.log(this.itemsubcategoryData);
                }, error => {
                    console.log(error);
                });
        }
    }
    addItemsubcategory() {
        console.log(this.itemsubcategoryData);
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        // this.spinner.show();
        if (this.id > 0) {
            this.ItemsubcategoryService.updateItemsubcategoryItems(this.itemsubcategoryData, this.id).subscribe(
                data => {
                    // this.spinner.hide();
                    if (data['status'] == 409) {
                        alert('Sub Category Already Exist');
                    }
                    else {
                        this.router.navigate(['procurement/itemsubcategory']);
                        alert("Procurement Sub Category has been Updated Sucessfully");
                    }
                }, error => {
                    alert('Procurement Sub Category Already Exist');
                    return false;
                    // console.log(error);
                });
        } else {
            this.ItemsubcategoryService.insertItemsubcategoryItems(this.itemsubcategoryData).subscribe(
                data => {
                    // this.spinner.hide();
                    if (data['status'] == 409) {
                        alert('Procurement Sub Category Already Exist');
                    }
                    else {
                        this.router.navigate(['procurement/itemsubcategory']);
                        alert("Procurement Sub Category has been Added Sucessfully");
                    }
                }, error => {
                    console.log(error);
                });
        }
    }

}