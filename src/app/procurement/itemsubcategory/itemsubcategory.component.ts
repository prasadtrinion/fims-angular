import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ItemsubcategoryService } from '../service/itemsubcategory.service'
import { NgxSpinnerService } from 'ngx-spinner';


@Component({
    selector: 'Itemsubcategory',
    templateUrl: 'itemsubcategory.component.html'
})
export class ItemsubcategoryComponent implements OnInit {
    itemsubcategoryList = [];
    itemsubcategoryData = {};
    id: number;
    constructor(
        private ItemsubcategoryService: ItemsubcategoryService,
        private spinner: NgxSpinnerService,
    ) { }

    ngOnInit() {
        this.spinner.show();
        this.ItemsubcategoryService.getItems().subscribe(
            data => {
                this.spinner.hide();
                this.itemsubcategoryList = data['result']['data'];
            }, error => {
                console.log(error);
            });
    }
}