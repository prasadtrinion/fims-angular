import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { faSearch, faEdit } from '@fortawesome/free-solid-svg-icons';
import { PurchaseorderWarrantyService } from "../service/powarranty.service";
import { NgxSpinnerService } from 'ngx-spinner';
@Component({
    selector: 'PowarrantyComponent',
    templateUrl: 'powarranty.component.html'
})

export class PowarrantyComponent implements OnInit {
    poList =  [];
    faSearch = faSearch;
    faEdit = faEdit;
    id: number;
    title: string;
    type: string;
    constructor(        
        private PurchaseorderWarrantyService: PurchaseorderWarrantyService,
        private route: ActivatedRoute,
        private spinner: NgxSpinnerService,
    ) { }

    ngOnInit() {
        this.spinner.show(); 
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        
        this.PurchaseorderWarrantyService.getItems().subscribe(
            data => {
                this.spinner.hide();
                let activityData = [];
                activityData = data['result']['data'];
                console.log("asdf");
                for (var i = 0; i < activityData.length; i++) {
                    if (activityData[i]['f034fapprovalStatus'] == 0) {
                        activityData[i]['f034fapprovalStatus'] = 'Pending';
                    } else if (activityData[i]['f034fapprovalStatus'] == 1) {
                        activityData[i]['f034fapprovalStatus'] = 'Approved';
                    } else {
                        activityData[i]['f034fapprovalStatus'] = 'Rejected';
                    }
                }
                this.poList = activityData;
        }, error => {
            console.log(error);
        });
    }

   
}