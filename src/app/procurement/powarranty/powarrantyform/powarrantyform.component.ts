
import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PurchaseorderWarrantyService } from "../../service/powarranty.service";
import { PurchaseorderService } from '../../service/purchaseorder.service';
import { TaxsetupcodeService } from '../../../generalledger/service/taxsetupcode.service'
import { ItemsetupService } from '../../../procurement/service/itemsetup.service';
import { AlertService } from '../../../_services/alert.service';
import { FinancialyearService } from '../../../generalledger/service/financialyear.service';
import { SupplierregistrationService } from '../../service/supplierregistration.service';
import { PurchaserequistionentryService } from '../../service/purchaserequistionentry.service';
import { FundService } from '../../../generalsetup/service/fund.service';
import { AccountcodeService } from '../../../generalsetup/service/accountcode.service';
import { ActivitycodeService } from '../../../generalsetup/service/activitycode.service';
import { DepartmentService } from '../../../generalsetup/service/department.service';
import { PrapprovalService } from '../../service/prapproval.service';
import { PoapprovalService } from "../../service/poapproval.service";

import { BudgetactivitiesService } from "../../../budget/service/budgetactivities.service";
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'PowarrantyformComponent',
    templateUrl: 'powarrantyform.component.html'
})

export class PowarrantyformComponent implements OnInit {
    purchaseorderList = [];
    purchaseorderData = {};
    purchaseorderDataheader = {};
    DeptList = [];
    accountcodeList = [];
    fundList = [];
    activitycodeList = [];
    itemUnitList = [];
    itemList = [];
    taxcodeList = [];
    departmentList = [];
    financialList = [];
    supplierList = [];
    purchaseMethods = [];
    fordertypeobj = [];
    purchaseRequisitionListBasedonId = [];
    purchaseRequisitionList = [];
    budgetcostcenterList = [];
    costCenterFund = [];
    costCenterAccount = [];
    costCenterActivity = [];
    poapprovalList = [];
    poapprovalData = {};
    approvalData = {};
    taxcodepercenList = [];
    deleteList = [];
    costCenterList = [];
    id: number;
    valueDate: string;
    ajaxCount: number;
    gstValue: number;
    Amount: number;
    taxAmount: number;
    showLastRow: boolean;
    showLastRowMinus: boolean;
    showLastRowPlus: boolean;
    listshowLastRowMinus: boolean;
    listshowLastRowPlus: boolean;
    viewDisabled: boolean;
    editDisabled: boolean;
    saveBtnDisable: boolean;
    idview: number;
    //totalAmount: number;
    type: string;

    constructor(

        private PurchaserequistionentryService: PurchaserequistionentryService,
        private PurchaseorderService: PurchaseorderService,
        private PrapprovalService: PrapprovalService,
        private TaxsetupcodeService: TaxsetupcodeService,
        private ActivitycodeService: ActivitycodeService,
        private AccountcodeService: AccountcodeService,
        private FundService: FundService,
        private ItemService: ItemsetupService,
        private DepartmentService: DepartmentService,
        private FinancialyearService: FinancialyearService,
        private SupplierregistrationService: SupplierregistrationService,
        private PoapprovalService: PoapprovalService,
        private BudgetactivitiesService: BudgetactivitiesService,
        private spinner: NgxSpinnerService,
        private route: ActivatedRoute,
        private router: Router

    ) { }


    ngDoCheck() {
        const change = this.ajaxCount;

        // console.log(this.ajaxCount);
        if (this.ajaxCount == 0) {
            this.editFunction();
            this.ajaxCount = 10;
            this.spinner.hide();
        }
        this.route.paramMap.subscribe((data) => this.idview = + data.get('idview'));
        if (this.ajaxCount == 0 && this.purchaseorderList.length > 0) {
            this.ajaxCount = 20;
            console.log("asdfsdf");
            // this.showIcons();
            //console.log(this.newbudgetactivitiesList[0]);
            if (this.purchaseorderList[0]['f034fapprovalStatus'] == 1 || this.purchaseorderList[0]['f034fapprovalStatus'] == 2) {
                this.saveBtnDisable = true;
                this.viewDisabled = true;
                this.listshowLastRowPlus = false;

                $("#target input,select").prop("disabled", true);
                $("#target1 input,select").prop("disabled", true);
            }
        }
        if (this.ajaxCount == 0 && this.purchaseorderList.length > 0) {
            this.ajaxCount = 20;
            if (this.idview > 1) {
                this.saveBtnDisable = true;
                this.viewDisabled = true;
                this.listshowLastRowPlus = false;

                $("#target input,select").prop("disabled", true);
                $("#target1 input,select").prop("disabled", true);
            }
        }
    }

    editFunction() {
        this.ajaxCount++;
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.route.paramMap.subscribe((data) => this.idview = + data.get('idview'));
        if (this.id > 0) {
            this.PurchaseorderService.getItemsDetail(this.id).subscribe(
                data => {
                    this.ajaxCount--;
                    this.purchaseorderDataheader = data['result'][0];
                    this.purchaseorderList = data['result'];
                    this.PurchaserequistionentryService.getItems().subscribe(
                        data => {
                            this.ajaxCount--;
                            this.purchaseRequisitionList = data['result']['data'];
                            // console.log(this.purchaseRequisitionList);
                        }, error => {
                            console.log(error);
                        });
                    console.log(data)
                    this.purchaseorderDataheader['f034fidFinancialyear'] = data['result'][0]['f034fidFinancialyear'];
                    this.purchaseorderDataheader['f034fidSupplier'] = data['result'][0]['f034fidSupplier'];
                    this.purchaseorderDataheader['f034fidDepartment'] = data['result'][0]['f034fidDepartment'];
                    this.purchaseorderDataheader['f034fexpiredDate'] = data['result'][0]['f034fexpiredDate'];
                    this.purchaseorderDataheader['f034fdescription'] = data['result'][0]['f034fdescription'];
                    this.purchaseorderDataheader['f034fidPurchaseRequisition'] = parseInt(data['result'][0]['f034fidPurchasereq']);
                    this.purchaseorderDataheader['f034ftotalAmount'] = data['result'][0]['f034ftotalAmount'];
                    if (data['result'][0]['f034fapprovalStatus'] == 1 || this.purchaseorderList[0]['f034fapprovalStatus'] == 2) {
                        this.viewDisabled = true;

                        $("#target input,select").prop("disabled", true);
                        $("#target1 input,select").prop("disabled", true);
                    }
                    if (this.idview == 2 || this.idview == 3 || this.idview == 4) {
                        this.viewDisabled = true;

                        $("#target input,select").prop("disabled", true);
                        $("#target1 input,select").prop("disabled", true);
                    }
                    console.log(this.purchaseorderList);
                    this.PrapprovalService.getItems().subscribe(
                        data => {
                            this.ajaxCount--;
                            this.purchaseRequisitionList = data['result']['data'];
                            // console.log(this.purchaseRequisitionList);

                        }, error => {
                            console.log(error);
                        });
                    this.editDisabled = true;
                    this.getGlList();
                    this.onBlurMethod();
                    this.showIcons();
                    this.getDepartment();
                }, error => {
                    console.log(error);
                });
        }
    }
    showIcons() {
        // this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.purchaseorderList.length > 0) {
            this.showLastRowMinus = true;
            this.showLastRowPlus = true;
            this.listshowLastRowMinus = true;
        }

        if (this.purchaseorderList.length > 1) {
            if (this.showLastRow == false) {
                this.listshowLastRowPlus = false;
                this.listshowLastRowMinus = true;
                this.showLastRowPlus = true;

            } else {
                this.listshowLastRowPlus = true;
                this.listshowLastRowMinus = false;

            }
        }
        if (this.purchaseorderList.length == 1 && this.showLastRow == false) {
            this.listshowLastRowMinus = true;
            this.listshowLastRowPlus = false;

        }
        if (this.purchaseorderList.length == 1 && this.showLastRow == true) {
            this.listshowLastRowMinus = false;
            this.listshowLastRowPlus = true;

        }
        if (this.purchaseorderList.length == 0 && this.showLastRow == false) {
            this.showLastRowMinus = false;
            this.showLastRowPlus = true;
        }
        if (this.purchaseorderList.length == 0 && this.showLastRow == true) {
            this.showLastRowMinus = true;
            this.showLastRowPlus = false;
        }
        console.log(this.purchaseorderList.length);
        console.log("Show last row" + this.showLastRow);
        console.log("showLastRowMinus" + this.showLastRowMinus);
        console.log("showLastRowPlus" + this.showLastRowPlus);
        console.log("listshowLastRowMinus" + this.listshowLastRowMinus);
        console.log("listshowLastRowPlus" + this.listshowLastRowPlus);

        if (this.viewDisabled == true || this.purchaseorderDataheader['f034fapprovalStatus'] == 1 || this.purchaseorderDataheader['f034fapprovalStatus'] == 2) {
            this.listshowLastRowPlus = false;
        }

    }
    deletemodule() {
        this.showLastRow = true;
        this.showIcons();
    }
    showNewRow() {
        var confirmPop = confirm("Do you want to Add?");
        if (confirmPop == false) {
            return false;
        }
        this.showLastRow = false;
        this.showIcons();
    }
    sixMonthsDiff() {

        console.log(this.purchaseorderDataheader['f034forderDate']);

        var orderDate = new Date(this.purchaseorderDataheader['f034forderDate'].setMonth(this.purchaseorderDataheader['f034forderDate'].getMonth() + 6)).toISOString().substr(0, 10);
        console.log(orderDate);

    }


    ngOnInit() {
        this.ajaxCount = 0;
        this.spinner.show();
        this.viewDisabled = false;
        this.editDisabled = false;
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id < 1) {
            this.showLastRow = false;
            this.showLastRowMinus = false;
            this.showLastRowPlus = true;
        } else {
            this.showLastRow = true;

        }

        console.log(this.id);

        let ordertypeobj = {};
        ordertypeobj['name'] = 'Warrant';
        this.fordertypeobj.push(ordertypeobj);
        this.purchaseorderDataheader['f034forderType'] = ordertypeobj['name']
        // ordertypeobj = {};
        // ordertypeobj['name'] = 'Staffs';
        // this.fordertypeobj.push(ordertypeobj);
        this.onBlurMethod();
        this.purchaseorderDataheader['f034forderDate'] = new Date().toISOString().substr(0, 10);
        this.purchaseorderDataheader['f034fexpiredDate'] = new Date(new Date().setMonth(new Date().getMonth() + 6)).toISOString().substr(0, 10);

        //purchase requisition dropdown
        if (this.id < 1) {
            this.ajaxCount++;
            this.PrapprovalService.getWarrantItems().subscribe(
                data => {
                    this.ajaxCount--;
                    this.purchaseRequisitionList = data['result'];
                }, error => {
                    console.log(error);
                });
        }
        //item dropdown
        this.ajaxCount++;
        this.ItemService.getAllItems().subscribe(
            data => {
                this.ajaxCount--;
                console.log(this.ajaxCount);

                this.itemList = data['result']['data'];

            }
        );
        //Supplierregistration dropdown
        this.ajaxCount++;
        this.SupplierregistrationService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                console.log(this.ajaxCount);

                this.supplierList = data['result']['data'];
            }
        );
        //Department dropdown
        this.ajaxCount++;
        this.DepartmentService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.DeptList = data['result']['data'];
            }
        );
        //Activity dropdown
        this.ajaxCount++;
        this.ActivitycodeService.getItemsByLevel().subscribe(
            data => {
                this.ajaxCount--;
                this.activitycodeList = data['result']['data'];
            }
        );
        //Account dropdown
        this.ajaxCount++;
        this.AccountcodeService.getItemsByLevel().subscribe(
            data => {
                this.ajaxCount--;
                this.accountcodeList = data['result']['data'];
            }
        );
        //Fund dropdown
        this.ajaxCount++;
        this.FundService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.fundList = data['result']['data'];
            }
        );
        //Financial year dropdown
        this.ajaxCount++;
        this.FinancialyearService.getActiveItems().subscribe(
            data => {
                this.ajaxCount--;
                console.log(this.ajaxCount);

                this.financialList = data['result'];
            }
        );
        //Department dropdown
        this.ajaxCount++;
        this.DepartmentService.getZerodepartment().subscribe(
            data => {
                this.ajaxCount--;
                console.log(this.ajaxCount);

                this.departmentList = data['result']['data'];
            }
        );
        this.ajaxCount++;
        this.TaxsetupcodeService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.taxcodeList = data['result']['data'];
            });
    }
    getGlList() {
        var idfinancialYear = this.purchaseorderDataheader['f034fidFinancialyear'];

        this.PurchaserequistionentryService.urlgetAllpurcahseGls(idfinancialYear).subscribe(
            data => {
                var depArray = data['result']['Department'];
                this.budgetcostcenterList = depArray;

                this.costCenterFund = data['result']['Fund'];

                this.costCenterAccount = data['result']['Account'];

                this.costCenterActivity = data['result']['Activity'];

                console.log(data);
            }, error => {
                console.log(error);
            }
        );
    }
    getDepartment() {

        var departmentId = this.purchaseorderDataheader['f034fidDepartment'];
        var departmentCode = departmentId.substring(0, 3);;

        this.BudgetactivitiesService.getCostCenterBasedOnDepartmentCode(departmentCode).subscribe(
            data => {
                this.costCenterList = data['result']['data'];
            }, error => {
                console.log(error);
            });
    }
    getPurchaseOrderNumber() {
        let typeObject = {};
        typeObject['type'] = this.purchaseorderDataheader['f034forderType'];
        this.type = this.purchaseorderDataheader['f034forderType']
        // console.log(typeObject);

        this.PurchaseorderService.getPurchaseOrderNumber(typeObject).subscribe(
            data => {
                this.purchaseorderDataheader['f034freferenceNumber'] = data['number'];
                this.valueDate = new Date().toISOString().substr(0, 10);
                this.purchaseorderDataheader['f034forderDate'] = this.valueDate;
            }
        );
        return this.type;
    }

    ConvertToFloat(val) {
        return parseFloat(val);
    }
    getPurchaseOrderDetails() {


        let PurchaseOrderId = this.purchaseorderDataheader['f034fidPurchaseRequisition'];
        this.purchaseorderList = [];
        this.PurchaserequistionentryService.getItemsDetail(PurchaseOrderId).subscribe(
            data => {
                this.purchaseRequisitionListBasedonId = data['result'];
                this.purchaseorderDataheader['f034forderType'] = data['result'][0]['f088forderType'];
                this.purchaseorderDataheader['f034fidFinancialyear'] = data['result'][0]['f088fidFinancialyear'];
                this.purchaseorderDataheader['f034fidSupplier'] = data['result'][0]['f088fidSupplier'];
                this.purchaseorderDataheader['f034fidDepartment'] = data['result'][0]['f088fidDepartment'];
                this.purchaseorderDataheader['f034fdescription'] = data['result'][0]['f088fdescription'];
                this.purchaseorderDataheader['f034ftotalAmount'] = data['result'][0]['f088ftotalAmount'];
                this.getGlList();

                for (var i = 0; i < this.purchaseRequisitionListBasedonId.length; i++) {
                    var purchaseOrderObject = {}

                    purchaseOrderObject['f034fidItem'] = this.purchaseRequisitionListBasedonId[i]['f088fidItem'];
                    purchaseOrderObject['f034funit'] = this.purchaseRequisitionListBasedonId[i]['f088funit'];
                    purchaseOrderObject['f034ffundCode'] = this.purchaseRequisitionListBasedonId[i]['f088ffundCode'];
                    purchaseOrderObject['f034factivityCode'] = this.purchaseRequisitionListBasedonId[i]['f088factivityCode'];
                    purchaseOrderObject['f034fdepartmentCode'] = this.purchaseRequisitionListBasedonId[i]['f088fdepartmentCode'];
                    purchaseOrderObject['f034faccountCode'] = this.purchaseRequisitionListBasedonId[i]['f088faccountCode'];
                    purchaseOrderObject['f034fbudgetFundCode'] = this.purchaseRequisitionListBasedonId[i]['f088fbudgetFundCode'];
                    purchaseOrderObject['f034fbudgetActivityCode'] = this.purchaseRequisitionListBasedonId[i]['f088fbudgetActivityCode'];
                    purchaseOrderObject['f034fbudgetDepartmentCode'] = this.purchaseRequisitionListBasedonId[i]['f088fbudgetDepartmentCode'];
                    purchaseOrderObject['f034fbudgetAccountCode'] = this.purchaseRequisitionListBasedonId[i]['f088fbudgetAccountCode'];
                    purchaseOrderObject['f034fsoCode'] = this.purchaseRequisitionListBasedonId[i]['f088fsoCode'];
                    purchaseOrderObject['f034frequiredDate'] = this.purchaseRequisitionListBasedonId[i]['f088frequiredDate'];
                    purchaseOrderObject['f034fquantity'] = this.purchaseRequisitionListBasedonId[i]['f088fquantity'];
                    purchaseOrderObject['f034fprice'] = this.ConvertToFloat(this.purchaseRequisitionListBasedonId[i]['f088fprice']).toFixed(2);
                    purchaseOrderObject['f034total'] = this.ConvertToFloat(this.purchaseRequisitionListBasedonId[i]['f088total']).toFixed(2);
                    purchaseOrderObject['f034ftaxCode'] = this.purchaseRequisitionListBasedonId[i]['f088ftaxCode'];
                    purchaseOrderObject['f034fpercentage'] = this.purchaseRequisitionListBasedonId[i]['f088fpercentage'];
                    purchaseOrderObject['f034ftaxAmount'] = this.ConvertToFloat(this.purchaseRequisitionListBasedonId[i]['f088ftaxAmount']).toFixed(2);;
                    purchaseOrderObject['f034ftotalIncTax'] = this.ConvertToFloat(this.purchaseRequisitionListBasedonId[i]['f088ftotalIncTax']).toFixed(2);

                    purchaseOrderObject['categoryName'] = this.purchaseRequisitionListBasedonId[i]['categoryName'];
                    purchaseOrderObject['subCategoryName'] = this.purchaseRequisitionListBasedonId[i]['subCategoryName'];
                    purchaseOrderObject['itemName'] = this.purchaseRequisitionListBasedonId[i]['itemName'];
                    purchaseOrderObject['f034fitemType'] = this.purchaseRequisitionListBasedonId[i]['f088fcategoryType'];
                    purchaseOrderObject['f034fitemCategory'] = this.purchaseRequisitionListBasedonId[i]['f088fitemCategory'];
                    purchaseOrderObject['f034fitemSubCategory'] = this.purchaseRequisitionListBasedonId[i]['f088fitemSubCategory'];
                    purchaseOrderObject['f034fassetCode'] = this.purchaseRequisitionListBasedonId[i]['f088fassetCode'];

                    // purchaseOrderObject['f034ftotalIncTax'] = this.ConvertToFloat(this.purchaseRequisitionListBasedonId[i]['f088ftotalIncTax']).toFixed(2);

                    this.purchaseorderList.push(purchaseOrderObject);
                }
                this.showLastRow = true;
                this.showIcons();
                this.getDepartment();
                console.log(this.purchaseorderList);
            }
        );
    }
    getUOM() {
        let itemId = this.purchaseorderData['f034fidItem'];
        console.log(itemId);
        this.ItemService.getItemsDetail(itemId).subscribe(
            data => {
                this.itemUnitList = data['result'][0];
                console.log(data);
                this.purchaseorderData['f034funit'] = data['result'][0]['f029funit'];
            }
        );
    }
    getPercentage() {
        let percenId = this.purchaseorderData['f034ftaxCode'];
        console.log(percenId);
        this.TaxsetupcodeService.getItemsDetail(percenId).subscribe(
            data => {
                this.taxcodepercenList = data['result'];

                this.purchaseorderData['f034fpercentage'] = data['result'][0]['f034fpercentage'];
            }
        );
    }
    amountprefill() {
        // console.log(this.purchaserequistionentryList);
        var finaltotal = 0;
        if (this.purchaseorderData['f034fprice'] != undefined) {
            var totalamount = (this.ConvertToFloat(this.purchaseorderData['f034fquantity'])) * (this.ConvertToFloat(this.purchaseorderData['f034fprice']));
            console.log(totalamount);
            var gstamount = (this.ConvertToFloat(this.purchaseorderData['f034fpercentage']) / 100 * (totalamount));
            console.log(gstamount);
            this.purchaseorderData['f034total'] = totalamount;
            if (this.purchaseorderData['f034ftaxCode'] != undefined) {
                this.getGstvalue();
            }
        }
    }
    listamountprefill() {
        // console.log(this.purchaserequistionentryList);
        for (let i = 0; i < this.purchaseorderList.length; i++) {
            if (this.purchaseorderList[i]['f088fprice'] != undefined) {
                var totalamount = (this.ConvertToFloat(this.purchaseorderList[i]['f034fquantity'])) * (this.ConvertToFloat(this.purchaseorderList[i]['f034fprice']));
                console.log(totalamount);
                var gstamount = (this.ConvertToFloat(this.purchaseorderList[i]['f034fpercentage']) / 100 * (totalamount));
                console.log(gstamount);
                this.purchaseorderList[i]['f034total'] = totalamount;
                if (this.purchaseorderList[i]['f034ftaxCode'] != undefined) {
                    let taxId = this.purchaseorderList[i]['f034ftaxCode'];
                    let quantity = this.purchaseorderList[i]['f034fquantity'];
                    let price = this.purchaseorderList[i]['f034fprice'];

                    var taxSelectedObject = {};
                    for (var k = 0; k < this.taxcodeList.length; k++) {
                        if (this.taxcodeList[k]['f034fid'] == taxId) {
                            taxSelectedObject = this.taxcodeList[k];
                        }
                    }
                    this.gstValue = taxSelectedObject['f081fpercentage'];
                    this.purchaseorderList[i]['f088fpercentage'] = this.gstValue;
                    this.Amount = (this.ConvertToFloat(price)) * (this.ConvertToFloat(quantity));
                    this.taxAmount = (this.ConvertToFloat(this.gstValue)) / 100 * this.ConvertToFloat((this.Amount));

                    let totalAm = ((this.taxAmount) + (this.Amount));
                    // this.purchaserequistionentryData['f088total'] = this.Amount.toFixed(2);
                    this.purchaseorderList[i]['f088ftaxAmount'] = this.taxAmount.toFixed(2);
                    this.purchaseorderList[i]['f088ftotalIncTax'] = totalAm.toFixed(2);
                }
            }
        }

    }
    savePurchaseOrder() {
        this.onBlurMethod();
        if (this.showLastRow == false) {
            var addactivities = this.addEntrylist();
            if (addactivities = false) {
                return false;
            }
        }
        let purchaseOrderObject = {};
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        console.log(this.id);
        if (this.id > 0) {
            purchaseOrderObject['f034fid'] = this.id;
        }

        if (this.purchaseorderList.length > 0) {

        } else {
            alert("Please add one details");
            return false;
        }
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }

        purchaseOrderObject['f034forderType'] = this.purchaseorderDataheader['f034forderType'];
        purchaseOrderObject['f034forderDate'] = this.purchaseorderDataheader['f034forderDate'];
        purchaseOrderObject['f034fidFinancialyear'] = this.purchaseorderDataheader['f034fidFinancialyear'];
        purchaseOrderObject['f034fidSupplier'] = this.purchaseorderDataheader['f034fidSupplier'];
        purchaseOrderObject['f034fdescription'] = this.purchaseorderDataheader['f034fdescription'];
        purchaseOrderObject['f034freferenceNumber'] = this.purchaseorderDataheader['f034freferenceNumber'];
        purchaseOrderObject['f034fidDepartment'] = this.purchaseorderDataheader['f034fidDepartment'];
        purchaseOrderObject['f034fidPurchaseRequisition'] = this.purchaseorderDataheader['f034fidPurchaseRequisition'];
        purchaseOrderObject['f034ftotalAmount'] = this.purchaseorderDataheader['f034ftotalAmount'];
        purchaseOrderObject['f034fexpiredDate'] = this.purchaseorderDataheader['f034fexpiredDate'];
        purchaseOrderObject['f034fstatus'] = 0;
        if (this.id > 0) {
            purchaseOrderObject['f034fidDetails'] = this.id;
        }
        purchaseOrderObject['purchase-details'] = this.purchaseorderList;

        if (this.id > 0) {
            this.PurchaseorderService.updateEntryItems(purchaseOrderObject, this.id).subscribe(
                data => {
                    this.router.navigate(['procurement/powarranty']);
                    alert("Warrant has been Updated Successfully");
                }, error => {
                    console.log(error);
                });
        } else {
            this.PurchaseorderService.insertEntryItems(purchaseOrderObject).subscribe(
                data => {
                    this.router.navigate(['procurement/powarranty']);
                    alert("Warrant has been Added Successfully");

                }, error => {
                    console.log(error);
                });
        }
    }

    getGstvalue() {
        let taxId = this.purchaseorderData['f034ftaxCode'];
        let quantity = this.purchaseorderData['f034fquantity'];
        let price = this.purchaseorderData['f034fprice'];
        //console.log(this.taxcodeList);
        var taxSelectedObject = {};
        for (var k = 0; k < this.taxcodeList.length; k++) {
            if (this.taxcodeList[k]['f081fid'] == taxId) {
                taxSelectedObject = this.taxcodeList[k];
            }
        }
        this.gstValue = taxSelectedObject['f081fpercentage'];
        this.purchaseorderData['f034fpercentage'] = this.gstValue;
        this.Amount = (this.ConvertToFloat(price)) * (this.ConvertToFloat(quantity));
        this.taxAmount = (this.ConvertToFloat(this.gstValue)) / 100 * this.ConvertToFloat((this.Amount));

        let totalAm = ((this.taxAmount) + (this.Amount));
        this.purchaseorderData['f034total'] = this.Amount.toFixed(2);
        this.purchaseorderData['f034ftaxAmount'] = this.taxAmount.toFixed(2);
        this.purchaseorderData['f034ftotalIncTax'] = totalAm.toFixed(2);

        console.log(this.purchaseorderData);
    }



    onBlurMethod() {
        console.log(this.purchaseorderList);
        var finaltotal = 0;
        console.log(finaltotal);
        for (var i = 0; i < this.purchaseorderList.length; i++) {
            var totalamount = (this.ConvertToFloat(this.purchaseorderList[i]['f034fquantity'])) * (this.ConvertToFloat(this.purchaseorderList[i]['f034fprice']));
            console.log(totalamount);
            var gstamount = (this.ConvertToFloat(this.purchaseorderList[i]['f034fpercentage']) / 100 * (totalamount));
            console.log(gstamount);
            this.purchaseorderList[i]['f034total'] = totalamount;

            this.purchaseorderList[i]['f034ftotalIncTax'] = gstamount + totalamount;
            console.log(finaltotal);
            finaltotal = finaltotal + this.purchaseorderList[i]['f034ftotalIncTax'];
            console.log(finaltotal);
        }

        this.purchaseorderDataheader['f034ftotalAmount'] = this.ConvertToFloat(finaltotal.toFixed(2));
    }

    deleteEntry(object) {
        var cnf = confirm("Do you really want to delete");
        if (cnf == true) {
            console.log(this.purchaseorderList);
            var index = this.purchaseorderList.indexOf(object);
            this.deleteList.push(this.purchaseorderList[index]['f034fidDetails']);
            let deleteObject = {};
            deleteObject['id'] = this.deleteList;
            this.PurchaseorderService.deleteItems(deleteObject).subscribe(
                data => {
                }, error => {
                    console.log(error);
                });
            if (index > -1) {
                this.purchaseorderList.splice(index, 1);
            }
            this.onBlurMethod();
            this.showIcons();
        }
    }

    addEntrylist() {
        console.log(this.purchaseorderData);
        if (this.purchaseorderDataheader['f034forderType'] == undefined) {
            alert('Select Order Type');
            return false;
        }
        if (this.purchaseorderDataheader['f034forderDate'] == undefined) {
            alert('Select Ordered Date');
            return false;
        }
        if (this.purchaseorderDataheader['f034fidPurchaseRequisition'] == undefined) {
            alert('Select Requisition Reference Number');
            return false;
        }
        if (this.purchaseorderDataheader['f034fidFinancialyear'] == undefined) {
            alert('Select Financial Year');
            return false;
        }
        if (this.purchaseorderDataheader['f034fidSupplier'] == undefined) {
            alert('Select Vendor');
            return false;
        }
        if (this.purchaseorderDataheader['f034fidDepartment'] == undefined) {
            alert('Select Cost Center');
            return false;
        }
        if (this.purchaseorderDataheader['f034fexpiredDate'] == undefined) {
            alert('Select Expired Date');
            return false;
        }
        if (this.purchaseorderDataheader['f034fdescription'] == undefined) {
            alert('Enter the Description');
            return false;
        }

        if (this.purchaseorderData['f034fidItem'] == undefined) {
            alert('Select Item');
            return false;
        }
        if (this.purchaseorderData['f034ffundCode'] == undefined) {
            alert('Select GL Fund');
            return false;
        }
        if (this.purchaseorderData['f034factivityCode'] == undefined) {
            alert('Select GL Activity Code');
            return false;
        }
        if (this.purchaseorderData['f034fdepartmentCode'] == undefined) {
            alert('Select GL Cost Center Code');
            return false;
        }
        if (this.purchaseorderData['f034faccountCode'] == undefined) {
            alert('Select GL Account Code');
            return false;
        }
        if (this.purchaseorderData['f034fbudgetFundCode'] == undefined) {
            alert('Select Budget Fund');
            return false;
        }
        if (this.purchaseorderData['f034fbudgetActivityCode'] == undefined) {
            alert('Select Budget Activity Code');
            return false;
        }
        if (this.purchaseorderData['f034fbudgetDepartmentCode'] == undefined) {
            alert('Select Budget Cost Center Code');
            return false;
        }
        if (this.purchaseorderData['f034fbudgetAccountCode'] == undefined) {
            alert('Select Budget Account Code');
            return false;
        }
        // if (this.purchaseorderData['f034fsoCode'] == undefined) {
        //     alert('Enter the SO Code');
        //     return false;
        // }
        if (this.purchaseorderData['f034frequiredDate'] == undefined) {
            alert('Select Required Date');
            return false;
        }
        if (this.purchaseorderData['f034fquantity'] == undefined) {
            alert('Enter the Quantity');
            return false;
        }
        var confirmPop = confirm("Do you want to Add?");
        if (confirmPop == false) {
            return false;
        }
        if (this.purchaseorderData['f034fbudgetAccountCode'] === undefined) {
            alert("Select Account Code to Continue !!")
        } else {

            if (this.purchaseorderData['f034fbudgetAccountCode'] === this.purchaseorderData['f034faccountCode']) {
                alert("Account Code cannot be same !!");

                console.log(this.purchaseorderData['f034fbudgetAccountCode']);
                console.log(this.purchaseorderData['f034faccountCode']);
                $(this.purchaseorderData['f034fbudgetAccountCode']).focus()
                this.purchaseorderData['f034fbudgetAccountCode'] = undefined;
            } else {

                var dataofCurrentRow = this.purchaseorderData;
                this.purchaseorderData = {};
                this.purchaseorderList.push(dataofCurrentRow);
                this.onBlurMethod();
                this.amountprefill();
                this.showIcons();
            }
        }
    }
    onlyNumberKey(event) {
        //console.log(event);
        if (event.charCode == 46) {

        } else {
            return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
        }
    }
    getListData() {
        this.PoapprovalService.getItems().subscribe(
            data => {
                this.poapprovalList = data['result'];
            }, error => {
                console.log(error);
            });
    }
    PrapprovalListData() {
        console.log(this.poapprovalList);
        var approvaloneIds = [];
        for (var i = 0; i < this.poapprovalList.length; i++) {
            if (this.poapprovalList[i].f034fapprovalStatus == true) {
                approvaloneIds.push(this.poapprovalList[i].f034fid);
            }
        }
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        approvaloneIds.push(this.id);
        var confirmPop = confirm("Do you want to Approve?");
        if (confirmPop == false) {
            return false;
        }
        var approvaloneUpdate = {};
        approvaloneUpdate['id'] = approvaloneIds;
        approvaloneUpdate['status'] = 1;
        approvaloneUpdate['reason'] = '';
        console.log(approvaloneIds);
        this.PoapprovalService.updatePoapprovalItems(approvaloneUpdate).subscribe(
            data => {

                this.router.navigate(['procurement/poapproval']);
                this.getListData();
                this.approvalData['reason'] = '';
                alert(" Approved Sucessfully ! ");

            }, error => {
                console.log(error);
            });
    }
    regectApprovalListData() {
        console.log(this.poapprovalList);
        var approvaloneIds = [];
        for (var i = 0; i < this.poapprovalList.length; i++) {
            if (this.poapprovalList[i].f034fapprovalStatus == true) {
                approvaloneIds.push(this.poapprovalList[i].f034fid);
            }
        }
        if (this.approvalData['reason'] == undefined) {
            alert('Please Enter the Description');
            return false;
        }
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        approvaloneIds.push(this.id);
        var confirmPop = confirm("Do you want to Reject?");
        if (confirmPop == false) {
            return false;
        }
        var budgetActivityDataObject = {}
        budgetActivityDataObject['id'] = approvaloneIds;
        budgetActivityDataObject['status'] = 2;
        budgetActivityDataObject['reason'] = this.approvalData['reason'];
        this.PoapprovalService.updatePoapprovalItems(budgetActivityDataObject).subscribe(
            data => {
                this.getListData();
                this.router.navigate(['procurement/poapproval']);
                this.approvalData['reason'] = '';
                alert(" Rejected Sucessfully ! ");
            }, error => {
                console.log(error);
            });
    }
    maximize() {
        $("#myModal").addClass("modal");
        $("#overlay1").show();
        $(".maximize-icon").addClass("hidden");
        $(".minimize-icon").removeClass("hidden");
        $(".modal-backdrop").show();

    }
    minimize() {
        $("#myModal").removeClass("modal");
        $("#overlay1").hide();
        $(".maximize-icon").removeClass("hidden");
        $(".minimize-icon").addClass("hidden");
        $(".modal-backdrop").hide();
        $('#myModal').show();
    }
}