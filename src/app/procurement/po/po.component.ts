import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { faSearch, faEdit } from '@fortawesome/free-solid-svg-icons';
import { PurchaseorderService } from "../service/purchaseorder.service";
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'PoComponent',
    templateUrl: 'po.component.html'
})

export class PoComponent implements OnInit {
    poList =  [];
    faSearch = faSearch;
    faEdit = faEdit;
    id: number;
    title: string;
    type: string;
    constructor(        
        private PurchaseorderService: PurchaseorderService,
        private route: ActivatedRoute,
        private spinner: NgxSpinnerService
    ) { }

    ngOnInit() { 
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.spinner.show();        
        this.PurchaseorderService.getItems().subscribe(
            data => {
                this.spinner.hide();
                let activityData = [];
                activityData = data['result']['data'];
                console.log("asdf");
                for (var i = 0; i < activityData.length; i++) {
                    if (activityData[i]['f034fapprovalStatus'] == 0) {
                        activityData[i]['f034fapprovalStatus'] = 'Pending';
                    } else if (activityData[i]['f034fapprovalStatus'] == 1) {
                        activityData[i]['f034fapprovalStatus'] = 'Approved';
                    } else {
                        activityData[i]['f034fapprovalStatus'] = 'Rejected';
                    }
                }
                this.poList = activityData;
        }, error => {
            console.log(error);
        });
    }

   
}