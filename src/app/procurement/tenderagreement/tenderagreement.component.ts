import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { TenderquatationService } from '../service/tenderquotation.service'
import { faSearch, faEdit } from '@fortawesome/free-solid-svg-icons';
import { NgxSpinnerService } from 'ngx-spinner';
import { environment } from '../../../environments/environment';

@Component({
    selector: 'TenderagreementComponent',
    templateUrl: 'tenderagreement.component.html'
})

export class TenderagreementComponent implements OnInit {
    tenderQuotationList =  [];
    tenderQuotationData = {};
    faSearch = faSearch;
    faEdit = faEdit;
    id: number;
    downloadUrl: string = environment.api.downloadbase;
    constructor(
        
        private TenderquatationService: TenderquatationService,
        private spinner: NgxSpinnerService

    ) { }
 
    generatePdf(id){
        this.spinner.hide();
        this.TenderquatationService.downloadAgreement(id).subscribe(
            data => { 
                console.log(data['name']);
                window.open(this.downloadUrl + data['name']); 
            }, error => {
                console.log(error);
            });
    }
    ngOnInit() { 
        this.spinner.show();
        this.TenderquatationService.getAgreementItems().subscribe(
            data => {
                this.spinner.hide();
                this.tenderQuotationList = data['result']['data'];
        }, error => {
            console.log(error);
        });
    }

    
}