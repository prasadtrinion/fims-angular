import { Injector } from '@angular/core';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LicenseService } from '../../service/license.service'
import { AlertService } from '../../../_services/alert.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'LicenseFormComponent',
    templateUrl: 'licenseform.component.html'
})

export class LicenseFormComponent implements OnInit {

    licenseList = [];
    licenseData = {};
    accountcodeList = [];
    ajaxCount: number;
    id: number;
    constructor(
        private LicenseService: LicenseService,
        private route: ActivatedRoute,
        private spinner: NgxSpinnerService,
        private AlertService: AlertService,
        private router: Router,

    ) { }
    ngDoCheck() {
        const change = this.ajaxCount;
        if (this.ajaxCount == 0) {
            this.spinner.hide();
        }
    }
    ngOnInit() {
        this.licenseData['f091fstatus'] = 1;
        this.ajaxCount = 0;
        this.spinner.show();
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        if (this.id > 0) {
            this.ajaxCount++;
            this.LicenseService.getItemsDetail(this.id).subscribe(
                data => {
                    this.ajaxCount--;
                    console.log(data);
                    this.licenseData = data['result'][0];
                    this.licenseData['f091fstatus'] = parseInt(this.licenseData['f091fstatus']);

                    console.log(this.licenseData);
                }, error => {
                    console.log(error);
                });
        }

    }
    addLicense() {
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.LicenseService.updateLicenseItems(this.licenseData, this.id).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert("Grade Name Already Exist");
                    }
                    else {
                        this.router.navigate(['procurement/license']);
                        alert("Grade has been Updated Successfully");
                    }
                }, error => {
                    alert("Grade Name Already Exist");
                    console.log(error);
                });

        } else {

            this.LicenseService.insertLicenseItems(this.licenseData).subscribe(
                data => {

                    if (data['status'] == 409) {
                        alert("Grade Name Already Exist");
                    }
                    else {
                        this.router.navigate(['procurement/license']);
                        alert("Grade has been Added Successfully");

                    }


                }, error => {
                    console.log(error);
                });

        }
    }

}