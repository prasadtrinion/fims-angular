import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LicenseService } from '../service/license.service';
import { NgxSpinnerService } from 'ngx-spinner';


@Component({
    selector: 'LicenseComponent',
    templateUrl: 'license.component.html'
})
export class LicenseComponent implements OnInit {
    licenseList = [];
    licenseData = {};
    id: number;
    constructor(
        private LicenseService: LicenseService,
        private spinner: NgxSpinnerService
    ) { }

    ngOnInit() {
        this.spinner.show();
        this.LicenseService.getItems().subscribe(
            data => {
                this.spinner.hide();
                this.licenseList = data['result']['data'];
            }, error => {
                console.log(error);
            });
    }
    
}