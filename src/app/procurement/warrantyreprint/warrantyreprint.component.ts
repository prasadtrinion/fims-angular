import {Injector} from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { WarrantyPrintService } from '../service/warantyprint.service'

@Component({
    selector: 'WarrantyRePrint',
    templateUrl: 'warrantyreprint.component.html'
})

export class WarrantyRePrintComponent implements OnInit {
   
    poapprovalList =  [];
    poapprovalData = {};
    approvalData ={};
    selectAllCheckbox = false;

    constructor(
        private PoReportapprovalService: WarrantyPrintService,
    ) { }

    ngOnInit() {    
       this.getListData();
    }
    getListData(){
        this.PoReportapprovalService.getApprovedItems().subscribe(
            data => {
                this.poapprovalList = data['result'];
        }, error => {
            console.log(error);
        });
    }
    }
