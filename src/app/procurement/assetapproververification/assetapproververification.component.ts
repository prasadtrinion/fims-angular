import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { GrnapprovalService } from '../service/grnapproval.service'
import { AssetApproverVerificationService } from "../service/assetapproververification.service";
import { AlertService } from './../../_services/alert.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'AssetApproverVerification',
    templateUrl: 'assetapproververification.component.html'
})

export class AssetApproverVerificationComponent implements OnInit {

    grnapprovalList = [];
    approvalData = {};
    grnapprovalData = {};
    selectAllCheckbox = true;
    displaybutton : boolean;
    constructor(

        private GrnapprovalService: GrnapprovalService,
        private AssetApproverVerificationService: AssetApproverVerificationService,
        private AlertService: AlertService,
        private spinner: NgxSpinnerService

    ) { }

    ngOnInit() {
        this.getListData();
        this.displaybutton = false;
    }
    getListData() {
        this.spinner.show();
        this.GrnapprovalService.getlistItems().subscribe(
            data => {
                this.spinner.hide();
                this.grnapprovalList = data['result']['data'];
            }, error => {
                console.log(error);
            });
    }

    addtoAssetData() {
        this.displaybutton = true;
        var approvaloneIds = [];
        for (var i = 0; i < this.grnapprovalList.length; i++) {
            if (this.grnapprovalList[i].status == true) {
                approvaloneIds.push(this.grnapprovalList[i].f034fid);
            }
        }
        if (approvaloneIds.length < 1) {
            alert("Please select atleast one cost center to Add to Asset");
            return false;
        }
        var confirmPop = confirm("Do you want to Add to Asset?");
        if (confirmPop == false) {
            return false;
        }
        var approvaloneUpdate = {};
        approvaloneUpdate['id'] = approvaloneIds;
        this.AssetApproverVerificationService.addAssetItems(approvaloneUpdate).subscribe(
            data => {
                this.getListData();
                alert("Added to Asset Successfully");
            }, error => {
                console.log(error);
            });
    }
    expenseData() {
        this.displaybutton = true;
        var approvaloneIds = [];
        for (var i = 0; i < this.grnapprovalList.length; i++) {
            if (this.grnapprovalList[i].status == true) {
                approvaloneIds.push(this.grnapprovalList[i].f034fid);
            }
        }
        if (approvaloneIds.length < 1) {
            alert("Please select atleast one cost center to Expense");
            return false;
        }
        var confirmPop = confirm("Do you want to Expense?");
        if (confirmPop == false) {
            return false;
        }
        var budgetActivityDataObject = {}
        budgetActivityDataObject['id'] = approvaloneIds;
        this.AssetApproverVerificationService.getExpensetems(budgetActivityDataObject).subscribe(
            data => {
                this.getListData()
                this.approvalData['reason'] = ""
                alert("Expense Successfully");
            }, error => {
                console.log(error);
            });


    }
    extendAssetLifeData() {
        var approvaloneIds = [];
        for (var i = 0; i < this.grnapprovalList.length; i++) {
            if (this.grnapprovalList[i].status == true) {
                approvaloneIds.push(this.grnapprovalList[i].f034fid);
            }
        }
        if (approvaloneIds.length < 1) {
            alert("Please select atleast one cost center to Extend Asset Life");
            return false;
        }
        var confirmPop = confirm("Do you want to Extend Asset Life?");
        if (confirmPop == false) {
            return false;
        }
        var budgetActivityDataObject = {}
        budgetActivityDataObject['id'] = approvaloneIds;
        this.AssetApproverVerificationService.getExtendItems(budgetActivityDataObject).subscribe(
            data => {
                this.getListData()
                this.approvalData['reason'] = ""
                alert("Extended Asset Life Successfully");
            }, error => {
                console.log(error);
            });


    }
    incrementAmount() {
        var approvaloneIds = [];
        for (var i = 0; i < this.grnapprovalList.length; i++) {
            if (this.grnapprovalList[i].status == true) {
                approvaloneIds.push(this.grnapprovalList[i].f034fid);
            }
        }
        if (approvaloneIds.length < 1) {
            alert("Please select atleast one cost center to Increase Amount");
            return false;
        }
        var confirmPop = confirm("Do you want to Increase Amount?");
        if (confirmPop == false) {
            return false;
        }
        var budgetActivityDataObject = {}
        budgetActivityDataObject['id'] = approvaloneIds;
        this.AssetApproverVerificationService.getIncreaseItems(budgetActivityDataObject).subscribe(
            data => {
                this.getListData()
                this.approvalData['reason'] = ""
                alert("Increased Amount Successfully");
            }, error => {
                console.log(error);
            });


    }
}
