import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { PurchaserequistionentryService } from '../service/purchaserequistionentry.service'
import { ActivatedRoute, Router } from '@angular/router';
import { faSearch, faEdit } from '@fortawesome/free-solid-svg-icons';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'PurchaserequistionentryComponent',
    templateUrl: 'purchaserequistionentry.component.html'
})

export class PurchaserequistionentryComponent implements OnInit {
    purchaseRequisitionList =  [];

    faSearch = faSearch;
    faEdit = faEdit;
    id: number;
    title: string;
    type: string;
    constructor(        
        private PurchaserequistionentryService: PurchaserequistionentryService,
        private spinner: NgxSpinnerService,
        private route: ActivatedRoute
    ) { }

    ngOnInit() { 
        this.spinner.show();
        this.PurchaserequistionentryService.getItems().subscribe(
            data => {
                this.spinner.hide();
                let activityData = [];
                activityData = data['result']['data'];
                console.log("asdf");
                for (var i = 0; i < activityData.length; i++) {
                    if (activityData[i]['f088fapprovalStatus'] == 0) {
                        activityData[i]['f088fapprovalStatus'] = 'Pending';
                    } else if (activityData[i]['f088fapprovalStatus'] == 1) {
                        activityData[i]['f088fapprovalStatus'] = 'Approved';
                    } else {
                        activityData[i]['f088fapprovalStatus'] = 'Rejected';
                    }
                }
                this.purchaseRequisitionList = activityData;
        }, error => {
            console.log(error);
        });
    }
}