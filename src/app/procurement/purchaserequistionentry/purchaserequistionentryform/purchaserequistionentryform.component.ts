// import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PurchaserequistionentryService } from '../../service/purchaserequistionentry.service';
import { TaxsetupcodeService } from '../../../generalledger/service/taxsetupcode.service'
import { ItemsetupService } from '../../../procurement/service/itemsetup.service';
import { FinancialyearService } from '../../../generalledger/service/financialyear.service';
import { SupplierregistrationService } from '../../service/supplierregistration.service';
import { OrdertypeService } from '../../service/ordertype.service';
import { FundService } from '../../../generalsetup/service/fund.service';
import { AccountcodeService } from '../../../generalsetup/service/accountcode.service';
import { ActivitycodeService } from '../../../generalsetup/service/activitycode.service';
import { DepartmentService } from '../../../generalsetup/service/department.service';
import { PurchaselimitService } from "../../service/purchaselimit.service";
import { PrapprovalService } from '../../service/prapproval.service';
import { BudgetactivitiesService } from '../../../budget/service/budgetactivities.service';
import { ItemcategoryService } from '../../service/itemcategory.service';
import { ItemsubcategoryService } from '../../service/itemsubcategory.service';
import { AssetinformationService } from "../../../assets/service/assetinformation.service";
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'PurchaserequistionentryformComponent',
    templateUrl: 'purchaserequistionentryform.component.html'
})

export class PurchaserequistionentryformComponent implements OnInit {
    purchaserequistionentryList = [];
    purchaserequistionentryData = {};
    purchaserequistionentryDataheader = {};
    assetCodeOriginal= [];
    itemList = [];
    taxcodeList = [];
    orderType = [];
    sponserList = [];
    departmentList = [];
    DeptList = [];
    accountcodeList = [];
    fundList = [];
    activitycodeList = [];
    taxcodepercenList = [];
    financialList = [];
    supplierList = [];
    orderTypeList = [];
    deleteList = [];
    costCenterFund = [];
    fordertype = [];
    costCenterActivity = [];
    costCenterAccount = [];
    itemUnitList = [];
    budgetcostcenterList = [];
    purchaseLimitList = [];
    budgetcostcenterLists = [];
    budgetcostcenterList1 = [];
    prapprovalList = [];
    costCenterList = [];
    prapprovalData = {};
    approvalData = {};
    itemcategoryList = [];
    itemsubcategoryList = [];
    assetinformationList = [];
    assetDummyList = [];
    assetCodeList = [];
    socodeList = [];
    assetCode: boolean;
    id: number;
    valueDate: string;
    ajaxCount: number;
    deptId: number;
    gstValue: number;
    Amount: number;
    taxAmount: number;
    totalAmount: number;
    type: string;
    showLastRow: boolean;
    showLastRowMinus: boolean;
    showLastRowPlus: boolean;
    listshowLastRowMinus: boolean;
    listshowLastRowPlus: boolean;
    saveBtnDisable: boolean;
    viewDisabled: boolean;
    amountValidationFlag: boolean;
    idview: number;
    finaltotal: number;
    assetCodeNotApplicable = [];


    constructor(

        private PurchaserequistionentryService: PurchaserequistionentryService,
        private TaxsetupcodeService: TaxsetupcodeService,
        private ItemService: ItemsetupService,
        private OrdertypeService: OrdertypeService,
        private DepartmentService: DepartmentService,
        private ActivitycodeService: ActivitycodeService,
        private AccountcodeService: AccountcodeService,
        private FundService: FundService,
        private FinancialyearService: FinancialyearService,
        private SupplierregistrationService: SupplierregistrationService,
        private PurchaselimitService: PurchaselimitService,
        private BudgetactivitiesService: BudgetactivitiesService,
        private spinner: NgxSpinnerService,
        private PrapprovalService: PrapprovalService,
        private ItemsubcategoryService: ItemsubcategoryService,
        private ItemcategoryService: ItemcategoryService,
        private AssetinformationService: AssetinformationService,
        private route: ActivatedRoute,
        private router: Router

    ) { }

    showIcons() {
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        if (this.purchaserequistionentryList.length > 0) {
            this.showLastRowPlus = true;
            this.showLastRowMinus = true;
            this.listshowLastRowMinus = true;
        }
        if (this.purchaserequistionentryList.length > 1) {
            if (this.showLastRow == false) {
                this.listshowLastRowPlus = false;
                this.listshowLastRowMinus = true;
                this.showLastRowPlus = true;

            } else {
                this.listshowLastRowPlus = true;
                this.listshowLastRowMinus = false;

            }
        }
        if (this.purchaserequistionentryList.length == 1 && this.showLastRow == false) {
            this.listshowLastRowMinus = true;
            this.listshowLastRowPlus = false;

        }
        if (this.purchaserequistionentryList.length == 1 && this.showLastRow == true) {
            this.listshowLastRowMinus = false;
            this.listshowLastRowPlus = true;

        }
        if (this.purchaserequistionentryList.length == 0 && this.showLastRow == false) {
            this.showLastRowMinus = false;
            this.showLastRowPlus = true;
        }
        if (this.purchaserequistionentryList.length == 0 && this.showLastRow == true) {
            this.showLastRowMinus = true;
            this.showLastRowPlus = false;
        }
        if (this.viewDisabled == true || this.purchaserequistionentryDataheader['f088fapprovalStatus'] == 1 || this.purchaserequistionentryDataheader['f088fapprovalStatus'] == 2) {
            this.listshowLastRowPlus = false;
        }
    }

    deletemodule() {
        this.showLastRow = true;
        this.showIcons();
    }

    showNewRow() {
        var confirmPop = confirm("Do you want to Add?");
        if (confirmPop == false) {
            return false;
        }
        this.showLastRow = false;
        this.showIcons();
    }
    ngDoCheck() {
        this.showIcons();
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        const change = this.ajaxCount;

        if (this.ajaxCount == 0) {
            this.editFunction();
            this.spinner.hide();
            this.ajaxCount = 10;
        }
        this.route.paramMap.subscribe((data) => this.idview = + data.get('idview'));
        if (this.ajaxCount == 0 && this.purchaserequistionentryList.length > 0) {
            this.ajaxCount = 20;

            if (this.purchaserequistionentryList[0]['f088fapprovalStatus'] == 1 || this.purchaserequistionentryList[0]['f088fapprovalStatus'] == 2) {
                this.saveBtnDisable = true;
                this.viewDisabled = true;
                this.listshowLastRowPlus = false;

                $("#target input,select").prop("disabled", true);
                $("#target1 input,select").prop("disabled", true);
            }
        }
        if (this.ajaxCount == 0 && this.purchaserequistionentryList.length > 0) {
            this.ajaxCount = 20;
            if (this.idview > 1) {
                this.saveBtnDisable = true;
                this.viewDisabled = true;
                this.listshowLastRowPlus = false;

                $("#target input,select").prop("disabled", true);
                $("#target1 input,select").prop("disabled", true);
            }
        }
    }

    editFunction() {
        // this.spinner.show();
        this.route.paramMap.subscribe((data) => this.idview = + data.get('idview'));
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.ajaxCount++;
            this.PurchaserequistionentryService.getItemsDetail(this.id).subscribe(
                data => {
                    this.ajaxCount--;
                    this.purchaserequistionentryList = data['result'];
                    this.purchaserequistionentryDataheader['f088forderType'] = data['result'][0]['f088forderType'];
                    this.purchaserequistionentryDataheader['f088fdate'] = data['result'][0]['f088fdate'];
                    this.purchaserequistionentryDataheader['f088fidSupplier'] = data['result'][0]['f088fidSupplier'];
                    this.purchaserequistionentryDataheader['f088fdescription'] = data['result'][0]['f088fdescription'];
                    this.purchaserequistionentryDataheader['f088freferenceNumber'] = data['result'][0]['f088freferenceNumber'];
                    this.purchaserequistionentryDataheader['f088fidFinancialyear'] = data['result'][0]['f088fidFinancialyear'];
                    this.purchaserequistionentryDataheader['f088fidDepartment'] = data['result'][0]['f088fidDepartment'];
                    this.purchaserequistionentryDataheader['f088ftotalAmount'] = data['result'][0]['f088ftotalAmount'];

                    // this.getListOfPendingCostCenter();

                    if (data['result'][0]['f088fapprovalStatus'] == 1 || this.purchaserequistionentryList[0]['f088fapprovalStatus'] == 2) {
                        this.viewDisabled = true;

                        $("#target input,select").prop("disabled", true);
                        $("#target1 input,select").prop("disabled", true);
                    }
                    if (this.idview == 2) {
                        this.viewDisabled = true;

                        $("#target input,select").prop("disabled", true);
                        $("#target1 input,select").prop("disabled", true);
                    }
                    // this.purchaserequistionentryDataheader['f088fid'] = data['result'].f088fid;
                    console.log(this.purchaserequistionentryList);
                    this.onBlurMethod();
                    this.showIcons();
                    this.AmountComparision();
                    this.getDepartment();
                    this.getSubCategories();
                }, error => {
                    console.log(error);
                });
        }
    }

    getAssetCode() {
        if (this.purchaserequistionentryData['f088fitemCategory'] == 'PERKHIDMATAN : PENYELENGGARAAN DAN PEMBAIKAN KECIL') {
            this.assetCode = true;
        }
    }

    getUOM() {
        for(var i=0;i<this.itemList.length;i++) {
            if(this.itemList[i]['itemId']==this.purchaserequistionentryData['idItem']){
                this.purchaserequistionentryData['itemName'] = this.itemList[i]['itemName'];
            }
        }

        let itemId = this.purchaserequistionentryData['itemId'];
        console.log(itemId);
        this.ItemService.getItemsDetail(itemId).subscribe(
            data => {
                this.itemUnitList = data['result'];
                console.log(data);

                // this.purchaserequistionentryData['f088funit'] = data['result'][0]['f029funit'];
            }
        );
    }
    getPercentage() {
        let percenId = this.purchaserequistionentryData['f088ftaxCode'];
        console.log(percenId);
        this.TaxsetupcodeService.getItemsDetail(percenId).subscribe(
            data => {
                this.taxcodepercenList = data['result'];

                this.purchaserequistionentryData['f088fpercentage'] = data['result'][0]['f081fpercentage'];
            }
        );
    }

    ngOnInit() {
        this.ajaxCount = 0;
        this.spinner.show();
        this.assetCode = false;
        let ordertypeobj = {};
        ordertypeobj['name'] = 'PO';
        this.fordertype.push(ordertypeobj);
        ordertypeobj = {};
        ordertypeobj['name'] = 'Direct Purchase';
        this.fordertype.push(ordertypeobj);
        ordertypeobj = {};
        ordertypeobj['name'] = 'Warrant';
        this.fordertype.push(ordertypeobj);
        ordertypeobj = {};
        ordertypeobj['name'] = 'Tender';
        this.fordertype.push(ordertypeobj);
        ordertypeobj = {};
        ordertypeobj['name'] = 'Quotation';
        this.fordertype.push(ordertypeobj);

        let assetCodeObject = {};
        assetCodeObject['f085fassetCode'] = 'NA';
        this.assetCodeNotApplicable.push(assetCodeObject);

        this.viewDisabled = false;
        this.onBlurMethod();
        this.getTotal();
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        if (this.id < 1) {
            this.showLastRow = false;
            this.showLastRowMinus = false;
            this.showLastRowPlus = true;
        } else {
            this.showLastRow = true;

        }
        this.purchaserequistionentryDataheader['f088fdate'] = new Date().toISOString().substr(0, 10);
        this.purchaserequistionentryData['f088frequiredDate'] = new Date().toISOString().substr(0, 10);

        // //item dropdown
        // this.ajaxCount++;
        // this.ItemService.getAllItems().subscribe(
        //     data => {
        //         this.ajaxCount--;
        //         this.itemList = data['result']['data'];
        //     }
        // );
        //socode dropdown
        this.ajaxCount++;
        this.PurchaserequistionentryService.getSocodeItems().subscribe(
            data => {
                this.ajaxCount--;
                this.socodeList = data['result'];
            }, error => {
                console.log(error);
            }
        )

        //item category
        this.ajaxCount++;
        this.ItemcategoryService.getbothProcurementAndAssetCategory().subscribe(
            data => {
                console.log(data['result']['data']);
                this.ajaxCount--;
                this.itemcategoryList = data['result']['data'];
                console.log(this.itemcategoryList);
            }, error => {
                console.log(error);
            });

        //Asset Code
        this.ajaxCount++;
        this.AssetinformationService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                // this.assetCodeList = data['data']['data'];
                this.assetCodeOriginal = data['data']['data'];
            }, error => {
                console.log(error);
            });

        //Supplierregistration dropdown
        this.ajaxCount++;
        this.SupplierregistrationService.getActiveItems().subscribe(
            data => {
                this.ajaxCount--;
                this.supplierList = data['result']['data'];
                console.log(this.supplierList);
            }
        );

        //order type dropdown
        this.ajaxCount++;
        this.OrdertypeService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.orderTypeList = data['result']['data'];

            }
        );

        // Financial year dropdown
        this.ajaxCount++;
        this.FinancialyearService.getActiveItems().subscribe(
            data => {
                this.ajaxCount--;

                this.financialList = data['result'];
                if (data['result'].length == 1) {
                    this.purchaserequistionentryDataheader['f088fidFinancialyear'] = data['result'][0]['f015fid'];
                    this.getAllpurchaseGlList();
                }
            }
        );
        this.ajaxCount++;
        this.PurchaselimitService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.purchaseLimitList = data['result']['data'];
                console.log(this.purchaseLimitList);

            });

        //Department dropdown
        this.ajaxCount++;
        this.DepartmentService.getZerodepartment().subscribe(
            data => {
                this.ajaxCount--;
                this.departmentList = data['result']['data'];
            }
        );
        //Department dropdown
        this.ajaxCount++;
        this.DepartmentService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.DeptList = data['result']['data'];
            }
        );
        //Activity dropdown
        this.ajaxCount++;
        this.ActivitycodeService.getItemsByLevel().subscribe(
            data => {
                this.ajaxCount--;
                this.activitycodeList = data['result']['data'];
            }
        );
        //Account dro(pdown
        this.ajaxCount++;
        this.AccountcodeService.getItemsByLevel().subscribe(
            data => {
                this.ajaxCount--;
                this.accountcodeList = data['result']['data'];
            }
        );
        //Fund dropdown
        this.ajaxCount++;
        this.FundService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.fundList = data['result']['data'];
            }
        );

        //Tax Code
        this.ajaxCount++;
        this.TaxsetupcodeService.getActiveItems().subscribe(
            data => {
                this.ajaxCount--;
                this.taxcodeList = data['result']['data'];
            });
    }
    setFundCode()
    {
        this.purchaserequistionentryData['f088fbudgetFundCode'] = this.purchaserequistionentryData['f088ffundCode'];
    }
    setActivityCode(){
        this.purchaserequistionentryData['f088fbudgetActivityCode'] = this.purchaserequistionentryData['f088factivityCode'];
    }
    setDepartmentCode(){
        this.purchaserequistionentryData['f088fbudgetDepartmentCode'] = this.purchaserequistionentryData['f088fdepartmentCode'];
    }
    setAccountCode(){
        this.purchaserequistionentryData['f088fbudgetAccountCode'] = this.purchaserequistionentryData['f088faccountCode'];
    }
    getSubCategories() {

        let purchaseCategory = this.purchaserequistionentryData['category_code'];
        if(purchaseCategory=='') {
            return false;
        }
        let categoryType = purchaseCategory.slice(-1);
        console.log(categoryType);
        var categoryId = purchaseCategory;
        this.purchaserequistionentryData['f088fitemCategory'] = categoryId = categoryId.substring(0, categoryId.length - 1); 
        console.log(categoryId);

        for(var i=0;i<this.itemcategoryList.length;i++) {
            if(this.itemcategoryList[i]['category_code']==purchaseCategory){
                console.log(this.itemcategoryList[i]);
                this.purchaserequistionentryData['categoryName'] = this.itemcategoryList[i]['category_name'];
                this.purchaserequistionentryData['maintenanceCode'] = this.itemcategoryList[i]['maintenanceCode'];
            }
        }
        this.assetCodeList = [];
        if(this.purchaserequistionentryData['maintenanceCode']==1) {
            this.assetCodeList = this.assetCodeOriginal;
        } else {
            this.assetCodeList = this.assetCodeNotApplicable;
        }
        if(categoryType=='A') {
            this.assetinformationList = this.assetCodeNotApplicable;
            console.log(this.assetinformationList);

            this.ajaxCount++;
            this.ItemsubcategoryService.assetgetSubCategoriesByCategory(categoryId).subscribe(
                data => {
                    this.ajaxCount--;
                    this.itemsubcategoryList = data['result'];
                }, error => {
                    console.log(error);
                });
        }
        if(categoryType=='P') {
            if(this.purchaserequistionentryData['maintenanceCode']!=null){
                this.assetinformationList = this.assetCodeList;
            } else {
                this.assetinformationList = this.assetCodeNotApplicable;
            }

            this.ajaxCount++;
            this.ItemsubcategoryService.getProcurmentSubCategoriesByProcuremntCategory(categoryId).subscribe(
                data => {
                    this.ajaxCount--;
                    this.itemsubcategoryList = data['result'];
                }, error => {
                    console.log(error);
                });
        }
    }
    getItems(){
        console.log(this.purchaserequistionentryData);
        let purchaseCategory = this.purchaserequistionentryData['category_code'];
        let categoryType = purchaseCategory.slice(-1);
        console.log(this.itemsubcategoryList);

        for(var i=0;i<this.itemsubcategoryList.length;i++) {
            if(this.itemsubcategoryList[i]['subCategoryId']==this.purchaserequistionentryData['subCategoryId']){
                console.log(this.itemcategoryList[i]);
                this.purchaserequistionentryData['subCategoryName'] = this.itemsubcategoryList[i]['subCategoryName'];
            }
        }

       
        if(this.purchaserequistionentryData['f088fitemCategory']==1) {
            this.ajaxCount++;
            this.purchaserequistionentryData['f088fcategoryType'] = 'A';
            this.ItemsubcategoryService.assetItemBySubCategoryId(this.purchaserequistionentryData['subCategoryId']).subscribe(
                data => {
                    this.ajaxCount--;
                    this.itemList = data['result'];
                }, error => {
                    console.log(error);
                });
        }else {
            this.ajaxCount++;
            this.purchaserequistionentryData['f088fcategoryType'] = 'P';
            this.ItemsubcategoryService.procurementItemBySubCategoryId(this.purchaserequistionentryData['subCategoryId']).subscribe(
                data => {
                    this.ajaxCount--;
                    this.itemList = data['result']['data'];
                }, error => {
                    console.log(error);
                });
        }

    }
   
    getPurchaseRequisitionNumber() {
        let typeObject = {};
        typeObject['type'] = "PR";
        console.log(typeObject);
        this.PurchaserequistionentryService.getPurchaseRequisitionNumber(typeObject).subscribe(
            data => {
                this.purchaserequistionentryDataheader['f088freferenceNumber'] = data['number'];
                this.valueDate = new Date().toISOString().substr(0, 10);
                this.purchaserequistionentryDataheader['f088fdate'] = this.valueDate;
            }
        );
        return this.type;
    }
    getDepartment() {

        var departmentId = this.purchaserequistionentryDataheader['f088fidDepartment'];
        var departmentCode = departmentId.substring(0, 3);;

        this.BudgetactivitiesService.getCostCenterBasedOnDepartmentCode(departmentCode).subscribe(
            data => {
                this.costCenterList = data['result']['data'];
            }, error => {
                console.log(error);
            });
    }
    ConvertToFloat(val) {
        return parseFloat(val);
    }

    dateComparison() {

        let expectedDate = Date.parse(this.purchaserequistionentryData['f088frequiredDate']);
        let prDate = Date.parse(this.purchaserequistionentryDataheader['f088fdate']);
        if (expectedDate < prDate) {
            alert("Expected Delivery Date cannot be Less than Purchase Requisition Date");
            this.purchaserequistionentryData['f088frequiredDate'] = "";
        }

    }
    savePurchaseRequisition() {
        if (this.purchaserequistionentryDataheader['f088forderType'] == undefined) {
            alert('Select Order Type');
            return false;
        }
        if (this.purchaserequistionentryDataheader['f088fidDepartment'] == undefined) {
            alert('Select Department');
            return false;
        }
        if (this.purchaserequistionentryDataheader['f088fidSupplier'] == undefined) {
            alert('Select Vendor');
            return false;
        }
       
        this.onBlurMethod();
        this.getTotal();
        if (this.showLastRow == false) {
            var addactivities = this.addEntrylist();
            if (addactivities == false) {
                return false;
            }
        }

        let purchaseRequisitionObject = {};
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            purchaseRequisitionObject['f088fid'] = this.id;
        }
        purchaseRequisitionObject['f088forderType'] = this.purchaserequistionentryDataheader['f088forderType'];
        purchaseRequisitionObject['f088fdate'] = this.purchaserequistionentryDataheader['f088fdate'];
        purchaseRequisitionObject['f088fidSupplier'] = this.purchaserequistionentryDataheader['f088fidSupplier'];
        purchaseRequisitionObject['f088fidFinancialyear'] = this.purchaserequistionentryDataheader['f088fidFinancialyear'];
        purchaseRequisitionObject['f088fidDepartment'] = this.purchaserequistionentryDataheader['f088fidDepartment'];
        purchaseRequisitionObject['f088fdescription'] = this.purchaserequistionentryDataheader['f088fdescription'];
        purchaseRequisitionObject['f088ftotalAmount'] = this.ConvertToFloat(this.purchaserequistionentryDataheader['f088ftotalAmount']).toFixed(2);;
        purchaseRequisitionObject['f088fstatus'] = 0;
        purchaseRequisitionObject['purchase-details'] = this.purchaserequistionentryList;
        var sum =0;
        for (var i = 0; i < purchaseRequisitionObject['purchase-details'].length; i++) {
            purchaseRequisitionObject['purchase-details'][i]['f088total'] = this.ConvertToFloat(purchaseRequisitionObject['purchase-details'][i]['f088total']).toFixed(2);
            purchaseRequisitionObject['purchase-details'][i]['f088fprice'] = this.ConvertToFloat(purchaseRequisitionObject['purchase-details'][i]['f088fprice']).toFixed(2);
            purchaseRequisitionObject['purchase-details'][i]['f088ftaxAmount'] = this.ConvertToFloat(purchaseRequisitionObject['purchase-details'][i]['f088ftaxAmount']).toFixed(2);
            purchaseRequisitionObject['purchase-details'][i]['f088ftotalIncTax'] = this.ConvertToFloat(purchaseRequisitionObject['purchase-details'][i]['f088ftotalIncTax']).toFixed(2);
            sum = sum + purchaseRequisitionObject['purchase-details'][i]['f088ftotalIncTax']

        }
        
        for (var i = 0; i < purchaseRequisitionObject['purchase-details'].length; i++) {
            let balanceObj = {};
            balanceObj['FinancialYear'] = this.purchaserequistionentryDataheader['f088fidFinancialyear'];
            balanceObj['Department'] = purchaseRequisitionObject['purchase-details'][i]['f088fbudgetDepartmentCode'];
            balanceObj['Fund'] = purchaseRequisitionObject['purchase-details'][i]['f088fbudgetFundCode'];
            balanceObj['Activity'] = purchaseRequisitionObject['purchase-details'][i]['f088fbudgetActivityCode'];
            balanceObj['Account'] = purchaseRequisitionObject['purchase-details'][i]['f088fbudgetAccountCode'];
            balanceObj['Amount'] = purchaseRequisitionObject['purchase-details'][i]['f088ftotalIncTax'];
            this.PrapprovalService.getBalanceStatus(balanceObj).subscribe(
                data => {
                    if (data['status'] == 411) {
                        alert('Budget Amount Exceeded..!!');
                        return false;
                    }
                    console.log('Response');
                }, error => {
                    console.log(error);
                });
        }
        this.purchaserequistionentryDataheader['f088ftotalAmount'] = sum;
        let amountValidationFlag = this.AmountComparision();
        console.log(amountValidationFlag);
        if (amountValidationFlag == false) {
            alert("Purchase Order Amount is not greater than Maximum Amount of Purchase Method");
            return false;
        }
       
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }

        if (this.id > 0) {
            this.PurchaserequistionentryService.updateEntryItems(purchaseRequisitionObject, this.id).subscribe(
                data => {
                    this.router.navigate(['procurement/purchaserequisitionentry']);
                    alert("Purchase Requistion Entry has been Updated Sucessfully !!");
                }, error => {
                    console.log(error);
                });
        } else {
            this.PurchaserequistionentryService.insertEntryItems(purchaseRequisitionObject).subscribe(
                data => {
                    this.router.navigate(['procurement/purchaserequisitionentry']);
                    alert("Purchase Requistion Entry has been Added Sucessfully!!");
                }, error => {
                    console.log(error);
                });
        }
    }
    getAllpurchaseGlList() {
        var idfinancialYear = this.purchaserequistionentryDataheader['f088fidFinancialyear'];

        this.PurchaserequistionentryService.urlgetAllpurcahseGls(idfinancialYear).subscribe(
            data => {
                var depArray = data['result']['Department'];
                this.budgetcostcenterList = depArray;

                this.costCenterFund = data['result']['Fund'];

                this.costCenterAccount = data['result']['Account'];

                this.costCenterActivity = data['result']['Activity'];

                console.log(data);
            }, error => {
                console.log(error);
            }
        );
    }


    getGstvalue() {
        let taxId = this.purchaserequistionentryData['f088ftaxCode'];
        let quantity = this.purchaserequistionentryData['f088fquantity'];
        let price = this.purchaserequistionentryData['f088fprice'];
        var taxSelectedObject = {};
        for (var k = 0; k < this.taxcodeList.length; k++) {
            if (this.taxcodeList[k]['f081fid'] == taxId) {
                taxSelectedObject = this.taxcodeList[k];
            }
        }
        this.gstValue = taxSelectedObject['f081fpercentage'];
        this.purchaserequistionentryData['f088fpercentage'] = this.gstValue;
        this.Amount = (this.ConvertToFloat(price)) * (this.ConvertToFloat(quantity));
        this.taxAmount = (this.ConvertToFloat(this.gstValue)) / 100 * this.ConvertToFloat((this.Amount));

        let totalAm = ((this.taxAmount) + (this.Amount));
        this.purchaserequistionentryData['f088ftaxAmount'] = this.taxAmount.toFixed(2);
        this.purchaserequistionentryData['f088ftotalIncTax'] = totalAm.toFixed(2);
    }
    AmountComparision() {
        let amountFlag = true;
        console.log(this.purchaseLimitList);
        console.log(this.purchaserequistionentryDataheader);
        for (var i = 0; i < this.purchaseLimitList.length; i++) {
            if (this.purchaseLimitList[i]['f109ftype'] == this.purchaserequistionentryDataheader['f088forderType']) {
                if (this.ConvertToFloat(this.purchaseLimitList[i]['f109fmaxLimit']) < this.ConvertToFloat(this.purchaserequistionentryDataheader['f088ftotalAmount'])) {
                    amountFlag = false;
                    break;
                }
            }
        }
        return amountFlag;
    }

    onBlurMethod() {
        // console.log(this.purchaserequistionentryList);
        var finaltotal = 0;
        for (var i = 0; i < this.purchaserequistionentryList.length; i++) {
            var totalamount = (this.ConvertToFloat(this.purchaserequistionentryList[i]['f088fquantity'])) * (this.ConvertToFloat(this.purchaserequistionentryList[i]['f088fprice']));
            console.log(totalamount);
            var gstamount = (this.ConvertToFloat(this.purchaserequistionentryList[i]['f088fpercentage']) / 100 * (totalamount));
            console.log(gstamount);
            this.purchaserequistionentryList[i]['f088total'] = totalamount;
            this.purchaserequistionentryList[i]['f088ftotalIncTax'] = gstamount + totalamount;
            console.log(finaltotal);
            finaltotal = finaltotal + this.purchaserequistionentryList[i]['f088ftotalIncTax'];
            console.log(finaltotal);
        }
        this.purchaserequistionentryDataheader['f088ftotalAmount'] = this.ConvertToFloat(finaltotal.toFixed(2));
    }
    amountprefill() {

        var finaltotal = 0;
        if (this.purchaserequistionentryData['f088fprice'] != undefined) {
            var totalamount = (this.ConvertToFloat(this.purchaserequistionentryData['f088fquantity'])) * (this.ConvertToFloat(this.purchaserequistionentryData['f088fprice']));
            console.log(totalamount);
            var gstamount = (this.ConvertToFloat(this.purchaserequistionentryData['f088fpercentage']) / 100 * (totalamount));
            console.log(gstamount);
            this.purchaserequistionentryData['f088total'] = totalamount;
            if (this.purchaserequistionentryData['f088ftaxCode'] != undefined) {
                this.getGstvalue();
            }
        }
    }
    listamountprefill() {

        for (let i = 0; i < this.purchaserequistionentryList.length; i++) {
            if (this.purchaserequistionentryList[i]['f088fprice'] != undefined) {
                var totalamount = (this.ConvertToFloat(this.purchaserequistionentryList[i]['f088fquantity'])) * (this.ConvertToFloat(this.purchaserequistionentryList[i]['f088fprice']));
                console.log(totalamount);
                var gstamount = (this.ConvertToFloat(this.purchaserequistionentryList[i]['f088fpercentage']) / 100 * (totalamount));
                console.log(gstamount);
                this.purchaserequistionentryList[i]['f088total'] = totalamount;
                if (this.purchaserequistionentryList[i]['f088ftaxCode'] != undefined) {
                    let taxId = this.purchaserequistionentryList[i]['f088ftaxCode'];
                    let quantity = this.purchaserequistionentryList[i]['f088fquantity'];
                    let price = this.purchaserequistionentryList[i]['f088fprice'];
                    var taxSelectedObject = {};
                    for (var k = 0; k < this.taxcodeList.length; k++) {
                        if (this.taxcodeList[k]['f081fid'] == taxId) {
                            taxSelectedObject = this.taxcodeList[k];
                        }
                    }
                    this.gstValue = taxSelectedObject['f081fpercentage'];
                    this.purchaserequistionentryList[i]['f088fpercentage'] = this.gstValue;
                    this.Amount = (this.ConvertToFloat(price)) * (this.ConvertToFloat(quantity));
                    this.taxAmount = (this.ConvertToFloat(this.gstValue)) / 100 * this.ConvertToFloat((this.Amount));
                    let totalAm = ((this.taxAmount) + (this.Amount));
                    this.purchaserequistionentryList[i]['f088ftaxAmount'] = this.taxAmount.toFixed(2);
                    this.purchaserequistionentryList[i]['f088ftotalIncTax'] = totalAm.toFixed(2);
                }
            }
        }
    }
    getTotal() {
        this.totalAmount = (this.ConvertToFloat(this.finaltotal));
    }
    deleteEntry(object) {
        var cnf = confirm("Do you really want to delete");
        if (cnf == true) {
            var index = this.purchaserequistionentryList.indexOf(object);
            this.deleteList.push(this.purchaserequistionentryList[index]['f088fidDetails']);
            let deleteObject = {};
            deleteObject['id'] = this.deleteList;
            this.PurchaserequistionentryService.deleteItems(deleteObject).subscribe(
                data => {
                }, error => {
                    console.log(error);
                });
            if (index > -1) {
                this.purchaserequistionentryList.splice(index, 1);
            }
            this.showIcons();
            this.onBlurMethod();
            this.getTotal();
        }
    }

    addEntrylist() {

       console.log(this.purchaserequistionentryData);
        if (this.purchaserequistionentryData['f088ffundCode'] == undefined) {
            alert('Select GL  Fund');
            return false;
        }
        if (this.purchaserequistionentryData['f088factivityCode'] == undefined) {
            alert('Select GL Activity Code');
            return false;
        }
        if (this.purchaserequistionentryData['f088fdepartmentCode'] == undefined) {
            alert('Select GL Cost Center');
            return false;
        }
        if (this.purchaserequistionentryData['f088faccountCode'] == undefined) {
            alert('Select GL Account Code');
            return false;
        }
        if (this.purchaserequistionentryData['f088fbudgetFundCode'] == undefined) {
            alert('Select Budget Fund');
            return false;
        }
        if (this.purchaserequistionentryData['f088fbudgetActivityCode'] == undefined) {
            alert('Select Budget Activity Code');
            return false;
        }
        if (this.purchaserequistionentryData['f088fbudgetDepartmentCode'] == undefined) {
            alert('Select Budget Cost Center');
            return false;
        }
        if (this.purchaserequistionentryData['f088fbudgetAccountCode'] == undefined) {
            alert('Select Budget Account Code');
            return false;
        }

        // if (this.purchaserequistionentryData['f088fsoCode'] == undefined) {
        //     alert('Enter the SO Code');
        //     return false;
        // }
        if (this.purchaserequistionentryData['f088frequiredDate'] == undefined) {
            alert('Select Required Date');
            return false;
        }
        if (this.purchaserequistionentryData['f088fquantity'] == undefined) {
            alert('Enter the Quantity');
            return false;
        }
        if (this.purchaserequistionentryData['f088ftaxCode'] == undefined) {
            alert('Enter the Tax Code');
            return false;
        }


        if ((this.purchaserequistionentryData['f088ffundCode'] == this.purchaserequistionentryDataheader['f088fbudgetFundCode']) &&
            (this.purchaserequistionentryData['f088faccountCode'] == this.purchaserequistionentryDataheader['f088fbudgetAccountCode'])) {
            alert('Cost Center and Account code cannot be same');
            return false;
        }
        // for (var i = 0; i < this.purchaserequistionentryList.length; i++) {

        //     if (this.purchaserequistionentryList[i]['f088fbudgetAccountCode'] == this.purchaserequistionentryData['f088fbudgetAccountCode']) {
        //         alert("This Account Code has already been allocated, Change Account Code");
        //         return false;
        //     }
        //     if (this.purchaserequistionentryList[i]['f088fbudgetAccountCode'] == this.purchaserequistionentryData['f088fbudgetAccountCode']) {
        //         alert("This Cost Center Code has been already allocated Change the Cost Center Code");
        //         return false;
        //     }
        // }
        var confirmPop = confirm("Do you want to Add?");
        if (confirmPop == false) {
            return false;
        }

        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.purchaserequistionentryData['f088fitemSubCategory'] = this.purchaserequistionentryData['subCategoryId'];
        this.purchaserequistionentryData['f088fidItem'] = this.purchaserequistionentryData['idItem'];
        var dataofCurrentRow = this.purchaserequistionentryData;
        this.purchaserequistionentryData = {};
        this.purchaserequistionentryList.push(dataofCurrentRow);
        this.onBlurMethod();
        this.amountprefill();
        this.getTotal();
        this.showIcons();
        this.AmountComparision();

    }
    onlyNumberKey(event) {
        //console.log(event);
        if (event.charCode == 46) {

        } else {
            return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
        }
    }
    getListData() {
        this.PrapprovalService.getItems().subscribe(
            data => {
                this.prapprovalList = data['result'];
                // this.select();
            }, error => {
                console.log(error);
            });
    }
    PrapprovalListData() {
        console.log(this.prapprovalList);
        var approvaloneIds = [];
        for (var i = 0; i < this.prapprovalList.length; i++) {
            if (this.prapprovalList[i].f088fapprovalStatus == true) {
                approvaloneIds.push(this.prapprovalList[i].f088fid);
            }
        }
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        approvaloneIds.push(this.id);
        var confirmPop = confirm("Do you want to Approve?");
        if (confirmPop == false) {
            return false;
        }
        var approvaloneUpdate = {};
        approvaloneUpdate['id'] = approvaloneIds;
        approvaloneUpdate['status'] = 1;
        approvaloneUpdate['reason'] = '';
        console.log(approvaloneIds);
        this.PrapprovalService.updatePrapprovalItems(approvaloneUpdate).subscribe(
            data => {
                if (data['status'] == 411) {
                    alert('Budget Amount Exceeded..!!');
                }
                else {
                alert(" Approved Sucessfully ! ");
                }
                this.getListData();
                this.router.navigate(['procurement/prapproval']);
                this.approvalData['reason'] = '';

            }, error => {
                console.log(error);
            });
    }
    regectapprovalListData() {
        console.log(this.prapprovalList);
        var approvaloneIds = [];
        for (var i = 0; i < this.prapprovalList.length; i++) {
            if (this.prapprovalList[i].f088fapprovalStatus == true) {
                approvaloneIds.push(this.prapprovalList[i].f088fid);
            }
        }
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        approvaloneIds.push(this.id);
        if (this.approvalData['reason'] == undefined) {
            alert('Please Enter the Description');
            return false;
        }

        var confirmPop = confirm("Do you want to Reject?");
        if (confirmPop == false) {
            return false;
        }
        var approvaloneUpdate = {};
        approvaloneUpdate['id'] = approvaloneIds;
        approvaloneUpdate['status'] = 2;
        approvaloneUpdate['reason'] = this.approvalData['reason'];

        console.log(approvaloneIds);
        this.PrapprovalService.updatePrapprovalItems(approvaloneUpdate).subscribe(
            data => {
                this.getListData();
                this.router.navigate(['procurement/prapproval']);
                this.approvalData['reason'] = '';
                alert(" Rejected Sucessfully ! ");

            }, error => {
                console.log(error);
            });
    }
    maximize() {
        $("#myModal").addClass("modal");
        $("#overlay1").show();
        $(".maximize-icon").addClass("hidden");
        $(".minimize-icon").removeClass("hidden");
        $(".modal-backdrop").show();

    }
    minimize() {
        $("#myModal").removeClass("modal");
        $("#overlay1").hide();
        $(".maximize-icon").removeClass("hidden");
        $(".minimize-icon").addClass("hidden");
        $(".modal-backdrop").hide();
        $('#myModal').show();
    }

}

