import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { TenderquatationService } from '../../service/tenderquotation.service'
import { ActivatedRoute, Router } from '@angular/router';
import { TaxsetupcodeService } from '../../../generalledger/service/taxsetupcode.service'
import { ItemcategoryService } from '../../service/itemcategory.service'
import { DepartmentService } from '../../../generalsetup/service/department.service';
import { StaffService } from "../../../loan/service/staff.service";
import { ItemsetupService } from '../../service/itemsetup.service'
import { FundService } from '../../../generalsetup/service/fund.service';
import { AccountcodeService } from '../../../generalsetup/service/accountcode.service';
import { ActivitycodeService } from '../../../generalsetup/service/activitycode.service';
import { PurchaserequistionentryService } from '../../service/purchaserequistionentry.service';
import { FinancialyearService } from '../../../generalledger/service/financialyear.service';
import { PrapprovalService } from '../../service/prapproval.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'TenderQuatationFormComponent',
    templateUrl: 'tenderquotationform.component.html'
})

export class TenderQuatationFormComponent implements OnInit {
    tenderList = [];
    tenderData = {};
    DeptList = [];
    deptData = [];
    categoryList = [];
    committListData = {};
    committList = [];
    specList = [];
    specData = {};
    smartStaffList = [];
    itemsetupList = [];
    generalspecList = [];
    generalspecData = {};
    fcategory = [];
    financialList = [];
    deleteList = [];
    id: number;
    ajaxCount: number;
    idview: number;
    itemList = [];
    taxcodeList = [];
    orderType = [];
    sponserList = [];
    showLastRow: boolean;
    showLastRowMinus: boolean;
    showLastRowPlus: boolean;
    listshowLastRowMinus: boolean;
    listshowLastRowPlus: boolean;
    viewDisabled: boolean;
    accountcodeList = [];
    fundList = [];
    purchaseRequisitionList = [];
    departmentList = [];
    activitycodeList = [];
    budgetaccountcodeList = [];
    budgetfundList = [];
    budgetdepartmentList = [];
    budgetactivitycodeList = [];
    supplierList = [];
    orderTypeList = [];
    valueDate: string;
    gstValue: number;
    Amount: number;
    taxAmount: number;
    totalAmount: number;
    type: string;
    purchaseRequisitionListBasedonId = [];
    constructor(
        private TenderquatationService: TenderquatationService,
        private ItemcategoryService: ItemcategoryService,
        private DepartmentService: DepartmentService,
        private route: ActivatedRoute,
        private router: Router,
        private StaffService: StaffService,
        private ItemsetupService: ItemsetupService,
        private ActivitycodeService: ActivitycodeService,
        private AccountcodeService: AccountcodeService,
        private FundService: FundService,
        private FinancialyearService: FinancialyearService,
        private TaxsetupcodeService: TaxsetupcodeService,
        private PurchaserequistionentryService: PurchaserequistionentryService,
        private PrapprovalService: PrapprovalService,
        private spinner: NgxSpinnerService,
    ) { }
    ngDoCheck() {

        if (this.ajaxCount == 0 && this.specList.length > 0) {
            this.ajaxCount = 20;
            console.log("asdfsdf");
            this.showIcons();
        }

        const change = this.ajaxCount;
        console.log(this.ajaxCount);
        if (this.ajaxCount == 0) {
            this.editFunction();
            this.spinner.hide();
            this.ajaxCount = 10;
        }
    }
    getPRData(){
        console.log(this.tenderData['f035fidCategory']);
        let newrequestObject = {};
        newrequestObject['orderType'] = this.tenderData['f035fidCategory'];
        console.log(newrequestObject);

        this.TenderquatationService.getTenderAndQuotationRequisiontData(newrequestObject).subscribe(
            data =>{
                this.purchaseRequisitionList = data['result'];

            }
        );
    }
    getPurchaseOrderDetails() {
        let PurchaseOrderId = this.tenderData['f034fidPurchaseRequisition'];
        this.PurchaserequistionentryService.getItemsDetail(PurchaseOrderId).subscribe(
            data => {
                this.purchaseRequisitionListBasedonId = data['result'];
                this.tenderData['f034forderType'] = data['result'][0]['f088forderType'];
                this.tenderData['f035fidFinancialYear'] = data['result'][0]['f088fidFinancialyear'];
                this.tenderData['f034fidSupplier'] = data['result'][0]['f088fidSupplier'];
                this.tenderData['f035fidDepartment'] = data['result'][0]['f088fidDepartment'];
                this.tenderData['f034fdescription'] = data['result'][0]['f088fdescription'];
                this.tenderData['f034ftotalAmount'] = data['result'][0]['f088ftotalAmount'];
                for (var i = 0; i < this.purchaseRequisitionListBasedonId.length; i++) {
                    var purchaseOrderObject = {}
                    purchaseOrderObject['f038fidItem'] = this.purchaseRequisitionListBasedonId[i]['f088fidItem'];
                    purchaseOrderObject['f038funit'] = this.purchaseRequisitionListBasedonId[i]['f088funit'];
                    purchaseOrderObject['f038ffundCode'] = this.purchaseRequisitionListBasedonId[i]['f088ffundCode'];
                    purchaseOrderObject['f038factivityCode'] = this.purchaseRequisitionListBasedonId[i]['f088factivityCode'];
                    purchaseOrderObject['f038fdepartmentCode'] = this.purchaseRequisitionListBasedonId[i]['f088fdepartmentCode'];
                    purchaseOrderObject['f038faccountCode'] = this.purchaseRequisitionListBasedonId[i]['f088faccountCode'];
                    purchaseOrderObject['f038fbudgetFundCode'] = this.purchaseRequisitionListBasedonId[i]['f088fbudgetFundCode'];
                    purchaseOrderObject['f038fbudgetActivityCode'] = this.purchaseRequisitionListBasedonId[i]['f088fbudgetActivityCode'];
                    purchaseOrderObject['f038fbudgetDepartmentCode'] = this.purchaseRequisitionListBasedonId[i]['f088fbudgetDepartmentCode'];
                    purchaseOrderObject['f038fbudgetAccountCode'] = this.purchaseRequisitionListBasedonId[i]['f088fbudgetAccountCode'];
                    purchaseOrderObject['f038fsoCode'] = this.purchaseRequisitionListBasedonId[i]['f088fsoCode'];
                    purchaseOrderObject['f038frequiredDate'] = this.purchaseRequisitionListBasedonId[i]['f088frequiredDate'];
                    purchaseOrderObject['f038fquantity'] = this.purchaseRequisitionListBasedonId[i]['f088fquantity'];
                    purchaseOrderObject['f038fprice'] = this.ConvertToFloat(this.purchaseRequisitionListBasedonId[i]['f088fprice']).toFixed(2);
                    purchaseOrderObject['f038total'] = this.ConvertToFloat(this.purchaseRequisitionListBasedonId[i]['f088total']).toFixed(2);
                    purchaseOrderObject['f038ftaxCode'] = this.purchaseRequisitionListBasedonId[i]['f088ftaxCode'];
                    purchaseOrderObject['f038fpercentage'] = this.purchaseRequisitionListBasedonId[i]['f088fpercentage'];
                    purchaseOrderObject['f038ftaxAmount'] = this.ConvertToFloat(this.purchaseRequisitionListBasedonId[i]['f088ftaxAmount']).toFixed(2);;
                    purchaseOrderObject['f038ftotalIncTax'] = this.ConvertToFloat(this.purchaseRequisitionListBasedonId[i]['f088ftotalIncTax']).toFixed(2);
                    purchaseOrderObject['categoryName'] = this.purchaseRequisitionListBasedonId[i]['categoryName'];
                    purchaseOrderObject['subCategoryName'] = this.purchaseRequisitionListBasedonId[i]['subCategoryName'];
                    purchaseOrderObject['itemName'] = this.purchaseRequisitionListBasedonId[i]['itemName'];
                    purchaseOrderObject['f038fitemType'] = this.purchaseRequisitionListBasedonId[i]['f088fcategoryType'];
                    purchaseOrderObject['f038fitemCategory'] = this.purchaseRequisitionListBasedonId[i]['f088fitemCategory'];
                    purchaseOrderObject['f038fitemSubCategory'] = this.purchaseRequisitionListBasedonId[i]['f088fitemSubCategory'];
                    purchaseOrderObject['f038fassetCode'] = this.purchaseRequisitionListBasedonId[i]['f088fassetCode'];
                    // purchaseOrderObject['f034ftotalIncTax'] = this.ConvertToFloat(this.purchaseRequisitionListBasedonId[i]['f088ftotalIncTax']).toFixed(2);
                    console.log(purchaseOrderObject);
                    this.specList.push(purchaseOrderObject);
                }
                this.showLastRow = true;
                this.showIcons();
                console.log(this.specList);
            }
        );
    }
    getAllpurchaseGlList() {
        var idfinancialYear = this.tenderData['f035fidFinancialYear'];

        this.PurchaserequistionentryService.urlgetAllpurcahseGls(idfinancialYear).subscribe(
            data => {
                var depArray = data['result']['Department'];
                this.budgetdepartmentList = depArray;

                this.budgetfundList = data['result']['Fund'];

                this.budgetaccountcodeList = data['result']['Account'];

                this.budgetactivitycodeList = data['result']['Activity'];

                console.log(data);
            }, error => {
                console.log(error);
            }
        );
    }
    showIcons() {
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.specList.length > 0) {
            this.showLastRowMinus = true;
            this.showLastRowPlus = true;
            this.listshowLastRowMinus = true;
        }

        if (this.specList.length > 1) {
            if (this.showLastRow == false) {
                this.listshowLastRowPlus = false;
                this.listshowLastRowMinus = true;
                this.showLastRowPlus = true;

            } else {
                this.listshowLastRowPlus = true;
                this.listshowLastRowMinus = false;

            }
        }

        if (this.specList.length == 1 && this.showLastRow == false) {
            this.listshowLastRowMinus = true;
            this.listshowLastRowPlus = false;

        }
        if (this.specList.length == 1 && this.showLastRow == true) {
            this.listshowLastRowMinus = false;
            this.listshowLastRowPlus = true;

        }
        if (this.specList.length == 0 && this.showLastRow == false) {
            this.showLastRowMinus = false;
            this.showLastRowPlus = true;
        }
        if (this.specList.length == 0 && this.showLastRow == true) {
            this.showLastRowMinus = true;
            this.showLastRowPlus = false;
        }
        if (this.viewDisabled == true) {
            this.listshowLastRowPlus = false;
        }
        console.log(this.tenderList.length);
        console.log("Show last row" + this.showLastRow);
        console.log("showLastRowMinus" + this.showLastRowMinus);
        console.log("showLastRowPlus" + this.showLastRowPlus);
        console.log("listshowLastRowMinus" + this.listshowLastRowMinus);
        console.log("listshowLastRowPlus" + this.listshowLastRowPlus);
    }
    editFunction() {
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.route.paramMap.subscribe((data) => this.idview = + data.get('idview'));
        this.ajaxCount++;
        if (this.id > 0) {
            this.TenderquatationService.getItemsDetail(this.id).subscribe(
                data => {
                    this.ajaxCount--;
                    this.tenderData = data['result'];
                    console.log(data['result']);
                    this.committList = data['result']['commitee-details'];
                    this.specList = data['result']['spec-details'];
                    this.tenderData = data['result']['tender-result'][0]
                    if (data['result']['tender-result'][0]['f035ftype'] == 'TND') {
                        this.tenderData['f035fidCategory'] = 1;
                    }
                    if (data['result']['tender-result'][0]['f035ftype'] == 'QN') {
                        this.tenderData['f035fidCategory'] = 2;
                    }
                    this.getAllpurchaseGlList();
                    this.tenderData['f035fstartDate'] = this.tenderData['f035fstartDate'].replace(" ", "T");
                    this.tenderData['f035fendDate'] = this.tenderData['f035fendDate'].replace(" ", "T");;///'2018-11-15T03:45:00';


                    if (data['result']['tender-result'].f035fstatus === true) {
                        console.log(data);
                        this.tenderData['f035fstatus'] = 1;
                    } else {
                        this.tenderData['f035fstatus'] = 0;

                    }
                    if (data['result']['tender-result'][0]['f035fshortlisted'] == 1) {
                        this.viewDisabled = true;
                        console.log(data['result']['tender-result']['f035fshortlisted']);
                        $("#target input,select").prop("disabled", true);
                        $("#target1 input,select").prop("disabled", true);
                        $("#target2 input,select").prop("disabled", true);
                    }

                    console.log(this.tenderData);
                    this.showIcons();
                }, error => {
                    console.log(error);
                });
        }


    }
    deletemodule() {


        this.showLastRow = true;
        this.showIcons();
    }

    showNewRow() {
        var confirmPop = confirm("Do you want to Add?");
        if (confirmPop == false) {
            return false;
        }
        this.showLastRow = false;
        this.showIcons();
    }

    fillDetails() {
        console.log(this.specData['f038fidItem']);
        for (var i = 0; i < this.itemsetupList.length; i++) {
            if (this.itemsetupList[i]['f029fid'] == this.specData['f038fidItem']) {
                this.specData['f038fidItem'] = this.itemsetupList[i]['f029funit'];
                //this.specData[''] = this.itemsetupList[i]['tax;'];
            }
        }

    }
    ngOnInit() {
        this.ajaxCount = 0;
        this.spinner.show();
        this.viewDisabled = false;
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id < 1) {
            this.showLastRow = false;
            this.showLastRowMinus = false;
            this.showLastRowPlus = true;
        } else {
            this.showLastRow = true;

        }
        this.tenderData['f035fstatus'] = 1;
        this.tenderData['f035fidDepartment'] = '';
        this.tenderData['f035fidCategory'] = '';
        this.tenderData['f035ftype'] = '';

        let categoryobj = {};
        categoryobj['name'] = 'Tender';
        this.fcategory.push(categoryobj);
        categoryobj = {};
        categoryobj['name'] = 'Quotation';
        this.fcategory.push(categoryobj);
        //item dropdown
        this.ajaxCount++;
        this.ItemsetupService.getActiveItems().subscribe(
            data => {
                this.ajaxCount--;
                // console.log(this.ajaxCount);

                this.itemList = data['result']['data'];
            }
        );

        // categoryList dropdown
        this.ajaxCount++;
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.ItemcategoryService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.categoryList = data['result']['data'];
            }, error => {
                console.log(error);
            });

        // department dropdown
        this.ajaxCount++;
        this.DepartmentService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.DeptList = data['result']['data'];
            }, error => {
                console.log(error);
            });

        this.ajaxCount++;
        this.StaffService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.smartStaffList = data['result']['data'];
                console.log(this.smartStaffList);
            }, error => {
                console.log(error);
            });
        // this.ajaxCount++;
        // this.ItemsetupService.getItems().subscribe(
        //     data => {
        //         this.ajaxCount--;
        //         this.itemsetupList = data['result']['data'];
        //         console.log(this.itemsetupList);
        //     }, error => {
        //         console.log(error);
        //     });


        //Financial year dropdown
        this.ajaxCount++;
        this.FinancialyearService.getActiveItems().subscribe(
            data => {
                this.ajaxCount--;
                // console.log(this.ajaxCount);

                this.financialList = data['result'];
            }
        );

        //Department dropdown
        this.ajaxCount++;
        this.DepartmentService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                // console.log(this.ajaxCount);

                this.departmentList = data['result']['data'];
            }
        );
        //Department dropdown
        this.ajaxCount++;
        this.DepartmentService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.DeptList = data['result']['data'];
            }
        );


        //Activity dropdown
        this.ajaxCount++;
        this.ActivitycodeService.getItemsByLevel().subscribe(
            data => {
                this.ajaxCount--;
                this.activitycodeList = data['result']['data'];
            }
        );


        //Account dropdown
        this.ajaxCount++;
        this.AccountcodeService.getItemsByLevel().subscribe(
            data => {
                this.ajaxCount--;
                this.accountcodeList = data['result']['data'];
            }
        );



        //Fund dropdown
        this.ajaxCount++;
        this.FundService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.fundList = data['result']['data'];
            }
        );


        this.ajaxCount++;
        this.TaxsetupcodeService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.taxcodeList = data['result']['data'];
            });
        this.showIcons();

        console.log(this.id);
    }

    ConvertToFloat(val) {
        return parseFloat(val);
    }
    getUOM() {
        let itemId = this.specData['f038fidItem'];
        console.log(itemId);
        this.ItemsetupService.getItemsDetail(itemId).subscribe(
            data => {
                this.itemsetupList = data['result'][0];
                console.log(data);
                this.specData['f038funit'] = data['result'][0]['f029funit'];
            }
        );
    }

    fillUOM() {
        var iditem = this.specData['f038fidItem'];
        for (var i = 0; i < this.itemsetupList.length; i++) {
            if (this.itemsetupList[i]['f029fid'] == iditem) {
                this.specData['f038funit'] = this.itemsetupList[i]['f029funit'];
            }
        }
    }

    addListGeneralSpecs() {
        if (this.generalspecData['f038faccountCode'] == this.generalspecData['f038fbudgetAccountCode']) {
            alert("Cannot have same account code");
            return false;

        } else {
            this.generalspecList.push(this.generalspecData);
            this.generalspecData = {};
        }
    }

    addEntrylist() {

        if (this.specData['f038fidItem'] == undefined) {
            alert('Select Item');
            return false;
        }
        if (this.specData['f038ffundCode'] == undefined) {
            alert('Select GL Fund');
            return false;
        }
        if (this.specData['f038factivityCode'] == undefined) {
            alert('Select GL Activity Code');
            return false;
        }
        if (this.specData['f038fdepartmentCode'] == undefined) {
            alert('Select GL Cost Center');
            return false;
        }
        if (this.specData['f038faccountCode'] == undefined) {
            alert('Select GL Account Code');
            return false;
        }
        if (this.specData['f038fbudgetFundCode'] == undefined) {
            alert('Select Budget Fund');
            return false;
        }
        if (this.specData['f038fbudgetActivityCode'] == undefined) {
            alert('Select Budget Activity Code');
            return false;
        }
        if (this.specData['f038fbudgetDepartmentCode'] == undefined) {
            alert('Select Budget Cost Center');
            return false;
        }
        if (this.specData['f038fbudgetAccountCode'] == undefined) {
            alert('Select Budget Account Code');
            return false;
        }
        if (this.specData['f038fsoCode'] == undefined) {
            alert('Enter SoCode');
            return false;
        }
        if (this.specData['f038frequiredDate'] == undefined) {
            alert('Select Delivery Date');
            return false;
        }
        var confirmPop = confirm("Do you want to Add?");
        if (confirmPop == false) {
            return false;
        }
        var dataofCurrentRow = this.specData;
        this.specList.push(dataofCurrentRow);
        this.showIcons();
        this.dateComparison();
        //this.onBlurMethod();
        this.specData = {};

    }

    getGstvalue() {
        let taxId = this.specData['f038ftaxCode'];
        let quantity = this.specData['f038fquantity'];
        let price = this.specData['f038fprice'];
        //console.log(this.taxcodeList);
        var taxSelectedObject = {};
        for (var k = 0; k < this.taxcodeList.length; k++) {
            if (this.taxcodeList[k]['f081fid'] == taxId) {
                taxSelectedObject = this.taxcodeList[k];
            }
        }


        this.gstValue = taxSelectedObject['f081fpercentage'];
        this.specData['f038fpercentage'] = this.gstValue;
        this.Amount = (this.ConvertToFloat(price)) * (this.ConvertToFloat(quantity));
        this.taxAmount = (this.ConvertToFloat(this.gstValue)) / 100 * this.ConvertToFloat((this.Amount));

        let totalAm = ((this.taxAmount) + (this.Amount));
        this.specData['f038ftotal'] = this.Amount.toFixed(2);
        this.specData['f038ftaxAmount'] = this.taxAmount.toFixed(2);
        this.specData['f038ftotalIncTax'] = totalAm.toFixed(2);
    }
    addListSpec() {
        this.specList.push(this.specData);
        this.specData = {};
    }

    addListCommittee() {

        var confirmPop = confirm("Do you want to Add?");
        if (confirmPop == false) {
            return false;
        }
        this.committListData['f038fname'] = '-';
        this.committList.push(this.committListData);
        this.committListData = {};
        this.showIcons();
    }
    dateComparison1() {

        let startDate = Date.parse(this.tenderData['f035fstartDate']);
        let endDate = Date.parse(this.tenderData['f035fendDate']);
        console.log(startDate);
        console.log(endDate);
        if (startDate > endDate) {
            alert("Start Date cannot be greater than End Date !");
            this.tenderData['f035fendDate'] = "";
        }

    }

    getReferenceNumber() {
        var idCat = this.tenderData['f035fidCategory'];
        if (this.tenderData['f035fidCategory'] == 1) {
            idCat = 'TND';
            this.tenderData['f035ftype'] = 'TND';

        } else {
            idCat = 'QN';
            this.tenderData['f035ftype'] = 'QN';
        }
        let typeObject = {};
        typeObject['type'] = idCat;
        this.TenderquatationService.getNumber(typeObject).subscribe(
            newData => {
                console.log(newData);
                this.tenderData['f035fquotationNumber'] = newData['number'];
            }
        );
    }
    deleteEntry(object) {
        var cnf = confirm("Do you really want to delete");
        if (cnf == true) {
            var index = this.specList.indexOf(object);
            this.deleteList.push(this.specList[index]['f038fid']);
            let deleteObject = {};
            deleteObject['id'] = this.deleteList;
            this.TenderquatationService.deleteItems(deleteObject).subscribe(
                data => {
                }, error => {
                    console.log(error);
                });
            if (index > -1) {
                this.specList.splice(index, 1);
            }
            this.showIcons();
        }
    }

    dateComparison() {

        let requireDate = Date.parse(this.specData['f038frequiredDate']);
        let endDate = Date.parse(this.tenderData['f035fendDate']);
        console.log(endDate);
        if (requireDate < endDate) {
            alert("Delivery Date cannot be Less than End Date !");
            this.specData['f038frequiredDate'] = "";
        }

    }
    dateComparisonList1() {
        for (var i = 0; i < this.specList.length; i++) {
            let requireDate = Date.parse(this.specList[i]['f038frequiredDate']);
            let endDate = Date.parse(this.tenderData['f035fendDate']);
            console.log(endDate);
            if (requireDate < endDate) {
                alert("Delivery Date cannot be Less than End Date !");
                this.specList[i]['f038frequiredDate'] = "";
                console.log(requireDate);
                console.log(endDate);
            }
        }
    }
    addtender() {
        if (this.tenderData['f035fidCategory'] == undefined) {
            alert('Select Category');
            return false;
        }
        if (this.tenderData['f035fstartDate'] == undefined) {
            alert('Select Start Date and Time');
            return false;
        }
        if (this.tenderData['f035fendDate'] == undefined) {
            alert('Select End Date and Time');
            return false;
        }
        if (this.tenderData['f035ftitle'] == undefined) {
            alert('Enter Title');
            return false;
        }
        if (this.tenderData['f035fidDepartment'] == undefined) {
            alert('Select Department');
            return false;
        }
        if (this.tenderData['f035fidFinancialYear'] == undefined) {
            alert('Select Financial Year');
            return false;
        }
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        if (this.showLastRow == false) {
            var addactivities = this.addEntrylist();
            if (addactivities == false) {
                return false;
            }
        }

        console.log(this.tenderData);
        console.log(this.specList);
        console.log(this.committList);
        this.tenderData['commitee-details'] = this.committList;
        this.tenderData['spec-details'] = this.specList;
        this.tenderData['general-details'] = this.generalspecList;

        if (this.specList.length > 0) {

        } else {
            alert("There should be atleast one Item");
            return false;
        }
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        for (var i = 0; i < this.tenderData['spec-details'].length; i++) {

            let balanceObj = {};
            balanceObj['FinancialYear'] = this.tenderData['f035fidFinancialYear'];
            balanceObj['Department'] = this.tenderData['spec-details'][i]['f038fbudgetDepartmentCode'];
            balanceObj['Fund'] = this.tenderData['spec-details'][i]['f038fbudgetFundCode'];
            balanceObj['Activity'] = this.tenderData['spec-details'][i]['f038fbudgetActivityCode'];
            balanceObj['Account'] = this.tenderData['spec-details'][i]['f038fbudgetAccountCode'];
            balanceObj['Amount'] = this.tenderData['spec-details'][i]['f088ftotalIncTax'];
            if (this.id > 0) {
                balanceObj['f038fid'] = this.id;
            }
            this.PrapprovalService.getBalanceStatus(balanceObj).subscribe(
                data => {

                    if (data['result'] == 0) {
                        alert("Glcode has no amount");
                        return false;
                    }
                });
        }
        this.tenderData['f035ftenderStatus'] = 0;
        this.TenderquatationService.insertTenderquatationItems(this.tenderData).subscribe(
            data => {
                if (this.id > 0) {
                    alert("Tender Quotation has been Updated Successfully")
                }
                else {
                    alert("Tender Quotation has been Added Successfully")
                }
                this.router.navigate(['procurement/tenderquotation']);
            },
            error => {
                console.log(error);
            });
    }
    maximize() {
        $("#myModal").addClass("modal");
        $("#overlay1").show();
        $(".maximize-icon").addClass("hidden");
        $(".minimize-icon").removeClass("hidden");
        $(".modal-backdrop").show();

    }
    minimize() {
        $("#myModal").removeClass("modal");
        $("#overlay1").hide();
        $(".maximize-icon").removeClass("hidden");
        $(".minimize-icon").addClass("hidden");
        $(".modal-backdrop").hide();
        $('#myModal').show();
    }
}



