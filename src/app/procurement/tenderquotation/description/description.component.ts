import { Injector } from '@angular/core';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DescriptionService } from '../../service/description.service';
import { AlertService } from '../../../_services/alert.service';
import { StaffService } from "../../../loan/service/staff.service";

@Component({
    selector: 'DescriptionComponent',
    templateUrl: 'description.component.html'
})

export class DescriptionComponent implements OnInit {

    descriptionList = [];
    descriptionData = {};
    staffList = [];
    descriptionDataHeader = {};
    ajaxcount = 0;


    id: number;
    constructor(

        private StaffService: StaffService,
        private DescriptionService: DescriptionService,
        private AlertService: AlertService,
        private route: ActivatedRoute,
        private router: Router

    ) { }

    ngDoCheck() {
        const change = this.ajaxcount;
        console.log(this.ajaxcount);
        if (this.ajaxcount == 0) {
            this.editFunction();
            this.ajaxcount = 10;
        }
    }

    editFunction() {
    }

    ngOnInit() {
        this.StaffService.getItems().subscribe(
            data => {
                this.staffList = data['result']['data'];
            }, error => {
                console.log(error);
            });


        }

    deleteList(object){
        var cnf = confirm("Do you really want to delete");
        if (cnf == true) {
            console.log(this.descriptionList);
            var index = this.descriptionList.indexOf(object);;
            if (index > -1) {
                this.descriptionList.splice(index, 1);
            }
          
        }
    }

    addList(){
        var dataofCurrentRow = this.descriptionData;
        console.log("asdf");
        this.descriptionList.push(dataofCurrentRow);
        this.descriptionData = {};

    }

    // getVendorCode(){

    //     let typeObject = {};
    //     typeObject['type']  = "Vendor"
    //     this.DescriptionService.getCode(typeObject).subscribe(
    //         data=>{
    //             this.vendorDataHeader['f030fvendorCode'] = data['number'];
    //         }
    //     );
    // }

    saveVendorData() {


// let VendorObject = {};
//         this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
//         console.log(this.id);
//         if (this.id > 0) {
//             VendorObject['f031fid'] = this.id;
//         }
//         if(this.licenseDataList.length>0) {

//         }  else {
//             alert("Please add one License details");
//             return false;
//         }

//         VendorObject['f030fcompanyName'] = this.vendorDataHeader['f030fcompanyName'];
//         VendorObject['f030femail'] = this.vendorDataHeader['f030femail'];
//         VendorObject['f030faddress'] = this.vendorDataHeader['f030faddress'];
//         VendorObject['f030fphone'] = this.vendorDataHeader['f030fphone'];
//         VendorObject['f030fcontactPerson1'] = "person1";
//         VendorObject['f030fstartDate'] = this.vendorDataHeader['f030fstartDate'];
//         VendorObject['f030fendDate'] = this.vendorDataHeader['f030fendDate'];
//         VendorObject['f030fcity'] = this.vendorDataHeader['f030fcity'];
//         VendorObject['f030fpostCode'] = this.vendorDataHeader['f030fpostCode'];
//         VendorObject['f030fstate'] = this.vendorDataHeader['f030fstate'];
//         VendorObject['f030fcountry'] = this.vendorDataHeader['f030fcountry'];
//         VendorObject['f030fwebsite'] = this.vendorDataHeader['f030fwebsite'];
//         VendorObject['f030fcontactPerson2'] = "person2";
//         VendorObject['f030fcontact1Name'] = this.vendorDataHeader['f030fcontact1Name'];
//         VendorObject['f030fcontact1Email'] = this.vendorDataHeader['f030fcontact1Email'];
//         VendorObject['f030fcontact1Phone'] = this.vendorDataHeader['f030fcontact1Phone'];
//         VendorObject['f030fcontact2Name'] = this.vendorDataHeader['f030fcontact2Name'];
//         VendorObject['f030fcontact2Email'] = this.vendorDataHeader['f030fcontact2Email'];
//         VendorObject['f030fcontact2Phone'] = this.vendorDataHeader['f030fcontact2Phone'];
//         VendorObject['f030fidBank'] = this.vendorDataHeader['f030fidBank'];
//         VendorObject['f030faccountNumber'] = this.vendorDataHeader['f030faccountNumber'];
//         VendorObject['f030ffax'] = this.vendorDataHeader['f030ffax'];
//         VendorObject['f030fvendorCode'] = this.vendorDataHeader['f030fvendorCode'];

//         //file upload
//         VendorObject['f030fcompanyRegistration'] = "file";

//       //license Details
//         VendorObject['license-details'] = this.licenseDataList;

//         console.log(VendorObject);

//         if (this.id > 0) {
//             this.SupplierregistrationService.updateSupplierregistrationItems(VendorObject, this.id).subscribe(
//                 data => {
//                     this.router.navigate(['procurement/supplierregistration']);
//                     this.AlertService.success("Updated Sucessfully !!")

//                 }, error => {
//                     console.log(error);
//                 });


//         } else {

//             this.SupplierregistrationService.insertSupplierregistrationItems(VendorObject).subscribe(
//                 data => {
//                     this.router.navigate(['procurement/supplierregistration']);
//                     this.AlertService.success("Added Sucessfully !!")

//                 }, error => {
//                     console.log(error);
//                 });

        // }
    }

}