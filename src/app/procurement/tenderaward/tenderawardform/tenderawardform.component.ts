import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { TenderquatationService } from '../../service/tenderquotation.service'
import { ActivatedRoute, Router } from '@angular/router';
import { StaffService } from "../../../loan/service/staff.service";
import { SupplierregistrationService } from "../../../procurement/service/supplierregistration.service";
import { TaxsetupcodeService } from '../../../generalledger/service/taxsetupcode.service'
import { LicenseService } from '../../service/license.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'TenderawardformComponent',
    templateUrl: 'tenderawardform.component.html'
})

export class TenderawardformComponent implements OnInit {
    tenderList = [];
    tenderData = {};
    DeptList = [];
    deptData = [];
    categoryList = [];
    committListData = {};
    committList = [];
    shortList = [];
    specData = {};
    smartStaffList = [];
    tenderQuotationList = [];
    shortListData = {};
    vendorList = [];
    taxcodeList = [];
    vendorListNew = [];
    licenseList = [];
    generalspecData = {};
    generalspecList = [];
    supplierList = [];
    remarkList = [];
    ajaxCount : number;
    id: number;

    constructor(
        private TenderquatationService: TenderquatationService,
        private SupplierregistrationService: SupplierregistrationService,
        private route: ActivatedRoute,
        private TaxsetupcodeService: TaxsetupcodeService,
        private router: Router,
        private StaffService: StaffService,
        private LicenseService: LicenseService,
        private spinner: NgxSpinnerService,
    ) { }
    showTenderItems() {
        console.log("asdfsdf");
        this.TenderquatationService.getAllAwardListBasedOnTendor(this.specData['f050fidTendor']).subscribe(
            data => {
                console.log(data);

                this.specData['f050fstartDate'] = data['result']['vendor-details'][0]['f035fstartDate'].replace(" ", "T");;
                this.specData['f050fendDate'] = data['result']['vendor-details'][0]['f035fendDate'].replace(" ", "T");;
                this.specData['f050ftitle'] = data['result']['vendor-details'][0]['f035ftitle'];
                this.specData['f050fgrade'] = data['result']['vendor-details'][0]['f050fgrade'];
                this.specData['f050ftenderOpeningDate'] = data['result']['vendor-details'][0]['f035fopeningDate'].replace(" ", "T");;

                this.committList = data['result']['commitee-details'];
                for (var i = 0; i < this.committList.length; i++) {
                    this.committList[i]['f051fidStaff'] = this.committList[i]['f051fidStaff'];
                }
                this.supplierList = data['result']['vendor-details'];
                this.remarkList = data['result']['remarks'];
                for (var j = 0; j < this.supplierList.length; j++) {
                    this.supplierList[j]['statuscheckbox'] = false;
                    this.supplierList[j]['f050fgrade'] = parseInt(this.supplierList[j]['f050fgrade']);
                }
            }, error => {
                console.log(error);
            });
    }

    getVendorName(obj, value) {
        console.log(obj);
        console.log(value);
        console.log(this.vendorList);
    }




    ngDoCheck() {
        const change = this.ajaxCount;
        if (this.ajaxCount == 0) {
        this.spinner.hide();            
        }
    }




    ngOnInit() {
        this.spinner.show();
        this.tenderData['f035fstatus'] = 1;
        this.tenderData['f035fidDepartment'] = '';
        this.tenderData['f035fidCategory'] = '';
        this.tenderData['f035ftype'] = '';
        $('.dropdownSelect').hide();
        this.ajaxCount = 0;
        this.ajaxCount++;
        this.TenderquatationService.getAwardShorlists().subscribe(
            data => {
                this.ajaxCount--;
                this.tenderQuotationList = data['result'];
                console.log(this.tenderQuotationList);
            }, error => {
                console.log(error);
            });
            this.ajaxCount++;
        this.TaxsetupcodeService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.taxcodeList = data['result']['data'];
            });
            this.ajaxCount++;
        this.SupplierregistrationService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.vendorList = data['result']['data'];
            }, error => {
                console.log(error);
            });
            this.ajaxCount++;
        this.LicenseService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.licenseList = data['result']['data'];
            }, error => {
                console.log(error);
            });

        // staff dropdown
        this.ajaxCount++;
        this.StaffService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.smartStaffList = data['result']['data'];
                console.log(this.smartStaffList);
            }, error => {
                console.log(error);
            });

        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        console.log(this.id);

        if (this.id > 0) {
            this.ajaxCount++;
            this.TenderquatationService.getItemsDetail(this.id).subscribe(
                data => {
                this.ajaxCount++;
                    this.tenderData = data['result'][0];
                    if (data['result'].f035fstatus === true) {
                        console.log(data);
                        this.tenderData['f035fstatus'] = 1;
                    } else {
                        this.tenderData['f035fstatus'] = 0;

                    }
                    console.log(this.tenderData);
                }, error => {
                    console.log(error);
                });
        }


        console.log(this.id);
    }



    addListCommittee() {
        this.shortList.push(this.shortListData);
        this.shortListData = {};

    }

    addListGeneralSpecs() {
        this.generalspecList.push(this.generalspecData);
        this.generalspecData = {};
    }

    saveData() {
        console.log(this.vendorList);
        var finalArray = [];
        for (var i = 0; i < this.supplierList.length; i++) {
            if (this.supplierList[i]['statuscheckbox'] == true) {
                finalArray.push(this.supplierList[i]['f050fid']);
            }
        }
        if (this.specData['f050fidTendor'] == undefined) {
            alert('Select Reference Number ');
            return false;
        }
        if (finalArray.length > 1) {
            alert("Select only one vendor");
            return false;
        }
        if (finalArray.length < 1) {
            alert("Select atleast one vendor");
            return false;
        }
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        var finalTen = {};
        console.log(this.vendorList);
        finalTen['id'] = finalArray;
        finalTen['idTendor'] = this.specData['f050fidTendor'];
        console.log(finalTen);
        this.tenderData['f035ftenderStatus'] = 0;
        this.TenderquatationService.awardVendor(finalTen).subscribe(
            data => {
                alert("Tendor has been awared successfully")
                this.supplierList = [];
                this.remarkList = [];
                this.committList = [];
                this.specData = {};
            }, error => {
                console.log(error);
            });
    }

    addtender() {
        console.log(this.tenderData);
    }
}