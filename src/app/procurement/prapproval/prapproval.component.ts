import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { PrapprovalService } from '../service/prapproval.service'
import { AlertService } from './../../_services/alert.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'Prapproval',
    templateUrl: 'prapproval.component.html'
})

export class PrapprovalComponent implements OnInit {

    prapprovalList = [];
    prapprovalData = {};
    approvalData = {};
    checked: boolean;
    constructor(

        private PrapprovalService: PrapprovalService,
        private AlertService: AlertService,
        private spinner: NgxSpinnerService
    ) { }

    ngOnInit() {
        this.getListData();
    }
    getListData() {
        this.spinner.show();        
        this.PrapprovalService.getItems().subscribe(
            data => {
                this.spinner.hide();
                this.prapprovalList = data['result']['data'];
            }, error => {
                console.log(error);
            });
    }
}
