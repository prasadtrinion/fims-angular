// import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PurchaserequistionentryService } from '../../service/purchaserequistionentry.service';
import { TaxsetupcodeService } from '../../../generalledger/service/taxsetupcode.service'
import { ItemsetupService } from '../../../procurement/service/itemsetup.service';
import { AlertService } from '../../../_services/alert.service';
import { FinancialyearService } from '../../../generalledger/service/financialyear.service';
import { SupplierregistrationService } from '../../service/supplierregistration.service';
import { OrdertypeService } from '../../service/ordertype.service';
import { FundService } from '../../../generalsetup/service/fund.service';
import { AccountcodeService } from '../../../generalsetup/service/accountcode.service';
import { ActivitycodeService } from '../../../generalsetup/service/activitycode.service';
import { DepartmentService } from '../../../generalsetup/service/department.service';

@Component({
    selector: 'PrapprovalformComponent',
    templateUrl: 'prapprovalform.component.html'
})

export class PrapprovalformComponent implements OnInit {
    purchaserequistionentryList = [];
    purchaserequistionentryData = {};
    purchaserequistionentryDataheader = {};
    itemList = [];
    taxcodeList = [];
    orderType = [];
    sponserList = [];
    departmentList = [];
    DeptList = [];
    accountcodeList = [];
    fundList = [];
    activitycodeList = [];
    financialList = [];
    supplierList = [];
    orderTypeList = [];
    id: number;
    valueDate: string;
    ajaxCount: number;
    gstValue: number;
    Amount: number;
    taxAmount: number;
    totalAmount: number;
    type: string;

    constructor(

        private PurchaserequistionentryService: PurchaserequistionentryService,
        private TaxsetupcodeService: TaxsetupcodeService,
        private ItemService: ItemsetupService,
        private OrdertypeService: OrdertypeService,
        private alertService: AlertService,
        private DepartmentService: DepartmentService,
        private ActivitycodeService: ActivitycodeService,
        private AccountcodeService: AccountcodeService,
        private FundService: FundService,
        private FinancialyearService: FinancialyearService,
        private SupplierregistrationService: SupplierregistrationService,
        private route: ActivatedRoute,
        private router: Router

    ) { }


    ngDoCheck() {

        const change = this.ajaxCount;
        // console.log(this.ajaxCount);
        if (this.ajaxCount == 0) {
            this.editFunction();
            this.ajaxCount = 10;
        }
    }
    editFunction() {
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.PurchaserequistionentryService.getItemsDetail(this.id).subscribe(
                data => {
                    console.log(data['result']);
                    this.purchaserequistionentryList = data['result'];
                    this.purchaserequistionentryDataheader['f088forderType'] = data['result'][0]['f088forderType'];
                    this.purchaserequistionentryDataheader['f088fdate'] = data['result'][0]['f088fdate'];
                    this.purchaserequistionentryDataheader['f088fidSupplier'] = data['result'][0]['f088fidSupplier'];
                    this.purchaserequistionentryDataheader['f088fdescription'] = data['result'][0]['f088fdescription'];
                    this.purchaserequistionentryDataheader['f088freferenceNumber'] = data['result'][0]['f088freferenceNumber'];
                    this.purchaserequistionentryDataheader['f088fidFinancialyear'] = data['result'][0]['f088fidFinancialyear'];
                    this.purchaserequistionentryDataheader['f088fidDepartment'] = data['result'][0]['f088fidDepartment'];
                    this.purchaserequistionentryDataheader['f088ftotalAmount'] = data['result'][0]['f088ftotalAmount'];

                    setTimeout(function () {
                        $("#target input,select,textarea").prop("disabled", true);
                    }, 500);

                    console.log(this.purchaserequistionentryList);
                }, error => {
                    console.log(error);
                });
        }
    }

    ngOnInit() {
        this.getPurchaseRequisitionNumber();
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        this.ajaxCount = 0;
        //item dropdown
        this.ajaxCount++;
        this.ItemService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.itemList = data['result']['data'];
            }
        );
        //Supplierregistration dropdown
        this.ajaxCount++;
        this.SupplierregistrationService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                // console.log(this.ajaxCount);

                this.supplierList = data['result']['data'];
                console.log(this.supplierList);

            }
        );
        //order type dropdown
        this.ajaxCount++;
        this.OrdertypeService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.orderTypeList = data['result']['data'];

            }
        );
        //Financial year dropdown
        this.ajaxCount++;
        this.FinancialyearService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                // console.log(this.ajaxCount);

                this.financialList = data['result']['data'];
            }
        );
        //Department dropdown
        this.ajaxCount++;
        this.DepartmentService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                // console.log(this.ajaxCount);

                this.departmentList = data['result']['data'];
            }
        );
        //Department dropdown
        this.ajaxCount++;
        this.DepartmentService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.DeptList = data['result']['data'];
            }
        );
        //Activity dropdown
        this.ajaxCount++;
        this.ActivitycodeService.getItemsByLevel().subscribe(
            data => {
                this.ajaxCount--;
                this.activitycodeList = data['result']['data'];
            }
        );
        //Account dropdown
        this.ajaxCount++;
        this.AccountcodeService.getItemsByLevel().subscribe(
            data => {
                this.ajaxCount--;
                this.accountcodeList = data['result']['data'];
            }
        );
        //Fund dropdown
        this.ajaxCount++;
        this.FundService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.fundList = data['result']['data'];
            }
        );

        this.ajaxCount++;
        this.TaxsetupcodeService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.taxcodeList = data['result']['data'];
            });
    }

    getPurchaseRequisitionNumber() {
        let typeObject = {};
        typeObject['type'] = "PR";
        console.log(typeObject);
        this.PurchaserequistionentryService.getPurchaseRequisitionNumber(typeObject).subscribe(
            data => {
                this.purchaserequistionentryDataheader['f088freferenceNumber'] = data['number'];
                this.valueDate = new Date().toISOString().substr(0, 10);
                this.purchaserequistionentryDataheader['f088fdate'] = this.valueDate;
            }
        );
        return this.type;
    }

    ConvertToFloat(val) {
        return parseFloat(val);
    }

    savePurchaseRequisition() {

        let purchaseRequisitionObject = {};
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        // console.log(this.id);
        if (this.id > 0) {
            purchaseRequisitionObject['f088fid'] = this.id;
        }
        purchaseRequisitionObject['f088freferenceNumber'] = this.purchaserequistionentryDataheader['f088freferenceNumber'];
        purchaseRequisitionObject['f088forderType'] = this.purchaserequistionentryDataheader['f088forderType'];
        purchaseRequisitionObject['f088fdate'] = this.purchaserequistionentryDataheader['f088fdate'];
        purchaseRequisitionObject['f088fidSupplier'] = this.purchaserequistionentryDataheader['f088fidSupplier'];
        purchaseRequisitionObject['f088fidFinancialyear'] = this.purchaserequistionentryDataheader['f088fidFinancialyear'];
        purchaseRequisitionObject['f088fidDepartment'] = this.purchaserequistionentryDataheader['f088fidDepartment'];
        purchaseRequisitionObject['f088fdescription'] = this.purchaserequistionentryDataheader['f088fdescription'];
        purchaseRequisitionObject['f088fstatus'] = 0;
        purchaseRequisitionObject['purchase-details'] = this.purchaserequistionentryList;
        if (this.id > 0) {
            this.PurchaserequistionentryService.updateEntryItems(purchaseRequisitionObject, this.id).subscribe(
                data => {
                    this.router.navigate(['procurement/purchaserequisitionentry']);
                    this.alertService.success(" Updated Sucessfully ! ");
                }, error => {
                    console.log(error);
                });
        } else {
            this.PurchaserequistionentryService.insertEntryItems(purchaseRequisitionObject).subscribe(
                data => {
                    this.router.navigate(['procurement/purchaserequisitionentry']);
                    this.alertService.success(" Added Sucessfully ! ");

                }, error => {
                    console.log(error);
                });
        }
    }

    getGstvalue() {
        let taxId = this.purchaserequistionentryData['f088ftaxCode'];
        let quantity = this.purchaserequistionentryData['f088fquantity'];
        let price = this.purchaserequistionentryData['f088fprice'];
        var taxSelectedObject = {};
        for (var k = 0; k < this.taxcodeList.length; k++) {
            if (this.taxcodeList[k]['f081fid'] == taxId) {
                taxSelectedObject = this.taxcodeList[k];
            }
        }
        this.gstValue = taxSelectedObject['f081fpercentage'];
        this.purchaserequistionentryData['f088fpercentage'] = this.gstValue;
        this.Amount = (this.ConvertToFloat(price)) * (this.ConvertToFloat(quantity));
        this.taxAmount = (this.ConvertToFloat(this.gstValue)) / 100 * this.ConvertToFloat((this.Amount));

        let totalAm = ((this.taxAmount) + (this.Amount));
        this.purchaserequistionentryData['f088total'] = this.Amount.toFixed(2);
        this.purchaserequistionentryData['f088ftaxAmount'] = this.taxAmount.toFixed(2);
        this.purchaserequistionentryData['f088ftotalIncTax'] = totalAm.toFixed(2);
    }

    onBlurMethod() {
        var finaltotal = 0;
        for (var i = 0; i < this.purchaserequistionentryList.length; i++) {
            var totalamount = (this.ConvertToFloat(this.purchaserequistionentryList[i]['f088fquantity'])) * (this.ConvertToFloat(this.purchaserequistionentryList[i]['f088fprice']));
            var gstamount = (this.ConvertToFloat(this.gstValue) / 100 * (totalamount));
            this.purchaserequistionentryList[i]['f088ftotal'] = gstamount + totalamount;
            console.log(gstamount);
            console.log(totalamount)
            finaltotal = finaltotal + this.purchaserequistionentryList[i]['f088ftotal'];
        }
    }
    deleteEntry(object) {
        var cnf = confirm("Do you really want to delete");
        if (cnf == true) {
            var index = this.purchaserequistionentryList.indexOf(object);;
            if (index > -1) {
                this.purchaserequistionentryList.splice(index, 1);
            }
            this.onBlurMethod();
        }
    }
    addEntrylist() {

        var dataofCurrentRow = this.purchaserequistionentryData;
        this.purchaserequistionentryData = {};
        this.purchaserequistionentryList.push(dataofCurrentRow);
        this.onBlurMethod();
    }
}