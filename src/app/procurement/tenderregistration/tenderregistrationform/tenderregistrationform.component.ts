import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { TenderquatationService } from '../../service/tenderquotation.service'
import { ActivatedRoute, Router } from '@angular/router';
import { LicenseService } from '../../service/license.service';
import { StaffService } from "../../../loan/service/staff.service";
import { SupplierregistrationService } from "../../../procurement/service/supplierregistration.service";
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'TenderregistrationformComponent',
    templateUrl: 'tenderregistrationform.component.html'
})

export class TenderregistrationformComponent implements OnInit {
    tenderList = [];
    tenderData = {};
    DeptList = [];
    deptData = [];
    categoryList = [];
    committListData = {};
    committList = [];
    shortList = [];
    specData = {};
    smartStaffList = [];
    tenderQuotationList = [];
    shortListData = {};
    specList = [];
    itemsetupList = [];
    taxcodeList = [];
    ajaxCount: number;
    vendorData = {};
    vendorList = [];
    vendorListNew = [];
    id: number = 0;
    supplierList = [];
    smartStaffListNew = [];
    generalspecList = [];
    generalspecData = {};
    licenseList = [];
    remarkList = [];
    remarkData = {};
    deleteList2 = [];
    deleteList1 = [];
    deleteList = [];
    slno: number;
    showLastRow: boolean;
    showLastRowMinus: boolean;
    showLastRowPlus: boolean;
    listshowLastRowMinus: boolean;
    listshowLastRowPlus: boolean;
    showLastRow1: boolean;
    showLastRowMinus1: boolean;
    showLastRowPlus1: boolean;
    listshowLastRowMinus1: boolean;
    listshowLastRowPlus1: boolean;
    showLastRow2: boolean;
    showLastRowMinus2: boolean;
    showLastRowPlus2: boolean;
    listshowLastRowMinus2: boolean;
    listshowLastRowPlus2: boolean;
    viewDisabled: boolean;
    editDisabled: boolean;
    edit = '';
    constructor(
        private TenderquatationService: TenderquatationService,
        private SupplierregistrationService: SupplierregistrationService,
        private spinner: NgxSpinnerService,
        private route: ActivatedRoute,
        private router: Router,
        private StaffService: StaffService,
        private LicenseService: LicenseService
    ) { }

    ngDoCheck() {
        const change = this.ajaxCount;
        if (this.ajaxCount == 0) {
            this.editFunction();
            this.spinner.hide();
            this.ajaxCount = 10;
        }
    }
    deletemodule() {


        this.showLastRow = true;
        this.showIcons();
    }

    showIcons() {
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.vendorList.length > 0) {
            this.showLastRowMinus = true;
            this.showLastRowPlus = true;
            this.listshowLastRowMinus = true;
        }

        if (this.vendorList.length > 1) {
            if (this.showLastRow == false) {
                this.listshowLastRowPlus = false;
                this.listshowLastRowMinus = true;
                this.showLastRowPlus = true;

            } else {
                this.listshowLastRowPlus = true;
                this.listshowLastRowMinus = false;

            }
        }
        if (this.vendorList.length == 1 && this.showLastRow == false) {
            this.listshowLastRowMinus = true;
            this.listshowLastRowPlus = false;

        }
        if (this.vendorList.length == 1 && this.showLastRow == true) {
            this.listshowLastRowMinus = false;
            this.listshowLastRowPlus = true;

        }
        if (this.vendorList.length == 0 && this.showLastRow == false) {
            this.showLastRowMinus = false;
            this.showLastRowPlus = true;
        }
        if (this.vendorList.length == 0 && this.showLastRow == true) {
            this.showLastRowMinus = true;
            this.showLastRowPlus = false;
        }
        if (this.viewDisabled == true) {
            this.listshowLastRowPlus = false;
        }
    }
    showNewRow() {
        var confirmPop = confirm("Do you want to Add?");
        if (confirmPop == false) {
            return false;
        }
        this.showLastRow = false;
        this.showIcons();
    }
    deletemodule1() {


        this.showLastRow1 = true;
        this.showIcons1();
    }

    showIcons1() {
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        if (this.committList.length > 0) {
            this.showLastRowMinus1 = true;
            this.listshowLastRowMinus1 = true;
            this.showLastRowPlus1 = true;
        }

        if (this.committList.length > 1) {
            if (this.showLastRow1 == false) {
                this.listshowLastRowPlus1 = false;
                this.listshowLastRowMinus1 = true;
                this.showLastRowPlus1 = true;

            } else {
                this.listshowLastRowPlus1 = true;
                this.listshowLastRowMinus1 = false;

            }
        }
        if (this.committList.length == 1 && this.showLastRow1 == false) {
            this.listshowLastRowMinus1 = true;
            this.listshowLastRowPlus1 = false;

        }
        if (this.committList.length == 1 && this.showLastRow1 == true) {
            this.listshowLastRowMinus1 = false;
            this.listshowLastRowPlus1 = true;

        }
        if (this.committList.length == 0 && this.showLastRow1 == false) {
            this.showLastRowMinus1 = false;
            this.showLastRowPlus1 = true;
        }
        if (this.committList.length == 0 && this.showLastRow1 == true) {
            this.showLastRowMinus1 = true;
            this.showLastRowPlus1 = false;
        }
        if (this.viewDisabled == true) {
            this.listshowLastRowPlus1 = false;
        }
        console.log(this.committList.length);
        console.log("Show last row" + this.showLastRow1);
        console.log("showLastRowMinus" + this.showLastRowMinus1);
        console.log("showLastRowPlus" + this.showLastRowPlus1);
        console.log("listshowLastRowMinus" + this.listshowLastRowMinus1);
        console.log("listshowLastRowPlus" + this.listshowLastRowPlus1);
    }
    showNewRow1() {
        var confirmPop = confirm("Do you want to Add?");
        if (confirmPop == false) {
            return false;
        }
        this.showLastRow1 = false;
        this.showIcons1();
    }
    deletemodule2() {


        this.showLastRow2 = true;
        this.showIcons2();
    }

    showIcons2() {
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        if (this.remarkList.length > 0) {
            this.showLastRowMinus2 = true;
            this.listshowLastRowMinus2 = true;
            this.showLastRowPlus2 = true;
        }

        if (this.remarkList.length > 1) {
            if (this.showLastRow2 == false) {
                this.listshowLastRowPlus2 = false;
                this.listshowLastRowMinus2 = true;
                this.showLastRowPlus2 = true;

            } else {
                this.listshowLastRowPlus2 = true;
                this.listshowLastRowMinus2 = false;

            }
        }
        if (this.remarkList.length == 1 && this.showLastRow2 == false) {
            this.listshowLastRowMinus2 = true;
            this.listshowLastRowPlus2 = false;

        }
        if (this.remarkList.length == 1 && this.showLastRow2 == true) {
            this.listshowLastRowMinus2 = false;
            this.listshowLastRowPlus2 = true;

        }
        if (this.remarkList.length == 0 && this.showLastRow2 == false) {
            this.showLastRowMinus2 = false;
            this.showLastRowPlus2 = true;
        }
        if (this.remarkList.length == 0 && this.showLastRow2 == true) {
            this.showLastRowMinus2 = true;
            this.showLastRowPlus2 = false;
        }
        if (this.viewDisabled == true) {
            this.listshowLastRowPlus2 = false;
        }
        console.log(this.remarkList.length);
        console.log("Show last row" + this.showLastRow2);
        console.log("showLastRowMinus2" + this.showLastRowMinus2);
        console.log("showLastRowPlus2" + this.showLastRowPlus2);
        console.log("listshowLastRowMinus2" + this.listshowLastRowMinus2);
        console.log("listshowLastRowPlus2" + this.listshowLastRowPlus2);
    }
    showNewRow2() {
        var confirmPop = confirm("Do you want to Add?");
        if (confirmPop == false) {
            return false;
        }
        this.showLastRow2 = false;
        this.showIcons2();
    }
    editFunction() {
        this.ajaxCount++;
        if (this.id > 0) {
            this.edit = 'disabled';
            this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
            this.TenderquatationService.getAllListBasedOnTendor(this.id).subscribe(
                data => {
                    this.ajaxCount--;
                    this.TenderquatationService.getItems().subscribe(
                        data => {
                            this.ajaxCount--;
                            this.tenderQuotationList = data['result']['data'];

                            console.log('In edit');
                            console.log(this.tenderQuotationList);
                        }, error => {
                            console.log(error);
                        });
                    this.specData['f050fidTendor'] = this.id;
                    console.log('id = ' + this.id);
                    this.showTenderItems();
                    this.vendorList = data['result']['vendor-details'];
                    this.committList = data['result']['commitee-details'];

                    for (var i = 0; i < this.vendorList.length; i++) {
                        this.vendorList[i]['f050fcompletion'] = parseInt(this.vendorList[i]['f050fcompletionDuration']);
                        this.vendorList[i]['f050fgrade'] = parseInt(this.vendorList[i]['f050fgrade']);
                    }
                    for (var i = 0; i < this.committList.length; i++) {
                        this.committList[i]['f051fidStaff'] = this.committList[i]['f051fidStaff'];
                    }
                    this.remarkList = data['result']['remarks'];
                    var tenderData = data['result']['tender-details'];
                    console.log(tenderData);
                    if (tenderData['f035fshortlisted'] == 1) {
                        this.viewDisabled = true;
                        console.log(data['result']['vendor-details']['f035fshortlisted']);
                        $("#target input,select").prop("disabled", true);
                        $("#target1 input,select").prop("disabled", true);
                        $("#target2 input,select").prop("disabled", true);
                    }
                    this.editDisabled = true;
                    this.slno = this.vendorList.length + 1;
                    this.showIcons1();
                    this.showIcons();
                    this.showIcons2();
                }, error => {
                    console.log(error);
                });
        }
    }

    getVendorDetails() {
        console.log(this.vendorData['f037fidVendor']);
        for (var j = 0; j < this.smartStaffList.length; j++) {
            if (this.smartStaffList[j]['f030fid'] == this.vendorData['f037fidVendor']) {
                this.vendorData = this.smartStaffList[j];
            }
        }
    }

    pushVendorList() {

        console.log(this.vendorData);
        if (parseInt(this.vendorData['f050fidVendor']) < 0) {
            alert("Please select proper Vendor");
            return false;
        }
        if (this.vendorData['f050fidVendor'] === undefined) {
            alert('Select Vendor');
            return false;
        }
        if (this.vendorData['f050fgrade'] === undefined) {
            alert('Select Grade');
            return false;
        }
        if (this.vendorData['f050famount'] === undefined) {
            alert('Please Enter Amount');
            return false;
        }
        if (this.vendorData['f050fcompletion'] === undefined) {
            alert('Enter Completion Duration');
            return false;
        }
        if (this.vendorData['f050fdescription'] === undefined) {
            alert('Enter Description');
            return false;
        }
        var confirmPop = confirm("Do you want to Add?");
        if (confirmPop == false) {
            return false;
        }
        for (var j = 0; j < this.vendorList.length; j++) {
            if (this.vendorData['f050fidVendor'] > 0) {
                if (this.vendorList[j]['f050fidVendor'] == this.vendorData['f050fidVendor']) {
                    alert("Duplicate vendor cannot be allowed");
                    return false;
                }
            }

        }

        this.vendorList.push(this.vendorData);
        this.slno = this.vendorList.length + 1;
        this.vendorData = {};
        this.showIcons();
    }

    ngOnInit() {
        this.ajaxCount = 0;
        this.spinner.show();
        this.viewDisabled = false;
        this.editDisabled = false;
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id < 1) {
            this.showLastRow1 = false;
            this.showLastRowMinus1 = false;
            this.showLastRowPlus1 = true;
        } else {
            this.showLastRow1 = true;

        }
        if (this.id < 1) {
            this.showLastRow = false;
            this.showLastRowMinus = false;
            this.showLastRowPlus = true;
        } else {
            this.showLastRow = true;

        }
        if (this.id < 1) {
            this.showLastRow2 = false;
            this.showLastRowMinus2 = false;
            this.showLastRowPlus2 = true;
        } else {
            this.showLastRow2 = true;

        }
        this.slno = 1;
        this.tenderData['f035fstatus'] = 1;
        this.tenderData['f035fidDepartment'] = '';
        this.tenderData['f035fidCategory'] = '';
        this.tenderData['f035ftype'] = '';
        if (this.id == 0) {
            this.ajaxCount++;
            this.TenderquatationService.getNewItems().subscribe(
                data => {
                    this.ajaxCount--;
                    this.tenderQuotationList = data['result'];
                }, error => {
                    console.log(error);
                });
        }
        this.ajaxCount++;
        this.SupplierregistrationService.getActiveItems().subscribe(
            data => {
                this.ajaxCount--;
                console.log(this.ajaxCount);

                this.supplierList = data['result']['data'];
            }
        );
        this.ajaxCount++;
        this.LicenseService.getActiveItems().subscribe(
            data => {
                this.ajaxCount--;
                this.licenseList = data['result'];
            }, error => {
                console.log(error);
            });

        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.ajaxCount++;
        this.StaffService.getItems().subscribe(
            data => {
                this.smartStaffList = [];
                this.ajaxCount--;
                this.smartStaffListNew = data['result']['data'];
                //Commented because of doubt

                // for (var j = 0; j < this.smartStaffListNew.length; j++) {
                //     var gradeValue = this.smartStaffListNew[j]['f034fgrade_id'].replace(/\D/g, '');
                //     if (parseInt(gradeValue) >= 41) {
                //         this.smartStaffList.push(this.smartStaffListNew[j]);
                //     }
                // }
                this.showIcons1();
                this.showIcons();
                this.showIcons2();
            }, error => {
                console.log(error);
            });
    }

    getVendorCode() {
        this.vendorData['f050fvendorCode'] = this.vendorData['f037fidVendorList']['f030fvendorCode'];
    }

    showTenderItems() {
        this.slno = 1;
        let tenderId = this.specData['f050fidTendor'];
        this.TenderquatationService.getItemsDetail(tenderId).subscribe(
            data => {
                this.specData = data['result'];
                this.specData['f050fstartDate'] = data['result']['tender-result'][0]['f035fstartDate'].replace(" ", "T");
                this.specData['f050fendDate'] = data['result']['tender-result'][0]['f035fendDate'].replace(" ", "T");
                this.specData['f050fopeningDate'] = data['result']['tender-result'][0]['f035fopeningDate'].replace(" ", "T");

                this.specData['f050ftitle'] = data['result']['tender-result'][0]['f035ftitle'];
                this.specData['f050fidTendor'] = tenderId;
            }, error => {
                console.log(error);
            });
    }

    getUOM() {
        let itemId = this.vendorData['f050fidVendor'];
        this.SupplierregistrationService.getItemsDetail(itemId).subscribe(
            data => {
                this.vendorData['f050fvendorCode'] = data['result']['f030fvendorCode'];
            }
        );
    }

    addListCommittee() {
        if (this.committListData['f051fidStaff'] == undefined) {
            alert("Select Staff Id");
            return false;
        }
        var confirmPop = confirm("Do you want to Add?");
        if (confirmPop == false) {
            return false;
        }
        for (var j = 0; j < this.committList.length; j++) {
            if (this.committList[j]['f051fidStaff'] == this.committListData['f051fidStaff']) {
                alert("Duplication of Staff not allowed");
                return false;
            }
        }
        this.committList.push(this.committListData);
        this.committListData = {};
        this.showIcons1();
    }

    ConvertToFloat(val) {
        return parseFloat(val);
    }

    saveData() {
        if (this.specData['f050fidTendor'] == undefined) {
            alert("Select Reference Number");
            return false;
        }
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        if (this.showLastRow1 == false) {
            var addactivities = this.addListCommittee();
            if (addactivities == false) {
                return false;
            }

        }
        if (this.showLastRow == false) {
            var addactivities = this.pushVendorList();
            if (addactivities == false) {
                return false;
            }

        }
        if (this.showLastRow2 == false) {
            var addactivities = this.addListGeneralSpecs();
            if (addactivities == false) {
                return false;
            }

        }
        var d = Date.parse(this.specData['f035ftenderOpeningDate']);
        var s = Date.parse(this.specData['f050fendDate']);
        if (s > d) {
            alert("Tender Opening Date should be greatere than End Date of Tender");
            return false;
        }
        var finalObject = {}
        finalObject['f050fidTendor'] = this.specData['f050fidTendor'];
        finalObject['commitee-details'] = this.committList;
        finalObject['vendor-details'] = this.vendorList;
        finalObject['remarks'] = this.remarkList;
        finalObject['f050ftenderOpeningDate'] = this.specData['f050ftenderOpeningDate'];
        for (var i = 0; i < finalObject['vendor-details'].length; i++) {
            finalObject['vendor-details'][i]['f050famount'] = this.ConvertToFloat(finalObject['vendor-details'][i]['f050famount']).toFixed(2);
        }
        console.log(finalObject);
        this.tenderData['f035ftenderStatus'] = 0;
        if (this.id > 0) {
            this.TenderquatationService.insertTenderRefistrationItems(finalObject).subscribe(
                data => {
                    alert("Tendor Submission has been Updated Successfully")
                    this.router.navigate(['procurement/tenderregistration']);
                    // this.vendorList = [];
                    // this.specData['f037ftenderNumber'] = '';

                }, error => {
                    console.log(error);
                });

        } else {
            this.TenderquatationService.insertTenderRefistrationItems(finalObject).subscribe(
                data => {
                    alert("Tendor has been Submitted Successfully")
                    this.router.navigate(['procurement/tenderregistration']);
                    // this.vendorList = [];
                    // this.specData['f037ftenderNumber'] = '';

                }, error => {
                    console.log(error);
                });
        }
    }

    addListGeneralSpecs() {
        if (this.remarkData['f053fremarks'] == undefined) {
            alert("Enter Committee Remarks");
            return false;
        }
        var confirmPop = confirm("Do you want to Add?");
        if (confirmPop == false) {
            return false;
        }
        var dataofCurrentRow = this.remarkData;
        this.remarkData = {};
        this.remarkList.push(dataofCurrentRow);
        this.showIcons2();
    }
    deleteItems(type, object) {
        var cnf = confirm("Do you really want to delete");
        if (cnf == true) {
            if (type == 'vendor') {
                var index = this.vendorList.indexOf(object);
                this.deleteList.push(this.vendorList[index]['f050fid']);
                let deleteObject = {};
                deleteObject['id'] = this.deleteList;
                this.TenderquatationService.deleteVendorItems(deleteObject).subscribe(
                    data => {
                    }, error => {
                        console.log(error);
                    });
                if (index > -1) {
                    this.vendorList.splice(index, 1);
                }
                this.showIcons();
            }
            else if (type == 'staff') {
                var index = this.committList.indexOf(object);
                this.deleteList1.push(this.committList[index]['f051fid']);
                let deleteObject = {};
                deleteObject['id'] = this.deleteList1;
                this.TenderquatationService.deleteCommiteeItems(deleteObject).subscribe(
                    data => {
                    }, error => {
                        console.log(error);
                    });
                if (index > -1) {
                    this.committList.splice(index, 1);
                }
                this.showIcons1();
            }
            else {
                var index = this.remarkList.indexOf(object);
                this.deleteList2.push(this.remarkList[index]['f053fid']);
                let deleteObject = {};
                deleteObject['id'] = this.deleteList2;
                this.TenderquatationService.deleteRemarksItems(deleteObject).subscribe(
                    data => {
                    }, error => {
                        console.log(error);
                    });
                if (index > -1) {
                    this.remarkList.splice(index, 1);
                }
                this.showIcons2();

            }
        }
    }
    onlyNumberKey(event) {
        //console.log(event);
        if (event.charCode == 46) {

        } else {
            return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
        }
    }
    maximize() {
        $("#myModal").addClass("modal");
        $("#overlay1").show();
        $(".maximize-icon").addClass("hidden");
        $(".minimize-icon").removeClass("hidden");
        $(".modal-backdrop").show();

    }
    minimize() {
        $("#myModal").removeClass("modal");
        $("#overlay1").hide();
        $(".maximize-icon").removeClass("hidden");
        $(".minimize-icon").addClass("hidden");
        $(".modal-backdrop").hide();
        $('#myModal').show();
    }
}