import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ItemsetupComponent } from './itemsetup/itemsetup.component';
import { ItemsetupFormComponent } from './itemsetup/itemsetupform/itemsetupform.component';

import { ItemcategoryComponent } from './itemcategory/itemcategory.component';
import { ItemcategoryFormComponent } from './itemcategory/itemcategoryform/itemcategoryform.component';

import { ItemsubcategoryComponent } from './itemsubcategory/itemsubcategory.component';
import { ItemsubcategoryFormComponent } from './itemsubcategory/itemsubcategoryform/itemsubcategoryform.component';

import { PurchaserequistionentryformComponent } from './purchaserequistionentry/purchaserequistionentryform/purchaserequistionentryform.component';
import { PurchaserequistionentryComponent } from './purchaserequistionentry/purchaserequistionentry.component';

import { PoformComponent } from './po/poform/poform.component';
import { PoComponent } from './po/po.component';

import { MofcategoryFormComponent } from "../procurement/mofcategory/mofcategoryform/mofcategoryform.component";
import { MofcategoryComponent } from "../procurement/mofcategory/mofcategory.component";

import { PoapprovalComponent } from './poapproval/poapproval.component';
import { PoapprovalformComponent } from './poapproval/poapprovalform/poapprovalform.component';

import { OrdertypeComponent } from '../procurement/ordertype/ordertype.component';
import { OrdertypeFormComponent } from '../procurement/ordertype/ordertypeform/ordertypeform.component';
import { TenderagreementComponent } from './tenderagreement/tenderagreement.component';

import { GrnformComponent } from './grn/grnform/grnform.component';
import { GrnComponent } from './grn/grn.component';

import { GrnapprovalComponent } from './grnapproval/grnapproval.component';
import { GrnapprovalformComponent } from './grnapproval/grnapprovalform/grnapprovalform.component';


import { PurchaselimitComponent } from './purchaselimit/purchaselimit.component';
import { PurchaselimitFormComponent } from './purchaselimit/purchaselimitform/purchaselimitform.component';

import { LicenseComponent } from '../procurement/license/license.component';
import { LicenseFormComponent } from '../procurement/license/licenseform/licenseform.component';

import { SupplierregistrationComponent } from './supplierregistration/supplierregistration.component';
import { SupplierregistrationFormComponent } from './supplierregistration/supplierregistrationform/supplierregistrationform.component';

import { SupplierapprovalComponent } from './supplierapproval/supplierapproval.component';
import { SupplierapprovalFormComponent } from './supplierapproval/supplierapprovalform/supplierapprovalform.component';

import { PrapprovalComponent } from './prapproval/prapproval.component';
import { PrapprovalformComponent } from './prapproval/prapprovalform/prapprovalform.component';

import { PurchaseOrderRePrintComponent } from "./poreprint/poreprint.component";

import { TenderQuatationFormComponent } from './tenderquotation/tenderquotationform/tenderquotationform.component';
import { TenderquotationComponent } from './tenderquotation/tenderquotation.component';

import { TendershortlistformComponent } from './tendershortlist/tendershortlistform/tendershortlistform.component';
import { TendershortlistComponent } from './tendershortlist/tendershortlist.component';

import { TenderawardformComponent } from './tenderaward/tenderawardform/tenderawardform.component';
import { TenderawardComponent } from './tenderaward/tenderaward.component';

import { WarrantyPrintComponent } from "./warrantyprint/warrantyprint.component";
import { WarrantyRePrintComponent } from "./warrantyreprint/warrantyreprint.component";

import { LeveloneComponent } from './businesscode/levelone/levelone.component';
import { LeveltwoComponent } from './businesscode/leveltwo/leveltwo.component';
import { LevelthreeComponent } from './businesscode/levelthree/levelthree.component';

import { TenderregistrationformComponent } from './tenderregistration/tenderregistrationform/tenderregistrationform.component';
import { TenderregistrationComponent } from './tenderregistration/tenderregistration.component';

import { PurchaseOrderPrintComponent } from "./purchaseorderprint/purchaseorderprint.component";

import { SpecCodeComponent } from './tenderquotation/speccode/speccode.component';
import { DescriptionComponent } from './tenderquotation/description/description.component';

import { PowarrantyComponent } from "./powarranty/powarranty.component";
import { PowarrantyformComponent } from "./powarranty/powarrantyform/powarrantyform.component";

import { BlockvendorComponent } from './blockvendor/blockvendor.component';
import { BlockvendorFormComponent } from './blockvendor/blockvendorform/blockvendorform.component';

import { AssetApproverVerificationComponent } from "./assetapproververification/assetapproververification.component";

import { PowarrantyapprovalComponent } from "./powarrantyapproval/powarrantyapproval.component";

import { ContractComponent } from "./contract/contract.component";
import { ContractFormComponent } from "./contract/contractform/contractform.component";

import { WarrantFormComponent } from "./warrant/warrantform/warrantform.component";
import { WarrantComponent } from "./warrant/warrant.component";

import { WarrantapprovalComponent } from "./warrantapproval/warrantapproval.component";

const routes: Routes = [

  { path: 'tenderregistration', component: TenderregistrationComponent },
  { path: 'addnewtenderregistration', component: TenderregistrationformComponent },
  { path: 'edittenderregistration/:id', component: TenderregistrationformComponent },

  { path: 'itemsetup', component: ItemsetupComponent },
  { path: 'addnewitemsetup', component: ItemsetupFormComponent },
  { path: 'edititemsetup/:id', component: ItemsetupFormComponent },

  { path: 'mofcategory', component: MofcategoryComponent },
  { path: 'addnewmofcategory', component: MofcategoryFormComponent },
  { path: 'editmofcategory/:id', component: MofcategoryFormComponent},

  { path: 'license', component: LicenseComponent },
  { path: 'addnewlicense', component: LicenseFormComponent },
  { path: 'editlicense/:id', component: LicenseFormComponent },

  { path: 'itemcategory', component: ItemcategoryComponent },
  { path: 'addnewitemcategory', component: ItemcategoryFormComponent },
  { path: 'edititemcategory/:id', component: ItemcategoryFormComponent },

  { path: 'itemsubcategory', component: ItemsubcategoryComponent },
  { path: 'addnewitemsubcategory', component: ItemsubcategoryFormComponent },
  { path: 'edititemsubcategory/:id', component: ItemsubcategoryFormComponent },

  { path: 'ordertype', component: OrdertypeComponent },
  { path: 'addnewordertype', component: OrdertypeFormComponent },
  { path: 'editordertype/:id', component: OrdertypeFormComponent },

  { path: 'supplierregistration', component: SupplierregistrationComponent },
  { path: 'addnewsupplierregistration', component: SupplierregistrationFormComponent },
  { path: 'editsupplierregistration/:id', component: SupplierregistrationFormComponent },
  { path: 'viewsupplierregistration/:id/:idview', component: SupplierregistrationFormComponent },

  { path: 'supplierapproval', component: SupplierapprovalComponent },
  { path: 'editsupplierapproval/:id', component: SupplierapprovalFormComponent },

  { path: 'purchaselimit', component: PurchaselimitComponent },
  { path: 'addnewpurchaselimit', component: PurchaselimitFormComponent },
  { path: 'editpurchaselimit/:id', component: PurchaselimitFormComponent },
 
  { path: 'purchaserequisitionentry', component: PurchaserequistionentryComponent },
  { path: 'addnewpurchaserequistionentry', component: PurchaserequistionentryformComponent },
  { path: 'editpurchaserequistionentry/:id', component: PurchaserequistionentryformComponent },


  { path: 'nonpurchaserequisitionentry', component: PurchaserequistionentryComponent },
  { path: 'nonaddnewpurchaserequistionentry', component: PurchaserequistionentryformComponent },
  { path: 'noneditpurchaserequistionentry/:id', component: PurchaserequistionentryformComponent },

  { path: 'prapproval', component: PrapprovalComponent },
  { path: 'editprapproval/:id', component: PrapprovalformComponent },
  { path: 'vieweditpr/:id/:idview', component: PurchaserequistionentryformComponent},

  { path: 'poapproval', component: PoapprovalComponent },
  { path: 'editpoapproval/:id', component: PoapprovalformComponent },

  { path: 'tenderquotation', component: TenderquotationComponent },
  { path: 'addnewtenderquotation', component: TenderQuatationFormComponent },
  { path: 'edittenderquotation/:id', component: TenderQuatationFormComponent },
  { path: 'viewtenderquotation/:id/:idview', component:TenderQuatationFormComponent},

  { path: 'po', component: PoComponent },
  { path: 'addnewpo', component: PoformComponent },
  { path: 'editpo/:id', component: PoformComponent},
  { path: 'vieweditpo/:id/:idview', component: PoformComponent},

  { path: 'powarranty', component: PowarrantyComponent },
  { path: 'addnewpowarranty', component: PowarrantyformComponent },
  { path: 'editpowarranty/:id', component: PowarrantyformComponent},
  { path: 'vieweditpoWarrant/:id/:idview', component: PowarrantyformComponent},

  { path: 'powarrantyapproval', component: PowarrantyapprovalComponent },
  { path: 'editpowarrantyapproval/:id', component: PowarrantyapprovalComponent },

  { path: 'grn', component: GrnComponent },
  { path: 'addnewgrn', component: GrnformComponent },
  { path: 'editgrn/:id', component: GrnformComponent },
  { path: 'vieweditgrn/:id/:idview', component: GrnformComponent },

  { path: 'grnapproval', component: GrnapprovalComponent },
  { path: 'editgrnapproval/:id', component: GrnapprovalformComponent },

  { path: 'poprint', component: PurchaseOrderPrintComponent },
  { path: 'poreprint', component: PurchaseOrderRePrintComponent },

  { path: 'warrantyprint', component: WarrantyPrintComponent },

  { path: 'warrantyreprint', component: WarrantyRePrintComponent },
  { path: 'speccode', component: SpecCodeComponent },
  { path: 'description', component: DescriptionComponent },

  { path: 'tenderagreement', component:TenderagreementComponent},

  { path: 'tendershortlist', component:TendershortlistComponent},
  { path: 'addtendershortlist', component:TendershortlistformComponent},
  { path: 'edittendershortlist/:id', component:TendershortlistformComponent},

  { path: 'tenderaward', component:TenderawardComponent},
  { path: 'addtenderaward', component:TenderawardformComponent},
  { path: 'edittenderaward/:id', component:TenderawardformComponent},

  { path: 'blockvendor', component:BlockvendorComponent},
  { path: 'addnewblockvendor', component:BlockvendorFormComponent},
  { path: 'editblockvendor/:id', component:BlockvendorFormComponent},


  { path: 'levelone', component: LeveloneComponent },
  { path: 'leveltwo', component: LeveltwoComponent },
  { path: 'levelthree', component: LevelthreeComponent },

  {path: 'assetapprover', component: AssetApproverVerificationComponent},

  { path: 'contract', component: ContractComponent },
  { path: 'addcontract', component: ContractFormComponent },
  { path: 'editcontract/:id', component: ContractFormComponent },

  { path: 'warrant', component: WarrantComponent },
  { path: 'addwarrant', component: WarrantFormComponent },
  { path: 'editwarrant/:id', component: WarrantFormComponent },

  { path: 'warrantapproval', component: WarrantapprovalComponent },
  { path: 'viewwarrant/:id/:idview', component: WarrantFormComponent },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  
})
export class ProcurementRoutingModule { 
  
}