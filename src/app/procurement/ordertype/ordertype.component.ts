import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { OrdertypeService } from '../service/ordertype.service'

@Component({
    selector: 'OrdertypeComponent',
    templateUrl: 'ordertype.component.html'
})
export class OrdertypeComponent implements OnInit {
    orderTypeList = [];
    orderTypeData = {};
    id: number;
    constructor(
        private OrdertypeService: OrdertypeService
    ) { }

    ngOnInit() {

        this.OrdertypeService.getItems().subscribe(
            data => {
                this.orderTypeList = data['result']['data'];
            }, error => {
                console.log(error);
            });
    }
    
}