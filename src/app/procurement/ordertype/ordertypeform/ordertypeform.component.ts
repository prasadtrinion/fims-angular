import { Injector } from '@angular/core';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { OrdertypeService } from '../../service/ordertype.service'
import { AccountcodeService } from '../../../generalsetup/service/accountcode.service';
import { AlertService } from '../../../_services/alert.service';

@Component({
    selector: 'OrdertypeFormComponent',
    templateUrl: 'ordertypeform.component.html'
})

export class OrdertypeFormComponent implements OnInit {

    orderTypeList = [];
    orderTypeData = {};

    accountcodeList = [];

    id: number;
    constructor(

        private OrdertypeService: OrdertypeService,
        private AccountcodeService: AccountcodeService,
        private route: ActivatedRoute,
        private AlertService: AlertService,
        private router: Router,

    ) { }

    ngOnInit() {
        this.orderTypeData['f081fstatus'] = 1;

        //account code
        this.AccountcodeService.getItemsByLevel().subscribe(
            data => {
                this.accountcodeList = data['result']['data'];
            }, error => {
                console.log(error);
            });

        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        if (this.id > 0) {
            this.OrdertypeService.getItemsDetail(this.id).subscribe(
                data => {
                    console.log(data);
                    this.orderTypeData = data['result'][0];
                    this.orderTypeData['f081fstatus'] = parseInt(this.orderTypeData['f081fstatus']);

                    console.log(this.orderTypeData);
                }, error => {
                    console.log(error);
                });
        }

    }
    addItemsetup() {
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.OrdertypeService.updateOrderTypeItems(this.orderTypeData, this.id).subscribe(
                data => {
                    this.router.navigate(['procurement/ordertype']);
                    this.AlertService.success("Updated Sucessfully !!")
                }, error => {
                    console.log(error);
                });

        } else {
            this.OrdertypeService.insertOrderTypeItems(this.orderTypeData).subscribe(
                data => {

                    if (data['status'] == 409) {
                        alert("Order Type Already Exist !!");
                    }
                    else {
                        this.router.navigate(['procurement/ordertype']);
                        this.AlertService.success("Added Sucessfully !!");
                    }
                }, error => {
                    console.log(error);
                });
        }
    }

}