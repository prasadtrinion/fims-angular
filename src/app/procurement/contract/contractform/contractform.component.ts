import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { AlertService } from '../../../_services/alert.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators, PatternValidator } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { ContractService } from "../../service/contract.service";
import { valid } from 'semver';
import { INVALID } from '@angular/forms/src/model';

@Component({
    selector: 'ContractFormComponent',
    templateUrl: 'contractform.component.html'
})

export class ContractFormComponent implements OnInit {
    contractHeaderData = {};
    contractList = [];
    contractData = {};
    deleteList = [];
    paymentList = [];
    id: number;
    showLastRow: boolean;
    showLastRowMinus: boolean;
    showLastRowPlus: boolean;
    listshowLastRowMinus: boolean;
    listshowLastRowPlus: boolean;
    ajaxCount: number;

    constructor(
        private AlertService: AlertService,
        private spinner: NgxSpinnerService,
        private ContractService: ContractService,
        private route: ActivatedRoute,
        private router: Router

    ) { }
    ngDoCheck() {
        if (this.ajaxCount == 0 && this.contractList.length > 0) {
            this.ajaxCount = 20;
            console.log("asdfsdf");
            this.showIcons();
        }
        const change = this.ajaxCount;
        console.log(this.ajaxCount);
        if (this.ajaxCount == 0) {
            this.editFunction();
            this.spinner.hide();
            this.ajaxCount = 10;
        }
    }
    showIcons() {
        if (this.contractList.length > 0) {
            this.showLastRowMinus = true;
            this.listshowLastRowMinus = true;
            this.showLastRowPlus = true;
        }

        if (this.contractList.length > 1) {
            if (this.showLastRow == false) {
                this.listshowLastRowPlus = false;
                this.listshowLastRowMinus = true;
                this.showLastRowPlus = true;

            } else {
                this.listshowLastRowPlus = true;
                this.listshowLastRowMinus = false;

            }
        }
        if (this.contractList.length == 1 && this.showLastRow == false) {
            this.listshowLastRowMinus = true;
            this.listshowLastRowPlus = false;

        }
        if (this.contractList.length == 1 && this.showLastRow == true) {
            this.listshowLastRowMinus = false;
            this.listshowLastRowPlus = true;

        }
        if (this.contractList.length == 0 && this.showLastRow == false) {
            this.showLastRowMinus = false;
            this.showLastRowPlus = true;
        }
        if (this.contractList.length == 0 && this.showLastRow == true) {
            this.showLastRowMinus = true;
            this.showLastRowPlus = false;
        }
    }
    deletemodule() {
        this.showLastRow = true;
        this.showIcons();
    }
    showNewRow() {
        var confirmPop = confirm("Do you want to Add?");
        if (confirmPop == false) {
            return false;
        }
        this.showLastRow = false;
        this.showIcons();
    }

    editFunction() {
        this.ajaxCount++;
        if (this.id > 0) {
            this.ContractService.getItemsDetail(this.id).subscribe(
                data => {
                    this.ajaxCount--;
                    this.contractHeaderData['f026fname'] = data['result'][0]['f026fname'];
                    this.contractList = data['result'];
                    this.showIcons();

                }, error => {
                    console.log(error);
                });
        }
    }
    ngOnInit() {
        this.ajaxCount = 0;
        this.spinner.show();
        this.contractHeaderData['f088fdate'] = new Date().toISOString().substr(0, 10);

        //Payment Award Dropdown
        this.ContractService.getPaymentAwardItems().subscribe(
            data => {
                this.paymentList = data['result'];
            }, error => {
                console.log(error);
            });

        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id < 1) {
            this.showLastRow = false;
            this.showLastRowMinus = false;
            this.showLastRowPlus = true;
        } else {
            this.showLastRow = true;

        }
    }

    saveLegalinformation() {


        if (this.contractHeaderData['f026fname'] == undefined) {
            alert("Select Payment Amount");
            return false;
        }
        if (this.showLastRow == false) {
            var addactivities = this.addlegalinformationlist();
            if (addactivities = true) {
                return false;
            }
        }
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        let invoiceObject = {};
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        console.log(this.id);
        if (this.id > 0) {
            invoiceObject['f098fid'] = this.id;
        }
        invoiceObject['f026fname'] = this.contractHeaderData['f026fname'];
        invoiceObject['f026fstatus'] = 0;
        invoiceObject['legal-details'] = this.contractList;
        // if(this.legalinformationList.length>0) {

        // } else {
        //     alert("Please add atleast one student");
        //     return false;
        // }
        console.log(invoiceObject);

        if (this.id > 0) {
            this.ContractService.updateContractItems(invoiceObject, this.id).subscribe(
                data => {
                    this.router.navigate(['procurement/contract']);
                    alert("Contract has been Updated Sucessfully !");


                }, error => {
                    alert("Legal Associate Already exists")
                    console.log(error);
                });
        } else {
            this.ContractService.insertContractItems(invoiceObject).subscribe(
                data => {

                    this.router.navigate(['procurement/contract']);
                    alert("Contract has been Added Sucessfully !");

                }, error => {
                    console.log(error);
                });
        }
    }
    deletelegalinformation(object) {
        var cnf = confirm("Do you really want to delete");
        if (cnf == true) {
            console.log(this.contractList);
            var index = this.contractList.indexOf(object);
            this.deleteList.push(this.contractList[index]['f026fidDetails']);
            let deleteObject = {};
            deleteObject['id'] = this.deleteList;
            // this.ContractService.getdeleteItems(deleteObject).subscribe(
            //     data => {
            //     }, error => {
            //         console.log(error);
            //     })
            if (index > -1) {
                this.contractList.splice(index, 1);
            }

            this.showIcons();
        }
    }


    addlegalinformationlist() {
        if (this.contractData['f026fidStudent'] == undefined) {
            alert(" Select Calendar");
            return false;
        }
        if (this.contractData['f026fdescription'] == undefined) {
            alert(" Enter the Amount");
            return false;
        }
        var confirmPop = confirm("Do you want to Add?");
        if (confirmPop == false) {
            return false;
        }
        console.log(this.contractData);
        var dataofCurrentRow = this.contractData;
        this.contractData = {};
        this.contractList.push(dataofCurrentRow);
        this.showIcons();
        // console.log(this.legalinformationList);

    }
    onlyNumberKey(event) {
        console.log(event);
        return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
    }
    maximize() {
        $("#myModal").addClass("modal");
        $("#overlay1").show();
        $(".maximize-icon").addClass("hidden");
        $(".minimize-icon").removeClass("hidden");
        $(".modal-backdrop").show();

    }
    minimize() {
        $("#myModal").removeClass("modal");
        $("#overlay1").hide();
        $(".maximize-icon").removeClass("hidden");
        $(".minimize-icon").addClass("hidden");
        $(".modal-backdrop").hide();
        $('#myModal').show();

    }

}


