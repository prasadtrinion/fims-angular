import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ContractService } from '../service/contract.service'
import { NgxSpinnerService } from 'ngx-spinner';
@Component({
    selector: 'Contract',
    templateUrl: 'contract.component.html'
})

export class ContractComponent implements OnInit {
    legalinformationList =  [];
    legalinformationData = {};
    id: number;

    constructor(
        private ContractService: ContractService,
        private spinner: NgxSpinnerService,
    ) { }

    ngOnInit() { 
        this.spinner.show();                                                                        
        this.ContractService.getItems().subscribe(
            data => {
                this.spinner.hide();
                this.legalinformationList = data['data'];
        }, error => {
            console.log(error);
        });
    }
}