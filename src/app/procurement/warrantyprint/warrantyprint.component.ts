import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { WarrantyPrintService } from '../service/warantyprint.service';
import { environment } from '../../../environments/environment';

@Component({
    selector: 'WarrantyPrint',
    templateUrl: 'warrantyprint.component.html'
})

export class WarrantyPrintComponent implements OnInit {

    poapprovalList = [];
    poapprovalData = {};
    approvalData = {};
    selectAllCheckbox = false;
    downloadUrl: string = environment.api.downloadbase;
    id: number;
    constructor(

        private PoReportapprovalService: WarrantyPrintService,
        private route: ActivatedRoute,
        private router: Router,
    ) { }

    ngOnInit() {
        this.getListData();
    }
    getListData() {
        this.PoReportapprovalService.getApprovedItems().subscribe(
            data => {
                this.poapprovalList = data['result'];

            }, error => {
                console.log(error);
            });
    }
    Print(id) {
        let newobj = {};
        newobj['id'] = id;
        newobj['type'] = 1;
        // newobj['toDate'] = id;
        var confirmPop = confirm("Do you want to Print?");
        if (confirmPop == false) {
            return false;
        }
        this.route.paramMap.subscribe((data) => this.id);
        this.PoReportapprovalService.insertPoPrintItems(newobj).subscribe(
            data => {
                // alert('KWAP has been saved');
                window.open(this.downloadUrl + data['name']);
                this.router.navigate(['procurement/warrantyprint']);

            }, error => {
                console.log(error);
            });
    }
}
