import { Injector } from '@angular/core';
import { Component, OnInit, ViewChild } from '@angular/core';

import { ActivatedRoute, Router } from '@angular/router';
import { ItemsetupService } from '../../service/itemsetup.service'
import { ItemcategoryService } from '../../service/itemcategory.service'
import { TaxsetupcodeService } from '../../../generalledger/service/taxsetupcode.service'
import { ItemsubcategoryService } from '../../service/itemsubcategory.service';
import { AccountcodeService } from '../../../generalsetup/service/accountcode.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'ItemsetupFormComponent',
    templateUrl: 'itemsetupform.component.html'
})

export class ItemsetupFormComponent implements OnInit {

    itemsetupList = [];
    itemsetupData = {};
    itemcategoryList = [];
    taxsetupcodeList = [];
    purchaseList = [];
    itemsubcategoryList = [];
    itemaccountList = [];
    itemaccountData = {};
    accountcodeList = [];
    ajaxCount: number;

    id: number;
    constructor(

        private ItemsetupService: ItemsetupService,
        private ItemcategoryService: ItemcategoryService,
        private TaxsetupcodeService: TaxsetupcodeService,
        private ItemsubcategoryService: ItemsubcategoryService,
        private route: ActivatedRoute,
        private router: Router,
        private AccountcodeService: AccountcodeService,
        private spinner: NgxSpinnerService,

    ) { }
    ngDoCheck() {
        const change = this.ajaxCount;
        if (this.ajaxCount == 0) {
            this.spinner.hide();
        }
    }
    ngOnInit() {
        this.ajaxCount = 0;
        this.spinner.show();
        this.itemsetupData['f029fstatus'] = 1;
        //item category
        this.ajaxCount++;
        this.ItemcategoryService.getActiveItems().subscribe(
            data => {
                this.ajaxCount--;
                this.itemcategoryList = data['result']['data'];
            }, error => {
                console.log(error);
            });
        // Tax setup code
        this.ajaxCount++;
        this.TaxsetupcodeService.getActiveItems().subscribe(
            data => {
                this.ajaxCount--;
                this.taxsetupcodeList = data['result']['data'];
            }, error => {
                console.log(error);
            });

        // account dropdown
        this.ajaxCount++;
        this.AccountcodeService.getItemsByLevel().subscribe(
            data => {
                this.ajaxCount--;
                this.accountcodeList = data['result']['data'];
            }, error => {
                //console.log(error);
            });
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        if (this.id > 0) {
            this.ajaxCount++;
            this.ItemsetupService.getItemsDetail(this.id).subscribe(
                data => {
                    this.ajaxCount--;
                    this.itemsetupData = data['result'][0];
                    this.getSubCategories();
                    this.itemsetupData['f029fstatus'] = parseInt(this.itemsetupData['f029fstatus']);

                    console.log(this.itemsetupData);
                }, error => {
                    console.log(error);
                });
        }

    }
    getSubCategories() {
        //  Sub cAt
        this.ajaxCount++;
        this.ItemsubcategoryService.getSubCategoriesByCategory(this.itemsetupData['f029fidCategory']).subscribe(
            data => {
                this.ajaxCount--;
                console.log("asdfasdfsub cat");
                this.itemsubcategoryList = data['result'];
            }, error => {
                console.log(error);
            });

    }
    addItemsetup() {
        console.log(this.itemsetupData);
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.ItemsetupService.updateItemsetupItems(this.itemsetupData, this.id).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert('Item Already Exist');
                        return false;
                    }
                    else {
                        this.router.navigate(['procurement/itemsetup']);
                        alert(" Item has been Updated Sucessfully !!");
                    }
                }, error => {
                    console.log(error);
                    alert('Item  Already Exist');
                });

        } else {
            this.ItemsetupService.insertItemsetupItems(this.itemsetupData).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert('Item  Already Exist');
                        return false;
                    }
                    this.router.navigate(['procurement/itemsetup']);
                    alert("Item has been Added Sucessfully !!");
                }, error => {
                    console.log(error);
                });
        }
    }

}