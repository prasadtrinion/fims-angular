import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ItemsetupService } from '../service/itemsetup.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'Itemsetup',
    templateUrl: 'itemsetup.component.html'
})
export class ItemsetupComponent implements OnInit {
    itemsetupList = [];
    itemsetupData = {};
    id: number;
    constructor(
        private ItemsetupService: ItemsetupService,
        private spinner: NgxSpinnerService
    ) { }

    ngOnInit() {
        this.spinner.show();
        this.ItemsetupService.getItems().subscribe(
            data => {
              this.spinner.hide();
                this.itemsetupList = data['result']['data'];
            }, error => {
                console.log(error);
            });
    }
}