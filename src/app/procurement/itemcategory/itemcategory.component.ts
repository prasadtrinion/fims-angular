import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ItemcategoryService } from '../service/itemcategory.service'
import { AlertService } from '../../_services/alert.service';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'Itemcategory',
    templateUrl: 'itemcategory.component.html'
})
export class ItemcategoryComponent implements OnInit {
    itemcategoryList = [];
    itemcategoryData = {};
    id: number;
    constructor(
        private ItemcategoryService: ItemcategoryService,
        private AlertService: AlertService,
        private spinner: NgxSpinnerService,
        private router: Router

    ) { }

    ngOnInit() {
        this.itemcategoryData['f027fstatus'] = 1;
        this.getList();
    }

    getList() {
        this.spinner.show();
        this.ItemcategoryService.getItems().subscribe(
            data => {
                this.spinner.hide();
                this.itemcategoryList = data['result']['data'];


            }, error => {
                console.log(error);
            });
    }
    addItemCat() {
        if (this.itemcategoryData['f027fid'] > 0) {
            this.ItemcategoryService.updateItemcategoryItems(this.itemcategoryData, this.itemcategoryData['f027fid']).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert('Item Category Already Exist');
                    }
                    else {
                        this.AlertService.success("Updated Sucessfully !!")
                        this.getList();
                        this.itemcategoryData = {};
                    }

                }, error => {
                    console.log(error);
                });
        } else {
            this.ItemcategoryService.insertItemcategoryItems(this.itemcategoryData).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert("No duplication of Item Category Name");
                    }
                    else {
                        this.router.navigate(['procurement/itemcategory']);
                        this.AlertService.success("Added Sucessfully !!")
                        this.getList();
                        this.itemcategoryData = {};
                    }

                }, error => {
                    console.log(error);
                });
        }
    }
    prefill(id) {
        console.log(id);
        this.ItemcategoryService.getItemsDetail(id).subscribe(
            data => {
                console.log(data);
                this.itemcategoryData = data['result'][0];
                this.itemcategoryData['f027fstatus'] = parseInt(this.itemcategoryData['f027fstatus']);
                console.log(this.itemcategoryData);
            }, error => {
                console.log(error);
            });
    }
}