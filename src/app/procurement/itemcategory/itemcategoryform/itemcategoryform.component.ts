import { Injector } from '@angular/core';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ItemcategoryService } from '../../service/itemcategory.service'
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'ItemcategoryFormComponent',
    templateUrl: 'itemcategoryform.component.html'
})

export class ItemcategoryFormComponent implements OnInit {

    itemcategoryList = [];
    itemcategoryData = {};
    ajaxCount: number;

    id: number;
    constructor(

        private ItemcategoryService: ItemcategoryService,
        private route: ActivatedRoute,
        private spinner: NgxSpinnerService,
        private router: Router

    ) { }

    ngDoCheck() {
        const change = this.ajaxCount;
        if (this.ajaxCount == 0) {
            this.spinner.hide();
        }
    }
    ngOnInit() {
        this.itemcategoryData['f027fstatus'] = 1;
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.ajaxCount=0;
        this.spinner.show();
      
        if (this.id > 0) {
            this.ajaxCount++;
            this.ItemcategoryService.getItemsDetail(this.id).subscribe(
                data => {
                    this.ajaxCount--;
                    this.itemcategoryData = data['result'][0];
                    this.itemcategoryData['f027fstatus'] = parseInt(this.itemcategoryData['f027fstatus']);
                    this.itemcategoryData['f027fmaintenance'] = parseInt(this.itemcategoryData['f027fmaintenance']);


                }, error => {
                    console.log(error);
                });
        }
    }
    addItemcategory() {
        console.log(this.itemcategoryData);
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
    
        if (this.id > 0) {
            this.ItemcategoryService.updateItemcategoryItems(this.itemcategoryData, this.id).subscribe(
                data => {
                  
                    if (data['status'] == 409) {
                        alert('Procurement Category Code Already Exist');
                        return false;
                    }
                    else {
                        this.router.navigate(['procurement/itemcategory']);
                        alert("Procurement Category has been Updated Sucessfully !!");
                    }
                }, error => {
                    alert('Procurement Category Code Already Exist');
                });
        } else {
            this.ItemcategoryService.insertItemcategoryItems(this.itemcategoryData).subscribe(
                data => {

                    if (data['status'] == 409) {
                        alert('Procurement Category Code Already Exist');
                        return false;
                    }
                    this.router.navigate(['procurement/itemcategory']);
                    alert("Procurement Category has been Added Sucessfully !!");

                }, error => {
                    console.log(error);
                });
        }
    }

}