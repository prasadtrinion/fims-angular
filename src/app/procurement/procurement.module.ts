import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../material/material.module';
import { Ng4LoadingSpinnerModule } from 'ng4-loading-spinner';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { DataTableModule } from "angular-6-datatable";
import { NguiAutoCompleteModule } from '@ngui/auto-complete';

import { HttpClientModule, HttpClient, HTTP_INTERCEPTORS } from '@angular/common/http';

import { ItemsetupComponent } from './itemsetup/itemsetup.component';
import { ItemsetupFormComponent } from './itemsetup/itemsetupform/itemsetupform.component';
import { ItemsetupService } from './service/itemsetup.service';

import { ItemcategoryComponent } from './itemcategory/itemcategory.component';
import { ItemcategoryFormComponent } from './itemcategory/itemcategoryform/itemcategoryform.component';
import { ItemcategoryService } from './service/itemcategory.service';

import { ItemsubcategoryComponent } from './itemsubcategory/itemsubcategory.component';
import { ItemsubcategoryFormComponent } from './itemsubcategory/itemsubcategoryform/itemsubcategoryform.component';
import { ItemsubcategoryService } from './service/itemsubcategory.service';
import { BudgetactivitiesService } from '../budget/service/budgetactivities.service';

import { LicenseComponent } from '../procurement/license/license.component';
import { LicenseFormComponent } from '../procurement/license/licenseform/licenseform.component';
import { LicenseService } from '../procurement/service/license.service';


import { PrapprovalComponent } from './prapproval/prapproval.component';
import { PrapprovalformComponent } from './prapproval/prapprovalform/prapprovalform.component';
import { PrapprovalService } from "./service/prapproval.service";

import { PoapprovalComponent } from './poapproval/poapproval.component';
import { PoapprovalformComponent } from './poapproval/poapprovalform/poapprovalform.component';
import { PoapprovalService } from './service/poapproval.service';


import { PurchaselimitComponent } from './purchaselimit/purchaselimit.component';
import { PurchaselimitFormComponent } from './purchaselimit/purchaselimitform/purchaselimitform.component';
import { PurchaselimitService } from './service/purchaselimit.service';

import { SupplierregistrationComponent } from './supplierregistration/supplierregistration.component';
import { SupplierregistrationFormComponent } from './supplierregistration/supplierregistrationform/supplierregistrationform.component';
import { SupplierregistrationService } from './service/supplierregistration.service';

import { SupplierapprovalComponent } from './supplierapproval/supplierapproval.component';
import { SupplierapprovalFormComponent } from './supplierapproval/supplierapprovalform/supplierapprovalform.component';
import { SupplierapprovalService } from './service/supplierapproval.service';

import { PurchaserequistionentryformComponent } from './purchaserequistionentry/purchaserequistionentryform/purchaserequistionentryform.component';
import { PurchaserequistionentryComponent } from './purchaserequistionentry/purchaserequistionentry.component';
import { PurchaserequistionentryService } from './service/purchaserequistionentry.service';

import { OrdertypeService } from '../procurement/service/ordertype.service';

import { PoformComponent } from './po/poform/poform.component';
import { PoComponent } from './po/po.component';

import { PowarrantyComponent } from "./powarranty/powarranty.component";
import { PowarrantyformComponent } from "./powarranty/powarrantyform/powarrantyform.component";
import { PurchaseorderWarrantyService } from "./service/powarranty.service";

import { PowarrantyapprovalComponent } from "./powarrantyapproval/powarrantyapproval.component";
import { PowarrantyapprovalService } from "./service/powarrantyapproval.service";

import { PurchaseOrderPrintComponent } from "./purchaseorderprint/purchaseorderprint.component";
import { PoReportapprovalService } from "./service/purchaseorderprint.service";

import { PurchaseOrderRePrintComponent } from "./poreprint/poreprint.component";
import { PoReprintService } from "./service/poreprint.service";

import { WarrantyPrintComponent } from "./warrantyprint/warrantyprint.component";
import { WarrantyPrintService } from "./service/warantyprint.service";

import { WarrantyRePrintComponent } from "./warrantyreprint/warrantyreprint.component";
import { WarrantyRePrintService } from "./service/warrantyreprint.service";

import { OrdertypeComponent } from '../procurement/ordertype/ordertype.component';
import { OrdertypeFormComponent } from '../procurement/ordertype/ordertypeform/ordertypeform.component';

import { GrnformComponent } from './grn/grnform/grnform.component';
import { GrnComponent } from './grn/grn.component';
import { GrnService } from './service/grn.service';

import { MofcategoryFormComponent } from "../procurement/mofcategory/mofcategoryform/mofcategoryform.component";
import { MofcategoryComponent } from "../procurement/mofcategory/mofcategory.component";
import { MofcategoryService } from "./service/mofcategory.service";

import { GrnapprovalComponent } from './grnapproval/grnapproval.component';
import { GrnapprovalformComponent } from './grnapproval/grnapprovalform/grnapprovalform.component';
import { GrnapprovalService } from './service/grnapproval.service';

import { BlockvendorComponent } from './blockvendor/blockvendor.component';
import { BlockvendorFormComponent } from './blockvendor/blockvendorform/blockvendorform.component';
import { BlockvendorService } from './service/blockvendor.service';


import { TenderQuatationFormComponent } from './tenderquotation/tenderquotationform/tenderquotationform.component';
import { TenderquotationComponent } from './tenderquotation/tenderquotation.component';

import { TenderregistrationformComponent } from './tenderregistration/tenderregistrationform/tenderregistrationform.component';
import { TenderregistrationComponent } from './tenderregistration/tenderregistration.component';

import { TenderagreementComponent } from './tenderagreement/tenderagreement.component';

import { TendershortlistformComponent } from './tendershortlist/tendershortlistform/tendershortlistform.component';
import { TendershortlistComponent } from './tendershortlist/tendershortlist.component';
import { TenderawardformComponent } from './tenderaward/tenderawardform/tenderawardform.component';
import { TenderawardComponent } from './tenderaward/tenderaward.component';

import { SpecCodeComponent } from './tenderquotation/speccode/speccode.component';
import { DescriptionComponent } from './tenderquotation/description/description.component';

import { TenderquatationService } from './service/tenderquotation.service';
import { SpecCodeService } from './service/speccode.service';
import { DescriptionService } from './service/description.service';
import { PurchaseorderService } from './service/purchaseorder.service';

import { LeveloneComponent } from './businesscode/levelone/levelone.component';
import { LeveltwoComponent } from './businesscode/leveltwo/leveltwo.component';
import { LevelthreeComponent } from './businesscode/levelthree/levelthree.component';
import { BusinesscodeService } from './service/businesscode.service';

import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { ProcurementRoutingModule } from './procurement.router.module';

import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { JwtInterceptor } from '../_helpers/jwt.interceptors';
import { NgSelectModule } from '@ng-select/ng-select';

import { AssetApproverVerificationService } from "./service/assetapproververification.service";
import { AssetApproverVerificationComponent } from "./assetapproververification/assetapproververification.component";

import { AssetinformationService } from "../assets/service/assetinformation.service";

import { ContractService } from "./service/contract.service";
import { ContractComponent } from "./contract/contract.component";
import { ContractFormComponent } from "./contract/contractform/contractform.component";

import { WarrantFormComponent } from "./warrant/warrantform/warrantform.component";
import { WarrantComponent } from "./warrant/warrant.component";
import { WarrantService } from "./service/warrant.service";

import { WarrantapprovalComponent } from "./warrantapproval/warrantapproval.component";
import { WarrantapprovalService } from "./service/warrantapproval.service";

import { SharedModule } from "../shared/shared.module";
import { importExpr } from '@angular/compiler/src/output/output_ast';

@NgModule({
        imports: [
                CommonModule,
                DataTableModule,
                FormsModule,
                ReactiveFormsModule,
                MaterialModule,
                ProcurementRoutingModule,
                HttpClientModule,
                TabsModule.forRoot(),
                Ng2SearchPipeModule,
                HttpClientModule,
                FontAwesomeModule,
                NgSelectModule,
                NguiAutoCompleteModule,
                SharedModule
        ],
        exports: [],
        declarations: [

                ItemsetupComponent,
                ItemsetupFormComponent,
                ItemcategoryComponent,
                ItemcategoryFormComponent,
                ItemsubcategoryComponent,
                ItemsubcategoryFormComponent,
                TendershortlistComponent,
                TendershortlistformComponent,
                TenderawardComponent,
                TenderawardformComponent,
                TenderagreementComponent,
                PurchaselimitFormComponent,
                PurchaselimitComponent,
                SupplierregistrationComponent,
                SupplierregistrationFormComponent,
                PurchaserequistionentryComponent,
                PurchaserequistionentryformComponent,
                TenderQuatationFormComponent,
                TenderquotationComponent,
                PrapprovalComponent,
                PrapprovalformComponent,
                PoformComponent,
                PoComponent,
                GrnformComponent,
                GrnComponent,
                GrnapprovalComponent,
                GrnapprovalformComponent,
                OrdertypeFormComponent,
                OrdertypeComponent,
                LicenseFormComponent,
                LicenseComponent,
                SupplierapprovalFormComponent,
                SupplierapprovalComponent,
                SpecCodeComponent,
                LeveloneComponent,
                LeveltwoComponent,
                LevelthreeComponent,
                TenderregistrationComponent,
                TenderregistrationformComponent,
                PoapprovalformComponent,
                PoapprovalComponent,
                PowarrantyformComponent,
                BlockvendorComponent,
                BlockvendorFormComponent,
                PowarrantyComponent,
                PurchaseOrderPrintComponent,
                PowarrantyapprovalComponent,
                DescriptionComponent,
                PurchaseOrderRePrintComponent,
                WarrantyPrintComponent,
                WarrantyRePrintComponent,
                MofcategoryFormComponent,
                MofcategoryComponent,
                AssetApproverVerificationComponent,
                ContractFormComponent,
                ContractComponent,
                WarrantComponent,
                WarrantFormComponent,
                WarrantapprovalComponent
        ],
        providers: [
                ItemsetupService,
                BudgetactivitiesService,
                ItemcategoryService,
                ItemsubcategoryService,
                BudgetactivitiesService,
                PurchaselimitService,
                SupplierregistrationService,
                PurchaserequistionentryService,
                TenderquatationService,
                PurchaseorderService,
                PrapprovalService,
                GrnService,
                GrnapprovalService,
                OrdertypeService,
                LicenseService,
                SupplierapprovalService,
                SpecCodeService,
                BusinesscodeService,
                PoapprovalService,
                BlockvendorService,
                DescriptionService,
                PoReportapprovalService,
                PurchaseorderWarrantyService,
                PowarrantyapprovalService,
                PoReprintService,
                WarrantyPrintService,
                WarrantyRePrintService,
                DescriptionService,
                AssetApproverVerificationService,
                AssetinformationService,
                ContractService,
                WarrantService,
                WarrantapprovalService,
                MofcategoryService,
                { 
                        provide: HTTP_INTERCEPTORS,
                        useClass: JwtInterceptor,
                        multi: true
                }
        ],
})
export class ProcurementModule { }
