import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { WarrantapprovalService } from "../service/warrantapproval.service";
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'Warrantapproval',
    templateUrl: 'warrantapproval.component.html'
})

export class WarrantapprovalComponent implements OnInit {

    poapprovalList = [];
    poapprovalData = {};
    rejectData = {};
    selectAllCheckbox = false;

    constructor(
        private WarrantapprovalService: WarrantapprovalService,
        private spinner: NgxSpinnerService
    ) { }

    ngOnInit() {
        this.getListData();
    }
    getListData() {
        this.spinner.show();
        this.WarrantapprovalService.getItems().subscribe(
            data => {
                this.spinner.hide();
                this.poapprovalList = data['result']['data'];
                console.log(this.poapprovalList);
            }, error => {
                console.log(error);
            });
    }

    approveWarrantData() {
        console.log(this.poapprovalList);
        var approvaloneIds = [];
        for (var i = 0; i < this.poapprovalList.length; i++) {
            if (this.poapprovalList[i].f143fstatus == true) {
                approvaloneIds.push(this.poapprovalList[i].f143fid);
            }
        }
        if (approvaloneIds.length < 1) {
            alert("Select atleast one Vendor to Reject");
            return false;
        }
        var confirmPop = confirm("Do you want to Approve?");
        if (confirmPop == false) {
            return false;
        }
        var approvaloneUpdate = {};
        approvaloneUpdate['id'] = approvaloneIds;
        approvaloneUpdate['status'] = 1;
        console.log(approvaloneIds);
        this.WarrantapprovalService.updateWarrantApproval(approvaloneUpdate).subscribe(
            data => {

                this.getListData();
                alert(" Approved Sucessfully ! ");

            }, error => {
                console.log(error);
            });
    }
    rejectWarrantData() {
        console.log(this.poapprovalList);
        var approvaloneIds = [];
        for (var i = 0; i < this.poapprovalList.length; i++) {
            if (this.poapprovalList[i].f143fstatus == true) {
                approvaloneIds.push(this.poapprovalList[i].f143fid);
            }
        }
        if (approvaloneIds.length < 1) {
            alert("Select atleast one cost center to Reject");
            return false;
        }
        if (this.rejectData['reason'] == undefined) {
            alert('Please Enter the Description');
            return false;
        }
        var confirmPop = confirm("Do you want to Reject?");
        if (confirmPop == false) {
            return false;
        }
        var budgetActivityDataObject = {}
        budgetActivityDataObject['id'] = approvaloneIds;
        budgetActivityDataObject['status'] = 2;
        budgetActivityDataObject['reason'] = this.rejectData['reason'];


        this.WarrantapprovalService.updateWarrantApproval(budgetActivityDataObject).subscribe(
            data => {
                this.getListData();
                this.rejectData['reason'] = '';
                alert(" Rejected Sucessfully ! ");
            }, error => {
                console.log(error);
            });
    }
}
