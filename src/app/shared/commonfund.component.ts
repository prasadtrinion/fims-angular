import {  Injectable } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { FundService } from "../generalsetup/service/fund.service";
@Injectable()
@Component({
    selector: 'CommonFundComponent',
    templateUrl: 'commonfund.component.html'
})

export class CommonFundComponent implements OnInit {
    fundList = [];
    idFund;
    constructor(
        private FundService: FundService

       ) { 
    }

    ngOnInit() {
        this.FundService.getItems().subscribe(
            data => {
        
                this.fundList = data['result']['data'];
        }, error => {
            console.log(error);
        });
    }

 
}