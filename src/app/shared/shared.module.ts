import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { MaterialModule } from '../material/material.module';
import { HttpClientModule, HttpClient, HTTP_INTERCEPTORS } from '@angular/common/http';
import { Ng2SearchPipeModule } from 'ng2-search-filter';



import { SharedRoutingModule } from './shared.router.module';

import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { JwtInterceptor } from '../_helpers/jwt.interceptors';
import { ArraySortPipe } from "./sort.pipe";
@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        MaterialModule,
        SharedRoutingModule,
        HttpClientModule,
        TabsModule.forRoot(),
        Ng2SearchPipeModule,
        HttpClientModule,
        FontAwesomeModule,
    ],
    exports: [
        ArraySortPipe
    ],
    declarations: [
ArraySortPipe

    ],
    providers: [

        {
            provide: HTTP_INTERCEPTORS,
            useClass: JwtInterceptor,
            multi: true
        }

    ],
})
export class SharedModule { }
