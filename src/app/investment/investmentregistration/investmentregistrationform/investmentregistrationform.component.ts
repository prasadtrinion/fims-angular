import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { InvestmentregistrationService } from '../../service/investmentregistration.service';
import { InvestmentinstitutionService } from '../../service/investmentinstitution.service';
import { VoucherService } from '../../../accountsrecivable/service/voucher.service';
import { InvestmentapplicationService } from '../../service/investmentapplication.service';
import { BanksetupaccountService } from '../../../generalsetup/service/banksetupaccount.service';
import { BankService } from '../../../generalsetup/service/bank.service';
import { GlcodegenerationService } from '../../../generalledger/service/glcodegeneration.service';
import { DefinationmsService } from '../../../studentfinance/service/definationms.service';
import { FundService } from '../../../generalsetup/service/fund.service'
import { ActivitycodeService } from '../../../generalsetup/service/activitycode.service'
import { AccountcodeService } from '../../../generalsetup/service/accountcode.service'
import { DepartmentService } from '../../../generalsetup/service/department.service'
import { TypeofinvestmentService } from "../../service/typeofinvestment.service";
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Alert } from 'selenium-webdriver';
import { HttpClientModule, HttpHeaders, HttpClient } from '@angular/common/http';
import { environment } from '../../../../environments/environment';

@Component({
    selector: 'InvestmentregistrationFormComponent',
    templateUrl: 'investmentregistrationform.component.html'
})

export class InvestmentregistrationFormComponent implements OnInit {
    investmentregistrationform: FormGroup;
    invoiceList = [];
    invoiceData = {};
    InvestmentBankList = [];
    invoiceDataheader = {};
    investmentregistrationList = [];
    investmentregistrationData = {};
    investmentapeplicationList = [];
    investmentinstitutionData = {};
    banksetupaccountList = [];
    investmentapplicationList = [];
    banksetupaccountData = {};
    investmentstatusList = [];
    fundList = [];
    file;
    idview: number;
    RegObject = {};
    durationtype = [];
    accountcodeList = [];
    activitycodeList = [];
    DeptList = [];
    bankList = [];
    bankData = {};
    glList = [];
    glData = {};
    ajaxCount: number;
    saveBtnDisable: boolean;
    viewDisabled: boolean;
    id: number;
    valueDate: string;
    downloadUrl: string = environment.api.downloadbase;
    constructor(
        private InvestmentregistrationService: InvestmentregistrationService,
        private InvestmentapplicationService: InvestmentapplicationService,
        private VoucherService: VoucherService,
        private FundService: FundService,
        private AccountcodeService: AccountcodeService,
        private TypeofinvestmentService: TypeofinvestmentService,
        private ActivitycodeService: ActivitycodeService,
        private DepartmentService: DepartmentService,
        private httpClient: HttpClient,
        private BanksetupaccountService: BanksetupaccountService,
        private DefinationmsService: DefinationmsService,
        private BankService: BankService,
        private GlcodegenerationService: GlcodegenerationService,
        private route: ActivatedRoute,
        private router: Router
    ) { }




    ngOnInit() {

        this.viewDisabled = false;

        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.route.paramMap.subscribe((data) => this.idview = + data.get('idview'));

        if (this.id > 0) {

        } else {
            this.getRefNumber();

        }
        this.investmentregistrationData['f064fstatus'] = 1;
        this.investmentregistrationData['f064fidApplication'] = '';
        this.investmentregistrationData['f064fuumbank'] = '';
        // this.investmentregistrationData['f064finvestmentBank'] = '';
        this.investmentregistrationData['f064fidglcode'] = '';
        this.investmentregistrationData['f064finvestmentStatus'] = '';
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        let durationtypeobj = {};
        durationtypeobj['name'] = 'Days';
        this.durationtype.push(durationtypeobj);
        durationtypeobj = {};
        durationtypeobj['name'] = 'Month';
        this.durationtype.push(durationtypeobj);
        durationtypeobj = {};
        durationtypeobj['name'] = 'Year';
        this.durationtype.push(durationtypeobj);
        // investmentapplication 
        this.InvestmentapplicationService.getApprovedApplicationList().subscribe(
            data => {
                this.investmentapplicationList = data['result'];
            }
        );
        //company uum bank dropdown
        this.BanksetupaccountService.getActiveCompanyBank().subscribe(
            data => {
                this.banksetupaccountList = data['result']['data'];
            }
        );

        //Fund dropdown
        this.FundService.getItems().subscribe(
            data => {
                this.fundList = data['result']['data'];
            }
        );

        // activity dropdown
        this.ActivitycodeService.getItemsByLevel().subscribe(
            data => {
                this.activitycodeList = data['result']['data'];
                for (var i = 0; i < this.activitycodeList.length; i++) {
                    this.activitycodeList[i]['f058fcompleteCode'] = this.activitycodeList[i]['f058fcompleteCode'];
                    this.activitycodeList[i]['f058fcompleteCode'].trim();
                }

            }, error => {
            });
        // account dropdown

        this.ajaxCount++;
        this.AccountcodeService.getItemsByLevel().subscribe(
            data => {
                this.ajaxCount--;
                this.accountcodeList = data['result']['data'];
            }
        );

        // department dropdown
        this.DepartmentService.getItems().subscribe(
            data => {
                this.DeptList = data['result']['data'];
            }, error => {
                console.log(error);
            });
        
        //glcode  dropdown
        this.GlcodegenerationService.getItems().subscribe(
            data => {
                this.glList = data['result']['data'];
            }
        );

        this.DefinationmsService.getInvestmentStatus('InvestmentStatus').subscribe(
            data => {
                this.investmentstatusList = data['result'];
            }
        );
        this.route.paramMap.subscribe((data) => this.idview = + data.get('idview'));

        if (this.id > 0) {
            this.InvestmentregistrationService.getItemsDetail(this.id).subscribe(
                data => {
                    this.investmentregistrationData = data['result']['data'][0];
                    // this.investmentregistrationData['file'] = data['result']['data'][0]['f064ffile'];

                    console.log(data['result']['data'][0].f064ffile);
                    if (this.idview > 0) {
                        this.viewDisabled = true;

                        $("#target input,select").prop("disabled", true);

                    }
                }, error => {
                    console.log(error);
                });
        }

        console.log(this.ajaxCount);
    }

    fillInvestementDetails() {

    }

    getRefNumber() {
        let typeObject = {};
        typeObject['type'] = "FD";
        this.VoucherService.getInvoiceNumber(typeObject).subscribe(
            data => {
                this.investmentregistrationData['f064freferenceNumber'] = data['number'];
                console.log(data);

            }
        );
    }
    fileSelected(event) {

        this.file = event.target.files[0];
        console.log(this.file);

    }

    investmentRegistrationBankIdCheck() {
        this.bankList =[];
        let idApplication = this.investmentregistrationData['f064fidApplication'];
        let idBank = this.investmentregistrationData['f064finvestmentBank'];
        let checkBalance = {};
        checkBalance['applicationId'] = idApplication;
        checkBalance['bank'] = idBank;

        this.InvestmentregistrationService.checkIfRegisteredOrNot(checkBalance).subscribe(
            data => {
                console.log(data);
                if(data['result']!=1) {
                    alert('You have already made Registration with this Investment Application and Bank');
                    this.investmentregistrationData['f064finvestmentBank']='';
                    this.investmentregistrationData['f064fidApplication'] = '';
                    this.getRefNumber();
                }
            }
        );
    }

    getInvestmentbankbymaster() {
        this.bankList =[];
        let investId = this.investmentregistrationData['f064fidApplication'];
        let sum = this.investmentregistrationData['f064fidApplication'];

        console.log(investId);
        this.InvestmentregistrationService.getInvestmentBankListByMasterId(investId).subscribe(
            data => {
                this.bankList = data['result']['data'];
                 console.log(this.bankList);
            }
        );
    }
    preFillOtherDetails() {
        for(var i=0;i<this.bankList.length;i++){
            if(this.investmentregistrationData['f064finvestmentBank']==this.bankList[i]['f063fbankId']){
                this.investmentregistrationData['f064fsum'] = this.bankList[i]['f063famount'];
                this.investmentregistrationData['f064frateOfInterest'] = this.bankList[i]['f063fprofitRate'];
            }
        }
        this.investmentRegistrationBankIdCheck();

    }

    checkprofit() {
        let x = parseFloat(this.investmentregistrationData['f064finterestDay']);
        // let a = parseFloat(x).toFixed(2)
        console.log(x);
        if (isNaN(x) || x < 0 || x > 1000000) {
            // value is out of range
            alert("percentage cannot be more than 1000000 !");
            this.investmentregistrationData['f064finterestDay'] = '';
        }
    }
    fileView() {
        let fileId;
        fileId = this.investmentregistrationData['f064fid'];
        console.log("asdfsad")
        console.log(fileId);
        this.InvestmentregistrationService.getItemsDetail(fileId).subscribe(
            data => {

                if (data['result']['data'][0]['f063ffile'] == null) {
                    alert("File not uploaded !");
                } else {
                    window.open(this.downloadUrl + data['result']['data'][0]['f063ffile']);

                }
                console.log(data['result']['data'][0]['f063ffile']);
            }, error => {
                console.log(error);
            });
    }

    dateCompare() {
        var effectiveDate = new Date(this.investmentregistrationData['f064feffectiveDate']);
        var maturityDate = new Date(this.investmentregistrationData['f064fmaturityDate']);
        var timeDiff = Math.abs(maturityDate.getTime() - effectiveDate.getTime());
        var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
        console.log("maturityDate " + maturityDate);
        console.log("maturityDate " + effectiveDate);
        console.log("diffDays " + diffDays);
        let amountpercentage = this.investmentregistrationData['f064frateOfInterest'] / 100;
        let interestAmout = ((diffDays / 356) * (amountpercentage) * (this.investmentregistrationData['f064fsum']));
        this.investmentregistrationData['f064finterestDay'] = interestAmout.toFixed(2);
    }

    addInvestmentregistration() {

        let investmentRegObject = {};
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        console.log(this.investmentregistrationData);
        if (this.investmentregistrationData['f064fcontactEmail'] == this.investmentregistrationData['f064fcontactEmail2']) {
            alert("Email One and Email Two cannot be same");
            return false;
        }
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        this.investmentregistrationData['f064fsum'] = this.ConvertToFloat(this.investmentregistrationData['f064fsum']).toFixed(2);

        let fd = new FormData();
        fd.append('file', this.file);

        if (this.id > 0) {
            this.InvestmentregistrationService.updateInvestmentregistrationItems(this.investmentregistrationData, this.id).subscribe(
                data => {

                    console.log(data['status']);
                    switch (data['status']) {
                        case 241:
                            alert('Enter two digits after decimal for Amount field');
                            break;

                        case 409:
                            alert('Certificate Number Already Exist');
                            break;

                        case 200:
                            this.router.navigate(['investment/investmentregistration']);
                            alert('Investment Registration has been Updated Successfully');
                            break;
                    }

                    // if (data['status'] == 409) {
                    //     alert('Investment Registration Already Exist');
                    // } else if (data['status'] == 241) {
                    //     alert('Enter two digits after decimal for Amount field');
                    // }
                    // else if(data['status'] == 200){
                    //     this.router.navigate(['investment/investmentregistration']);
                    //     alert("Investment Registration has been Updated Successfully")
                    // }

                }, error => {
                    console.log(error);
                });
        } else {

            console.log(this.investmentregistrationData['file']);
            console.log(this.file);

            if (this.file == undefined) {
                alert("Select file !");
            }

            this.httpClient.post(environment.api.filebase, fd)
                .subscribe(
                    data => {
                        this.investmentregistrationData['f064ffile'] = data['name'];
                        
                        this.InvestmentregistrationService.insertInvestmentregistrationItems(this.investmentregistrationData).subscribe(
                            data => {

                                console.log(investmentRegObject);

                                console.log(data['status']);
                                switch (data['status']) {
                                    case 241:
                                        alert('Enter two digits after decimal');
                                        break;

                                    case 409:
                                        alert('Certificate Number Already Exist');
                                        break;

                                    case 200:
                                        this.router.navigate(['investment/investmentregistration']);
                                        alert('Investment Registration has been Updated Successfully');
                                        break;
                                }

                                // if (data['status'] == 409) {
                                //     alert('Already Exist');
                                // }
                                // else {
                                //     this.router.navigate(['investment/investmentregistration']);
                                //     alert("Investment Registration has been Added Successfully");
                                // }
                            }, error => {
                                console.log(error);
                            });
                    },
                    err => {
                        console.log("Error occured");
                    }
                );
        }

    }

    ConvertToFloat(val) {
        return parseFloat(val);
    }

    onlyNumberKey(event) {
        console.log(event);
        if (event.charCode == 46) {

        } else {
            return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
        }
    }

}