import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { InvestmentregistrationService } from '../service/investmentregistration.service'
import { diff } from 'semver';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'Investmentregistration',
    templateUrl: 'investmentregistration.component.html'
})

export class InvestmentregistrationComponent implements OnInit {
    investmentregistrationList = [];
    investmentregistrationData = {};
    effectiveDate: Date;
    maturityDate: Date;
    Duration: number;
    id: number;

    constructor(

        private InvestmentregistrationService: InvestmentregistrationService,
        private spinner: NgxSpinnerService,


    ) { }

    ngOnInit() {
        this.spinner.show();                                

        this.InvestmentregistrationService.getItems().subscribe(
            data => {
                this.spinner.hide();
                this.investmentregistrationList = data['result']['data'];
                // this.investmentregistrationData['s'] = parseInt(data['result'][0]['f064fstatus']);
                this.getDuration();
            }, error => {
                console.log(error);
            });
    }

    getDuration() {

        for (var i = 0; i < this.investmentregistrationList.length; i++) {

            this.effectiveDate = new Date(this.investmentregistrationList[i]['f064feffectiveDate']);
            this.maturityDate = new Date(this.investmentregistrationList[i]['f064fmaturityDate']);

            var timeDiff = Math.abs(this.maturityDate.getTime() - this.effectiveDate.getTime());
            var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
            this.investmentregistrationList[i]['Duration'] = diffDays;
            
        }

        
        console.log(this.investmentregistrationList);




    }

    addInvestmentregistration() {
        console.log(this.investmentregistrationData);
        this.InvestmentregistrationService.insertInvestmentregistrationItems(this.investmentregistrationData).subscribe(
            data => {
                this.investmentregistrationList = data['todos'];
            }, error => {
                console.log(error);
            });

    }
}