import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InvestmentinstitutionComponent } from './investmentinstitution/investmentinstitution.component';
import { InvestmentinstitutionFormComponent } from './investmentinstitution/investmentinstitutionform/investmentinstitutionform.component';

import { InvestmentapplicationComponent } from './investmentapplication/investmentapplication.component';
import { InvestmentapplicationFormComponent } from './investmentapplication/investmentapplicationform/investmentapplicationform.component';

import { TypeofinvestmentComponent } from './typeofinvestment/typeofinvestment.component';
import { TypeofinvestmentFormComponent } from './typeofinvestment/typeofinvestmentform/typeofinvestmentform.component';

import { InvestmentregistrationComponent } from './investmentregistration/investmentregistration.component';
import { InvestmentregistrationFormComponent } from './investmentregistration/investmentregistrationform/investmentregistrationform.component';
import { LetterofinstitutionComponent } from './letterofinstitution/letterofinstitution.component';
import { WithdrawletterComponent } from './withdrawletter/withdrawletter.component';
import { ApprovaloneComponent } from './approvalone/approvalone.component';
import { ApprovaltwoComponent } from './approvaltwo/approvaltwo.component';
import { ApprovalthreeComponent } from './approvalthree/approvalthree.component'

import { LetterOfInstructionComponent } from './letterofinstruction/letterofinstruction.component';
import { ApplicationletterComponent } from './applicationletter/applicationletter.component';
import { UploadfileComponent } from './uploadfile/uploadfile.component';

import { ReinvestmentEditComponent } from './reinvestment/reinvestmentedit.component.html/reinvestmentedit.component';
import { InstructionletterComponent } from './instructionletter/instructionletter.component';
import { InvestmentsummaryComponent } from './investmentsummary/investmentsummary.component';
import { InvestmentsummaryformComponent } from './investmentsummary/investmentsummaryform/investmentsummaryform.component';

import { OnlineapplicationoneComponent } from './onlineapplicationone/onlineapplicationone.component';
import { OnlineapprovaloneComponent } from './onlineapprovalone/onlineapprovalone.component';
import { OnlineapprovaltwoComponent } from './onlineapprovaltwo/onlineapprovaltwo.component';
import { OnlineapprovalthreeComponent } from "./onlineapprovalthree/onlineapprovalthree.component";
import { SoldapplicationComponent } from './soldapplication/soldapplication.component';
import { ReinvestmentComponent } from './reinvestment/reinvestment.component';
import { NotificationComponent } from './notification/notification.component';
import { NotificationFormComponent } from './notification/notificationform/notificationform.component';
import { ApprovaloneformComponent } from './approvalone/approvaloneform/approvaloneform.component';
import { ApprovalthreeformComponent } from './approvalthree/approvalthreeform/approvalthreeform.component';
import { ApprovaltwoformComponent } from './approvaltwo/approvaltwoform/approvaltwoform.component';

import { WithdrawFormComponent } from "./withdrawletter/withdrawaletterform/withdrawaletterform.component";

import { BatchWithdrawComponent } from "./batchwithdraw/batchwithdraw.component";
import { BatchWithdrawFormComponent } from "./batchwithdraw/batchwithdrawform/batchwithdrawform.component";

const routes: Routes = [

  { path: 'investmentinstitution', component: InvestmentinstitutionComponent },
  { path: 'addnewinvestmentinstitution', component: InvestmentinstitutionFormComponent },
  { path: 'editinvestmentinstitution/:id', component: InvestmentinstitutionFormComponent },

  { path: 'investmentapplication', component: InvestmentapplicationComponent },
  { path: 'addnewinvestmentapplication', component: InvestmentapplicationFormComponent },
  { path: 'editinvestmentapplication/:id/:idview', component: InvestmentapplicationFormComponent },


  { path: 'investmentregistration', component: InvestmentregistrationComponent },
  { path: 'addnewinvestmentregistration', component: InvestmentregistrationFormComponent },
  { path: 'editinvestmentregistration/:id/:idview', component: InvestmentregistrationFormComponent },



  { path: 'typeofinvestment', component: TypeofinvestmentComponent },
  { path: 'addnewtypeofinvestment', component: TypeofinvestmentFormComponent },
  { path: 'edittypeofinvestment/:id', component: TypeofinvestmentFormComponent },

  { path: 'approvalone/:id', component: ApprovaloneComponent },
  { path: 'approvaloneform', component: ApprovaloneformComponent },

  { path: 'approvaltwo/:id', component: ApprovaltwoComponent },
  { path: 'approvaltwoform', component: ApprovaltwoformComponent },

  { path: 'approvalthree/:id', component: ApprovalthreeComponent },
  { path: 'approvalthreeform', component: ApprovalthreeformComponent },

  { path: 'viewapprovalone/:id/:idview', component: InvestmentapplicationFormComponent },
  { path: 'viewapprovaltwo/:id/:idview', component: InvestmentapplicationFormComponent },
  { path: 'viewapprovalthree/:id/:idview', component: InvestmentapplicationFormComponent },

  { path: 'onlineapplicationone', component: OnlineapplicationoneComponent },

  { path: 'onlineapprovalone', component: OnlineapprovaloneComponent },
  { path: 'onlineapprovaltwo', component: OnlineapprovaltwoComponent },
  { path: 'onlineapprovalthree', component: OnlineapprovalthreeComponent },

  { path: 'onlineapprovaltwo/:id/:idview', component: OnlineapprovaltwoComponent },
  { path: 'onlineapprovalthree/:id/:idview', component: OnlineapprovalthreeComponent },

  { path: 'viewonlineapprovalone/:id/:idview', component: InvestmentregistrationFormComponent },
  { path: 'viewonlineapprovaltwo/:id/:idview', component: InvestmentregistrationFormComponent },
  { path: 'viewonlineapprovalthree/:id/:idview', component: InvestmentregistrationFormComponent },


  { path: 'soldapplication', component: SoldapplicationComponent },

  { path: 'reinvestment', component: ReinvestmentComponent },
  { path: 'reinvestmentadd', component: ReinvestmentEditComponent },


  { path: 'letterofinstitution', component: LetterofinstitutionComponent },
  { path: 'applicationletter', component: ApplicationletterComponent },


  { path: 'addwithdrawletter', component: WithdrawFormComponent },
  { path: 'withdrawletter', component: WithdrawletterComponent },

  { path: 'letterofinstruction', component: LetterOfInstructionComponent },
  { path: 'instructionletter', component: InstructionletterComponent },

  { path: 'uploadfile', component: UploadfileComponent },

  { path: 'notification', component: NotificationComponent },
  { path: 'addnewnotification', component: NotificationFormComponent },

  { path: 'investmentsummary', component: InvestmentsummaryComponent },
  { path: 'investmentsummaryform/:id', component: InvestmentsummaryformComponent },

  { path: 'batchwithdraw', component: BatchWithdrawComponent },
  { path: 'addbatchwithdraw', component: BatchWithdrawFormComponent },
  { path: 'editbatchwithdraw/:id', component: BatchWithdrawFormComponent },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],

})
export class InvestmentRoutingModule {

}