import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { BanksetupaccountService } from '../../../generalsetup/service/banksetupaccount.service';
import {BatchWithdrawService} from '../../service/batchwithdraw.service'
import { DefinationmsService } from '../../../studentfinance/service/definationms.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { WithdrawletterService } from "../../service/Withdrawletter.service";
import { NgxSpinnerService } from 'ngx-spinner';
import { SupplierregistrationService } from '../../../procurement/service/supplierregistration.service';
import { BankService } from "../../../generalsetup/service/bank.service";

@Component({
    selector: 'BatchWithdrawFormComponent',
    templateUrl: 'batchwithdrawform.component.html'
})

export class BatchWithdrawFormComponent implements OnInit {
    batchWithdrawDataheader = {};
    paymentvoucherform: FormGroup;
    payType: string;
    custId: number;
    studentId: number;
    valueDate: string;
    billRegistrationList = [];
    id: number;
    newBillRegistration = [];
    sourceList = [];
    bankList = [];
    viewDisabled: boolean;
    saveBtnDisable: boolean;
    viewid: number;
    ajaxCount: number;
    vendorsupplierList = [];
    originalData = [];
    withdrawBatchList = [];
    paymenttypeList = [];
    paymentmodeList = [];
    companyBankList = [];
    totalAmount = 0;
    constructor(
        private DefinationmsService: DefinationmsService,
        private BankService: BankService,
        private spinner: NgxSpinnerService,
        private BatchWithdrawService:BatchWithdrawService,
        private WithdrawletterService: WithdrawletterService,
        private BanksetupaccountService: BanksetupaccountService,
        private route: ActivatedRoute,
        private router: Router
    ) { }
    ngDoCheck() {
        if (this.ajaxCount == 0) {
            this.spinner.hide();
        }
    }

    ngOnInit() {

        // company bank 
        this.ajaxCount++;
        this.BanksetupaccountService.getActiveCompanyBank().subscribe(
            data => {
                this.ajaxCount--;
                this.companyBankList = data['result']['data'];
                console.log(this.companyBankList);
            }, error => {
                console.log(error);
            });

            //bank
            this.ajaxCount++;
            this.BankService.getActiveBank().subscribe(
                data => {
                    this.ajaxCount--;
                    this.bankList = data['result']['data'];
                }, error => {
                    console.log(error);
                });

        this.ajaxCount++;
        this.DefinationmsService.getPaymenttype('PaymentVoucherType').subscribe(
            data => {
                this.ajaxCount--;
                this.paymenttypeList = data['result'];
                console.log(this.paymenttypeList);
            }, error => {
                console.log(error);
            });

        // payment mode dropdown
        this.ajaxCount++;
        this.DefinationmsService.getPaymentmode('PaymentMode').subscribe(
            data => {
                this.ajaxCount--;
                this.paymentmodeList = data['result'];
            }, error => {
                console.log(error);
            });

       
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.ajaxCount++;
            this.BatchWithdrawService.getItemsDetail(this.id).subscribe(
                data => {
                    this.ajaxCount--;
                    this.withdrawBatchList = data['result']['data'];
                    this.batchWithdrawDataheader['f145fbatchNo'] = data['result']['data'][0]['f145fbatchNo'];
                    this.batchWithdrawDataheader['f145finvoiceBank'] = data['result']['data'][0]['f145finvoiceBank'];
                    this.batchWithdrawDataheader['f145fuumBank'] = data['result']['data'][0]['f145fuumBank'];
                    this.batchWithdrawDataheader['f145fmaturityDate'] = data['result']['data'][0]['f145fmaturityDate'];
                    this.batchWithdrawDataheader['f145fcertificateDate'] = data['result']['data'][0]['f145fcertificateDate'];
            
                }, error => {
                    console.log(error);
                });
        }

    }

    getDetails() {
        let withdraw = {};
        withdraw['idBank'] = this.batchWithdrawDataheader['f145finvoiceBank'];
        withdraw['idUUMBank'] = this.batchWithdrawDataheader['f145fuumBank'];
        console.log(withdraw);
        this.BatchWithdrawService.getRangeData(withdraw).subscribe(
            data => {
                this.ajaxCount--;
                this.withdrawBatchList = data['result']['data'];
        
            }, error => {
                console.log(error);
            });
    }

    savePaymentvoucher() {
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        let indobject = {};
        let detailsArray = [];
        this.totalAmount = 0;
        for (var i = 0; i < this.withdrawBatchList.length; i++) {
            if (this.withdrawBatchList[i]['selectedvalue'] == true) {
                detailsArray.push(this.withdrawBatchList[i]);

            }
        }
        this.batchWithdrawDataheader['details'] = detailsArray;
        console.log(this.batchWithdrawDataheader);
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.BatchWithdrawService.updateApprovallimitItems(this.batchWithdrawDataheader, this.id).subscribe(
                data => {
                    this.router.navigate(['investment/batchwithdraw']);
                    alert("Batch Withdraw has been Updated Successfully");
                }, error => {
                    console.log(error);
                });
        } else {
            this.BatchWithdrawService.insertEtf(this.batchWithdrawDataheader).subscribe(
                data => {
                    this.router.navigate(['investment/batchwithdraw']);
                    alert("Batch Withdraw has been Added Successfully");
                }, error => {
                    console.log(error);
                });
        }

    }



}