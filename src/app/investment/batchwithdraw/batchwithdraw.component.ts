import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { BatchWithdrawService } from "../service/batchwithdraw.service";
import { environment } from '../../../environments/environment';

@Component({
    selector: 'batchWithdraw',
    templateUrl: 'batchwithdraw.component.html'
})

export class BatchWithdrawComponent implements OnInit {
    etfvoucherList =  [];
    etfvoucherData = {};
    id: number;
    etfDetails = [];
    downloadUrl: string = environment.api.downloadbase;

    constructor(
        private BatchWithdrawService: BatchWithdrawService,
        private spinner: NgxSpinnerService,                
    ) { }

    ngOnInit() { 
        this.BatchWithdrawService.getItems().subscribe(
            data => {
                console.log(data);
                this.etfDetails = data['result']['data'];
            }, error => {
                console.log(error);
            });
    }

    downloadData(id) {
        var approvaloneUpdate = {};
        approvaloneUpdate['applicationId'] = id;
        console.log("asdfsad")
        console.log(id);
        this.BatchWithdrawService.downloadDataprint(approvaloneUpdate).subscribe(
            data => {
                window.open(this.downloadUrl + data['name']);

                // window.open('http://uum-api.mtcsb.my/download/'+data['name']);
            }, error => {
                console.log(error);
            });
    }
}