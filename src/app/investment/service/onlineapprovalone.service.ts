import { Injectable } from '@angular/core';
import { HttpClientModule,HttpHeaders, HttpClient} from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({ 'X-Auth-Client': 'qazwsx123',
    'Content-Type': 'application/json' })
    
  };
@Injectable()

export class OnlineapprovaloneService {

     url: string = environment.api.base + environment.api.endPoints.sellonlineapprovalone;
     approvalPuturl: string = environment.api.base + environment.api.endPoints.sellapproveOne;
     approveoneurl: string = environment.api.base + environment.api.endPoints.sellrejectOne;
     getRegistrationApprovalOneListURL :string = environment.api.base + environment.api.endPoints.getRegistrationApprovalOneList;
     registrationApprovalStatus :string = environment.api.base + environment.api.endPoints.registrationApprovalStatus;
     rejectiononeURL :string = environment.api.base + environment.api.endPoints.registrationRejectOne;
     getInvestmentRegistrationApprovalListUrl: string = environment.api.base + environment.api.endPoints.getInvestmentRegistrationApprovalList;

    constructor(private httpClient: HttpClient) {    
    }
    getItems() {
        return this.httpClient.get(this.url+'/'+2,httpOptions);
    }   
    getItemsDetail(id) {
        return this.httpClient.get(this.getInvestmentRegistrationApprovalListUrl+'/'+1+'/'+id, httpOptions);
    }
    insertOnlineapprovaloneItems(onlineapprovaloneData): Observable<any> {
        return this.httpClient.post(this.url, onlineapprovaloneData,httpOptions);
    }
    updateOnlineapprovaloneItems(onlineapprovaloneData): Observable<any> {
        return this.httpClient.post(this.registrationApprovalStatus, onlineapprovaloneData,httpOptions);
    }
    rejectOnlineapprovalone(onlineapprovaloneData): Observable<any> {
        return this.httpClient.post(this.registrationApprovalStatus, onlineapprovaloneData,httpOptions);
    }
}