import { Injectable } from '@angular/core';
import { HttpClientModule, HttpHeaders, HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({
        'X-Auth-Client': 'qazwsx123',
        'Content-Type': 'application/json'
    })

};
@Injectable()

export class OnlineapplicationoneService {
    listofApprovalUserUrl: string = environment.api.base + environment.api.endPoints.sellonlineapprovalone;
    investmentRegistrationDetailsUrl : string = environment.api.base+environment.api.endPoints.investmentRegistrationDetails;
    registrationApprove: string = environment.api.base + environment.api.endPoints.sellapproveOne;
    registrationReject: string = environment.api.base + environment.api.endPoints.sellrejectOne;

    constructor(private httpClient: HttpClient) {
    }

    getItems() {
        return this.httpClient.get(this.listofApprovalUserUrl, httpOptions);
    }

    getItemsDetail(id) {
        return this.httpClient.get(this.listofApprovalUserUrl + '/' + id, httpOptions);
    }
    // getItemsDetail(id) {
    //     return this.httpClient.get(this.url + '/' + id, httpOptions);
    // }
    insertOnlineapplicationoneItems(onlineapplicationoneData): Observable<any> {
        return this.httpClient.post(this.listofApprovalUserUrl, onlineapplicationoneData, httpOptions);
    }

    updateOnlineapplicationoneItems(onlineapplicationoneData, id): Observable<any> {
        return this.httpClient.put(this.listofApprovalUserUrl + '/' + id, onlineapplicationoneData, httpOptions);
    }
    insertOnlineapplicationone(onlineapprovaloneData): Observable<any> {
        return this.httpClient.post(this.listofApprovalUserUrl, onlineapprovaloneData, httpOptions);
    }
}