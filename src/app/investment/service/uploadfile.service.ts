import { Injectable } from '@angular/core';
import { HttpClientModule, HttpHeaders, HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({
        'X-Auth-Client': 'qazwsx123',
        'Content-Type': 'application/json'
    })

};
@Injectable()

export class UploadfileService {
    printUrl: string = environment.api.base + environment.api.endPoints.uploadfile;

    constructor(private httpClient: HttpClient) {
    }

    downloadDataprint(ApprovalthreeData): Observable<any> {
        return this.httpClient.post(this.printUrl,ApprovalthreeData, httpOptions);
    }
   
}