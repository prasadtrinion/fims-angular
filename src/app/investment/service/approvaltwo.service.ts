import { Injectable } from '@angular/core';
import { HttpClientModule,HttpHeaders, HttpClient} from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({ 'X-Auth-Client': 'qazwsx123',
    'Content-Type': 'application/json' })
    
  };
@Injectable()

export class ApprovaltwoService {

    getListurl: string = environment.api.base + environment.api.endPoints.getInvestmentApplicationApprovalList
    approvalPuturl: string = environment.api.base + environment.api.endPoints.detailApproval
     approvetwourl: string = environment.api.base + environment.api.endPoints.rejectTwo
     invesmentGetByApprovalDetailsUrl :  string = environment.api.base + environment.api.endPoints.invesmentGetByApprovalDetails;
    constructor(private httpClient: HttpClient) {    
    }
    getItems() {
        return this.httpClient.get(this.invesmentGetByApprovalDetailsUrl,httpOptions);
    }
    getItemsDetail(id) {
        return this.httpClient.get(this.getListurl+'/'+2+'/'+id, httpOptions);
    }
    insertApprovaltwoItems(ApprovaltwoData): Observable<any> {
        return this.httpClient.post(this.getListurl, ApprovaltwoData,httpOptions);
    }
    updateApprovaltwoItems(ApprovaltwoData): Observable<any> {
        return this.httpClient.put(this.approvalPuturl, ApprovaltwoData,httpOptions);
    }
    rejectApprovalTwo(ApprovaltwoData): Observable<any> {
        return this.httpClient.post(this.approvalPuturl, ApprovaltwoData,httpOptions);
    }
}