import { Injectable } from '@angular/core';
import { HttpClientModule,HttpHeaders, HttpClient} from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({ 'X-Auth-Client': 'qazwsx123',
    'Content-Type': 'application/json' })

  };
@Injectable()

export class ReinvestmentService {
    url: string = environment.api.base + environment.api.endPoints.investmentregistration;
    getRegistrationListUrl: string = environment.api.base + environment.api.endPoints.getRegistrationList;
    reinvestmentUrl: string = environment.api.base + environment.api.endPoints.reinvestment;
    getReinvestmentUrl: string = environment.api.base + environment.api.endPoints.getReinvestment;

    constructor(private httpClient: HttpClient) {    
    }
    
    getItems() {
        return this.httpClient.get(this.url,httpOptions);
    }

    getItemsDetail(id) {
        return this.httpClient.get(this.url+'/'+id,httpOptions);
    }

    getRegistrationList(){
        return this.httpClient.get(this.getRegistrationListUrl, httpOptions);
    }

    getReinvestItems(){
        return this.httpClient.get(this.getReinvestmentUrl,httpOptions);

    }

    updateItems(reinvestmentData){
        return this.httpClient.post(this.reinvestmentUrl, reinvestmentData,httpOptions);
    }

    insertReinvestmentItems(reinvestmentData): Observable<any> {
        return this.httpClient.post(this.url, reinvestmentData,httpOptions);
    }

    updateReinvestmentItems(reinvestmentData,id): Observable<any> {
        return this.httpClient.put(this.url+'/'+id, reinvestmentData,httpOptions);
       }
}