
import { Injectable } from '@angular/core';
import { HttpClientModule, HttpHeaders, HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({
        'X-Auth-Client': 'qazwsx123',
        'Content-Type': 'application/json'
    })

};

@Injectable()

export class TypeofinvestmentService {
    url: string = environment.api.base + environment.api.endPoints.typeofinvestment;
    investmenturl: string = environment.api.base + environment.api.endPoints.investmenttype;
    getInvestmentAccountCodesUrl: string = environment.api.base + environment.api.endPoints.getInvestmentAccountCodes;

    constructor(private httpClient: HttpClient) {

    }

    getItems() {
        return this.httpClient.get(this.url, httpOptions);
    }

    getItemsDetail(id) {
        return this.httpClient.get(this.url + '/' + id, httpOptions);
    }


    insertTypeofinvestmentItems(typeofinvestmentData): Observable<any> {
        return this.httpClient.post(this.url, typeofinvestmentData, httpOptions);

    }
    getDebotorAccountcode(){
        return this.httpClient.get(this.getInvestmentAccountCodesUrl, httpOptions);
    }

    updateTypeofinvestmentItems(typeofinvestmentData, id): Observable<any> {
        return this.httpClient.put(this.url + '/' + id, typeofinvestmentData, httpOptions);
    }
    getActiveInvestmentType() {
        return this.httpClient.get(this.investmenturl, httpOptions);
    }
}