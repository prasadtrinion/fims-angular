import { Injectable } from '@angular/core';
import { HttpClientModule, HttpHeaders, HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({
        'X-Auth-Client': 'qazwsx123',
        'Content-Type': 'application/json'
    })

};
@Injectable()

export class InvestmentsummaryService {
    url: string = environment.api.base + environment.api.endPoints.getInvestmentSummary;
    approvedListUrl: string = environment.api.base + environment.api.endPoints.approvedList;
    getListUrl: string = environment.api.base + environment.api.endPoints.getInvestmentApplicationApprovalList;
    getApplicationListurl: string = environment.api.base + environment.api.endPoints.getApplicationList;
    deleteturl: string = environment.api.base + environment.api.endPoints.deleteInvestmentApplicationDetails;
    getAllInvestmentTypeurl: string = environment.api.base + environment.api.endPoints.getAllInvestmentType;
    
    getInvestmentBankListByMasterIdurl: string = environment.api.base + environment.api.endPoints.getInvestmentBankListByMasterId;
   
    constructor(private httpClient: HttpClient) {
    }

    getItems() {
        return this.httpClient.get(this.url, httpOptions);
    }
    deleteItems(deleteObject) {
        return this.httpClient.post(this.deleteturl,deleteObject, httpOptions);
    }
    getItemsDetail(id) {
        return this.httpClient.get(this.url + '/' + id, httpOptions);
    }

    insertInvestmentsummaryItems(investmentsummaryData): Observable<any> {
        return this.httpClient.post(this.url, investmentsummaryData, httpOptions);
    }

    updateInvestmentsummaryItems(investmentsummaryData): Observable<any> {
        return this.httpClient.put(this.url, investmentsummaryData, httpOptions);
    }

   
    getApprovedApplicationList() {
        return this.httpClient.get(this.getApplicationListurl,httpOptions);
    }

    getInvestmentTypeByInstituteId(instituteId){
        return this.httpClient.get(this.getAllInvestmentTypeurl + '/' + instituteId, httpOptions);

    }
    getInvestmentBankListByMasterId(investId){
        return this.httpClient.get(this.getInvestmentBankListByMasterIdurl + '/' + investId, httpOptions);

    }
}