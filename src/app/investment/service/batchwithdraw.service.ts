import { Injectable } from '@angular/core';
import { HttpClientModule,HttpHeaders, HttpClient} from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({ 'X-Auth-Client': 'qazwsx123',
    'Content-Type': 'application/json' })
    
  };
  
@Injectable()

export class BatchWithdrawService {
    url: string = environment.api.base + environment.api.endPoints.batchWithdraw;
    approveEtfDataurl : string = environment.api.base + environment.api.endPoints.approveEtfData;
    printWithdrawUrl : string = environment.api.base + environment.api.endPoints.printWithdraw;

    getInvestmentRegNotInBatchWithdrawurl : string = environment.api.base + environment.api.endPoints.getInvestmentRegNotInBatchWithdraw;
    constructor(private httpClient: HttpClient) { 
        
    }
    
    getItems() {
        return this.httpClient.get(this.url,httpOptions);
    }

    getRangeData(approvallimitData): Observable<any> {
        return this.httpClient.post(this.getInvestmentRegNotInBatchWithdrawurl, approvallimitData,httpOptions);
    }

    downloadDataprint(ApprovalthreeData): Observable<any> {
        return this.httpClient.post(this.printWithdrawUrl,ApprovalthreeData, httpOptions);
    }
    getItemsDetail(id) {
        return this.httpClient.get(this.url+'/'+id,httpOptions);
    }

    insertEtf(approvallimitData): Observable<any> {
        return this.httpClient.post(this.url, approvallimitData,httpOptions);
    }
    approveEtf(approvallimitData): Observable<any> {
        return this.httpClient.post(this.approveEtfDataurl, approvallimitData,httpOptions);
    }

    updateApprovallimitItems(approvallimitData,id): Observable<any> {
        return this.httpClient.post(this.url+'/'+id, approvallimitData,httpOptions);
       }
}