import { Injectable } from '@angular/core';
import { HttpClientModule, HttpHeaders, HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({
        'X-Auth-Client': 'qazwsx123',
        'Content-Type': 'application/json'
    })

};
@Injectable()

export class InvestmentregistrationService {
    url: string = environment.api.base + environment.api.endPoints.investmentregistration;
    //  printUrl :string = environment.api.base + environment.api.endPoints.fdWithdrawalProposal;
    getInvestmentBankListByMasterIdurl: string = environment.api.base + environment.api.endPoints.getInvestmentBankListByMasterId;
    investmentRegistrationBankIdCheckurl: string = environment.api.base + environment.api.endPoints.investmentRegistrationBankIdCheck;

    constructor(private httpClient: HttpClient) {
    }

    checkIfRegisteredOrNot(dataobject): Observable<any> {
        return this.httpClient.post(this.investmentRegistrationBankIdCheckurl, dataobject, httpOptions);
    }

    getItems() {
        return this.httpClient.get(this.url, httpOptions);
    }

    updateInvestmentItems(withdrawalUpdate){
        return this.httpClient.get(this.url, httpOptions);
    }

    getItemsDetail(id) {
        return this.httpClient.get(this.url + '/' + id, httpOptions);
    }
    // downloadDataprint(ApprovalthreeData): Observable<any> {
    //     return this.httpClient.post(this.printUrl,ApprovalthreeData, httpOptions);
    // }
    insertInvestmentregistrationItems(investmentregistrationData): Observable<any> {
        return this.httpClient.post(this.url, investmentregistrationData, httpOptions);
    }

    updateInvestmentregistrationItems(investmentregistrationData, id): Observable<any> {
        return this.httpClient.put(this.url + '/' + id, investmentregistrationData, httpOptions);
    }
    getInvestmentBankListByMasterId(investId){
        return this.httpClient.get(this.getInvestmentBankListByMasterIdurl + '/' + investId, httpOptions);

    }
}