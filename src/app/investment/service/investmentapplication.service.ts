import { Injectable } from '@angular/core';
import { HttpClientModule, HttpHeaders, HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({
        'X-Auth-Client': 'qazwsx123',
        'Content-Type': 'application/json'
    })

};
@Injectable()

export class InvestmentapplicationService {
    url: string = environment.api.base + environment.api.endPoints.investmentapplication;
    approvedListUrl: string = environment.api.base + environment.api.endPoints.approvedList;
    getListUrl: string = environment.api.base + environment.api.endPoints.getInvestmentApplicationApprovalList;
    getApplicationListurl: string = environment.api.base + environment.api.endPoints.getApplicationList;
    deleteturl: string = environment.api.base + environment.api.endPoints.deleteInvestmentApplicationDetails;
    getAllInvestmentTypeurl: string = environment.api.base + environment.api.endPoints.getAllInvestmentType;
    investmentApplicationDetailsUrl : string = environment.api.base+environment.api.endPoints.investmentApplicationDetails;
    
    getInvestmentBankListByMasterIdurl: string = environment.api.base + environment.api.endPoints.investmentApplicationDetails;
   
    constructor(private httpClient: HttpClient) {
    }

    getAllItems() {
        return this.httpClient.get(this.investmentApplicationDetailsUrl, httpOptions);
    }
    getItems() {
        return this.httpClient.get(this.url, httpOptions);
    }
    deleteItems(deleteObject) {
        return this.httpClient.post(this.deleteturl,deleteObject, httpOptions);
    }
    getItemsDetail(id) {
        return this.httpClient.get(this.url + '/' + id, httpOptions);
    }

    insertInvestmentapplicationItems(investmentregistrationData): Observable<any> {
        return this.httpClient.post(this.url, investmentregistrationData, httpOptions);
    }

    updateInvestmentapplicationItems(investmentregistrationData, id): Observable<any> {
        return this.httpClient.put(this.url + '/' + id, investmentregistrationData, httpOptions);
    }

    // getInvestmentBankListByMasterId(investId) {
    //     return this.httpClient.get(this.getInvestmentBankListByMasterIdurl + '/' +investId,httpOptions);
    // }
    getApprovedApplicationList() {
        return this.httpClient.get(this.getApplicationListurl,httpOptions);
    }

    getInvestmentTypeByInstituteId(instituteId){
        return this.httpClient.get(this.getAllInvestmentTypeurl + '/' + instituteId, httpOptions);

    }
    getInvestmentBankListByMasterId(investId){
        return this.httpClient.get(this.getInvestmentBankListByMasterIdurl + '/' + investId, httpOptions);

    }
}