import { Injectable } from '@angular/core';
import { HttpClientModule,HttpHeaders, HttpClient} from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({ 'X-Auth-Client': 'qazwsx123',
    'Content-Type': 'application/json' })
    
  };
@Injectable()

export class ApprovaloneService {
    url: string = environment.api.base + environment.api.endPoints.investmentapplication;

     getListurl: string = environment.api.base + environment.api.endPoints.getInvestmentApplicationApprovalList
     approvalPuturl: string = environment.api.base + environment.api.endPoints.detailApproval
     approveoneurl: string = environment.api.base + environment.api.endPoints.rejectOne
    constructor(private httpClient: HttpClient) {    
    }
    getItems() {
        return this.httpClient.get(this.getListurl+'/'+2,httpOptions);
    }
    getItemsDetail(id) {
        return this.httpClient.get(this.getListurl+'/'+1+'/'+id, httpOptions);
    }
    insertApprovaloneItems(ApprovaloneData): Observable<any> {
        return this.httpClient.post(this.getListurl, ApprovaloneData,httpOptions);
    }
    updateApprovaloneItems(ApprovaloneData): Observable<any> {
        return this.httpClient.post(this.approvalPuturl, ApprovaloneData,httpOptions);
    } 
    getItemsDetails(id) {
        return this.httpClient.get(this.url + '/' + id, httpOptions);
    }
    rejectApprovalone(ApprovaloneData): Observable<any> {
        return this.httpClient.post(this.approvalPuturl, ApprovaloneData,httpOptions);
    }
}