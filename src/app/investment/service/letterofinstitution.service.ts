import { Injectable } from '@angular/core';
import { HttpClientModule,HttpHeaders, HttpClient} from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({ 'X-Auth-Client': 'qazwsx123',
    'Content-Type': 'application/json' })
  };
@Injectable()
export class LetterofintitutionService {
    url: string = environment.api.base + environment.api.endPoints.letter;
    getInvestmentRegistrationApprovalListUrl: string = environment.api.base + environment.api.endPoints.getInvestmentRegistrationApprovalList;

    constructor(private httpClient: HttpClient) {    
    }
    getItems() {
        return this.httpClient.get(this.getInvestmentRegistrationApprovalListUrl+'/'+4, httpOptions);
    }
    getItemsDetail(id) {
        return this.httpClient.get(this.getInvestmentRegistrationApprovalListUrl+'/'+4+'/'+id, httpOptions);
    }
    insertLetterofintitutionItems(letterData): Observable<any> {
        return this.httpClient.post(this.url, letterData,httpOptions);
    }

    updateLetterofintitutionItems(letterData,id): Observable<any> {
        return this.httpClient.put(this.url+'/'+id, letterData,httpOptions);
       }
}