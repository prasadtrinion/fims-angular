import { Injectable } from '@angular/core';
import { HttpClientModule, HttpHeaders, HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({
        'X-Auth-Client': 'qazwsx123',
        'Content-Type': 'application/json'
    })
};
@Injectable()
export class WithdrawletterService {
    url: string = environment.api.base + environment.api.endPoints.getRegistrationWithdrawlList;
    updateWithdrawlStatusUrl: string = environment.api.base + environment.api.endPoints.updateWithdrawlStatus;
    getWithdrawListurl: string = environment.api.base + environment.api.endPoints.getWithdrawList;

    constructor(private httpClient: HttpClient) {
    }

    getItems() {
        return this.httpClient.get(this.url+'/'+1, httpOptions);
    }
    getWithdrawList() {
        return this.httpClient.get(this.getWithdrawListurl, httpOptions);
    }
    getItemsDetail(id) {
        return this.httpClient.get(this.url + '/' + id, httpOptions);
    }
    insertWithdrawletterItems(withdrawletterData): Observable<any> {
        return this.httpClient.post(this.url, withdrawletterData, httpOptions);
    }
    updatewithdrawalItems(withdrawletterData): Observable<any> {
        return this.httpClient.put(this.updateWithdrawlStatusUrl, withdrawletterData, httpOptions);
    }

}