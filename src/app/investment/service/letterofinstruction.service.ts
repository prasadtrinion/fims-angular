import { Injectable } from '@angular/core';
import { HttpClientModule,HttpHeaders, HttpClient} from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({ 'X-Auth-Client': 'qazwsx123',
    'Content-Type': 'application/json' })
    
  };
@Injectable()

export class LetterOfInstructionService {

    getApprovedListUrl: string = environment.api.base + environment.api.endPoints.letter
    approveThreeUrl: string = environment.api.base + environment.api.endPoints.approveThree
    rejectThreeUrl: string = environment.api.base + environment.api.endPoints.rejectThree
    printUrl :string = environment.api.base + environment.api.endPoints.downloadhtml;
    getListUrl: string = environment.api.base + environment.api.endPoints.getInvestmentApplicationApprovalList

    constructor(private httpClient: HttpClient) {    
    }
    getItems() {
        return this.httpClient.get(this.getListUrl+'/'+4,httpOptions);
    }
    getItemsDetail(id) {
        return this.httpClient.get(this.getApprovedListUrl+'/'+id, httpOptions);
    }
    downloadDataprint(ApprovalthreeData): Observable<any> {
        return this.httpClient.post(this.printUrl,ApprovalthreeData, httpOptions);
    }

    updateApprovalthreeItems(ApprovalthreeData): Observable<any> {
        return this.httpClient.put(this.approveThreeUrl, ApprovalthreeData,httpOptions);
    }
    rejectApprovalthree(ApprovalthreeData): Observable<any> {
        return this.httpClient.post(this.rejectThreeUrl, ApprovalthreeData,httpOptions);
    }
}