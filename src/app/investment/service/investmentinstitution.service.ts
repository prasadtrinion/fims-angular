import { Injectable } from '@angular/core';
import { HttpClientModule, HttpHeaders, HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({
        'X-Auth-Client': 'qazwsx123',
        'Content-Type': 'application/json'
    })

};
@Injectable()

export class InvestmentinstitutionService {
    url: string = environment.api.base + environment.api.endPoints.investmentinstitution;
    allinvestmenturl: string = environment.api.base + environment.api.endPoints.getAllInvestmentType;
    activeListUrl: string = environment.api.base + environment.api.endPoints.activeListInstitution;
    deleteturl: string = environment.api.base + environment.api.endPoints.deleteInvestmentInstitution;
    constructor(private httpClient: HttpClient) {

    }
    deleteItems(deleteObject) {
        return this.httpClient.post(this.deleteturl,deleteObject, httpOptions);
    }
    getItems() {
        return this.httpClient.get(this.url, httpOptions);
    }

    getItemsDetail(id) {
        return this.httpClient.get(this.url + '/' + id, httpOptions);
    }

    insertInvestmentinstitutionItems(investmentinstitutionData): Observable<any> {
        return this.httpClient.post(this.url, investmentinstitutionData, httpOptions);
    }

    

    updateInvestmentinstitutionItems(investmentinstitutionData, id): Observable<any> {
        return this.httpClient.put(this.url + '/' + id, investmentinstitutionData, httpOptions);
    }
    getAllInvestmentType(id) {
        return this.httpClient.get(this.allinvestmenturl+'/'+id, httpOptions);
    }
    getActiveInstitute(){
        return this.httpClient.get(this.activeListUrl, httpOptions);
    }
}