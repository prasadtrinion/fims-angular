import { Injectable } from '@angular/core';
import { HttpClientModule,HttpHeaders, HttpClient} from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({ 'X-Auth-Client': 'qazwsx123',
    'Content-Type': 'application/json' })
    
  };
@Injectable()

export class OnlineapprovaltwoService {

    url: string = environment.api.base + environment.api.endPoints.sellonlineapprovaltwo
    approvalPuturl: string = environment.api.base + environment.api.endPoints.sellapproveTwo
     approveoneurl: string = environment.api.base + environment.api.endPoints.sellrejectTwo 
     getRegistrationApprovalTwoListUrl : string = environment.api.base + environment.api.endPoints.getRegistrationApprovalTwoList 
     registrationApproveTwoURL :  string = environment.api.base + environment.api.endPoints.registrationApproveTwo 
     registrationRejectTwoUrl  :  string = environment.api.base + environment.api.endPoints.registrationRejectTwo 
     getInvestmentRegistrationApprovalListUrl: string = environment.api.base + environment.api.endPoints.getInvestmentRegistrationApprovalList;
     registrationApprovalStatus :string = environment.api.base + environment.api.endPoints.registrationApprovalStatus;
     investmentRegistrationDetailsForApprovalsUrl : string = environment.api.base+environment.api.endPoints.investmentRegistrationDetailsForApprovals;


    constructor(private httpClient: HttpClient) {    
    }
    getItems() {
        return this.httpClient.get(this.url+'/'+2,httpOptions);
    }
    getItemsDetail(id) {
        return this.httpClient.get(this.investmentRegistrationDetailsForApprovalsUrl, httpOptions);
    }
    insertOnlineapprovaltwoItems(onlineapprovaltwoData): Observable<any> {
        return this.httpClient.post(this.url, onlineapprovaltwoData,httpOptions);
    }
    updateOnlineapprovaltwoItems(onlineapprovaltwoData): Observable<any> {
        return this.httpClient.post(this.registrationApprovalStatus, onlineapprovaltwoData,httpOptions);
    }
    rejectOnlineapprovaltwo(onlineapprovaltwoData): Observable<any> {
        return this.httpClient.post(this.registrationApprovalStatus, onlineapprovaltwoData,httpOptions);
    }
}