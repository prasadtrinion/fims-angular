import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { ApprovaltwoService } from '../../service/approvaltwo.service'
import { ActivatedRoute, Router } from '@angular/router';

import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'Approvaltwoform',
    templateUrl: 'approvaltwoform.component.html'
})

export class ApprovaltwoformComponent implements OnInit {
    rejectoneList = [];
    approvaltwoformList = [];
    approvaltwoformData = {};
    selectAllCheckbox = true;

    constructor(
        private ApprovaltwoService: ApprovaltwoService,
        private spinner: NgxSpinnerService,
        private route: ActivatedRoute,
        private router: Router

    ) { }

    ngOnInit() {
        this.getListData();
    }
    getListData() {
        this.approvaltwoformList = [];
        this.spinner.show();                                

        this.ApprovaltwoService.getItems().subscribe(
            data => {
                this.spinner.hide();

                this.approvaltwoformList = data['result'];
            }, error => {
                console.log(error);
            });
    }

    approvaltwoformListData() {     

        var confirmPop = confirm("Do you want to Approve ?");

        if (confirmPop == false) {
            return false;
        }
        console.log(this.approvaltwoformList);
        var approvaloneIds = [];
        var approvalrenewoneIds = [];

        for (var i = 0; i < this.approvaltwoformList.length; i++) {
            if (this.approvaltwoformList[i].f063fapproved1Status == true) {
                approvaloneIds.push(this.approvaltwoformList[i].f063fidDetails);
            }
            if (this.approvaltwoformList[i].autoRenewal == true) {
                approvalrenewoneIds.push(this.approvaltwoformList[i].f063fidDetails);
            }
           
        }
        if (approvaloneIds.length < 1) {
            alert("Select atleast one Application to Approve");
            return false;
        }

        var approvaloneUpdate = {};
        approvaloneUpdate['id'] = approvaloneIds;
        approvaloneUpdate['autoRenewal'] = approvalrenewoneIds;
        approvaloneUpdate['status'] = 1;
        approvaloneUpdate['level'] = 1;
        approvaloneUpdate['reason'] = this.approvaltwoformData['reason'];


        console.log(approvaloneUpdate);


        if (this.approvaltwoformData['check'] == '' || this.approvaltwoformData['check'] == undefined) {
            alert("Select Declartaion To approve !");
        } else {
        this.ApprovaltwoService.updateApprovaltwoItems(approvaloneUpdate).subscribe(
            data => {
                alert(" Application Approved ! ");

                this.router.navigate(['investment/approvaltwoform']);


            }, error => {
                console.log(error);
            });
        }
    }


    rejectthreeformListData() {

        var confirmPop = confirm("Do you want to Reject ?");

        if (confirmPop == false) {
            return false;
        }
       

        console.log(this.approvaltwoformList);
        var rejectoneIds = [];
        for (var i = 0; i < this.approvaltwoformList.length; i++) {
            if (this.approvaltwoformList[i].f063fapproved1Status == true) {
                rejectoneIds.push(this.approvaltwoformList[i].f063fidDetails);
            }
        }

        if (rejectoneIds.length < 1) {
            alert("Select atleast one Application to Reject");
            return false;
        }

        var rejectoneUpdate = {};
            rejectoneUpdate['id'] = rejectoneIds;
            rejectoneUpdate['status'] = 2;
            rejectoneUpdate['level'] = 1;
            rejectoneUpdate['reason'] = this.approvaltwoformData['reason'];


        console.log(this.approvaltwoformData['reason']);

        if (this.approvaltwoformData['reason'] == '' || this.approvaltwoformData['reason'] == undefined) {
            // alert("Enter the Description to Reject !");
        } else {

        this.ApprovaltwoService.rejectApprovalTwo(rejectoneUpdate).subscribe(
                data => {
                    alert(" Application Rejected ! ");

                    this.router.navigate(['investment/approvaltwoform']);


                }, error => {
                    console.log(error);
                });
        }
    }
}
