import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { ApprovaltwoService } from '../service/approvaltwo.service';
import { ActivatedRoute, Router } from '@angular/router';

import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'Approvaltwo',
    templateUrl: 'approvaltwo.component.html'
})

export class ApprovaltwoComponent implements OnInit {
    rejecttwoList = [];
    approvaltwoList = [];
    approvaltwoData = {};
    selectAllCheckbox = true;
    id:number;

    constructor(

        private ApprovaltwoService: ApprovaltwoService,
        private spinner: NgxSpinnerService,
        private route: ActivatedRoute,
        private router: Router

    ) { }

    ngOnInit() {
        this.getListData();
    }
    // getListData() {
    //     this.spinner.show();                                
    //     this.ApprovaltwoService.getItemsDetail(0).subscribe(
    //         data => {
    //             this.spinner.hide();
    //             this.approvaltwoList = data['result'];
    //         }, error => {
    //             console.log(error);
    //         });
    // }
    getListData() {
        this.spinner.show();    
        var listArray = []                            
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        this.ApprovaltwoService.getItemsDetail(0).subscribe(
            data => {
                this.spinner.hide();
                listArray = data['result'];
                console.log(listArray);
                for(var i=0;i<listArray.length;i++) {
                    if(listArray[i]['f063fid']==this.id) {
                        this.approvaltwoList.push(listArray[i])
                    }
                }

                
            }, error => {
                console.log(error);
            });
    }

    approvaltwoListData() {

        var confirmPop = confirm("Do you want to Approve ?");
        if (confirmPop == false) {
            return false;
        }

        console.log(this.approvaltwoList);
        var approvaltwoIds = [];
        var approvalrenewtwoIds = [];
        for (var i = 0; i < this.approvaltwoList.length; i++) {
            if (this.approvaltwoList[i].f063fapproved2Status == true) {
                approvaltwoIds.push(this.approvaltwoList[i].f063fidDetails);
            }
            if (this.approvaltwoList[i].autoRenewal == true) {
                approvalrenewtwoIds.push(this.approvaltwoList[i].f063fidDetails);
            }
        }

        if (approvaltwoIds.length < 1) {
            alert("Select atleast one Application to Approve");
            return false;
        }
        var approvaltwoUpdate = {};
        approvaltwoUpdate['id'] = approvaltwoIds;
        approvaltwoUpdate['autoRenewal'] = approvalrenewtwoIds;
        approvaltwoUpdate['status'] = 1;
        approvaltwoUpdate['level'] = 2;
        approvaltwoUpdate['reason'] = this.approvaltwoData['reason'];

        if (this.approvaltwoData['check'] == '' || this.approvaltwoData['check'] == undefined) {
            alert("Select Declartaion To approve !");
        } else {
        this.ApprovaltwoService.updateApprovaltwoItems(approvaltwoUpdate).subscribe(
            data => {
                alert(" Application Approved ! ");

                this.router.navigate(['investment/approvaltwoform']);

            }, error => {
                console.log(error);
            });
        }
    }


    rejecttwoListData() {

        var confirmPop = confirm("Do you want to Reject ?");
        if (confirmPop == false) {
            return false;
        }

        console.log(this.approvaltwoList);
        var rejectoneIds = [];
        for (var i = 0; i < this.approvaltwoList.length; i++) {
            if (this.approvaltwoList[i].f063fapproved2Status == true) {
                rejectoneIds.push(this.approvaltwoList[i].f063fidDetails);
            }
        }

        if (rejectoneIds.length < 1) {
            alert("Select atleast one Application to Reject");
            return false;
        }

        var rejecttwoUpdate = {};
        rejecttwoUpdate['id'] = rejectoneIds;
        rejecttwoUpdate['status'] = 2;
        rejecttwoUpdate['level'] = 2;
        rejecttwoUpdate['reason'] = this.approvaltwoData['reason'];


        console.log(this.approvaltwoData['reason']);

        if (this.approvaltwoData['reason'] == '' || this.approvaltwoData['reason'] == undefined) {
            alert("Enter the Description to Reject !");
        } else {


            this.ApprovaltwoService.rejectApprovalTwo(rejecttwoUpdate).subscribe(
                data => {
                    alert(" Application Rejected ! ");

                    this.getListData();
                    this.approvaltwoData['reason'] = '';

                }, error => {
                    console.log(error);
                });

        }

    }

    selectAll() {
        console.log("asdf");
        console.log(this.selectAllCheckbox);
        for (var i = 0; i < this.approvaltwoList.length; i++) {
            this.approvaltwoList[i].f063fapprovedStatus = this.selectAllCheckbox;
        }
        console.log(this.approvaltwoList);
    }
}

