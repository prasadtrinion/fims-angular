import {Injector} from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { ApprovalthreeService } from '../service/approvalthree.service';
import { LetterOfInstructionService } from '../service/letterofinstruction.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { environment } from '../../../environments/environment';

@Component({
    selector: 'LetterOfInstructionComponent',
    templateUrl: 'letterofinstruction.component.html'
})

export class LetterOfInstructionComponent implements OnInit {
    rejecttwoList=[];
    LetterOfInstructionList =  [];
    LetterOfInstructionData = {};
    selectAllCheckbox = true;
    downloadUrl: string = environment.api.downloadbase;

    constructor(
        
        private ApprovalthreeService: ApprovalthreeService,
        private LetterOfInstructionService: LetterOfInstructionService,
        private spinner: NgxSpinnerService
    ) { }

    ngOnInit()
     {    
       this.getListData();

   
    }
    downloadData(id) {
        var approvaloneUpdate = {};
        approvaloneUpdate['applicationId'] = id;
        console.log("asdfsad")
        console.log(id);
        this.ApprovalthreeService.downloadDataprint(approvaloneUpdate).subscribe(
            data => {
                window.open(this.downloadUrl + data['name']);

                // window.open('http://uum-api.mtcsb.my/download/'+data['name']);
            }, error => {
                console.log(error);
            });
    }
    getListData(){
        this.spinner.show();
        this.LetterOfInstructionService.getItems().subscribe(
            data => {
                this.spinner.hide();
                this.LetterOfInstructionList = data['result'];
                // var newletterList = [];
                // newletterList =  Array.from(new Set(this.LetterOfInstructionList ));
                // this.LetterOfInstructionList = newletterList;
                // console.log(this.LetterOfInstructionList);

        }, error => {
            console.log(error);
        });
    }
    approvalthreeListData() {
        console.log(this.LetterOfInstructionList);
        var approvaloneIds = [];
        for (var i = 0; i < this.LetterOfInstructionList.length; i++) {
            if (this.LetterOfInstructionList[i].f063fapprovedStatus == true) {
                approvaloneIds.push(this.LetterOfInstructionList[i].f063fid);
            }
        }
        var approvaloneUpdate = {};
        approvaloneUpdate['id'] = approvaloneIds;
        approvaloneUpdate['reason'] = this.LetterOfInstructionData['reason'];

        this.ApprovalthreeService.updateApprovalthreeItems(approvaloneUpdate).subscribe(
            data => {
                alert(" Application Approved ! ");

                this.getListData();
            }, error => {
                console.log(error);
            });
    }


    rejectthreeListData() {
        console.log(this.LetterOfInstructionList);
        var rejectoneIds = [];
        for (var i = 0; i < this.LetterOfInstructionList.length; i++) {
            if (this.LetterOfInstructionList[i].f063fapprovedStatus == true) {
                rejectoneIds.push(this.LetterOfInstructionList[i].f063fid);
            }
        }
       


console.log(this.LetterOfInstructionData['reason']);

        if (this.LetterOfInstructionData['reason'] == '' || this.LetterOfInstructionData['reason'] == undefined) {
            alert("Provide the reason for Rejection !");
        } else {

            var rejectoneUpdate = {};
            rejectoneUpdate['id'] = rejectoneIds;
            rejectoneUpdate['reason'] = this.LetterOfInstructionData['reason'];

            this.ApprovalthreeService.rejectApprovalthree(rejectoneUpdate).subscribe(
                data => {
                    alert(" Application Rejected ! ");

                    this.getListData();
                }, error => {
                    console.log(error);
                });

        }
    }

    selectAll() {
        console.log("asdf");
        console.log(this.selectAllCheckbox);
        for (var i = 0; i < this.LetterOfInstructionList.length; i++) {
                this.LetterOfInstructionList[i].f063fapprovedStatus = this.selectAllCheckbox;
        }
        console.log(this.LetterOfInstructionList);
      }
}
   
