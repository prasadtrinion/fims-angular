import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { NotificationService } from '../../service/notification.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
    selector: 'NotificationFormComponent',
    templateUrl: 'notificationform.component.html'
})

export class NotificationFormComponent implements OnInit {
    notificationform: FormGroup;
    notificationList = [];
    notificationData = {};
    roleidparentList = [];
    notificationDataHeader={};
    days = [];
    deleteList=[];
    id: number;
    idview: number;    
    ajaxCount: number;
    showLastRow: boolean;
    showLastRowMinus: boolean;
    showLastRowPlus: boolean;
    listshowLastRowMinus: boolean;
    listshowLastRowPlus: boolean;
    viewDisabled: boolean;
    constructor(

        private NotificationService: NotificationService,
        private route: ActivatedRoute,
        private router: Router
    ) { }
    ngDoCheck() {
        const change = this.ajaxCount;
        this.route.paramMap.subscribe((data) => this.idview = + data.get('idview'));
        if (this.ajaxCount == 0 && this.notificationList.length > 0) {
            this.ajaxCount = 20;
            console.log("asdfsdf");
            this.showIcons();
        }
        if (this.ajaxCount == 0) {
            this.editFunction();
            this.ajaxCount = 10;
        }
    }
    deletemodule() {


        this.showLastRow = true;
        this.showIcons();
    }
    showIcons() {
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.notificationList.length > 0) {
            this.showLastRowPlus = true;
            this.showLastRowMinus = true;
            this.listshowLastRowMinus = true;
        }

        if (this.notificationList.length > 1) {
            if (this.showLastRow == false) {
                this.listshowLastRowPlus = false;
                this.listshowLastRowMinus = true;
                this.showLastRowPlus = true;
            } else {
                this.listshowLastRowPlus = true;
                this.listshowLastRowMinus = false;

            }
        }
        if (this.notificationList.length == 1 && this.showLastRow == false) {
            this.listshowLastRowMinus = true;
            this.listshowLastRowPlus = false;

        }
        if (this.notificationList.length == 1 && this.showLastRow == true) {
            this.listshowLastRowMinus = false;
            this.listshowLastRowPlus = true;

        }
        if (this.notificationList.length == 0 && this.showLastRow == false) {
            this.showLastRowMinus = false;
            this.showLastRowPlus = true;
        }
        if (this.notificationList.length == 0 && this.showLastRow == true) {
            this.showLastRowMinus = true;
            this.showLastRowPlus = false;
        }
        console.log(this.notificationList.length);
        console.log("Show last row" + this.showLastRow);
        console.log("showLastRowMinus" + this.showLastRowMinus);
        console.log("showLastRowPlus" + this.showLastRowPlus);
        console.log("listshowLastRowMinus" + this.listshowLastRowMinus);
        console.log("listshowLastRowPlus" + this.listshowLastRowPlus);
        //  if(this.journalList.length>0){
        //     if(this.viewDisabled==true || this.journalList[0]['f017fapprovedStatus']==1 || this.journalList[0]['f017fapprovedStatus'] == 2) {
        //         this.listshowLastRowPlus = false;
        //     }
        // }
        // else{
        //     if(this.viewDisabled==true) {
        //         this.listshowLastRowPlus = false;
        //     }
        // }
    }
    showNewRow() {
        this.showLastRow = false;
        this.showIcons();
    }
    editFunction() {

        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {

            // alert("calld");
            this.NotificationService.getItemsDetail(this.id).subscribe(
                data => {
                    // this.investmentinstitutionData = data['result'][0];
                    if (data['result'].length == 0) {
                        // alert("Edit api not working ");
                        return false;
                    }
                    this.roleidparentList = data['result'];
                    this.notificationDataHeader['f061finstitutionCode'] = data['result'][0]['f061finstitutionCode'];
                    this.notificationDataHeader['f061fname'] = data['result'][0]['f061fname'];
                    this.notificationDataHeader['f061fbranch'] = data['result'][0]['f061fbranch'];
                
                    console.log(this.notificationDataHeader);
                    // this.showLastRow = false;
                    this.showIcons();

                }, error => {
                    console.log(error);
                });
        }
    }

    ngOnInit() {
        this.viewDisabled = false;

        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id < 1) {
            this.showLastRow = false;
            this.showLastRowMinus = false;
            this.showLastRowPlus = true;
        } else {
            this.showLastRow = true;

        }
        this.notificationData['f038fstatus'] = 1;

        this.notificationform = new FormGroup({
            notification: new FormControl('', Validators.required)
        });

        let dayobj = {};
        dayobj['name'] = 'Sunday';
        this.days.push(dayobj);
        dayobj = {};
        dayobj['name'] = 'Monday';
        this.days.push(dayobj);
        dayobj = {};
        dayobj['name'] = 'Tuesday';
        this.days.push(dayobj);
        dayobj = {};
        dayobj['name'] = 'Wednsday';
        this.days.push(dayobj);
        dayobj = {};
        dayobj['name'] = 'Thursday';
        this.days.push(dayobj);
        dayobj = {};
        dayobj['name'] = 'Friday';
        this.days.push(dayobj);
        dayobj = {};
        dayobj['name'] = 'Saturday';
        this.days.push(dayobj);
        


        if (this.id > 0) {
            this.NotificationService.getItemsDetail(this.id).subscribe(
                data => {
                    this.notificationData = data['result'][0];
                    this.notificationData['f038fstatus'] = parseInt(data['result'][0]['f038fstatus']);
                    this.notificationList = data['result']['data'];

                }, error => {
                    console.log(error);
                });
        }
        this.showIcons();       
    }

    addNotification() {
        console.log(this.notificationData);
        let investmentinstObject = {};

        if (this.notificationDataHeader['f061finstitutionCode'] == undefined) {
            alert('Enter Institution Code');
            return false;
        }
        var confirmPop = confirm("Do you want to Save?");

        if (confirmPop == false) {
            return false;
        }
        if (this.showLastRow == false) {
            var addactivities = this.addNotification();
            if (addactivities == false) {
                return false;
            }
        }
        
        investmentinstObject['f061finstitutionCode'] = this.notificationDataHeader['f061finstitutionCode'];

        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            investmentinstObject['f061fid'] = this.id;
        }
        
        if (this.id > 0) {
            this.NotificationService.updateNotificationItems(this.notificationData).subscribe(
                data => {

                    this.router.navigate(['investment/notification']);
                    alert("Type of Investment has been Updated Successfully")
                }, error => {
                    return false;
                });
        } else {
            this.NotificationService.insertNotificationItems(this.notificationData).subscribe(
                data => {
                    console.log(this.notificationData);
                    if (data['status'] == 409) {
                    }
                    else {
                        this.router.navigate(['investment/notification']);
                        alert("Type of Investment has been Added Successfully")
                    }

                }, error => {
                    console.log(error);
                });
        }
    }
    deleteNotificationList(object) {
        var cnf = confirm("Do you really want to delete");
        if (cnf == true) {
            console.log(this.notificationList);
            var index = this.notificationList.indexOf(object);
            this.deleteList.push(this.notificationList[index]['f063fidDetails']);
            let deleteObject = {};
            deleteObject['id'] = this.deleteList;
            this.NotificationService.deleteItems(deleteObject).subscribe(
                data => {
                }, error => {
                    console.log(error);
                });
            if (index > -1) {
                this.notificationList.splice(index, 1);
            }
            // this.onBlurMethod();
            this.showIcons();
        }
    }
    addNotifications() {
        console.log(this.notificationList);
        var confirmPop = confirm("Do you want to Add?");
        if (confirmPop == false) {
            return false;
        }

        // if (this.notificationData['email'] == undefined) {
        //     alert('Enter email');
        //     return false;
        // }

        var dataofCurrentRow = this.notificationData;
        this.notificationData = {};
        this.notificationList.push(dataofCurrentRow);
        this.showIcons();
        console.log(this.notificationList);
    }
    maximize() {
        $("#myModal").addClass("modal");
        $("#overlay1").show();
        $(".maximize-icon").addClass("hidden");
        $(".minimize-icon").removeClass("hidden");
        $(".modal-backdrop").show();

    }
    minimize() {
        $("#myModal").removeClass("modal");
        $("#overlay1").hide();
        $(".maximize-icon").removeClass("hidden");
        $(".minimize-icon").addClass("hidden");
        $(".modal-backdrop").hide();
        $('#myModal').show();

    }
}