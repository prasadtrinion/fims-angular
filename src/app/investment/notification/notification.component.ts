import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NotificationService } from '../service/notification.service'
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'Notification',
    templateUrl: 'notification.component.html'
})

export class NotificationComponent implements OnInit {
    notificationList = [];
    notificationData = {};

    constructor(
        
        private NotificationService: NotificationService,
        private spinner: NgxSpinnerService,
    ) { }

    ngOnInit() { 
        this.spinner.show();                                
        this.NotificationService.getItems().subscribe(
            data => {
                this.spinner.hide();
                this.notificationList = data['result']['data'];
        }, error => {
            console.log(error);
        });
    }
   
}