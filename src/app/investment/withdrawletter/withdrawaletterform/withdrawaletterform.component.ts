import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { InvestmentregistrationService } from '../../service/investmentregistration.service';
import { WithdrawletterService } from '../../service/Withdrawletter.service'
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Alert } from 'selenium-webdriver';

@Component({
    selector: 'withdrawaletterFormComponent',
    templateUrl: 'withdrawaletterform.component.html'
})

export class WithdrawFormComponent implements OnInit {
    investmentregistrationform: FormGroup;
    investmentregistrationList = [];
    investmentregistrationData = {};
    withdrawletterList = [];
    withdrawletterData = {};
    file;
    idview: number;
    RegObject = {};
    newRegistrationList = [];
    ajaxCount: number;
    id: number;
    valueDate: string;

    effectiveDate: Date;
    maturityDate: Date;
    Duration: number;


    constructor(

        private InvestmentregistrationService: InvestmentregistrationService,
        private WithdrawletterService: WithdrawletterService,
        private route: ActivatedRoute,
        private router: Router

    ) { }

    ngOnInit() {
        this.getListData();
        this.withdrawletterData['investRegId'] = '';
    }


    getListData() {
        this.WithdrawletterService.getWithdrawList().subscribe(
            data => {
                this.investmentregistrationList = data['data']['data'];
            }, error => {
                console.log(error);
            });
    }

    getInvestmentRegDetails() {

        let RegId = this.withdrawletterData['investRegId'];
        this.InvestmentregistrationService.getItemsDetail(RegId).subscribe(
            data => {
                this.withdrawletterList = data['result']['data'];

                for (var i = 0; i < this.withdrawletterList.length; i++) {

                    this.effectiveDate = new Date(this.withdrawletterList[i]['f064feffectiveDate']);
                    this.maturityDate = new Date(this.withdrawletterList[i]['f064fmaturityDate']);

                    var timeDiff = Math.abs(this.maturityDate.getTime() - this.effectiveDate.getTime());
                    var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
                    console.log(diffDays);
                    this.withdrawletterList[i]['Duration'] = diffDays;
                }
            }
        );
    }



    WithdrawListData() {
        var withdrawalIds = [];
        var confirmPop = confirm("Do you want to Withdraw Application?");
        if (confirmPop == false) {
            return false;
        }

        for (var i = 0; i < this.withdrawletterList.length; i++) {
            if (this.withdrawletterList[i].f064fstatuss == true) {
                withdrawalIds.push(this.withdrawletterList[i].f064fid);
            }
        }

        if (withdrawalIds.length < 1) {
            alert("Please select atleast one Application to Withdraw");
            return false;
        }

        var withdrawalUpdate = {};
        withdrawalUpdate['id'] = withdrawalIds;
        withdrawalUpdate['status'] = 1;
        withdrawalUpdate['reason'] = this.withdrawletterData['reason'];

        if (this.withdrawletterData['reason'] == '' || this.withdrawletterData['reason'] == undefined) {
            alert("Enter the Description to Withdraw Application !");
        } else {

            this.WithdrawletterService.updatewithdrawalItems(withdrawalUpdate).subscribe(
                data => {

                    alert(" Withdraw Application Sucessfull ! ");
                    this.router.navigate(['investment/withdrawletter']);

                    this.withdrawletterData['reason'] = '';
                }, error => {
                    console.log(error);
                });
        }
    }


}