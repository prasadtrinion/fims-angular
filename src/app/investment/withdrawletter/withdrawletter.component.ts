import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { WithdrawletterService } from '../service/Withdrawletter.service'
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'WithdrawletterComponent',
    templateUrl: 'withdrawletter.component.html'
})

export class WithdrawletterComponent implements OnInit {
    intakeform: FormGroup;

    withdrawletterList = [];
    withdrawletterData = {};

    constructor(

        private WithdrawletterService: WithdrawletterService,
        private spinner: NgxSpinnerService

    ) { }

    ngOnInit() {
        this.getListData();
    }

    getListData(){
        this.spinner.show();
        this.WithdrawletterService.getItems().subscribe(
            data => {
                this.spinner.hide();
                this.withdrawletterList = data['result']['data'];
                for(var i=0;i<this.withdrawletterList.length;i++) {
                    let status ='';
                     if(this.withdrawletterList[i]['f064ffinalStatus']==0) {
                          status = 'Entry';

                     } else  if(this.withdrawletterList[i]['f064ffinalStatus']==1) {
                          status = 'Approved';
                    } else  if(this.withdrawletterList[i]['f064ffinalStatus']==2) {
                         status = 'Rejected';
                    }
                    this.withdrawletterList[i]['finalStatus'] = status;
                }

            }, error => {
                console.log(error);
            });
    }
}