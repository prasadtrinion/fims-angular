import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { TypeofinvestmentService } from '../service/typeofinvestment.service'
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'typeofinvestment',
    templateUrl: 'typeofinvestment.component.html'
})

export class TypeofinvestmentComponent implements OnInit {
    typeofinvestmentList = [];
    typeofinvestmentData = {};

    constructor(
        
        private TypeofinvestmentService: TypeofinvestmentService,
        private spinner: NgxSpinnerService,
    ) { }

    ngOnInit() { 
        this.spinner.show();                                
        this.TypeofinvestmentService.getItems().subscribe(
            data => {
                this.spinner.hide();
                this.typeofinvestmentList = data['result']['data'];
        }, error => {
            console.log(error);
        });
    }
   
}