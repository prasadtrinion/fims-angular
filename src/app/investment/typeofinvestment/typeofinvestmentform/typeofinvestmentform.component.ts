import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { TypeofinvestmentService } from '../../service/typeofinvestment.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AccountcodeService } from '../../../generalsetup/service/accountcode.service'
import { ActivitycodeService } from '../../../generalsetup/service/activitycode.service';
import { DepartmentService } from '../../../generalsetup/service/department.service'
import { FundService } from '../../../generalsetup/service/fund.service';
@Component({
    selector: 'TypeofinvestmentFormComponent',
    templateUrl: 'typeofinvestmentform.component.html'
})

export class TypeofinvestmentFormComponent implements OnInit {
    typeofinvestmentform: FormGroup;
    typeofinvestmentList = [];
    typeofinvestmentData = {};
    roleidparentList = [];
    accountcodeList = [];
    invtype = [];
    id: number;
    activitycodeList=[];
    DeptList=[];
    fundList=[];
    ajaxCount: number;
    constructor(

        private TypeofinvestmentService: TypeofinvestmentService,
        private AccountcodeService: AccountcodeService,
        private DepartmentService: DepartmentService,
        private ActivitycodeService:ActivitycodeService,
        private FundService: FundService,
        private route: ActivatedRoute,
        private router: Router
    ) { }

    ngOnInit() {
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        this.typeofinvestmentData['f038fstatus'] = 1;

        this.typeofinvestmentform = new FormGroup({
            typeofinvestment: new FormControl('', Validators.required)
        });

        let invtypeobj = {};
        invtypeobj['name'] = 'Fixed Deposit';
        this.invtype.push(invtypeobj);
        invtypeobj = {};
        invtypeobj['name'] = 'Mudharabah';
        this.invtype.push(invtypeobj);

        //Activity dropdown
        this.ajaxCount++;
        this.ActivitycodeService.getItemsByLevel().subscribe(
            data => {
                this.ajaxCount--;
                this.activitycodeList = data['result']['data'];
            }
        );
        this.ajaxCount = 0;
        this.ajaxCount++;
        this.DepartmentService.getZerodepartment().subscribe(
            data => {
                this.ajaxCount--;
                this.DeptList = data['result']['data'];
            }
        );
           //Fund dropdown
           this.ajaxCount++;
           this.FundService.getActiveItems().subscribe(
               data => {
                   this.ajaxCount--;
                   this.fundList = data['result']['data'];
               }
           );

        //Account dropdown
        // this.ajaxCount++;
        // this.TypeofinvestmentService.getDebotorAccountcode().subscribe(
        //     data => {
        //         this.ajaxCount--;
        //         this.accountcodeList = data['data'];

        //         console.log(this.accountcodeList);
        //     }
        // );
        //Account dropdown
        this.ajaxCount++;
        this.AccountcodeService.getItemsByLevel().subscribe(
            data => {
                this.ajaxCount--;
                this.accountcodeList = data['result']['data'];
            }
        );

        if (this.id > 0) {
            this.TypeofinvestmentService.getItemsDetail(this.id).subscribe(
                data => {
                    this.typeofinvestmentData = data['result'][0];
                    this.typeofinvestmentData['f038fstatus'] = parseInt(data['result'][0]['f038fstatus']);
                    this.typeofinvestmentList = data['result']['data'];

                }, error => {
                    console.log(error);
                });
        }
        console.log(this.id);
    }
    keyPress(event: any) {
        const pattern = /[0-9\+\ ]/;

        let inputChar = String.fromCharCode(event.charCode);
        if (event.keyCode != 8 && !pattern.test(inputChar)) {
            event.preventDefault();
        }
    }

    addTypeofinvestment() {
        console.log(this.typeofinvestmentData);

        var confirmPop = confirm("Do you want to Save?");

        if (confirmPop == false) {
            return false;
        }

        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        if (this.id > 0) {
            this.TypeofinvestmentService.updateTypeofinvestmentItems(this.typeofinvestmentData, this.id).subscribe(
                data => {

                    this.router.navigate(['investment/typeofinvestment']);
                    alert("Type of Investment has been Updated Successfully")

                }, error => {
                    // alert('Type of Investment Already Exist');
                    return false;
                });
        } else {
            this.TypeofinvestmentService.insertTypeofinvestmentItems(this.typeofinvestmentData).subscribe(
                data => {
                    console.log(this.typeofinvestmentData);
                    if (data['status'] == 409) {
                             alert('Type of Investment Already Exist');
                    }
                    else {
                        this.router.navigate(['investment/typeofinvestment']);
                        alert("Type of Investment has been Added Successfully")

                    }

                }, error => {
                    console.log(error);
                });

        }


    }
}