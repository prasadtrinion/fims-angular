import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { LetterofintitutionService } from '../service/letterofinstitution.service'
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { OnlineapprovalthreeService } from "../service/onlineapprovalthree.service";
import { NgxSpinnerService } from 'ngx-spinner';
import { environment } from '../../../environments/environment';

@Component({
    selector: 'LetterofinstitutionComponent',
    templateUrl: 'letterofinstitution.component.html'
})

export class LetterofinstitutionComponent implements OnInit {
    intakeform: FormGroup;

    letterList = [];
    letterData = {};;
    letterDataheader = {};
    downloadUrl: string = environment.api.downloadbase;
    ajaxCount: number;

    constructor(
        
        private LetterofintitutionService: LetterofintitutionService,
        private OnlineapprovalthreeService: OnlineapprovalthreeService,
        private spinner: NgxSpinnerService,


    ) { }

    ngOnInit() { 
        this.spinner.show();                                
        
        this.LetterofintitutionService.getItems().subscribe(
            data => {
                this.spinner.hide();

                this.letterList = data['result'];

        }, error => {
            console.log(error);
        });
    }

    downloadData(id) {
        var approvaloneUpdate = {};
        approvaloneUpdate['applicationId'] = id;
        this.OnlineapprovalthreeService.downloadDataprint(approvaloneUpdate).subscribe(
            data => {
                window.open(this.downloadUrl + data['name']);
                // window.open('http://uum-api.mtcsb.my/download/'+data['name']);
            }, error => {
                console.log(error);
            });
    }

    addLetterofintitution(){
           console.log(this.letterData);
        this.LetterofintitutionService.insertLetterofintitutionItems(this.letterData).subscribe(
            data => {
                this.letterList = data['todos'];
            }, error => {
                console.log(error);
            });

    }
}