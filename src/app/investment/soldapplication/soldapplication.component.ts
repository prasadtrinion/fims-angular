import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SoldapplicationService } from './../service/soldapplication.service'

@Component({
    selector: 'Soldapplication',
    templateUrl: 'soldapplication.component.html'
})

export class SoldapplicationComponent implements OnInit {
    soldapplicationList = [];
    soldapplicationData = {};
    id: number;

    constructor(

        private SoldapplicationService: SoldapplicationService,
    ) { }

    ngOnInit() {

        this.getListData();
    }

    getListData() {
        this.SoldapplicationService.getItems().subscribe(
            data => {
                this.soldapplicationList = data['data']['data'];
            }, error => {
                console.log(error);
            });
    }
    soldapplicationListData() {
        console.log(this.soldapplicationList);
        var rejectoneIds = [];
        for (var i = 0; i < this.soldapplicationList.length; i++) {
            if (this.soldapplicationList[i].f063fstatus == true) {
                rejectoneIds.push(this.soldapplicationList[i].f063fid);
            }
        }
        var soldUpdate = {};
        soldUpdate['id'] = rejectoneIds;
        soldUpdate['reason'] = this.soldapplicationData['f063fapprove2Reason'];
        console.log(soldUpdate);
        this.SoldapplicationService.insertsoldapplication(soldUpdate).subscribe(
            data => {
                this.getListData();
            }, error => {
                console.log(error);
            });
    }
}