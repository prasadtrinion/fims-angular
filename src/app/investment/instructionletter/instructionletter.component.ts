import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { ApplicationletterService } from '../service/applicationletter.service'
import { ApprovalthreeService } from '../service/approvalthree.service';
import { InstructionletterService } from '../service/instructionletter.service';
import { AlertService } from './../../_services/alert.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { environment } from '../../../environments/environment';

@Component({
    selector: 'InstructionletterComponent',
    templateUrl: 'instructionletter.component.html'
})

export class InstructionletterComponent implements OnInit {
    rejecttwoList = [];
    LetterOfInstructionList = [];
    LetterOfInstructionData = {};
    selectAllCheckbox = true;
    downloadUrl: string = environment.api.downloadbase;

    constructor(

        private ApprovalthreeService: ApprovalthreeService,
        private AlertService: AlertService,
        private InstructionletterService: InstructionletterService,
        private spinner: NgxSpinnerService,
        private ApplicationletterService: ApplicationletterService
    ) { }

    ngOnInit() {
        this.getListData();


    }
    downloadData(id) {
        var instructionletter = {};
        instructionletter['applicationId'] = id;
        console.log("asdfsad")
        console.log(id);
        this.ApplicationletterService.generateVoucher(id).subscribe(
            data => {

                console.log("asdfs");
            }, error => {
                console.log(error);
            });

        this.InstructionletterService.downloadDataprint(instructionletter).subscribe(
            data => {
                window.open(this.downloadUrl + data['name']);
            }, error => {
                console.log(error);
            });
    }
    getListData() {
        this.spinner.show();
        this.InstructionletterService.getItems().subscribe(
            data => {
                this.spinner.hide();
                this.LetterOfInstructionList = data['result'];
                // var newletterList = [];
                // newletterList =  Array.from(new Set(this.LetterOfInstructionList ));
                // this.LetterOfInstructionList = newletterList;
                // console.log(this.LetterOfInstructionList);

            }, error => {
                console.log(error);
            });
    }


    approvalthreeListData() {
        console.log(this.LetterOfInstructionList);
        var approvaloneIds = [];
        for (var i = 0; i < this.LetterOfInstructionList.length; i++) {
            if (this.LetterOfInstructionList[i].f063fapprovedStatus == true) {
                approvaloneIds.push(this.LetterOfInstructionList[i].f063fid);
            }
        }
        var approvaloneUpdate = {};
        approvaloneUpdate['id'] = approvaloneIds;
        approvaloneUpdate['reason'] = this.LetterOfInstructionData['reason'];

        this.ApprovalthreeService.updateApprovalthreeItems(approvaloneUpdate).subscribe(
            data => {
                alert(" Application Approved ! ");

                this.getListData();
            }, error => {
                console.log(error);
            });
    }


    rejectthreeListData() {
        console.log(this.LetterOfInstructionList);
        var rejectoneIds = [];
        for (var i = 0; i < this.LetterOfInstructionList.length; i++) {
            if (this.LetterOfInstructionList[i].f063fapprovedStatus == true) {
                rejectoneIds.push(this.LetterOfInstructionList[i].f063fid);
            }
        }



        console.log(this.LetterOfInstructionData['reason']);

        if (this.LetterOfInstructionData['reason'] == '' || this.LetterOfInstructionData['reason'] == undefined) {
            alert("Provide the reason for Rejection !");
        } else {

            var rejectoneUpdate = {};
            rejectoneUpdate['id'] = rejectoneIds;
            rejectoneUpdate['reason'] = this.LetterOfInstructionData['reason'];

            this.ApprovalthreeService.rejectApprovalthree(rejectoneUpdate).subscribe(
                data => {
                    alert(" Application Rejected ! ");

                    this.getListData();
                }, error => {
                    console.log(error);
                });

        }
    }
}

