import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { ApprovaloneService } from '../../service/approvalone.service'
import { ActivatedRoute, Router } from '@angular/router';
import { InvestmentapplicationService } from '../../service/investmentapplication.service'

import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'Approvaloneform',
    templateUrl: 'approvaloneform.component.html'
})

export class ApprovaloneformComponent implements OnInit {
    rejectoneList = [];
    approvalformList = [];
    approvalformData = {};
    selectAllCheckbox = true;

    constructor(
        private ApprovaloneService: ApprovaloneService,
        private spinner: NgxSpinnerService,
        private InvestmentapplicationService:InvestmentapplicationService

    ) { }

    ngOnInit() {
        this.getListData();
    }
    getListData() {
        this.spinner.show();                                
        this.InvestmentapplicationService.getAllItems().subscribe(
            data => {
                this.spinner.hide();
                this.approvalformList = data['result'];
                
            }
        );
    }

    approvalformListData() {     

        var confirmPop = confirm("Do you want to Approve ?");

        if (confirmPop == false) {
            return false;
        }
        console.log(this.approvalformList);
        var approvaloneIds = [];
        var approvalrenewoneIds = [];

        for (var i = 0; i < this.approvalformList.length; i++) {
            if (this.approvalformList[i].f063fapproved1Status == true) {
                approvaloneIds.push(this.approvalformList[i].f063fidDetails);
            }
            if (this.approvalformList[i].autoRenewal == true) {
                approvalrenewoneIds.push(this.approvalformList[i].f063fidDetails);
            }
           
        }
        if (approvaloneIds.length < 1) {
            alert("Select atleast one Application to Approve");
            return false;
        }

        var approvaloneUpdate = {};
        approvaloneUpdate['id'] = approvaloneIds;
        approvaloneUpdate['autoRenewal'] = approvalrenewoneIds;
        approvaloneUpdate['status'] = 1;
        approvaloneUpdate['level'] = 1;
        approvaloneUpdate['reason'] = this.approvalformData['reason'];


        console.log(approvaloneUpdate);


        if (this.approvalformData['check'] == '' || this.approvalformData['check'] == undefined) {
            alert("Select Declartaion To approve !");
        } else {
        this.ApprovaloneService.updateApprovaloneItems(approvaloneUpdate).subscribe(
            data => {
                alert(" Application Approved ! ");

                this.getListData();
                this.approvalformData['check'] = '';

            }, error => {
                console.log(error);
            });
        }
    }


    rejectformListData() {

        var confirmPop = confirm("Do you want to Reject ?");

        if (confirmPop == false) {
            return false;
        }
       

        console.log(this.approvalformList);
        var rejectoneIds = [];
        for (var i = 0; i < this.approvalformList.length; i++) {
            if (this.approvalformList[i].f063fapproved1Status == true) {
                rejectoneIds.push(this.approvalformList[i].f063fidDetails);
            }
        }

        if (rejectoneIds.length < 1) {
            alert("Select atleast one Application to Reject");
            return false;
        }

        var rejectoneUpdate = {};
            rejectoneUpdate['id'] = rejectoneIds;
            rejectoneUpdate['status'] = 2;
            rejectoneUpdate['level'] = 1;
            rejectoneUpdate['reason'] = this.approvalformData['reason'];


        console.log(this.approvalformData['reason']);

        if (this.approvalformData['reason'] == '' || this.approvalformData['reason'] == undefined) {
            // alert("Enter the Description to Reject !");
        } else {

        this.ApprovaloneService.rejectApprovalone(rejectoneUpdate).subscribe(
                data => {
                    alert(" Application Rejected ! ");

                    this.getListData();
                    this.approvalformData['reason'] = '';

                }, error => {
                    console.log(error);
                });
        }
    }
}
