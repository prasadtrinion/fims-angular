import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { ApplicationletterService } from '../service/applicationletter.service'
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { OnlineapprovalthreeService } from "../service/onlineapprovalthree.service";
import { NgxSpinnerService } from 'ngx-spinner';
import { environment } from '../../../environments/environment';

@Component({
    selector: 'ApplicationletterComponent',
    templateUrl: 'applicationletter.component.html'
})

export class ApplicationletterComponent implements OnInit {
    intakeform: FormGroup;

    applicationletterList = [];
    applicationletterData = {};;
    letterDataheader = {};
    downloadUrl: string = environment.api.downloadbase;
    id: number;
    ajaxCount: number;
    constructor(
        
        private ApplicationletterService: ApplicationletterService,
        private OnlineapprovalthreeService: OnlineapprovalthreeService,
        private spinner: NgxSpinnerService,


    ) { }

    ngOnInit() { 
        this.spinner.show();                                
        
        this.ApplicationletterService.getItems().subscribe(
            data => {
                this.spinner.hide();
                this.applicationletterList = data['result'];

        }, error => {
            console.log(error);
        });
    }

    downloadData(id) {
        var approvaloneUpdate = {};
        approvaloneUpdate['applicationId'] = id;   
        this.OnlineapprovalthreeService.downloadDataprint(approvaloneUpdate).subscribe(
            data => {
                 
                window.open(this.downloadUrl + data['name']);
            }, error => {
                console.log(error);
            });
           
    }
    // addPrint(id) {
    //     this.route.paramMap.subscribe((data) => this.id);
    //     let newobj = {};
    //     newobj['id'] = id;
    //     newobj['type']=2;
    //     var confirmPop = confirm("Do you want to Re-Print?");
    //     if (confirmPop == false) {
    //         return false;
    //     } this.PoReportapprovalService.insertPoPrintItems(newobj).subscribe(
    //         data => {
    //             // alert('KWAP has been saved');
    //             window.open(this.downloadUrl + data['name']);
    //             this.router.navigate(['procurement/poreprint']);

    //         }, error => {
    //             console.log(error);
    //         });
    // }
    // addapplicationletter(){
    //        console.log(this.applicationletterData);
    //     this.ApplicationletterService.insertApplicationletterItems(this.applicationletterData).subscribe(
    //         data => {
    //             this.applicationletterList = data['todos'];
    //         }, error => {
    //             console.log(error);
    //         });

    // }
}