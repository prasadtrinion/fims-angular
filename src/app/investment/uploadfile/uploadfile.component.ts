import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { UploadfileService } from '../service/uploadfile.service'
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
    selector: 'UploadfileComponent',
    templateUrl: 'uploadfile.component.html'
})

export class UploadfileComponent implements OnInit {
    ptptnform: FormGroup;
    ptptnList = [];
    uploadData = {};
    uploadList = [];
    file: any;
    // semsequenceList=[];


    id: number;

    constructor(
        private UploadfileService: UploadfileService,
        private route: ActivatedRoute,
        private router: Router

    ) { }

    ngOnInit() {
        this.uploadData['f053fstatus'] = 1;
        // this.semesterData['f066fidFeeCategory']='';


        this.ptptnform = new FormGroup({
            ptptn: new FormControl('', Validators.required)
        });
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        console.log(this.id);

        if (this.id > 0) {
            this.UploadfileService.downloadDataprint(this.id).subscribe(
                data => {
                    this.uploadData = data['result'][0];

                    // converting from string to int for radio button default selection
                    this.uploadData['f053fstatus'] = parseInt(data['result'][0]['f053fstatus']);

                    console.log(this.uploadData);
                }, error => {
                    console.log(error);
                });
        }


        console.log(this.id);
    }
    fileSelected(event) {

        this.file = event.target.files[0];
        console.log(this.file);
    }
    addUploadfile() {
        let fd = new FormData();
        fd.append('file', this.file);
        this.UploadfileService.downloadDataprint(this.uploadData).subscribe(
            data => {
                this.uploadData['file'] = data['name'];

                this.router.navigate(['investment/uploadfile']);
                alert("File Uploaded Successfully");
            }, error => {
                console.log(error);
            });
    }
}