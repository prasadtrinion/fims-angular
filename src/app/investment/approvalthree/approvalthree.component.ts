import {Injector} from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { ApprovalthreeService } from '../service/approvalthree.service'
import { AlertService } from './../../_services/alert.service';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'Approvalthree',
    templateUrl: 'approvalthree.component.html'
})

export class ApprovalthreeComponent implements OnInit {
    rejecttwoList=[];
    approvalthreeList =  [];
    approvalthreeData = {};
    selectAllCheckbox = true;
    id:number;

    constructor(
        
        private ApprovalthreeService: ApprovalthreeService,
        private AlertService: AlertService,
        private spinner: NgxSpinnerService,
        private route: ActivatedRoute,
        private router: Router
    ) { }

    ngOnInit() {    
       this.getListData();
    }
    // getListData() {
    //     this.spinner.show();
    //     this.ApprovalthreeService.getItemsDetail(0).subscribe(
    //         data => {
    //             this.spinner.hide();
    //             this.approvalthreeList = data['result'];
    //         }, error => {
    //             console.log(error);
    //         });
    // }
    getListData() {
        this.spinner.show();    
        var listArray = []                            
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        this.ApprovalthreeService.getItemsDetail(0).subscribe(
            data => {
                this.spinner.hide();
                listArray = data['result'];
                console.log(listArray);
                for(var i=0;i<listArray.length;i++) {
                    if(listArray[i]['f063fid']==this.id) {
                        this.approvalthreeList.push(listArray[i])
                    }
                }

                
            }, error => {
                console.log(error);
            });
    }


    approvalthreeListData() {

        var confirmPop = confirm("Do you want to Approve ?");

        if (confirmPop == false) {
            return false;
        }

        console.log(this.approvalthreeList);
        var approvalthreeIds = [];
        var approvalrenewthreeIds = [];

        for (var i = 0; i < this.approvalthreeList.length; i++) {
            if (this.approvalthreeList[i].f063fapproved3Status == true) {
                approvalthreeIds.push(this.approvalthreeList[i].f063fidDetails);
            }
            if (this.approvalthreeList[i].autoRenewal == true) {
                approvalrenewthreeIds.push(this.approvalthreeList[i].f063fidDetails);
            }
        }

        if (approvalthreeIds.length < 1) {
            alert("Select atleast one Application to Approve");
            return false;
        }

        var approvalthreeUpdate = {};
        approvalthreeUpdate['id'] = approvalthreeIds;
        approvalthreeUpdate['autoRenewal'] = approvalrenewthreeIds;
        approvalthreeUpdate['status'] = 1;
        approvalthreeUpdate['level'] = 3;
        approvalthreeUpdate['reason'] = this.approvalthreeData['reason'];
        
        if (this.approvalthreeData['check'] == '' || this.approvalthreeData['check'] == undefined) {
            alert("Select Declartaion To approve !");
        } else {
        this.ApprovalthreeService.updateApprovalthreeItems(approvalthreeUpdate).subscribe(
            data => {
                alert(" Application Approved ! ");

                this.router.navigate(['investment/approvalthreeform']);

            }, error => {
                console.log(error);
            });
        }
    }


    rejectthreeListData() {

        var confirmPop = confirm("Do you want to Reject ?");

        if (confirmPop == false) {
            return false;
        }

        console.log(this.approvalthreeList);
        var rejectthreeIds = [];
        for (var i = 0; i < this.approvalthreeList.length; i++) {
            if (this.approvalthreeList[i].f063fapproved3Status == true) {
                rejectthreeIds.push(this.approvalthreeList[i].f063fidDetails);
            }
        }

        if (rejectthreeIds.length < 1) {
            alert("Select atleast one Application to Reject");
            return false;
        }

        var rejectthreeUpdate = {};
        rejectthreeUpdate['id'] = rejectthreeIds;
        rejectthreeUpdate['status'] = 2;
        rejectthreeUpdate['level'] = 3;
        rejectthreeUpdate['reason'] = this.approvalthreeData['reason'];


        console.log(this.approvalthreeData['reason']);

        if (this.approvalthreeData['reason'] == '' || this.approvalthreeData['reason'] == undefined) {
            alert("Enter the Description to Reject  !");
        } else {

            
            this.ApprovalthreeService.rejectApprovalthree(rejectthreeUpdate).subscribe(
                data => {
                    alert(" Application Rejected ! ");

                    this.getListData();
                    this.approvalthreeData['reason'] = '';

                }, error => {
                    console.log(error);
                });

        }
    }

    
}
   
