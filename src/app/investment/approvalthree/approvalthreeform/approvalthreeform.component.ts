import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { ApprovaloneService } from '../../service/approvalone.service'
import { ActivatedRoute, Router } from '@angular/router';
import { ApprovaltwoService } from '../../service/approvaltwo.service'

import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'ApprovalthreeformComponent',
    templateUrl: 'approvalthreeform.component.html'
})

export class ApprovalthreeformComponent implements OnInit {
    rejectoneList = [];
    approvalthreeformList = [];
    approvalthreeformData = {};
    selectAllCheckbox = true;

    constructor(
        private ApprovaloneService: ApprovaloneService,
        private ApprovaltwoService:ApprovaltwoService,
        private spinner: NgxSpinnerService,
        private route: ActivatedRoute,
        private router: Router

    ) { }

    ngOnInit() {
        this.getListData();
    }
    getListData() {
        this.spinner.show();                                
        this.ApprovaltwoService.getItems().subscribe(
            data => {
                this.spinner.hide();

                this.approvalthreeformList = data['result'];
            }, error => {
                console.log(error);
            });
    }


    approvalformthreeListData() {     

        var confirmPop = confirm("Do you want to Approve ?");

        if (confirmPop == false) {
            return false;
        }
        console.log(this.approvalthreeformList);
        var approvaloneIds = [];
        var approvalrenewoneIds = [];

        for (var i = 0; i < this.approvalthreeformList.length; i++) {
            if (this.approvalthreeformList[i].f063fapproved1Status == true) {
                approvaloneIds.push(this.approvalthreeformList[i].f063fidDetails);
            }
            if (this.approvalthreeformList[i].autoRenewal == true) {
                approvalrenewoneIds.push(this.approvalthreeformList[i].f063fidDetails);
            }
           
        }
        if (approvaloneIds.length < 1) {
            alert("Select atleast one Application to Approve");
            return false;
        }

        var approvaloneUpdate = {};
        approvaloneUpdate['id'] = approvaloneIds;
        approvaloneUpdate['autoRenewal'] = approvalrenewoneIds;
        approvaloneUpdate['status'] = 1;
        approvaloneUpdate['level'] = 1;
        approvaloneUpdate['reason'] = this.approvalthreeformData['reason'];


        console.log(approvaloneUpdate);


        if (this.approvalthreeformData['check'] == '' || this.approvalthreeformData['check'] == undefined) {
            alert("Select Declartaion To approve !");
        } else {
        this.ApprovaloneService.updateApprovaloneItems(approvaloneUpdate).subscribe(
            data => {
                alert(" Application Approved ! ");

                this.getListData();
                this.approvalthreeformData['check'] = '';

            }, error => {
                console.log(error);
            });
        }
    }


    rejectthreeformListData() {

        var confirmPop = confirm("Do you want to Reject ?");

        if (confirmPop == false) {
            return false;
        }
       

        console.log(this.approvalthreeformList);
        var rejectoneIds = [];
        for (var i = 0; i < this.approvalthreeformList.length; i++) {
            if (this.approvalthreeformList[i].f063fapproved1Status == true) {
                rejectoneIds.push(this.approvalthreeformList[i].f063fidDetails);
            }
        }

        if (rejectoneIds.length < 1) {
            alert("Select atleast one Application to Reject");
            return false;
        }

        var rejectoneUpdate = {};
            rejectoneUpdate['id'] = rejectoneIds;
            rejectoneUpdate['status'] = 2;
            rejectoneUpdate['level'] = 1;
            rejectoneUpdate['reason'] = this.approvalthreeformData['reason'];


        console.log(this.approvalthreeformData['reason']);

        if (this.approvalthreeformData['reason'] == '' || this.approvalthreeformData['reason'] == undefined) {
            // alert("Enter the Description to Reject !");
        } else {

        this.ApprovaloneService.rejectApprovalone(rejectoneUpdate).subscribe(
                data => {
                    alert(" Application Rejected ! ");

                    this.getListData();
                    this.approvalthreeformData['reason'] = '';

                }, error => {
                    console.log(error);
                });
        }
    }
}
