import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { InvestmentapplicationService } from '../../service/investmentapplication.service'
import { InvestmentinstitutionService } from '../../service/investmentinstitution.service'
import { TypeofinvestmentService } from '../../service/typeofinvestment.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { BanksetupaccountService } from '../../../generalsetup/service/banksetupaccount.service';
import { ActivatedRoute, Router } from '@angular/router';
import { BankService } from '../../../generalsetup/service/bank.service';
import { HttpClientModule, HttpHeaders, HttpClient } from '@angular/common/http';
import { environment } from '../../../../environments/environment';

@Component({
    selector: 'InvestmentapplicationFormComponent',
    templateUrl: 'investmentapplicationform.component.html'
})
export class InvestmentapplicationFormComponent implements OnInit {
    investmentapplicationform: FormGroup;
    investmentappDataheader = {};
    investmentapplicationList = [];
    durationtype = [];
    banksetupaccountList = [];
    investmentapplicationData = {};
    typeofinstitutionList = [];
    investmentinstitutionData = {};
    investmentinstitutionList = [];
    investmentTypeList = [];
    InvestmentTypeByInstituteId = [];
    bankList = [];
    branchList=[];
    invtype = [];
    deleteList = [];
    InvestmentTypeList = [];
    id: number;
    BatchNumberShow: boolean;
    idview: number;
    valueDate: string;
    ajaxCount: number;
    gstValue: number;
    Amount: number;
    type: string;
    typeList = [];
    file;
    saveBtnDisable: boolean;
    viewDisabled: boolean;
    editDisabled: boolean;
    showLastRow: boolean;
    showLastRowMinus: boolean;
    showLastRowPlus: boolean;
    listshowLastRowMinus: boolean;
    listshowLastRowPlus: boolean;
    downloadUrl: string = environment.api.downloadbase;
    edit: boolean = false;
    

    constructor(
        private InvestmentapplicationService: InvestmentapplicationService,
        private InvestmentinstitutionService: InvestmentinstitutionService,
        private TypeofinvestmentService: TypeofinvestmentService,
        private BanksetupaccountService: BanksetupaccountService,
        private route: ActivatedRoute,
        private BankService: BankService,
        private router: Router,
        private httpClient: HttpClient,
        private modalService: NgbModal


    ) { }

    ngDoCheck() {
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.route.paramMap.subscribe((data) => this.idview = + data.get('idview'));
        const change = this.ajaxCount;
        if (this.ajaxCount == 0 && this.investmentapplicationList.length > 0) {
            this.ajaxCount = 20;
            this.showIcons();
            if (this.investmentapplicationList[0]['f063fapproved1Status'] == 2 || this.investmentapplicationList[0]['f063fapproved2Status'] == 3 || this.investmentapplicationList[0]['f063fapproved3Status'] == 4) {
                this.saveBtnDisable = true;
                this.viewDisabled = true;
                this.editDisabled = true;
                $("#target input,select").prop("disabled", true);
                $("#target1 input,select").prop("disabled", true);
            }
        }
        if (this.ajaxCount == 0 && this.investmentapplicationList.length > 0) {
            this.ajaxCount = 20;
            // console.log("asdfsdf");
            this.showIcons();
            if (this.idview > 1) {
                this.saveBtnDisable = true;
                this.viewDisabled = true;
                this.listshowLastRowPlus = false;
                this.editDisabled = true;
                $("#target input,select").prop("disabled", true);
                $("#target1 input,select").prop("disabled", true);
            }
        }
        if (this.ajaxCount == 0) {
            this.editFunction();
            this.ajaxCount = 10;
        }
    }

    deletemodule() {

        this.showLastRow = true;
        this.showIcons();
    }

    showIcons() {
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.route.paramMap.subscribe((data) => this.idview = + data.get('idview'));


        console.log(this.investmentapplicationList.length);
        console.log("Show last row" + this.showLastRow);
        console.log("showLastRowMinus" + this.showLastRowMinus);
        console.log("showLastRowPlus" + this.showLastRowPlus);
        console.log("listshowLastRowMinus" + this.listshowLastRowMinus);
        console.log("listshowLastRowPlus" + this.listshowLastRowPlus);

        if (this.investmentapplicationList.length > 0) {
            this.showLastRowPlus = true;
            this.showLastRowMinus = true;
            this.listshowLastRowMinus = true;
        }

        if (this.investmentapplicationList.length > 1) {
            if (this.showLastRow == false) {
                this.listshowLastRowPlus = false;
                this.listshowLastRowMinus = true;
                this.showLastRowPlus = true;

            } else {
                this.listshowLastRowPlus = true;
                this.listshowLastRowMinus = false;

            }
        }

        if (this.investmentapplicationList.length == 1 && this.showLastRow == false) {
            this.listshowLastRowMinus = true;
            this.listshowLastRowPlus = false;

        }
        if (this.investmentapplicationList.length == 1 && this.showLastRow == true) {
            this.listshowLastRowMinus = false;
            this.listshowLastRowPlus = true;

        }
        if (this.investmentapplicationList.length == 0 && this.showLastRow == false) {
            this.showLastRowMinus = false;
            this.showLastRowPlus = true;
        }
        if (this.investmentapplicationList.length == 0 && this.showLastRow == true) {
            this.showLastRowMinus = true;
            this.showLastRowPlus = false;
        }
        if (this.investmentapplicationList.length > 0) {
            if (this.viewDisabled == true || this.investmentapplicationList[0]['f063fapproved1Status'] == 2 || this.investmentapplicationList[0]['f063fapproved2Status'] == 3 || this.investmentapplicationList[0]['f063fapproved3Status'] == 4) {
                this.listshowLastRowPlus = false;
            }
        }
        else {
            if (this.viewDisabled == true) {
                this.listshowLastRowPlus = false;
            }
        }

        //console.log(this.salaryList);
        console.log("Show last row" + this.showLastRow);
        console.log("showLastRowMinus" + this.showLastRowMinus);
        console.log("showLastRowPlus" + this.showLastRowPlus);
        console.log("listshowLastRowMinus" + this.listshowLastRowMinus);
        console.log("listshowLastRowPlus" + this.listshowLastRowPlus);
        console.log("list view disabled" + this.viewDisabled);

    }

    showNewRow() {
        var confirmPop = confirm("Do you want to Add?");
        if (confirmPop == false) {
            return false;
        }
        this.showLastRow = false;
        this.showIcons();
    }

    editFunction() {
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.route.paramMap.subscribe((data) => this.idview = + data.get('idview'));
        if (this.id > 0) {
            this.edit = true;
            this.BatchNumberShow = true;
            this.InvestmentapplicationService.getItemsDetail(this.id).subscribe(
                data => {
                    console.log(data);
                    var dataArray = data['result']['data'];
                    for (var i = 0; i < dataArray.length; i++) {
                        this.investmentapplicationData['f063finstitutionId'] = dataArray[i]['f063finstitutionId'];
                    }

                    this.investmentappDataheader['f063fuumBank'] = data['result']['data'][0]['f063fuumBank'];
                    this.investmentappDataheader['f063fapplicationId'] = data['result']['data'][0]['f063fapplicationId'];
                    this.investmentappDataheader['f063fdescription'] = data['result']['data'][0]['f063fdescription'];
                    this.investmentappDataheader['f063finvestmentAmount'] = data['result']['data'][0]['f063finvestmentAmount'];
                    this.investmentapplicationList = data['result']['data'];

                    if (data['result']['data'][0]['f063fapproved1Status'] == 2 || data['result']['data'][0]['f063fapproved2Status'] == 3 || data['result']['data'][0]['f063fapproved3Status'] == 4) {
                        this.viewDisabled = true;
                        this.editDisabled = true;
                        $("#target input,select").prop("disabled", true);
                        $("#target1 input,select").prop("disabled", true);
                    }

                    if (this.idview == 2 || this.idview == 3 || this.idview == 4) {
                        this.saveBtnDisable = true;
                        this.viewDisabled = true;
                        this.editDisabled = true;

                        $("#target input,select").prop("disabled", true);
                        $("#target1 select,input").prop("disabled", true);
                    }
                    if (this.id > 0) {
                        this.editDisabled = true;
                    }
                    this.onBlurMethod();
                    this.showIcons();
                    // this.onInstitutionChange();
                }, error => {
                    console.log(error);
                });
        }
    }


    ngOnInit() {
        this.BatchNumberShow = false;
        console.log("on init" + this.BatchNumberShow);
        //to view
        this.route.paramMap.subscribe((data) => this.idview = + data.get('idview'));
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id < 1) {
            this.showLastRow = false;
            this.showLastRowMinus = false;
            this.showLastRowPlus = true;
        } else {
            this.showLastRow = true;

        }
        this.viewDisabled = false;
        this.editDisabled = false;

        this.showIcons();

        this.investmentapplicationData['f063finstitutionId'] = '';
        // this.investmentapplicationData['f063fidInvestmentType'] = '';

        let durationtypeobj = {};
        durationtypeobj['name'] = 'Days';
        this.durationtype.push(durationtypeobj);
        durationtypeobj = {};
        durationtypeobj['name'] = 'Month';
        this.durationtype.push(durationtypeobj);
        durationtypeobj = {};
        durationtypeobj['name'] = 'Year';
        this.durationtype.push(durationtypeobj);


        // console.log(this.id);
        this.ajaxCount = 0;
       
        //company uum bank dropdown
        this.BanksetupaccountService.getActiveCompanyBank().subscribe(
            data => {
                this.banksetupaccountList = data['result']['data'];
            }
        );

        // bank dropdown
        this.ajaxCount++;
        this.BankService.getActiveBank().subscribe(
            data => {
                this.ajaxCount--;
                this.bankList = data['result']['data'];
            }
        );

        // Investment Institution dropdown
        this.ajaxCount++;
        this.InvestmentinstitutionService.getActiveInstitute().subscribe(
            data => {
                this.ajaxCount--;
                console.log(this.ajaxCount);

                this.investmentinstitutionList = data['result']['data'];
            }
        );
        this.onBlurMethod();
    }
    ConvertToFloat(val) {
        return parseFloat(val);
    }
    fileSelected(event) {

        this.file = event.target.files[0];
        console.log(this.file);

    }
    fileView() {
        let fileId;
        fileId = this.investmentappDataheader['f064fid'];
        console.log("asdfsad")
        console.log(fileId);
        this.InvestmentapplicationService.getItemsDetail(fileId).subscribe(
            data => {

                if (data['result']['data'][0]['f063ffileUpload'] == null) {
                    alert("File not uploaded !");
                } else {
                    window.open(this.downloadUrl + data['result']['data'][0]['f063ffileUpload']);

                }
                console.log(data['result']['data'][0]['f063ffileUpload']);
            }, error => {
                console.log(error);
            });
    }
    onInstitutionChanges() {
        if (this.investmentapplicationData['f063finstitutionId'] == undefined) {
            this.investmentapplicationData['f063finstitutionId'] = 0;
        }

        this.InvestmentinstitutionService.getItemsDetail(this.investmentapplicationData['f063finstitutionId']).subscribe(
            data => {
                let institutionobj = {}
                institutionobj['f042fbankName'] = data['result'][0]['f042fbankName'];
                institutionobj['f061fidBank'] = data['result'][0]['f061fidBank'];
                this.investmentTypeList = [];
                this.investmentTypeList.push(institutionobj);
               
            })

        this.InvestmentinstitutionService.getAllInvestmentType(this.investmentapplicationData['f063finstitutionId']).subscribe(
            data => {
                this.typeofinstitutionList = data['result'];
            }
        );
    }

    openAddNewModal(content){
        this.modalService.open(content, {
            backdropClass: 'custom-backdrop',
            windowClass: 'custom-modal',
            size: 'lg'
        });
    }

   
    getType() {
        let id = this.investmentapplicationData['f063fbankId'];
        console.log(id);
        //  this.InvestmentapplicationService.getItemsDetail(id).subscribe(
         this.BankService.getItemsDetail(id).subscribe(
            data => {
                // this.bankList = data['result'][0];
                // this.branchList = data['result'][0];

                console.log(data);
                this.investmentapplicationData['f063fbranch'] = data['result']['f042fbranchName'];
            }
        );
    }
    
    saveInvestmentapplication() {
        let investmentappObject = {};

        if (this.investmentappDataheader['f063fdescription'] == undefined) {
            alert('Enter Description');
            return false;
        }
       
        // if (this.edit == false) {
            if (this.file == undefined) {
                alert('Select a file to upload');
                return false;
            }
        // }
        if (this.investmentappDataheader['f063fuumBank'] == undefined) {
            alert('Select UUM Bank');
            return false;
        }
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }

        // if (this.showLastRow == false) {
        //     console.log("1");
        //     var addactivities = this.addinvestmentapplication();
        //     if (addactivities == false) {
        //         return false;
        //     }

        // }

        let fd = new FormData();
        fd.append('file', this.file);

        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        console.log("save fun id " + this.id);
        if (this.id > 0) {
            investmentappObject['f063fid'] = this.id;
        }
        investmentappObject['f063fuumBank'] = this.investmentappDataheader['f063fuumBank'];
        investmentappObject['f063fapplicationId'] = this.investmentappDataheader['f063fapplicationId'];
        investmentappObject['f063fdescription'] = this.investmentappDataheader['f063fdescription'];
        investmentappObject['f063finvestmentAmount'] = this.investmentappDataheader['f063finvestmentAmount'];
        investmentappObject['f063fstatus'] = 0;
        investmentappObject['application-details'] = this.investmentapplicationList;


        if (this.id > 0) {
            this.InvestmentapplicationService.updateInvestmentapplicationItems(investmentappObject, this.id).subscribe(
                data => {

                    switch (data['status']) {
                        case 241:
                            alert("Enter two digits after decimal");
                            break;

                        // case 409:
                        //     alert("Enter the new Certificate Number");
                        //     break;

                        case 200:
                            alert("Investment Application has been Updated Successfully")
                            this.router.navigate(['investment/investmentapplication']);
                            break;

                    }


                }, error => {
                    console.log(error);
                });
        }
        else {

            // this.httpClient.post(environment.api.filebase, fd)
            //     .subscribe(
            //         data => {
            //             investmentappObject['f063ffileUpload'] = data['name'];
            this.InvestmentapplicationService.insertInvestmentapplicationItems(investmentappObject).subscribe(
                data => {

                    switch (data['status']) {
                        case 241:
                            alert("Enter two digits after decimal");
                            break;
                        case 200:
                            alert("Investment Application has been Added Successfully")
                            this.router.navigate(['investment/investmentapplication']);
                            break;

                    }

                }, error => {
                    console.log(error);
                });

            //     }
            // );

        }
    }




    showData(asdfsa) {
        console.log(asdfsa);
        console.log(this.investmentapplicationData['f063finstitutionId']);
        for (var i = 0; i < this.investmentinstitutionList.length; i++) {
            if (this.investmentinstitutionList[i]['f061fid'] == this.investmentapplicationData['f063finstitutionId']) {
                this.investmentapplicationData['f063fbranch'] = this.investmentinstitutionList[i]['f061fbranch'];
            }
        }
    }

    onBlurMethod() {

        if(this.investmentapplicationData['f063famount']>100000) {
            alert('Investement Amount cannot be greater than one million');
            this.investmentapplicationData['f063famount']=0;
            return false;
        }


    }
  
    keyPress(event: any) {

        alert(event);
        const pattern = /^(10{2}(?:,0{2})?|[1-9]?\d(?:,\d{2})?) %$/;

        let inputChar = String.fromCharCode(event.charCode);
        if (event.keyCode != 8 && !pattern.test(inputChar)) {
            event.preventDefault();
        }
    }

    deleteInvestmentList(object) {
        var cnf = confirm("Do you really want to delete");
        if (cnf == true) {
            console.log(this.investmentapplicationList);
            var index = this.investmentapplicationList.indexOf(object);
            this.deleteList.push(this.investmentapplicationList[index]['f063fidDetails']);
            let deleteObject = {};
            deleteObject['id'] = this.deleteList;
            this.InvestmentapplicationService.deleteItems(deleteObject).subscribe(
                data => {
                }, error => {
                    console.log(error);
                });
            if (index > -1) {
                this.investmentapplicationList.splice(index, 1);
            }
            this.onBlurMethod();
            this.showIcons();
        }
    }

    addinvestmentapplication() {

        //console.log("2");

        if (this.investmentapplicationData['f063finstitutionId'] == '') {
            alert('Select Investment Institution');
            return false;
        }
        if (this.investmentapplicationData['f063fbankId'] == undefined) {
            alert('Select Investment Bank');
            return false;
        }
        if (this.investmentapplicationData['f063fbranch'] == undefined) {
            alert('Enter Branch');
            return false;
        }
        if (this.investmentapplicationData['f063fidInvestmentType'] == undefined) {
            alert('Select Type Of Investment');
            return false;
        }
        if (this.investmentapplicationData['f063fdurationType'] == undefined) {
            alert('Select Duration Type');
            return false;
        } 
        if (this.investmentapplicationData['f063fduration'] == undefined) {
            alert('Enter Duration');
            return false;
        } 
       
        if (this.investmentapplicationData['f063fprofitRate'] == undefined) {
            alert('Enter Profit(%)');
            return false;
        }
        if (this.investmentapplicationData['f063famount'] == undefined) {
            alert('Enter Amount(RM)');
            return false;
        }

        var confirmPop = confirm("Do you want to Add?");
        if (confirmPop == false) {
            return false;
        }
        console.log(this.investmentapplicationData);

        var dataofCurrentRow = this.investmentapplicationData;


        // let amounti = this.investmentapplicationData['f063famount'];
        // console.log(amounti);

        // // for(var i = 0;i<this.investmentapplicationList.length;i ++){
        // //     this.investmentapplicationList[i]['f063famount'] = amounti.toFixed(2);
        // // }
        this.investmentapplicationData = {};
        this.investmentapplicationList.push(dataofCurrentRow);
        this.totalAmountCalculation();
        this.showIcons();
        this.modalService.dismissAll();

    }

    totalAmountCalculation(){
        let finaltotal=0;
        for (var i = 0; i < this.investmentapplicationList.length; i++) {
            finaltotal = finaltotal + this.ConvertToFloat(this.investmentapplicationList[i]['f063famount']);
        }

        this.investmentappDataheader['f063finvestmentAmount'] = this.ConvertToFloat(finaltotal).toFixed(2);

    }
    onlyNumberKey(event) {
        //console.log(event);
        if (event.charCode == 46) {

        } else {
            return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
        }
    }

    checkprofit() {
        let x = parseFloat(this.investmentapplicationData['f063fprofitRate']);
        // let a = parseFloat(x).toFixed(2)
        console.log(x);
        if (isNaN(x) || x < 0 || x > 100) {
            // value is out of range
            alert("percentage cannot be more than 100 !");
            this.investmentapplicationData['f063fprofitRate'] = '';
        }
    }
    maximize() {
        $("#myModal").addClass("modal");
        $("#overlay1").show();
        $(".maximize-icon").addClass("hidden");
        $(".minimize-icon").removeClass("hidden");
        $(".modal-backdrop").show();

    }
    minimize() {
        $("#myModal").removeClass("modal");
        $("#overlay1").hide();
        $(".maximize-icon").removeClass("hidden");
        $(".minimize-icon").addClass("hidden");
        $(".modal-backdrop").hide();
        $('#myModal').show();

    }
}
