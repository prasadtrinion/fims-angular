import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { InvestmentapplicationService } from '../service/investmentapplication.service'
import { NgxSpinnerService } from 'ngx-spinner';
import { environment } from '../../../environments/environment';

@Component({
    selector: 'Investmentapplication',
    templateUrl: 'investmentapplication.component.html'
})

export class InvestmentapplicationComponent implements OnInit {
    investmentapplicationList =  [];
    investmentapplicationData = {};
    id: number;
    showDownload: boolean;
    downloadUrl: string = environment.api.downloadbase;
    constructor(
        
        private InvestmentapplicationService: InvestmentapplicationService,
        private spinner: NgxSpinnerService,

    ) { }

    downloadReport() {
        console.log(this.investmentapplicationData);
        let newobje = {};
        newobje['financialYear'] = this.investmentapplicationData['financialYear'];
        this.InvestmentapplicationService.insertInvestmentapplicationItems(newobje).subscribe(
            data => {
                console.log(data['name']);
                window.open(this.downloadUrl + data['name']);
            }, error => {
                console.log(error);
            });
    }

    ngOnInit() { 
        
        this.spinner.show();                                
        
        this.InvestmentapplicationService.getItems().subscribe(
            data => {
                this.spinner.hide();
                this.investmentapplicationList = data['result']['data'];
                console.log(this.investmentapplicationList);

                for(var i=0;i<this.investmentapplicationList.length;i++) {
                    if(this.investmentapplicationList[i]['f063fapproved3Status']!=0) {
                        if(this.investmentapplicationList[i]['f063fapproved3Status']==1) {
                            this.investmentapplicationList[i]['currentStatus'] = 'Approved by Approval VC';
                            this.showDownload = true;
                        } else {
                            this.investmentapplicationList[i]['currentStatus'] = 'Rejected by Approval VC ';

                        }
                    } else if(this.investmentapplicationList[i]['f063fapproved2Status']!=0) {
                        if(this.investmentapplicationList[i]['f063fapproved2Status']==1) {
                            this.investmentapplicationList[i]['currentStatus'] = 'Approved by Approval Register';
                            this.showDownload = true;

                        } else {
                            this.investmentapplicationList[i]['currentStatus'] = 'Rejected by Approval Register';

                        }
                    } else if(this.investmentapplicationList[i]['f063fapproved1Status']!=0) {
                        if(this.investmentapplicationList[i]['f063fapproved1Status']==1) {
                            this.investmentapplicationList[i]['currentStatus'] = 'Approved by Approval Bursury';
                            this.showDownload = true;


                        } else {
                            this.investmentapplicationList[i]['currentStatus'] = 'Rejected by Approval Bursury';
                        }
                    }
                        else {
                            this.investmentapplicationList[i]['currentStatus'] = 'Pending for Approval';
    
                        }
                    } 
        }, error => {
            console.log(error);
        });
    }

    addInvestmentapplication(){
           console.log(this.investmentapplicationData);
        this.InvestmentapplicationService.insertInvestmentapplicationItems(this.investmentapplicationData).subscribe(
            data => {
                this.investmentapplicationList = data['todos'];
            }, error => {
                console.log(error);
            });

    }
}