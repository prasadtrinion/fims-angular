import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../material/material.module';
import { Ng4LoadingSpinnerModule } from 'ng4-loading-spinner';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { DataTableModule } from "angular-6-datatable";

import { HttpClientModule, HttpClient,HTTP_INTERCEPTORS } from '@angular/common/http';
import { InvestmentinstitutionComponent } from './investmentinstitution/investmentinstitution.component';
import { InvestmentinstitutionFormComponent } from './investmentinstitution/investmentinstitutionform/investmentinstitutionform.component';
import { InvestmentapplicationComponent } from './investmentapplication/investmentapplication.component';
import { InvestmentapplicationFormComponent } from './investmentapplication/investmentapplicationform/investmentapplicationform.component';

import { InvestmentregistrationComponent } from './investmentregistration/investmentregistration.component';
import { InvestmentregistrationFormComponent } from './investmentregistration/investmentregistrationform/investmentregistrationform.component';
import { LetterofinstitutionComponent } from './letterofinstitution/letterofinstitution.component';
import { WithdrawletterComponent } from './withdrawletter/withdrawletter.component';
import { WithdrawFormComponent } from "./withdrawletter/withdrawaletterform/withdrawaletterform.component";

import { TypeofinvestmentComponent } from './typeofinvestment/typeofinvestment.component';
import { TypeofinvestmentFormComponent } from './typeofinvestment/typeofinvestmentform/typeofinvestmentform.component';
import { ReinvestmentEditComponent } from './reinvestment/reinvestmentedit.component.html/reinvestmentedit.component';

import { ApprovaloneComponent } from './approvalone/approvalone.component';
import { ApprovaloneformComponent } from './approvalone/approvaloneform/approvaloneform.component';

import { ApprovalthreeformComponent } from './approvalthree/approvalthreeform/approvalthreeform.component';
import { ApprovaltwoformComponent } from './approvaltwo/approvaltwoform/approvaltwoform.component';
import { InvestmentsummaryformComponent } from './investmentsummary/investmentsummaryform/investmentsummaryform.component';

import { ApprovaltwoComponent } from './approvaltwo/approvaltwo.component';
import { ApprovalthreeComponent } from './approvalthree/approvalthree.component';
import { InvestmentsummaryComponent } from './investmentsummary/investmentsummary.component';
import { OnlineapplicationoneComponent } from './onlineapplicationone/onlineapplicationone.component';
import { OnlineapprovaloneComponent } from './onlineapprovalone/onlineapprovalone.component';
import { OnlineapprovaltwoComponent } from './onlineapprovaltwo/onlineapprovaltwo.component';
import { OnlineapprovalthreeComponent } from "./onlineapprovalthree/onlineapprovalthree.component";

import { SoldapplicationComponent } from './soldapplication/soldapplication.component';
import { ReinvestmentComponent } from './reinvestment/reinvestment.component';
import { LetterOfInstructionComponent } from '../investment/letterofinstruction/letterofinstruction.component';
import { InvestmentsummaryService } from './service/investmentsummary.service'
import { InvestmentinstitutionService } from './service/investmentinstitution.service';
import { InvestmentapplicationService } from './service/investmentapplication.service';
import { InvestmentregistrationService } from './service/investmentregistration.service';
import { VoucherService } from '../accountsrecivable/service/voucher.service';
import { ReinvestmentService } from './service/reinvestment.service';
import { TypeofinvestmentService } from './service/typeofinvestment.service';

import { ApprovaloneService } from './service/approvalone.service';
import { ApprovaltwoService } from './service/approvaltwo.service';
import { ApprovalthreeService } from './service/approvalthree.service';
import { OnlineapplicationoneService } from './service/onlineapplicationone.service';
import { OnlineapprovaloneService } from './service/onlineapprovalone.service';
import { OnlineapprovaltwoService } from './service/onlineapprovaltwo.service';
import { SoldapplicationService } from './service/soldapplication.service';
import { BanksetupaccountService } from './../generalsetup/service/banksetupaccount.service';
import { BankService } from './../generalsetup/service/bank.service';
import { GlcodegenerationService } from './../generalledger/service/glcodegeneration.service';
import { DefinationmsService } from './../studentfinance/service/definationms.service';
import { LetterofintitutionService } from './../investment/service/letterofinstitution.service';
import { WithdrawletterService } from './../investment/service/Withdrawletter.service';
import { LetterOfInstructionService } from '../investment/service/letterofinstruction.service';
import { OnlineapprovalthreeService } from "../investment/service/onlineapprovalthree.service";
import { ApplicationletterService } from './service/applicationletter.service'
import { InstructionletterService } from './../investment/service/instructionletter.service';
import { ApplicationletterComponent } from './applicationletter/applicationletter.component';
import { InstructionletterComponent } from './instructionletter/instructionletter.component';

import { BatchWithdrawService } from "./service/batchwithdraw.service";
import { BatchWithdrawComponent } from "./batchwithdraw/batchwithdraw.component";
import { BatchWithdrawFormComponent } from "./batchwithdraw/batchwithdrawform/batchwithdrawform.component";

import { UploadfileComponent } from './uploadfile/uploadfile.component';
import { NotificationFormComponent } from './notification/notificationform/notificationform.component';
import { NotificationComponent } from './notification/notification.component';
import { NotificationService } from '../investment/service/notification.service';
import { UploadfileService } from '../investment/service/uploadfile.service';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { InvestmentRoutingModule } from './investment.router.module';
import { JwtInterceptor } from '../_helpers/jwt.interceptors';
import { SharedModule } from "../shared/shared.module";
import { NgSelectModule } from '@ng-select/ng-select';


@NgModule({
    imports: [
        CommonModule,
        DataTableModule,
        FormsModule,
        ReactiveFormsModule,
        MaterialModule,
        InvestmentRoutingModule,
        HttpClientModule,
        TabsModule.forRoot(),
        Ng2SearchPipeModule,
        HttpClientModule,
        NgSelectModule,
        SharedModule
        ],
    exports: [],
    declarations: [
        InvestmentsummaryformComponent,
        UploadfileComponent,
        InvestmentsummaryComponent,
        InvestmentinstitutionComponent,
        InvestmentinstitutionFormComponent,
        InvestmentapplicationComponent,
        InvestmentapplicationFormComponent,
        ApprovaloneComponent,
        ApprovaltwoComponent,
        InvestmentregistrationComponent,
        InvestmentregistrationFormComponent,
        OnlineapplicationoneComponent,
        OnlineapprovaloneComponent,
        OnlineapprovaltwoComponent,
        OnlineapprovalthreeComponent,
        SoldapplicationComponent,
        ReinvestmentComponent,
        ApprovalthreeComponent,
        LetterofinstitutionComponent,
        WithdrawletterComponent,
        WithdrawFormComponent,
        TypeofinvestmentComponent,
        TypeofinvestmentFormComponent,
        LetterOfInstructionComponent,
        ReinvestmentEditComponent,
        ApplicationletterComponent,
        InstructionletterComponent,
        NotificationComponent,
        NotificationFormComponent,
        ApprovaloneformComponent,
        ApprovalthreeformComponent,
        ApprovaltwoformComponent,
        BatchWithdrawFormComponent,
        BatchWithdrawComponent
        
    ],
    providers: [
        UploadfileService,
        InvestmentsummaryService,
        InvestmentinstitutionService,
        InvestmentapplicationService,
        ApprovaltwoService,
        ApprovaloneService,
        InvestmentregistrationService,
        VoucherService,
        OnlineapplicationoneService,
        OnlineapprovaloneService,
        OnlineapprovaltwoService,
        SoldapplicationService,
        ReinvestmentService,
        BanksetupaccountService,
        BankService,
        GlcodegenerationService,
        DefinationmsService,
        ApprovalthreeService,
        LetterofintitutionService,
        WithdrawletterService,
        TypeofinvestmentService,
        LetterOfInstructionService,
        OnlineapprovalthreeService,
        ApplicationletterService,
        InstructionletterService,
        BatchWithdrawService,
        NotificationService,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: JwtInterceptor,
            multi: true
          }
    ],

    
})
export class InvestmentModule { }
