import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { OnlineapprovaloneService } from '../service/onlineapprovalone.service'
import { AlertService } from './../../_services/alert.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'Onlineapprovalone',
    templateUrl: 'onlineapprovalone.component.html'
})

export class OnlineapprovaloneComponent implements OnInit {

    onlineapprovaloneList = [];
    onlineapprovaloneData = {};
    selectAllCheckbox = true;

    constructor(

        private OnlineapprovaloneService: OnlineapprovaloneService,
        private AlertService: AlertService,
        private spinner: NgxSpinnerService
    ) { }

    ngOnInit() {
        this.getListData();
    }
    getListData() {
        this.spinner.show();
        this.OnlineapprovaloneService.getItemsDetail(0).subscribe(
            data => {
                this.spinner.hide();
                this.onlineapprovaloneList = data['result'];
                console.log(this.onlineapprovaloneList);
            }, error => {
                console.log(error);
            });
    }

    onlineapprovaloneListData() {
        console.log(this.onlineapprovaloneList);

        var confirmPop = confirm("Do you want to Approve ?");

        if (confirmPop == false) {
            return false;
        }
        var approvaloneIds = [];

        for (var i = 0; i < this.onlineapprovaloneList.length; i++) {
            if (this.onlineapprovaloneList[i]['f064fstatuss'] == true) {
                approvaloneIds.push(this.onlineapprovaloneList[i].f064fid);
            }
        }

        if (approvaloneIds.length < 1) {
            alert("Please select atleast one Application to Approve");
            return false;
        }

        var approvaloneUpdate = {};
        approvaloneUpdate['id'] = approvaloneIds;
        approvaloneUpdate['status'] = 1;
        approvaloneUpdate['level'] = 1;
        approvaloneUpdate['reason'] = this.onlineapprovaloneData['reason'];

        this.OnlineapprovaloneService.updateOnlineapprovaloneItems(approvaloneUpdate).subscribe(
            data => {
                alert(" Approved Sucessfully ! ");
                this.getListData();
                this.onlineapprovaloneData['reason'] = '';

            }, error => {
                console.log(error);
            });
    }

   


    rejectoneListData() {

        var confirmPop = confirm("Do you want to Reject ?");

        if (confirmPop == false) {
            return false;
        }

        console.log(this.onlineapprovaloneList);
        var rejectoneIds = [];
        for (var i = 0; i < this.onlineapprovaloneList.length; i++) {
            if (this.onlineapprovaloneList[i]['f064fstatuss'] == true) {
                rejectoneIds.push(this.onlineapprovaloneList[i].f064fid);
            }
        }

        if (rejectoneIds.length < 1) {
            alert("Please select atleast one Application to Reject");
            return false;
        }

        var rejectoneUpdate = {};
        rejectoneUpdate['id'] = rejectoneIds;
        rejectoneUpdate['status'] = 2;
        rejectoneUpdate['level'] = 1;
        rejectoneUpdate['reason'] = this.onlineapprovaloneData['reason'];

        if (this.onlineapprovaloneData['reason'] == '' || this.onlineapprovaloneData['reason'] == undefined) {
            alert("Enter the Description Rejection !");
        } else {

        this.OnlineapprovaloneService.rejectOnlineapprovalone(rejectoneUpdate).subscribe(
            data => {
                alert(" Application Rejected ! ");
                this.getListData();
                this.onlineapprovaloneData['reason'] = '';

            }, error => {
                console.log(error);
            });
        }
    }
    selectAll() {
        console.log("asdf");
        console.log(this.selectAllCheckbox);
        for (var i = 0; i < this.onlineapprovaloneList.length; i++) {
            this.onlineapprovaloneList[i].f064fstatus = this.selectAllCheckbox;
        }
        console.log(this.onlineapprovaloneList);
    }

}
