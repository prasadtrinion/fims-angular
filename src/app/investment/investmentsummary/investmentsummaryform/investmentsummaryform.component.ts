import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { InvestmentsummaryService } from '../../service/investmentsummary.service'
import { ActivatedRoute, Router } from '@angular/router';
import { ApprovalthreeService } from '../../service/approvalthree.service'

import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'Investmentsummaryform',
    templateUrl: 'investmentsummaryform.component.html'
})

export class InvestmentsummaryformComponent implements OnInit {
    rejectoneList = [];
    summaryList = [];
    summaryData = {};
    selectAllCheckbox = true;
    id :number;

    constructor(
        private InvestmentsummaryService: InvestmentsummaryService,
        private spinner: NgxSpinnerService,
        private route: ActivatedRoute,
        private ApprovalthreeService:ApprovalthreeService,
        private router: Router

    ) { }

    ngOnInit() {
        this.getListData();
    }
    //to approve other page after view
    getListData() {
        this.spinner.show();    
        var listArray = []                            
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        this.ApprovalthreeService.getItemsDetail(0).subscribe(
            data => {
                this.spinner.hide();
                listArray = data['result'];
                console.log(listArray);
                for(var i=0;i<listArray.length;i++) {
                    if(listArray[i]['f063fid']==this.id) {
                        this.summaryList.push(listArray[i])
                    }
                }

                
            }, error => {
                console.log(error);
            });
    }

    approvaloneListData() {     

        var confirmPop = confirm("Do you want to Approve ?");

        if (confirmPop == false) {
            return false;
        }
        console.log(this.summaryList);
        var approvaloneIds = [];
        var approvalrenewoneIds = [];

        for (var i = 0; i < this.summaryList.length; i++) {
            if (this.summaryList[i].f063fapproved1Status == true) {
                approvaloneIds.push(this.summaryList[i].f063fidDetails);
            }
            if (this.summaryList[i].autoRenewal == true) {
                approvalrenewoneIds.push(this.summaryList[i].f063fidDetails);
            }
           
        }
        if (approvaloneIds.length < 1) {
            alert("Select atleast one Application to Approve");
            return false;
        }

        var approvaloneUpdate = {};
        approvaloneUpdate['id'] = approvaloneIds;
        approvaloneUpdate['autoRenewal'] = approvalrenewoneIds;
        approvaloneUpdate['status'] = 1;
        approvaloneUpdate['level'] = 1;
        approvaloneUpdate['reason'] = this.summaryData['reason'];


        console.log(approvaloneUpdate);


        if (this.summaryData['check'] == '' || this.summaryData['check'] == undefined) {
            alert("Select Declartaion To approve !");
        } else {
        this.InvestmentsummaryService.updateInvestmentsummaryItems(approvaloneUpdate).subscribe(
            data => {
                alert(" Application Approved ! ");

                this.router.navigate(['investment/approvaloneform']);

            }, error => {
                console.log(error);
            });
        }
    }


    // rejectoneListData() {

    //     var confirmPop = confirm("Do you want to Reject ?");

    //     if (confirmPop == false) {
    //         return false;
    //     }
       

    //     console.log(this.approvaloneList);
    //     var rejectoneIds = [];
    //     for (var i = 0; i < this.approvaloneList.length; i++) {
    //         if (this.approvaloneList[i].f063fapproved1Status == true) {
    //             rejectoneIds.push(this.approvaloneList[i].f063fidDetails);
    //         }
    //     }

    //     if (rejectoneIds.length < 1) {
    //         alert("Select atleast one Application to Reject");
    //         return false;
    //     }

    //     var rejectoneUpdate = {};
    //         rejectoneUpdate['id'] = rejectoneIds;
    //         rejectoneUpdate['status'] = 2;
    //         rejectoneUpdate['level'] = 1;
    //         rejectoneUpdate['reason'] = this.approvaloneData['reason'];


    //     console.log(this.approvaloneData['reason']);

    //     if (this.approvaloneData['reason'] == '' || this.approvaloneData['reason'] == undefined) {
    //         alert("Enter the Description to Reject !");
    //     } else {

    //     this.InvestmentsummaryService.rejectApprovalone(rejectoneUpdate).subscribe(
    //             data => {
    //                 alert(" Application Rejected ! ");

    //                 this.getListData();
    //                 this.approvaloneData['reason'] = '';

    //             }, error => {
    //                 console.log(error);
    //             });
    //     }
    // }
}
