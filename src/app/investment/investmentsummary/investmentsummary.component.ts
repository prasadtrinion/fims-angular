import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { InvestmentsummaryService } from '../service/investmentsummary.service'
import { NgxSpinnerService } from 'ngx-spinner';
import { environment } from '../../../environments/environment';


@Component({
    selector: 'Investmentsummary',
    templateUrl: 'investmentsummary.component.html'
})

export class InvestmentsummaryComponent implements OnInit {
    investmentsummaryList =  [];
    investmentsummaryData = {};
    id: number;
    showDownload: boolean;
    downloadUrl: string = environment.api.downloadbase;

    constructor(
        
        private InvestmentsummaryService: InvestmentsummaryService,
        private spinner: NgxSpinnerService,

    ) { }

    downloadReport() {
        console.log(this.investmentsummaryData);
        let newobje = {};
        newobje['financialYear'] = this.investmentsummaryData['financialYear'];
        this.InvestmentsummaryService.insertInvestmentsummaryItems(newobje).subscribe(
            data => {
                console.log(data['name']);
                window.open(this.downloadUrl + data['name']);
            }, error => {
                console.log(error);
            });
    }

    ngOnInit() { 
        
        this.spinner.show();                                
        
        this.InvestmentsummaryService.getItems().subscribe(
            data => {
                this.spinner.hide();
                this.investmentsummaryList = data['result']['data'];
                console.log(this.investmentsummaryList);

               
                    
        }, error => {
            console.log(error);
        });
    }

    addInvestmentsummary(){
           console.log(this.investmentsummaryData);
        this.InvestmentsummaryService.getItems().subscribe(
            data => {
                this.investmentsummaryList = data['todos'];
            }, error => {
                console.log(error);
            });

    }
}