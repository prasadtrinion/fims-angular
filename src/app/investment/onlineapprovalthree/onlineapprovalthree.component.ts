import {Injector} from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { OnlineapprovalthreeService } from '../service/onlineapprovalthree.service'
import { AlertService } from './../../_services/alert.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'Onlineapprovalthree',
    templateUrl: 'onlineapprovalthree.component.html'
})

export class OnlineapprovalthreeComponent implements OnInit {
    
    onlineapprovalthreeList =  [];
    onlineapprovalthreeData = {};
    selectAllCheckbox = true;

    constructor(
        private OnlineapprovalthreeService: OnlineapprovalthreeService,
        private spinner: NgxSpinnerService
    ) { }

    ngOnInit() {    
       this.getListData();
    }
    getListData(){
        this.spinner.show();
        this.OnlineapprovalthreeService.getItemsDetail(0).subscribe(
            data => {
                this.spinner.hide();
                this.onlineapprovalthreeList = data['result']['data'];
                console.log(this.onlineapprovalthreeList);
        }, error => {
            console.log(error);
        });
    }
    
    onlineapprovalthreeListData() {
        console.log(this.onlineapprovalthreeList);

        var confirmPop = confirm("Do you want to Approve ?");

        if (confirmPop == false) {
            return false;
        }
       
        
        var approveIds = [];
        for (var i = 0; i < this.onlineapprovalthreeList.length; i++) {
            if(this.onlineapprovalthreeList[i].f064fstatus==true) {
                approveIds.push(this.onlineapprovalthreeList[i].f064fid);
            }
        }

        if (approveIds.length < 1) {
            alert("Please select atleast one Application to Approve");
            return false;
        }

        var approvalthreeUpdate = {};
        approvalthreeUpdate['id'] = approveIds;
        approvalthreeUpdate['status'] = 1;
        approvalthreeUpdate['level'] = 3;
        approvalthreeUpdate['reason'] = this.onlineapprovalthreeData['reason'];

        this.OnlineapprovalthreeService.updateOnlineapprovaltwoItems(approvalthreeUpdate).subscribe(
            data => {
                alert(" Application Approved ! ");

                this.getListData();
                this.onlineapprovalthreeData['reason'] = '';

        }, error => {
            console.log(error);
        });
    }
    rejectthreeListData(){
        console.log(this.onlineapprovalthreeList);

        var confirmPop = confirm("Do you want to Reject ?");

        if (confirmPop == false) {
            return false;
        }

        var rejectoneIds = [];
        for (var i = 0; i < this.onlineapprovalthreeList.length; i++) {
            if(this.onlineapprovalthreeList[i].f064fstatus==true) {
                rejectoneIds.push(this.onlineapprovalthreeList[i].f064fid);
            }
        }

        if (rejectoneIds.length < 1) {
            alert("Please select atleast one Application to Reject");
            return false;
        }


        var rejectUpdate = {};
        rejectUpdate['id'] = rejectoneIds;
        rejectUpdate['status'] = 2;
        rejectUpdate['level'] = 3;
        rejectUpdate['reason'] = this.onlineapprovalthreeData['reason'];
        
        if (this.onlineapprovalthreeData['reason'] == '' || this.onlineapprovalthreeData['reason'] == undefined) {
            alert("Enter the Description Rejection !");
        } else {

        this.OnlineapprovalthreeService.rejectOnlineapprovaltwo(rejectUpdate).subscribe(
            data => {
                alert(" Application Rejected ! ");

                this.getListData();
                this.onlineapprovalthreeData['reason'] = '';
        }, error => {
            console.log(error);
        });
    }
    }

    selectAll() {
        console.log("asdf");
        console.log(this.selectAllCheckbox);
        for (var i = 0; i < this.onlineapprovalthreeList.length; i++) {
                this.onlineapprovalthreeList[i].f070fstatus = this.selectAllCheckbox;
        }
        console.log(this.onlineapprovalthreeList);
      }
}
   
