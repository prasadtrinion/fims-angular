import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { InvestmentinstitutionService } from '../../service/investmentinstitution.service';
import { TypeofinvestmentService } from '../../service/typeofinvestment.service';
import { BankService } from '../../../generalsetup/service/bank.service';
import { CountryService } from '../../../generalsetup/service/country.service';
import { BanksetupaccountService } from '../../../generalsetup/service/banksetupaccount.service'
import { StateService } from '../../../generalsetup/service/state.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
    selector: 'InvestmentinstitutionFormComponent',
    templateUrl: 'investmentinstitutionform.component.html'
})

export class InvestmentinstitutionFormComponent implements OnInit {
    investmentinstitutionform: FormGroup;
    investmentinstitutionList = [];
    investmentinstitutionData = {};
    investmentinstitutionDataHeader = {};
    investmentapplList = [];
    investmentapplData = {};
    typeofinvestmentList = [];
    bankList = [];
    deleteList = [];
    countryList = [];
    invtype = [];
    stateList = [];
    idview: number;
    ajaxCount: number;
    showLastRow: boolean;
    showLastRowMinus: boolean;
    showLastRowPlus: boolean;
    listshowLastRowMinus: boolean;
    listshowLastRowPlus: boolean;
    viewDisabled: boolean;
    id: number;
    constructor(
        private InvestmentinstitutionService: InvestmentinstitutionService,
        private BanksetupaccountService: BanksetupaccountService,
        private TypeofinvestmentService: TypeofinvestmentService,
        private CountryService: CountryService,
        private StateService: StateService,
        private BankService: BankService,
        private route: ActivatedRoute,
        private router: Router
    ) { }
    ngDoCheck() {
        const change = this.ajaxCount;
        this.route.paramMap.subscribe((data) => this.idview = + data.get('idview'));
        if (this.ajaxCount == 0 && this.investmentinstitutionList.length > 0) {
            this.ajaxCount = 20;
            console.log("asdfsdf");
            this.showIcons();
        }
        if (this.ajaxCount == 0) {
            this.editFunction();
            this.ajaxCount = 10;
        }
    }
    deletemodule() {


        this.showLastRow = true;
        this.showIcons();
    }

    showIcons() {
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.investmentinstitutionList.length > 0) {
            this.showLastRowPlus = true;
            this.showLastRowMinus = true;
            this.listshowLastRowMinus = true;
        }

        if (this.investmentinstitutionList.length > 1) {
            if (this.showLastRow == false) {
                this.listshowLastRowPlus = false;
                this.listshowLastRowMinus = true;
                this.showLastRowPlus = true;
            } else {
                this.listshowLastRowPlus = true;
                this.listshowLastRowMinus = false;

            }
        }
        if (this.investmentinstitutionList.length == 1 && this.showLastRow == false) {
            this.listshowLastRowMinus = true;
            this.listshowLastRowPlus = false;

        }
        if (this.investmentinstitutionList.length == 1 && this.showLastRow == true) {
            this.listshowLastRowMinus = false;
            this.listshowLastRowPlus = true;

        }
        if (this.investmentinstitutionList.length == 0 && this.showLastRow == false) {
            this.showLastRowMinus = false;
            this.showLastRowPlus = true;
        }
        if (this.investmentinstitutionList.length == 0 && this.showLastRow == true) {
            this.showLastRowMinus = true;
            this.showLastRowPlus = false;
        }
        console.log(this.investmentinstitutionList.length);
        console.log("Show last row" + this.showLastRow);
        console.log("showLastRowMinus" + this.showLastRowMinus);
        console.log("showLastRowPlus" + this.showLastRowPlus);
        console.log("listshowLastRowMinus" + this.listshowLastRowMinus);
        console.log("listshowLastRowPlus" + this.listshowLastRowPlus);
        //  if(this.journalList.length>0){
        //     if(this.viewDisabled==true || this.journalList[0]['f017fapprovedStatus']==1 || this.journalList[0]['f017fapprovedStatus'] == 2) {
        //         this.listshowLastRowPlus = false;
        //     }
        // }
        // else{
        //     if(this.viewDisabled==true) {
        //         this.listshowLastRowPlus = false;
        //     }
        // }
    }

    showNewRow() {
        this.showLastRow = false;
        this.showIcons();
    }
    onCountryChange() {
        this.ajaxCount++;
        if (this.investmentinstitutionDataHeader['f061fcountry'] == undefined) {
            this.investmentinstitutionDataHeader['f061fcountry'] = 0;
        }
        this.StateService.getStates(this.investmentinstitutionDataHeader['f061fcountry']).subscribe(
            data => {
                this.ajaxCount--;
                this.stateList = data['result'];
                let other = {
                    'f012fid': "1",
                    "f012fshortName": "SH",
                    "f012fstateName": "Others"

                };
                this.stateList.push(other);

            }
        );
    }
    editFunction() {

        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {

            // alert("calld");
            this.InvestmentinstitutionService.getItemsDetail(this.id).subscribe(
                data => {
                    // this.investmentinstitutionData = data['result'][0];
                    if (data['result'].length == 0) {
                        // alert("Edit api not working ");
                        return false;
                    }
                    this.investmentinstitutionList = data['result'];
                    // this.investmentinstitutionData = data['result'][0];
                    this.investmentinstitutionDataHeader['f061finstitutionCode'] = data['result'][0]['f061finstitutionCode'];
                    this.investmentinstitutionDataHeader['f061fname'] = data['result'][0]['f061fname'];
                    this.investmentinstitutionDataHeader['f061fbranch'] = data['result'][0]['f061fbranch'];
                    this.investmentinstitutionDataHeader['f061fidBank'] = data['result'][0]['f061fidBank'];
                    this.investmentinstitutionDataHeader['f061faddress'] = data['result'][0]['f061faddress'];
                    this.investmentinstitutionDataHeader['f061fcontactNo'] = data['result'][0]['f061fcontactNo'];
                    this.investmentinstitutionDataHeader['f061fcontactPerson'] = data['result'][0]['f061fcontactPerson'];
                    this.investmentinstitutionDataHeader['f061fstatus'] = data['result'][0]['f061fstatus'];

                    this.investmentinstitutionDataHeader['f061fcountry'] = data['result'][0]['f061fcountry'];
                    this.investmentinstitutionDataHeader['f061fstate'] = data['result'][0]['f061fstate'];
                    this.investmentinstitutionDataHeader['f061fcity'] = data['result'][0]['f061fcity'];
                    this.investmentinstitutionDataHeader['f061fpostCode'] = data['result'][0]['f061fpostCode'];

                    // this.investmentinstitutionData = data['result'];
                    console.log(this.investmentinstitutionDataHeader);
                    // this.showLastRow = false;
                    this.showIcons();

                }, error => {
                    console.log(error);
                });
        }
    }



    ngOnInit() {
        this.viewDisabled = false;
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id < 1) {
            this.showLastRow = false;
            this.showLastRowMinus = false;
            this.showLastRowPlus = true;
        } else {
            this.showLastRow = true;

        }
        // this.investmentapplData['f038ftype'] = '';
        // this.investmentinstitutionDataHeader['f061fidBank'] = '';
        this.ajaxCount = 0;
        //bank dropdown
        this.ajaxCount++;
        this.BankService.getActiveBank().subscribe(
            data => {
                this.ajaxCount--;
                this.bankList = data['result']['data'];
            }
        );
        //country dropdown
        this.ajaxCount++;
        this.CountryService.getActiveCountryList().subscribe(
            data => {
                this.ajaxCount--;
                this.countryList = data['result']['data'];
            }
        );

        //state dropdown
        this.ajaxCount++;
        this.StateService.getActiveStateList().subscribe(
            data => {
                this.ajaxCount--;
                this.stateList = data['result']['data'];
            }
        );

        // Investment Type dropdown
        this.ajaxCount++;
        this.TypeofinvestmentService.getActiveInvestmentType().subscribe(
            data => {
                this.ajaxCount--;
                console.log(this.ajaxCount);
                this.typeofinvestmentList = data['result']['data'];
            }
        );

        // let invtypeobj = {};
        // invtypeobj['name'] = 'Fixed Deposit';
        // this.invtype.push(invtypeobj);

        // invtypeobj = {};
        // invtypeobj['name'] = 'Mudharabah';
        // this.invtype.push(invtypeobj);
        this.investmentinstitutionform = new FormGroup({
            institution: new FormControl('', Validators.required),
        });
        this.investmentinstitutionDataHeader['f061fstatus'] = 1;
        // this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        // if (this.id > 0) {
        //     this.InvestmentinstitutionService.getItemsDetail(this.id).subscribe(
        //         data => {
        //             this.investmentinstitutionData = data['result'][0];
        //             this.investmentapplList = data['result']['data'];

        //             this.investmentinstitutionData['f061finstitutionCode'] = data['result'][0]['f061finstitutionCode'];
        //             this.investmentinstitutionData['f061fname'] = data['result'][0]['f061fname'];
        //             this.investmentinstitutionData['f061fbranch'] = data['result'][0]['f061fbranch'];
        //             this.investmentinstitutionData['f061fidBank'] = data['result'][0]['f061fidBank'];
        //             this.investmentinstitutionData['f061faddress'] = data['result'][0]['f061faddress'];
        //             this.investmentinstitutionData['f061fcontactNo'] = data['result'][0]['f061fcontactNo'];
        //             this.investmentinstitutionData['f061fcontactPerson'] = data['result'][0]['f061fcontactPerson'];
        //             this.investmentinstitutionData['f061fstatus'] = data['result'][0]['f061fstatus'];

        //             console.log(this.investmentinstitutionData);
        //         }, error => {
        //             console.log(error);
        //         });
        // }
        this.showIcons();


    }

    addInvestmentinstitution() {

        let investmentinstObject = {};

        if (this.investmentinstitutionDataHeader['f061finstitutionCode'] == undefined) {
            alert('Enter Institution Code');
            return false;
        }
        if (this.investmentinstitutionDataHeader['f061fidBank'] == '') {
            alert('Select Investment Bank');
            return false;
        }


        if (this.investmentinstitutionDataHeader['f061fbranch'] == undefined) {
            alert('Enter Branch');
            return false;
        }

        if (this.investmentinstitutionDataHeader['f061faddress'] == undefined) {
            alert('Enter Address');
            return false;
        }

        if (this.investmentinstitutionDataHeader['f061fcontactNo'] == undefined) {
            alert('Enter Contact Number');
            return false;
        }
        if (this.investmentinstitutionDataHeader['f061fcontactPerson'] == undefined) {
            alert('Enter Contact Person');
            return false;
        }
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        if (this.showLastRow == false) {
            var addactivities = this.addInvestmentregistration();
            if (addactivities == false) {
                return false;
            }
        }
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        if (this.id > 0) {
            investmentinstObject['f061fid'] = this.id;
        }

        investmentinstObject['f061finstitutionCode'] = this.investmentinstitutionDataHeader['f061finstitutionCode'];
        investmentinstObject['f061fname'] = this.investmentinstitutionDataHeader['f061fname'];
        investmentinstObject['f061fcity'] = this.investmentinstitutionDataHeader['f061fcity'];
        investmentinstObject['f061fpostCode'] = this.investmentinstitutionDataHeader['f061fpostCode'];
        investmentinstObject['f061fcountry'] = this.investmentinstitutionDataHeader['f061fcountry'];
        investmentinstObject['f061fstatus'] = this.investmentinstitutionDataHeader['f061fstatus'];
        investmentinstObject['f061fstate'] = this.investmentinstitutionDataHeader['f061fstate']
        investmentinstObject['f061fbranch'] = this.investmentinstitutionDataHeader['f061fbranch'];
        investmentinstObject['f061fidBank'] = this.investmentinstitutionDataHeader['f061fidBank'];
        investmentinstObject['f061faddress'] = this.investmentinstitutionDataHeader['f061faddress'];
        investmentinstObject['f061fcontactNo'] = this.investmentinstitutionDataHeader['f061fcontactNo'];
        investmentinstObject['f061fcontactPerson'] = this.investmentinstitutionDataHeader['f061fcontactPerson'];
        investmentinstObject['f061fstatus'] = this.investmentinstitutionDataHeader['f061fstatus'];
        investmentinstObject['institute-details'] = this.investmentinstitutionList;

        console.log(investmentinstObject);

        if (this.id > 0) {
            this.InvestmentinstitutionService.updateInvestmentinstitutionItems(investmentinstObject, this.id).subscribe(
                data => {
                    if (data['status'] == 409)
                    this.router.navigate(['investment/investmentinstitution']);
                    alert("Investment Institution has been Updated Successfully")
                }, error => {
                    alert(' Institution Code Already Exist');
                    return false;
                });
        } else {
            this.InvestmentinstitutionService.insertInvestmentinstitutionItems(investmentinstObject).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert(' Institution Code Already Exist');
                    }
                    else {
                        this.router.navigate(['investment/investmentinstitution']);
                        alert("Investment Institution has been Added Successfully")
                    }
                }, error => {
                    console.log(error);
                });
        }
    }

  
    getType() {
        let id = this.investmentinstitutionDataHeader['f061fidBank'];
        console.log(id);
         this.BankService.getItemsDetail(id).subscribe(
            data => {
                // this.bankList = data['result'][0];
                // this.branchList = data['result'][0];

                console.log(data);
                this.investmentinstitutionDataHeader['f061fbranch'] = data['result']['f042fbranchName'];
            }
        );
    }
    phone(event: any) {
        const pattern = /[0-9\+\-\ ]/;

        let inputChar = String.fromCharCode(event.charCode);
        if (event.keyCode != 8 && !pattern.test(inputChar)) {
            event.preventDefault();
        }
    }

    deleteInvestmentList(object) {
        var cnf = confirm("Do you really want to delete");
        if (cnf == true) {
            var index = this.investmentinstitutionList.indexOf(object);
            this.deleteList.push(this.investmentinstitutionList[index]['f061fidDetails']);
            let deleteObject = {};
            deleteObject['id'] = this.deleteList;
            console.log(this.deleteList);
            this.InvestmentinstitutionService.deleteItems(deleteObject).subscribe(
                data => {
                }, error => {
                    console.log(error);
                });
            if (index > -1) {
                this.investmentinstitutionList.splice(index, 1);
            }
            this.showIcons();
        }
    }
    showBranchName(name) {
        console.log(name);

    }


    addInvestmentregistration() {
        console.log(this.investmentinstitutionList);
        var confirmPop = confirm("Do you want to Add?");
        if (confirmPop == false) {
            return false;
        }

        if (this.investmentinstitutionData['f061fidInvestmentType'] == undefined) {
            alert('Select Investment Type');
            return false;
        }

        var dataofCurrentRow = this.investmentinstitutionData;
        this.investmentinstitutionData = {};
        this.investmentinstitutionList.push(dataofCurrentRow);
        this.showIcons();
        console.log(this.investmentapplList);
    }
    maximize() {
        $("#myModal").addClass("modal");
        $("#overlay1").show();
        $(".maximize-icon").addClass("hidden");
        $(".minimize-icon").removeClass("hidden");
        $(".modal-backdrop").show();

    }
    minimize() {
        $("#myModal").removeClass("modal");
        $("#overlay1").hide();
        $(".maximize-icon").removeClass("hidden");
        $(".minimize-icon").addClass("hidden");
        $(".modal-backdrop").hide();
        $('#myModal').show();

    }
    onlyNumberKey(event) {
        if (event.charCode == 46) {

        } else {
            return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
        }
    }
    onlyNumberKey1(event) {
        console.log(event);
        return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
    }
}