import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { InvestmentinstitutionService } from '../service/investmentinstitution.service'
import { NgxSpinnerService } from 'ngx-spinner';


@Component({
    selector: 'investmentinstitution',
    templateUrl: 'investmentinstitution.component.html'
})

export class InvestmentinstitutionComponent implements OnInit {
    investmentinstitutionList =  [];
    investmentinstitutionData = {};
    id: number;

    constructor(
        private InvestmentinstitutionService: InvestmentinstitutionService,
        private spinner: NgxSpinnerService,


    ) { }
    ngOnInit() { 
        this.spinner.show();                                

        this.InvestmentinstitutionService.getItems().subscribe(
            data => {
                this.spinner.hide();

                this.investmentinstitutionList = data['result']['data'];
        }, error => {
            console.log(error);
        });
    }

    addInvestmentinstitution(){
           console.log(this.investmentinstitutionData);
        this.InvestmentinstitutionService.insertInvestmentinstitutionItems(this.investmentinstitutionData).subscribe(
            data => {
                this.investmentinstitutionList = data['todos'];
            }, error => {
                console.log(error);
            });

    }
}