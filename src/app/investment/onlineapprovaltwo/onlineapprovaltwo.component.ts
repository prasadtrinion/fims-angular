import {Injector} from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { OnlineapprovaltwoService } from '../service/onlineapprovaltwo.service'
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'Onlineapprovaltwo',
    templateUrl: 'onlineapprovaltwo.component.html'
})

export class OnlineapprovaltwoComponent implements OnInit {
    
    onlineapprovaltwoList =  [];
    onlineapprovaltwoData = {};
    selectAllCheckbox = true;

    constructor(
        
        private OnlineapprovaltwoService: OnlineapprovaltwoService,
        private spinner: NgxSpinnerService
    ) { }

    ngOnInit() {    
       this.getListData();
    }
    getListData(){
        this.spinner.show();
        this.OnlineapprovaltwoService.getItemsDetail(0).subscribe(
            data => {
                this.spinner.hide();
                this.onlineapprovaltwoList = data['result']['data'];
                console.log(this.onlineapprovaltwoList);
        }, error => {
            console.log(error);
        });
    }
    
    onlineapprovaltwoListData() {
        console.log(this.onlineapprovaltwoList);

        var confirmPop = confirm("Do you want to Approve ?");

        if (confirmPop == false) {
            return false;
        }

        var approvalIds = [];
        for (var i = 0; i < this.onlineapprovaltwoList.length; i++) {
            if(this.onlineapprovaltwoList[i].f070fstatus==true) {
                approvalIds.push(this.onlineapprovaltwoList[i].f064fid);
            }
        }
        if (approvalIds.length < 1) {
            alert("Please select atleast one Application to Approve");
            return false;
        }
        var approvaltwoUpdate = {};
        approvaltwoUpdate['id'] = approvalIds;
        approvaltwoUpdate['status'] = 1;
        approvaltwoUpdate['level'] = 2;
        approvaltwoUpdate['reason'] = this.onlineapprovaltwoData['reason'];

        this.OnlineapprovaltwoService.updateOnlineapprovaltwoItems(approvaltwoUpdate).subscribe(
            data => {
                alert(" Application Approved ! ");
                this.getListData();
                this.onlineapprovaltwoData['reason'] = '';
        }, error => {
            console.log(error);
        });
    }
    rejecttwoListData(){
        console.log(this.onlineapprovaltwoList);

        var confirmPop = confirm("Do you want to Reject ?");

        if (confirmPop == false) {
            return false;
        }
       

        var rejectoneIds = [];
        for (var i = 0; i < this.onlineapprovaltwoList.length; i++) {
            if(this.onlineapprovaltwoList[i].f070fstatus==true) {
                rejectoneIds.push(this.onlineapprovaltwoList[i].f064fid);
            }
        }
        if (rejectoneIds.length < 1) {
            alert("Please select atleast one Application to Reject");
            return false;
        }
        var rejectUpdate = {};
        rejectUpdate['id'] = rejectoneIds;
        rejectUpdate['status'] = 2;
        rejectUpdate['level'] = 2;
        rejectUpdate['reason'] = this.onlineapprovaltwoData['reason'];
        
        if (this.onlineapprovaltwoData['reason'] == '' || this.onlineapprovaltwoData['reason'] == undefined) {
            alert("Enter the Description Rejection !");
        } else {


        this.OnlineapprovaltwoService.rejectOnlineapprovaltwo(rejectUpdate).subscribe(
            data => {
                alert("Application Rejected ! ");

                this.getListData();
                this.onlineapprovaltwoData['reason'] = '';

        }, error => {
            console.log(error);
        });
    }
    }

    selectAll() {
        console.log("asdf");
        console.log(this.selectAllCheckbox);
        for (var i = 0; i < this.onlineapprovaltwoList.length; i++) {
                this.onlineapprovaltwoList[i].f070fstatus = this.selectAllCheckbox;
        }
        console.log(this.onlineapprovaltwoList);
      }
}
   
