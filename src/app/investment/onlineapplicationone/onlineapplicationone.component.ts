import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { OnlineapplicationoneService } from '../service/onlineapplicationone.service'
import { AlertService } from './../../_services/alert.service';

@Component({
    selector: 'Onlineapplicationone',
    templateUrl: 'onlineapplicationone.component.html'
})

export class OnlineapplicationoneComponent implements OnInit {
    onlineapplicationoneList = [];
    onlineapplicationoneData = {};
    // sellapplicationList = [];
    id: number;

    constructor(

        private OnlineapplicationoneService: OnlineapplicationoneService,
        private AlertService: AlertService
    ) { }

    ngOnInit() {

        this.getListData();
    }

    getListData() {

        this.OnlineapplicationoneService.getItems().subscribe(
            data => {
                this.onlineapplicationoneList = data['data']['data'];
            }, error => {
                console.log(error);
            });
    }


    sellOnlineapplicationoneData() {
        console.log(this.onlineapplicationoneList);
        var rejectoneIds = [];
        for (var i = 0; i < this.onlineapplicationoneList.length; i++) {
            if (this.onlineapplicationoneList[i]['f063fstatus'] == true) {
                rejectoneIds.push(this.onlineapplicationoneList[i].f063fid);
            }
        }
        var sellUpdate = {};
        sellUpdate['id'] = rejectoneIds;

        sellUpdate['reason'] = this.onlineapplicationoneData['f063fapprove2Reason'];
        console.log(sellUpdate);

        this.OnlineapplicationoneService.insertOnlineapplicationone(sellUpdate).subscribe(
            data => {
                this.getListData();
            }, error => {
                console.log(error);
            });

    }

    // addOnlineapplicationoneData(){
    //        console.log(this.onlineapplicationoneData);
    //     this.OnlineapplicationoneService.insertOnlineapplicationoneItems(this.onlineapplicationoneData).subscribe(
    //         data => {
    //             this.onlineapplicationoneList = data['todos'];
    //         }, error => {
    //             console.log(error);
    //         });

    // }
}