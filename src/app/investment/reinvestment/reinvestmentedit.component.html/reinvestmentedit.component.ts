import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { InvestmentregistrationService } from '../../service/investmentregistration.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ReinvestmentService } from '../../service/reinvestment.service';
import { InvestmentapplicationService } from '../../service/investmentapplication.service';
import { BanksetupaccountService } from '../../../generalsetup/service/banksetupaccount.service';
import { BankService } from '../../../generalsetup/service/bank.service';
import { DefinationmsService } from '../../../studentfinance/service/definationms.service';
import { FundService } from '../../../generalsetup/service/fund.service'
import { ActivitycodeService } from '../../../generalsetup/service/activitycode.service'
import { DepartmentService } from '../../../generalsetup/service/department.service'
import { TypeofinvestmentService } from "../../service/typeofinvestment.service";
import { HttpClientModule, HttpHeaders, HttpClient } from '@angular/common/http';
import { environment } from '../../../../environments/environment';

@Component({
    selector: 'ReinvestmentEditComponent',
    templateUrl: 'reinvestmentedit.component.html'
})

export class ReinvestmentEditComponent implements OnInit {

    investmentregistrationform: FormGroup;
    investmentregistrationList = [];
    investmentregistrationData = {};
    investmentapplicationList = [];
    bankList = [];
    banksetupaccountList = [];
    investmentstatusList = [];
    fundList = [];
    activitycodeList = [];
    DeptList = [];
    accountcodeList = [];
    investmentregistrationDataObject = {};
    registrationList = [];
    id: number;
    valueDate: string;
    file;
    effectiveDate: Date;
    maturityDate: Date;
    Duration: number;
    downloadUrl: string = environment.api.downloadbase;

    constructor(

        private InvestmentregistrationService: InvestmentregistrationService,
        private ReinvestmentService: ReinvestmentService,
        private InvestmentapplicationService: InvestmentapplicationService,
        private FundService: FundService,
        private TypeofinvestmentService: TypeofinvestmentService,
        private ActivitycodeService: ActivitycodeService,
        private DepartmentService: DepartmentService,
        private DefinationmsService: DefinationmsService,
        private BanksetupaccountService: BanksetupaccountService,
        private BankService: BankService,
        private httpClient: HttpClient,

        private route: ActivatedRoute,
        private router: Router

    ) { }

    ngOnInit() {
        this.getListData();
        this.investmentregistrationDataObject['investRegId'] = '';

        // investmentapplication 
        this.InvestmentapplicationService.getApprovedApplicationList().subscribe(
            data => {
                this.investmentapplicationList = data['result'];
            }
        );
        //company uum bank dropdown
        this.BanksetupaccountService.getItems().subscribe(
            data => {
                this.banksetupaccountList = data['result']['data'];
            }
        );

        //Fund dropdown
        this.FundService.getItems().subscribe(
            data => {
                this.fundList = data['result']['data'];
            }
        );

        // activity dropdown
        this.ActivitycodeService.getItemsByLevel().subscribe(
            data => {
                this.activitycodeList = data['result']['data'];
                for (var i = 0; i < this.activitycodeList.length; i++) {
                    this.activitycodeList[i]['f058fcompleteCode'] = this.activitycodeList[i]['f058fcompleteCode'];
                    this.activitycodeList[i]['f058fcompleteCode'].trim();
                }

            }, error => {
            });
        // account dropdown
        this.TypeofinvestmentService.getDebotorAccountcode().subscribe(
            data => {
                this.accountcodeList = data['data'];
            }, error => {
                console.log(error);
            });

        // department dropdown
        this.DepartmentService.getItems().subscribe(
            data => {
                this.DeptList = data['result']['data'];
            }, error => {
                console.log(error);
            });
        //company bank dropdown
        this.BankService.getItems().subscribe(
            data => {
                this.bankList = data['result']['data'];
            }
        );
        this.DefinationmsService.getInvestmentStatus('InvestmentStatus').subscribe(
            data => {
                this.investmentstatusList = data['result'];
            }
        );
    }

    getListData() {
        this.ReinvestmentService.getRegistrationList().subscribe(
            data => {
                this.registrationList = data['data']['data'];
            }, error => {
                console.log(error);
            });
    }

    onlyNumberKey(event) {
        console.log(event);
        if (event.charCode == 46) {

        } else {
            return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
        }
    }

    getInvestmentRegDetails() {
        let RegId = this.investmentregistrationDataObject['investRegId'];
        this.InvestmentregistrationService.getItemsDetail(RegId).subscribe(
            data => {
                this.investmentregistrationData = data['result']['data'][0];
            }, error => {
                console.log(error);
            });
    }

    fileView() {
        let fileId;
        fileId = this.investmentregistrationDataObject['investRegId'];
        console.log("asdfsad")
        console.log(fileId);
        this.InvestmentregistrationService.getItemsDetail(fileId).subscribe(
            data => {
                window.open(this.downloadUrl + data['result']['data'][0]['f064ffile']);
                console.log(data['result']['data'][0]['f064ffile']);
            }, error => {
                console.log(error);
            });
    }

    fileSelected(event) {

        this.file = event.target.files[0];
        console.log(this.file);

    }

    ReinvestListData() {

        if (this.investmentregistrationDataObject['investRegId'] == '' || this.investmentregistrationDataObject['investRegId'] == undefined) {
            alert("Select atleast one Application to Reinvest");
            return false;
        }
        if(this.investmentregistrationData['f064fname'] == undefined){
            alert("Enter Certificate Number");
            return false;
        }
        if(this.investmentregistrationData['f064feffectiveDate'] == undefined){
            alert("Select Effective Date");
            return false;
        }
        if(this.investmentregistrationData['f064fmaturityDate'] == undefined){
            alert("Select Maturity Number");
            return false;
        }
        var confirmPop = confirm("Do you want to Reinvest Application?");
        if (confirmPop == false) {
            return false;
        }
        let fd = new FormData();
        fd.append('file', this.file);

        let currentdate = new Date();
        let maturedate = new Date(this.investmentregistrationData['f064fmaturityDate']);

        console.log(currentdate.getTime());
        console.log(maturedate.getTime());

        if (maturedate.getTime() > currentdate.getTime()) {
            alert("Maturity date cannot be greater than current date");
            return false;
        } else {

            this.httpClient.post(this.downloadUrl, fd)
            .subscribe(
                data => {
                    this.investmentregistrationData['f064ffile'] = data['name'];
                    this.ReinvestmentService.updateItems(this.investmentregistrationData).subscribe(
                        data => {

                            switch (data['status']) {
                                case 241:
                                    alert("Enter two digits after decimal");
                                    break;

                                case 409:
                                    alert("Enter the new Certificate Number");
                                    break;

                                case 200:
                                    alert(" Reinvestment Application Sucessfull ! ");
                                    this.router.navigate(['investment/reinvestment']);
                                    break;
                            }

                        }, error => {
                            console.log(error);
                        });
                },
                err => {
                    console.log("Error occured");
                }
            );
        }
    }
}