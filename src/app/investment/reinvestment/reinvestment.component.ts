import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ReinvestmentService } from '../service/reinvestment.service'
import { InvestmentregistrationService } from '../service/investmentregistration.service'
import { NgxSpinnerService } from 'ngx-spinner';


@Component({
    selector: 'Reinvestment',
    templateUrl: 'reinvestment.component.html'
})

export class ReinvestmentComponent implements OnInit {
    reinvestmentList = [];
    reinvestmentData = {};
    investmentregistrationList = [];
    id: number;
    effectiveDate: Date;
    maturityDate: Date;
    Duration: number;

    constructor(
        private ReinvestmentService: ReinvestmentService,
        private InvestmentregistrationService: InvestmentregistrationService,
        private spinner: NgxSpinnerService,


    ) { }

    ngOnInit() {
        this.spinner.show();                                
        this.ReinvestmentService.getReinvestItems().subscribe(
            data => {
                this.spinner.hide();

                this.reinvestmentList = data['result']['data'];
            }, error => {
                console.log(error);
            });
        this.InvestmentregistrationService.getItems().subscribe(
            data => {
                this.investmentregistrationList = data['result']['data'];
                this.getDuration();
            }, error => {
                console.log(error);
            });
    }
    getDuration() {

        for (var i = 0; i < this.investmentregistrationList.length; i++) {
            this.effectiveDate = new Date(this.investmentregistrationList[i]['f064feffectiveDate']);
            this.maturityDate = new Date(this.investmentregistrationList[i]['f064fmaturityDate']);

            var timeDiff = Math.abs(this.maturityDate.getTime() - this.effectiveDate.getTime());
            var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
            this.reinvestmentList[i]['Duration'] = diffDays;
        }
    }
}