import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PurchaseorderService } from '../../../procurement/service/purchaseorder.service';
import { TaxsetupcodeService } from '../../../generalledger/service/taxsetupcode.service'
import { ItemsetupService } from '../../../procurement/service/itemsetup.service';
import { AlertService } from '../../../_services/alert.service';
import { FinancialyearService } from '../../../generalledger/service/financialyear.service';
import { SupplierregistrationService } from '../../../procurement/service/supplierregistration.service';
import { PurchaserequistionentryService } from '../../../procurement/service/purchaserequistionentry.service';
import { FundService } from '../../../generalsetup/service/fund.service';
import { AccountcodeService } from '../../../generalsetup/service/accountcode.service';
import { ActivitycodeService } from '../../../generalsetup/service/activitycode.service';
import { DepartmentService } from '../../../generalsetup/service/department.service';
import { PrapprovalService } from '../../../procurement/service/prapproval.service';


@Component({
    selector: 'VendorPoformComponent',
    templateUrl: 'vendorpoform.component.html'
})

export class VendorPoformComponent implements OnInit {
    purchaseorderList = [];
    purchaseorderData = {};
    purchaseorderDataheader = {};
    DeptList = [];
    accountcodeList = [];
    fundList = [];
    activitycodeList = [];
    itemUnitList = [];
    itemList = [];
    taxcodeList = [];
    departmentList = [];
    financialList = [];
    supplierList = [];
    purchaseMethods = [];
    purchaseRequisitionListBasedonId = [];
    purchaseRequisitionList = [];
    id: number;
    valueDate: string;
    ajaxCount: number;
    gstValue: number;
    Amount: number;
    taxAmount: number;
    totalAmount: number;
    type: string;

    constructor(

        private PurchaserequistionentryService: PurchaserequistionentryService,
        private PurchaseorderService: PurchaseorderService,
        private PrapprovalService: PrapprovalService,
        private TaxsetupcodeService: TaxsetupcodeService,
        private ActivitycodeService: ActivitycodeService,
        private AccountcodeService: AccountcodeService,
        private FundService: FundService,
        private ItemService: ItemsetupService,
        private alertService: AlertService,
        private DepartmentService: DepartmentService,
        private FinancialyearService: FinancialyearService,
        private SupplierregistrationService: SupplierregistrationService,
        private route: ActivatedRoute,
        private router: Router

    ) { }


    ngDoCheck() {
        const change = this.ajaxCount;
        // console.log(this.ajaxCount);
        if (this.ajaxCount == 0) {
            this.editFunction();
            this.ajaxCount = 10;
        }
    }

    editFunction() {
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.PurchaseorderService.getItemsDetail(this.id).subscribe(
                data => {
                    this.purchaseorderDataheader = data['result'][0];
                    this.purchaseorderList = data['result'];
                    console.log(data)
                    this.purchaseorderDataheader['f034fidFinancialyear'] = data['result'][0]['f034fidFinancialyear'];
                    this.purchaseorderDataheader['f034fidSupplier'] = data['result'][0]['f034fidSupplier'];
                    this.purchaseorderDataheader['f034fidDepartment'] = data['result'][0]['f034fidDepartment'];
                    this.purchaseorderDataheader['f034fexpiredDate'] = data['result'][0]['f034fexpiredDate'];
                    this.purchaseorderDataheader['f034fdescription'] = data['result'][0]['f034fdescription'];
                    this.purchaseorderDataheader['f034fidPurchaseRequisition'] = data['result'][0]['f034fidPurchasereq'];
                    this.purchaseorderDataheader['f034ftotalAmount'] = data['result'][0]['f034ftotalAmount'];

                    console.log(this.purchaseorderList);
                }, error => {
                    console.log(error);
                });
        }
    }

    ngOnInit() {
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        console.log(this.id);

        //  this.getInvoiceNumber();
        this.ajaxCount = 0;
        //purchase requisition dropdown
        this.ajaxCount++;
        this.PrapprovalService.getApprovedItems().subscribe(
            data => {
                this.ajaxCount--;
                this.purchaseRequisitionList = data['result'];
                // console.log(this.purchaseRequisitionList);
            }, error => {
                console.log(error);
            });
        //item dropdown
        this.ajaxCount++;
        this.ItemService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                console.log(this.ajaxCount);

                this.itemList = data['result']['data'];
                // console.log(this.itemList);
            }
        );
        //Supplierregistration dropdown
        this.ajaxCount++;
        this.SupplierregistrationService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                console.log(this.ajaxCount);

                this.supplierList = data['result']['data'];
            }
        );
        //Department dropdown
        this.ajaxCount++;
        this.DepartmentService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.DeptList = data['result']['data'];
            }
        );


        //Activity dropdown
        this.ajaxCount++;
        this.ActivitycodeService.getItemsByLevel().subscribe(
            data => {
                this.ajaxCount--;
                this.activitycodeList = data['result']['data'];
            }
        );


        //Account dropdown
        this.ajaxCount++;
        this.AccountcodeService.getItemsByLevel().subscribe(
            data => {
                this.ajaxCount--;
                this.accountcodeList = data['result']['data'];
            }
        );



        //Fund dropdown
        this.ajaxCount++;
        this.FundService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.fundList = data['result']['data'];
            }
        );

        //Financial year dropdown
        this.ajaxCount++;
        this.FinancialyearService.getActiveItems().subscribe(
            data => {
                this.ajaxCount--;
                console.log(this.ajaxCount);

                this.financialList = data['result'];
            }
        );
        //Department dropdown
        this.ajaxCount++;
        this.DepartmentService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                console.log(this.ajaxCount);

                this.departmentList = data['result']['data'];
            }
        );


        this.ajaxCount++;
        this.TaxsetupcodeService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.taxcodeList = data['result']['data'];
            });

        // if (this.id > 0) {
        //     this.PurchaseorderService.getItemsDetail(this.id).subscribe(
        //         data => {
        //             this.purchaseorderData = data['result'][0];
        //             this.purchaseorderData['f034fstatus'] = parseInt(this.purchaseorderData['f034fstatus']);

        //         }, error => {
        //             console.log(error);
        //         });
        // }


    }

    getPurchaseOrderNumber() {
        let typeObject = {};
        typeObject['type'] = this.purchaseorderDataheader['f034forderType'];
        this.type = this.purchaseorderDataheader['f034forderType']
        // console.log(typeObject);

        this.PurchaseorderService.getPurchaseOrderNumber(typeObject).subscribe(
            data => {
                this.purchaseorderDataheader['f034freferenceNumber'] = data['number'];
                this.valueDate = new Date().toISOString().substr(0, 10);
                this.purchaseorderDataheader['f034forderDate'] = this.valueDate;
            }
        );
        return this.type;
    }

    ConvertToFloat(val) {
        return parseFloat(val);
    }

    getPurchaseOrderDetails() {
        let PurchaseOrderId = this.purchaseorderDataheader['f034fidPurchaseRequisition'];

        this.purchaseorderList = [];
        this.PurchaserequistionentryService.getItemsDetail(PurchaseOrderId).subscribe(
            data => {
                this.purchaseRequisitionListBasedonId = data['result'];

                this.purchaseorderDataheader['f034fidFinancialyear'] = data['result'][0]['f088fidFinancialyear'];
                this.purchaseorderDataheader['f034fidSupplier'] = data['result'][0]['f088fidSupplier'];
                this.purchaseorderDataheader['f034fidDepartment'] = data['result'][0]['f088fidDepartment'];
                this.purchaseorderDataheader['f034fexpiredDate'] = data['result'][0]['f088fdate'];
                this.purchaseorderDataheader['f034fdescription'] = data['result'][0]['f088fdescription'];
                this.purchaseorderDataheader['f034ftotalAmount'] = data['result'][0]['f088ftotalAmount'];




                for (var i = 0; i < this.purchaseRequisitionListBasedonId.length; i++) {
                    var purchaseOrderObject = {}

                    //    purchaseOrderObject['f032fid'] = this.purchaseRequisitionList[i]['f032fid'];
                    purchaseOrderObject['f034fidItem'] = this.purchaseRequisitionListBasedonId[i]['f088fidItem'];
                    purchaseOrderObject['f034funit'] = this.purchaseRequisitionListBasedonId[i]['f088funit'];
                    purchaseOrderObject['f034ffundCode'] = this.purchaseRequisitionListBasedonId[i]['f088ffundCode'];
                    purchaseOrderObject['f034factivityCode'] = this.purchaseRequisitionListBasedonId[i]['f088factivityCode'];
                    purchaseOrderObject['f034fdepartmentCode'] = this.purchaseRequisitionListBasedonId[i]['f088fdepartmentCode'];
                    purchaseOrderObject['f034faccountCode'] = this.purchaseRequisitionListBasedonId[i]['f088faccountCode'];
                    purchaseOrderObject['f034fbudgetFundCode'] = this.purchaseRequisitionListBasedonId[i]['f088fbudgetFundCode'];
                    purchaseOrderObject['f034fbudgetActivityCode'] = this.purchaseRequisitionListBasedonId[i]['f088fbudgetActivityCode'];
                    purchaseOrderObject['f034fbudgetDepartmentCode'] = this.purchaseRequisitionListBasedonId[i]['f088fbudgetDepartmentCode'];
                    purchaseOrderObject['f034fbudgetAccountCode'] = this.purchaseRequisitionListBasedonId[i]['f088fbudgetAccountCode'];
                    purchaseOrderObject['f034fsoCode'] = this.purchaseRequisitionListBasedonId[i]['f088fsoCode'];
                    purchaseOrderObject['f034frequiredDate'] = this.purchaseRequisitionListBasedonId[i]['f088frequiredDate'];
                    purchaseOrderObject['f034fquantity'] = this.purchaseRequisitionListBasedonId[i]['f088fquantity'];
                    purchaseOrderObject['f034fprice'] = this.purchaseRequisitionListBasedonId[i]['f088fprice'];
                    purchaseOrderObject['f034total'] = this.purchaseRequisitionListBasedonId[i]['f088total'];
                    purchaseOrderObject['f034ftaxCode'] = this.purchaseRequisitionListBasedonId[i]['f088ftaxCode'];
                    purchaseOrderObject['f034fpercentage'] = this.purchaseRequisitionListBasedonId[i]['f088fpercentage'];
                    purchaseOrderObject['f034ftaxAmount'] = this.purchaseRequisitionListBasedonId[i]['f088ftaxAmount'];
                    purchaseOrderObject['f034ftotalIncTax'] = this.purchaseRequisitionListBasedonId[i]['f088ftotalIncTax'];


                    this.purchaseorderList.push(purchaseOrderObject);
                }




                console.log(this.purchaseorderList);
            }
        );
    }

    getUOM() {
        let itemId = this.purchaseorderData['f034fidItem'];
        console.log(itemId);
        this.ItemService.getItemsDetail(itemId).subscribe(
            data => {
                this.itemUnitList = data['result'][0];
                console.log(data);
                this.purchaseorderData['f034funit'] = data['result'][0]['f029funit'];
            }
        );
    }

    savePurchaseOrder() {

        let purchaseOrderObject = {};
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        console.log(this.id);
        if (this.id > 0) {
            purchaseOrderObject['f034fid'] = this.id;
        }

        if (this.purchaseorderList.length > 0) {

        } else {
            alert("Please add one details");
            return false;
        }


        purchaseOrderObject['f034forderType'] = this.purchaseorderDataheader['f034forderType'];
        purchaseOrderObject['f034forderDate'] = this.purchaseorderDataheader['f034forderDate'];
        purchaseOrderObject['f034fidFinancialyear'] = this.purchaseorderDataheader['f034fidFinancialyear'];
        purchaseOrderObject['f034fidSupplier'] = this.purchaseorderDataheader['f034fidSupplier'];
        purchaseOrderObject['f034fdescription'] = this.purchaseorderDataheader['f034fdescription'];
        purchaseOrderObject['f034freferenceNumber'] = this.purchaseorderDataheader['f034freferenceNumber'];
        purchaseOrderObject['f034fidDepartment'] = this.purchaseorderDataheader['f034fidDepartment'];
        purchaseOrderObject['f034fidPurchaseRequisition'] = this.purchaseorderDataheader['f034fidPurchaseRequisition'];
        purchaseOrderObject['f034ftotalAmount'] = this.purchaseorderDataheader['f034ftotalAmount'];

        purchaseOrderObject['f034fexpiredDate'] = this.purchaseorderDataheader['f034fexpiredDate'];
        purchaseOrderObject['f034fstatus'] = 0;
        purchaseOrderObject['purchase-details'] = this.purchaseorderList;

        if (this.id > 0) {
            this.PurchaseorderService.updateEntryItems(purchaseOrderObject, this.id).subscribe(
                data => {

                    this.router.navigate(['procurement/po']);
                    this.alertService.success(" Updated Sucessfully ! ");

                }, error => {
                    console.log(error);
                });
        } else {
            this.PurchaseorderService.insertEntryItems(purchaseOrderObject).subscribe(
                data => {


                    this.router.navigate(['procurement/po']);
                    this.alertService.success(" Added Sucessfully ! ");

                }, error => {
                    console.log(error);
                });
        }



    }

    getGstvalue() {
        let taxId = this.purchaseorderData['f034ftaxCode'];
        let quantity = this.purchaseorderData['f034fquantity'];
        let price = this.purchaseorderData['f034fprice'];
        //console.log(this.taxcodeList);
        var taxSelectedObject = {};
        for (var k = 0; k < this.taxcodeList.length; k++) {
            if (this.taxcodeList[k]['f081fid'] == taxId) {
                taxSelectedObject = this.taxcodeList[k];
            }
        }


        this.gstValue = taxSelectedObject['f081fpercentage'];
        this.purchaseorderData['f034fpercentage'] = this.gstValue;
        this.Amount = (this.ConvertToFloat(price)) * (this.ConvertToFloat(quantity));
        this.taxAmount = (this.ConvertToFloat(this.gstValue)) / 100 * this.ConvertToFloat((this.Amount));

        let totalAm = ((this.taxAmount) + (this.Amount));
        this.purchaseorderData['f034total'] = this.Amount.toFixed(2);
        this.purchaseorderData['f034ftaxAmount'] = this.taxAmount.toFixed(2);
        this.purchaseorderData['f034ftotalIncTax'] = totalAm.toFixed(2);

        console.log(this.purchaseorderData);

    }



    onBlurMethod() {
        console.log(this.purchaseorderList);
        var finaltotal = 0;
        for (var i = 0; i < this.purchaseorderList.length; i++) {
            var totalamount = (this.ConvertToFloat(this.purchaseorderList[i]['f034fquantity'])) * (this.ConvertToFloat(this.purchaseorderList[i]['f034fprice']));
            var gstamount = (this.ConvertToFloat(this.gstValue) / 100 * (totalamount));
            this.purchaseorderList[i]['f034ftotalIncTax'] = gstamount + totalamount;
            console.log(gstamount);
            console.log(totalamount)
            finaltotal = finaltotal + this.purchaseorderList[i]['f034ftotalIncTax'];
        }

        this.purchaseorderDataheader['f034ftotalAmount'] = this.ConvertToFloat(finaltotal.toFixed(2));
    }

    deleteEntry(object) {
        var cnf = confirm("Do you really want to delete");
        if (cnf == true) {
            console.log(this.purchaseorderList);
            var index = this.purchaseorderList.indexOf(object);;
            if (index > -1) {
                this.purchaseorderList.splice(index, 1);
            }
            this.onBlurMethod();
        }
    }

    addEntrylist() {
        // console.log(this.purchaseorderData);
        if (this.purchaseorderData['f034fbudgetAccountCode'] === undefined) {
            alert("Select Account Code to Continue !!")
        } else {

            if (this.purchaseorderData['f034fbudgetAccountCode'] === this.purchaseorderData['f034faccountCode']) {
                alert("Account Code cannot be same !!");

                console.log(this.purchaseorderData['f034fbudgetAccountCode']);
                console.log(this.purchaseorderData['f034faccountCode']);
                $(this.purchaseorderData['f034fbudgetAccountCode']).focus()
                this.purchaseorderData['f034fbudgetAccountCode'] = undefined;
            } else {

                var dataofCurrentRow = this.purchaseorderData;
                this.purchaseorderData = {};
                this.purchaseorderList.push(dataofCurrentRow);
                this.onBlurMethod();
            }
        }
    }
}