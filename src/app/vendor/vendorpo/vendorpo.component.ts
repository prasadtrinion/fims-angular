import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { faSearch, faEdit } from '@fortawesome/free-solid-svg-icons';
import { PurchaseorderService } from "../../procurement/service/purchaseorder.service";
@Component({
    selector: 'VendorPoComponent',
    templateUrl: 'vendorpo.component.html'
})

export class VendorPoComponent implements OnInit {
    poList =  [];
    faSearch = faSearch;
    faEdit = faEdit;
    id: number;
    title: string;
    type: string;
    constructor(        
        private PurchaseorderService: PurchaseorderService,
        private route: ActivatedRoute
    ) { }

    ngOnInit() { 
        this.id = parseInt(sessionStorage.getItem('f014fid'));        
        this.PurchaseorderService.getSupplierItems(this.id).subscribe(
            data => {
                
                this.poList = data['result']['data'];
        }, error => {
            console.log(error);
        });
    }

   
}