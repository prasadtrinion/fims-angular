import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { VendorinvoiceService } from '../service/vendorinvoice.service'
import { ActivatedRoute, Router } from '@angular/router';
import { faSearch, faEdit } from '@fortawesome/free-solid-svg-icons';

@Component({
    selector: 'VendorinvoiceComponent',
    templateUrl: 'vendorinvoice.component.html'
})

export class VendorinvoiceComponent implements OnInit {
    invoiceList =  [];
    invoiceData = {};
    faSearch = faSearch;
    faEdit = faEdit;
    data = {};
    id: number;
    title: string;
    type: string;
    constructor(        
        private VendorinvoiceService: VendorinvoiceService,
        private route: ActivatedRoute
    ) { }

    ngOnInit() { 
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.data['type'] = 'DOC';
        this.data['idCustomer'] = parseInt(sessionStorage.getItem('f014fid'));
        this.VendorinvoiceService.getItems(this.data).subscribe(
            data => {
                
                this.invoiceList = data['result']['data'];
                for( var i=0;i<this.invoiceList.length;i++) {
                    this.invoiceList[i]['f071finvoiceType'] = this.getName(this.invoiceList[i]['f071finvoiceType']);
                }

        }, error => {
            console.log(error);
        });
    }

    getName (name) {

      

        var text = '';
        switch(name) {
            case 'SP' : text =  "Sponsor";break;
            case 'STD' : text =  "Student";break;
            case 'STA' : text =  "Staff";break;
            case 'OR' : text = "Other Recevables";break;
        }
        return text;
    }

    downloadInvoice(id) {
        console.log(id);

        let invObject = {};
        invObject['id'] = id;
        invObject['download'] = 'yes';
        invObject['type'] = 'pdf';
        console.log(invObject);
        this.VendorinvoiceService.downloadInvoice(invObject).subscribe(
            data => { 
                console.log(data['name']);
                window.open('http://fims.mtcsbdeveloper.asia/download/'+data['name']); 
            }, error => {
                console.log(error);
            });


    }

}