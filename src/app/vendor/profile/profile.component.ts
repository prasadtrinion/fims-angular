import { Injector } from '@angular/core';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SupplierregistrationService } from '../../procurement/service/supplierregistration.service';
import { CountryService } from '../../generalsetup/service/country.service';
import { StateService } from '../../generalsetup/service/state.service';
import { LicenseService } from '../../procurement/service/license.service';
import { BankService } from '../../generalsetup/service/bank.service';
import { AlertService } from '../../_services/alert.service';
@Component({
    selector: 'ProfileComponent',
    templateUrl: 'profile.component.html'
})

export class ProfileComponent implements OnInit {

    vendorList = [];
    vendorData = {};

    vendorLicenseData = {};
    licenseDataList = [];

    vendorDataHeader = {};
    countryList = [];
    stateList = [];
    licenseList = [];
    bankList = [];
    ajaxcount = 0;


    id: number;
    constructor(

        private SupplierregistrationService: SupplierregistrationService,
        private CountryService : CountryService,
        private LicenseService: LicenseService,
        private StateService: StateService,
        private BankService: BankService,
        private AlertService: AlertService,
        private route: ActivatedRoute,
        private router: Router

    ) { }

    ngDoCheck() {
        const change = this.ajaxcount;
        console.log(this.ajaxcount);
        if (this.ajaxcount == 0) {
            this.editFunction();
            this.ajaxcount = 10;
        }
    }

    editFunction() {
        // this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.id = parseInt(sessionStorage.getItem('f014fid'));
        if (this.id > 0) {
            this.SupplierregistrationService.getItemsDetail(this.id).subscribe(
                data => {
                    this.licenseDataList = data['result'];
                    this.vendorDataHeader['f030fcompanyName'] = data['result'][0].f030fcompanyName;
                    this.vendorDataHeader['f030femail'] = data['result'][0].f030femail;
                    this.vendorDataHeader['f030faddress1'] = data['result'][0].f030faddress1;
                    this.vendorDataHeader['f030faddress2'] = data['result'][0].f030faddress2;
                    this.vendorDataHeader['f030faddress3'] = data['result'][0].f030faddress3;
                    this.vendorDataHeader['f030faddress4'] = data['result'][0].f030faddress4;
                    this.vendorDataHeader['f030fphone'] = data['result'][0].f030fphone;
                    this.vendorDataHeader['f030fvendorCode'] = data['result'][0].f030fvendorCode;
                    this.vendorDataHeader['f030fstartDate'] = data['result'][0].f030fstartDate;
                    this.vendorDataHeader['f030fendDate'] = data['result'][0].f030fendDate;
                    this.vendorDataHeader['f030fcity'] = data['result'][0].f030fcity;
                    this.vendorDataHeader['f030fpostCode'] = data['result'][0].f030fpostCode;
                    this.vendorDataHeader['f030fstate'] = data['result'][0].f030fstate;
                    this.vendorDataHeader['f030fcountry'] = data['result'][0].f030fcountry;
                    this.vendorDataHeader['f030fwebsite'] = data['result'][0].f030fwebsite;
                    this.vendorDataHeader['f030fcontact1Name'] = data['result'][0].f030fcontact1Name;
                    this.vendorDataHeader['f030fcontact1Email'] = data['result'][0].f030fcontact1Email;
                    this.vendorDataHeader['f030fcontact1Phone'] = data['result'][0].f030fcontact1Phone;
                    this.vendorDataHeader['f030fcontact2Name'] = data['result'][0].f030fcontact2Name;
                    this.vendorDataHeader['f030fcontact2Email'] = data['result'][0].f030fcontact2Email;
                    this.vendorDataHeader['f030fcontact2Phone'] = data['result'][0].f030fcontact2Phone;
                    this.vendorDataHeader['f030fidBank'] = data['result'][0].f030fidBank;
                    this.vendorDataHeader['f030faccountNumber'] = data['result'][0].f030faccountNumber;
                    this.vendorDataHeader['f030ffax'] = data['result'][0].f030ffax;


                    // if(data['result'][0]['f071fstatus']==1) {
                    //     this.saveBtnDisable = true;
        
                    //     $("#target input,select").prop("disabled", true);
                    //     $("#target1 input,select").prop("disabled", true);
                    // }


                    
                }, error => {
                    console.log(error);
                });
        }
    }

    ngOnInit() {

        this.getVendorCode();

        
        this.vendorLicenseData['f031fstatus'] = 1;
        this.vendorDataHeader['f030fstatus'] = 1;

        this.ajaxcount = 0;
        //country dropdown
        this.ajaxcount ++;
        this.CountryService.getActiveCountryList().subscribe(
            data=>{
                this.ajaxcount --;
                    this.countryList = data['result']['data'];
            }
        );

        //state dropdown
        this.ajaxcount ++;
        this.StateService.getActiveStateList().subscribe(
            data=>{
                this.ajaxcount --;
                    this.stateList = data['result']['data'];
            }
        );

      
        //License dropdown
        this.ajaxcount ++;
        this.LicenseService.getItems().subscribe(
            data=>{
                this.ajaxcount --;
                    this.licenseList = data['result']['data'];
            }
        );

        //Bank dropdown
        this.ajaxcount ++;
        this.BankService.getItems().subscribe(
            data=>{
                this.ajaxcount --;
                    this.bankList = data['result']['data'];
            }
        );

       



    }
    deletelicenseDataList(object){
        var cnf = confirm("Do you really want to delete");
        if (cnf == true) {
            console.log(this.licenseDataList);
            var index = this.licenseDataList.indexOf(object);;
            if (index > -1) {
                this.licenseDataList.splice(index, 1);
            }
          
        }
    }

    addlicenseDataList(){
        var dataofCurrentRow = this.vendorLicenseData;
        console.log("asdf");
        this.licenseDataList.push(dataofCurrentRow);
        this.vendorLicenseData = {};

    }

    getVendorCode(){

        let typeObject = {};
        typeObject['type']  = "Vendor"
        this.SupplierregistrationService.getCode(typeObject).subscribe(
            data=>{
                this.vendorDataHeader['f030fvendorCode'] = data['number'];
            }
        );
    }

    saveVendorData() {


let VendorObject = {};
        this.id = parseInt(sessionStorage.getItem('f014fid'));
        console.log(this.id);
        if (this.id > 0) {
            VendorObject['f030fid'] = this.id;
        }
        if(this.licenseDataList.length>0) {

        }  else {
            alert("Please add one License details");
            return false;
        }
        VendorObject['f030fid'] = this.id;
        VendorObject['f030fcompanyName'] = this.vendorDataHeader['f030fcompanyName'];
        VendorObject['f030femail'] = this.vendorDataHeader['f030femail'];
        VendorObject['f030faddress1'] = this.vendorDataHeader['f030faddress1'];
        VendorObject['f030faddress2'] = this.vendorDataHeader['f030faddress2'];
        VendorObject['f030faddress3'] = this.vendorDataHeader['f030faddress3'];
        VendorObject['f030faddress4'] = this.vendorDataHeader['f030faddress4'];
        VendorObject['f030fphone'] = this.vendorDataHeader['f030fphone'];
        VendorObject['f030fstartDate'] = this.vendorDataHeader['f030fstartDate'];
        VendorObject['f030fendDate'] = this.vendorDataHeader['f030fendDate'];
        VendorObject['f030fcity'] = this.vendorDataHeader['f030fcity'];
        VendorObject['f030fpostCode'] = this.vendorDataHeader['f030fpostCode'];
        VendorObject['f030fstate'] = this.vendorDataHeader['f030fstate'];
        VendorObject['f030fcountry'] = this.vendorDataHeader['f030fcountry'];
        VendorObject['f030fwebsite'] = this.vendorDataHeader['f030fwebsite'];
        VendorObject['f030fcontact1Name'] = this.vendorDataHeader['f030fcontact1Name'];
        VendorObject['f030fcontact1Email'] = this.vendorDataHeader['f030fcontact1Email'];
        VendorObject['f030fcontact1Phone'] = this.vendorDataHeader['f030fcontact1Phone'];
        VendorObject['f030fcontact2Name'] = this.vendorDataHeader['f030fcontact2Name'];
        VendorObject['f030fcontact2Email'] = this.vendorDataHeader['f030fcontact2Email'];
        VendorObject['f030fcontact2Phone'] = this.vendorDataHeader['f030fcontact2Phone'];
        VendorObject['f030fidBank'] = this.vendorDataHeader['f030fidBank'];
        VendorObject['f030faccountNumber'] = this.vendorDataHeader['f030faccountNumber'];
        VendorObject['f030ffax'] = this.vendorDataHeader['f030ffax'];
        VendorObject['f030fvendorCode'] = this.vendorDataHeader['f030fvendorCode'];

        //file upload
        VendorObject['f030fcompanyRegistration'] = "file";

      //license Details
        VendorObject['license-details'] = this.licenseDataList;

        console.log(VendorObject);

        if (this.id > 0) {
            this.SupplierregistrationService.updateSupplierregistrationItems(VendorObject, this.id).subscribe(
                data => {
                    this.router.navigate(['/vendor']);
                    this.AlertService.success("Updated Sucessfully !!")

                }, error => {
                    console.log(error);
                });


        } else {

            this.SupplierregistrationService.insertSupplierregistrationItems(VendorObject).subscribe(
                data => {
                    this.router.navigate(['/vendor']);
                    this.AlertService.success("Added Sucessfully !!")

                }, error => {
                    console.log(error);
                });

        }
    }

}