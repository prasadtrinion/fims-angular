import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ReceiptService } from '../../accountsrecivable/service/receipt.service'
import { ActivatedRoute, Router } from '@angular/router';


@Component({
    selector: 'VendorReceiptComponent',
    templateUrl: 'vendorreceipt.component.html'
})

export class VendorReceiptComponent implements OnInit {
    invoiceList =  [];
    invoiceData = {};
    id: number;
    title: string;
    type: string;

    constructor(
        
        private ReceiptService: ReceiptService,
        private route: ActivatedRoute
    ) { }

    ngOnInit() { 
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
console.log(this.id);
        if (this.id==0){
            this.title='Request Invoice';
            this.type='requestinvoice';
        }        
        if (this.id==1){
            
            this.type='invoice';
            this.title='Invoice';
        }
        this.id = parseInt(sessionStorage.getItem('f014fid'));
        
        this.ReceiptService.getVendorItemsDetail(this.id).subscribe(
            data => {
                
                this.invoiceList = data['result'];
        }, error => {
            console.log(error);
        });
    }

    addPremise(){
           console.log(this.invoiceData);
        this.ReceiptService.insertReceiptItems(this.invoiceData).subscribe(
            data => { 
                this.invoiceList = data['todos'];
            }, error => {
                console.log(error);
            });

    }
}