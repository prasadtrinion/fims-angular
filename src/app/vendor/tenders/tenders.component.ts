import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { TendersService } from '../service/tenders.service'
import { faSearch, faEdit } from '@fortawesome/free-solid-svg-icons';


@Component({
    selector: 'TendersComponent',
    templateUrl: 'tenders.component.html'
})

export class TendersComponent implements OnInit {
    tenderQuotationList =  [];
    tenderQuotationData = {};
    faSearch = faSearch;
    faEdit = faEdit;
    
    id: number;

    constructor(
        
        private TendersService: TendersService

    ) { }

    ngOnInit() { 
        
        this.TendersService.getItems().subscribe(
            data => {
                this.tenderQuotationList = data['result']['data'];
        }, error => {
            console.log(error);
        });
    }

    // addGlcodegeneration(){
    //        console.log(this.revenueData);
    //     this.RevenueService.insertRevenueItems(this.revenueData).subscribe(
    //         data => {
    //             this.revenueList = data['todos'];
    //         }, error => {
    //             console.log(error);
    //         });

    // }
}