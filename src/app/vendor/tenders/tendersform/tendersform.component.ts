import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { TenderquatationService } from '../../../procurement/service/tenderquotation.service'
import { ActivatedRoute, Router } from '@angular/router';
import { AlertService } from '../../../_services/alert.service'
import { TaxsetupcodeService } from '../../../generalledger/service/taxsetupcode.service'

import { ItemcategoryService } from '../../../procurement/service/itemcategory.service'
import { DepartmentService } from '../../../generalsetup/service/department.service';
import { StaffService } from "../../../loan/service/staff.service";
import { ItemsetupService } from '../../../procurement/service/itemsetup.service'
import { FinancialyearService } from '../../../generalledger/service/financialyear.service';
import { SupplierregistrationService } from '../../../procurement/service/supplierregistration.service';
import { OrdertypeService } from '../../../procurement/service/ordertype.service';
import { FundService } from '../../../generalsetup/service/fund.service';
import { AccountcodeService } from '../../../generalsetup/service/accountcode.service';
import { ActivitycodeService } from '../../../generalsetup/service/activitycode.service';

@Component({
    selector: 'TendersFormComponent',
    templateUrl: 'tendersform.component.html'
})

export class TendersFormComponent implements OnInit {
    tenderList = [];
    tenderData = {};
    DeptList = [];
    deptData = [];
    categoryList = [];
    committListData = {};
    committList = [];
    specList = [];
    specData = {};
    smartStaffList = [];
    itemsetupList = [];
    generalspecList = [];
    generalspecData = {};


    id: number;
    ajaxCount:number;
    idview:number;
    itemList = [];
    taxcodeList = [];
    orderType = [];
    sponserList = [];
    departmentList = [];
   
    accountcodeList = [];
    fundList = [];
    activitycodeList = [];
    financialList = [];
    supplierList = [];
    orderTypeList = [];
    valueDate: string;
    gstValue: number;
    Amount: number;
    taxAmount: number;
    totalAmount: number;
    type :string;
    constructor(
        private TenderquatationService: TenderquatationService,
        private ItemcategoryService: ItemcategoryService,
        private DepartmentService: DepartmentService,
        private route: ActivatedRoute,
        private AlertService: AlertService,
        private router: Router,
        private StaffService:StaffService,
        private ItemsetupService:ItemsetupService,
        private ActivitycodeService: ActivitycodeService,
        private AccountcodeService: AccountcodeService,
        private FundService: FundService,
        private FinancialyearService: FinancialyearService,
        private SupplierregistrationService: SupplierregistrationService,
        private TaxsetupcodeService:TaxsetupcodeService
    ) { }

    ngDoCheck() {
        const change = this.ajaxCount;
        console.log(this.ajaxCount);
        if (this.ajaxCount == 0) {
            this.editFunction();
            this.ajaxCount = 10;
        }
    }

    editFunction(){
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.route.paramMap.subscribe((data) => this.idview = + data.get('idview'));
           
        if (this.id > 0) {
            this.TenderquatationService.getItemsDetail(this.id).subscribe(
                data => {
                    this.tenderData = data['result']['tender-result'][0];
                    this.committList = data['result']['commitee-details'];
                    this.specList = data['result']['spec-details'];

                    // this.tenderData['commitee-details'] = this.committList;
                    // this.tenderData['spec-details'] = this.specList;

                    
                    if (data['result'].f035fstatus === true) {
                        console.log(data);
                        this.tenderData['f035fstatus'] = 1;
                    } else {
                        this.tenderData['f035fstatus'] = 0;

                    }

                    setTimeout(function(){ 
                        $("#target input,select,button").prop("disabled", true);
                        $("#target1 input,select,button").prop("disabled", true);

                        $("#target2 input,select,button").prop("disabled", true);

                    }, 1500);


                    console.log(this.tenderData);
                }, error => {
                    console.log(error);
                });
        }


    }

    fillDetails(){
        console.log(this.specData['f038fidItem']);
        for(var i=0;i<this.itemsetupList.length;i++) {
            if(this.itemsetupList[i]['f029fid']==this.specData['f038fidItem']) {
                this.specData['f038fidItem'] = this.itemsetupList[i]['f029funit'];
                this.specData[''] = this.itemsetupList[i]['tax;'];
            }
        }

    }
    ngOnInit() {
        this.tenderData['f035fstatus'] = 1;
        this.tenderData['f035fidDepartment'] = '';
        this.tenderData['f035fidCategory'] = '';
        this.tenderData['f035ftype'] = '';
        this.ajaxCount = 0;

        // categoryList dropdown
        this.ajaxCount++;
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.ItemcategoryService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.categoryList = data['result']['data'];
            }, error => {
                console.log(error);
            });

        // department dropdown
        this.ajaxCount++;
        this.DepartmentService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.DeptList = data['result']['data'];
            }, error => {
                console.log(error);
            });

            this.ajaxCount++;
            this.StaffService.getItems().subscribe(
                data => {
                    this.ajaxCount--;
                    this.smartStaffList = data['result']['data'];
                    console.log(this.smartStaffList);
                }, error => {
                    console.log(error);
                });

                this.ItemsetupService.getItems().subscribe(
                    data => {
                        this.itemsetupList = data['result']['data'];
                        console.log(this.itemsetupList);
                    }, error => {
                        console.log(error);
                    });


        //Financial year dropdown
        this.ajaxCount++;
        this.FinancialyearService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                // console.log(this.ajaxCount);

                this.financialList = data['result']['data'];
            }
        );
        
        //Department dropdown
        this.ajaxCount++;
        this.DepartmentService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                // console.log(this.ajaxCount);

                this.departmentList = data['result']['data'];
            }
        );
        //Department dropdown
        this.ajaxCount++;
        this.DepartmentService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.DeptList = data['result']['data'];
            }
        );

      
        //Activity dropdown
        this.ajaxCount++;
        this.ActivitycodeService.getItemsByLevel().subscribe(
            data => {
                this.ajaxCount--;
                this.activitycodeList = data['result']['data'];
            }
        );

       
        //Account dropdown
        this.ajaxCount++;
        this.AccountcodeService.getItemsByLevel().subscribe(
            data => {
                this.ajaxCount--;
                this.accountcodeList = data['result']['data'];
            }
        );


       
        //Fund dropdown
        this.ajaxCount++;
        this.FundService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.fundList = data['result']['data'];
            }
        );

              
        this.ajaxCount++;
        this.TaxsetupcodeService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.taxcodeList = data['result']['data'];
            });

        // Fee Item dropdown
        //  this.FeeitemService.getItems().subscribe(
        //      data => {
        //          this.feeItemList = data['result']['data'];
        //  }, error => {
        //      console.log(error);
        //  });
        // activity dropdown
        // this.ActivitycodeService.getItemsByLevel().subscribe(
        //     data => {
        //         console.log(data);
        //         this.activitycodeList = data['result']['data'];
        // }, error => {
        //     console.log(error);
        // });
        // account dropdown
        // this.AccountcodeService.getItemsByLevel().subscribe(
        //     data => {
        //         this.accountcodeList = data['result']['data'];
        // }, error => {
        //     console.log(error);
        // });


        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        console.log(this.id);

        

        console.log(this.id);
    }

    ConvertToFloat(val) {
        return parseFloat(val);
    }

    fillUOM(){
        var iditem = this.specData['f038fidItem'];
        for (var i=0;i<this.itemsetupList.length;i++) {
            if(this.itemsetupList[i]['f029fid']==iditem) {
                this.specData['f038funit'] = this.itemsetupList[i]['f029funit'];
            }
        }
    }

    addListGeneralSpecs(){
        this.generalspecList.push(this.generalspecData);
        this.generalspecData = {};
    }

    addEntrylist() {
        // console.log(this.purchaserequistionentryData);

        var dataofCurrentRow = this.specData;
        this.specList.push(dataofCurrentRow);
        //this.onBlurMethod();
        this.specData = {};

    }

    getGstvalue() {
        let taxId = this.specData['f038ftaxCode'];
        let quantity = this.specData['f038fquantity'];
        let price = this.specData['f038fprice'];
        //console.log(this.taxcodeList);
        var taxSelectedObject = {};
        for (var k = 0; k < this.taxcodeList.length; k++) {
            if (this.taxcodeList[k]['f081fid'] == taxId) {
                taxSelectedObject = this.taxcodeList[k];
            }
        }


        this.gstValue = taxSelectedObject['f081fpercentage'];
        this.specData['f038fpercentage'] = this.gstValue;
        this.Amount = (this.ConvertToFloat(price)) * (this.ConvertToFloat(quantity));
        this.taxAmount = (this.ConvertToFloat(this.gstValue)) / 100 * this.ConvertToFloat((this.Amount));

        let totalAm = ((this.taxAmount) + (this.Amount));
        this.specData['f038total'] = this.Amount.toFixed(2);
        this.specData['f038ftaxAmount'] = this.taxAmount.toFixed(2);
        this.specData['f038ftotalIncTax'] = totalAm.toFixed(2);



    }


    addListSpec() {
        this.specList.push(this.specData);
        this.specData = {};
    }

    addListCommittee(){
        this.committListData['f038fname'] = '-';
        this.committList.push(this.committListData);
        this.committListData = {};

    }
    getReferenceNumber(){
        var idCat = this.tenderData['f035fidCategory'];
        if(this.tenderData['f035fidCategory']==1) {
            idCat = 'TND';
            this.tenderData['f035ftype'] = 'TND';

        } else {
            idCat = 'QN';
            this.tenderData['f035ftype'] = 'QN';
        }
        let typeObject = {};
        typeObject['type'] = idCat;
        this.TenderquatationService.getNumber(typeObject).subscribe(
            newData => {
                console.log(newData);
                this.tenderData['f035fquotationNumber'] = newData['number'];
            }
        );
    }


    addtender() {
        console.log(this.tenderData);
        console.log(this.specList);
        console.log(this.committList);
        this.tenderData['commitee-details'] = this.committList;
        this.tenderData['spec-details'] = this.specList;
        this.tenderData['general-details'] = this.generalspecList;

        // if(this.committList.length>0) {

        // } else {
        //     alert("There should be atleast one committe person");
        //     return false;
        // }
        if(this.specList.length>0) {

        } else {
            alert("There should be atleast one Item");
            return false;
        }


        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
            if(this.id>0) {
                this.TenderquatationService.updateTenderquatationItems(this.tenderData,this.id).subscribe(
                    data => {
                        this.router.navigate(['procurement/tenderquotation']);
                        this.AlertService.success("Updated Sucessfully !!");
                }, error => {
                    console.log(error);
                });


        } else {
            this.tenderData['f035ftenderStatus'] = 0;
            this.TenderquatationService.insertTenderquatationItems(this.tenderData).subscribe(
                data => {
                    this.router.navigate(['procurement/tenderquotation']);
                    this.AlertService.success("Added Sucessfully !!");


            }, error => {
                console.log(error);
            });

        }

    }
}