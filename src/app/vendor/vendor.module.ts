import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { MaterialModule } from '../material/material.module';
import { HttpClientModule, HttpClient, HTTP_INTERCEPTORS } from '@angular/common/http';
import { DataTableModule } from "angular-6-datatable";
import { Ng2SearchPipeModule } from 'ng2-search-filter';



import { VendorRoutingModule } from './vendor.router.module';

import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { JwtInterceptor } from '../_helpers/jwt.interceptors';

import { TendersComponent } from "./tenders/tenders.component";
import { TendersFormComponent } from "./tenders/tendersform/tendersform.component";
import { ProfileComponent } from "./profile/profile.component";
import { TendersService } from "./service/tenders.service";

import { VendorinvoiceComponent } from "./vendorinvoice/vendorinvoice.component";
import { VendorReceiptComponent } from "./vendorreceipt/vendorreceipt.component";
import { VendorinvoiceService } from "./service/vendorinvoice.service";
import { VendorPoComponent } from "./vendorpo/vendorpo.component";
import { VendorPoformComponent } from "./vendorpo/vendorpoform/vendorpoform.component";


@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        MaterialModule,
        VendorRoutingModule,
        HttpClientModule,
        TabsModule.forRoot(),
        Ng2SearchPipeModule,
        HttpClientModule,
        DataTableModule,
        FontAwesomeModule,
    ],
    exports: [],
    declarations: [

        TendersComponent,
        VendorinvoiceComponent,
        TendersFormComponent,
        VendorReceiptComponent,
        ProfileComponent,
        VendorPoComponent,
        VendorPoformComponent

    ],
    providers: [

        TendersService,
        VendorinvoiceService,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: JwtInterceptor,
            multi: true
        }

    ],
})
export class VendorModule { }
