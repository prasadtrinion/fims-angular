import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';



import { TendersComponent } from "./tenders/tenders.component";
import { VendorinvoiceComponent } from "./vendorinvoice/vendorinvoice.component";
import { TendersFormComponent } from "./tenders/tendersform/tendersform.component";
import { VendorReceiptComponent } from "./vendorreceipt/vendorreceipt.component";

import { ProfileComponent } from "./profile/profile.component";

import { VendorPoComponent } from "./vendorpo/vendorpo.component";
import { VendorPoformComponent } from "./vendorpo/vendorpoform/vendorpoform.component";
const routes: Routes = [



  { path: 'tender', component: TendersComponent },
  { path: 'invoice', component: VendorinvoiceComponent },
  { path: 'receipt', component: VendorReceiptComponent },
  { path: 'profile', component: ProfileComponent },
  { path: 'edittender/:id', component: TendersFormComponent },
  { path: 'po', component: VendorPoComponent },
  { path: 'viewpo/:id', component: VendorPoformComponent }

  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],

})
export class VendorRoutingModule {

}