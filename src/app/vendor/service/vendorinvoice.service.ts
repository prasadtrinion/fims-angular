import { Injectable } from '@angular/core';
import { HttpClientModule,HttpHeaders, HttpClient} from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({ 'X-Auth-Client': 'qazwsx123',
    'Content-Type': 'application/json' })
    
  };
  
@Injectable()

export class VendorinvoiceService {
    masterInvoiceurl: string = environment.api.base + environment.api.endPoints.invoicesByType;
    downloadInvoiceUrl: string = environment.api.base + environment.api.endPoints.invoiceReport;

    constructor(private httpClient: HttpClient) { 
        
    }
    
    getItems(data) {
            return this.httpClient.post(this.masterInvoiceurl,data,httpOptions);  
    }
    

    getItemsDetail(id) {
        return this.httpClient.get(this.masterInvoiceurl+'/'+id,httpOptions);
    }
    
    downloadInvoice(invoiceData): Observable<any> {
        return this.httpClient.post(this.downloadInvoiceUrl, invoiceData,httpOptions);
    }
}