import { Injectable } from '@angular/core';
import { HttpClientModule,HttpHeaders, HttpClient} from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({ 'X-Auth-Client': 'qazwsx123',
    'Content-Type': 'application/json' })
    
  };
  
@Injectable()

export class TendersService {
    url: string = environment.api.base + environment.api.endPoints.tender;
    urltendershortlist: string = environment.api.base + environment.api.endPoints.getTendorDetails
    urltenderSuppliers: string = environment.api.base + environment.api.endPoints.tenderSuppliers;

    constructor(private httpClient: HttpClient) { 
        
    }
    
    insertTenderShortlist(tenderData): Observable<any> {
        return this.httpClient.post(this.urltenderSuppliers, tenderData,httpOptions);
    }
    getItems() {
        return this.httpClient.get(this.url,httpOptions);
    }

   
}