import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { YearlyprocessService } from '../service/yearlyprocess.service'


@Component({
    selector: 'YearlyprocessComponent',
    templateUrl: 'yearlyprocess.component.html'
})

export class YearlyprocessComponent implements OnInit {
    yearlyprocessList =  [];
    monthlyprocessData = {};
    id: number;

    constructor(
        private YearlyprocessService: YearlyprocessService

    ) { }
    ngOnInit() { 
        this.YearlyprocessService.getItems().subscribe(
            data => {
                this.yearlyprocessList = data['result']['data'];
        }, error => {
            console.log(error);
        });
    }  
}