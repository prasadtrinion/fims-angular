import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { YearlyprocessService } from '../../service/yearlyprocess.service'
import { ActivatedRoute, Router } from '@angular/router';
import { FinancialyearService } from '../../../generalledger/service/financialyear.service';

@Component({
    selector: 'YearlyprocessFormComponent',
    templateUrl: 'yearlyprocessform.component.html'
})

export class YearlyprocessFormComponent implements OnInit {
    yearlyprocessList = [];
    yearlyprocessData = {};
    finantialyearList = [];
    fyear = [];
    id: number;
    constructor(
        private YearlyprocessService: YearlyprocessService,
        private FinancialyearService: FinancialyearService,
        private route: ActivatedRoute,
        private router: Router
    ) { }

    ngOnInit() {
        this.yearlyprocessData['f019fstatus'] = 1;
        this.FinancialyearService.getActiveItems().subscribe(
            data => {
                //  this.ajaxCount--;
                this.finantialyearList = data['result'];
            }, error => {
                console.log(error);
            });
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        console.log(this.id);
        if (this.id > 0) {
            this.YearlyprocessService.getItemsDetail(this.id).subscribe(
                data => {
                    this.yearlyprocessData = data['result'];

                    if (data['result'].f019fstatus == '1') {
                        console.log(data);
                        this.yearlyprocessData['f019fstatus'] = 1;
                    } else {
                        this.yearlyprocessData['f019fstatus'] = 0;

                    }
                    console.log(this.yearlyprocessData['f019fstatus']);

                }, error => {
                    console.log(error);
                });
        }
        console.log(this.id);
    }
    addYearlyprocess() {
        console.log(this.yearlyprocessData);
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.YearlyprocessService.updateYearlyprocessItems(this.yearlyprocessData, this.id).subscribe(
                data => {
                    this.router.navigate(['generalledger/yearlyprocess']);
                }, error => {
                    console.log(error);
                });
        } else {
            this.YearlyprocessService.insertYearlyprocessItems(this.yearlyprocessData).subscribe(
                data => {
                    console.log(this.yearlyprocessList);
                    this.router.navigate(['generalledger/yearlyprocess']);

                }, error => {
                    console.log(error);
                });
        }
    }
}