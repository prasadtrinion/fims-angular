import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { OpeningbalanceService } from '../../service/openingbalance.service'
import { FinancialyearService } from '../../service/financialyear.service'
import { GlcodegenerationService } from '../../service/glcodegeneration.service'
import { ActivatedRoute, Router } from '@angular/router';
import { FundService } from '../../../generalsetup/service/fund.service';
import { AccountcodeService } from '../../../generalsetup/service/accountcode.service'
import { ActivitycodeService } from '../../../generalsetup/service/activitycode.service';
import { DepartmentService } from '../../../generalsetup/service/department.service'
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'OpeningbalanceFormComponent',
    templateUrl: 'openingbalanceform.component.html'
})

export class OpeningbalanceFormComponent implements OnInit {
    openingbalanceList = [];
    openingbalanceData = {};
    openingbalanceDataheader = {};
    financialyearList = [];
    newOpeningBalanceList = [];
    glList = [];
    glData = {};
    activitycodeList = [];
    accountcodeList = [];
    fundList = [];
    DeptList = [];
    credit: number;
    debit: number;
    idview: number;
    f016fidFinancialyear = {};
    showCrReadonly: boolean;
    showDrReadonly: boolean;
    id: number;
    ajaxCount: number;
    saveBtnDisable: boolean;
    viewDisabled: boolean;
    constructor(
        private OpeningbalanceService: OpeningbalanceService,
        private FinancialyearService: FinancialyearService,
        private GlcodegenerationService: GlcodegenerationService,
        private FundService: FundService,
        private AccountcodeService: AccountcodeService,
        private ActivitycodeService: ActivitycodeService,
        private DepartmentService: DepartmentService,
        private route: ActivatedRoute,
        private spinner: NgxSpinnerService,
        private router: Router
    ) { }

    ngDoCheck() {
        if (this.ajaxCount == 0) {
            this.ajaxCount = 10;
            this.spinner.hide();

        }
        const change = this.ajaxCount;
        this.saveBtnDisable = false;
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.route.paramMap.subscribe((data) => this.id = + data.get('idview'));
        if (this.ajaxCount == 0 && this.newOpeningBalanceList.length > 0) {
            this.ajaxCount = 20;
            if (this.newOpeningBalanceList[0]['f016fapprovedStatus'] == 1 || this.newOpeningBalanceList[0]['f016fapprovedStatus'] == 2) {
                this.saveBtnDisable = true;
                this.viewDisabled = true;

                $("#target input,select").prop("disabled", true);
                $("#target1 input,select").prop("disabled", true);
            }
        }
        if (this.ajaxCount == 0 && this.newOpeningBalanceList.length > 0) {
            this.ajaxCount = 20;
            if (this.idview > 1) {
                this.saveBtnDisable = true;
                this.viewDisabled = true;

                $("#target input,select").prop("disabled", true);
                $("#target1 input,select").prop("disabled", true);
            }
        }
    }

    ngOnInit() {
        this.spinner.show();
        this.viewDisabled = false;
        this.openingbalanceData['f016fstatus'] = 1;
        this.openingbalanceData['f016fapprovedStatus'] = 0;
        this.openingbalanceData['f016fdrAmount'] = 0;
        this.openingbalanceData['f016fcrAmount'] = 0;
        this.debit = 0;
        this.credit = 0;
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.ajaxCount++;
        this.FinancialyearService.getActiveItems().subscribe(
            data => {
                this.ajaxCount--;
                this.financialyearList = data['result'];
                if (data['result'].length == 1) {
                    this.openingbalanceData['f016fidFinancialyear'] = data['result'][0]['f015fid'];
                }
            }, error => {
                console.log(error);
            });
        // financial year
        if (this.id < 0 || this.id == undefined) {
        this.ajaxCount++;
            this.FinancialyearService.getActiveItems().subscribe(
                data => {
                    this.ajaxCount--;
                    this.financialyearList = data['result'];
                    if (data['result'].length == 1) {
                        this.openingbalanceData['f016fidFinancialyear'] = data['result'][0]['f015fid'];

                    }
                }, error => {
                    console.log(error);
                });
        }

        //Department dropdown
        this.ajaxCount = 0;
        this.ajaxCount++;
        this.DepartmentService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.DeptList = data['result']['data'];
            }
        );

        //Activity dropdown
        this.ajaxCount++;
        this.ActivitycodeService.getItemsByLevel().subscribe(
            data => {
                this.ajaxCount--;
                this.activitycodeList = data['result']['data'];
            }
        );

        //Account dropdown
        this.ajaxCount++;
        this.AccountcodeService.getItemsByLevel().subscribe(
            data => {
                this.ajaxCount--;
                this.accountcodeList = data['result']['data'];
            }
        );

        //Fund dropdown
        this.ajaxCount++;
        this.FundService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.fundList = data['result']['data'];
            }
        );
        //  glcode  dropdown
        this.ajaxCount++;

        this.GlcodegenerationService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.glList = data['result']['data'];
            }, error => {
                console.log(error);
            });

        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.route.paramMap.subscribe((data) => this.idview = + data.get('idview'));

        // console.log(this.id);
        if (this.id > 0) {
            this.ajaxCount++;
            this.OpeningbalanceService.getItemsDetail(this.id).subscribe(
                data => {
                    this.FinancialyearService.getItems().subscribe(
                        data => {
                            this.ajaxCount--;
                            this.financialyearList = data['result']['data'];

                        }, error => {
                            console.log(error);
                        });
                    this.openingbalanceData = data['result'][0];
                    // this.openingbalanceData['f016fstatus'] = parseInt(data['result'][0]['f016fstatus']);
                    console.log(data);
                    // this.newOpeningBalanceList = data['result']['data'];

                    if (data['result'][0]['f016fapprovedStatus'] == 1) {
                        this.viewDisabled = true;

                        setTimeout(
                            function () {
                                $("#target input,select").prop("disabled", true);
                                $("#target1 input,select").prop("disabled", true);
                            }, 2000);
                    }
                    if (this.idview == 2) {
                        this.viewDisabled = true;

                        $("#target input,select").prop("disabled", true);
                        $("#target1 input,select").prop("disabled", true);
                    }
                }, error => {
                    console.log(error);
                });
        }
    }
    indexTracker(index: number, value: any) {
        return index;
    }
    ConvertToInt(val) {
        return parseInt(val);
    }
    ConvertToFloat(val) {
        return parseFloat(val);
    }
    addOpeningbalance() {
        console.log(this.openingbalanceData);
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.openingbalanceData['f016fdrAmount'] = this.ConvertToFloat(this.openingbalanceData['f016fdrAmount']).toFixed(2);
        this.openingbalanceData['f016fcrAmount'] = this.ConvertToFloat(this.openingbalanceData['f016fcrAmount']).toFixed(2);
        if (this.id > 0) {
            this.OpeningbalanceService.updateOpeningbalanceItems(this.openingbalanceData, this.id).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert(' Financial Year Already Exist');
                    }
                    else {
                        this.router.navigate(['generalledger/openingbalance']);
                        alert("Opening Balance has been Updated Successfully")
                    }
                }, error => {
                    console.log(error);
                });
        } else {
            this.OpeningbalanceService.insertOpeningbalanceItems(this.openingbalanceData).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert(' Financial Year Already Exist');
                    }
                    else {
                        this.router.navigate(['generalledger/openingbalance']);
                        alert("Opening Balance has been Added Successfully");
                    }

                }, error => {
                    console.log(error);
                });
        }
    }
    onlyNumberKey(event) {
        //console.log(event);
        if (event.charCode == 46) {

        } else {
            return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
        }
    }

}

