import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { OpeningbalanceService } from '../service/openingbalance.service'
import { NgxSpinnerService } from 'ngx-spinner';


@Component({
    selector: 'Openingbalance',
    templateUrl: 'openingbalance.component.html'
})

export class OpeningbalanceComponent implements OnInit {
    openingbalanceList = [];
    openingbalanceData = {};
    id: number;

    constructor(

        private OpeningbalanceService: OpeningbalanceService,
        private spinner: NgxSpinnerService


    ) { }

    ngOnInit() {
        this.spinner.show();

        this.OpeningbalanceService.getItems().subscribe(
            data => {
                this.spinner.hide();
                
                this.openingbalanceList = data['result']['data'];
                let activityData = [];
                activityData = data['result']['data'];
                for (var i = 0; i < activityData.length; i++) {
                    if (activityData[i]['f016fapprovedStatus'] == 0) {
                        activityData[i]['f016fapprovedStatus'] = 'Pending';
                    } else if (activityData[i]['f016fapprovedStatus'] == 1) {
                        activityData[i]['f016fapprovedStatus'] = 'Approved';
                    } else {
                        activityData[i]['f016fapprovedStatus'] = 'Rejected';
                    }
                }
                this.openingbalanceList = activityData;
            }, error => {
                console.log(error);
            });
    }
}