import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MonthlyprocessService } from '../service/monthlyprocess.service'


@Component({
    selector: 'Monthlyprocess',
    templateUrl: 'monthlyprocess.component.html'
})

export class MonthlyprocessComponent implements OnInit {
    monthlyprocessList =  [];
    monthlyprocessData = {};
    id: number;

    constructor(
        
        private MonthlyprocessService: MonthlyprocessService

    ) { }

    ngOnInit() { 
        
        this.MonthlyprocessService.getItems().subscribe(
            data => {
                this.monthlyprocessList = data['result']['data'];
        }, error => {
            console.log(error);
        });
    }
}