import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { MonthlyprocessService } from '../../service/monthlyprocess.service'
import { ActivatedRoute, Router } from '@angular/router';
import { FinancialyearService } from '../../../generalledger/service/financialyear.service';

@Component({
    selector: 'MonthlyprocessFormComponent',
    templateUrl: 'monthlyprocessform.component.html'
})

export class MonthlyprocessFormComponent implements OnInit {
    monthlyprocessList = [];
    monthlyprocessData = {};
    finantialyearList = [];
    fmonth = [];
    fyear = [];
    id: number;
    constructor(
        private MonthlyprocessService: MonthlyprocessService,
        private route: ActivatedRoute,
        private FinancialyearService: FinancialyearService,
        private router: Router
    ) { }

    ngOnInit() {
        this.monthlyprocessData['f019fstatus'] = 1;

        // financial year dropdown
        //  this.ajaxCount++;
        this.FinancialyearService.getActiveItems().subscribe(
            data => {
                //  this.ajaxCount--;
                this.finantialyearList = data['result'];
            }, error => {
                console.log(error);
            });

        // var year = (new Date()).getFullYear().toString().substring(2);
        let month = {};
        month['name'] = 'January ';
        this.fmonth.push(month);
        month = {};
        month['name'] = 'February ';
        this.fmonth.push(month);
        month = {};
        month['name'] = 'March ';
        this.fmonth.push(month);
        month = {};
        month['name'] = 'April ';
        this.fmonth.push(month);
        month = {};
        month['name'] = 'May ';
        this.fmonth.push(month);
        month = {};
        month['name'] = 'June ';
        this.fmonth.push(month);
        month = {};
        month['name'] = 'July ';
        this.fmonth.push(month);
        month = {};
        month['name'] = 'August ';
        this.fmonth.push(month);
        month = {};
        month['name'] = 'September ';
        this.fmonth.push(month);
        month = {};
        month['name'] = 'October ';
        this.fmonth.push(month);
        month = {};
        month['name'] = 'November ';
        this.fmonth.push(month);
        month = {};
        month['name'] = 'December ';
        this.fmonth.push(month);

        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        console.log(this.id);

        if (this.id > 0) {
            alert("adfj");
            this.MonthlyprocessService.getItemsDetail(this.id).subscribe(
                data => {
                    this.monthlyprocessData = data['result'];
                    alert("adfs");
                }, error => {
                    console.log(error);
                });
        }
    }

    addMonthlyprocess() {
        console.log(this.monthlyprocessData);

        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.MonthlyprocessService.updateMonthlyprocessItems(this.monthlyprocessData, this.id).subscribe(
                data => {
                    this.router.navigate(['generalledger/monthlyprocess']);
                }, error => {
                    console.log(error);
                });
        } else {
            this.MonthlyprocessService.insertMonthlyprocessItems(this.monthlyprocessData).subscribe(
                data => {
                    this.router.navigate(['generalledger/monthlyprocess']);

                }, error => {
                    console.log(error);
                });
        }
    }
}