import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { FinancialyearService } from '../service/financialyear.service'
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'Financialyear',
    templateUrl: 'financialyear.component.html'
})
export class FinancialyearComponent implements OnInit {
    financialyearList = [];
    financialyearData = {};
    id: number;
    constructor(
        private FinancialyearService: FinancialyearService,
        private spinner: NgxSpinnerService
    ) { }
    
    ngOnInit() {
        this.spinner.show();
        this.FinancialyearService.getItems().subscribe(
            data => {
                this.spinner.hide();
                this.financialyearList = data['result']['data'];
            }, error => {
                console.log(error);
            });
    }
}