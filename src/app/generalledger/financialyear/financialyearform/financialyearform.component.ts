import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { FinancialyearService } from '../../service/financialyear.service'
import { ActivatedRoute, Router } from '@angular/router';

@Component({
    selector: 'financialyearFormComponent',
    templateUrl: 'financialyearform.component.html'
})

export class FinancialyearFormComponent implements OnInit {
    financialyearList = [];
    financialyearData = {};
    id: number;

    constructor(
        private financialyearService: FinancialyearService,
        private route: ActivatedRoute,
        private router: Router
    ) { }
    ngOnInit() {

        this.financialyearData['f015fstatus'] = 1;
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        console.log(this.id);
        if (this.id > 0) {
            this.financialyearService.getItemsDetail(this.id).subscribe(
                data => {
                    this.financialyearData = data['result'][0];

                    this.financialyearData['f015fstatus'] = parseInt(data['result'][0]['f015fstatus']);


                    console.log(this.financialyearData);
                }, error => {
                    console.log(error);
                });
        }
        console.log(this.id);
    }
    dateComparison() {

        let startDate = Date.parse(this.financialyearData['f015fstartDate']);
        let endDate = Date.parse(this.financialyearData['f015fendDate']);
        if (startDate > endDate) {
            alert("Start Date cannot be greater than End Date !");
            this.financialyearData['f015fendDate'] = "";
        }
    }
    checkvalidyear() {
        let d = new Date();
        let n = d.getFullYear();
        if (parseInt(this.financialyearData['f015fname']) < n) {
            alert('Enter a valid financial year');
            return false;
        }
    }

    addfinancialyear() {
        console.log(this.financialyearData);
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        let EnteredYear = parseInt(this.financialyearData['f015fname']);
        console.log("EnteredYear - " + EnteredYear);

        let startDate = this.financialyearData['f015fstartDate'];
        var startarr1 = startDate.split("-");
        let selectedstartYear = parseInt(startarr1[0]);
        console.log("selectedstartYear - " + selectedstartYear);

        let endDate = this.financialyearData['f015fendDate'];
        var endarr1 = endDate.split("-");
        let selectedendYear = parseInt(endarr1[0]);
        console.log("selectedendYear - " + selectedendYear);

        if (selectedstartYear == EnteredYear && selectedendYear == ++EnteredYear) {

            this.financialyearData['f015fcreatedBy'] = 1;
            this.financialyearData['f015fupdatedBy'] = 0;
            this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

            if (this.id > 0) {
                this.financialyearService.updatefinancialyearItems(this.financialyearData, this.id).subscribe(
                    data => {
                        if (data['status'] == 409) {
                            alert(' Financial Year Already Exist');
                            return false;
                        }
                        this.router.navigate(['generalledger/financialyear']);
                        alert("Financial Year has been Updated Successfully");
                    }, error => {
                        alert(' Financial Year Already Exist');
                        return false;
                        // console.log(error);
                    });

            } else {
                this.financialyearService.insertfinancialyearItems(this.financialyearData).subscribe(
                    data => {
                        if (data['status'] == 409) {
                            alert(' Financial Year Already Exist');
                            return false;
                        }
                        this.router.navigate(['generalledger/financialyear']);
                        alert("Financial Year has been Added Successfully");
                    }, error => {
                        alert(' Financial Year Already Exist');
                        return false;
                    });
            }
        } else {
            this.financialyearData['f015fstartDate'] = '';
            this.financialyearData['f015fendDate'] = '';
            return false;
        }
    }
    onlyNumberKey(event) {
        console.log(event);
        return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
    }
}