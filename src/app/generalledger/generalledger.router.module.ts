import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { JournalComponent } from './journal/journal.component';
import { JournalFormComponent } from './journal/journalform/journalform.component';
import { JournalmonthlysummaryComponent } from './journalmonthlysummary/journalmonthlysummary.component';
import { JournalapprovalComponent } from './journalapproval/journalapproval.component';

import { OpeningbalanceComponent } from './openingbalance/openingbalance.component';
import { OpeningbalanceFormComponent } from './openingbalance/openingbalanceform/openingbalanceform.component';
import { OpeningbalanceapprovalComponent } from './openingbalancepproval/openingbalanceapproval.component';

import { LedgerFormComponent } from './ledger/ledgerform/ledgerform.component';
import { LedgerComponent } from './ledger/ledger.component';

import { FinancialyearComponent } from './financialyear/financialyear.component';
import { FinancialyearFormComponent } from './financialyear/financialyearform/financialyearform.component';

import { MonthlyprocessComponent } from './monthlyprocess/monthlyprocess.component';
import { MonthlyprocessFormComponent } from './monthlyprocess/monthlyprocessform/monthlyprocessform.component';

import { JournalreversalComponent } from './journalreversal/journalreversal.component';
import { JournalreversalformComponent } from './journalreversal/journalreversalform/journalreversalform.component';

import { YearlyprocessFormComponent } from './yearlyprocess/yearlyprocessform/yearlyprocessform.component';
import { YearlyprocessComponent } from './yearlyprocess/yearlyprocess.component';
import { CashbankinComponent } from './cashbankin/cashbankin.component';

import { JournalapprovedataComponent } from "./journalapprovedata/journalapprovedata.component";

const routes: Routes = [

  { path: 'financialyear', component: FinancialyearComponent },
  { path: 'addnewfinancialyear', component: FinancialyearFormComponent },
  { path: 'editfinancialyear/:id', component: FinancialyearFormComponent },

  { path: 'journal', component: JournalComponent },
  { path: 'addnewjournal', component: JournalFormComponent },
  { path: 'editjournal/:id/:idview', component: JournalFormComponent },

  { path: 'journalmonthlysummary', component: JournalmonthlysummaryComponent },
  { path: 'journalapproval', component: JournalapprovalComponent },

  { path: 'journalreversal', component: JournalreversalComponent },
  { path: 'journalreversalform/:id', component: JournalreversalformComponent },

  { path: 'openingbalance', component: OpeningbalanceComponent },
  { path: 'addnewopeningbalance', component: OpeningbalanceFormComponent },
  { path: 'editopeningbalance/:id/:idview', component: OpeningbalanceFormComponent },

  { path: 'ledger', component: LedgerComponent },
  { path: 'addnewledger', component: LedgerFormComponent },
  { path: 'editledger/:id', component: LedgerFormComponent },

  { path: 'cashbook', component: CashbankinComponent },

  { path: 'monthlyprocess', component: MonthlyprocessComponent },
  { path: 'addnewmonthlyprocess', component: MonthlyprocessFormComponent },
  { path: 'editmonthlyprocess/:id', component: MonthlyprocessFormComponent },

  { path: 'yearlyprocess', component: YearlyprocessComponent },
  { path: 'addnewyearlyprocess', component: YearlyprocessFormComponent },
  { path: 'edityearlyprocess/:id', component: YearlyprocessFormComponent },

  { path: 'openingbalanceapproval', component: OpeningbalanceapprovalComponent },

  { path: 'journalapprovedata', component: JournalapprovedataComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GeneralledgerRoutingModule {

}