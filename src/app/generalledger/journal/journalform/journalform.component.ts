import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { JournalService } from '../../service/journal.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FundService } from '../../../generalsetup/service/fund.service';
import { AccountcodeService } from '../../../generalsetup/service/accountcode.service'
import { ActivitycodeService } from '../../../generalsetup/service/activitycode.service';
import { DepartmentService } from '../../../generalsetup/service/department.service'
import { StaffService } from '../../../loan/service/staff.service';
import { UserService } from "../../../generalsetup/service/user.service";
import { PurchaserequistionentryService } from "../../../procurement/service/purchaserequistionentry.service";
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'journalFormComponent',
    templateUrl: 'journalform.component.html'
})

export class JournalFormComponent implements OnInit {
    journalList = [];
    journalData = {};
    journalDataheader = {};
    DeptList = [];
    fundList = [];
    activitycodeList = [];
    accountcodeList = [];
    staffList = [];
    deleteList = [];
    staffList1 = [];
    socodeList = [];
    showCrReadonly: boolean;
    showDrReadonly: boolean;
    glCode: number
    debittotal: number;
    credittotal: number;
    valueDate: string;;
    id: number;
    ajaxCount: number;
    credit: number;
    debit: number;
    idview: number;
    viewDisabled: boolean;
    showLastRow: boolean;
    showLastRowMinus: boolean;
    showLastRowPlus: boolean;
    listshowLastRowMinus: boolean;
    listshowLastRowPlus: boolean;
    saveBtnDisable: boolean;
    showReference: boolean = false;
    constructor(
        private journalService: JournalService,
        private FundService: FundService,
        private AccountcodeService: AccountcodeService,
        private ActivitycodeService: ActivitycodeService,
        private DepartmentService: DepartmentService,
        private StaffService: StaffService,
        private UserService: UserService,
        private PurchaserequistionentryService: PurchaserequistionentryService,
        private route: ActivatedRoute,
        private spinner: NgxSpinnerService,
        private router: Router,
        private modalService: NgbModal

    ) { }

    ngDoCheck() {
        if (this.ajaxCount == 0) {
            this.editFunction();
            this.ajaxCount = 10;
            this.spinner.hide();

        }
        const change = this.ajaxCount;
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.route.paramMap.subscribe((data) => this.id = + data.get('idview'));
        if (this.ajaxCount == 0 && this.journalList.length > 0) {
            this.ajaxCount = 20;
            
            this.showIcons();
            if (this.journalList[0]['f017fapprovedStatus'] == 1 || this.journalList[0]['f017fapprovedStatus'] == 2) {
                this.saveBtnDisable = true;
                this.viewDisabled = true;
                $("#target input,select").prop("disabled", true);
                $("#target1 input,select").prop("disabled", true);
            }
        }
        if (this.ajaxCount == 0 && this.journalList.length > 0) {
            this.ajaxCount = 20;
            if (this.idview > 1) {
                this.saveBtnDisable = true;
                this.viewDisabled = true;
                $("#target input,select").prop("disabled", true);
                $("#target1 input,select").prop("disabled", true);
            }
        }
      
    }
    deletemodule() {
        this.showLastRow = true;
        this.showIcons();
    }

    openAddNewModal(content){
        this.modalService.open(content, {
            backdropClass: 'custom-backdrop',
            windowClass: 'custom-modal',
            size: 'lg'
        });
    }


    showIcons() {
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        console.log(this.journalList.length);
        console.log("Show last row" + this.showLastRow);
        console.log("showLastRowMinus" + this.showLastRowMinus);
        console.log("showLastRowPlus" + this.showLastRowPlus);
        console.log("listshowLastRowMinus" + this.listshowLastRowMinus);
        console.log("listshowLastRowPlus" + this.listshowLastRowPlus);

        if (this.journalList.length > 0) {
            this.showLastRowMinus = true;
            this.listshowLastRowMinus = true;
        }
        if (this.journalList.length > 1) {
            if (this.showLastRow == false) {
                this.listshowLastRowPlus = false;
                this.listshowLastRowMinus = true;
                this.showLastRowPlus = true;
            } else {
                this.listshowLastRowPlus = true;
                this.listshowLastRowMinus = false;
            }
        }
        if (this.journalList.length == 1 && this.showLastRow == false) {
            this.listshowLastRowMinus = true;
            this.listshowLastRowPlus = false;

        }
        if (this.journalList.length == 1 && this.showLastRow == true) {
            this.listshowLastRowMinus = false;
            this.listshowLastRowPlus = true;
        }
        if (this.journalList.length == 0 && this.showLastRow == false) {
            this.showLastRowMinus = false;
            this.showLastRowPlus = true;
        }
        if (this.journalList.length == 0 && this.showLastRow == true) {
            this.showLastRowMinus = true;
            this.showLastRowPlus = false;
        }
        if (this.journalList.length > 0) {
            if (this.viewDisabled == true || this.journalList[0]['f017fapprovedStatus'] == 1 || this.journalList[0]['f017fapprovedStatus'] == 2) {
                this.listshowLastRowPlus = false;
            }
        }
        else {
            if (this.viewDisabled == true) {
                this.listshowLastRowPlus = false;
            }
        }
    }

    showNewRow() {
        var confirmPop = confirm("Do you want to Add?");
        if (confirmPop == false) {
            return false;
        }
        this.showLastRow = false;
        this.showIcons();
    }
    ngOnInit() {
        this.ajaxCount = 0
        this.spinner.show();
        this.viewDisabled = false;
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.route.paramMap.subscribe((data) => this.idview = + data.get('idview'));
        this.valueDate = new Date().toISOString().substr(0, 10);
        this.journalData['f018fdateOfTransaction'] = this.valueDate;
        this.journalData['f017fapprovedStatus'] = 0;
        this.debit = 0;
        this.credit = 0;
        if (this.id < 1) {
            this.showLastRow = false;
            this.showLastRowMinus = false;
            this.showLastRowPlus = true;
        } else {
            this.showLastRow = true;
        }
        //Department dropdown
        this.ajaxCount = 0;
        this.ajaxCount++;
        this.DepartmentService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.DeptList = data['result']['data'];
            }
        );
        //Activity dropdown
        this.ajaxCount++;
        this.ActivitycodeService.getItemsByLevel().subscribe(
            data => {
                this.ajaxCount--;
                this.activitycodeList = data['result']['data'];
            }
        );
        //Account dropdown
        this.ajaxCount++;
        this.AccountcodeService.getItemsByLevel().subscribe(
            data => {
                this.ajaxCount--;
                this.accountcodeList = data['result']['data'];
            }
        );
        //Fund dropdown
        this.ajaxCount++;
        this.FundService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.fundList = data['result']['data'];
            }
        );
        //Fund dropdown
        this.ajaxCount++;
        this.StaffService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.staffList = data['result']['data'];
            }
        );
        //user dropdown
        this.ajaxCount++;
        this.UserService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.staffList1 = data['result']['data'];
            }, error => {
                console.log(error);
            });

        //SO Code
        this.ajaxCount++;
        this.PurchaserequistionentryService.getSocodeItems().subscribe(
            data => {
                this.ajaxCount--;
                this.socodeList = data['result'];
            }, error => {
                console.log(error);
            }
        )
        this.getRefNumber();
        this.showIcons();
    }
    editFunction() {

        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.route.paramMap.subscribe((data) => this.idview = + data.get('idview'));
        if (this.id > 0) {
            this.showReference = true;
        this.ajaxCount++;

            this.journalService.getItemsDetail(this.id).subscribe(
                data => {
                this.ajaxCount--;

                    this.journalDataheader = data['result'];
                    this.journalDataheader['f017freferenceNumber'] = data['result'][0]['f017freferenceNumber'];
                    this.journalDataheader['f017fdescription'] = data['result'][0]['f017fdescription'];
                    this.journalDataheader['f017fapprover'] = data['result'][0]['f017fapprover'];
                    this.journalList = data['result'];
                    if (data['result'][0]['f017fapprovedStatus'] == 1 || data['result'][0]['f017fapprovedStatus'] == 2 || data['result'][0]['f017fapprovedStatus'] == 3) {
                        this.viewDisabled = true;
                        setTimeout(
                            function () {
                                $("#target input,select").prop("disabled", true);
                                $("#target1 input,select").prop("disabled", true);
                            }, 2000);
                    }
                    if (this.idview == 2) {
                        this.viewDisabled = true;

                        $("#target input,select").prop("disabled", true);
                        $("#target1 input,select").prop("disabled", true);
                    }
                    this.showIcons();
                    this.onBlurMethod();
                }, error => {
                    //console.log(error);
                });
        }
    }
    getRefNumber() {
        let typeObject = {};
        typeObject['type'] = "GL";
        this.ajaxCount++;
        this.journalService.getRefNumber(typeObject).subscribe(
            data => {
        this.ajaxCount--;
                console.log(data);
                this.journalDataheader['f017freferenceNumber'] = data['number'];
                console.log(data['number']);
            }
        );
    }

    indexTracker(index: number, value: any) {
        return index;
    }
    onBlurMethod() {
        this.debittotal = 0;
        this.credittotal = 0;
        for (var i = 0; i < this.journalList.length; i++) {
            this.debittotal = this.debittotal + parseFloat(this.journalList[i]['f018fdrAmount']);
            this.credittotal = this.credittotal + parseFloat(this.journalList[i]['f018fcrAmount']);
        }
    }
    saveJournal() {
        if (this.credit != this.debit) {
            alert("Sum of Credit should be equal to Sum of Debit");
            return;
        }
        if (this.journalDataheader['f017fdescription'] == undefined) {
            alert(" Enter Journal Description");
            return false;
        }
        if (this.journalDataheader['f017fapprover'] == undefined) {
            alert(" Select Approver");
            return false;
        }
        // if (this.showLastRow == false) {
        //     var addactivities = this.addjournallist();
        //     if (addactivities == false) {
        //         return false;
        //     }
        // }
        if(this.journalList.length==0) {
            alert("Please add atleast one journal details");
            return false;
        }
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        // alert("data can be saved");
        let journalObject = {};

        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            journalObject['f017fid'] = this.id;
        }
        journalObject['f017fglId'] = this.journalDataheader['f017fglId'];
        journalObject['f017fdescription'] = this.journalDataheader['f017fdescription'];
        journalObject['f017freferenceNumber'] = this.journalDataheader['f017freferenceNumber'];
        journalObject['f017fapprover'] = this.journalDataheader['f017fapprover'];
        journalObject['f017fapprovedStatus'] = 0;
        journalObject['f017fmodule'] = "Manual";
        journalObject['journal-details'] = this.journalList;

        for (var i = 0; i < journalObject['journal-details'].length; i++) {
            journalObject['journal-details'][i]['f018fdrAmount'] = this.ConvertToFloat(journalObject['journal-details'][i]['f018fdrAmount']).toFixed(2);
            journalObject['journal-details'][i]['f018fcrAmount'] = this.ConvertToFloat(journalObject['journal-details'][i]['f018fcrAmount']).toFixed(2);

        }

        let deleteObj = {};
        deleteObj['id'] = this.deleteList;
        deleteObj['table'] = 't018fjournal_details';
        deleteObj['field'] = 'f018fid';
        if (this.deleteList.length > 0) {
            this.journalService.deletejournalItems(journalObject).subscribe(
                data => {
                }, error => {
                    console.log(error);
                });
        }
        this.journalService.insertjournalItems(journalObject).subscribe(
            data => {
                this.router.navigate(['generalledger/journal']);
                if (this.id == 0) {
                    alert("Journal has been Updated Successfully")
                }
                else {
                    alert("Journal has been Added Successfully")
                }
            }, error => {
                console.log(error);
            });
    }
    ConvertToInt(val) {
        return parseInt(val);
    }
    ConvertToFloat(val) {
        return parseFloat(val);
    }
    disableFieldsOfDRCR() {
        this.showCrReadonly = false;
        this.showDrReadonly = false;

        console.log(this.journalData);
        if (this.journalData['f018fdrAmount'] == undefined ||
            this.journalData['f018fdrAmount'] == 0 ||
            this.journalData['f018fdrAmount'] == '') {
            this.showDrReadonly = true;
        }
        if (this.journalData['f018fcrAmount'] == undefined ||
            this.journalData['f018fcrAmount'] == 0 ||
            this.journalData['f018fcrAmount'] == '') {
            this.showCrReadonly = true;
        }

        if (this.showCrReadonly == true && this.showDrReadonly == true) {
            this.showCrReadonly = false;
            this.showDrReadonly = false;
        }
        this.showIcons();
        console.log(this.showCrReadonly);
        console.log(this.showDrReadonly);
    }
    deleteJournal(object) {
        var cnf = confirm("Do you really want to delete");
        if (cnf == true) {
            console.log(this.journalList);
            var index = this.journalList.indexOf(object);;
            this.deleteList.push(this.journalList[index]['f018fid']);
            if (index > -1) {
                this.journalList.splice(index, 1);
            }
            this.showIcons();
            this.onBlurMethod();
        }
    }
    addjournallist() {
        if (this.journalData['f018fdateOfTransaction'] == undefined) {
            alert("Select Transaction Date");
            return false;
        }
        if (this.journalData['f018ffundCode'] == undefined) {
            alert("Select Fund");
            return false;
        }
        if (this.journalData['f018factivityCode'] == undefined) {
            alert("Select Activity Code");
            return false;
        }
        if (this.journalData['f018fdepartmentCode'] == undefined) {
            alert("Select Department Code");
            return false;
        }
        if (this.journalData['f018faccountCode'] == undefined) {
            alert("Select Account Code");
            return false;
        }

        if (this.journalData['f018fsoCode'] == undefined) {
            alert("Enter So Code");
            return false;
        }
        var confirmPop = confirm("Do you want to Add?");
        if (confirmPop == false) {
            return false;
        }
        // console.log(this.journalData);
        // for (var i = 0; i < this.journalList.length; i++) {

        //     if (this.journalList[i]['f018ffundCode'] == this.journalData['f018ffundCode']) {
        //         alert("Fund has already been allocated, Change Fund");
        //         return false;
        //     }
        //     if (this.journalList[i]['f018faccountCode'] == this.journalData['f018faccountCode']) {
        //         alert("Account Code has already been allocated, Change Account Code");
        //         return false;
        //     }
        // }
        this.debit = 0;
        this.credit = 0;
        var dataofCurrentRow = this.journalData;
        this.journalData = {};
        this.journalData['f018fdrAmount'] = 0;
        this.journalData['f018fcrAmount'] = 0;
        this.journalList.push(dataofCurrentRow);
        console.log(this.journalList);

        for (var i = 0; i < this.journalList.length; i++) {
            if (this.journalList[i]['f018fdrAmount'] == undefined) {
                this.journalList[i]['f018fdrAmount'] = 0;
            } if (this.journalList[i]['f018fcrAmount'] == undefined) {
                this.journalList[i]['f018fcrAmount'] = 0;
            }
            this.debit = (this.ConvertToInt(this.debit) + this.ConvertToInt(this.journalList[i]['f018fdrAmount']));
            this.credit = (this.ConvertToInt(this.credit) + this.ConvertToInt(this.journalList[i]['f018fcrAmount']));
        }
        console.log(this.journalList);
        this.showCrReadonly = false;
        this.showDrReadonly = false;
        this.showIcons();
        this.onBlurMethod();
        this.modalService.dismissAll();

    }
    addjournal() {
        console.log(this.journalData);
    }
    goToListPage() {
    }
    onlyNumberKey(event) {
        if (event.charCode == 46) {
        } else {
            return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
        }
    }
    maximize(){
        $("#myModal").addClass("modal");
        $("#overlay1").show();
        $(".maximize-icon").addClass("hidden");
        $(".minimize-icon").removeClass("hidden");
        $(".modal-backdrop").show();

    }
    minimize(){
        $("#myModal").removeClass("modal");
        $("#overlay1").hide();
        $(".maximize-icon").removeClass("hidden");
        $(".minimize-icon").addClass("hidden");
        $(".modal-backdrop").hide();
        $('#myModal').show();

    }
}