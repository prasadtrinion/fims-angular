import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { JournalService } from '../service/journal.service'
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { environment } from '../../../environments/environment';

 
@Component({
    selector: 'journal',
    templateUrl: 'journal.component.html'
})

export class JournalComponent implements OnInit {
    journalList = [];
    journalData = {};
    id: number;
    downloadUrl: string = environment.api.downloadbase;

    constructor(

        private JournalService: JournalService,
        private route: ActivatedRoute,
        private router: Router,
        private spinner: NgxSpinnerService

    ) { }

    ngOnInit() {
        this.spinner.show();

        this.JournalService.getItems().subscribe(
            data => {
                this.spinner.hide();

                let activityData = [];
                activityData = data['result']['data'];
                for (var i = 0; i < activityData.length; i++) {
                    if (activityData[i]['f017fapprovedStatus'] == 0) {
                        activityData[i]['f017fapprovedStatus'] = 'Pending';
                    } else if (activityData[i]['f017fapprovedStatus'] == 1) {
                        activityData[i]['f017fapprovedStatus'] = 'Approved';
                    } else if (activityData[i]['f017fapprovedStatus'] == 2) {
                        activityData[i]['f017fapprovedStatus'] = 'Rejected';
                    }
                    else if (activityData[i]['f017fapprovedStatus'] == 3) {
                        activityData[i]['f017fapprovedStatus'] = 'Reversed';
                    }
                }
                this.journalList = activityData;
            }, error => {
                console.log(error);
            });
    }
    addPrint(id) {
        let newobj = {};
        newobj['journalId'] = id;
        var confirmPop = confirm("Do you want to Print?");
        if (confirmPop == false) {
            return false;
        }
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.JournalService.insertjournalPrintItems(newobj).subscribe(
            data => {
                // alert('KWAP has been saved');
                window.open(this.downloadUrl + data['name']);
                this.router.navigate(['generalledger/journal']);

            }, error => {
                console.log(error);
            });
    }

}