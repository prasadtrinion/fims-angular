import { Injectable } from '@angular/core';
import { HttpClientModule,HttpHeaders, HttpClient} from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({ 'X-Auth-Client': 'qazwsx123',
    'Content-Type': 'application/json' })
    
  };
@Injectable()

export class JournalmonthlysummaryService {
    cashbankinurl: string = environment.api.base + environment.api.endPoints.cashbook;
    url: string = environment.api.base + environment.api.endPoints.journalmonthlysummary
    constructor(private httpClient: HttpClient) {    
    }
    getItems() {
        return this.httpClient.get(this.url,httpOptions);
    }
    callMonthlySummaryAPI(month){
        return this.httpClient.get(environment.api.base+'monthlySummary/'+month,httpOptions);
    }
    cashbankin(){
        return this.httpClient.get(this.cashbankinurl,httpOptions);
    }
    getItemsDetail(id) {
        return this.httpClient.get(this.url+'/'+id, httpOptions);
    }
    insertJournalmonthlysummaryItems(journalData): Observable<any> {
        return this.httpClient.post(this.url, journalData,httpOptions);
    }
    updateJournalmonthlysummaryItems(financialyearData,id): Observable<any> {
        return this.httpClient.put(this.url+'/'+id, financialyearData,httpOptions);
    }
   
}