import { Injectable } from '@angular/core';
import { HttpClientModule,HttpHeaders, HttpClient} from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({ 'X-Auth-Client': 'qazwsx123',
    'Content-Type': 'application/json' })
    
  };
@Injectable()

export class JournalreversalService {

    url: string = environment.api.base + environment.api.endPoints.journal 
    getJournalsurl: string = environment.api.base + environment.api.endPoints.getJournals 

    journalReversalurl: string = environment.api.base + environment.api.endPoints.journalReversal
    constructor(private httpClient: HttpClient) {    
    }
    getItems() {
        return this.httpClient.get(this.getJournalsurl+'/'+1,httpOptions);
    }
    getItemsDetail(id) {
        return this.httpClient.get(this.url+'/'+id, httpOptions);
    }
    insertJournalreversalItems(journalapprovalData): Observable<any> {
        return this.httpClient.post(this.url, journalapprovalData,httpOptions);
    }
    updateJournalreversalItems(journalreversalData,id): Observable<any> {
        return this.httpClient.put(this.journalReversalurl+'/'+id, journalreversalData,httpOptions);
    }
    updateJournal(journalreversalData): Observable<any> {
        return this.httpClient.post(this.journalReversalurl, journalreversalData,httpOptions);
    }
}