import { Injectable } from '@angular/core';
import { HttpClientModule, HttpHeaders, HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({
        'X-Auth-Client': 'qazwsx123',
        'Content-Type': 'application/json'
    })

};

@Injectable()

export class OpeningbalanceapprovalService {
    url: string = environment.api.base + environment.api.endPoints.openingbalanceApprove
    approveurl: string = environment.api.base + environment.api.endPoints.openingBalanceApprovalStatus
    constructor(private httpClient: HttpClient) {

    }
    getItems() {
        return this.httpClient.get(this.url + '/' + 0, httpOptions);
    }
    getItemsDetail(id) {
        return this.httpClient.get(this.url + '/' + id, httpOptions);
    }
    insertOpeningbalanceapprovalItems(openingbalanceapprovalData): Observable<any> {
        return this.httpClient.post(this.url, openingbalanceapprovalData, httpOptions);
    }
    updateOpeningbalanceapprovalItems(openingbalanceapprovalData, id): Observable<any> {
        return this.httpClient.put(this.approveurl + '/' + id, openingbalanceapprovalData, httpOptions);
    }
    updateOpeningbalance(openingbalanceapprovalData): Observable<any> {
        return this.httpClient.post(this.approveurl, openingbalanceapprovalData, httpOptions);
    }
}