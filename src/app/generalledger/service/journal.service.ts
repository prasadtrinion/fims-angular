import { Injectable } from '@angular/core';
import { HttpClientModule, HttpHeaders, HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({
        'X-Auth-Client': 'qazwsx123',
        'Content-Type': 'application/json'
    })

};
@Injectable()

export class JournalService {

    url: string = environment.api.base + environment.api.endPoints.journal;
    printurl: string = environment.api.base + environment.api.endPoints.journalEntryPrint;
    deleteurl: string = environment.api.base + environment.api.endPoints.delete;
    RefNumUrl: string = environment.api.base + environment.api.endPoints.generateNumber;
    constructor(private httpClient: HttpClient) {
    }
    getItems() {
        return this.httpClient.get(this.url, httpOptions);
    }
    getItemsDetail(id) {
        return this.httpClient.get(this.url + '/' + id, httpOptions);
    }
    insertjournalItems(journalData): Observable<any> {
        return this.httpClient.post(this.url, journalData, httpOptions);
    }
    insertjournalPrintItems(journalData): Observable<any> {
        return this.httpClient.post(this.printurl, journalData, httpOptions);
    }
    deletejournalItems(journalData): Observable<any> {
        return this.httpClient.post(this.url, journalData, httpOptions);
    }
    updateJournalItems(journalData,id): Observable<any> {
        return this.httpClient.put(this.url+'/'+id, journalData, httpOptions);
    }

    getRefNumber(typeObject) {
        return this.httpClient.post(this.RefNumUrl, typeObject,httpOptions);
    }
}