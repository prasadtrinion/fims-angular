import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { JournalmonthlysummaryService } from '../service/journalmonthlysummary.service'
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'Journalmonthlysummary',
    templateUrl: 'journalmonthlysummary.component.html'
})
export class JournalmonthlysummaryComponent implements OnInit {
    journalmonthlysummaryList =  [];
    journalmonthlysummaryData = {};
    id: number;
    constructor( 
        private JournalmonthlysummaryService: JournalmonthlysummaryService,
        private spinner: NgxSpinnerService

    ) { }
    ngOnInit() { 
        this.spinner.show();

        this.JournalmonthlysummaryService.getItems().subscribe(
            data => {
                this.spinner.hide();

                this.journalmonthlysummaryList = data['result'];
                for(var i=0;i<this.journalmonthlysummaryList.length;i++){
                    switch(this.journalmonthlysummaryList[i]['f018fmonth']){
                        case '1': this.journalmonthlysummaryList[i]['month'] = 'January';break;
                        case '2': this.journalmonthlysummaryList[i]['month'] = 'February';break;
                        case '3': this.journalmonthlysummaryList[i]['month'] = 'March';break;
                        case '4': this.journalmonthlysummaryList[i]['month'] = 'April';break;
                        case '5': this.journalmonthlysummaryList[i]['month'] = 'May';break;
                        case '6': this.journalmonthlysummaryList[i]['month'] = 'June';break;
                        case '7': this.journalmonthlysummaryList[i]['month'] = 'July';break;
                        case '8': this.journalmonthlysummaryList[i]['month'] = 'August';break;
                        case '9': this.journalmonthlysummaryList[i]['month'] = 'September';break;
                        case '10': this.journalmonthlysummaryList[i]['month'] = 'October';break;
                        case '11': this.journalmonthlysummaryList[i]['month'] = 'November';break;
                        case '12': this.journalmonthlysummaryList[i]['month'] = 'December';break;
                    }
                }
        }, error => {
            console.log(error);
        });
    }
    callMonthlySummary(){ 
        var currentDate = new Date();
        var month = currentDate.getMonth()+1;
        this.JournalmonthlysummaryService.callMonthlySummaryAPI(month).subscribe(
            data => {
                alert('Monthly Summary processed successfully!!');
                this.JournalmonthlysummaryService.getItems().subscribe(
                    data => {
                        this.journalmonthlysummaryList = data['result'];
                        for(var i=0;i<this.journalmonthlysummaryList.length;i++){
                            switch(this.journalmonthlysummaryList[i]['f018fmonth']){
                                case '1': this.journalmonthlysummaryList[i]['month'] = 'January';break;
                                case '2': this.journalmonthlysummaryList[i]['month'] = 'February';break;
                                case '3': this.journalmonthlysummaryList[i]['month'] = 'March';break;
                                case '4': this.journalmonthlysummaryList[i]['month'] = 'April';break;
                                case '5': this.journalmonthlysummaryList[i]['month'] = 'May';break;
                                case '6': this.journalmonthlysummaryList[i]['month'] = 'June';break;
                                case '7': this.journalmonthlysummaryList[i]['month'] = 'July';break;
                                case '8': this.journalmonthlysummaryList[i]['month'] = 'August';break;
                                case '9': this.journalmonthlysummaryList[i]['month'] = 'September';break;
                                case '10': this.journalmonthlysummaryList[i]['month'] = 'October';break;
                                case '11': this.journalmonthlysummaryList[i]['month'] = 'November';break;
                                case '12': this.journalmonthlysummaryList[i]['month'] = 'December';break;
                            }
                        }
                }, error => {
                    console.log(error);
                });
                //this.journalmonthlysummaryList = data['result'];
        }, error => {
            console.log(error);
        });
    }
}