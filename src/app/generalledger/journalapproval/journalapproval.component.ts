import {Injector} from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { JournalapprovalService } from '../service/journalapproval.service'
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'Journalapproval',
    templateUrl: 'journalapproval.component.html'
})

export class JournalapprovalComponent implements OnInit {
    journalapprovalList =  [];
    journalapprovalData = {};
    selectAllCheckbox = true;

    constructor(
        
        private JournalapprovalService: JournalapprovalService,
        private spinner: NgxSpinnerService

    ) { }

    ngOnInit() {    
       this.getListData();
    }
    getListData(){
        this.spinner.show();

        this.JournalapprovalService.getItemsDetail(0).subscribe(
            data => {
                this.spinner.hide();
                this.journalapprovalList = data['result'];
        }, error => {
            console.log(error);
        });
    }
    journalApprovalListData() {
        console.log(this.journalapprovalList);
        var journalIds = [];
        for (var i = 0; i < this.journalapprovalList.length; i++) {
            if(this.journalapprovalList[i].f017fapprovedStatus==true) {
                journalIds.push(this.journalapprovalList[i].f017fid);
            }
        }
        if(journalIds.length<1) {
            alert(" select atleast one Journal to Approve");
            return false;
        }
        var confirmPop = confirm("Do you want to Approve?");
        if (confirmPop == false) {
            return false;
        }
        var journalApprovalUpdate = {};
        journalApprovalUpdate['id'] = journalIds;
        journalApprovalUpdate['reason'] = '';
        // journalApprovalUpdate['reason'] = this.journalapprovalData['reason'];
        this.JournalapprovalService.updateJournal(journalApprovalUpdate).subscribe(
            data => {
                this.getListData();
                this.journalapprovalData['reason']='';
                alert("Journal Entry has been Approved Successfully");
        }, error => {
            console.log(error);
        });
    }
    regectJournalApprovalListData() {
        console.log(this.journalapprovalList);
        var approvalArray = [];
        console.log(this.selectAllCheckbox);
        for (var i = 0; i < this.journalapprovalList.length; i++) {
            if (this.journalapprovalList[i].f017fapprovedStatus == true) {
                approvalArray.push(this.journalapprovalList[i]['f017fid']);
            }
        }
        if(approvalArray.length<1) {
            alert(" select atleast one Journal to Reject");
            return false;
        }
        if (this.journalapprovalData['reason'] == undefined) {
            alert('Enter the Description');
            return false;
        }
        var confirmPop = confirm("Do you want to Reject?");
        if (confirmPop == false) {
            return false;
        }
        var budgetActivityDataObject = {}
        budgetActivityDataObject['id'] = approvalArray;
        budgetActivityDataObject['status'] = '2';
        budgetActivityDataObject['reason'] = this.journalapprovalData['reason'];
         this.JournalapprovalService.updateJournal(budgetActivityDataObject).subscribe(
            data => {
                this.getListData();
                this.journalapprovalData['reason']='';
                alert("Jountry Entry has been Rejected Successfully");
            }, error => {
                console.log(error);
            });
    }
    selectAll() {
        console.log("asdf");
        console.log(this.selectAllCheckbox);
        for (var i = 0; i < this.journalapprovalList.length; i++) {
                this.journalapprovalList[i].f017fapprovedStatus = this.selectAllCheckbox;
        }
      }  
}