import { Injector, ViewEncapsulation } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { TabsModule } from 'ngx-bootstrap';
import { LedgerService } from '../service/ledger.service'
import { ActivatedRoute, Router } from '@angular/router';
import { GroupsService } from '../../generalsetup/service/groups.service';
import { StateService } from '../../generalsetup/service/state.service'
import { CountryService } from '../../generalsetup/service/country.service'
import { BankService } from '../../generalsetup/service/bank.service'


@Component({
    selector: 'ledger',
    templateUrl: 'ledger.component.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./ledger.component.css']
})

export class LedgerComponent implements OnInit {
    ledgerList = [];
    groupsList = [];
    bankList = [];
    countryList = [];
    stateList = [];
    groupsData = {};
    ledgerData = {};
    ledgerAddressData = {};
    ledgerBankDetails = {};
    ledgerRegistrationDetails = {};

    id: number;

    constructor(
        private groupsService: GroupsService,
        private ledgerService: LedgerService,
        private bankService: BankService,
        private StateService: StateService,
        private countryService: CountryService,
        private route: ActivatedRoute,
        private router: Router


    ) { }

    ngOnInit() {

        //group dropdown
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.groupsService.getItems().subscribe(
            data => {
                this.groupsList = data['result']['data'];
            }, error => {
                console.log(error);
            });
        //Country dropdown    
        this.countryService.getItems().subscribe(
            data => {
                this.countryList = data['result']['data'];
            }, error => {
                console.log(error);
            });
        // State dropdown    
        this.StateService.getItems().subscribe(
            data => {
                this.stateList = data['result']['data'];
            }, error => {
                console.log(error);
            });
        //Bank dropdown    
        this.bankService.getItems().subscribe(
            data => {
                this.bankList = data['result']['data'];
            }, error => {
                console.log(error);
            });
    }

    addledger() {
        console.log(this.ledgerData);
        this.ledgerData['address-details'] = this.ledgerAddressData;
        this.ledgerData['registration-details'] = this.ledgerRegistrationDetails;
        this.ledgerData['bank-details'] = this.ledgerBankDetails

        console.log(this.ledgerData);


        this.ledgerService.insertledgerItems(this.ledgerData).subscribe(
            data => {
                alert('Data saved successfully');
                this.ledgerList = data['todos'];

            }, error => {
                console.log(error);
            });

    }
}