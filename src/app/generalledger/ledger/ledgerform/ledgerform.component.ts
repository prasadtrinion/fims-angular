import {Injector} from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { LedgerService } from '../../service/ledger.service'
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'ledgerFormComponent',
    templateUrl: 'ledgerform.component.html'
})

export class LedgerFormComponent implements OnInit {
    ledgerList =  [];
    ledgerData = {};
    id: number;

    constructor(
        
        private ledgerService: LedgerService,
        private route: ActivatedRoute,
    ) { }

    ngOnInit() { 
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        console.log(this.id);
        
        if(this.id>0) {
            this.ledgerService.getItemsDetail(this.id).subscribe(
                data => {
                    console.log(data);
                    
                    this.ledgerData['f014fid'] = data['result']['f014fid'];

    
            }, error => {
                console.log(error);
            });
        }
        

        console.log(this.id);
    }
    addGlCode(){
           console.log(this.ledgerData);
           if(this.id>0) {

           } else {
        this.ledgerService.insertledgerItems(this.ledgerData).subscribe(
                    data => {
                        this.ledgerList = data['todos'];
                }, error => {
                    console.log(error);
                });
           }
       

    }
    goToListPage(){
        
    }
}