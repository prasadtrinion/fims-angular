import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { MaterialModule } from '../material/material.module';
import { HttpClientModule, HttpClient,HTTP_INTERCEPTORS } from '@angular/common/http';
import { DataTableModule } from "angular-6-datatable";

import { FinancialyearComponent } from './financialyear/financialyear.component';
import { FinancialyearFormComponent } from './financialyear/financialyearform/financialyearform.component';

import { MonthlyprocessComponent } from './monthlyprocess/monthlyprocess.component';
import { MonthlyprocessFormComponent } from './monthlyprocess/monthlyprocessform/monthlyprocessform.component';
import { YearlyprocessFormComponent } from './yearlyprocess/yearlyprocessform/yearlyprocessform.component';
import { YearlyprocessComponent } from './yearlyprocess/yearlyprocess.component';
import { YearlyprocessService } from './service/yearlyprocess.service';

import { JournalComponent } from './journal/journal.component';
import { JournalFormComponent } from './journal/journalform/journalform.component';
import { JournalmonthlysummaryComponent } from './journalmonthlysummary/journalmonthlysummary.component';
import { JournalapprovalComponent } from './journalapproval/journalapproval.component';
import { JournalreversalComponent } from './journalreversal/journalreversal.component';
import { JournalreversalformComponent } from './journalreversal/journalreversalform/journalreversalform.component';

import { CashbankinComponent } from './cashbankin/cashbankin.component';
import { OpeningbalanceComponent } from './openingbalance/openingbalance.component';
import { OpeningbalanceFormComponent } from './openingbalance/openingbalanceform/openingbalanceform.component';

import { LedgerComponent } from './ledger/ledger.component';
import { LedgerFormComponent } from './ledger/ledgerform/ledgerform.component';

import { TaxsetupcodeService } from './service/taxsetupcode.service'
import { JournalService } from './service/journal.service'
import { JournalapprovalService } from './service/journalapproval.service';
import { JournalreversalService } from './service/journalreversal.service';
import { JournalmonthlysummaryService } from './service/journalmonthlysummary.service';
import { OpeningbalanceService } from './service/openingbalance.service';
import { GlcodegenerationService } from './service/glcodegeneration.service';
import { LedgerService } from './service/ledger.service';
import { OpeningbalanceapprovalComponent } from './openingbalancepproval/openingbalanceapproval.component';
import { StaffService } from '../loan/service/staff.service';
import { PurchaserequistionentryService } from "../procurement/service/purchaserequistionentry.service";

import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { GeneralledgerRoutingModule } from './generalledger.router.module';
import { UserService } from '../generalsetup/service/user.service';

import { GroupsService } from '../generalsetup/service/groups.service';
import { CountryService } from '../generalsetup/service/country.service';
import { StateService } from '../generalsetup/service/state.service';
import { BankService } from '../generalsetup/service/bank.service';
import { FundService } from '../generalsetup/service/fund.service';
import { FinancialyearService } from '../generalledger/service/financialyear.service';
import { DepartmentService } from '../generalsetup/service/department.service';
import { ActivitycodeService } from '../generalsetup/service/activitycode.service';
import { AccountcodeService } from '../generalsetup/service/accountcode.service';
import { GlcodeService } from '../generalledger/../generalsetup/service/glcode.service'
import { MonthlyprocessService } from './service/monthlyprocess.service';
import { OpeningbalanceapprovalService } from './service/openingbalanceapproval.service'

import { JournalapprovedataComponent } from "./journalapprovedata/journalapprovedata.component";
import { JournalapprovedataService } from "./service/journalapprovedata.service";

import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { JwtInterceptor } from '../_helpers/jwt.interceptors';

import { SharedModule } from "../shared/shared.module";
import { NgSelectModule } from '@ng-select/ng-select';




@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        MaterialModule,
        GeneralledgerRoutingModule,
        HttpClientModule,
        TabsModule.forRoot(),
        Ng2SearchPipeModule,
        HttpClientModule,
        DataTableModule,
        FontAwesomeModule,
        SharedModule,
        NgSelectModule
    ],
    exports: [],
    declarations: [

        JournalComponent,
        JournalapprovalComponent,
        JournalFormComponent,
        OpeningbalanceComponent,
        OpeningbalanceFormComponent,
        LedgerComponent,
        LedgerFormComponent,
        JournalmonthlysummaryComponent,
        JournalreversalComponent,
        FinancialyearComponent,
        FinancialyearFormComponent,
        MonthlyprocessComponent,
        MonthlyprocessFormComponent,
        JournalreversalformComponent,
        OpeningbalanceapprovalComponent,
        YearlyprocessFormComponent,
        YearlyprocessComponent,
        CashbankinComponent,
        JournalapprovedataComponent

    ],
    providers: [
        TaxsetupcodeService,
        JournalService,
        OpeningbalanceService,
        FinancialyearService,
        LedgerService,
        GroupsService,
        CountryService,
        BankService,
        GlcodegenerationService,
        StateService,
        FundService,
        DepartmentService,
        ActivitycodeService,
        AccountcodeService,
        GlcodeService,
        JournalapprovalService,
        UserService,
        JournalmonthlysummaryService,
        MonthlyprocessService,
        OpeningbalanceapprovalService,
        JournalreversalService,
        JournalapprovedataService,
        PurchaserequistionentryService,
        StaffService,
        YearlyprocessService,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: JwtInterceptor,
            multi: true
          }
        
    ],
})
export class GeneralledgerModule { }
