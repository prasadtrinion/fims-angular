import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { OpeningbalanceapprovalService } from '../service/openingbalanceapproval.service'
import { NgxSpinnerService } from 'ngx-spinner';


@Component({
    selector: 'Openingbalanceapproval',
    templateUrl: 'openingbalanceapproval.component.html'
})

export class OpeningbalanceapprovalComponent implements OnInit {
    openingbalanceapprovalList = [];
    openingbalanceapprovalData = {};
    selectAllCheckbox = false;
    id: number;

    constructor(

        private OpeningbalanceapprovalService: OpeningbalanceapprovalService,
        private spinner: NgxSpinnerService


    ) { }

    ngOnInit() {
        this.getListData();
    }

    getListData() {
        this.spinner.show();

        this.OpeningbalanceapprovalService.getItems().subscribe(
            data => {
                this.spinner.hide();

                this.openingbalanceapprovalList = data['result'];
            }, error => {
                console.log(error);
            });
    }

    openingbalanceApprovalListData() {
        console.log(this.openingbalanceapprovalList);
        var openingbalanceApprovalIds = [];
        for (var i = 0; i < this.openingbalanceapprovalList.length; i++) {
            if (this.openingbalanceapprovalList[i].approvalstatus == true) {
                openingbalanceApprovalIds.push(this.openingbalanceapprovalList[i].f016fidFinancialyear);
            }
        }
        if (openingbalanceApprovalIds.length < 1) {
            alert("Select atleast one Opening Balance to Approve");
            return false;
        }
        var confirmPop = confirm("Do you want to Approve?");
        if (confirmPop == false) {
            return false;
        }
        var openingbalanceApprovalUpdate = {};
        openingbalanceApprovalUpdate['id'] = openingbalanceApprovalIds;
        this.OpeningbalanceapprovalService.updateOpeningbalance(openingbalanceApprovalUpdate).subscribe(
            data => {
                this.getListData();
                alert("Opening Balance has been approved successfully");
            }, error => {
                console.log(error);
            });
    }
    regectJournalApprovalListData() {
        console.log(this.openingbalanceapprovalList);
        var openingbalanceApprovalIds = [];
        for (var i = 0; i < this.openingbalanceapprovalList.length; i++) {
            if (this.openingbalanceapprovalList[i].approvalstatus == true) {
                openingbalanceApprovalIds.push(this.openingbalanceapprovalList[i].f016fidFinancialyear);
            }
        }
        if (openingbalanceApprovalIds.length < 1) {
            alert(" Select atleast one Opening Balance to Reject");
            return false;
        }
       
        if (this.openingbalanceapprovalData['reason'] == undefined) {
            alert(' Enter the Description');
            return false;
        }
        
        var confirmPop = confirm("Do you want to Reject?");
        if (confirmPop == false) {
            return false;
        }
        var budgetActivityDataObject = {}
        budgetActivityDataObject['id'] = openingbalanceApprovalIds;
        budgetActivityDataObject['status'] = '2';
        budgetActivityDataObject['reason'] = this.openingbalanceapprovalData['reason'];

        this.OpeningbalanceapprovalService.updateOpeningbalance(budgetActivityDataObject).subscribe(
            data => {
                this.getListData();
                alert("Opening Balance has been Rejected ! ");
                this.openingbalanceapprovalData['reason'] = '';
            }, error => {
                console.log(error);
            });
    }
}