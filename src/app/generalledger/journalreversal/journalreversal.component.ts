import {Injector} from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { JournalreversalService } from '../service/journalreversal.service';
import { NgxSpinnerService } from 'ngx-spinner';


@Component({
    selector: 'JournalreversalComponent',
    templateUrl: 'journalreversal.component.html'
})

export class JournalreversalComponent implements OnInit {
    journalreversalList =  [];
    journalreversalData = {};
    selectAllCheckbox = true;

    constructor(
        
        private JournalreversalService: JournalreversalService,
        private spinner: NgxSpinnerService
    ) { }

    ngOnInit() {    
       this.getListData();
    }
    getListData(){
        this.spinner.show();

        this.JournalreversalService.getItems().subscribe(
            data => {
                this.spinner.hide();
                this.journalreversalList = data['result'];
        }, error => {
            console.log(error);
        });
    }
    
    journalReversalData() {

        var journalIds = [];
        for (var i = 0; i < this.journalreversalList.length; i++) {

            if (this.journalreversalList[i]['journalApproval']==true) {
                journalIds.push(this.journalreversalList[i]['f017fid']);
            }
            
        } 
        if(journalIds.length<1) {
            alert(" select atleast one Journal to Reverse");
            return false;
        }
        if (this.journalreversalData['description'] == undefined) {
            alert('Please Enter the Description');
            return false;
        }
        var confirmPop = confirm("Do you want to Reverse?");
        if (confirmPop == false) {
            return false;
        }
        var journalreversalUpdate = {};
        journalreversalUpdate['id'] = journalIds;
        journalreversalUpdate['description'] = this.journalreversalData['description'];


        console.log(journalreversalUpdate);
        this.JournalreversalService.updateJournal(journalreversalUpdate).subscribe(
            data => {                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     
                this.getListData();
                this.journalreversalData['description']='';
                alert("Reversed Successfully")
        }, error => {
            console.log(error);
        });
    }
}