import {Injector} from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { JournalreversalService } from '../../service/journalreversal.service';
import { GlcodegenerationService } from '../../service/glcodegeneration.service';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../../../generalsetup/service/user.service';

@Component({
    selector: 'JournalreversalformComponent',
    templateUrl: 'journalreversalform.component.html'
})

export class JournalreversalformComponent implements OnInit {
    journalList =  [];
    journalData = {};
    journalDataheader = {};
    glList=[];
    glData={};
    glDetailData = [];
    journalDetail={};
    glNewCode={}
    userList=[];
    showCrReadonly:boolean;
    showDrReadonly:boolean;
    id: number;
    ajaxCount : number;


    constructor(
        
        private journalService: JournalreversalService,
        private GlcodegenerationService:GlcodegenerationService,
        private userService:UserService,
        private route: ActivatedRoute,
        private router: Router
    ) { }

    ngOnInit() { 
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.ajaxCount = 0;
        this.ajaxCount++;
        this.userService.getItems().subscribe(
            data => {
                this.userList = data['result']['data'];
                this.GlcodegenerationService.getItems().subscribe(
                    data => {
                        this.glList = data['result']['data'];
                        if(this.id>0) {
                            this.journalService.getItemsDetail(this.id).subscribe(
                                data => {
                                    this.journalList = data['result'];
                                    this.journalDataheader['f017fdateOfTransaction'] = data['result'][0].f017fdateOfTransaction;
                                    this.journalDataheader['f017fdescription'] = data['result'][0].f017fdescription;
                                    this.journalDataheader['f017fapprover'] = data['result'][0].f017fapprover;
                                    this.journalDataheader['f017fid'] = data['result'][0].f017fid;

                                    console.log(this.journalList);
                                }, error => {
                                    console.log(error);
                                });
                        }
                    }, error => {
                        console.log(error);
                    });

        }, error => {
            console.log(error);
        });         
            
    }

    indexTracker(index: number, value: any) {
        return index;
      }

    // saveJournal(){


    //     let journalObject = {};
    //     this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
    //     if(this.id>0) {
    //         journalObject['f017fid'] = this.id;
    //     }
    //     journalObject['f017fdateOfTransaction'] = this.journalDataheader['f017fdateOfTransaction'];
    //     journalObject['f017fdescription'] = this.journalDataheader['f017fdescription'];
    //     journalObject['f017fapprover'] = this.journalDataheader['f017fapprover'];
    //     journalObject['f017fapprovedStatus'] = 0;
    //     journalObject['f017fmodule'] = "Manual";
        
    //     journalObject['journal-details'] = this.journalList;

    //     if(this.id>0) {
    //         this.journalService.updateJournalreversalItems(journalObject).subscribe(
    //             data => {
    //                 this.router.navigate(['generalledger/journal']);
    
    //             }, error => {
    //                 console.log(error);
    //             });
    //     } else {
    //         this.journalService.insertjournalItems(journalObject).subscribe(
    //             data => {
    //                 this.router.navigate(['generalledger/journal']);
    
    //             }, error => {
    //                 console.log(error);
    //             });
    //     }
       


    // }
    ConvertToInt(val) {
        return parseInt(val);
    }

    disableFieldsOfDRCR() {
        this.showCrReadonly = false;
        this.showDrReadonly = false;

        console.log(this.journalData);
        if(this.journalData['f018fdrAmount']==undefined ||
           this.journalData['f018fdrAmount']==0 ||
           this.journalData['f018fdrAmount']=='') {
               this.showDrReadonly = true;
           }
           if(this.journalData['f018fcrAmount']==undefined ||
           this.journalData['f018fcrAmount']==0 ||
           this.journalData['f018fcrAmount']=='') {
               this.showCrReadonly = true;
           }

        if(this.showCrReadonly==true && this.showDrReadonly==true) {
            this.showCrReadonly = false;
            this.showDrReadonly = false;    
        }

           console.log(this.showCrReadonly);
           console.log(this.showDrReadonly);

    }

    
    deleteJournal(object) {
     
        var cnf = confirm("Do you really want to delete");
        if(cnf==true){
            console.log(this.journalList);
            var index = this.journalList.indexOf(object);;
            if (index > -1) {
                this.journalList.splice(index, 1);
            }
           // this.onBlurMethod();
        }
    }
    
    addjournallist() {
        console.log(this.journalData);
        // if(this.journalData['f018fidGlcode']==undefined) {
        //     alert('Select the GL Code');
        //     return false;;
        // }
        // if(this.journalData['f018freferenceNumber']==undefined) {
        //     alert('Enter Ref Number');
        //     return false;;
        // }
 
        //  var totalamount = (this.ConvertToInt(this.journalData['f072fquantity']))*(this.ConvertToInt(this.journalData['f072fprice']));
        //  var gstamount = (this.ConvertToInt(this.journalData['f072fgst']))/100*(totalamount);
        //  this.journalData['f072ftotal'] = gstamount + totalamount;
         //this.invoiceList.push(totalamount);
         var dataofCurrentRow = this.journalData;
         this.journalData = {};
         this.journalList.push(dataofCurrentRow);
         //this.onBlurMethod();
         console.log(this.journalList);
         this.showCrReadonly = false;
         this.showDrReadonly = false;    
     }

    addjournal(){
           console.log(this.journalData);
       

    }
    goToListPage(){
        
    }
}