import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { JournalmonthlysummaryService } from '../service/journalmonthlysummary.service'
@Component({
    selector: 'Cashbankin',
    templateUrl: 'cashbankin.component.html'
})
export class CashbankinComponent implements OnInit {
    journalmonthlysummaryList =  [];
    journalmonthlysummaryData = {};
    id: number;
    constructor( 
        private JournalmonthlysummaryService: JournalmonthlysummaryService
    ) { }
    ngOnInit() { 
        this.JournalmonthlysummaryService.cashbankin().subscribe(
            data => {
                this.journalmonthlysummaryList = data['result']['data'];
        }, error => {
            console.log(error);
        });
    }
    
}