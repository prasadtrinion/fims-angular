import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { lawyersService } from '../service/lawyers.service';

@Component({
    selector: 'listoflawyers',
    templateUrl: 'listoflawyers.component.html'
})

export class ListoflawyersComponent implements OnInit {
    lawyersList = [];
    lawyesrData = {};

    constructor(

        private LawyersService: lawyersService
    ) { }

    ngOnInit() {

        this.LawyersService.getItems().subscribe(
            data => {
                this.lawyersList = data['todos'];
            }, error => {
                console.log(error);
            });
    }
    addlawyers() {
        console.log(this.lawyesrData);
        this.LawyersService.insertlawyerItems(this.lawyesrData).subscribe(
            data => {
                this.lawyersList = data['todos'];
            }, error => {
                console.log(error);
            });

    }
}