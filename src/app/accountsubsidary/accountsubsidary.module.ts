import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HttpClient,HTTP_INTERCEPTORS } from '@angular/common/http';

import { MaterialModule } from '../material/material.module';

import { ListoflawyersComponent } from './listoflawyers/listoflawyers.component';
import { SetupageingComponent } from './setupageing/setupageing.component';
import { SetupnotificationComponent } from './setupnotification/setupnotification.component';

import { AccountsubsidaryRoutingModule } from './accountsubsidary.router.module';
import { JwtInterceptor } from '../_helpers/jwt.interceptors';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        MaterialModule,
        AccountsubsidaryRoutingModule,
    ],
    exports: [],
    declarations: [
        ListoflawyersComponent,
        SetupageingComponent,
        SetupnotificationComponent,

    ],
    providers: [
        {
            provide: HTTP_INTERCEPTORS,
            useClass: JwtInterceptor,
            multi: true
          }
    ],
})
export class AccountsubsidaryModule { }
