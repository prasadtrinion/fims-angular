import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

 import { ListoflawyersComponent } from './listoflawyers/listoflawyers.component';
 import{ SetupageingComponent } from './setupageing/setupageing.component';
 import{ SetupnotificationComponent } from './setupnotification/setupnotification.component';


const routes: Routes = [
  { path: 'setupageing', component: SetupageingComponent },
  { path: 'setupnotification', component: SetupnotificationComponent },
  { path: 'listoflawyers', component: ListoflawyersComponent }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  
})
export class AccountsubsidaryRoutingModule { 
  
}