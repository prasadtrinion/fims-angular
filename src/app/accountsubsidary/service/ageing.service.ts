import { Injectable } from '@angular/core';
import { HttpClientModule, HttpClient} from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';

@Injectable()
export class ageingService {
    url: string = 'https://api.myjson.com/bins/19rtj2'

    constructor(private httpClient: HttpClient) { }
    
    getItems() {
        return this.httpClient.get(this.url);
    }
    
    insertageingItems(frequencyData) {
        return this.httpClient.post(this.url,frequencyData);
    }
}
