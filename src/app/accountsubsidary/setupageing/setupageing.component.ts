import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ageingService } from '../service/ageing.service'

@Component({
    selector: 'setupageing',
    templateUrl: 'setupageing.component.html'
})

export class SetupageingComponent implements OnInit {
    
        pageingList = [];
        pageingData = {};
    
        constructor(
    
            private AgeingService: ageingService
        ) { }
    
        ngOnInit() {
    
            this.AgeingService.getItems().subscribe(
                data => {
                    this.pageingList = data['todos'];
                }, error => {
                    console.log(error);
                });
        }
        addlawyers() {
            console.log(this.pageingData);
            this.AgeingService.insertageingItems(this.pageingData).subscribe(
                data => {
                    this.pageingList = data['todos'];
                }, error => {
                    console.log(error);
                });
    
        }
    }