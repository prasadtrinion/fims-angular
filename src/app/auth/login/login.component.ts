import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from "../auth.service";
import { ActivatedRoute, Router } from '@angular/router';

@Component({
    selector: 'login',
    templateUrl: 'login.component.html'
})

export class LoginComponent implements OnInit {
    loginForm: FormGroup;
    userData = {};
    id:number;
    error:string;
    token : any;
    link : any;
    moduleLinks = new Array();
    module : any;

    constructor(private fb: FormBuilder,private authService:AuthService, private route: ActivatedRoute,
        private router: Router) {
            
        this.loginForm = fb.group({
            email: ['', Validators.required],
            
            password: ['', [Validators.required, Validators.minLength(8)]]
        });
    }

    ngOnInit() {
        this.moduleLinks['budget'] = "/budget";
        this.moduleLinks['maintainence'] = "/generalsetup";
        this.moduleLinks['ar'] = "/accountsrecivable";
        this.moduleLinks['ap'] = "/accountspayable";
        this.moduleLinks['sf'] = "/studentfinance";
        this.moduleLinks['gl'] = "/generalledger";
        this.moduleLinks['investment'] = "/investment";
        this.moduleLinks['procurement'] = "/procurement";
        this.moduleLinks['loan'] = "/loan";
        this.moduleLinks['payroll'] = "/payroll";
        this.moduleLinks['assets'] = "/assets";

        //console.log(httpOptions);
        
     }

    login(){
        //console.log(this.userData);
        
        this.authService.login(this.userData).subscribe(
            data => {
                    //console.log(data);
                    if(data['status'] == 412){
                        this.error = "Password Mismatch";
                    }
                    else  if(data['status'] == 411){
                        this.error = "Pending For Approval";
                    }
                    else if(data['status'] == 410){
                        this.error = "Username Doesn't Exist";
                    }
                    else{
                        console.log(data);
                        localStorage.removeItem('staffId');
                        localStorage.removeItem('username');
                       sessionStorage.setItem('f014ftoken',data['token']);
                       localStorage.setItem('staffId',data['staffId']);
                       localStorage.setItem('username',data['username']);


                       this.authService.getModules(data['token']).subscribe(
                           data => {
                               
                               this.module = data['result']['data'][0]['f011fmodule'];
                               this.link = this.moduleLinks[this.module];
                           this.router.navigate([this.link]);
                                
                           }, error => {
                               console.log(error);
                           });
                         

                    }
                    
        }, error => {
            console.log(error);
        });
    }

}