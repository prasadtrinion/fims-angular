import { Injectable } from '@angular/core';
import { HttpClientModule,HttpHeaders, HttpClient} from '@angular/common/http';
import { environment} from '../../environments/environment';
import { Observable } from 'rxjs/Observable';

const httpOptions = {
    headers: new HttpHeaders({ 'X-Auth-Client': 'qazwsx123',
    'Content-Type': 'application/json' })
    
  };
@Injectable()
export class AuthService {
    url: string = environment.api.base + environment.api.endPoints.login;
    studenturl: string = environment.api.base + environment.api.endPoints.studentlogin;
    url1: string = environment.api.base + environment.api.endPoints.roleModule;
    supplierurl: string = environment.api.base + environment.api.endPoints.vendor;
    supplierlogin: string = environment.api.base + environment.api.endPoints.supplierlogin;
    CodeUrl: string = environment.api.base + environment.api.endPoints.generateNumber;
    checkEmail: string = environment.api.base + environment.api.endPoints.checkEmail;
    // httpOptions  = httpOptions;
    constructor(private httpClient: HttpClient) { 
        
    }

    login(userData): Observable<any> {
        return this.httpClient.post(this.url, userData, httpOptions);
    }
    vendorlogin(userData): Observable<any> {
        return this.httpClient.post(this.supplierlogin, userData, httpOptions);
    }
    checkemail(userData): Observable<any> {
        return this.httpClient.post(this.checkEmail, userData, httpOptions);
    }
    studentlogin(studentData): Observable<any> {
        return this.httpClient.post(this.studenturl, studentData, httpOptions);
    }

    register(userData): Observable<any> {
        return this.httpClient.post(this.url, userData, httpOptions);
    }

    isAuthenticated () {
        
    }

    getAuthToken () {
        return sessionStorage.getItem('token');
    }

    getModules(token) {
        return this.httpClient.get(this.url1+'/'+token,httpOptions);
    }
    insertSupplierregistrationItems(supplierData): Observable<any> {
        return this.httpClient.post(this.supplierurl, supplierData,httpOptions);
    }
    getCode(type): Observable<any> {
        return this.httpClient.post(this.CodeUrl,type,httpOptions);

    }
    getItems() {
        return this.httpClient.get(this.supplierurl,httpOptions);
    }

    getItemsDetail(id) {
        return this.httpClient.get(this.supplierurl+'/'+id,httpOptions);
    }

   

    updateSupplierregistrationItems(supplierData,id): Observable<any> {
        return this.httpClient.put(this.supplierurl+'/'+id, supplierData,httpOptions);
       }
}