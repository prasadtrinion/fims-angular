import { Injector } from '@angular/core';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from "../auth.service"
import { CountryService } from '../../generalsetup/service/country.service';
import { StateService } from '../../generalsetup/service/state.service';
import { LicenseService } from '../../procurement/service/license.service';
import { BankService } from '../../generalsetup/service/bank.service';
import { AlertService } from '../../_services/alert.service';
import { HttpClientModule,HttpHeaders, HttpClient} from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Component({
    selector: 'SupplierregistrationComponent',
    templateUrl: 'supplierregistration.component.html'
})

export class SupplierRegistrationComponent implements OnInit {

    vendorList = [];
    vendorData = {};

    vendorLicenseData = {};
    licenseDataList = [];

    vendorDataHeader = {};
    countryList = [];
    stateList = [];
    licenseList = [];
    bankList = [];
    ajaxcount = 0;
    file;
    emailError;
    VendorObject= {};
    id: number;
    downloadUrl: string = environment.api.downloadbase;
    constructor(

        private SupplierregistrationService: AuthService,
        private CountryService : CountryService,
        private LicenseService: LicenseService,
        private StateService: StateService,
        private BankService: BankService,
        private AlertService: AlertService,
        private route: ActivatedRoute,
        private router: Router,
        private httpClient: HttpClient

    ) { }

    ngDoCheck() {
        const change = this.ajaxcount;
        console.log(this.ajaxcount);
        if (this.ajaxcount == 0) {
            this.editFunction();
            this.ajaxcount = 10;
        }
    }
 
    editFunction() {
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.SupplierregistrationService.getItemsDetail(this.id).subscribe(
                data => {
                    this.licenseDataList = data['result'];
                    this.vendorDataHeader['f030fcompanyName'] = data['result'][0].f030fcompanyName;
                    this.vendorDataHeader['f030femail'] = data['result'][0].f030femail;
                    this.vendorDataHeader['f030faddress'] = data['result'][0].f030faddress;
                    this.vendorDataHeader['f030fphone'] = data['result'][0].f030fphone;
                    this.vendorDataHeader['f030fvendorCode'] = data['result'][0].f030fvendorCode;
                    this.vendorDataHeader['f030fstartDate'] = data['result'][0].f030fstartDate;
                    this.vendorDataHeader['f030fendDate'] = data['result'][0].f030fendDate;
                    this.vendorDataHeader['f030fcity'] = data['result'][0].f030fcity;
                    this.vendorDataHeader['f030fpostCode'] = data['result'][0].f030fpostCode;
                    this.vendorDataHeader['f030fstate'] = data['result'][0].f030fstate;
                    this.vendorDataHeader['f030fcountry'] = data['result'][0].f030fcountry;
                    this.vendorDataHeader['f030fwebsite'] = data['result'][0].f030fwebsite;
                    this.vendorDataHeader['f030fcontact1Name'] = data['result'][0].f030fcontact1Name;
                    this.vendorDataHeader['f030fcontact1Email'] = data['result'][0].f030fcontact1Email;
                    this.vendorDataHeader['f030fcontact1Phone'] = data['result'][0].f030fcontact1Phone;
                    this.vendorDataHeader['f030fcontact2Name'] = data['result'][0].f030fcontact2Name;
                    this.vendorDataHeader['f030fcontact2Email'] = data['result'][0].f030fcontact2Email;
                    this.vendorDataHeader['f030fcontact2Phone'] = data['result'][0].f030fcontact2Phone;
                    this.vendorDataHeader['f030fidBank'] = data['result'][0].f030fidBank;
                    this.vendorDataHeader['f030faccountNumber'] = data['result'][0].f030faccountNumber;
                    this.vendorDataHeader['f030ffax'] = data['result'][0].f030ffax;


                    // if(data['result'][0]['f071fstatus']==1) {
                    //     this.saveBtnDisable = true;
        
                    //     $("#target input,select").prop("disabled", true);
                    //     $("#target1 input,select").prop("disabled", true);
                    // }


                    
                }, error => {
                    console.log(error);
                });
        }
    }

    ngOnInit() {

        this.getVendorCode();

        
        this.vendorLicenseData['f031fstatus'] = 1;
        this.vendorDataHeader['f030fstatus'] = 1;

        this.ajaxcount = 0;
        //country dropdown
        this.ajaxcount ++;
        this.CountryService.getActiveCountryList().subscribe(
            data=>{
                this.ajaxcount --;
                    this.countryList = data['result']['data'];
                    let other = {
                        'f013fid' : "0",
                        "f013fcountryName":"Others"

                    };
                    this.countryList.push(other);

            }
        );

        //state dropdown
        

      
        //License dropdown
        this.ajaxcount ++;
        this.LicenseService.getItems().subscribe(
            data=>{
                this.ajaxcount --;
                    this.licenseList = data['result']['data'];
            }
        );

        //Bank dropdown
        this.ajaxcount ++;
        this.BankService.getItems().subscribe(
            data=>{
                this.ajaxcount --;
                    this.bankList = data['result']['data'];
            }
        );

       



    }
    onCountryChange(){
        this.ajaxcount ++;
        if(this.vendorDataHeader['f030fcountry'] == undefined){
            this.vendorDataHeader['f030fcountry'] = 0;
        }
        this.StateService.getStates(this.vendorDataHeader['f030fcountry']).subscribe(
            data=>{
                this.ajaxcount --;
                    this.stateList = data['result'];
                    let other = {
                        'f012fid' : "-1",
                        "f012fshortName":"SH",
                        "f012fstateName":"Others"

                    };
                    this.stateList.push(other);

            }
        );
    }
    checkEmail(){
        this.ajaxcount ++;
        let email = this.vendorDataHeader['f030femail'];
        console.log(email);
        let obj={
            'email' : email
        };
        this.SupplierregistrationService.checkemail(obj).subscribe(
            data=>{
                if(data['status'] == 201){
                    this.emailError = 'Email Already Exists';
                }
                else{
                    this.emailError = '';
                }

            }
        );
    }
    deletelicenseDataList(object){
        var cnf = confirm("Do you really want to delete");
        if (cnf == true) {
            console.log(this.licenseDataList);
            var index = this.licenseDataList.indexOf(object);;
            if (index > -1) {
                this.licenseDataList.splice(index, 1);
            }
          
        }
    }

    addlicenseDataList(){
        console.log(this.vendorLicenseData);
        if(this.vendorLicenseData['f031fidLicense']==undefined){
            alert('Select Grade');
            return false;
        }
        if(this.vendorLicenseData['f031fexpireDate']==undefined){
            alert('Enter expiry date');
            return false;
        }
        for(let o of this.licenseDataList){
            if(o['f031fidLicense'] == this.vendorLicenseData['f031fidLicense']) {
                alert('License already exist');
                return false;
              }
         }
        
        var dataofCurrentRow = this.vendorLicenseData;
        console.log("asdf");
        this.licenseDataList.push(dataofCurrentRow);
        this.vendorLicenseData = {};
        // this.vendorLicenseData['f031fidLicense'] = '';
        // this.vendorLicenseData['f031fexpireDate'] = '';
    }

    getVendorCode(){

        let typeObject = {};
        typeObject['type']  = "Vendor"
        this.SupplierregistrationService.getCode(typeObject).subscribe(
            data=>{
                this.vendorDataHeader['f030fvendorCode'] = data['number'];
            }
        );
    }
    fileSelected(event){

        this.file = event.target.files[0];
        console.log(this.file);
        // let fileReader = new FileReader();
        // fileReader.onload = (e) => {
        // this.vendorDataHeader['fileData'] = fileReader.result;
        // this.vendorDataHeader['fileName'] = this.file.name;
        // console.log(this.vendorData);
        // }
        // fileReader.readAsText(this.file);

    }
    saveVendorData() {

// this.VendorObject = {};
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        console.log(this.id);
        if (this.id > 0) {
            this.VendorObject['f031fid'] = this.id;
        }
        if(this.licenseDataList.length>0) {

        }  else {
            alert("Please add one License details");
            return false;
        }

        this.VendorObject['file'] = this.file;
        this.VendorObject['fileData'] = this.vendorDataHeader['fileData'];
        this.VendorObject['fileName'] = this.vendorDataHeader['fileName'];

        this.VendorObject['f030fcompanyName'] = this.vendorDataHeader['f030fcompanyName'];
        this.VendorObject['f030femail'] = this.vendorDataHeader['f030femail'];
        this.VendorObject['f030faddress1'] = this.vendorDataHeader['f030faddress1'];
        this.VendorObject['f030faddress2'] = this.vendorDataHeader['f030faddress2'];
        this.VendorObject['f030faddress3'] = this.vendorDataHeader['f030faddress3'];
        this.VendorObject['f030faddress4'] = this.vendorDataHeader['f030faddress4'];
        this.VendorObject['f030fphone'] = this.vendorDataHeader['f030fphone'];
        this.VendorObject['f030fstartDate'] = this.vendorDataHeader['f030fstartDate'];
        this.VendorObject['f030fendDate'] = this.vendorDataHeader['f030fendDate'];
        this.VendorObject['f030fcity'] = this.vendorDataHeader['f030fcity'];
        this.VendorObject['f030fpostCode'] = this.vendorDataHeader['f030fpostCode'];
        this.VendorObject['f030fstate'] = this.vendorDataHeader['f030fstate'];
        this.VendorObject['f030fcountry'] = this.vendorDataHeader['f030fcountry'];
        this.VendorObject['f030fwebsite'] = this.vendorDataHeader['f030fwebsite'];
        this.VendorObject['f030fcontact1Name'] = this.vendorDataHeader['f030fcontact1Name'];
        this.VendorObject['f030fcontact1Email'] = this.vendorDataHeader['f030fcontact1Email'];
        this.VendorObject['f030fcontact1Phone'] = this.vendorDataHeader['f030fcontact1Phone'];
        this.VendorObject['f030fcontact2Name'] = this.vendorDataHeader['f030fcontact2Name'];
        this.VendorObject['f030fcontact2Email'] = this.vendorDataHeader['f030fcontact2Email'];
        this.VendorObject['f030fcontact2Phone'] = this.vendorDataHeader['f030fcontact2Phone'];
        this.VendorObject['f030fidBank'] = this.vendorDataHeader['f030fidBank'];
        this.VendorObject['f030faccountNumber'] = this.vendorDataHeader['f030faccountNumber'];
        this.VendorObject['f030ffax'] = this.vendorDataHeader['f030ffax'];
        this.VendorObject['f030fvendorCode'] = this.vendorDataHeader['f030fvendorCode'];

        //file upload
        this.VendorObject['f030fcompanyRegistration'] = "file";

      //license Details
      this.VendorObject['license-details'] = this.licenseDataList;

        console.log(this.VendorObject);
        let fd = new FormData();
        fd.append('file',this.file);
        if (this.id > 0) {
            this.SupplierregistrationService.updateSupplierregistrationItems(this.VendorObject, this.id).subscribe(
                data => {
                    alert('Thanks for registering with us, you will receive an email from us soon. ')
                    // this.router.navigate(['procurement/supplierregistration']);
                    // this.AlertService.success("Updated Sucessfully !!")

                }, error => {
                    console.log(error);
                });


        } else {
            this.httpClient.post(this.downloadUrl, fd)
                .subscribe(
                  (res:Response) => {
                    console.log(res.json());
                  },
                  err => {
                    console.log("Error occured"); 
                  }
                );
            this.SupplierregistrationService.insertSupplierregistrationItems(this.VendorObject).subscribe(
                data => {
                    console.log(data);
                    alert('Thanks for registering with us, you will receive an email from us soon. ');
                    
                    this.router.navigate(['/vendorlogin']);
                    // this.AlertService.success("Added Sucessfully !!")

                }, error => {
                    alert('Email already exist');
                    return false;
                    // console.log(error);
                });

               

        }
    }

    resolved(captchaResponse: string) {
        console.log(`Resolved captcha with response ${captchaResponse}:`);
    }

}