import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from "../auth.service";
import { ActivatedRoute, Router } from '@angular/router';

@Component({
    selector: 'vendorLogin',
    templateUrl: 'supplierlogin.component.html'
})

export class SupplierLoginComponent implements OnInit {
    loginForm: FormGroup;
    userData = {};
    id:number;
    error:string;
    token : any;
    link : any;
    diff;
    moduleLinks = new Array();
    module : any;

    constructor(private fb: FormBuilder,private authService:AuthService, private route: ActivatedRoute,
        private router: Router) {
            
        this.loginForm = fb.group({
            email: ['', Validators.required],
            
            password: ['', [Validators.required, Validators.minLength(8)]]
        });
    }

    ngOnInit() {
        
     }

    login(){
        //console.log(this.userData);
        
        this.authService.vendorlogin(this.userData).subscribe(
            data => {
                    //console.log(data);
                    if(data['status'] == 413){
                        this.error = "Please contact administrator for approval";
                    }
                    // else if(data['status'] == 414){
                    //     this.error = "Please renew your account";
                    // }
                    else if(data['status'] == 412){
                        this.error = "Password Mismatch";
                    }
                    else if(data['status'] == 410){
                        this.error = "Username Doesn't Exist";
                    }
                    else{
                        sessionStorage.setItem('f014ftoken',data['token']);
                        sessionStorage.setItem('f014fuserName',data['username']);
                        sessionStorage.setItem('f014fid',data['id']);
                        sessionStorage.setItem('endDate',data['endDate']['date']);
                        let expireDate = new Date(data['endDate']['date']);
                        let currentDate = new Date();
                        var one_day=1000*60*60*24;

                        // Convert both dates to milliseconds
                        var date1_ms = currentDate.getTime();
                        var date2_ms = expireDate.getTime();
                      
                        // Calculate the difference in milliseconds
                        var difference_ms = date2_ms - date1_ms;
                          
                        // Convert back to days and return
                        this.diff =  Math.round(difference_ms/one_day);
                        // console.log(this.diff);
                        sessionStorage.setItem('expireDays',this.diff);
                        this.router.navigate(['/vendor']);
                    }
                    
        }, error => {
            console.log(error);
        });
    }

}