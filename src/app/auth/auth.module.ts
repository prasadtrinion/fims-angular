import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// import { RecaptchaModule } from 'ng-recaptcha';

import { NguiAutoCompleteModule } from '@ngui/auto-complete';

import { MaterialModule } from '../material/material.module';
import { HttpClientModule, HttpClient,HTTP_INTERCEPTORS } from '@angular/common/http';

import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { AuthService } from './auth.service';

import { AuthRoutingModule } from './auth.router.module';
import { JwtInterceptor } from '../_helpers/jwt.interceptors';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        MaterialModule,
        AuthRoutingModule,
        NguiAutoCompleteModule
       // RecaptchaModule

    ],
    exports: [],
    declarations: [
        LoginComponent,
        RegisterComponent,
        ForgotPasswordComponent,
    ],
    providers: [
        AuthService,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: JwtInterceptor,
            multi: true
          }
    ],
})
export class AuthModule { }
