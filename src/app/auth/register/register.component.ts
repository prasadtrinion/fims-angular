import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../auth.service';
import { environment } from '../../../environments/environment';

@Component({
    selector: 'register',
    templateUrl: 'register.component.html'
})

export class RegisterComponent implements OnInit {
    registerForm: FormGroup;
    constructor(
        private fb: FormBuilder,
        private authService: AuthService
    ) {
        this.registerForm = fb.group({
            email: ['', Validators.required, Validators.email],
            password: ['', [Validators.required, Validators.minLength(8)]],
            confirmPassword: ['', [Validators.required, Validators.minLength(8)]]
        });
    }
    ngOnInit(){
    }
    submitForm() {
        //console.log(this.userData);
        if (!this.registerForm.invalid) {
            this.authService.register(this.registerForm.value)
                .subscribe(data => {
                        console.log(data);
                }, error => {
                    alert(error);
                });
        }
    }
}