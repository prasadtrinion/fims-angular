import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from "../auth.service";
import { ActivatedRoute, Router } from '@angular/router';

@Component({
    selector: 'studentlogin',
    templateUrl: 'studentlogin.component.html'
})

export class StudentloginComponent implements OnInit {
    loginForm: FormGroup;
    studentData = {};
    id:number;
    error:string;
    
    constructor(private fb: FormBuilder,private authService:AuthService, private route: ActivatedRoute,
        private router: Router) {
            
        this.loginForm = fb.group({
            email: ['', Validators.required],
            
            password: ['', [Validators.required, Validators.minLength(8)]]
        });
    }

    ngOnInit() {
        
     }

    login(){
        console.log(this.studentData);
        
        this.authService.studentlogin(this.studentData).subscribe(
            data => {
                    // console.log(data);
                    sessionStorage.setItem("StudentName",data['name']);
                    sessionStorage.setItem("StudentId",data['id']);
                    if(data['status'] == 412){
                        this.error = "Password Mismatch";
                    }
                    else if(data['status'] == 410){
                        this.error = "Student Doesn't Exist";
                    }
                    else{
                           this.router.navigate(['/student/returns']);
                    }
                    
        }, error => {
            console.log(error);
        });
    }

}