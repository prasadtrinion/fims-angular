import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { ProgrammeService } from '../../service/programme.service'
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { DefinationmsService } from '../../service/definationms.service';
import { ActivitycodeService } from '../../../generalsetup/service/activitycode.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'ProgrammeFormComponent',
    templateUrl: 'programmeform.component.html'
})

export class ProgrammeFormComponent implements OnInit {
    programmeform: FormGroup;
    programmeList = [];
    programmeData = {};
    AwardList = [];
    SchemeList = [];
    typeList = [];
    activitycodeList = [];
    id: number;
    ajaxCount: number;
    constructor(
        private ProgrammeService: ProgrammeService,
        private DefinationmsService: DefinationmsService,
        private ActivitycodeService: ActivitycodeService,
        private route: ActivatedRoute,
        private spinner: NgxSpinnerService,
        private router: Router
    ) { }
    ngDoCheck() {
        const change = this.ajaxCount;
        if (this.ajaxCount == 0) {
            this.spinner.hide();
        }
    }
    ngOnInit() {
        this.ajaxCount = 0
        this.spinner.show();
        let typeObj = {};
        typeObj['name'] = 'UG';
        this.typeList.push(typeObj);
        typeObj = {};
        typeObj['name'] = 'PG';
        this.typeList.push(typeObj);
        this.programmeData['f068fstatus'] = 1;

        this.programmeform = new FormGroup({
            programme: new FormControl('', Validators.required)
        });
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.ajaxCount++;
            this.ProgrammeService.getItemsDetail(this.id).subscribe(
                data => {
                    this.ajaxCount--;
                    this.programmeData = data['result'][0];

                    // converting from string to int for radio button default selection
                    this.programmeData['f068fstatus'] = parseInt(data['result'][0]['f068fstatus']);

                    console.log(this.programmeData);
                }, error => {
                    console.log(error);
                });
        }
        console.log(this.id);
        this.ajaxCount++;
        this.DefinationmsService.getScheme('ProgrammeScheme').subscribe(
            data => {
                this.ajaxCount--;
                this.SchemeList = data['result'];
            }, error => {
                console.log(error);
            });
        this.ajaxCount++;
        this.DefinationmsService.getAward('Award').subscribe(
            data => {
                this.ajaxCount--;
                this.AwardList = data['result'];
            }, error => {
                console.log(error);
            });
        //Activity dropdown
        this.ajaxCount++;
        this.ActivitycodeService.getItemsByLevel().subscribe(
            data => {
                this.ajaxCount--;
                this.activitycodeList = data['result']['data'];
            }
        );
    }



    addProgramme() {
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        console.log(this.programmeData);
        // this.feeitemData['f015fupdatedBy'] = 1;
        this.programmeData['f068fidScheme'] = 1;
        this.programmeData['f068faward'] = 1;

        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.ProgrammeService.updateprogrammeItems(this.programmeData, this.id).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert('Code Already Exist');
                    }
                    else {
                        this.router.navigate(['studentfinance/programme']);
                        alert("Programme has been Updated Sucessfully");
                    }
                }, error => {
                    console.log(error);
                    alert('Code Already Exist');
                });

        } else {
            this.ProgrammeService.insertprogrammeItems(this.programmeData).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert('Code Already Exist');
                    }
                    else {
                        this.router.navigate(['studentfinance/programme']);
                        alert("Programme has been Added Sucessfully");
                    }

                }, error => {
                    console.log(error);
                });

        }

    }
    onlyNumberKey(event) {
        if (event.charCode == 46) {
        } else {
            return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
        }
    }
}