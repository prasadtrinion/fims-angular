import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ProgrammeService } from '../service/programme.service'
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'Programme',
    templateUrl: 'programme.component.html'
})

export class ProgrammeComponent implements OnInit {
    programmeList =  [];
    programmeData = {};
    id: number;

    constructor(
        private spinner: NgxSpinnerService,                        
        private ProgrammeService: ProgrammeService

    ) { }

    ngOnInit() { 
        this.spinner.show();                                                
        this.ProgrammeService.getItems().subscribe(
            data => {
                this.spinner.hide();
                this.programmeList = data['result']['data'];
        }, error => {
            console.log(error);
        });
    }

    addProgramme(){
           console.log(this.programmeData);
        this.ProgrammeService.insertprogrammeItems(this.programmeData).subscribe(
            data => {
                this.programmeList = data['todos'];
            }, error => {
                console.log(error);
            });

    }
}