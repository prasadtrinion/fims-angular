import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { SemesterService } from '../../service/semester.service'
import { DefinationmsService } from '../../../studentfinance/service/definationms.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'SemesterFormComponent',
    templateUrl: 'semesterform.component.html'
})

export class SemesterFormComponent implements OnInit {
    semesterform: FormGroup;
    semesterList = [];
    semesterData = {};
    semsequenceList = [];
    graduationList = [];
    ajaxCount: number;
    id: number;

    constructor(
        private SemesterService: SemesterService,
        private DefinationmsService: DefinationmsService,
        private spinner: NgxSpinnerService,
        private route: ActivatedRoute,
        private router: Router

    ) { }
    ngDoCheck() {
        const change = this.ajaxCount;
        if (this.ajaxCount == 0) {
            this.spinner.hide();
        }
    }
    ngOnInit() {
        this.ajaxCount = 0
        this.spinner.show();
        this.semesterData['f053fstatus'] = 1;
        // this.semesterData['f066fidFeeCategory']='';


        this.semesterform = new FormGroup({
            semester: new FormControl('', Validators.required)
        });
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));


        //---------------DEFMS
        this.ajaxCount++;
        this.DefinationmsService.getSemesterType('SemesterType').subscribe(
            data => {
                this.ajaxCount--;
                this.semesterList = data['result'];
            }, error => {
                console.log(error);
            });
        this.ajaxCount++;
        this.DefinationmsService.getSemesterType('ProgrammeScheme').subscribe(
            data => {
                this.ajaxCount--;
                this.graduationList = data['result'];
            }, error => {
                console.log(error);
            });

        //---------------DEFMS
        this.ajaxCount++;
        this.DefinationmsService.getSemesterSequence('SemesterSequence').subscribe(
            data => {
                this.ajaxCount--;
                this.semsequenceList = data['result'];
            }, error => {
                console.log(error);
            });
        if (this.id > 0) {
            this.ajaxCount++;
            this.SemesterService.getItemsDetail(this.id).subscribe(
                data => {
                    this.ajaxCount--;
                    this.semesterData = data['result']['data'][0];

                    // converting from string to int for radio button default selection
                    this.semesterData['f053fstatus'] = parseInt(data['result']['data'][0]['f053fstatus']);

                    console.log(this.semesterData);
                }, error => {
                    console.log(error);
                });
        }
    }
    dateComparison() {
        let fromDate = Date.parse(this.semesterData['f053fstartDate']);
        let toDate = Date.parse(this.semesterData['f053fendDate']);
        if (fromDate > toDate) {
            alert("Start Date cannot be Less than End Date!");
            this.semesterData['f053fendDate'] = "";
        }
    }
    addSemester() {
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.SemesterService.updateSemesterItems(this.semesterData, this.id).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert("Code Already Exist");
                    }
                    else {
                        this.router.navigate(['studentfinance/semester']);
                        alert("Semester has been Updated Successfully");
                    }
                }, error => {
                    console.log(error);
                    alert("Code Already Exist");
                });

        } else {
            this.SemesterService.insertSemesterItems(this.semesterData).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert("Code Already Exist");
                    }
                    else {
                        this.router.navigate(['studentfinance/semester']);
                        alert("Semester has been Added Successfully");
                    }
                }, error => {
                    console.log(error);
                });

        }

    }
}