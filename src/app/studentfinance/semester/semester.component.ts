import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SemesterService } from '../service/semester.service'
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'Semester',
    templateUrl: 'semester.component.html'
})

export class SemesterComponent implements OnInit {
    semesterList =  [];
    semesterData = {};
    id: number;

    constructor(
        private spinner: NgxSpinnerService,                                
        private SemesterService: SemesterService

    ) { }

    ngOnInit() { 
        this.spinner.show();                                                        
        this.SemesterService.getItems().subscribe(
            data => {
                this.spinner.hide();
                this.semesterList = data['result']['data'];
        }, error => {
            console.log(error);
        });
    }

    addSemester(){
           console.log(this.semesterData);
        this.SemesterService.insertSemesterItems(this.semesterData).subscribe(
            data => {
                this.semesterList = data['todos'];
            }, error => {
                console.log(error);
            });

    }
}