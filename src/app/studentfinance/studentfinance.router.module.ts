import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CurrencysetupComponent } from './currencysetup/currencysetup.component';
import { CurrencysetupFormComponent } from './currencysetup/currencysetupform/currencysetupform.component';

import { CurrencyratesetupComponent } from './currencyratesetup/currencyratesetup.component';
import { CurrencyratesetupFormComponent } from './currencyratesetup/currencyratesetupform/currencyratesetupform.component';

import { InsurancesetupComponent } from './insurancesetup/insurancesetup.component';
import { InsurancesetupFormComponent } from './insurancesetup/insurancesetupform/insurancesetupform.component';

import { FeecategoryComponent } from './feecategory/feecategory.component';
import { FeecategoryFormComponent } from './feecategory/feecategoryform/feecategoryform.component';

import { FeeitemComponent } from './feeitem/feeitem.component';
import { FeeitemFormComponent } from './feeitem/feeitemform/feeitemform.component';
import { PtptnComponent } from './ptptn/ptptn.component'
import { IntakeComponent } from './intake/intake.component';
 import { IntakeFormComponent } from './intake/intakeform/intakeform.component';

import { SubjectComponent } from './subject/subject.component';
import { SubjectFormComponent} from './subject/subjectform/subjectform.component';

import { ProgrammeComponent } from './programme/programme.component';
import { ProgrammeFormComponent } from './programme/programmeform/programmeform.component';

import { SponsorComponent } from './sponsor/sponsor.component';
import { SponsorFormComponent } from './sponsor/sponsorform/sponsorform.component';

import { StudentComponent } from './student/student.component';
import { StudentFormComponent } from './student/studentform/studentform.component';

import { TaggingsponsorComponent } from './taggingsponsor/taggingsponsor.component';
import { TaggingsponsorFormComponent } from './taggingsponsor/taggingsponsorform/taggingsponsorform.component';

import { DebitinformationComponent } from './debitinformation/debitinformation.component';
import { DebitinformationFormComponent } from './debitinformation/debitinformationform/debitinformationform.component';

import { SemesterComponent } from './semester/semester.component';
import { SemesterFormComponent } from './semester/semesterform/semesterform.component';

import { GeneratebillComponent } from './generatebill/generatebill.component';


// import { CreditnoteComponent } from './../accountsrecivable/creditnote/creditnote.component';
// import { CreditnoteformComponent } from './../accountsrecivable/creditnote/creditnoteform/creditnoteform.component';

import { SponserbillComponent } from '../studentfinance/sponserbill/sponserbill.component';
import { SponserbillformComponent } from '../studentfinance/sponserbill/sponserbillform/sponserbillform.component';

import { DiscountComponent } from './discount/discount.component';
import { DiscountFormComponent } from './discount/discountform/discountform.component';

import { StudentinsuranceComponent } from './studentinsurance/studentinsurance.component';
import { StudentinsuranceFormComponent } from './studentinsurance/studentinsuranceform/studentinsuranceform.component';

import { FeestructureComponent } from './feestructure/feestructure.component';
import { FeestructureFormComponent } from './feestructure/feestructureform/feestructureform.component';

import { DebttaggingComponent } from './debttagging/debttagging.component';
import { DebttaggingFormComponent } from './debttagging/debttaggingform/debttaggingform.component';

import { CreditcardterminalComponent } from './creditcardterminal/creditcardterminal.component';
import { CreditcardterminalFormComponent } from './creditcardterminal/creditcardterminalform/creditcardterminalform.component';

import { GenerateinvoiceComponent } from './generateinvoice/generateinvoice.component';
import { Generateinvoice1Component } from './generateinvoice/generateinvoice1.component';

import { ReturnFormComponent } from './returns/returnform/returnform.component';
import { ReturnsComponent } from './returns/returns.component';
import { ReturnapprovalComponent } from './returnapproval/returnapproval.component';

import { BondinformationComponent } from './bondinformation/bondinformation.component';
import { BondinformationFormComponent } from './bondinformation/bondinformationform/bondinformationform.component';

import { LegalinformationComponent } from './legalinformation/legalinformation.component';
import { LegalinformationFormComponent } from './legalinformation/legalinformationform/legalinformationform.component';

import { TagstudentinsuranceComponent } from './tagstudentinsurance/tagstudentinsurance.component';
import { TagstudentinsuranceFormComponent } from './tagstudentinsurance/tagstudentinsuranceform/tagstudentinsuranceform.component';

import { BondapprovalComponent } from './bondapproval/bondapproval.component'

import {BulksponsortaggingFormComponent} from './bulksponsortagging/bulksponsortagging.component'

import { StudentledgerComponent } from './studentledger/studentledger.component';
import { StudentledgerFormComponent } from './studentledger/studentledgerform/studentledgerform.component';

import { FeetypeComponent } from "./feetype/feetype.component";
import { FeetypeFormComponent } from "./feetype/feetypeform/feetypeform.component";

const routes: Routes = [

  { path: 'studentledger', component: StudentledgerComponent },
  { path: 'addnewstudentledger', component: StudentledgerFormComponent },
  { path: 'editstudentledger/:id', component: StudentledgerFormComponent },

  { path: 'currencysetup', component: CurrencysetupComponent },
  { path: 'addnewcurrencysetup', component: CurrencysetupFormComponent },
  { path: 'editcurrencysetup/:id', component: CurrencysetupFormComponent },

  { path: 'semester', component: SemesterComponent },
  { path: 'addnewsemester', component: SemesterFormComponent },
  { path: 'editsemester/:id', component: SemesterFormComponent },

  { path: 'subject', component: SubjectComponent },
  { path: 'addnewsubject', component: SubjectFormComponent },
  { path: 'editsubject/:id', component: SubjectFormComponent },


  { path: 'currencyratesetup', component: CurrencyratesetupComponent },
  { path: 'addnewcurrencyratesetup', component: CurrencyratesetupFormComponent },
  { path: 'editcurrencyratesetup/:id', component: CurrencyratesetupFormComponent },

  { path: 'insurancesetup', component: InsurancesetupComponent },
  { path: 'addnewinsurancesetup', component: InsurancesetupFormComponent },
  { path: 'editinsurancesetup/:id', component: InsurancesetupFormComponent },

  { path: 'feecategory', component: FeecategoryComponent },
  { path: 'addnewfeecategory', component: FeecategoryFormComponent },
  { path: 'editfeecategory/:id', component: FeecategoryFormComponent },

  { path: 'feeitem', component: FeeitemComponent },
  { path: 'addnewfeeitem', component: FeeitemFormComponent },
  { path: 'editfeeitem/:id', component: FeeitemFormComponent },

  { path: 'intake', component: IntakeComponent },
  { path: 'addnewintake', component: IntakeFormComponent },
  { path: 'editintake/:id', component: IntakeFormComponent },

  { path: 'programme', component: ProgrammeComponent },
  { path: 'addnewprogramme', component: ProgrammeFormComponent },
  { path: 'editprogramme/:id', component: ProgrammeFormComponent },

  { path: 'sponsor', component: SponsorComponent },
  { path: 'addnewsponsor', component: SponsorFormComponent },
  { path: 'editsponsor/:id', component: SponsorFormComponent },

  { path: 'student', component: StudentComponent },
  { path: 'addnewstudent', component: StudentFormComponent },
  { path: 'editstudent/:id', component: StudentFormComponent },

  { path: 'taggingsponsor', component: TaggingsponsorComponent },
  { path: 'addnewtaggingsponsor', component: TaggingsponsorFormComponent },
  { path: 'edittaggingsponsor/:id', component: TaggingsponsorFormComponent },

  { path: 'debitinformation', component: DebitinformationComponent },
  { path: 'addnewdebitinformation', component: DebitinformationFormComponent },
  { path: 'editdebitinformation/:id', component: DebitinformationFormComponent },

  { path: 'discount', component: DiscountComponent },
  { path: 'addnewdiscount', component: DiscountFormComponent },
  { path: 'editdiscount/:id', component: DiscountFormComponent },

  { path: 'generatebill', component: GeneratebillComponent },

  { path: 'studentinsurance', component: StudentinsuranceComponent },
  { path: 'addnewstudentinsurance', component: StudentinsuranceFormComponent },
  { path: 'editstudentinsurance/:id', component: StudentinsuranceFormComponent },

  { path: 'feestructure', component: FeestructureComponent },
  { path: 'addnewfeestructure', component: FeestructureFormComponent },
  { path: 'editfeestructure/:id', component: FeestructureFormComponent },
  

  { path: 'debttagging', component: DebttaggingComponent },
  { path: 'addnewdebttagging', component: DebttaggingFormComponent },
  { path: 'editdebttagging/:id', component: DebttaggingFormComponent },


  { path: 'creditcardterminal', component: CreditcardterminalComponent },
  { path: 'addnewcreditcardterminal', component: CreditcardterminalFormComponent },
  { path: 'editcreditcardterminal/:id', component: CreditcardterminalFormComponent },

  { path: 'studentinvoices', component: GenerateinvoiceComponent },
  { path: 'generateinvoice', component: Generateinvoice1Component },

  { path: 'returns', component: ReturnsComponent },
  { path: 'addnewreturn', component: ReturnFormComponent },
  { path: 'editreturn/:id', component: ReturnFormComponent },
  { path: 'viewreturn/:id/:idview', component: ReturnFormComponent },

  { path: 'returnapproval', component: ReturnapprovalComponent },
  { path: 'bondapproval', component: BondapprovalComponent },

  { path: 'sponserbill', component: SponserbillComponent },
  { path: 'addnewsponserbill', component: SponserbillformComponent },
  { path: 'editsponserbill/:id', component: SponserbillformComponent },

  { path: 'bondinformation', component: BondinformationComponent },
  { path: 'addnewbondinformation', component: BondinformationFormComponent },
  { path: 'editbondinformation/:id', component: BondinformationFormComponent },


  { path: 'legalinformation', component: LegalinformationComponent },
  { path: 'addnewlegalinformation', component: LegalinformationFormComponent },
  { path: 'editlegalinformation/:id', component: LegalinformationFormComponent },
  
  { path: 'tagstudentinsurance', component: TagstudentinsuranceComponent },
  { path: 'addnewtagstudentinsurance', component:  TagstudentinsuranceFormComponent },
  { path: 'edittagstudentinsurance/:id', component:  TagstudentinsuranceFormComponent },
  { path: 'ptptn', component: PtptnComponent },

  { path: 'bulksponsortagging', component: BulksponsortaggingFormComponent },

  { path: 'feetype', component: FeetypeComponent },
  { path: 'addfeetype', component: FeetypeFormComponent },
  { path: 'editfeetype/:id', component: FeetypeFormComponent },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],

})
export class StudentfinanceRoutingModule {

}