import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { TaggingsponsorService } from '../../service/taggingsponsor.service'
import { StudentService } from '../../service/student.service'
import { SemesterService } from '../../service/semester.service'
import { SponsorService } from '../../service/sponsor.service'
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { HttpClientModule, HttpHeaders, HttpClient } from '@angular/common/http';
import { NgxSpinnerService } from 'ngx-spinner';
import { environment } from '../../../../environments/environment';

@Component({
    selector: 'TaggingsponsorFormComponent',
    templateUrl: 'taggingsponsorform.component.html'
})

export class TaggingsponsorFormComponent implements OnInit {
    taggingform: FormGroup;
    taggingList = [];
    studentList = [];
    taggingData = {};
    sponsorList = [];
    semesterList = [];
    edit: boolean = false;
    id: number;
    ajaxCount: number;
    downloadUrl: string = environment.api.downloadbase;
    file;
    constructor(
        private TaggingsponsorService: TaggingsponsorService,
        private StudentService: StudentService,
        private SponsorService: SponsorService,
        private SemesterService: SemesterService,
        private spinner: NgxSpinnerService,
        private route: ActivatedRoute,
        private router: Router,
        private httpClient: HttpClient,

    ) { }


    ngDoCheck() {
        const change = this.ajaxCount;
        console.log(this.ajaxCount);
        if (this.ajaxCount == 0) {
            this.editFunction();
            this.spinner.hide();
            this.ajaxCount = 10;
        }
    }

    editFunction() {
        this.ajaxCount++;
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.edit = true;
            this.TaggingsponsorService.getItemsDetail(this.id).subscribe(
                data => {
                    this.ajaxCount--;
                    this.taggingData = data['result'][0];
                    console.log(this.taggingData);
                    // converting from string to int for radio button default selection
                    this.taggingData['f064fstatus'] = parseInt(data['result'][0]['f064fstatus']);
                    console.log(this.taggingData);
                }, error => {
                    console.log(error);
                });
        }
    }

    ngOnInit() {
        this.ajaxCount = 0;
        this.spinner.show();
        this.taggingData['f064fstatus'] = 1;
        // drop down student
        this.ajaxCount++;
        this.StudentService.getActiveItems().subscribe(
            data => {
                this.ajaxCount--;
                this.studentList = data['result']['data'];
            }, error => {
                console.log(error);
            });
        // drop down sponsor
        this.ajaxCount++;
        this.SponsorService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.sponsorList = data['result']['data'];
            }, error => {
                console.log(error);
            });
        // drop down sem
        this.ajaxCount++;
        this.SemesterService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.semesterList = data['result']['data'];
            }, error => {
                console.log(error);
            });


        this.taggingform = new FormGroup({
            tagging: new FormControl('', Validators.required)
        });

    }
    dateComparison() {

        let startDate = Date.parse(this.taggingData['f064fstartDate']);
        let endDate = Date.parse(this.taggingData['f064fendDate']);
        if (startDate > endDate) {
            alert("Start Date cannot be greater than End Date !");
            this.taggingData['f064fendDate'] = "";
        }

    }
    extensiondateComparison() {

        let startDate = Date.parse(this.taggingData['f064fextensionDate']);
        let endDate = Date.parse(this.taggingData['f064fendDate']);
        if (startDate < endDate) {
            alert("Extension Date should be greater than End Date !");
            this.taggingData['f064fextensionDate'] = "";
        }

    }
    fileSelected(event) {

        this.file = event.target.files[0];
        console.log(this.file);

    }
    addTaggingsponsor() {
        if (this.edit == false) {
            if (this.file == undefined) {
                alert('Select a file to upload');
                return false;
            }
        }
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        console.log(this.taggingData);
        this.taggingData['f065fupdatedBy'] = 1;

        let fd = new FormData();
        fd.append('file', this.file);

        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.TaggingsponsorService.updatetaggingsponsorItems(this.taggingData, this.id).subscribe(
                data => {
                    this.router.navigate(['studentfinance/taggingsponsor']);
                    alert("Tagging Sponsor has been Updated Successfully");
                }, error => {
                    console.log(error);
                });

        } else {
            this.httpClient.post(this.downloadUrl, fd)
                .subscribe(
                    data => {
                        this.taggingData['f064fpath'] = data['name'];
                        this.TaggingsponsorService.inserttaggingsponsorItems(this.taggingData).subscribe(
                            data => {
                                this.router.navigate(['studentfinance/taggingsponsor']);
                                alert("Tagging Sponsor has been Added Successfully");

                            }, error => {
                                console.log(error);
                            });
                    },
                    err => {
                        console.log("Error occured");
                    }
                );
        }
    }
    onlyNumberKey(event) {
        if (event.charCode == 46) {

        } else {
            return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
        }
    }
}