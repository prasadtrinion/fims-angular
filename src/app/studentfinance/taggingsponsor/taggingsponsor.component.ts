import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { TaggingsponsorService } from '../service/taggingsponsor.service'
import { NgxSpinnerService } from 'ngx-spinner';
@Component({
    selector: 'Taggingsponsor',
    templateUrl: 'taggingsponsor.component.html'
})
export class TaggingsponsorComponent implements OnInit {
    taggingList = [];
    taggingData = {};
    id: number;
    constructor(
        private TaggingsponsorService: TaggingsponsorService,
        private spinner: NgxSpinnerService,                
    ) { }
    ngOnInit() {
        this.spinner.show();                                                
        this.TaggingsponsorService.getItems().subscribe(
            data => {
                this.spinner.hide();
                this.taggingList = data['result'];
            }, error => {
                console.log(error);
            });
    }
}