import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DiscountService } from '../service/discount.service'


@Component({
    selector: 'Discount',
    templateUrl: 'discount.component.html'
})

export class DiscountComponent implements OnInit {
    discountList =  [];
    discountData = {};
    id: number;

    constructor(
        
        private DiscountService: DiscountService

    ) { }

    ngOnInit() { 
        
        this.DiscountService.getItems().subscribe(
            data => {
                this.discountList = data['result']['data'];
        }, error => {
            console.log(error);
        });
    }

    addDiscount(){
           console.log(this.discountData);
        this.DiscountService.insertdiscountItems(this.discountData).subscribe(
            data => {
                this.discountList = data['todos'];
            }, error => {
                console.log(error);
            });

    }
}