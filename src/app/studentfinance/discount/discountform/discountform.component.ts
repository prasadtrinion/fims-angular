import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { DiscountService } from '../../service/discount.service'
import { FeeitemService } from '../../service/feeitem.service'
// import { TaxsetupcodeService } from '../../../generalledger/service/taxsetupcode.service'
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
    selector: 'DiscountFormComponent',
    templateUrl: 'discountform.component.html'
})

export class DiscountFormComponent implements OnInit {
    discountform: FormGroup;
    discountList = [];
    discountData = {};
    feeitemList = [];

    id: number;

    constructor(
        private DiscountService: DiscountService,
        private FeeitemService:FeeitemService,
        private route: ActivatedRoute,
        private router: Router

    ) { }

    ngOnInit() {
        this.discountData['f046fstatus'] = 1;

        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        //fee dropdown
        this.FeeitemService.getItems().subscribe(
            data => {
                this.feeitemList = data['result']['data'];
            }, error => {
                console.log(error);
            });

        this.discountform = new FormGroup({
            discount: new FormControl('', Validators.required)
        });
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
    

        console.log(this.id);

        if (this.id > 0) {
            this.DiscountService.getItemsDetail(this.id).subscribe(
                data => {
                    this.discountData = data['result'][0];

                    // converting from string to int for radio button default selection
                    this.discountData['f046fstatus'] = parseInt(data['result'][0]['f046fstatus']);
                    
                    console.log(this.discountData);
                }, error => {
                    console.log(error);
                });
        }


        console.log(this.id);
    }

    addDiscount() {
        console.log(this.discountData);

        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.DiscountService.updatediscountItems(this.discountData, this.id).subscribe(
                data => {
                    this.router.navigate(['studentfinance/discount']);
                }, error => {
                    console.log(error);
                });

        } else {
            this.DiscountService.insertdiscountItems(this.discountData).subscribe(
                data => {
                    this.router.navigate(['studentfinance/discount']);

                }, error => {
                    console.log(error);
                });

        }

    }
}