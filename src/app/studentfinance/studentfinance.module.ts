import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { HttpClientModule, HttpClient,HTTP_INTERCEPTORS } from '@angular/common/http';
import { QueryBuilderModule } from "angular2-query-builder";
import { NguiAutoCompleteModule } from '@ngui/auto-complete';

import { MaterialModule } from '../material/material.module';
import { DataTableModule } from 'angular-6-datatable';

import { CurrencysetupComponent } from './currencysetup/currencysetup.component';
import { CurrencysetupFormComponent } from './currencysetup/currencysetupform/currencysetupform.component';
import { CurrencysetupService } from './service/currencysetup.service'

import { CurrencyratesetupComponent } from './currencyratesetup/currencyratesetup.component';
import { CurrencyratesetupFormComponent } from './currencyratesetup/currencyratesetupform/currencyratesetupform.component';
import { CurrencyratesetupService } from './service/currencyratesetup.service'

import { GeneratebillComponent } from './generatebill/generatebill.component';

import { InsurancesetupComponent } from './insurancesetup/insurancesetup.component';
import { InsurancesetupFormComponent } from './insurancesetup/insurancesetupform/insurancesetupform.component';
import { InsurancesetupService } from './service/insurancesetup.service'

import { StudentledgerComponent } from './studentledger/studentledger.component';
import { StudentledgerFormComponent } from './studentledger/studentledgerform/studentledgerform.component';
import { StudentledgerService } from './service/studentledger.service'

import { FeecategoryComponent } from './feecategory/feecategory.component';
import { FeecategoryFormComponent } from './feecategory/feecategoryform/feecategoryform.component';
import { FeecategoryService } from './service/feecategory.service'

import { FeeitemComponent } from './feeitem/feeitem.component';
import { FeeitemFormComponent } from './feeitem/feeitemform/feeitemform.component';
import { FeeitemService } from './service/feeitem.service'

import { IntakeComponent } from './intake/intake.component';
import { IntakeFormComponent } from './intake/intakeform/intakeform.component';
import { IntakeService } from './service/intake.service'

import { ProgrammeComponent } from './programme/programme.component';
import { ProgrammeFormComponent } from './programme/programmeform/programmeform.component';
import { ProgrammeService } from './service/programme.service';

import { SponsorComponent } from './sponsor/sponsor.component';
import { SponsorFormComponent } from './sponsor/sponsorform/sponsorform.component';
import { SponsorService } from './service/sponsor.service';

import { StudentComponent } from './student/student.component';
import { StudentFormComponent } from './student/studentform/studentform.component';
import { StudentService } from './service/student.service';

import { SubjectComponent } from './subject/subject.component';
import { SubjectFormComponent} from './subject/subjectform/subjectform.component';

import { TaggingsponsorComponent } from './taggingsponsor/taggingsponsor.component';
import { TaggingsponsorFormComponent } from './taggingsponsor/taggingsponsorform/taggingsponsorform.component';
import { TaggingsponsorService } from './service/taggingsponsor.service';

import { CreditnoteComponent } from './../accountsrecivable/creditnote/creditnote.component';
import { CreditnoteformComponent } from './../accountsrecivable/creditnote/creditnoteform/creditnoteform.component';

import { CreditnoteService } from './../accountsrecivable/service/creditnote.service';
import { ItemService } from './../accountsrecivable/service/item.service';
import { MaininvoiceService } from './../accountsrecivable/service/maininvoice.service';
import { CustomerService } from './../accountsrecivable/service/customer.service';

import { DefinationmsService } from './service/definationms.service';

import { DebitinformationComponent } from './debitinformation/debitinformation.component';
import { DebitinformationFormComponent } from './debitinformation/debitinformationform/debitinformationform.component';
import { DebitinformationService } from './service/debitinformation.service';

import { SemesterComponent } from './semester/semester.component';
import { SemesterFormComponent } from './semester/semesterform/semesterform.component';
import { SemesterService } from './service/semester.service';

import { SponserbillComponent } from '../studentfinance/sponserbill/sponserbill.component';
import { SponserbillformComponent } from '../studentfinance/sponserbill/sponserbillform/sponserbillform.component';

import { SponserbillService } from '../studentfinance/service/sponserbill.service';

import { DiscountComponent } from './discount/discount.component';
import { DiscountFormComponent } from './discount/discountform/discountform.component';
import { DiscountService } from './service/discount.service';

import { FeestructureComponent } from './feestructure/feestructure.component';
import { FeestructureFormComponent } from './feestructure/feestructureform/feestructureform.component';

import { StudentinsuranceComponent } from './studentinsurance/studentinsurance.component';
import { StudentinsuranceFormComponent } from './studentinsurance/studentinsuranceform/studentinsuranceform.component';
import { StudentinsuranceService } from './service/studentinsurance.service'

import { DebttaggingComponent } from './debttagging/debttagging.component';
import { DebttaggingFormComponent } from './debttagging/debttaggingform/debttaggingform.component';
import { DebttaggingService } from './service/debttagging.service';

import { FeestructureService } from './service/feestructure.service';

import { CreditcardterminalComponent } from './creditcardterminal/creditcardterminal.component';
import { CreditcardterminalFormComponent } from './creditcardterminal/creditcardterminalform/creditcardterminalform.component';
import { CreditcardterminalService } from './service/creditcardterminal.service'

import { GenerateinvoiceComponent } from './generateinvoice/generateinvoice.component';
import { Generateinvoice1Component } from './generateinvoice/generateinvoice1.component';
import { GenerateinvoiceService } from './service/generateinvoice.service'

import { ReturnsComponent } from './returns/returns.component';
import { ReturnFormComponent } from './returns/returnform/returnform.component';
import { ReturnService } from './service/return.service'

import { ReturnapprovalComponent } from './returnapproval/returnapproval.component';
import { ReturnapprovalService } from './service/returnapproval.service'

import { BondapprovalComponent } from './bondapproval/bondapproval.component';
import { BondapprovalService } from './service/bondapproval.service'

import { BondinformationComponent } from './bondinformation/bondinformation.component';
import { BondinformationFormComponent } from './bondinformation/bondinformationform/bondinformationform.component';
import { BondinformationService } from './service/bondinformation.service'

import { LegalinformationComponent } from './legalinformation/legalinformation.component';
import { LegalinformationFormComponent } from './legalinformation/legalinformationform/legalinformationform.component';
import { LegalinformationService } from './service/legalinformation.service'

import { TagstudentinsuranceComponent } from './tagstudentinsurance/tagstudentinsurance.component';
import { TagstudentinsuranceFormComponent } from './tagstudentinsurance/tagstudentinsuranceform/tagstudentinsuranceform.component';
import { TagstudentinsuranceService } from './service/tagstudentinsurance.service';

import { PtptnComponent } from './ptptn/ptptn.component'

import { GeneratebillService } from './service/generatebill.service';

import { FeetypeService } from "./service/feetype.service";
import { FeetypeComponent } from "./feetype/feetype.component";
import { FeetypeFormComponent } from "./feetype/feetypeform/feetypeform.component";

import { PtptnService } from './service/ptptn.service';

import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { StudentfinanceRoutingModule } from './studentfinance.router.module';
import { JwtInterceptor } from '../_helpers/jwt.interceptors';
import { from } from 'rxjs';
import { SubjectService } from './service/subject.service';

import {BulksponsortaggingFormComponent} from './bulksponsortagging/bulksponsortagging.component'
import { BulkSponsorTaggingService } from "./service/bulksponsortagging.service";
import { NgSelectModule } from '@ng-select/ng-select';
import { SharedModule } from "../shared/shared.module";
@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        TabsModule,
        NgSelectModule,
        ReactiveFormsModule,
        MaterialModule,
        StudentfinanceRoutingModule,
        Ng2SearchPipeModule,
        HttpClientModule,
        QueryBuilderModule,
        DataTableModule,
        NguiAutoCompleteModule,
        SharedModule

    ],
    exports: [],
    declarations: [
        StudentledgerComponent,
        StudentledgerFormComponent,
        CurrencysetupComponent,
        // ArraySortPipe,
        CurrencysetupFormComponent,
        CurrencyratesetupComponent,
        CurrencyratesetupFormComponent,
        InsurancesetupComponent,
        InsurancesetupFormComponent,
        FeecategoryComponent,
        FeecategoryFormComponent,
        FeeitemComponent,
        FeeitemFormComponent,
        IntakeComponent,
        IntakeFormComponent,
        ProgrammeComponent,
        ProgrammeFormComponent,
        SponsorComponent,
        SponsorFormComponent,
        StudentComponent,
        StudentFormComponent,
        TaggingsponsorComponent,
        TaggingsponsorFormComponent,
        DebitinformationComponent,
        DebitinformationFormComponent,
        SemesterComponent,
        SemesterFormComponent,
        DiscountComponent,
        DiscountFormComponent,
        StudentinsuranceComponent,
        StudentinsuranceFormComponent,
        FeestructureFormComponent,
        FeestructureComponent,
        DebttaggingComponent,
        DebttaggingFormComponent,
        CreditcardterminalComponent,
        CreditcardterminalFormComponent,
        GenerateinvoiceComponent,
        Generateinvoice1Component,
        ReturnsComponent,
        ReturnFormComponent,
        ReturnapprovalComponent,
        BondinformationComponent,
        BondinformationFormComponent,
        LegalinformationComponent,
        LegalinformationFormComponent,
        TagstudentinsuranceComponent,
        TagstudentinsuranceFormComponent,
        BondapprovalComponent,
        SponserbillformComponent,
        SponserbillComponent,
        GeneratebillComponent,
        SubjectComponent,
        SubjectFormComponent,
        PtptnComponent,
        BulksponsortaggingFormComponent,
        FeetypeFormComponent,
        FeetypeComponent

    ],
    providers: [
        StudentledgerService,
        CurrencysetupService,
        CurrencyratesetupService,
        InsurancesetupService,
        FeecategoryService,
        FeeitemService,
        IntakeService,
        ProgrammeService,
        DefinationmsService,
        SponsorService,
        StudentService,
        CustomerService,
        TaggingsponsorService,
        CreditnoteService,
        ItemService,
        MaininvoiceService,
        DebitinformationService,
        SemesterService,
        DiscountService,
        StudentinsuranceService,
        DebttaggingService,
        CreditcardterminalService,
        FeestructureService,
        GenerateinvoiceService,
        ReturnService,
        ReturnapprovalService,
        BondinformationService,
        LegalinformationService,
        TagstudentinsuranceService,
        SubjectService,
        BondapprovalService,
        SponserbillService,
        GeneratebillService,
        BulkSponsorTaggingService,
        FeetypeService,
        PtptnService,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: JwtInterceptor,
            multi: true
          }
    ],
})
export class StudentfinanceModule { }
