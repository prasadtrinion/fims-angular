import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LegalinformationService } from '../service/legalinformation.service'
import { NgxSpinnerService } from 'ngx-spinner';
@Component({
    selector: 'Legalinformation',
    templateUrl: 'legalinformation.component.html'
})

export class LegalinformationComponent implements OnInit {
    legalinformationList =  [];
    legalinformationData = {};
    id: number;

    constructor(
        private LegalinformationService: LegalinformationService,
        private spinner: NgxSpinnerService,
    ) { }

    ngOnInit() { 
        this.spinner.show();                                                                        
        this.LegalinformationService.getItems().subscribe(
            data => {
                this.spinner.hide();
                this.legalinformationList = data['result']['data'];
        }, error => {
            console.log(error);
        });
    }

    addLegalinformation(){
           console.log(this.legalinformationData);
        this.LegalinformationService.insertLegalinformationItems(this.legalinformationData).subscribe(
            data => {
                this.legalinformationList = data['todos'];
            }, error => {
                console.log(error);
            });

    }
}