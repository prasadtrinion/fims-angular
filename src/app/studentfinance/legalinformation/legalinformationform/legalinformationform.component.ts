import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { LegalinformationService } from '../../service/legalinformation.service'
import { AlertService } from '../../../_services/alert.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators, PatternValidator } from '@angular/forms';
import { StudentService } from '../../service/student.service'
import { NgxSpinnerService } from 'ngx-spinner';
import { valid } from 'semver';
import { INVALID } from '@angular/forms/src/model';

@Component({
    selector: 'LegalinformationFormComponent',
    templateUrl: 'legalinformationform.component.html'
})

export class LegalinformationFormComponent implements OnInit {
    legalinformationform: FormGroup;
    legalinformationList = [];
    studentList = [];
    legalinformationData = {};
    legalinfoDataheader = {};
    deleteList = [];
    id: number;
    showLastRow: boolean;
    showLastRowMinus: boolean;
    showLastRowPlus: boolean;
    listshowLastRowMinus: boolean;
    listshowLastRowPlus: boolean;
    ajaxCount: number;

    constructor(
        private LegalinformationService: LegalinformationService,
        private AlertService: AlertService,
        private StudentService: StudentService,
        private spinner: NgxSpinnerService,
        private route: ActivatedRoute,
        private router: Router

    ) { }
    ngDoCheck() {
        if (this.ajaxCount == 0 && this.legalinformationList.length > 0) {
            this.ajaxCount = 20;
            console.log("asdfsdf");
            this.showIcons();
        }
        const change = this.ajaxCount;
        console.log(this.ajaxCount);
        if (this.ajaxCount == 0) {
            this.editFunction();
            this.spinner.hide();
            this.ajaxCount = 10;
        }
    }
    showIcons() {
        if (this.legalinformationList.length > 0) {
            this.showLastRowMinus = true;
            this.listshowLastRowMinus = true;
            this.showLastRowPlus = true;
        }

        if (this.legalinformationList.length > 1) {
            if (this.showLastRow == false) {
                this.listshowLastRowPlus = false;
                this.listshowLastRowMinus = true;
                this.showLastRowPlus = true;

            } else {
                this.listshowLastRowPlus = true;
                this.listshowLastRowMinus = false;

            }
        }
        if (this.legalinformationList.length == 1 && this.showLastRow == false) {
            this.listshowLastRowMinus = true;
            this.listshowLastRowPlus = false;

        }
        if (this.legalinformationList.length == 1 && this.showLastRow == true) {
            this.listshowLastRowMinus = false;
            this.listshowLastRowPlus = true;

        }
        if (this.legalinformationList.length == 0 && this.showLastRow == false) {
            this.showLastRowMinus = false;
            this.showLastRowPlus = true;
        }
        if (this.legalinformationList.length == 0 && this.showLastRow == true) {
            this.showLastRowMinus = true;
            this.showLastRowPlus = false;
        }
        console.log(this.legalinformationList.length);
        console.log("Show last row" + this.showLastRow);
        console.log("showLastRowMinus" + this.showLastRowMinus);
        console.log("showLastRowPlus" + this.showLastRowPlus);
        console.log("listshowLastRowMinus" + this.listshowLastRowMinus);
        console.log("listshowLastRowPlus" + this.listshowLastRowPlus);


    }
    deletemodule() {
        this.showLastRow = true;
        this.showIcons();
    }
    showNewRow() {
        var confirmPop = confirm("Do you want to Add?");
        if (confirmPop == false) {
            return false;
        }
        this.showLastRow = false;
        this.showIcons();
    }



    editFunction() {
        this.ajaxCount++;
        if (this.id > 0) {
            this.LegalinformationService.getItemsDetail(this.id).subscribe(
                data => {
                    this.ajaxCount--;
                    this.legalinfoDataheader['f026fname'] = data['result'][0]['f026fname'];
                    this.legalinfoDataheader['f026fphone'] = data['result'][0]['f026fphone'];
                    this.legalinfoDataheader['f026femail'] = data['result'][0]['f026femail'];
                    this.legalinfoDataheader['f026ffax'] = data['result'][0]['f026ffax'];
                    this.legalinfoDataheader['f026address'] = data['result'][0]['f026address'];
                    this.legalinformationList = data['result'];
                    this.showIcons();

                }, error => {
                    console.log(error);
                });
        }
    }
    ngOnInit() {
        this.ajaxCount = 0;
        this.spinner.show();
        this.ajaxCount++;
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id < 1) {
            this.showLastRow = false;
            this.showLastRowMinus = false;
            this.showLastRowPlus = true;
        } else {
            this.showLastRow = true;

        }
        this.StudentService.getActiveItems().subscribe(
            data => {
                this.ajaxCount--;
                this.studentList = data['result']['data'];
            }, error => {
                console.log(error);
            });
    }

    saveLegalinformation() {


        if (this.legalinfoDataheader['f026fname'] == undefined) {
            alert("Enter Legal Associate");
            return false;
        }
        if (this.legalinfoDataheader['f026ffax'] == undefined || this.legalinfoDataheader['f026ffax'] == '') {
            alert("Enter the FAX Number");
            return false;
        }

        if (this.legalinfoDataheader['f026fphone'] == undefined) {
            alert("Enter Phone Number");
            return false;
        }
        if (this.showLastRow == false) {
            var addactivities = this.addlegalinformationlist();
            if (addactivities = true) {
                return false;
            }
        }
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        let invoiceObject = {};
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        console.log(this.id);
        if (this.id > 0) {
            invoiceObject['f098fid'] = this.id;
        }
        invoiceObject['f026fname'] = this.legalinfoDataheader['f026fname'];
        invoiceObject['f026fphone'] = this.legalinfoDataheader['f026fphone'];
        invoiceObject['f026femail'] = this.legalinfoDataheader['f026femail'];
        invoiceObject['f026ffax'] = this.legalinfoDataheader['f026ffax'];
        invoiceObject['f026address'] = this.legalinfoDataheader['f026address'];

        invoiceObject['f026fstatus'] = 0;
        invoiceObject['legal-details'] = this.legalinformationList;
        // if(this.legalinformationList.length>0) {

        // } else {
        //     alert("Please add atleast one student");
        //     return false;
        // }
        console.log(invoiceObject);

        if (this.id > 0) {
            this.LegalinformationService.updateLegalinformationItems(invoiceObject, this.id).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert("Legal Associate Already exists")
                    }
                    else {
                        this.router.navigate(['studentfinance/legalinformation']);
                        alert("Legal Information has been Updated Sucessfully !");
                    }

                }, error => {
                    alert("Legal Associate Already exists")
                    console.log(error);
                });
        } else {
            this.LegalinformationService.insertLegalinformationItems(invoiceObject).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert("Legal Associate Already exists")
                    }
                    else {
                        this.router.navigate(['studentfinance/legalinformation']);
                        alert("Legal Information has been Added Sucessfully !");
                    }

                }, error => {
                    console.log(error);
                });
        }
    }
    deletelegalinformation(object) {
        var cnf = confirm("Do you really want to delete");
        if (cnf == true) {
            console.log(this.legalinformationList);
            var index = this.legalinformationList.indexOf(object);
            this.deleteList.push(this.legalinformationList[index]['f026fidDetails']);
            let deleteObject = {};
            deleteObject['id'] = this.deleteList;
            this.LegalinformationService.getdeleteItems(deleteObject).subscribe(
                data => {
                }, error => {
                    console.log(error);
                })
            if (index > -1) {
                this.legalinformationList.splice(index, 1);
            }

            this.showIcons();
        }
    }


    addlegalinformationlist() {
        if (this.legalinformationData['f026fidStudent'] == undefined) {
            alert(" Select Student");
            return false;
        }
        if (this.legalinformationData['f026fdescription'] == undefined) {
            alert(" Enter the Description");
            return false;
        }
        var confirmPop = confirm("Do you want to Add?");
        if (confirmPop == false) {
            return false;
        }
        console.log(this.legalinformationData);
        var dataofCurrentRow = this.legalinformationData;
        this.legalinformationData = {};
        this.legalinformationList.push(dataofCurrentRow);
        this.showIcons();
        // console.log(this.legalinformationList);

    }
    onlyNumberKey(event) {
        console.log(event);
        return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
    }
    maximize() {
        $("#myModal").addClass("modal");
        $("#overlay1").show();
        $(".maximize-icon").addClass("hidden");
        $(".minimize-icon").removeClass("hidden");
        $(".modal-backdrop").show();

    }
    minimize() {
        $("#myModal").removeClass("modal");
        $("#overlay1").hide();
        $(".maximize-icon").removeClass("hidden");
        $(".minimize-icon").addClass("hidden");
        $(".modal-backdrop").hide();
        $('#myModal').show();

    }

}


