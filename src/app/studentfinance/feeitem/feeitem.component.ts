import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { FeeitemService } from '../service/feeitem.service'
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'Feeitem',
    templateUrl: 'feeitem.component.html'
})

export class FeeitemComponent implements OnInit {
    feeitemList =  [];
    feeitemData = {};
    id: number;

    constructor(
        private FeeitemService: FeeitemService,
        private spinner: NgxSpinnerService,        

    ) { }

    ngOnInit() { 
        this.spinner.show();                                        
        this.FeeitemService.getItems().subscribe(
            data => {
                this.spinner.hide();
                this.feeitemList = data['result']['data'];
        }, error => {
            console.log(error);
        });
    }

    addFeeitem(){
           console.log(this.feeitemData);
        this.FeeitemService.insertfeeitemItems(this.feeitemData).subscribe(
            data => {
                this.feeitemList = data['todos'];
            }, error => {
                console.log(error);
            });

    }
}