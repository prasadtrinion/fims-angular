import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { FeeitemService } from '../../service/feeitem.service'
import { FeecategoryService } from '../../service/feecategory.service'
import { TaxsetupcodeService } from '../../../generalledger/service/taxsetupcode.service'
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { DefinationmsService } from '../../service/definationms.service';
import { FundService } from '../../../generalsetup/service/fund.service';
import { ActivitycodeService } from '../../../generalsetup/service/activitycode.service';
import { AccountcodeService } from '../../../generalsetup/service/accountcode.service';
import { DepartmentService } from '../../../generalsetup/service/department.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'FeeitemFormComponent',
    templateUrl: 'feeitemform.component.html'
})

export class FeeitemFormComponent implements OnInit {
    feeitemform: FormGroup;
    feeItemList = [];
    feeitemData = {};
    glcodeList = [];
    deleteList = [];
    feecategoryList = [];
    feecategoryData = {};
    taxsetupcodeList = [];
    taxsetupcodeData = {};
    calculationTypeList = [];
    feeItemDataHeader = [];
    DeptList = [];
    fundList = [];
    accountcodeList = [];
    activitycodeList = [];
    ajaxCount: number;
    showLastRow: boolean;
    showLastRowMinus: boolean;
    showLastRowPlus: boolean;
    listshowLastRowMinus: boolean;
    listshowLastRowPlus: boolean;
    id: number;
    constructor(
        private FeeitemService: FeeitemService,
        private FeecategoryService: FeecategoryService,
        private TaxsetupcodeService: TaxsetupcodeService,
        private DefinationmsService: DefinationmsService,
        private route: ActivatedRoute,
        private router: Router,
        private ActivitycodeService: ActivitycodeService,
        private AccountcodeService: AccountcodeService,
        private DepartmentService: DepartmentService,
        private FundService: FundService,
        private spinner: NgxSpinnerService,

    ) { }

    ngDoCheck() {
        const change = this.ajaxCount;
        console.log(this.ajaxCount);
        if (this.ajaxCount == 0 && this.feeItemList.length > 0) {
            this.ajaxCount = 20;
            console.log("asdfsdf");
            this.showIcons();
        }
        if (this.ajaxCount == 0) {
            this.editFunction();
            this.ajaxCount = 10;
            this.spinner.hide();
        }
    }
    showIcons() {
        // this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.feeItemList.length > 0) {
            this.showLastRowMinus = true;
            this.showLastRowPlus = true;
            this.listshowLastRowMinus = true;
        }

        if (this.feeItemList.length > 1) {
            if (this.showLastRow == false) {
                this.listshowLastRowPlus = false;
                this.listshowLastRowMinus = true;
                this.showLastRowPlus = true;

            } else {
                this.listshowLastRowPlus = true;
                this.listshowLastRowMinus = false;

            }
        }
        if (this.feeItemList.length == 1 && this.showLastRow == false) {
            this.listshowLastRowMinus = true;
            this.listshowLastRowPlus = false;

        }
        if (this.feeItemList.length == 1 && this.showLastRow == true) {
            this.listshowLastRowMinus = false;
            this.listshowLastRowPlus = true;

        }
        if (this.feeItemList.length == 0 && this.showLastRow == false) {
            this.showLastRowMinus = false;
            this.showLastRowPlus = true;
        }
        if (this.feeItemList.length == 0 && this.showLastRow == true) {
            this.showLastRowMinus = true;
            this.showLastRowPlus = false;
        }
        console.log(this.feeItemList.length);
        console.log("Show last row" + this.showLastRow);
        console.log("showLastRowMinus" + this.showLastRowMinus);
        console.log("showLastRowPlus" + this.showLastRowPlus);
        console.log("listshowLastRowMinus" + this.listshowLastRowMinus);
        console.log("listshowLastRowPlus" + this.listshowLastRowPlus);

        // if(this.viewDisabled==true || this.purchaseorderDataheader['f034fapprovalStatus']==1 || this.purchaseorderDataheader['f034fapprovalStatus'] == 2) {
        //     this.listshowLastRowPlus = false;
        // }

    }
    deletemodule() {
        this.showLastRow = true;
        this.showIcons();
    }
    showNewRow() {
        var confirmPop = confirm("Do you want to Add?");
        if (confirmPop == false) {
            return false;
        }
        this.showLastRow = false;
        this.showIcons();
    }
    editFunction() {
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.ajaxCount++;
        if (this.id > 0) {
            this.FeeitemService.getItemsDetail(this.id).subscribe(
                data => {
                    this.ajaxCount--;
                    this.feeItemDataHeader = data['result'];

                    this.feeItemDataHeader['f066fname'] = data['result'][0]['f066fname'];
                    this.feeItemDataHeader['f066fcode'] = data['result'][0]['f066fcode'];
                    this.feeItemDataHeader['f066fdescription'] = data['result'][0]['f066fdescription'];
                    this.feeItemDataHeader['f066fidFeeCategory'] = data['result'][0]['f066fidFeeCategory'];
                    this.feeItemDataHeader['f066fidTaxCode'] = data['result'][0]['f066fidTaxCode'];
                    this.feeItemDataHeader['f066finsuredItem'] = data['result'][0]['f066finsuredItem'];
                    this.feeItemDataHeader['f066fsoCode'] = data['result'][0]['f066fsoCode'];
                    this.feeItemList = data['result'];
                    // this.feeitemData = data['result'][0];
                    // console.log(this.feeitemData)
                    // this.feeitemData['f066factivity'] = data['result'][0]['f066factivity'].trim();

                    // // converting from string to int for radio button default selection
                    this.feeItemDataHeader['f066fstatus'] = parseInt(data['result'][0]['f066fstatus']);
                    this.feeItemDataHeader['f066frefundable'] = parseInt(data['result'][0]['f066frefundable']);
                    this.feeItemDataHeader['f066finvoiceItem'] = parseInt(data['result'][0]['f066finvoiceItem']);
                    this.feeItemDataHeader['f066fsoa'] = parseInt(data['result'][0]['f066fsoa']);
                    this.showIcons();
                }, error => {
                    console.log(error);
                });
        }
    }

    ngOnInit() {
        this.ajaxCount = 0;
        this.spinner.show();
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id < 1) {
            this.showLastRow = false;
            this.showLastRowMinus = false;
            this.showLastRowPlus = true;
        } else {
            this.showLastRow = true;
        }
        this.feeItemDataHeader['f066fstatus'] = 1;
        this.feeItemDataHeader['f066finvoiceItem'] = 1;
        this.feeItemDataHeader['f066fsoa'] = 1;
        this.feeItemDataHeader['f066frefundable'] = 1;

        this.feeitemform = new FormGroup({
            feeitem: new FormControl('', Validators.required)
        });
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.ajaxCount = 0;
        this.ajaxCount++;
        this.FundService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.fundList = data['result']['data'];
            }, error => {
                console.log(error);
            });
        // activity dropdown
        this.ajaxCount++;
        this.ActivitycodeService.getItemsByLevel().subscribe(
            data => {
                console.log(data);
                this.ajaxCount--;
                this.activitycodeList = data['result']['data'];
                // for (var i = 0; i < this.activitycodeList.length; i++) {
                //     this.activitycodeList[i]['f058fcompleteCode'] = '0' + this.activitycodeList[i]['f058fcompleteCode'];
                //     this.activitycodeList[i]['f058fcompleteCode'].trim();
                // }

                console.log(this.activitycodeList);
            }, error => {
                console.log(error);
            });
        // account dropdown
        this.ajaxCount++;
        this.AccountcodeService.getItemsByLevel().subscribe(
            data => {
                this.ajaxCount--;
                this.accountcodeList = data['result']['data'];
                for (var i = 0; i < this.accountcodeList.length; i++) {
                    if (this.accountcodeList[i]['f059fid'] == '1046') {
                        this.accountcodeList[i]['f059fcompleteCode'] = '00000';
                    }
                }
            }, error => {
                console.log(error);
            });

        // department dropdown
        this.ajaxCount++;
        this.DepartmentService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.DeptList = data['result']['data'];
            }, error => {
                console.log(error);
            });



        //calculationType drop down 
        this.ajaxCount++;
        this.DefinationmsService.getCalculationtype('calculationType').subscribe(
            data => {
                this.ajaxCount--;
                this.calculationTypeList = data['result'];
            }, error => {
                console.log(error);
            });

        //fee category drop down 
        this.ajaxCount++;
        this.FeecategoryService.getActive().subscribe(
            data => {
                this.ajaxCount--;
                this.feecategoryList = data['result'];
            }, error => {
                console.log(error);
            });

        this.ajaxCount++;
        // Taxsetup code dropdown
        this.TaxsetupcodeService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.taxsetupcodeList = data['result']['data'];
            }, error => {
                console.log(error);
            });
        this.showIcons();
        console.log(this.id);
    }

    savefeeItem() {
        if (this.feeItemDataHeader['f066fname'] == undefined) {
            alert("Enter Name");
            return false;
        }
        if (this.feeItemDataHeader['f066fcode'] == undefined) {
            alert("Enter Code");
            return false;
        }
        if (this.feeItemDataHeader['f066fdescription'] == undefined) {
            alert("Enter Description");
            return false;
        }

        if (this.feeItemDataHeader['f066fidFeeCategory'] == undefined) {
            alert("Select Fee Category");
            return false;
        }

        if (this.feeItemDataHeader['f066fidTaxCode'] == undefined) {
            alert("Select Tax Code");
            return false;
        }

        if (this.feeItemDataHeader['f066fsoCode'] == undefined) {
            alert("Enter So Code");
            return false;
        }

        if (this.feeItemDataHeader['f066finsuredItem'] == undefined) {
            alert("Enter Insured Item");
            return false;
        }

        if (this.feeItemDataHeader['f066finvoiceItem'] == undefined) {
            alert("Select Invoice Item");
            return false;
        }

        if (this.feeItemDataHeader['f066finsuredItem'] == undefined) {
            alert("Select Insured Item");
            return false;
        }

        if (this.feeItemDataHeader['f066frefundable'] == undefined) {
            alert("Select  Refundale ");
            return false;
        }
        if (this.feeItemDataHeader['f066fsoa'] == undefined) {
            alert("Select SOA ");
            return false;
        }
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        if (this.showLastRow == false) {
            var addactivities = this.addfeeItemList();
            if (addactivities == false) {
                return false;
            }

        }

        let feeItemObject = {};
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        console.log(this.id);
        if (this.id > 0) {
            feeItemObject['f066fid'] = this.id;
        }

        // detail entry code validation
        if (this.feeItemList.length > 0) {

        } else {
            alert("Please add one details");
            return false;
        }
        feeItemObject['f066fname'] = this.feeItemDataHeader['f066fname'];
        feeItemObject['f066fcode'] = this.feeItemDataHeader['f066fcode'];
        feeItemObject['f066fdescription'] = this.feeItemDataHeader['f066fdescription'];
        feeItemObject['f066fidFeeCategory'] = this.feeItemDataHeader['f066fidFeeCategory'];
        feeItemObject['f066fidTaxCode'] = this.feeItemDataHeader['f066fidTaxCode'];
        feeItemObject['f066fsoCode'] = this.feeItemDataHeader['f066fsoCode'];

        feeItemObject['f066finsuredItem'] = this.feeItemDataHeader['f066finsuredItem'];
        feeItemObject['f066finvoiceItem'] = this.feeItemDataHeader['f066finvoiceItem'];
        feeItemObject['f066frefundable'] = this.feeItemDataHeader['f066frefundable'];
        feeItemObject['f066fsoa'] = this.feeItemDataHeader['f066fsoa'];
        feeItemObject['f066fstatus'] = this.feeItemDataHeader['f066fstatus'];
        feeItemObject['fee-details'] = this.feeItemList;

        console.log(feeItemObject);

        if (this.id > 0) {
            this.FeeitemService.updatefeeitemItems(feeItemObject, this.id).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert('Code Already Exist');
                    }
                    else {
                        alert("Fee item has been  Updated Sucessfully !! !!")
                        this.router.navigate(['studentfinance/feeitem']);
                    }
                }, error => {
                    alert('Code Already Exist');
                    console.log(error);
                });
        } else {
            this.FeeitemService.insertfeeitemItems(feeItemObject).subscribe(
                data => {

                    if (data['status'] == 409) {
                        alert('Code Already Exist');

                    }
                    else {
                        // this.AlertService.success("Salary has been Added Sucessfully  !!")
                        alert('Fee item has been Added Sucessfully  !!"');
                        this.router.navigate(['studentfinance/feeitem']);
                    }
                }, error => {
                    console.log(error);
                });
        }

    }
    deleteitemList(object) {
        var cnf = confirm("Do you really want to delete");
        if (cnf == true) {
            console.log(this.feeItemList);
            var index = this.feeItemList.indexOf(object);
            this.deleteList.push(this.feeItemList[index]['f066fidDetails']);
            let deleteObject = {};
            deleteObject['id'] = this.deleteList;
            this.FeeitemService.getDeleteItems(deleteObject).subscribe(
                data => {
                }, error => {
                    console.log(error);
                })
            if (index > -1) {
                this.feeItemList.splice(index, 1);
            }
            this.showIcons();
        }
    }

    addfeeItemList() {
        // console.log(this.feeitemData);
        if (this.feeitemData['f066fdrFund'] == undefined) {
            alert(" Select Debit Fund");
            return false;
        }
        if (this.feeitemData['f066fdrActivity'] == undefined) {
            alert(" Select Debit Activity Code");
            return false;
        }
        if (this.feeitemData['f066fdrDepartment'] == undefined) {
            alert(" SelectDebit Department");
            return false;
        }
        if (this.feeitemData['f066fdrAccount'] == undefined) {
            alert(" Select Debit Account Code");
            return false;
        }
        if (this.feeitemData['f066fcrFund'] == undefined) {
            alert(" Select Credit Fund");
            return false;
        }
        if (this.feeitemData['f066fcrActivity'] == undefined) {
            alert(" Select Credit Activity Code");
            return false;
        }
        if (this.feeitemData['f066fcrDepartment'] == undefined) {
            alert(" Select Credit Department");
            return false;
        }
        if (this.feeitemData['f066fcrAccount'] == undefined) {
            alert(" Select Credit Account Code");
            return false;
        }
        var confirmPop = confirm("Do you want to Add?");
        if (confirmPop == false) {
            return false;
        }
        if (this.feeitemData['f066fdrAccount'] == this.feeitemData['f066fcrAccount']) {
            alert("DebitGl Account Code and CreditGl Account Code cannot be same");
            return false;
        }
        var dataofCurrentRow = this.feeitemData;
        this.feeitemData = {};
        this.feeItemList.push(dataofCurrentRow);
        console.log(this.feeItemList);
        this.showIcons();
    }
    maximize() {
        $("#myModal").addClass("modal");
        $("#overlay1").show();
        $(".maximize-icon").addClass("hidden");
        $(".minimize-icon").removeClass("hidden");
        $(".modal-backdrop").show();

    }
    minimize() {
        $("#myModal").removeClass("modal");
        $("#overlay1").hide();
        $(".maximize-icon").removeClass("hidden");
        $(".minimize-icon").addClass("hidden");
        $(".modal-backdrop").hide();
        $('#myModal').show();

    }
}