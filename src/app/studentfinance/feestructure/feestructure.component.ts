import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { FeestructureService } from '../service/feestructure.service'
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'FeestructureComponent',
    templateUrl: 'feestructure.component.html'
})

export class FeestructureComponent implements OnInit {
    feeStructureList = [];
    feeStructureData = {};
    id: number;

    constructor(
        
        private FeestructureService: FeestructureService,
        private spinner: NgxSpinnerService,        

    ) { }

    ngOnInit() { 
        this.spinner.show();                                                
        this.FeestructureService.getItems().subscribe(
            data => {
                this.spinner.hide();
                this.feeStructureList = data['result']['data'];
        }, error => {
            console.log(error);
        });
    }

   
}