import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { FeestructureService } from '../../service/feestructure.service'
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { DefinationmsService } from '../../service/definationms.service';
import { CurrencysetupService } from '../../service/currencysetup.service';
import { FeeitemService } from '../../service/feeitem.service';
import { AlertService } from '../../../_services/alert.service';
import { FeecategoryService } from '../../service/feecategory.service'
import { FundService } from '../../../generalsetup/service/fund.service';
import { AccountcodeService } from '../../../generalsetup/service/accountcode.service'
import { ActivitycodeService } from '../../../generalsetup/service/activitycode.service';
import { DepartmentService } from '../../../generalsetup/service/department.service';
import { FeetypeService } from "../../service/feetype.service";
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'FeestructureFormComponent',
    templateUrl: 'feestructureform.component.html'
})

export class FeestructureFormComponent implements OnInit {
    feecategoryform: FormGroup;
    feeStructureList = [];
    feeStructureDataHeader = {};
    feeStructureData = {};
    intakeList = [];
    fundList = [];
    activitycodeList = [];
    DeptList = [];
    accountcodeList = [];
    programmeList = [];
    glcodeList = [];
    citizenList = [];
    regionList = [];
    studentCategoryList = [];
    modeOfStudyList = [];
    calculationTypeList = [];
    currencySetupList = [];
    feeItemList = [];
    feeTypeList = [];
    feeStructureProgramList = [];
    feeStructureProgramData = {};
    feecategoryList = [];
    feeStructureProgramFeeList = [];
    feeStructureProgramFeeData = {};
    deleteFeestructureList = [];
    deleteProgramList = [];
    saveBtnDisable: boolean;
    graduationType = [];
    semesterList = [];
    learningList = [];
    itemList = [];
    showLastRow: boolean;
    showLastRowMinus: boolean;
    showLastRowPlus: boolean;
    listshowLastRowMinus: boolean;
    listshowLastRowPlus: boolean;
    showLastRow1: boolean;
    showLastRowMinus1: boolean;
    showLastRowPlus1: boolean;
    listshowLastRowMinus1: boolean;
    listshowLastRowPlus1: boolean;
    id: number;
    ajaxCount: number;
    readonly: boolean;
    program: boolean;
    editDisabled: boolean;
    constructor(
        private FeestructureService: FeestructureService,
        private DefinationmsService: DefinationmsService,
        private CurrencysetupService: CurrencysetupService,
        private FundService: FundService,
        private DepartmentService: DepartmentService,
        private ActivitycodeService: ActivitycodeService,
        private AccountcodeService: AccountcodeService,
        private FeecategoryService: FeecategoryService,
        private FeeitemService: FeeitemService,
        private FeetypeService: FeetypeService,
        private AlertService: AlertService,
        private spinner: NgxSpinnerService,
        private route: ActivatedRoute,
        private router: Router

    ) { }

    ngDoCheck() {
        const change = this.ajaxCount;
        // console.log(this.ajaxCount)
        if (this.ajaxCount == 0 && this.feeStructureList.length > 0) {
            this.ajaxCount = 20;
            console.log("asdfsdf");
            this.showIcons();
        };
        if (this.ajaxCount == 0) {
            this.editFunction();
            this.spinner.hide();
            this.readonly = true;
            this.ajaxCount = 10;
        }
    }
    deletemodule() {


        this.showLastRow = true;
        this.showIcons();
    }

    showIcons() {
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        if (this.feeStructureList.length > 0) {
            this.showLastRowMinus = true;
            this.listshowLastRowMinus = true;
        }

        if (this.feeStructureList.length > 1) {
            if (this.showLastRow == false) {
                this.listshowLastRowPlus = false;
                this.listshowLastRowMinus = true;
                this.showLastRowPlus = true;

            } else {
                this.listshowLastRowPlus = true;
                this.listshowLastRowMinus = false;

            }
        }
        if (this.feeStructureList.length == 1 && this.showLastRow == false) {
            this.listshowLastRowMinus = true;
            this.listshowLastRowPlus = false;

        }
        if (this.feeStructureList.length == 1 && this.showLastRow == true) {
            this.listshowLastRowMinus = false;
            this.listshowLastRowPlus = true;

        }
        if (this.feeStructureList.length == 0 && this.showLastRow == false) {
            this.showLastRowMinus = false;
            this.showLastRowPlus = true;
        }
        if (this.feeStructureList.length == 0 && this.showLastRow == true) {
            this.showLastRowMinus = true;
            this.showLastRowPlus = false;
        }
        //  if(this.feeStructureList.length>0){
        //     if(this.viewDisabled==true || this.feeStructureList[0]['f017fapprovedStatus']==1 || this.feeStructureList[0]['f017fapprovedStatus'] == 2) {
        //         this.listshowLastRowPlus = false;
        //     }
        // }
        // else{
        //     if(this.viewDisabled==true) {
        //         this.listshowLastRowPlus = false;
        //     }
        // }
        console.log(this.feeStructureList.length);
        console.log("Show last row" + this.showLastRow);
        console.log("showLastRowMinus" + this.showLastRowMinus);
        console.log("showLastRowPlus" + this.showLastRowPlus);
        console.log("listshowLastRowMinus" + this.listshowLastRowMinus);
        console.log("listshowLastRowPlus" + this.listshowLastRowPlus);
    }
    showNewRow() {
        var confirmPop = confirm("Do you want to Add?");
        if (confirmPop == false) {
            return false;
        }
        this.showLastRow = false;
        this.showIcons();
    }
    deletemodule1() {
        this.showLastRow1 = true;
        this.showIcons1();
    }

    showIcons1() {
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        if (this.feeStructureProgramList.length > 0) {
            this.showLastRowMinus1 = true;
            this.listshowLastRowMinus1 = true;
            this.showLastRowPlus1 = true;
        }
        if (this.feeStructureProgramList.length > 1) {
            if (this.showLastRow1 == false) {
                this.listshowLastRowPlus1 = false;
                this.listshowLastRowMinus1 = true;
                this.showLastRowPlus1 = true;

            } else {
                this.listshowLastRowPlus1 = true;
                this.listshowLastRowMinus1 = false;

            }
        }
        if (this.feeStructureProgramList.length == 1 && this.showLastRow1 == false) {
            this.listshowLastRowMinus1 = true;
            this.listshowLastRowPlus1 = false;

        }
        if (this.feeStructureProgramList.length == 1 && this.showLastRow1 == true) {
            this.listshowLastRowMinus1 = false;
            this.listshowLastRowPlus1 = true;

        }
        if (this.feeStructureProgramList.length == 0 && this.showLastRow1 == false) {
            this.showLastRowMinus1 = false;
            this.showLastRowPlus1 = true;
        }
        if (this.feeStructureProgramList.length == 0 && this.showLastRow1 == true) {
            this.showLastRowMinus1 = true;
            this.showLastRowPlus1 = false;
        }
    }
    showNewRow1() {
        var confirmPop = confirm("Do you want to Add?");
        if (confirmPop == false) {
            return false;
        }
        this.showLastRow1 = false;
        this.showIcons1();
    }
    editFunction() {
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.ajaxCount++;
        if (this.id > 0) {
            this.FeestructureService.getItemsDetail(this.id).subscribe(
                data => {
                    this.ajaxCount--;
                    this.feeStructureList = data['result']['fee-details'];
                    this.feeStructureProgramList = data['result']['program-details'];
                    this.feeStructureProgramFeeList = data['result']['course-details'];

                    this.feeStructureDataHeader['f048fname'] = data['result']['f048fname'];
                    this.feeStructureDataHeader['f048fcode'] = data['result']['f048fcode'];
                    this.feeStructureDataHeader['f048feffectiveDate'] = data['result']['f048feffectiveDate'];
                    this.feeStructureDataHeader['f048fidIntake'] = data['result']['f048fidIntake'];
                    this.feeStructureDataHeader['f048flearningCentre'] = data['result']['f048flearningCentre'];
                    this.feeStructureDataHeader['f048fcitizen'] = data['result']['f048fcitizen'];
                    this.feeStructureDataHeader['f048fisProgramAttached'] = parseInt(data['result']['f048fisProgramAttached']);

                    this.feeStructureDataHeader['f048fpartner'] = data['result']['f048fpartner'];
                    this.feeStructureDataHeader['f048fregion'] = data['result']['f048fregion'];
                    this.feeStructureDataHeader['f048fstudentCategory'] = data['result']['f048fstudentCategory'];
                    this.feeStructureDataHeader['f048ffeeType'] = data['result']['f048ffeeType'];
                    this.feeStructureDataHeader['f048fstudyMode'] = data['result']['f048fstudyMode'];
                    this.feeStructureDataHeader['f048ftotal'] = data['result']['f048ftotal'];
                    this.feeStructureDataHeader['f048fgraduationType'] = data['result']['f048fgraduationType'];
                    this.getViewData();
                    // this.getSubCategories();
                    this.saveBtnDisable = true;
                    setTimeout(function () {

                        //     $("#target input,select,number,ng-select").prop("disabled", true);
                        // $("#target1 input,select,number").prop("disabled", true);
                        // $("#target2 input,select,number").prop("disabled", true);

                    }, 3000);
                    this.showIcons();
                    this.showIcons1();
                    this.editDisabled = true;
                    this.listshowLastRowPlus = false;
                    this.listshowLastRowPlus1 = false;
                    console.log(this.feeStructureList);
                }, error => {
                    console.log(error);
                });
        }
    }

    ngOnInit() {
        this.ajaxCount = 0
        this.spinner.show();

        let regionObject = {};
        regionObject['id'] = 1;
        regionObject['name'] = "Asean";
        this.regionList.push(regionObject);
        regionObject = {};
        regionObject['id'] = 2;
        regionObject['name'] = "Non-Asean";
        this.regionList.push(regionObject);

        let citizenObject = {};
        citizenObject['id'] = 1;
        citizenObject['name'] = "Local";
        citizenObject['value'] = "M";
        this.citizenList.push(citizenObject);
        citizenObject = {};
        citizenObject['id'] = 2;
        citizenObject['name'] = "International";
        citizenObject['value'] = "NM";
        this.citizenList.push(citizenObject);

        this.editDisabled = false;
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id < 1) {
            this.showLastRow = false;
            this.showLastRowMinus = false;
            this.showLastRowPlus = true;
        } else {
            this.showLastRow = true;

        }
        if (this.id < 1) {
            this.showLastRow1 = false;
            this.showLastRowMinus1 = false;
            this.showLastRowPlus1 = true;
        } else {
            this.showLastRow1 = true;

        }
        this.feeStructureDataHeader['f0485fstatus'] = 1;
        this.feeStructureDataHeader['f048fisProgramAttached'] = 1;
        this.readonly = false;

        let semesterobj = {};
        semesterobj['name'] = 'First Semester';
        semesterobj['value'] = 0;
        this.semesterList.push(semesterobj);

        semesterobj = {};
        semesterobj['name'] = 'Second Semester';
        semesterobj['value'] = 1;
        this.semesterList.push(semesterobj);

        this.FeeitemService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.feeItemList = data['result']['data'];
            }, error => {
                console.log(error);
            });

        this.ajaxCount = 0;
        //Department dropdown
        this.ajaxCount++;
        this.DepartmentService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.DeptList = data['result']['data'];
            }
        );


        //Activity dropdown
        this.ajaxCount++;
        this.ActivitycodeService.getItemsByLevel().subscribe(
            data => {
                this.ajaxCount--;
                this.activitycodeList = data['result']['data'];
            }
        );

        //Account dropdown
        this.ajaxCount++;
        this.AccountcodeService.getItemsByLevel().subscribe(
            data => {
                this.ajaxCount--;
                this.accountcodeList = data['result']['data'];
            }
        );


        //Fund dropdown
        this.ajaxCount++;
        this.FundService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.fundList = data['result']['data'];
            }
        );




        this.ajaxCount++;
        this.FeecategoryService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.feecategoryList = data['result']['data'];
            }, error => {
                console.log(error);
            });

        //feeType Dropdown
        this.ajaxCount++;
        this.FeetypeService.getActiveItems().subscribe(
            data => {
                this.ajaxCount--;
                this.feeTypeList = data['result']['data'];
            }, error => {
                console.log(error)
            });
        //StudentCategory drop down
        this.ajaxCount++;

        this.DefinationmsService.getStudentCategory('StudentCategory').subscribe(
            data => {
                this.ajaxCount--;

                this.studentCategoryList = data['result'];
            }, error => {
                console.log(error);
            });

        this.ajaxCount++;

        this.DefinationmsService.getScheme('ProgrammeScheme').subscribe(
            data => {
                this.ajaxCount--;

                this.graduationType = data['result'];
            }, error => {
                console.log(error);
            });


        //ModeOfStudy drop down 
        this.ajaxCount++;

        this.DefinationmsService.getModeOfStudy('ModeOfStudy').subscribe(
            data => {
                this.ajaxCount--;

                this.modeOfStudyList = data['result'];
            }, error => {
                console.log(error);
            });


        //CalculationType drop down CurrencysetupService
        this.ajaxCount++;

        this.DefinationmsService.getCalculationType('CalculationType').subscribe(
            data => {
                this.ajaxCount--;

                this.calculationTypeList = data['result'];
            }, error => {
                console.log(error);
            });


        //Currencysetup drop down 
        this.ajaxCount++;

        this.CurrencysetupService.getItems().subscribe(
            data => {
                this.ajaxCount--;

                this.currencySetupList = data['result']['data'];
            }, error => {
                console.log(error);
            });

        this.feecategoryform = new FormGroup({
            feecategory: new FormControl('', Validators.required)
        });
        //Citizen drop down 
        // this.ajaxCount++;

        // this.DefinationmsService.getCitizen('Citizen').subscribe(
        //     data => {
        //         this.ajaxCount--;

        //         this.citizenList = data['result'];
        //     }, error => {
        //         console.log(error);
        //     });

        //Region drop down 

        this.showIcons1();
        this.showIcons();

    }
    getSubCategories() {
        this.ajaxCount++;

        this.FeeitemService.getFeeItemByCategory(this.feeStructureData['f048fidFeeCategory']).subscribe(
            data => {
                this.ajaxCount--;
                this.feeItemList = data['result'];
            }, error => {
                console.log(error);
            });

    }
    getProgram() {
        this.program = true;
        if (this.feeStructureDataHeader['f048fgraduationType'] == "UG") {
            //Intake drop down 

            // this.ajaxCount++;

            // this.FeestructureService.getUgSemester().subscribe(
            //     data => {
            //         this.ajaxCount--;
            //         this.intakeList = data['result'];
            //     }, error => {
            //         console.log(error);
            //     });

            //Programme drop down 
            this.ajaxCount++;

            this.FeestructureService.getUgPrograms().subscribe(
                data => {
                    this.ajaxCount--;
                    this.programmeList = data['result'];
                }, error => {
                    console.log(error);
                });



        }
        else {
            this.program = false;
            //Intake drop down 

            // this.ajaxCount++;

            // this.FeestructureService.getPgSemester().subscribe(
            //     data => {
            //         this.ajaxCount--;
            //         this.intakeList = data['result'];
            //     }, error => {
            //         console.log(error);
            //     });

            //Programme drop down 
            this.ajaxCount++;

            this.FeestructureService.getPgProgram().subscribe(
                data => {
                    this.ajaxCount--;
                    this.programmeList = data['result'];
                }, error => {
                    console.log(error);
                });
        }
    }
    ConvertToFloat(val) {
        return parseFloat(val);
    }

    addfeeStructureProgramList() {
        if (this.feeStructureProgramData['f048fidProgramme'] == undefined) {
            alert("Select Program");
            return false;
        }
        // var confirmPop = confirm("Do you want to Add?");
        // if (confirmPop == false) {
        //     return false;
        // }
        var dataofCurrentRow = this.feeStructureProgramData;

        for (var i = 0; i < this.feeStructureProgramList.length; i++) {
            if (this.feeStructureProgramData['f048fidProgramme'] == this.feeStructureProgramList[i]['f048fidProgramme']) {
                alert("Cannot add same program again");
                return false;
            }
        }
        var confirmPop = confirm("Do you want to Add?");
        if (confirmPop == false) {
            return false;
        }
        this.feeStructureProgramData = {};
        this.feeStructureProgramList.push(dataofCurrentRow);
        this.showIcons1();
    }
    deletefeeStructureProgramList(object) {
        var cnf = confirm("Do you really want to delete");
        if (cnf == true) {
            var index = this.feeStructureProgramList.indexOf(object);
            this.deleteProgramList.push(this.feeStructureProgramList[index]['f048fidFeeProgram']);
            let deleteObject = {};
            deleteObject['id'] = this.deleteProgramList;
            this.FeestructureService.getProgramDeleteItems(deleteObject).subscribe(
                data => {
                }, error => {
                    console.log(error);
                });
            if (index > -1) {
                this.feeStructureProgramList.splice(index, 1);
                this.showIcons1();
            }
        }
    }

    addfeeStructureList() {
        if (this.feeStructureData['f048fidFeeCategory'] == undefined) {
            alert("Select Fee Category");
            return false;
        }
        if (this.feeStructureData['f048fidFeeItem'] == undefined) {
            alert("Select Fee Item");
            return false;
        }
        if (this.feeStructureData['f048fdrFund'] == undefined) {
            alert("Select Debit Fund Code");
            return false;
        }
        if (this.feeStructureData['f048fdrActivity'] == undefined) {
            alert("Select Debit Activity Code");
            return false;
        }
        if (this.feeStructureData['f048fdrDepartment'] == undefined) {
            alert("Select Debit Department Code");
            return false;
        }
        if (this.feeStructureData['f048fdrAccount'] == undefined) {
            alert("Select Debit Account Code");
            return false;
        }
        if (this.feeStructureData['f048fcrFund'] == undefined) {
            alert("Select Credit Fund Code");
            return false;
        }
        if (this.feeStructureData['f048fcrActivity'] == undefined) {
            alert("Select Credit Activity Code");
            return false;
        }
        if (this.feeStructureData['f048fcrDepartment'] == undefined) {
            alert("Select Credit Department Code");
            return false;
        }
        if (this.feeStructureData['f048fcrAccount'] == undefined) {
            alert("Select Credit Account Code");
            return false;
        }
        if (this.feeStructureData['f048fidCurrency'] == undefined) {
            alert("Select Currency");
            return false;
        }
        if (this.feeStructureData['f048fcalType'] == undefined) {
            alert("Enter Calculation Type");
            return false;
        }
        if (this.feeStructureData['f048ffrequencyMode'] == undefined) {
            alert("Enter Frequency");
            return false;
        }
        if (this.feeStructureData['f048famount'] == undefined) {
            alert("Enter Amount(RM)");
            return false;
        }
        var confirmPop = confirm("Do you want to Add?");
        if (confirmPop == false) {
            return false;
        }
        var dataofCurrentRow = this.feeStructureData;
        this.feeStructureData = {};

        this.feeStructureList.push(dataofCurrentRow);
        this.onBlurMethod1();

        this.showIcons();
    }

    deletefeeStructureList(object) {
        var cnf = confirm("Do you really want to delete");
        if (cnf == true) {
            var index = this.feeStructureList.indexOf(object);
            this.deleteFeestructureList.push(this.feeStructureList[index]['f048fidDetails']);
            let deleteObject = {};
            deleteObject['id'] = this.deleteFeestructureList;
            this.FeestructureService.getDeleteItems(deleteObject).subscribe(
                data => {
                }, error => {
                    console.log(error);
                });
            if (index > -1) {
                this.feeStructureList.splice(index, 1);
            }
            this.onBlurMethod1();
            this.showIcons();
        }
    }

    addfeeStructureProgramFeeList() {
        var dataofCurrentRow = this.feeStructureProgramFeeData;
        this.feeStructureProgramFeeData = {};
        this.feeStructureProgramFeeList.push(dataofCurrentRow);
        // this.onBlurMethod2();

    }
    deletefeeStructureProgramFeeList(object) {
        var cnf = confirm("Do you really want to delete");
        if (cnf == true) {
            var index = this.feeStructureProgramFeeList.indexOf(object);;
            if (index > -1) {
                this.feeStructureProgramFeeList.splice(index, 1);
            }
            // this.onBlurMethod2();
        }
    }



    onBlurMethod2() {

        var finaltotal = this.feeStructureDataHeader['f048ftotal'];
        for (var i = 0; i < this.feeStructureProgramFeeList.length; i++) {
            finaltotal = this.ConvertToFloat(finaltotal) + this.ConvertToFloat(this.feeStructureProgramFeeList[i]['f048famount']);
        }

        this.feeStructureDataHeader['f048ftotal'] = this.ConvertToFloat(finaltotal).toFixed(2);
    }

    onBlurMethod1() {

        var finaltotal = 0;
        for (var i = 0; i < this.feeStructureList.length; i++) {
            finaltotal = this.ConvertToFloat(finaltotal) + this.ConvertToFloat(this.feeStructureList[i]['f048famount']);
        }
        // finaltotal = this.ConvertToFloat(finaltotal) + this.ConvertToFloat(this.feeStructureData['f048famount']);

        this.feeStructureDataHeader['f048ftotal'] = this.ConvertToFloat(finaltotal).toFixed(2);
        console.log("after blur " + this.feeStructureDataHeader['f048ftotal']);
    }
    getglcode() {
        let itemId = this.feeStructureData['f048fidFeeItem'];
        console.log(itemId);
        this.ajaxCount++;
        this.FeestructureService.getCrDrByFee(itemId).subscribe(
            data => {
                this.ajaxCount--;
                this.itemList = data['result'][0];
                this.feeStructureData['f048fdrFund'] = data['result'][0]['f066fdr_fund'];
                this.feeStructureData['f048fdrActivity'] = data['result'][0]['f066fdr_activity'];
                this.feeStructureData['f048fdrDepartment'] = data['result'][0]['f066fdr_department'];
                this.feeStructureData['f048fdrAccount'] = data['result'][0]['f066fdr_account'];
                this.feeStructureData['f048fcrFund'] = data['result'][0]['f066fcr_fund'];
                this.feeStructureData['f048fcrActivity'] = data['result'][0]['f066fcr_activity'];
                this.feeStructureData['f048fcrDepartment'] = data['result'][0]['f066fcr_department'];
                this.feeStructureData['f048fcrAccount'] = data['result'][0]['f066fcr_account'];
            }
        );
    }
    getglcodeList() {
        for (var i = 0; i < this.feeStructureList.length; i++) {
            let itemId = this.feeStructureList[i]['f048fidFeeItem'];
            // this.ajaxCount++;
            this.FeestructureService.getCrDrByFee(itemId).subscribe(
                data => {
                    // this.ajaxCount--;
                    this.itemList = data['result'][0];
                });
            this.feeStructureData['f048fdrFund'] = this.itemList['f066fdr_fund'];
            this.feeStructureData['f048fdrActivity'] = this.itemList['f066fdr_activity'];
            this.feeStructureData['f048fdrDepartment'] = this.itemList['f066fdr_department'];
            this.feeStructureData['f048fdrAccount'] = this.itemList['f066fdr_account'];
            this.feeStructureData['f048fcrFund'] = this.itemList['f066fcr_fund'];
            this.feeStructureData['f048fcrActivity'] = this.itemList['f066fcr_activity'];
            this.feeStructureData['f048fcrDepartment'] = this.itemList['f066fcr_department'];
            this.feeStructureData['f048fcrAccount'] = this.itemList['f066fcr_account'];
            console.log(this.feeStructureList[i]);
            console.log(this.itemList);
            // this.itemList = [];
        }

    }
    saveFeeStructure() {
        if (this.showLastRow == false) {
            var addactivities = this.addfeeStructureList();
            if (addactivities == false) {
                return false;
            }

        }
        if (this.showLastRow == false) {
            var addactivities = this.addfeeStructureProgramList();
            if (addactivities == false) {
                return false;
            }

        }
        if (this.feeStructureDataHeader['f048fname'] == undefined) {
            alert("Enter Description");
            return false;
        }
        // if (this.feeStructureDataHeader['f048fcode'] == undefined) {
        //     alert("Enter Code");
        //     return false;
        // }
        if (this.feeStructureDataHeader['f048feffectiveDate'] == undefined) {
            alert("Enter Effective Date");
            return false;
        }
        if (this.feeStructureDataHeader['f048fgraduationType'] == undefined) {
            alert("Select Level of Study");
            return false;
        }
        if (this.feeStructureDataHeader['f048flearningCentre'] == undefined) {
            alert("Enter Learning Center");
            return false;
        }
        if (this.feeStructureDataHeader['f048fcitizen'] == undefined) {
            alert("Select Citizen");
            return false;
        }
        if (this.feeStructureDataHeader['f048fpartner'] == undefined) {
            alert("Enter  Partner/Collaboration");
            return false;
        }
        if (this.feeStructureDataHeader['f048fregion'] == undefined) {
            alert("Select Region");
            return false;
        }
        if (this.feeStructureDataHeader['f048fstudentCategory'] == undefined) {
            alert("Select Student Category");
            return false;
        }
        if (this.feeStructureDataHeader['f048fstudyMode'] == undefined) {
            alert("Select Mode Of Study");
            return false;
        }
        if (this.feeStructureDataHeader['f048ftotal'] == undefined) {
            alert("Enter Total Amount (RM)");
            return false;
        }

        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        console.log(this.feeStructureDataHeader);
        this.feeStructureDataHeader['f048fupdatedBy'] = 1;

        if (this.feeStructureProgramList.length > 0) {


        } else {
            alert("Cannot add same program again");
            return false;
        }
        // for( var i=0;i<this.feeStructureProgramList.length;i++) {
        //     if(this.feeStructureProgramData['f048fidProgramme']==this.feeStructureProgramList[i]['f048fidProgramme']) {
        //         alert("Cannot add same program again");
        //         return false;
        //     }
        // }


        let feeStructureObject = {};
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        console.log(this.id);
        if (this.id > 0) {
            feeStructureObject['f071fid'] = this.id;
        }
        feeStructureObject['f048fname'] = this.feeStructureDataHeader['f048fname'];

        feeStructureObject['f048fcode'] = this.feeStructureDataHeader['f048fcode'];
        feeStructureObject['f048feffectiveDate'] = this.feeStructureDataHeader['f048feffectiveDate'];
        feeStructureObject['f048fidIntake'] = this.feeStructureDataHeader['f048fidIntake'];
        feeStructureObject['f048fpartner'] = this.feeStructureDataHeader['f048fpartner'];
        feeStructureObject['f048fcitizen'] = this.feeStructureDataHeader['f048fcitizen'];
        feeStructureObject['f048fregion'] = this.feeStructureDataHeader['f048fregion'];
        feeStructureObject['f048ffeeType'] = this.feeStructureDataHeader['f048ffeeType'];
        feeStructureObject['f048fstudentCategory'] = this.feeStructureDataHeader['f048fstudentCategory'];
        feeStructureObject['f048flearningCentre'] = this.feeStructureDataHeader['f048flearningCentre'];
        feeStructureObject['f048fstudyMode'] = this.feeStructureDataHeader['f048fstudyMode'];
        feeStructureObject['f048fisProgramAttached'] = this.feeStructureDataHeader['f048fisProgramAttached'];
        feeStructureObject['f048ftotal'] = this.ConvertToFloat(this.feeStructureDataHeader['f048ftotal']).toFixed(2);
        feeStructureObject['f048fgraduationType'] = this.feeStructureDataHeader['f048fgraduationType'];
        feeStructureObject['f048fstatus'] = 0;
        feeStructureObject['fee-program'] = this.feeStructureProgramList;
        feeStructureObject['fee-details'] = this.feeStructureList;
        feeStructureObject['course-fee'] = this.feeStructureProgramFeeList;

        for (var i = 0; i < feeStructureObject['fee-details'].length; i++) {
            feeStructureObject['fee-details'][i]['f048famount'] = this.ConvertToFloat(feeStructureObject['fee-details'][i]['f048famount']).toFixed(2);
        }

        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.FeestructureService.updateFeeStructureItems(feeStructureObject, this.id).subscribe(
                data => {
                    this.router.navigate(['studentfinance/feestructure']);
                    alert(" Fee Structure has been Updated Sucessfully ! ");

                }, error => {
                    console.log(error);
                });

        } else {
            this.FeestructureService.insertFeeStructureItems(feeStructureObject).subscribe(
                data => {
                    this.router.navigate(['studentfinance/feestructure']);
                    alert("Fee Structure has been  Added Sucessfully ! ");


                }, error => {
                    console.log(error);
                });

        }

    }

    getViewData() {
        this.program = true;
        this.learningList = [];
        this.intakeList = [];
        this.programmeList = [];
        if (this.feeStructureDataHeader['f048fgraduationType'] == "UG") {
            //Intake drop down 

            this.ajaxCount++; 

            this.FeestructureService.getUgIntake().subscribe(
                data => {
                    this.ajaxCount--;
                    this.intakeList = data['result'];
                }, error => {
                    console.log(error);
                });

                this.ajaxCount++; 

            this.FeestructureService.getUgLearningCenters().subscribe(
                data => {
                    this.ajaxCount--;
                    this.learningList = data['result'];
                }, error => {
                    console.log(error);
                });

            //Programme drop down 
            this.ajaxCount++;

            this.FeestructureService.getUgPrograms().subscribe(
                data => {
                    this.ajaxCount--;
                    this.programmeList = data['result'];
                }, error => {
                    console.log(error);
                });

            // this.FeestructureService.getUgRegion().subscribe(
            //     data => {
            //         this.ajaxCount--;

            //         this.regionList = data['result'];
            //     }, error => {
            //         console.log(error);
            //     });
            // this.ajaxCount++;



        }
        else {
            this.program = true;

            //Intake drop down 

            // this.ajaxCount++;

            // this.FeestructureService.getPgSemester().subscribe(
            //     data => {
            //         this.ajaxCount--;
            //         this.intakeList = data['result'];
            //     }, error => {
            //         console.log(error);
            //     });
            // this.ajaxCount++;

            // this.FeestructureService.getPgRegion().subscribe(
            //     data => {
            //         this.ajaxCount--;

            //         this.regionList = data['result'];
            //     }, error => {
            //         console.log(error);
            //     });
            this.learningList = [];
            this.intakeList = [];
            this.programmeList = [];
            this.ajaxCount++; 

            this.FeestructureService.getPgLearningCenters().subscribe(
                data => {
                    this.ajaxCount--;
                    this.learningList = data['result'];
                }, error => {
                    console.log(error);
                });
            this.ajaxCount++;

            this.FeestructureService.getPgIntake().subscribe(
                data => {
                    this.ajaxCount--;
                    this.intakeList = data['result'];
                }, error => {
                    console.log(error);
                });

            //Programme drop down 
            this.ajaxCount++;

            this.FeestructureService.getPgProgram().subscribe(
                data => {
                    this.ajaxCount--;
                    this.programmeList = data['result'];
                }, error => {
                    console.log(error);
                });


        }
    }
    onlyNumberKey(event) {
        //console.log(event);
        if (event.charCode == 46) {

        } else {
            return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
        }
    }
    maximize() {
        $("#myModal").addClass("modal");
        $("#overlay1").show();
        $(".maximize-icon").addClass("hidden");
        $(".minimize-icon").removeClass("hidden");
        $(".modal-backdrop").show();

    }
    minimize() {
        $("#myModal").removeClass("modal");
        $("#overlay1").hide();
        $(".maximize-icon").removeClass("hidden");
        $(".minimize-icon").addClass("hidden");
        $(".modal-backdrop").hide();
        $('#myModal').show();

    }
}