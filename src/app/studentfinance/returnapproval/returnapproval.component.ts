import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { ReturnapprovalService } from '../service/returnapproval.service'
import { AlertService } from './../../_services/alert.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'Returnapproval',
    templateUrl: 'returnapproval.component.html'
})

export class ReturnapprovalComponent implements OnInit {
    rejectreturnList = [];
    returnapprovalList = [];
    returnapprovalData = {};
    selectAllCheckbox = true;

    constructor(

        private ReturnapprovalService: ReturnapprovalService,
        private AlertService: AlertService,
        private spinner: NgxSpinnerService,                                
    ) { }

    ngOnInit() {
        this.getListData();
    }
    getListData() {
        this.spinner.show();                                                
        this.ReturnapprovalService.getItemsDetail(0).subscribe(
            data => {
                this.spinner.hide();
                this.returnapprovalList = data['result'];
            }, error => {
                console.log(error);
            });
    }

    returnapprovalListData() {
        console.log(this.returnapprovalList);
        var approvaloneIds = [];
        for (var i = 0; i < this.returnapprovalList.length; i++) {
            if (this.returnapprovalList[i].f118fapprovalStatus == true) {
                approvaloneIds.push(this.returnapprovalList[i].f118fid);
            }
        }
        if (approvaloneIds.length < 1) {
            alert("Select atleast one Student to Approve");
            return false;
        }
        var confirmPop = confirm("Do you want to Approve?");
        if (confirmPop == false) {
            return false;
        }
        var approvaloneUpdate = {};
        approvaloneUpdate['id'] = approvaloneIds;
        approvaloneUpdate['status'] = 1;
        approvaloneUpdate['reason'] = '';
        console.log(approvaloneIds);
        this.ReturnapprovalService.updateReturanapprovalItems(approvaloneUpdate).subscribe(
            data => {
                this.getListData();
                this.returnapprovalData['reason'] = '';
                alert(" Approved Sucessfully ! ");
            }, error => {
                console.log(error);
            });
    }

    rejectreturnapprovalListData() {
        console.log(this.returnapprovalList);
        var rejectoneIds = [];
        for (var i = 0; i < this.returnapprovalList.length; i++) {
            if (this.returnapprovalList[i].f118fapprovalStatus == true) {
                rejectoneIds.push(this.returnapprovalList[i].f118fid);
            }
        }
        if (rejectoneIds.length < 1) {
            alert("Select atleast one Student to Reject");
            return false;
        }
        if (this.returnapprovalData['reason'] == undefined) {
            alert('Enter the Description');
            return false;
        }
        var confirmPop = confirm("Do you want to Reject?");
        if (confirmPop == false) {
            return false;
        }
        var returnrejectUpdate = {};
        returnrejectUpdate['id'] = rejectoneIds;
        returnrejectUpdate['status'] = 2;
        returnrejectUpdate['reason'] = this.returnapprovalData['reason'];
        
        this.ReturnapprovalService.rejectReturanapproval(returnrejectUpdate).subscribe(
            data => {
                this.getListData();
                alert("Rejected Successfully");
                this.returnapprovalData['reason'] = '';
            }, error => {
                console.log(error);
            });

    }
}
