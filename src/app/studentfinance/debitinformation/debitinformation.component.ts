import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DebitinformationService } from '../service/debitinformation.service'
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'Debitinformation',
    templateUrl: 'debitinformation.component.html'
})
export class DebitinformationComponent implements OnInit {
    debitinfoList = [];
    debitinfoData = {};
    id: number;
    constructor(
        private DebitinformationService: DebitinformationService,
        private spinner: NgxSpinnerService,
    ) { }
    ngOnInit() {
        this.spinner.show();
        this.DebitinformationService.getItems().subscribe(
            data => {
                this.spinner.hide();
                this.debitinfoList = data['result']['data'];
            }, error => {
                console.log(error);
            });
    }
    addDebitinformation() {
        console.log(this.debitinfoData);
        this.DebitinformationService.insertDebitinformationItems(this.debitinfoData).subscribe(
            data => {
                this.debitinfoList = data['todos'];
            }, error => {
                console.log(error);
            });

    }
}