import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { DebitinformationService } from '../../service/debitinformation.service'
import { CountryService } from '../../../generalsetup/service/country.service'
import { StateService } from '../../../generalsetup/service/state.service'
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'DebitinformationFormComponent',
    templateUrl: 'debitinformationform.component.html'
})
export class DebitinformationFormComponent implements OnInit {
    debitinfoform: FormGroup;
    debitinfoList = [];
    debitinfoData = {};
    countryList = [];
    stateList = [];
    debtinfoDataheader = {};
    deleteList = [];
    id: number;
    ajaxCount: number;
    showLastRow: boolean;
    showLastRowMinus: boolean;
    showLastRowPlus: boolean;
    listshowLastRowMinus: boolean;
    listshowLastRowPlus: boolean;
    constructor(
        private DebitinformationService: DebitinformationService,
        private countryService: CountryService,
        private StateService: StateService,
        private spinner: NgxSpinnerService,
        private route: ActivatedRoute,
        private router: Router
    ) { }

    ngDoCheck() {
        const change = this.ajaxCount;
        // console.log(this.ajaxCount);
        if (this.ajaxCount == 0 && this.debitinfoList.length > 0) {
            this.ajaxCount = 10;
            console.log("asdfsdf");
            this.showIcons();
            // this.spinner.hide()
        }
        if (this.ajaxCount == 0) {
            this.spinner.hide();
        }
    }
    showIcons() {

        if (this.debitinfoList.length > 0) {
            this.showLastRowMinus = true;
            this.showLastRowPlus = true;
            this.listshowLastRowMinus = true;
        }

        if (this.debitinfoList.length > 1) {
            if (this.showLastRow == false) {
                this.listshowLastRowPlus = false;
                this.listshowLastRowMinus = true;
                this.showLastRowPlus = true;

            } else {
                this.listshowLastRowPlus = true;
                this.listshowLastRowMinus = false;

            }
        }
        if (this.debitinfoList.length == 1 && this.showLastRow == false) {
            this.listshowLastRowMinus = true;
            this.listshowLastRowPlus = false;

        }
        if (this.debitinfoList.length == 1 && this.showLastRow == true) {
            this.listshowLastRowMinus = false;
            this.listshowLastRowPlus = true;

        }
        if (this.debitinfoList.length == 0 && this.showLastRow == false) {
            this.showLastRowMinus = false;
            this.showLastRowPlus = true;
        }
        if (this.debitinfoList.length == 0 && this.showLastRow == true) {
            this.showLastRowMinus = true;
            this.showLastRowPlus = false;
        }
        console.log(this.debitinfoList.length);
        console.log("Show last row" + this.showLastRow);
        console.log("showLastRowMinus" + this.showLastRowMinus);
        console.log("showLastRowPlus" + this.showLastRowPlus);
        console.log("listshowLastRowMinus" + this.listshowLastRowMinus);
        console.log("listshowLastRowPlus" + this.listshowLastRowPlus);


    }
    deletemodule() {
        this.showLastRow = true;
        this.showIcons();
    }
    showNewRow() {
        this.showLastRow = false;
        this.showIcons();
    }

    ngOnInit() {
        this.ajaxCount = 0;
        // this.spinner.show();
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id < 1) {
            this.showLastRow = false;
            this.showLastRowMinus = false;
            this.showLastRowPlus = true;
        } else {
            this.showLastRow = true;

        }
        this.ajaxCount++;
        this.countryService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.countryList = data['result']['data'];
            }, error => {
                console.log(error);
            });
        // console.log(this.id);
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.ajaxCount++;
            this.DebitinformationService.getItemsDetail(this.id).subscribe(
                data => {
                    this.ajaxCount--;
                    this.debtinfoDataheader = data['result']['data'][0];
                    this.debtinfoDataheader['f098fcompanyName'] = data['result']['data'][0]['f098fcompanyName'];
                    this.debtinfoDataheader['f098fcontactOfficer'] = data['result']['data'][0]['f098fcontactOfficer'];
                    this.debtinfoDataheader['f098fphone'] = data['result']['data'][0]['f098fphone'];
                    this.debtinfoDataheader['f098femail'] = data['result']['data'][0]['f098femail'];
                    this.debtinfoDataheader['f098ffax'] = data['result']['data'][0]['f098ffax'];
                    this.debtinfoDataheader['f098faddress'] = data['result']['data'][0]['f098faddress'];
                    this.debtinfoDataheader['f098fcountry'] = data['result']['data'][0]['f098fcountry'];
                    this.debtinfoDataheader['f098fstate'] = data['result']['data'][0]['f098fstate'].toString();
                    this.debtinfoDataheader['f098fcity'] = data['result']['data'][0]['f098fcity'];

                    this.debitinfoList = data['result']['data'];
                    this.onCountryChange();
                    console.log(this.debtinfoDataheader);
                    this.showIcons();
                }, error => {
                    console.log(error);
                });
        }
    }
    onCountryChange() {

        if (this.debtinfoDataheader['f098fcountry'] == undefined) {
            this.debtinfoDataheader['f098fcountry'] = 0;
        }
        this.ajaxCount++;
        this.StateService.getStates(this.debtinfoDataheader['f098fcountry']).subscribe(
            data => {
                this.ajaxCount--;
                this.stateList = data['result'];
                let other = {
                    'f012fid': "1",
                    "f012fshortName": "SH",
                    "f012fstateName": "Others"

                };
                this.stateList.push(other);

            }
        );
    }
    dateComparison() {

        let startDate = Date.parse(this.debitinfoData['f098fstartDate']);
        let endDate = Date.parse(this.debitinfoData['f098fendDate']);
        if (startDate > endDate) {
            alert("Start Date cannot be greater than End Date !");
            this.debitinfoData['f098fendDate'] = "";
        }

    }
    dateComparisonList() {
        for (var i = 0; i < this.debitinfoList.length; i++) {

            let startDate = Date.parse(this.debitinfoList[i]['f098fstartDate']);
            let endDate = Date.parse(this.debitinfoList[i]['f098fendDate']);
            if (startDate > endDate) {
                alert("Start Date cannot be greater than End Date !");
                this.debitinfoList[i]['f098fendDate'] = "";
            }
        }

    }
    saveDebtinfo() {

        if (this.debtinfoDataheader['f098fcompanyName'] == undefined) {
            alert("Enter Company");
            return false;
        }
        if (this.debtinfoDataheader['f098fcontactOfficer'] == undefined) {
            alert("Enter Contact Officer");
            return false;
        }
        if (this.debtinfoDataheader['f098fphone'] == undefined) {
            alert("Enter Phone Number");
            return false;
        }

        if (this.debtinfoDataheader['f098femail'] == undefined) {
            alert("Enter Email");
            return false;
        }
        // if (this.debtinfoDataheader['f098femail'] == "[^@\s]+@[^@\s]+\.[^@\s]+") {
        //     alert("Enter Email");
        //     return false;
        // }

        if (this.debtinfoDataheader['f098ffax'] == undefined) {
            alert("Enter Fax Number");
            return false;
        }
        if (this.debtinfoDataheader['f098faddress'] == undefined) {
            alert("Enter Address");
            return false;
        }
        if (this.debtinfoDataheader['f098fcountry'] == undefined) {
            alert("Select Country");
            return false;
        }
        if (this.debtinfoDataheader['f098fstate'] == undefined) {
            alert("Select State");
            return false;
        }
        if (this.debtinfoDataheader['f098fcity'] == undefined) {
            alert("Select City");
            return false;
        }
        if (this.showLastRow == false) {
            var addactivities = this.addDebitinfolist();
            if (addactivities = false) {
                return false;
            }
        }
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        let invoiceObject = {};
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        console.log(this.id);

        invoiceObject['f098fcompanyName'] = this.debtinfoDataheader['f098fcompanyName'];
        invoiceObject['f098fcontactOfficer'] = this.debtinfoDataheader['f098fcontactOfficer'];
        invoiceObject['f098fphone'] = this.debtinfoDataheader['f098fphone'];
        invoiceObject['f098femail'] = this.debtinfoDataheader['f098femail'];
        invoiceObject['f098ffax'] = this.debtinfoDataheader['f098ffax'];
        invoiceObject['f098faddress'] = this.debtinfoDataheader['f098faddress'];
        invoiceObject['f098fcity'] = this.debtinfoDataheader['f098fcity'];
        invoiceObject['f098fstate'] = this.debtinfoDataheader['f098fstate'];
        invoiceObject['f098fcountry'] = this.debtinfoDataheader['f098fcountry'];
        invoiceObject['f098fstatus'] = 0;
        if (this.id > 0) {
            invoiceObject['f098fidDetails'] = this.id;
        }
        invoiceObject['debt-details'] = this.debitinfoList;

        if (this.id > 0) {
            this.DebitinformationService.updateDebitinformationItems(invoiceObject, this.id).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert('Company Name Already Exist');
                    }
                    else {
                        this.router.navigate(['studentfinance/debitinformation']);
                        alert(" Debt Information has been Updated Sucessfully ! ");
                    }

                }, error => {
                    console.log(error);
                    alert('Company Name Already Exist');
                });
        } else {
            this.DebitinformationService.insertDebitinformationItems(invoiceObject).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert('Company Name Already Exist');
                    }
                    else {
                        this.router.navigate(['studentfinance/debitinformation']);
                        alert(" Debt Information has been Added Sucessfully ! ");
                    }

                }, error => {
                    console.log(error);
                });
        }
    }

    deleteDebitinfo(object) {
        var cnf = confirm("Do you really want to delete");
        if (cnf == true) {
            var index = this.debitinfoList.indexOf(object);
            this.deleteList.push(this.debitinfoList[index]['f098fidDetails']);
            let deleteObject = {};
            deleteObject['id'] = this.deleteList;
            this.DebitinformationService.getDeleteItems(deleteObject).subscribe(
                data => {
                }, error => {
                    console.log(error);
                })
            if (index > -1) {
                this.debitinfoList.splice(index, 1);
            }
            this.showIcons();
        }
    }
    addDebitinfolist() {

        if (this.debitinfoData['f098fstartDate'] == undefined) {
            alert(" Select Start Date");
            return false;
        }
        if (this.debitinfoData['f098fendDate'] == undefined) {
            alert(" Select End Date");
            return false;
        }
        if (this.debitinfoData['f098fdescription'] == undefined) {
            alert(" Enter the Description");
            return false;
        }
        var confirmPop = confirm("Do you want to Add?");
        if (confirmPop == false) {
            return false;
        }
        console.log(this.debitinfoData);
        var dataofCurrentRow = this.debitinfoData;
        this.debitinfoData = {};
        this.debitinfoList.push(dataofCurrentRow);
        console.log(this.debitinfoList);
        this.showIcons();
    }
    maximize() {
        $("#myModal").addClass("modal");
        $("#overlay1").show();
        $(".maximize-icon").addClass("hidden");
        $(".minimize-icon").removeClass("hidden");
        $(".modal-backdrop").show();

    }
    minimize() {
        $("#myModal").removeClass("modal");
        $("#overlay1").hide();
        $(".maximize-icon").removeClass("hidden");
        $(".minimize-icon").addClass("hidden");
        $(".modal-backdrop").hide();
        $('#myModal').show();

    }
}
