import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MaininvoiceService } from '../../../accountsrecivable/service/maininvoice.service';
import { AlertService } from '../../../_services/alert.service';
import { StudentService } from '../../../studentfinance/service/student.service';
import { SponsorService } from '../../../studentfinance/service/sponsor.service';
import { SponserbillService } from '../../../studentfinance/service/sponserbill.service';
import { isEmpty } from 'rxjs/operators';
import { NgxSpinnerService } from 'ngx-spinner';
@Component({
    selector: 'SponserbillformComponent',
    templateUrl: 'sponserbillform.component.html'
})

export class SponserbillformComponent implements OnInit {
    sponserbillList = [];
    sponserbillData = {};
    sponserbillDataheader = {};
    studentList = [];
    sponserList = [];
    invoiceList = [];
    sponserStudentList = [];
    id: number;
    valueDate: string;
    ajaxCount: number;
    Amount: number;
    totalAmount: number;
    type: string;
    spId: number;
    editDisabled: boolean;
    constructor(

        private MaininvoiceService: MaininvoiceService,
        private StudentService: StudentService,
        private SponserbillService: SponserbillService,
        private alertService: AlertService,
        private SponsorService: SponsorService,
        private spinner: NgxSpinnerService,
        private route: ActivatedRoute,
        private router: Router

    ) { }


    ngDoCheck() {
        const change = this.ajaxCount;
        console.log(this.ajaxCount);
        if (this.ajaxCount == 0) {
            this.editFunction();
            this.spinner.hide();
            this.ajaxCount = 10;
        }
    }

    editFunction() {
        this.ajaxCount++;
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.SponserbillService.getInvoiceDetailsById(this.id).subscribe(
                data => {
                    this.ajaxCount--;
                    this.sponserbillList = data['result'];
                    this.sponserbillDataheader['f063fbillNo'] = data['result'][0].f063fbillNo;
                    this.sponserbillDataheader['f063fidSponsor'] = data['result'][0].f063fidSponsor;
                    this.sponserbillDataheader['f063famount'] = data['result'][0].f063famount;
                    this.sponserbillDataheader['f063fdate'] = data['result'][0].f063fdate;

                    console.log(this.sponserbillList);
                    this.editDisabled =true;
                }, error => {
                    console.log(error);
                });
        }
    }

    generateBillNumber() {
        let typeObject = {};
        typeObject['type'] = "SPBILL";
        this.SponserbillService.getBillNumber(typeObject).subscribe(
            data => {
                console.log(data);
                this.sponserbillDataheader['f063fbillNo'] = data['number'];
                this.valueDate = new Date().toISOString().substr(0, 10);
                console.log(this.valueDate);
                this.sponserbillDataheader['f063fdate'] = this.valueDate;
            }
        );
    }

    ngOnInit() {
        this.editDisabled =false;
        this.ajaxCount = 0
        this.spinner.show();
        this.generateBillNumber();
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        this.ajaxCount = 0;
        //Student dropdown
        this.ajaxCount++;
        this.StudentService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                console.log(this.ajaxCount);

                this.studentList = data['result']['data'];
            }
        );

        //Sponser dropdown
        this.ajaxCount++;
        this.SponsorService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                console.log(this.ajaxCount);

                this.sponserList = data['result']['data'];
            }
        );
        console.log(this.ajaxCount);

        this.ajaxCount++;
        this.MaininvoiceService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.invoiceList = data['result']['data'];
            }
        );
    }



    ConvertToFloat(val) {
        return parseFloat(val);
    }
    getList() {

        this.spId = this.sponserbillDataheader['f063fidSponsor'];
        if (this.spId == undefined) {
            alert("Select Sponser");
        }
        else {
            this.SponserbillService.getListInvoices(this.spId).subscribe(
                data => {
                    this.sponserbillList = data['result']['data'];
                    let amount = 0;
                    for (var i = 0; i < this.sponserbillList.length; i++) {
                        this.sponserbillList[i]['f064fidInvoice'] = this.sponserbillList[i]['f071fid'];
                        this.sponserbillList[i]['f064famount'] = this.sponserbillList[i]['f071finvoiceTotal'];;
                        amount = amount + parseFloat(this.sponserbillList[i]['f064famount']);
                    }
                    //this.onBlurMethod();
                    this.sponserbillDataheader['f063famount'] = amount;
                }
            );
        }


    }

    saveSponserBill() {

        let sponserBillObject = {};
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        console.log(this.id);
        var confirmPop = confirm("Do you want to Generate Sponsor Bill?");
        if (confirmPop == false) {
            return false;
        }
        if(this.sponserbillDataheader['f063fidSponsor']==undefined){
            alert("Select Sponsor");
            return false;
        }
        if (this.id > 0) {
            sponserBillObject['f071fid'] = this.id;
        }
        sponserBillObject['f063fidSponsor'] = this.sponserbillDataheader['f063fidSponsor'];
        sponserBillObject['f063fdate'] = this.sponserbillDataheader['f063fdate'];
        sponserBillObject['f063famount'] = this.sponserbillDataheader['f063famount'];
        sponserBillObject['f063fbillNo'] = this.sponserbillDataheader['f063fbillNo'];
        sponserBillObject['f063fstatus'] = 0;
        sponserBillObject['details'] = this.sponserbillList;

        if (this.id > 0) {
            this.SponserbillService.updatesponsorBillItems(sponserBillObject, this.id).subscribe(
                data => {
                    this.router.navigate(['studentfinance/sponserbill']);
                    alert(" Updated Sucessfully ! ");
                }, error => {
                    console.log(error);
                });
        } else {
            this.SponserbillService.insertsponsorBillItems(sponserBillObject).subscribe(
                data => {
                    this.router.navigate(['studentfinance/sponserbill']);
                    alert(" Generated Sucessfully ! ");
                }, error => {
                    console.log(error);
                });
        }
    }

    onBlurMethod() {
        var finaltotal = 0;
        for (var i = 0; i < this.sponserbillList.length; i++) {
            finaltotal = this.ConvertToFloat(finaltotal + this.ConvertToFloat(this.sponserbillList[i]['f064famount']));
            console.log("amt" + this.sponserbillList[i]['f064famount']);
            console.log("final" + finaltotal);
        }
        this.sponserbillDataheader['f063famount'] = (this.ConvertToFloat(finaltotal)).toFixed(2);

    }

    deleteSponserBill(object) {
        var cnf = confirm("Do you really want to delete");
        if (cnf == true) {
            var index = this.sponserbillList.indexOf(object);;
            if (index > -1) {
                this.sponserbillList.splice(index, 1);
            }
            this.onBlurMethod();
        }
    }

    addSponserBillList() {

        var dataofCurrentRow = this.sponserbillData;
        this.sponserbillData = {};
        this.sponserbillList.push(dataofCurrentRow);
        console.log(this.sponserbillList);
        this.onBlurMethod();
    }
}