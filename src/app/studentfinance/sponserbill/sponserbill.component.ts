import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Injector } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SponserbillService } from '../service/sponserbill.service';
import { NgxSpinnerService } from 'ngx-spinner';
@Component({
    selector: 'SponserbillComponent',
    templateUrl: 'sponserbill.component.html'
})

export class SponserbillComponent implements OnInit {
    sponserbillList = [];
    id: number;
    selectAllCheckbox: false;
    type: string;
    title = '';

    constructor(

        private route: ActivatedRoute,
        private SponserbillService: SponserbillService,
        private router: Router,
        private spinner: NgxSpinnerService,

    ) { }
    ngOnInit() {
        this.getList();
    }
    getList() {
        this.spinner.show();
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        this.SponserbillService.getItems().subscribe(
            data => {
                this.spinner.hide();
                this.sponserbillList = data['result']['data'];

            }, error => {
                console.log(error);
            });
    }



}