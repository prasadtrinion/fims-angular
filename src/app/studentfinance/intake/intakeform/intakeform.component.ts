import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { IntakeService } from '../../service/intake.service'
import { ProgrammeService } from '../../service/programme.service'
import { DefinationmsService } from '../../service/definationms.service'
import { AlertService } from '../../../_services/alert.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { FeestructureService } from '../../service/feestructure.service'

@Component({
    selector: 'IntakeFormComponent',
    templateUrl: 'intakeform.component.html'
})

export class IntakeFormComponent implements OnInit {
    intakeform: FormGroup;

    intakeList = [];
    intakeData = {};
    programmeList = [];
    programmeData = {};
    categoryList = [];
    semsequenceList = [];
    definationData = {};
    intakeDataheader = {};
    typeList = [];
    deleteList = [];
    ajaxCount: number;
    id: number;
    showLastRow: boolean;
    showLastRowMinus: boolean;
    showLastRowPlus: boolean;
    listshowLastRowMinus: boolean;
    listshowLastRowPlus: boolean;
    constructor(
        private IntakeService: IntakeService,
        private ProgrammeService: ProgrammeService,
        private DefinationmsService: DefinationmsService,
        private FeestructureService: FeestructureService,
        private spinner: NgxSpinnerService,
        private AlertService: AlertService,
        private route: ActivatedRoute,
        private router: Router

    ) { }

    ngDoCheck() {
        const change = this.ajaxCount;
        console.log(this.ajaxCount);
        if (this.ajaxCount == 0) {
            this.editFunction();
            this.spinner.hide();
            this.ajaxCount = 10;
        }
    }

    editFunction() {
        this.ajaxCount++;
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {

            this.IntakeService.getIntakeDetailsById(this.id).subscribe(
                data => {
                    this.ajaxCount--;
                    this.intakeList = data['result'];
                    this.intakeDataheader['f067fdescription'] = data['result'][0].f067fdescription;
                    this.intakeDataheader['f067fname'] = data['result'][0].f067fname;
                    this.intakeDataheader['f067fsemSequence'] = data['result'][0].f067fsemSequence;
                    this.intakeDataheader['f067ftype'] = data['result'][0]['f067ftype'];
                    console.log(this.intakeList);
                    this.showIcons();
                }, error => {
                    console.log(error);
                });
        }
    }
    showIcons() {
        // this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.intakeList.length > 0) {
            this.showLastRowMinus = true;
            this.showLastRowPlus = true;
            this.listshowLastRowMinus = true;
        }

        if (this.intakeList.length > 1) {
            if (this.showLastRow == false) {
                this.listshowLastRowPlus = false;
                this.listshowLastRowMinus = true;
                this.showLastRowPlus = true;

            } else {
                this.listshowLastRowPlus = true;
                this.listshowLastRowMinus = false;

            }
        }
        if (this.intakeList.length == 1 && this.showLastRow == false) {
            this.listshowLastRowMinus = true;
            this.listshowLastRowPlus = false;

        }
        if (this.intakeList.length == 1 && this.showLastRow == true) {
            this.listshowLastRowMinus = false;
            this.listshowLastRowPlus = true;

        }
        if (this.intakeList.length == 0 && this.showLastRow == false) {
            this.showLastRowMinus = false;
            this.showLastRowPlus = true;
        }
        if (this.intakeList.length == 0 && this.showLastRow == true) {
            this.showLastRowMinus = true;
            this.showLastRowPlus = false;
        }
        console.log(this.intakeList.length);
        console.log("Show last row" + this.showLastRow);
        console.log("showLastRowMinus" + this.showLastRowMinus);
        console.log("showLastRowPlus" + this.showLastRowPlus);
        console.log("listshowLastRowMinus" + this.listshowLastRowMinus);
        console.log("listshowLastRowPlus" + this.listshowLastRowPlus);

        // if(this.viewDisabled==true || this.purchaseorderDataheader['f034fapprovalStatus']==1 || this.purchaseorderDataheader['f034fapprovalStatus'] == 2) {
        //     this.listshowLastRowPlus = false;
        // }

    }
    deletemodule() {
        this.showLastRow = true;
        this.showIcons();
    }
    showNewRow() {
        var confirmPop = confirm("Do you want to Add?");
        if (confirmPop == false) {
            return false;
        }
        this.showLastRow = false;
        this.showIcons();
    }
    ngOnInit() {
        this.ajaxCount = 0
        this.spinner.show();
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id < 1) {
            this.showLastRow = false;
            this.showLastRowMinus = false;
            this.showLastRowPlus = true;
        } else {
            this.showLastRow = true;

        }
        let typeObj = {};
        typeObj['name'] = 'UG';
        this.typeList.push(typeObj);
        typeObj = {};
        typeObj['name'] = 'PG';
        this.typeList.push(typeObj);

        this.ajaxCount = 0;
        //Programme dropdown
        this.ajaxCount++;
        this.ProgrammeService.getActiveItems().subscribe(
            data => {
                this.ajaxCount--;
                this.programmeList = data['result']['data'];
            }, error => {
                console.log(error);
            });
        console.log(this.id);

        this.ajaxCount = 0;
        //sem seq dropdown
        this.ajaxCount++;
        this.DefinationmsService.getSemesterSequence('SemesterSequence').subscribe(
            data => {
                this.ajaxCount--;
                this.semsequenceList = data['result'];
            }, error => {
                console.log(error);
            });


        this.ajaxCount = 0;
        //category dropdown
        this.ajaxCount++;
        this.DefinationmsService.getCategory('Category').subscribe(
            data => {
                this.ajaxCount--;
                this.categoryList = data['result'];
            }, error => {
                console.log(error);
            });
    }
    dateComparison() {
        let fromDate = Date.parse(this.intakeData['f067fstartDate']);
        let toDate = Date.parse(this.intakeData['f067fendDate']);
        if (fromDate > toDate) {
            alert("Start Date cannot be Less than End Date!");
            this.intakeData['f067fendDate'] = "";
        }
    }
    dateComparison1() {
        for (var i = 0; i < this.intakeList.length; i++) {
            let fromDate = Date.parse(this.intakeList[i]['f067fstartDate']);
            let toDate = Date.parse(this.intakeList[i]['f067fendDate']);
            if (fromDate > toDate) {
                alert("Start Date cannot be Less than End Date!");
                this.intakeList[i]['f067fendDate'] = "";
            }
        }

    }

    saveIntake() {
        if (this.intakeDataheader['f067fname'] == undefined) {
            alert("Enter the Name");
            return false;
        }
        if (this.intakeDataheader['f067fdescription'] == undefined) {
            alert("Enter the Description");
            return false;
        }
        if (this.intakeDataheader['f067fsemSequence'] == undefined) {
            alert("Select Semester Sequence");
            return false;
        }
        if (this.intakeDataheader['f067ftype'] == undefined) {
            alert("Select Type");
            return false;
        }
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        if (this.showLastRow == false) {
            var addactivities = this.addIntakelist();
            if (addactivities == false) {
                return false;
            }

        }
        let invoiceObject = {};
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        console.log(this.id);
        if (this.id > 0) {
            invoiceObject['f067fid'] = this.id;
        }
        invoiceObject['f067fname'] = this.intakeDataheader['f067fname'];
        invoiceObject['f067fdescription'] = this.intakeDataheader['f067fdescription'];
        invoiceObject['f067fsemSequence'] = this.intakeDataheader['f067fsemSequence'];
        invoiceObject['f067ftype'] = this.intakeDataheader['f067ftype'];
        invoiceObject['f067fstatus'] = 0;
        invoiceObject['intake-details'] = this.intakeList;


        if (this.id > 0) {
            this.IntakeService.updateintakeItems(invoiceObject, this.id).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert('Name Already Exist');
                    }
                    else {
                        this.router.navigate(['studentfinance/intake']);
                        alert("Intake has been Updated Sucessfully !");
                    }
                }, error => {
                    console.log(error);
                    alert('Name Already Exist');
                });
        } else {
            this.IntakeService.insertintakeItems(invoiceObject).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert('Name Already Exist');
                    }
                    else {
                        this.router.navigate(['studentfinance/intake']);
                        alert("Intake has been Added Sucessfully !");
                    }

                }, error => {
                    console.log(error);
                });
        }
    }
    getViewData() {

        if (this.intakeDataheader['f067ftype'] == "UG") {
            //Programme drop down 

            this.FeestructureService.getUgPrograms().subscribe(
                data => {
                    this.programmeList = data['result'];
                }, error => {
                    console.log(error);
                });

        }
        else {
            this.FeestructureService.getPgProgram().subscribe(
                data => {
                    this.programmeList = data['result'];
                }, error => {
                    console.log(error);
                });

        }
    }
    deleteIntake(object) {
        var cnf = confirm("Do you really want to delete");
        if (cnf == true) {
            var index = this.intakeList.indexOf(object);;
            this.deleteList.push(this.intakeList[index]['f067fidDetails']);
            let deleteObject = {};
            deleteObject['id'] = this.deleteList;
            this.IntakeService.getDeleteItem(deleteObject).subscribe(
                data => {
                }, error => {
                    console.log(error);
                })
            if (index > -1) {
                this.intakeList.splice(index, 1);
            }
            this.showIcons();
        }
    }
    addIntakelist() {
        if (this.intakeData['f067fidProgramme'] == undefined) {
            alert("Select Programme");
            return false;
        }
        if (this.intakeData['f067fstartDate'] == undefined) {
            alert("Select Start Date");
            return false;
        }
        if (this.intakeData['f067fendDate'] == undefined) {
            alert("Select End Date");
            return false;
        }
        if (this.intakeData['f067fidCategory'] == undefined) {
            alert("Select Category");
            return false;
        }
        var confirmPop = confirm("Do you want to Add?");
        if (confirmPop == false) {
            return false;
        }
        var dataofCurrentRow = this.intakeData;
        this.intakeData = {};
        this.intakeList.push(dataofCurrentRow);
        this.showIcons();
    }
}