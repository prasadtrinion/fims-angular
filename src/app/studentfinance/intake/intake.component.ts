import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { IntakeService } from '../service/intake.service'
import { NgxSpinnerService } from 'ngx-spinner';


@Component({
    selector: 'Intake',
    templateUrl: 'intake.component.html'
})

export class IntakeComponent implements OnInit {
    intakeList =  [];
    intakeData = {};
    id: number;

    constructor(
        private IntakeService: IntakeService,
        private spinner: NgxSpinnerService,                                

    ) { }

    ngOnInit() { 
        this.spinner.show();                                                        
        this.IntakeService.getItems().subscribe(
            data => {
                this.spinner.hide();
                this.intakeList = data['result']['data'];
        }, error => {
            console.log(error);
        });
    }

    addIntake(){
           console.log(this.intakeData);
        this.IntakeService.insertintakeItems(this.intakeData).subscribe(
            data => {
                this.intakeList = data['todos'];
            }, error => {
                console.log(error);
            });

    }
}