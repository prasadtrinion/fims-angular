import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CurrencyratesetupService } from '../service/currencyratesetup.service'
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'Currencyratesetup',
    templateUrl: 'currencyratesetup.component.html'
})

export class CurrencyratesetupComponent implements OnInit {
    currencyratesetupList =  [];
    currencyratesetupData = {};
    id: number;

    constructor(
        
        private CurrencyratesetupService: CurrencyratesetupService,
        private spinner: NgxSpinnerService,        

    ) { }

    ngOnInit() { 
        this.spinner.show();                
        this.CurrencyratesetupService.getItems().subscribe(
            data => {
                this.spinner.hide();
                this.currencyratesetupList = data['result']['data'];
        }, error => {
            console.log(error);
        });
    }
}