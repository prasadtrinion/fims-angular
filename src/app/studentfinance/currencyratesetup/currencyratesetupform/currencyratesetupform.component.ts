import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { CurrencyratesetupService } from '../../service/currencyratesetup.service'
import { CurrencysetupService } from '../../service/currencysetup.service'
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'CurrencyratesetupFormComponent',
    templateUrl: 'currencyratesetupform.component.html'
})

export class CurrencyratesetupFormComponent implements OnInit {
    currencyratesetupList = [];
    currencyratesetupData = {};
    currencysetupList = [];
    currencysetupData = {};
    ajaxCount: number;
    id: number;
    constructor(
        private CurrencyratesetupService: CurrencyratesetupService,
        private CurrencysetupService: CurrencysetupService,
        private spinner: NgxSpinnerService,
        private route: ActivatedRoute,
        private router: Router
    ) { }

    ngDoCheck() {
        const change = this.ajaxCount;
        if (this.ajaxCount == 0) {
            this.spinner.hide();
        }
    }
    ngOnInit() {
        this.ajaxCount = 0
        this.spinner.show();
        this.currencyratesetupData['f064fstatus'] = 1;
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        // currency  dropdown
        this.ajaxCount++;
        this.CurrencysetupService.getCurrenyActiveItems().subscribe(
            data => {
                this.ajaxCount--;
                this.currencysetupList = data['result']['data'];
            }, error => {
                console.log(error);
            });
        if (this.id > 0) {
            this.ajaxCount++;
            this.CurrencyratesetupService.getItemsDetail(this.id).subscribe(
                data => {
                    this.ajaxCount--;
                    this.currencyratesetupData = data['result'][0];
                    this.currencyratesetupData['f064fstatus'] = parseInt(data['result'][0]['f064fstatus']);

                    console.log(this.currencyratesetupData);
                }, error => {
                    console.log(error);
                });
        }
    }
    ConvertToFloat(val){
        return parseFloat(val);
    }
    addCurrencyratesetup() {
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        console.log(this.currencyratesetupData);
        this.currencyratesetupData['f064fexchangeRate'] = this.ConvertToFloat(this.currencyratesetupData['f064fexchangeRate']).toFixed(2);
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.CurrencyratesetupService.updatecurrencyratesetupItems(this.currencyratesetupData, this.id).subscribe(
                data => {
                    this.router.navigate(['studentfinance/currencyratesetup']);
                    alert("Currency Rate Setup has been Updated Sucessfully !!");

                }, error => {
                    console.log(error);
                });
        } else {
            this.CurrencyratesetupService.insertcurrencyratesetupItems(this.currencyratesetupData).subscribe(
                data => {
                    this.router.navigate(['studentfinance/currencyratesetup']);
                    alert("Currency Rate Setup has been Added Sucessfully !!");

                }, error => {
                    console.log(error);
                });
        }
    }
    onlyNumberKey(event) {
        if (event.charCode == 46) {
        } else {
            return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
        }
    }
}