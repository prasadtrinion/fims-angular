import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CreditcardterminalService } from '../service/creditcardterminal.service'
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'Creditcardterminal',
    templateUrl: 'creditcardterminal.component.html'
})

export class CreditcardterminalComponent implements OnInit {
    creditterminalList =  [];
    creditterminalData = {};
    id: number;

    constructor(
        
        private CreditcardterminalService: CreditcardterminalService,
        private spinner: NgxSpinnerService,        

    ) { }
    ngOnInit() { 
        this.spinner.show();                        
        this.CreditcardterminalService.getItems().subscribe(
            data => {
                this.spinner.hide();
                this.creditterminalList = data['result']['data'];
        }, error => {
            console.log(error);
        });
    }
}