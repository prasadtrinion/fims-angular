import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { CreditcardterminalService } from '../../service/creditcardterminal.service';
import { DepartmentService } from '../../../generalsetup/service/department.service';
import { BankService } from '../../../generalsetup/service/bank.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'CreditcardterminalFormComponent',
    templateUrl: 'creditcardterminalform.component.html'
})

export class CreditcardterminalFormComponent implements OnInit {
    creditcardterminalform: FormGroup;
    creditterminalList = [];
    creditterminalData = {};
    bankList = [];
    deptList = [];
    id: number;
    ajaxCount: number;

    constructor(
        private CreditcardterminalService: CreditcardterminalService,
        private DepartmentService: DepartmentService,
        private BankService: BankService,
        private route: ActivatedRoute,
        private spinner: NgxSpinnerService,
        private router: Router

    ) { }
    ngDoCheck() {
        const change = this.ajaxCount;
        if (this.ajaxCount == 0) {
            this.spinner.hide();
        }
    }
    ngOnInit() {
        this.ajaxCount = 0
        this.spinner.show();
        this.creditterminalData['f049fstatus'] = 1;
        this.creditcardterminalform = new FormGroup({
            creditterminal: new FormControl('', Validators.required)
        });
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        
        this.ajaxCount++;
        // Department  dropdown
        this.DepartmentService.getActiveItems().subscribe(
            data => {
                this.ajaxCount--;
                this.deptList = data['result']['data'];
            }, error => {
                console.log(error);
            });
        //banksetup account dropdown
        this.BankService.getActiveBank().subscribe(
            data => {
                this.bankList = data['result']['data'];
            }, error => {
                console.log(error);
            });

        if (this.id > 0) {
            this.ajaxCount++;
            this.CreditcardterminalService.getItemsDetail(this.id).subscribe(
                data => {
                    this.ajaxCount--;
                    this.creditterminalData = data['result'][0];
                    // converting from string to int for radio button default selection
                    this.creditterminalData['f049fstatus'] = parseInt(data['result'][0]['f049fstatus']);

                    this.creditterminalData['f049fbank'] = parseInt(data['result'][0]['f049fbank']);
                }, error => {
                    console.log(error);
                });
        }
        console.log(this.id);
    }
    addCreditcardterminal() {
        // this.spinner.show();
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.CreditcardterminalService.updatecreditcardterminalItems(this.creditterminalData, this.id).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert('Terminal Number Already Exist');
                    }
                    else {
                        this.router.navigate(['studentfinance/creditcardterminal']);
                        alert("Credit Card Terminal has been Updated Sucessfully !!");
                    }

                }, error => {
                    console.log(error);
                    // this.spinner.hide();
                    alert('Terminal Number Already Exist');
                });

        } else {
            this.CreditcardterminalService.insertcreditcardterminalItems(this.creditterminalData).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert('Terminal Number Already Exist');
                    }
                    else {
                        this.router.navigate(['studentfinance/creditcardterminal']);
                        alert("Credit Card Terminal has been Added Sucessfully !!");
                    }
                }, error => {
                    console.log(error);
                    // this.spinner.hide();
                });
        }
    }
}