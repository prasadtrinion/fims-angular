import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { PtptnService } from '../service/ptptn.service'
import { SemesterService } from '../service/semester.service'
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
    selector: 'PtptnComponent',
    templateUrl: 'ptptn.component.html'
})

export class PtptnComponent implements OnInit {
    ptptnform: FormGroup;
    ptptnList = [];
    ptptnData = {};
    semesterList = [];
    file: any;
    // semsequenceList=[];


    id: number;

    constructor(
        private PtptnService: PtptnService,
        private SemesterService: SemesterService,
        private route: ActivatedRoute,
        private router: Router

    ) { }

    ngOnInit() {
        this.ptptnData['f053fstatus'] = 1;
        // this.semesterData['f066fidFeeCategory']='';


        this.ptptnform = new FormGroup({
            ptptn: new FormControl('', Validators.required)
        });
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        this.SemesterService.getItems().subscribe(
            data => {

                this.semesterList = data['result']['data'];
            }, error => {
                console.log(error);
            });
        console.log(this.id);


        console.log(this.id);

        if (this.id > 0) {
            this.PtptnService.getItemsDetail(this.id).subscribe(
                data => {
                    this.ptptnData = data['result'][0];

                    // converting from string to int for radio button default selection
                    this.ptptnData['f053fstatus'] = parseInt(data['result'][0]['f053fstatus']);

                    console.log(this.ptptnData);
                }, error => {
                    console.log(error);
                });
        }


        console.log(this.id);
    }
    fileSelected(event) {

        this.file = event.target.files[0];
        console.log(this.file);
    }
    addPtptn() {
        let fd = new FormData();
        fd.append('file', this.file);
        this.PtptnService.insertPtptnItems(this.ptptnData).subscribe(
            data => {
                this.ptptnData['file'] = data['name'];

                this.router.navigate(['studentfinance/ptptn']);
                alert("Imported Data Successfully");
                this.ptptnData['f053fsemester'] = '';
                this.ptptnData['f053fcode'] = '';
                this.ptptnData['f053fstartDate'] = '';
                this.ptptnData['f053fsemSequence'] = '';
            }, error => {
                console.log(error);
            });
    }
}