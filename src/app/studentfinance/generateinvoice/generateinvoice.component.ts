import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { GenerateinvoiceService } from '../service/generateinvoice.service'
import { ProgrammeService } from '../service/programme.service'
import { DefinationmsService } from '../service/definationms.service';
import { IntakeService } from '../service/intake.service'
import { StudentService } from '../service/student.service'
import { FeestructureService } from '../service/feestructure.service'


@Component({
    selector: 'Generateinvoice',
    templateUrl: 'generateinvoice.component.html'
})

export class GenerateinvoiceComponent implements OnInit {
    generateinvoiceList = [];
    generateinvoiceData = {};
    programmeList = [];
    intakeList = [];
    citizenList = [];
    modeOfStudyList = [];
    studentList = [];
    id: number;
    ajaxCount;
    graduationType = [];
    regionList = [];

    constructor(
        private FeestructureService: FeestructureService,
        private GenerateinvoiceService: GenerateinvoiceService,
        private ProgrammeService: ProgrammeService,
        private DefinationmsService: DefinationmsService,
        private IntakeService: IntakeService,
        private StudentService: StudentService
    ) { }

    ngOnInit() {

        // this.generateinvoiceData['f048fgraduationType'] = 'UG';
        // this.getViewData();
        //programme 
        // this.ProgrammeService.getItems().subscribe(
        //     data => {
        //         this.programmeList = data['result']['data'];
        //     }, error => {
        //         console.log(error);
        //     });
        // console.log(this.id);
        // //intake
        // this.IntakeService.getItems().subscribe(
        //     data => {
        //         this.intakeList = data['result']['data'];
        //     }, error => {
        //         console.log(error);
        //     });
        // //Citizen drop down 
        this.DefinationmsService.getCitizen('Citizen').subscribe(
            data => {
                this.citizenList = data['result'];
            }, error => {
                console.log(error);
            });
        this.ajaxCount++;

        this.DefinationmsService.getScheme('ProgrammeScheme').subscribe(
            data => {
                this.ajaxCount--;

                this.graduationType = data['result'];
            }, error => {
                console.log(error);
            });
        // this.GenerateinvoiceService.getItems().subscribe(
        //     data => {
        //         this.generateinvoiceList = data['result']['data'];
        //     }, error => {
        //         console.log(error);
        //     });
        //student dropdown
        // this.StudentService.getItems().subscribe(
        //     data => {
        //         this.studentList = data['result']['data'];
        //     }, error => {
        //         console.log(error);
        //     });
        // //ModeOfStudy drop down 
        this.DefinationmsService.getModeOfStudy('ModeOfStudy').subscribe(
            data => {
                this.modeOfStudyList = data['result'];
            }, error => {
                console.log(error);
            });
    }
    addGenerateinvoice() {
        console.log(this.generateinvoiceData);
        this.GenerateinvoiceService.getItemsDetail(this.generateinvoiceData).subscribe(
            data => {
                this.generateinvoiceList = data['result'];
            }, error => {
                console.log(error);
            });

    }
    // showStudent(){
    //     this.GenerateinvoiceService.getStudents().subscribe(
    //         data => {
    //             this.generateinvoiceList = data['result'];
    //         }, error => {
    //             console.log(error);
    //         });
    // }
    getViewData() {
        if (this.generateinvoiceData['graduationType'] == "UG") {
            //Intake drop down 
            this.FeestructureService.getUgIntake().subscribe(
                data => {
                    this.intakeList = data['result'];
                }, error => {
                    console.log(error);
                });
            // semester dropdown
            // this.ajaxCount++;
            // this.FeestructureService.getUgSemester().subscribe(
            //     data => {
            //         this.ajaxCount--;
            //         this.intakeList = data['result'];
            //     }, error => {
            //         console.log(error);
            //     });

            //Programme drop down 
            this.ajaxCount++;

            this.FeestructureService.getUgPrograms().subscribe(
                data => {
                    this.ajaxCount--;
                    this.programmeList = data['result'];
                }, error => {
                    console.log(error);
                });

            //Citizen drop down 
            this.ajaxCount++;

            this.FeestructureService.getUgStudent().subscribe(
                data => {
                    this.ajaxCount--;

                    this.studentList = data['result'];
                }, error => {
                    console.log(error);
                });
            this.FeestructureService.getUgRegion().subscribe(
                data => {

                    this.regionList = data['result'];
                }, error => {
                    console.log(error);
                });

        }
        else {
            //Intake drop down 
            this.FeestructureService.getPgIntake().subscribe(
                data => {
                    this.intakeList = data['result'];
                }, error => {
                    console.log(error);
                });
            // semester dropdown
            // this.ajaxCount++;

            // this.FeestructureService.getPgSemester().subscribe(
            //     data => {
            //         this.ajaxCount--;
            //         this.intakeList = data['result'];
            //     }, error => {
            //         console.log(error);
            //     });

            //Programme drop down 
            this.ajaxCount++;

            this.FeestructureService.getPgProgram().subscribe(
                data => {
                    this.ajaxCount--;
                    this.programmeList = data['result'];
                }, error => {
                    console.log(error);
                });

            //Citizen drop down 
            this.ajaxCount++;

            this.FeestructureService.getPgStudent().subscribe(
                data => {
                    this.ajaxCount--;

                    this.studentList = data['result'];
                }, error => {
                    console.log(error);
                });

            //Region drop down 
            this.FeestructureService.getPgRegion().subscribe(
                data => {

                    this.regionList = data['result'];
                }, error => {
                    console.log(error);
                });
        }
    }
}