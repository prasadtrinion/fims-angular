import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { GenerateinvoiceService } from '../service/generateinvoice.service'

 
@Component({
    selector: 'Generateinvoice1',
    templateUrl: 'generateinvoice1.component.html'
})

export class Generateinvoice1Component implements OnInit {
    constructor(
        private GenerateinvoiceService: GenerateinvoiceService,
    ) { }

    ngOnInit() {
    }
    generateinvoice(){
        let data={};
        var confirmPop = confirm("Do you want to Generate?");
        if (confirmPop == false) {
            return false;
        }
        this.GenerateinvoiceService.generatestudentinvoice(data).subscribe( 
            data => {
                alert("Generated Successfully");
            }, error => {
                console.log(error);
            });
    }
}