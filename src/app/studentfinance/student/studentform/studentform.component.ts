import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { StudentService } from '../../service/student.service'
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CountryService } from '../../../generalsetup/service/country.service';
import { StateService } from '../../../generalsetup/service/state.service';
import { DefinationmsService } from '../../service/definationms.service';
import { FeestructureService } from '../../service/feestructure.service';
import { DepartmentService } from '../../../generalsetup/service/department.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'StudentFormComponent',
    templateUrl: 'studentform.component.html'
})

export class StudentFormComponent implements OnInit {
    studentform: FormGroup;
    studentList = [];
    studentData = {};
    stateList = [];
    countryList = [];
    genderList = [];
    graduationType = [];
    intakeList = [];
    programmeList = [];
    regionList = [];
    citizenList = [];
    deptList = [];
    studentCategoryList = [];
    modeOfStudyList = [];
    semesterList = [];
    ajaxCount: number;
    id: number;

    constructor(
        private StudentService: StudentService,
        private route: ActivatedRoute,
        private router: Router,
        private StateService: StateService,
        private CountryService: CountryService,
        private DepartmentService: DepartmentService,
        private DefinationmsService: DefinationmsService,
        private FeestructureService: FeestructureService,
        private spinner: NgxSpinnerService,
    ) { }
    ngDoCheck() {
        const change = this.ajaxCount;
        if (this.ajaxCount == 0) {
            this.spinner.hide();
        }
    }
    ngOnInit() {
        this.ajaxCount = 0;
        this.spinner.show();
        this.studentData['f060fstatus'] = 1;
        let genderObj = {};
        genderObj['name'] = 'Male';
        genderObj['value'] = 'M';
        this.genderList.push(genderObj);
        genderObj = {};
        genderObj['name'] = 'Female';
        genderObj['value'] = 'F';
        this.genderList.push(genderObj);
        this.studentform = new FormGroup({
            student: new FormControl('', Validators.required)
        });
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.ajaxCount++;
        this.CountryService.getActiveCountryList().subscribe(
            data => {
                this.ajaxCount--;
                this.countryList = data['result']['data'];
            }
        );
        this.ajaxCount++;
        this.DefinationmsService.getModeOfStudy('ModeOfStudy').subscribe(
            data => {
                this.ajaxCount--;
                this.modeOfStudyList = data['result'];
            }, error => {
                console.log(error);
            });
        this.ajaxCount++;
        this.DefinationmsService.getStudentCategory('StudentCategory').subscribe(
            data => {
                this.ajaxCount--;
                this.studentCategoryList = data['result'];
            }, error => {
                console.log(error);
            });
        this.ajaxCount++;
        this.DefinationmsService.getScheme('ProgrammeScheme').subscribe(
            data => {
                this.ajaxCount--;
                this.graduationType = data['result'];
            }, error => {
                console.log(error);
            });
        this.ajaxCount++;
        this.DefinationmsService.getCitizen('Citizen').subscribe(
            data => {
                this.ajaxCount--;
                this.citizenList = data['result'];
            }, error => {
                console.log(error);
            });
        // Department  dropdown
        this.DepartmentService.getActiveItems().subscribe(
            data => {
                this.ajaxCount--;
                this.deptList = data['result']['data'];
            }, error => {
                console.log(error);
            });
        // this.StateService.getActiveStateList().subscribe(
        //     data => {
        //         this.stateList = data['result']['data'];
        //     }
        // );
        if (this.id > 0) {
            this.ajaxCount++;
            this.StudentService.getItemsDetail(this.id).subscribe(
                data => {
                    this.ajaxCount--;
                    this.studentData = data['result'][0];
                    // converting from string to int for radio button default selection
                    this.studentData['f065fstatus'] = parseInt(data['result'][0]['f065fstatus']);
                    this.studentData['f060fdepartment'] = data['result'][0]['f060fdepartment'];
                    this.studentData['f060fstate'] = data['result'][0]['f060fstate'].toString();
                    this.onCountryChange();
                    this.getViewData();
                    console.log(this.studentData);
                }, error => {
                    console.log(error);
                });
        }
    }
    onCountryChange() {
        if (this.studentData['f060fcountry'] == undefined) {
            this.studentData['f060fcountry'] = 0;
        }
        this.StateService.getStates(this.studentData['f060fcountry']).subscribe(
            data => {
                this.stateList = data['result'];
                let other = {
                    'f012fid': "1",
                    "f012fshortName": "SH",
                    "f012fstateName": "Others"

                };
                this.stateList.push(other);

            }
        );
    }
    getViewData() {

        if (this.studentData['f060ftype'] == "UG") {
            //Intake drop down 


            this.FeestructureService.getUgIntake().subscribe(
                data => {
                    this.intakeList = data['result'];
                }, error => {
                    console.log(error);
                });

            //Programme drop down 

            this.FeestructureService.getUgPrograms().subscribe(
                data => {
                    this.programmeList = data['result'];
                }, error => {
                    console.log(error);
                });

            this.FeestructureService.getUgRegion().subscribe(
                data => {

                    this.regionList = data['result'];
                }, error => {
                    console.log(error);
                });
            this.FeestructureService.getUgSemester().subscribe(
                data => {

                    this.semesterList = data['result']['data'];
                }, error => {
                    console.log(error);
                });

        }
        else {

            //Intake drop down 

            // this.ajaxCount++;

            // this.FeestructureService.getPgSemester().subscribe(
            //     data => {
            //         this.ajaxCount--;
            //         this.intakeList = data['result'];
            //     }, error => {
            //         console.log(error);
            //     });

            this.FeestructureService.getPgRegion().subscribe(
                data => {

                    this.regionList = data['result'];
                }, error => {
                    console.log(error);
                });

            this.FeestructureService.getPgIntake().subscribe(
                data => {
                    this.intakeList = data['result'];
                }, error => {
                    console.log(error);
                });

            //Programme drop down 

            this.FeestructureService.getPgProgram().subscribe(
                data => {
                    this.programmeList = data['result'];
                }, error => {
                    console.log(error);
                });
            this.FeestructureService.getPgSemester().subscribe(
                data => {
                    this.semesterList = data['result']['data'];
                }, error => {
                    console.log(error);
                });


        }
    }

    addStudent() {
        console.log(this.studentData);
        this.studentData['f065fupdatedBy'] = 1;
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.StudentService.updatestudentItems(this.studentData, this.id).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert("Metric Number Already Exist");
                    }
                    else {
                        this.router.navigate(['studentfinance/student']);
                        alert("Student has been Updated SUccessfully");
                    }
                }, error => {
                    console.log(error);
                    alert("Metric Number Already Exist");
                });

        } else {
            this.StudentService.insertstudentItems(this.studentData).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert("Metric Number Already Exist");
                    }
                    else {
                        this.router.navigate(['studentfinance/student']);
                        alert("Student has been Added SUccessfully");
                    }
                }, error => {
                    console.log(error);
                });

        }

    }
    onlyNumberKey(event) {
        if (event.charCode == 46) {

        } else {
            return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
        }
    }
}