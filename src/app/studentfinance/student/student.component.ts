import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { StudentService } from '../service/student.service'
import { NgxSpinnerService } from 'ngx-spinner';
@Component({
    selector: 'Student',
    templateUrl: 'student.component.html'
})
export class StudentComponent implements OnInit {
    studentList =  [];
    studentData = {};
    id: number;
    constructor( 
        private StudentService: StudentService,
        private spinner: NgxSpinnerService,                                
    ) { }

    ngOnInit() { 
        this.spinner.show();                                                                
        this.StudentService.getItems().subscribe(
            data => {
                this.spinner.hide();
                this.studentList = data['result']['data'];
        }, error => {
            console.log(error);
        });
    }

    addStudent(){
           console.log(this.studentData);
        this.StudentService.insertstudentItems(this.studentData).subscribe(
            data => {
                this.studentList = data['todos'];
            }, error => {
                console.log(error);
            });

    }
}