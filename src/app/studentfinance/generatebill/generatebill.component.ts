import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { GeneratebillService } from '../service/generatebill.service'
import { ProgrammeService } from '../service/programme.service'
import { DefinationmsService } from '../service/definationms.service';
import { IntakeService } from '../service/intake.service'
import { StudentService } from '../service/student.service'
import { ActivatedRoute, Router } from '@angular/router';
import { FeestructureService } from '../service/feestructure.service'
import { GenerateinvoiceService } from '../service/generateinvoice.service'
import { environment } from '../../../environments/environment';

@Component({
    selector: 'Generatebill',
    templateUrl: 'generatebill.component.html'
})

export class GeneratebillComponent implements OnInit {
    generatebillList = [];
    generatebillData = {};
    programmeList = [];
    intakeList = [];
    citizenList = [];
    modeOfStudyList = [];
    regionList = [];
    studentCategoryList = [];
    studentList = [];
    graduationType=[];
    id: number;
    downloadUrl: string = environment.api.downloadbase;

    constructor(

        private GeneratebillService: GeneratebillService,
        private ProgrammeService: ProgrammeService,
        private DefinationmsService: DefinationmsService,
        private IntakeService: IntakeService,
        private route: ActivatedRoute,
        private router: Router,
        private StudentService: StudentService,
        private FeestructureService: FeestructureService,
        private GenerateinvoiceService: GenerateinvoiceService,

    ) { }

    ngOnInit() {
        let citizenObject = {};
        citizenObject['id'] = 1;
        citizenObject['name'] = "Local";
        citizenObject['value'] = "M";
        this.citizenList.push(citizenObject);
        citizenObject = {};
        citizenObject['id'] = 2;
        citizenObject['name'] = "International";
        citizenObject['value'] = "NM";
        this.citizenList.push(citizenObject);
        this.DefinationmsService.getScheme('ProgrammeScheme').subscribe(
            data => {

                this.graduationType = data['result'];
            }, error => {
                console.log(error);
            });
       
        //Citizen drop down 
        // this.DefinationmsService.getCitizen('Citizen').subscribe(
        //     data => {
        //         this.citizenList = data['result'];
        //     }, error => {
        //         console.log(error);
        //     });
        //StudentCategory drop down 
        this.DefinationmsService.getStudentCategory('StudentCategory').subscribe(
            data => {
                this.studentCategoryList = data['result'];
            }, error => {
                console.log(error);
            });

        //ModeOfStudy drop down 
        this.DefinationmsService.getModeOfStudy('ModeOfStudy').subscribe(
            data => {
                this.modeOfStudyList = data['result'];
            }, error => {
                console.log(error);
            });
    }

    getViewData() {

        if (this.generatebillData['graduationType'] == "UG") {
            //Intake drop down 

            this.FeestructureService.getUgIntake().subscribe(
                data => {
                    this.intakeList = data['result']; 
                }, error => {
                    console.log(error);
                });

            //Programme drop down 

            this.FeestructureService.getUgPrograms().subscribe(
                data => {
                    this.programmeList = data['result'];
                }, error => {
                    console.log(error);
                });

                // this.FeestructureService.getUgRegion().subscribe(
                //     data => {
        
                //         this.regionList = data['result'];
                //     }, error => {
                //         console.log(error);
                //     });
                    this.FeestructureService.getUgStudent().subscribe(
                        data => {
            
                            this.studentList = data['result'];
                        }, error => {
                            console.log(error);
                        });

        }
        else {

            //Intake drop down 

            // this.ajaxCount++;

            // this.FeestructureService.getPgSemester().subscribe(
            //     data => {
            //         this.ajaxCount--;
            //         this.intakeList = data['result'];
            //     }, error => {
            //         console.log(error);
            //     });

            //Programme drop down 

            this.FeestructureService.getPgProgram().subscribe(
                data => {
                    this.programmeList = data['result'];
                }, error => {
                    console.log(error);
                });
                this.FeestructureService.getPgIntake().subscribe(
                    data => {
                        this.intakeList = data['result'];
                    }, error => {
                        console.log(error);
                    });
                    // this.FeestructureService.getPgRegion().subscribe(
                    //     data => {
            
                    //         this.regionList = data['result'];
                    //     }, error => {
                    //         console.log(error);
                    //     });
                this.FeestructureService.getPgStudent().subscribe(
                    data => {
        
                        this.studentList = data['result'];
                    }, error => {
                        console.log(error);
                    });


        }
    }
    showStudent(){
        this.GeneratebillService.getItemsDetail(this.generatebillData).subscribe(
            data => {
                this.generatebillList = data['result'];
                for(var i=0;i<this.generatebillList.length;i++){
                    this.generatebillList[i]['checked'] = false;
                }
            }, error => {
                console.log(error);
            });
    }
    generatebills(){
        let generateIds = [];
        for(var i=0;i<this.generatebillList.length;i++){
            if(this.generatebillList[i]['checked'] == true){
                generateIds.push(this.generatebillList[i]);
            }
        }
        var confirmPop = confirm("Do you want to Generate Bill?");
        if (confirmPop == false) {
            return false;
        }
        if (generateIds.length < 1) {
            alert("Select atleast one Student to Generate Bill");
            return false;
        }
       
        let postObj = {};
        postObj['id']=generateIds;
        this.GenerateinvoiceService.generatestudentinvoice(postObj).subscribe(
            data => {
                alert("Generated Successfully");
                this.generatebillList=[];
                this.generatebillData['graduationType'] = '';
                this.generatebillData['program'] = '';
                this.generatebillData['student'] = '';
                this.generatebillData['region'] = '';
                this.generatebillData['intake'] ='';
                this.generatebillData['citizen'] ='';
            }, error => {
                console.log(error);
            });
    }
    // downloadPDF(id, type) {
    //     console.log(id);
    //     let newobj = {};
    //     newobj['idLoan'] = id;
    //     if (type == 1) {
    //         this.GeneratebillService.downloadGeneratebill(newobj).subscribe(
    //             data => {
    //                 console.log(data['name']);
    //                 window.open('http://uum-api.mtcsb.my/download/' + data['name']);
    //             }, error => {
    //                 console.log(error);
    //             });

    //     }
    //     else {
    //         this.GeneratebillService.downloadComputerReport(newobj).subscribe(
    //             data => {
    //                 console.log(data['name']);
    //                 window.open('http://uum-api.mtcsb.my/download/' + data['name']);
    //             }, error => {
    //                 console.log(error);
    //             });

    //     }

    // }
    downloadPDF(id) {
        let newreport = {};
        newreport['studentId'] = id;
        var confirmPop = confirm("Do you want to Print?");
        if (confirmPop == false) {
            return false;
        }
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
            this.GeneratebillService.downloadGeneratebill(newreport).subscribe(
                data => {
                    // alert('KWAP has been saved');
                    window.open(this.downloadUrl + data['name']);
                    this.router.navigate(['generalledger/journal']);

                }, error => {
                    console.log(error);
                });
            }

    // generateBill(id){
    //     let newreport = {};
    //     newreport['studentId'] = id;
    //     this.GeneratebillService.invoiceGenerate(newreport).subscribe(
    //         data => {
    //             this.generatebillList = data['result'];
    //             alert("Invoice has been generated");
    //         }, error => {
    //             console.log(error);
    //         });
    // }
    
}