import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { BondinformationService } from '../../service/bondinformation.service'
import { IntakeService } from '../../service/intake.service';
import { ProgrammeService } from '../../service/programme.service';
import { DefinationmsService } from '../../service/definationms.service';
import { StudentService } from '../../service/student.service'
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { FeestructureService } from '../../service/feestructure.service'
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'BondinformationFormComponent',
    templateUrl: 'bondinformationform.component.html'
})

export class BondinformationFormComponent implements OnInit {
    bondinformationform: FormGroup;
    bondinformationList = [];
    bondinformationData = {};
    programmeList = [];
    intakeList = [];
    citizenList = [];
    modeOfStudyList = [];
    studentList = [];
    searchList = [];
    indList = [];
    graduationType = [];
    searchData = [];
    regionList = [];
    searchObject = {};
    selectAllCheckbox: false;
    searchFid: number;
    ajaxCount: number;
    id: number;

    constructor(
        private BondinformationService: BondinformationService,
        private IntakeService: IntakeService,
        private ProgrammeService: ProgrammeService,
        private DefinationmsService: DefinationmsService,
        private FeestructureService: FeestructureService,
        private StudentService: StudentService,
        private spinner: NgxSpinnerService,
        private route: ActivatedRoute,
        private router: Router

    ) { }

    ngDoCheck() {

        if (this.ajaxCount == 0) {
            this.spinner.hide();
        }
    }
    ngOnInit() {
        this.ajaxCount = 0;
        this.spinner.show();
        //graduation type dropdown
        this.ajaxCount++;
        this.DefinationmsService.getScheme('ProgrammeScheme').subscribe(
            data => {
                this.ajaxCount--;
                this.graduationType = data['result'];
            }, error => {
                this.spinner.hide();
                console.log(error);
            });
        //Intake drop down 
        this.ajaxCount++;
        this.IntakeService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.intakeList = data['result']['data'];
            }, error => {
                this.spinner.hide();
                console.log(error);
            });
        //Citizen drop down 
        this.ajaxCount++;
        this.DefinationmsService.getCitizen('Citizen').subscribe(
            data => {
                this.ajaxCount--;
                this.citizenList = data['result'];
            }, error => {
                this.spinner.hide();
                console.log(error);
            });
        //ModeOfStudy drop down 
        this.ajaxCount++;
        this.DefinationmsService.getModeOfStudy('ModeOfStudy').subscribe(
            data => {
                this.ajaxCount--;
                this.modeOfStudyList = data['result'];
            }, error => {
                this.spinner.hide();
                console.log(error);
            });
        //Graduation status  
        this.ajaxCount++;
        this.DefinationmsService.getModeOfStudy('ModeOfStudy').subscribe(
            data => {
                this.ajaxCount--;
                this.modeOfStudyList = data['result'];
            }, error => {
                this.spinner.hide();
                console.log(error);
            });

        this.bondinformationform = new FormGroup({
            bondinformation: new FormControl('', Validators.required)
        });
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        console.log(this.id);

        if (this.id > 0) {
            this.ajaxCount++;
            this.BondinformationService.getItemsDetail(this.id).subscribe(
                data => {
                    this.ajaxCount--;
                    this.bondinformationData = data['result'][0];
                    // converting from string to int for radio button default selection
                    this.bondinformationData['f065fstatus'] = parseInt(data['result'][0]['f065fstatus']);
                    console.log(this.bondinformationData);
                }, error => {
                    this.spinner.hide();
                    console.log(error);
                });
        }
        console.log(this.id);
    }
    getViewData() {

        if (this.bondinformationData['graduationType'] == "UG") {
            //Intake drop down 

            this.FeestructureService.getUgIntake().subscribe(
                data => {
                    this.intakeList = data['result'];
                }, error => {
                    console.log(error);
                });

            //Programme drop down 

            this.FeestructureService.getUgPrograms().subscribe(
                data => {
                    this.programmeList = data['result'];
                }, error => {
                    console.log(error);
                });

            this.FeestructureService.getUgRegion().subscribe(
                data => {

                    this.regionList = data['result'];
                }, error => {
                    console.log(error);
                });
            this.FeestructureService.getUgStudent().subscribe(
                data => {

                    this.studentList = data['result'];
                }, error => {
                    console.log(error);
                });

        }
        else {

            //Intake drop down 

            // this.ajaxCount++;

            // this.FeestructureService.getPgSemester().subscribe(
            //     data => {
            //         this.ajaxCount--;
            //         this.intakeList = data['result'];
            //     }, error => {
            //         console.log(error);
            //     });

            //Programme drop down 

            this.FeestructureService.getPgProgram().subscribe(
                data => {
                    this.programmeList = data['result'];
                }, error => {
                    console.log(error);
                });
            this.FeestructureService.getPgIntake().subscribe(
                data => {
                    this.intakeList = data['result'];
                }, error => {
                    console.log(error);
                });
            this.FeestructureService.getPgRegion().subscribe(
                data => {

                    this.regionList = data['result'];
                }, error => {
                    console.log(error);
                });
            this.FeestructureService.getPgStudent().subscribe(
                data => {

                    this.studentList = data['result'];
                }, error => {
                    console.log(error);
                });


        }
    }
    searchListFunction() {
        this.searchList = [];
        this.BondinformationService.searchListDetails(this.bondinformationData).subscribe(
            data => {
                this.searchList = data['result'];
                for (var i = 0; i < this.searchList.length; i++) {
                    this.searchList[i]['checked'] = false;
                }
            });
    }

    addBondInfo() {
        let returnArrayList = [];
        this.searchFid = 0;

        for (var i = 0; i < this.searchList.length; i++) {
            if (this.searchList[i].checked == true) {
                this.searchFid = this.searchList[i];
                returnArrayList.push(this.searchFid);
            }
            if (this.searchList[i]['returnDate'] == undefined || this.searchList[i]['returnDate'] == '') {
                alert("Select Return Date");
                return false;
            }
        }
        if (returnArrayList.length < 1) {
            alert("Select atleast one Student to Refund");
            return false;
        }
        var confirmPop = confirm("Do you want to Return?");
        if (confirmPop == false) {
            return false;
        }
        console.log(returnArrayList);
        var returnObject = {};
        returnObject['id'] = returnArrayList;
        this.BondinformationService.insertBondinformationItems(returnObject).subscribe(
            data => {
                this.router.navigate(['studentfinance/bondinformation']);
                alert("Refund Information has been Added Sucessfully");
            }, error => {
                console.log(error);
            });

    }
    selectAll() {
        console.log(this.selectAllCheckbox);
        for (var i = 0; i < this.searchList.length; i++) {
            this.searchList[i].f065fstatus = this.selectAllCheckbox;
        }
    }
}