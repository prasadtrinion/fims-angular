import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { BondinformationService } from '../service/bondinformation.service'
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'Bondinformation',
    templateUrl: 'bondinformation.component.html'
})

export class BondinformationComponent implements OnInit {
    bondinformationList =  [];
    bondinformationData = {};
    returnapprovalData = {};
    id: number;

    constructor(
        private BondinformationService: BondinformationService,
        private spinner: NgxSpinnerService,
    ) { }

    ngOnInit() { 
        this.spinner.show();
        this.BondinformationService.getItemsFull().subscribe(
            data => {
                this.spinner.hide();                
                this.bondinformationList = data['result']['data'];
                console.log(this.bondinformationList);
        }, error => {
            console.log(error);
        });
    }
}