import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { StudentinsuranceService } from '../../service/studentinsurance.service'
import { CountryService } from '../../../generalsetup/service/country.service'
import { StateService } from '../../../generalsetup/service/state.service'
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'StudentinsuranceFormComponent',
    templateUrl: 'studentinsuranceform.component.html'
})

export class StudentinsuranceFormComponent implements OnInit {
    studentinsuranceform: FormGroup;
    studentinsuranceList = [];
    studentinsuranceData = {};
    countryList = [];
    stateList = [];
    id: number;
    ajaxCount: number;
    constructor(
        private StudentinsuranceService: StudentinsuranceService,
        private countryService: CountryService,
        private StateService: StateService,
        private spinner: NgxSpinnerService,
        private route: ActivatedRoute,
        private router: Router

    ) { }
    ngDoCheck() {
        const change = this.ajaxCount;
        if (this.ajaxCount == 0) {
            this.spinner.hide();
        }
    }
    ngOnInit() {
        this.ajaxCount = 0;
        this.spinner.show();
        this.studentinsuranceData['f099fstatus'] = 1;

        //country dropdown
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.ajaxCount++;
        this.countryService.getActiveCountryList().subscribe(
            data => {
                this.ajaxCount--;
                this.countryList = data['result']['data'];
            }, error => {
                console.log(error);
            });
        //State dropdown 
        // this.StateService.getItems().subscribe(
        //     data => {
        //         this.stateList = data['result']['data'];
        //     }, error => {
        //         console.log(error);
        //     });

        this.studentinsuranceform = new FormGroup({
            studentinsurance: new FormControl('', Validators.required)
        });
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.ajaxCount++;
            this.StudentinsuranceService.getItemsDetail(this.id).subscribe(
                data => {
                    this.ajaxCount--;
                    this.studentinsuranceData = data['result'][0];
                    // converting from string to int for radio button default selection
                    this.studentinsuranceData['f050fstatus'] = parseInt(data['result'][0]['f050fstatus']);
                    this.studentinsuranceData['f099fstate'] = parseInt(data['result'][0]['f099fstate']);
                    console.log(this.studentinsuranceData);
                    this.onCountryChange();
                }, error => {
                    console.log(error);
                });
        }


        console.log(this.id);
    }
    onCountryChange() {

        if (this.studentinsuranceData['f099fcountry'] == undefined) {
            this.studentinsuranceData['f099fcountry'] = 0;
        }
        this.StateService.getStates(this.studentinsuranceData['f099fcountry']).subscribe(
            data => {
                // this.ajaxcount --;
                this.stateList = data['result'];
                let other = {
                    'f012fid': "1",
                    "f012fshortName": "SH",
                    "f012fstateName": "Others"

                };
                this.stateList.push(other);

            }
        );
    }
    addStudentinsurance() {
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        console.log(this.studentinsuranceData);

        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.StudentinsuranceService.updatestudentinsuranceItems(this.studentinsuranceData, this.id).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert('Insurance Code Already Exist');
                    }
                    else {
                        this.router.navigate(['studentfinance/studentinsurance']);
                        alert(" Student Insurance has been Updated Sucessfully ! ");
                    }
                }, error => {
                    console.log(error);
                    alert('Insurance Code Already Exist');
                });

        } else {
            this.StudentinsuranceService.insertstudentinsuranceItems(this.studentinsuranceData).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert('Insurance Code Already Exist');
                    }
                    else {
                        this.router.navigate(['studentfinance/studentinsurance']);
                        alert(" Student Insurance has been Added Sucessfully ! ");
                    }
                }, error => {
                    console.log(error);
                });

        }

    }
    onlyNumberKey(event) {
        //console.log(event);
        if (event.charCode == 46) {
        } else {
            return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
        }
    }
}