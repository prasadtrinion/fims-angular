import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { StudentinsuranceService } from '../service/studentinsurance.service'
import { NgxSpinnerService } from 'ngx-spinner';
@Component({
    selector: 'Studentinsurance',
    templateUrl: 'studentinsurance.component.html'
})

export class StudentinsuranceComponent implements OnInit {
    studentinsuranceList =  [];
    studentinsuranceData = {};
    id: number;

    constructor(
        private StudentinsuranceService: StudentinsuranceService,
        private spinner: NgxSpinnerService,                

    ) { }

    ngOnInit() { 
        this.spinner.show();                                                        
        this.StudentinsuranceService.getItems().subscribe(
            data => {
                this.spinner.hide();
                this.studentinsuranceList = data['result']['data'];
        }, error => {
            console.log(error);
        });
    }

    addStudentinsurance(){
           console.log(this.studentinsuranceData);
        this.StudentinsuranceService.insertstudentinsuranceItems(this.studentinsuranceData).subscribe(
            data => {
                this.studentinsuranceList = data['todos'];
            }, error => {
                console.log(error);
            });

    }
}