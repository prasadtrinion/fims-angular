import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { InsurancesetupService } from '../service/insurancesetup.service'


@Component({
    selector: 'Insurancesetup',
    templateUrl: 'insurancesetup.component.html'
})

export class InsurancesetupComponent implements OnInit {
    insurancesetupList =  [];
    insurancesetupData = {};
    id: number;

    constructor(
        
        private InsurancesetupService: InsurancesetupService

    ) { }

    ngOnInit() { 
        
        this.InsurancesetupService.getItems().subscribe(
            data => {
                this.insurancesetupList = data['result']['data'];
        }, error => {
            console.log(error);
        });
    }

    addInsurancesetup(){
           console.log(this.insurancesetupData);
        this.InsurancesetupService.insertinsurancesetupItems(this.insurancesetupData).subscribe(
            data => {
                this.insurancesetupList = data['todos'];
            }, error => {
                console.log(error);
            });

    }
}