import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { InsurancesetupService } from '../../service/insurancesetup.service'
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
    selector: 'InsurancesetupFormComponent',
    templateUrl: 'insurancesetupform.component.html'
})

export class InsurancesetupFormComponent implements OnInit {
    insurancesetupform: FormGroup;
    insurancesetupList = [];
    insurancesetupData = {};
    id: number;
    constructor(
        private InsurancesetupService: InsurancesetupService,
        private route: ActivatedRoute,
        private router: Router

    ) { }

    ngOnInit() {
        this.insurancesetupData['f069fstatus'] = 1;

        this.insurancesetupform = new FormGroup({
            insurancesetup: new FormControl('', Validators.required)
        });
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        console.log(this.id);

        if (this.id > 0) {
            this.InsurancesetupService.getItemsDetail(this.id).subscribe(
                data => {
                    this.insurancesetupData = data['result'][0];
                    // converting from string to int for radio button default selection
                    this.insurancesetupData['f069fstatus'] = parseInt(data['result'][0]['f069fstatus']);
                   
                console.log(this.insurancesetupData);
                }, error => {
                    console.log(error);
                });
        }


        console.log(this.id);
    }

    addInsurancesetup() {
        console.log(this.insurancesetupData);
           this.insurancesetupData['f015fupdatedBy']=1;

           this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if(this.id>0) {
            this.InsurancesetupService.updateinsurancesetupItems(this.insurancesetupData,this.id).subscribe(
                data => {
                    this.router.navigate(['studentfinance/insurancesetup']);
            }, error => {
                console.log(error);
            });

    } else {
        this.InsurancesetupService.insertinsurancesetupItems(this.insurancesetupData).subscribe(
            data => {
                this.router.navigate(['studentfinance/insurancesetup']);

        }, error => {
            console.log(error);
        });

    }

}
}