import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { BondapprovalService } from '../service/bondapproval.service'
import { AlertService } from './../../_services/alert.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'Bondapproval',
    templateUrl: 'bondapproval.component.html'
})

export class BondapprovalComponent implements OnInit {
    rejectbondapprovalList = [];
    bondapprovalList = [];
    bondapprovalData = {};
    returnapprovalData = {};
    selectAllCheckbox = true;
    bondinformationList = [];

    constructor(
        private BondapprovalService: BondapprovalService,
        private spinner: NgxSpinnerService,
        private AlertService: AlertService
    ) { }

    ngOnInit() {
        this.getListData();
    }
    getListData() {
        this.spinner.show();
        this.BondapprovalService.getItemsDetail(0).subscribe(
            data => {
                this.spinner.hide();
                this.bondinformationList = data['result']['data'];
                console.log(this.bondinformationList);
            }, error => {
                console.log(error);
            });
    }

    addBudgetcostcenterapproval() {
        console.log(this.bondinformationList);
        var approvaloneIds = [];
        for (var i = 0; i < this.bondinformationList.length; i++) {
            if (this.bondinformationList[i].f065freturnStatus == true) {
                approvaloneIds.push(this.bondinformationList[i].f065fid);
            }
        }
        var confirmPop = confirm("Do you want to Approve?");
        if (confirmPop == false) {
            return false;
        }
        if (approvaloneIds.length < 1) {
            alert("Select atleast one Student to Approve");
            return false;
        }  
        
        var approvaloneUpdate = {};
        approvaloneUpdate['id'] = approvaloneIds;
        approvaloneUpdate['status'] = 1;
        this.BondapprovalService.updateBondapprovalItems(approvaloneUpdate).subscribe(
            data => {
                alert("Refund has been Approved Sucessfully ! ");
                this.getListData();
            }, error => {
                console.log(error);
            });
    }
    selectAll() {
        console.log("asdf");
        console.log(this.selectAllCheckbox);
        for (var i = 0; i < this.bondapprovalList.length; i++) {
            this.bondapprovalList[i].f017fapprovedStatus = this.selectAllCheckbox;
        }
        console.log(this.bondapprovalList);
    }

}
