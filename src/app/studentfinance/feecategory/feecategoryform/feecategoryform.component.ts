import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { FeecategoryService } from '../../service/feecategory.service'
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'FeecategoryFormComponent',
    templateUrl: 'feecategoryform.component.html'
})

export class FeecategoryFormComponent implements OnInit {
    feecategoryform: FormGroup;
    feecategoryList = [];
    feecategoryData = {};
    id: number;
    ajaxCount: number;
    constructor(
        private FeecategoryService: FeecategoryService,
        private spinner: NgxSpinnerService,
        private route: ActivatedRoute,
        private router: Router

    ) { }
    ngDoCheck() {
        const change = this.ajaxCount;
        if (this.ajaxCount == 0) {
            this.spinner.hide();
        }
    }
    ngOnInit() {
        this.ajaxCount = 0
        this.spinner.show();
        this.feecategoryData['f065fstatus'] = 1;
        this.feecategoryform = new FormGroup({
            feecategory: new FormControl('', Validators.required)
        });
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        console.log(this.id);
        if (this.id > 0) {
            this.ajaxCount++;
            this.FeecategoryService.getItemsDetail(this.id).subscribe(
                data => {
                    this.ajaxCount--;
                    this.feecategoryData = data['result'][0];
                    // converting from string to int for radio button default selection
                    this.feecategoryData['f065fstatus'] = parseInt(data['result'][0]['f065fstatus']);
                    console.log(this.feecategoryData);
                }, error => {
                    console.log(error);
                });
        }
    }

    addFeecategory() {
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        console.log(this.feecategoryData);
        this.feecategoryData['f065fupdatedBy'] = 1;

        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.FeecategoryService.updatefeecategoryItems(this.feecategoryData, this.id).subscribe(
                data => {

                    if (data['status'] == 409) {
                        alert('Code  Already Exist');
                    }
                    else {
                        this.router.navigate(['studentfinance/feecategory']);
                        alert("Fee Category has been Updated Sucessfully !!");

                    }

                }, error => {
                    alert('Code  Already Exist');
                    console.log(error);
                });

        } else {
            this.FeecategoryService.insertfeecategoryItems(this.feecategoryData).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert('Code Already Exist');
                    }
                    else {
                        this.router.navigate(['studentfinance/feecategory']);
                        alert("Fee Category has been Added Sucessfully !!");

                    }


                }, error => {
                    console.log(error);
                });

        }

    }
}