import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { FeecategoryService } from '../service/feecategory.service'
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'Feecategory',
    templateUrl: 'feecategory.component.html'
})

export class FeecategoryComponent implements OnInit {
    feecategoryList =  [];
    feecategoryData = {};
    id: number;

    constructor(
        private FeecategoryService: FeecategoryService,
        private spinner: NgxSpinnerService,        

    ) { }

    ngOnInit() { 
        this.spinner.show();                                
        this.FeecategoryService.getItems().subscribe(
            data => {
                this.spinner.hide();
                this.feecategoryList = data['result']['data'];
        }, error => {
            console.log(error);
        });
    }

    addFeecategory(){
           console.log(this.feecategoryData);
        this.FeecategoryService.insertfeecategoryItems(this.feecategoryData).subscribe(
            data => {
                this.feecategoryList = data['todos'];
            }, error => {
                console.log(error);
            });

    }
}