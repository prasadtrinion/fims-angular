import { Injectable } from '@angular/core';
import { HttpClientModule, HttpHeaders, HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({
        'X-Auth-Client': 'qazwsx123',
        'Content-Type': 'application/json'
    })

};

@Injectable()

export class DefinationmsService {

    url: string = environment.api.base + environment.api.endPoints.defms;
    definationmsUrl: string = environment.api.base + environment.api.endPoints.definationms;
    Definationurl: string = environment.api.base + environment.api.endPoints.Definationurl;
    checkduplicationurl: string = environment.api.base + environment.api.endPoints.checkduplication;

    constructor(private httpClient: HttpClient) {

    }

    getItems(IntakeCategory) {
        return this.httpClient.get(this.url + '/' + IntakeCategory, httpOptions);
    }
    getStafftypeItems(Stafftype) {
        return this.httpClient.get(this.Definationurl + '/' + Stafftype, httpOptions);
    }
    getAllDefinationMsData() {
        return this.httpClient.get(this.url, httpOptions);
    }
    getPaymenttype(PaymentVoucherType) {

        return this.httpClient.get(this.Definationurl + '/' + PaymentVoucherType, httpOptions);
    }
    getCalculationtype(calculationType) {

        return this.httpClient.get(this.Definationurl + '/' + calculationType, httpOptions);
    }
    getPaymentmode(PaymentMode) {

        return this.httpClient.get(this.Definationurl + '/' + PaymentMode, httpOptions);
    }
    getTransaction(Transaction) {

        return this.httpClient.get(this.Definationurl + '/' + Transaction, httpOptions);
    }

    getScheme(ProgrammeScheme) {

        return this.httpClient.get(this.Definationurl + '/' + ProgrammeScheme, httpOptions);

    }
    getAward(Award) {

        return this.httpClient.get(this.Definationurl + '/' + Award, httpOptions);

    }
    getSemesterType(SemesterType) {

        return this.httpClient.get(this.Definationurl + '/' + SemesterType, httpOptions);

    }

    getSemesterSequence(SemesterSequence) {

        return this.httpClient.get(this.Definationurl + '/' + SemesterSequence, httpOptions);

    }
    getAccountType(AccountType) {

        return this.httpClient.get(this.Definationurl + '/' + AccountType, httpOptions);

    }
    getItemsDetail(id) {

        return this.httpClient.get(this.definationmsUrl + '/' + id, httpOptions);
    }

    insertDefinationmsItems(defmsData): Observable<any> {

        return this.httpClient.post(this.url, defmsData, httpOptions);
    }
    checkDuplication(data): Observable<any> {

        return this.httpClient.post(this.checkduplicationurl, data, httpOptions);
    }

    updateDefinationmsItems(defmsData, id): Observable<any> {

        return this.httpClient.put(this.url + '/' + id, defmsData, httpOptions);
    }
    getSponsor(Category){

        return this.httpClient.get(this.Definationurl+'/'+Category,httpOptions);

    }
    getInvestmentStatus(InvestmentStatus){

        return this.httpClient.get(this.Definationurl+'/'+InvestmentStatus,httpOptions);

    }
    getCategory(Category){

        return this.httpClient.get(this.Definationurl+'/'+Category,httpOptions);

    }
    getPayment(PaymentStatus){

        return this.httpClient.get(this.Definationurl+'/'+PaymentStatus,httpOptions);

    }
    getJobType(JobType){

        return this.httpClient.get(this.Definationurl+'/'+JobType,httpOptions);

    }
    getPurchaseMethod(PurchaseMethod){

        return this.httpClient.get(this.Definationurl+'/'+PurchaseMethod,httpOptions);

    }
    getSponsorType(SponsorType){

        return this.httpClient.get(this.Definationurl+'/'+SponsorType,httpOptions);

    }
    getDisposalType(DisposalType){

        return this.httpClient.get(this.Definationurl+'/'+DisposalType,httpOptions);

    }
    gettagstudentStatus(StudentTaggingStatus){

        return this.httpClient.get(this.Definationurl+'/'+StudentTaggingStatus,httpOptions);

    }
    getCitizen(Citizen){

        return this.httpClient.get(this.Definationurl+'/'+Citizen,httpOptions);

    }
    getRegion(Region){

        return this.httpClient.get(this.Definationurl+'/'+Region,httpOptions);

    }
    getStudentCategory(StudentCategory){

        return this.httpClient.get(this.Definationurl+'/'+StudentCategory,httpOptions);

    }
    getModeOfStudy(ModeOfStudy){

        return this.httpClient.get(this.Definationurl+'/'+ModeOfStudy,httpOptions);

    }
    getCalculationType(CalculationType){

        return this.httpClient.get(this.Definationurl+'/'+CalculationType,httpOptions);

    }
    getReceivableStatus(ReceivableStatus){

        return this.httpClient.get(this.Definationurl+'/'+ReceivableStatus,httpOptions);

    }
}