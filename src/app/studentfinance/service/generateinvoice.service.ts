import { Injectable } from '@angular/core';
import { HttpClientModule,HttpHeaders, HttpClient} from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({ 'X-Auth-Client': 'qazwsx123',
    'Content-Type': 'application/json' })
    
  };
  
@Injectable()

export class GenerateinvoiceService {
    url: string = environment.api.base + environment.api.endPoints.generateinvoiceData;
    studenturl: string = environment.api.base + environment.api.endPoints.generateStudentInvoice;
    generateurl: string = environment.api.base + environment.api.endPoints.studentInvoicesDetail;
    constructor(private httpClient: HttpClient) { 
        
    }
    
    getItems() {
        return this.httpClient.get(this.url,httpOptions);
    }

    getStudents() {
        return this.httpClient.get(this.studenturl,httpOptions);
    }

    getItemsDetail(generateinvoiceData) {
        return this.httpClient.post(this.generateurl,generateinvoiceData,httpOptions);
    }
    generatestudentinvoice(data): Observable<any> {
        return this.httpClient.post(this.studenturl,data,httpOptions);
    }
    

    insertGenerateinvoiceItems(generateinvoiceData): Observable<any> {
        return this.httpClient.post(this.url, generateinvoiceData,httpOptions);
    }

    updateGenerateinvoiceItems(generateinvoiceData,id): Observable<any> {
        return this.httpClient.put(this.url+'/'+id, generateinvoiceData,httpOptions);
       }
}