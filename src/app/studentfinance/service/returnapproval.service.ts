import { Injectable } from '@angular/core';
import { HttpClientModule,HttpHeaders, HttpClient} from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({ 'X-Auth-Client': 'qazwsx123',
    'Content-Type': 'application/json' })
    
  };
@Injectable()

export class ReturnapprovalService {

     url: string = environment.api.base + environment.api.endPoints.approveOne
     geturl: string = environment.api.base + environment.api.endPoints.getReturnsApprovedList
     approvalPuturl: string = environment.api.base + environment.api.endPoints.approveReturns
    //  approveoneurl: string = environment.api.base + environment.api.endPoints.rejectOne
    constructor(private httpClient: HttpClient) {    
    }
    getItems() {
        return this.httpClient.get(this.geturl+'/'+2,httpOptions);
    }
    getItemsDetail(id) {
        return this.httpClient.get(this.geturl+'/'+id, httpOptions);
    }
    insertReturanapprovalItems(returnapprovaloneData): Observable<any> {
        return this.httpClient.post(this.url, returnapprovaloneData,httpOptions);
    }
    updateReturanapprovalItems(returnapprovaloneData): Observable<any> {
        return this.httpClient.post(this.approvalPuturl, returnapprovaloneData,httpOptions);
    }
    rejectReturanapproval(returnapprovaloneData): Observable<any> {
        return this.httpClient.post(this.approvalPuturl, returnapprovaloneData,httpOptions);
    }
}