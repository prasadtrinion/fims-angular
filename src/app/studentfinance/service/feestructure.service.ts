import { Injectable } from '@angular/core';
import { HttpClientModule,HttpHeaders, HttpClient} from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({ 'X-Auth-Client': 'qazwsx123',
    'Content-Type': 'application/json' })
    
  };
  
@Injectable()
 
export class FeestructureService {
    url: string = environment.api.base + environment.api.endPoints.feeStructure;
    deleteurl: string = environment.api.base + environment.api.endPoints.deleteFeeStructureDetails;
    programdeleteurl: string = environment.api.base + environment.api.endPoints.deleteProgramCourceFee;
    
    itemcrdrurl: string = environment.api.base + environment.api.endPoints.getCreditDebitByFee;

    ugProgram: string = environment.api.base + environment.api.endPoints.ugProgram;
    employeeSalary: string = environment.api.base + environment.api.endPoints.employeeSalary;
    pgSemester: string = environment.api.base + environment.api.endPoints.pgSemester;
    pgProgram: string = environment.api.base + environment.api.endPoints.pgProgram;
    pgRegion: string = environment.api.base + environment.api.endPoints.pgRegion;
    pgStudent: string = environment.api.base + environment.api.endPoints.pgStudent;
    ugStudent: string = environment.api.base + environment.api.endPoints.ugStudent;
    pgNewStudent: string = environment.api.base + environment.api.endPoints.pgNewStudent;
    ugSemester: string = environment.api.base + environment.api.endPoints.ugSemester;
    ugRegion: string = environment.api.base + environment.api.endPoints.ugRegion;
    ugIntake: string = environment.api.base + environment.api.endPoints.ugIntake;
    pgIntake: string = environment.api.base + environment.api.endPoints.pgIntake;
    ugLearningCenter: string = environment.api.base + environment.api.endPoints.ugLearningCenter;
    pgLearningCenter: string = environment.api.base + environment.api.endPoints.pgLearningCenter;

    constructor(private httpClient: HttpClient) { 
        
    }
    
    getItems() {
        return this.httpClient.get(this.url,httpOptions);
    }
    getProgramDeleteItems(deleteObject){
        return this.httpClient.post(this.programdeleteurl,deleteObject,httpOptions);
    }
    getDeleteItems(deleteObject){
        return this.httpClient.post(this.deleteurl,deleteObject,httpOptions);
    }
    getUgPrograms() {
        return this.httpClient.get(this.ugProgram,httpOptions);
    }
    getUgLearningCenters() {
        return this.httpClient.get(this.ugLearningCenter,httpOptions);
    }
    getPgLearningCenters() {
        return this.httpClient.get(this.pgLearningCenter,httpOptions);
    }

    getEmployeeSalary() {
        return this.httpClient.get(this.employeeSalary,httpOptions);
    }
    getPgSemester() {
        return this.httpClient.get(this.pgSemester,httpOptions);
    }
    getPgProgram() {
        return this.httpClient.get(this.pgProgram,httpOptions);
    }
    getPgRegion() {
        return this.httpClient.get(this.pgRegion,httpOptions);
    }
    getPgStudent() {
        return this.httpClient.get(this.pgStudent,httpOptions);
    }
    getUgStudent() {
        return this.httpClient.get(this.ugStudent,httpOptions);
    }
    getPgNewStudent() {
        return this.httpClient.get(this.pgNewStudent
            ,httpOptions);
    }
    getUgSemester() {
        return this.httpClient.get(this.ugSemester,httpOptions);
    }
    getUgRegion() {
        return this.httpClient.get(this.ugRegion,httpOptions);
    }
    getUgIntake() {
        return this.httpClient.get(this.ugIntake,httpOptions);
    }
    getPgIntake() {
        return this.httpClient.get(this.pgIntake,httpOptions);
    }

    getItemsDetail(id) {
        return this.httpClient.get(this.url+'/'+id,httpOptions);
    }

    getCrDrByFee(id) {
        return this.httpClient.get(this.itemcrdrurl+'/'+id,httpOptions);
    }

    insertFeeStructureItems(feecategoryData): Observable<any> {
        return this.httpClient.post(this.url, feecategoryData,httpOptions);
    }

    updateFeeStructureItems(feecategoryData,id): Observable<any> {
        return this.httpClient.put(this.url+'/'+id, feecategoryData,httpOptions);
       }
}