import { Injectable } from '@angular/core';
import { HttpClientModule,HttpHeaders, HttpClient} from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({ 'X-Auth-Client': 'qazwsx123',
    'Content-Type': 'application/json' })
    
  };
  
@Injectable()

export class FeeitemService {
    url: string = environment.api.base + environment.api.endPoints.feeitem;
    getFeeItemurl: string = environment.api.base + environment.api.endPoints.getFeeItemByCategory;
    deleteurl: string = environment.api.base + environment.api.endPoints.deleteFeeItemDetails;
    Activeurl: string = environment.api.base + environment.api.endPoints.getFeeItemByStatus;
    constructor(private httpClient: HttpClient) { 
        
    }
    getFeeItemByCategory(id){
        return this.httpClient.get(this.getFeeItemurl+'/'+id,httpOptions);
    }
    getDeleteItems(deleteObject){
        return this.httpClient.post(this.deleteurl,deleteObject,httpOptions);
    }
    getItems() {
        return this.httpClient.get(this.url,httpOptions);
    }
    getActiveItems() {
        return this.httpClient.get(this.Activeurl+'/'+1,httpOptions);
    }
    getItemsDetail(id) {
        return this.httpClient.get(this.url+'/'+id,httpOptions);
    }

    insertfeeitemItems(feeitemData): Observable<any> {
        return this.httpClient.post(this.url, feeitemData,httpOptions);
    }

    updatefeeitemItems(feeitemData,id): Observable<any> {
        return this.httpClient.post(this.url+'/'+id, feeitemData,httpOptions);
       }
}