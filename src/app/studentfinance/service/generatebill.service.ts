import { Injectable } from '@angular/core';
import { HttpClientModule,HttpHeaders, HttpClient} from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({ 'X-Auth-Client': 'qazwsx123',
    'Content-Type': 'application/json' })
    
  };
  
@Injectable()

export class GeneratebillService {
    url: string = environment.api.base + environment.api.endPoints.generateStudentBill;
    urlinvoice: string = environment.api.base + environment.api.endPoints.generateStudentInvoice;
    generate : string = environment.api.base+environment.api.endPoints.generatebill;

    constructor(private httpClient: HttpClient) { 
        
    }
    
    getItems() {
        return this.httpClient.get(this.url,httpOptions);
    }

    getItemsDetail(generatebillData) {
        return this.httpClient.post(this.url,generatebillData,httpOptions);
    }
    downloadGeneratebill(generatebillData): Observable<any> {
        return this.httpClient.post(this.generate, generatebillData,httpOptions);
    }
    // downloadGeneratebillrReport(generatebillData): Observable<any> {
    //     return this.httpClient.post(this.generate, generatebillData,httpOptions);
    // }
    invoiceGenerate(generatebillData): Observable<any> {
        return this.httpClient.post(this.urlinvoice, generatebillData,httpOptions);
    }

    insertGeneratebillItems(generatebillData): Observable<any> {
        return this.httpClient.post(this.url, generatebillData,httpOptions);
    }

    updateGeneratebillItems(generatebillData,id): Observable<any> {
        return this.httpClient.put(this.url+'/'+id, generatebillData,httpOptions);
       }
}