import { Injectable } from '@angular/core';
import { HttpClientModule,HttpHeaders, HttpClient} from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({ 'X-Auth-Client': 'qazwsx123',
    'Content-Type': 'application/json' })
    
  };
  
@Injectable()

export class DebitinformationService {
    url: string = environment.api.base + environment.api.endPoints.debtInformation;
    Deleteurl: string = environment.api.base + environment.api.endPoints.deleteDebtInformationDetails;
    constructor(private httpClient: HttpClient) { 
        
    }
    
    getItems() {
        return this.httpClient.get(this.url,httpOptions);
    }
    getDeleteItems(deleteObject) {
        return this.httpClient.post(this.Deleteurl,deleteObject,httpOptions);
    }
    getItemsDetail(id) {
        return this.httpClient.get(this.url+'/'+id,httpOptions);
    }

    insertDebitinformationItems(debitinfoData): Observable<any> {
        return this.httpClient.post(this.url, debitinfoData,httpOptions);
    }

    updateDebitinformationItems(debitinfoData,id): Observable<any> {
        return this.httpClient.put(this.url+'/'+id, debitinfoData,httpOptions);
       }
}