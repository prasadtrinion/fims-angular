import { Injectable } from '@angular/core';
import { HttpClientModule,HttpHeaders, HttpClient} from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({ 'X-Auth-Client': 'qazwsx123',
    'Content-Type': 'application/json' })
    
  };
@Injectable()

export class BondapprovalService {

     url: string = environment.api.base + environment.api.endPoints.studentBondByApproval
     approvalPuturl: string = environment.api.base + environment.api.endPoints.approveOnePut
     approveoneurl: string = environment.api.base + environment.api.endPoints.rejectOne;
     approverefundurl: string = environment.api.base + environment.api.endPoints.approveRefund;
    constructor(private httpClient: HttpClient) {    
    }
    getItems() {
        return this.httpClient.get(this.url+'/'+2,httpOptions);
    }
    getItemsDetail(id) {
        return this.httpClient.get(this.url+'/'+id, httpOptions);
    }
    insertBondapprovalItems(approvaloneUpdate): Observable<any> {
        return this.httpClient.post(this.url, approvaloneUpdate,httpOptions);
    }
    updateBondapprovalItems(approvaloneUpdate): Observable<any> {
        return this.httpClient.post(this.approverefundurl, approvaloneUpdate,httpOptions);
    }
    rejectBondapproval(approvaloneUpdate): Observable<any> {
        return this.httpClient.post(this.approveoneurl, approvaloneUpdate,httpOptions);
    }
}