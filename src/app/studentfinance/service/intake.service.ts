import { Injectable } from '@angular/core';
import { HttpClientModule, HttpHeaders, HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({
        'X-Auth-Client': 'qazwsx123',
        'Content-Type': 'application/json'
    })

};

@Injectable()

export class IntakeService {
    intakeurl: string = environment.api.base + environment.api.endPoints.intake;
    deleteurl: string = environment.api.base + environment.api.endPoints.deleteIntakeDetails;
    // masterIntakeurl: string = environment.api.base + environment.api.endPoints.intake;

    constructor(private httpClient: HttpClient) {
    }

    getDeleteItem(deleteObject) {
        return this.httpClient.post(this.deleteurl, deleteObject, httpOptions);
    }
    getItems() {
        return this.httpClient.get(this.intakeurl, httpOptions);
    }

    getItemsDetail(id) {
        return this.httpClient.get(this.intakeurl + '/' + id, httpOptions);
    }

    getIntakeDetailsById(id) {
        return this.httpClient.get(this.intakeurl + '/' + id, httpOptions);
    }

    insertintakeItems(intakeData): Observable<any> {
        return this.httpClient.post(this.intakeurl, intakeData, httpOptions);
    }

    updateintakeItems(intakeData, id): Observable<any> {
        return this.httpClient.post(this.intakeurl +'/' + id, intakeData, httpOptions);
    }
}