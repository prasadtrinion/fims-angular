import { Injectable } from '@angular/core';
import { HttpClientModule,HttpHeaders, HttpClient} from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({ 'X-Auth-Client': 'qazwsx123',
    'Content-Type': 'application/json' })
    
  };
  
@Injectable()

export class SponserbillService {
    sponsorBillurl: string = environment.api.base + environment.api.endPoints.sponsorBill;
    urlREferenceNumber : string = environment.api.base + environment.api.endPoints.generateNumber;
    urlsponsorHasBills : string = environment.api.base + environment.api.endPoints.sponsorHasBills;
    sponsorStudentInvoicesulr : string = environment.api.base + environment.api.endPoints.sponsorStudentInvoices;

    constructor(private httpClient: HttpClient) { 
        
    }
    
    getItems() {
            return this.httpClient.get(this.sponsorBillurl,httpOptions);  
    }
    

    getItemsDetail(id) {
        return this.httpClient.get(this.sponsorBillurl+'/'+id,httpOptions);
    }

    getInvoiceDetailsById(id) {
            return this.httpClient.get(this.sponsorBillurl+'/'+id,httpOptions);
    }

    getListDetails(id){
        return this.httpClient.get(this.urlsponsorHasBills+'/'+id,httpOptions);

    }
    getListInvoices(id) {
        return this.httpClient.get(this.sponsorStudentInvoicesulr+'/'+id,httpOptions);
        
    }

    getBillNumber(type){
        return this.httpClient.post(this.urlREferenceNumber, type,httpOptions);

    }

    insertsponsorBillItems(sponserBillObject): Observable<any> {
        return this.httpClient.post(this.sponsorBillurl, sponserBillObject,httpOptions);
    }

    updatesponsorBillItems(sponserBillObject,id): Observable<any> {
        return this.httpClient.put(this.sponsorBillurl+'/'+id,sponserBillObject,httpOptions);
       }
}