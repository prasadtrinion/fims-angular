import { Injectable } from '@angular/core';
import { HttpClientModule,HttpHeaders, HttpClient} from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({ 'X-Auth-Client': 'qazwsx123',
    'Content-Type': 'application/json' })
    
  };
  
@Injectable()

export class BulkSponsorTaggingService {
    bulkSponsorurl: string = environment.api.base + environment.api.endPoints.bulkSponsorTagging;
    // urlREferenceNumber : string = environment.api.base + environment.api.endPoints.generateNumber;
    // urlsponsorHasBills : string = environment.api.base + environment.api.endPoints.sponsorHasBills;
    // sponsorStudentInvoicesulr : string = environment.api.base + environment.api.endPoints.sponsorStudentInvoices;

    constructor(private httpClient: HttpClient) { 
        
    }
    
    getItems() {
            return this.httpClient.get(this.bulkSponsorurl,httpOptions);  
    }
    getItemsDetail(sponserBillObject): Observable<any> {
        return this.httpClient.post(this.bulkSponsorurl, sponserBillObject,httpOptions);
    }
    insertbulkSponsorItems(savedObje): Observable<any> {
        return this.httpClient.post(this.bulkSponsorurl, savedObje,httpOptions);
    }

    updatebulkSposorItems(bulksponsorData,id): Observable<any> {
        return this.httpClient.put(this.bulkSponsorurl+'/'+id,bulksponsorData,httpOptions);
       }
}