import { Injectable } from '@angular/core';
import { HttpClientModule,HttpHeaders, HttpClient} from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({ 'X-Auth-Client': 'qazwsx123',
    'Content-Type': 'application/json' })
    
  };
  
@Injectable()

export class BondinformationService {
    url: string = environment.api.base + environment.api.endPoints.studentBond;
    generateRefundableStudentInvoiceUrl: string = environment.api.base + environment.api.endPoints.generateRefundableStudentInvoice;

    constructor(private httpClient: HttpClient) { 
        
    }
    
    getItems(id) {
        return this.httpClient.get(this.url+'/'+id,httpOptions);
    }
    getItemsFull(){
        return this.httpClient.get(this.url,httpOptions);

    }

    getItemsDetail(id) {
        return this.httpClient.get(this.url+'/'+id,httpOptions);
    }
    searchListDetails(searchListDetails){

        return this.httpClient.post(this.generateRefundableStudentInvoiceUrl,searchListDetails,httpOptions);
    }

    insertBondinformationItems(bondinformationData): Observable<any> {
        return this.httpClient.post(this.url, bondinformationData,httpOptions);
    }

    updateBondinformationItems(bondinformationData,id): Observable<any> {
        return this.httpClient.put(this.url+'/'+id, bondinformationData,httpOptions);
       }
}