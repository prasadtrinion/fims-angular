import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { TagstudentinsuranceService } from '../service/tagstudentinsurance.service'
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'Tagstudentinsurance',
    templateUrl: 'tagstudentinsurance.component.html'
})

export class TagstudentinsuranceComponent implements OnInit {
    tagstudentList =  [];
    tagstudentData = {};
    id: number;

    constructor(
        private TagstudentinsuranceService: TagstudentinsuranceService,
        private spinner: NgxSpinnerService,                

    ) { }

    ngOnInit() { 
        this.spinner.show();                                                
        this.TagstudentinsuranceService.getItems().subscribe(
            data => {
                this.spinner.hide();
                this.tagstudentList = data['result']['data'];
        }, error => {
            console.log(error);
        });
    }

    addTagstudentinsurance(){
           console.log(this.tagstudentData);
        this.TagstudentinsuranceService.insertTagstudentinsuranceItems(this.tagstudentData).subscribe(
            data => {
                this.tagstudentList = data['todos'];
            }, error => {
                console.log(error);
            });

    }
}