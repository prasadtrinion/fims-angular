import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { TagstudentinsuranceService } from '../../service/tagstudentinsurance.service'
import { StudentinsuranceService } from '../../service/studentinsurance.service'
import { DefinationmsService } from "../../service/definationms.service";
import { StudentService } from '../../service/student.service'
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'TagstudentinsuranceFormComponent',
    templateUrl: 'tagstudentinsuranceform.component.html'
})

export class TagstudentinsuranceFormComponent implements OnInit {
    tagstudentform: FormGroup;
    tagstudentList = [];
    tagstudentData = {};
    studentList = [];
    statusList = [];
    studentinsuranceList = [];
    id: number;
    ajaxCount: number;
    constructor(
        private TagstudentinsuranceService: TagstudentinsuranceService,
        private StudentinsuranceService: StudentinsuranceService,
        private DefinationmsService: DefinationmsService,
        private StudentService: StudentService,
        private spinner: NgxSpinnerService,
        private route: ActivatedRoute,
        private router: Router

    ) { }
    ngDoCheck() {
        const change = this.ajaxCount;
        if (this.ajaxCount == 0) {
            this.spinner.hide();
        }
    }
    ngOnInit() {
        this.tagstudentData['f050fstatus'] = 1;
        this.ajaxCount = 0
        this.spinner.show();
        //student drop down 
        this.ajaxCount++;
        this.StudentService.getActiveItems().subscribe(
            data => {
                this.ajaxCount--;
                this.studentList = data['result']['data'];
            }, error => {
                console.log(error);
            });

        //Insurance Drop down 
        this.ajaxCount++;
        this.StudentinsuranceService.getActiveItems().subscribe(
            data => {
                this.ajaxCount--;
                this.studentinsuranceList = data['result']['data'];
            }, error => {
                console.log(error);
            });

        // status Dropdown
        this.ajaxCount++;
        this.DefinationmsService.gettagstudentStatus('StudentTaggingStatus').subscribe(
            data => {
                this.ajaxCount--;
                this.statusList = data['result'];
            }, error => {
                console.log(error);
            });

        this.tagstudentform = new FormGroup({
            tagstudent: new FormControl('', Validators.required)
        });
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.ajaxCount++;
            this.TagstudentinsuranceService.getItemsDetail(this.id).subscribe(
                data => {
                    this.ajaxCount--;
                    this.tagstudentData = data['result']['data'][0];
                    this.tagstudentData['f050fstatus'] = parseInt(data['result']['data'][0]['f050fstatus']);
                    console.log(this.tagstudentData);

                    // console.log(this.tagstudentData[data['result'][0]['f050fStatus']]);
                }, error => {
                    console.log(error);
                });
        }

        // if (this.id > 0) {
        //     this.TagstudentinsuranceService.getItemsDetail(this.id).subscribe(
        //         data => {
        //             this.tagstudentData = data['result'];
        //             // converting from string to int for radio button default selection
        //             this.tagstudentData['f050fstatus'] = parseInt(data['result'][0]['f050fstatus']);
        //             console.log(this.tagstudentData);
        //         }, error => {
        //             console.log(error);
        //         });
        // }


        console.log(this.id);
    }


    addTagstudentinsurance() {
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        console.log(this.tagstudentData);

        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.TagstudentinsuranceService.updateTagstudentinsuranceItems(this.tagstudentData, this.id).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert("Policy Number Already Exists");
                    }
                    else {
                        this.router.navigate(['studentfinance/tagstudentinsurance']);
                        alert("Setup Insurance Policy has been Updated Successfully");
                    }
                }, error => {
                    console.log(error);
                    alert("Policy Number Already Exists");

                });


        } else {
            this.TagstudentinsuranceService.insertTagstudentinsuranceItems(this.tagstudentData).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert("Policy Number Already Exists");
                    }
                    else {
                        this.router.navigate(['studentfinance/tagstudentinsurance']);
                        alert("Setup Insurance Policy has been Added Successfully")
                    }
                }, error => {
                    console.log(error);
                });

        }

    }
}