import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SubjectService } from '../service/subject.service'


@Component({
    selector: 'Subject',
    templateUrl: 'subject.component.html'
})

export class SubjectComponent implements OnInit {
    subjectList =  [];
    subjectData = {};
    id: number;

    constructor(
        
        private SubjectService: SubjectService

    ) { }

    ngOnInit() { 
        
        this.SubjectService.getItems().subscribe(
            data => {
                this.subjectList = data['result']['data'];
        }, error => {
            console.log(error);
        });
    }

    addFeeitem(){
           console.log(this.subjectData);
        this.SubjectService.insertsubjectItems(this.subjectData).subscribe(
            data => {
                this.subjectList = data['todos'];
            }, error => {
                console.log(error);
            });

    }
}