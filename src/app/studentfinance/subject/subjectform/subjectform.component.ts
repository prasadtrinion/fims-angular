import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { SubjectService } from '../../service/subject.service'
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
    selector: 'SubjectFormComponent',
    templateUrl: 'subjectform.component.html'
})

export class SubjectFormComponent implements OnInit {
    subjectform: FormGroup;
    subjectList = [];
    subjectData = {};
    id: number;
    
    constructor(
        private SubjectService: SubjectService,
        private route: ActivatedRoute,
        private router: Router

    ) { }

    ngOnInit() {
        this.subjectData['f047fstatus'] = 1;

        this.subjectform = new FormGroup({
            subject: new FormControl('', Validators.required)
        });
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        console.log(this.subjectData);
       

        if (this.id > 0) {
            this.SubjectService.getItemsDetail(this.id).subscribe(
                data => {
                    this.subjectData = data['result']['0'];
                  
                   this.subjectData['f047fstatus'] = parseInt(data['result'][0]['f047fstatus']);
                console.log(this.subjectData);
                }, error => {
                    console.log(error);
                });
        }


        console.log(this.id);
    }

    addSubject() {
        console.log(this.subjectData);
      

        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.SubjectService.updatesubjectItems(this.subjectData, this.id).subscribe(
                data => {
                    this.router.navigate(['studentfinance/subject']);
                }, error => {
                    console.log(error);
                });

        } else {
            this.SubjectService.insertsubjectItems(this.subjectData).subscribe(
                data => {
                    this.router.navigate(['studentfinance/subject']);

                }, error => {
                    console.log(error);
                });

        }

    }
    


}