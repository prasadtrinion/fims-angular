import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SponsorService } from '../service/sponsor.service'
import { NgxSpinnerService } from 'ngx-spinner';
@Component({
    selector: 'Sponsor',
    templateUrl: 'sponsor.component.html'
})

export class SponsorComponent implements OnInit {
    sponsorList =  [];
    sponsorData = {};
    id: number;

    constructor(
        private SponsorService: SponsorService,
        private spinner: NgxSpinnerService,

    ) { }

    ngOnInit() { 
        this.spinner.show();                                                                
        this.SponsorService.getItems().subscribe(
            data => {
                this.spinner.hide();
                this.sponsorList = data['result']['data'];
        }, error => {
            console.log(error);
        });
    }

    addSponsor(){
           console.log(this.sponsorData);
        this.SponsorService.insertsponsorItems(this.sponsorData).subscribe(
            data => {
                this.sponsorList = data['todos'];
            }, error => {
                console.log(error);
            });

    }
}