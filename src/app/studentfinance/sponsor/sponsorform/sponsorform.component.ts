import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { SponsorService } from '../../service/sponsor.service'
import { DefinationmsService } from '../../service/definationms.service'
import { FundService } from '../../../generalsetup/service/fund.service';
import { ActivitycodeService } from '../../../generalsetup/service/activitycode.service';
import { AccountcodeService } from '../../../generalsetup/service/accountcode.service';
import { DepartmentService } from '../../../generalsetup/service/department.service';
import { CountryService } from '../../../generalsetup/service/country.service';
import { StateService } from '../../../generalsetup/service/state.service';
import { FeecategoryService } from '../../service/feecategory.service';
import { FeeitemService } from '../../service/feeitem.service';
import { FeetypeService } from "../../service/feetype.service";
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'SponsorFormComponent',
    templateUrl: 'sponsorform.component.html'
})

export class SponsorFormComponent implements OnInit {
    sponsorform: FormGroup;
    sponsorList = [];
    sponsorData = {};
    categoryList = [];
    categoryData = {};
    paymentList = [];
    jobtypeList = [];
    sponsortypeList = [];
    glcodeList = [];
    recivablestatusList = [];
    sponsorDataheader = {};
    feeItemList = [];
    ssponsorList = [];
    DeptList = [];
    fsponsortype = [];
    fundList = [];
    accountcodeList = [];
    activitycodeList = [];
    fstatus = [];
    feecategoryList = [];
    countryList = [];
    stateList = [];
    deleteList = [];
    id: number;
    name: string;
    ajaxCount: number;
    showLastRow: boolean;
    showLastRowMinus: boolean;
    showLastRowPlus: boolean;
    listshowLastRowMinus: boolean;
    listshowLastRowPlus: boolean;
    program: boolean;
    constructor(
        private SponsorService: SponsorService,
        private DefinationmsService: DefinationmsService,
        private FeeitemService: FeeitemService,
        private ActivitycodeService: ActivitycodeService,
        private AccountcodeService: AccountcodeService,
        private DepartmentService: DepartmentService,
        private FundService: FundService,
        private CountryService: CountryService,
        private StateService: StateService,
        private FeecategoryService: FeecategoryService,
        private FeetypeService: FeetypeService,
        private spinner: NgxSpinnerService,
        private route: ActivatedRoute,
        private router: Router
    ) { }

    ngDoCheck() {
        const change = this.ajaxCount;
        if (this.ajaxCount == 0 && this.sponsorList.length > 0) {
            this.ajaxCount = 20;
            this.showIcons();
            this.spinner.hide();
        }
        if (this.ajaxCount == 0) {
            this.spinner.hide();
        }
    }


    // editFunction(){
    //     this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
    //     if(this.id>0) {
    //         this.SponsorService.getItemsDetail(this.id).subscribe(
    //             data => {
    //                 this.sponsorList = data['result'];
    //                 this.sponsorDataheader['f063ffirstName'] = data['result'][0]['f063ffirstName'];
    //                 this.sponsorDataheader['f063flastName'] = data['result'][0]['f063flastName'];
    //                 this.sponsorDataheader['f063femail'] = data['result'][0]['f063femail'];
    //                 this.sponsorDataheader['f063ffaxNumber'] = data['result'][0]['f063ffaxNumber'];
    //                 this.sponsorDataheader['f063fpostCode'] = data['result'][0]['f063fpostCode'];
    //                 this.sponsorDataheader['f063ffund'] = data['result'][0]['f063ffund'];
    //                 this.sponsorDataheader['f063fdepartment'] = data['result'][0]['f063fdepartment'];
    //                 this.sponsorDataheader['f063factivity'] = data['result'][0]['f063factivity'];
    //                 this.sponsorDataheader['f063faccount'] = data['result'][0]['f063faccount'];
    //                 this.sponsorDataheader['f063faddress1'] = data['result'][0]['f063faddress1'];
    //                 this.sponsorDataheader['f063faddress2'] = data['result'][0]['f063faddress2'];
    //                 this.sponsorDataheader['f063fcontactNo'] = data['result'][0]['f063fcontactNo'];
    //                 this.sponsorDataheader['f063fcountry'] = data['result'][0]['f063fcountry'];
    //                 this.sponsorDataheader['f063fstate'] = data['result'][0]['f063fstate'];
    //                 this.sponsorDataheader['f063fsponsorshipType'] = data['result'][0]['f063fsponsorshipType'];
    //                 this.sponsorDataheader['f063fpaymentStatus'] = data['result'][0]['f063fpaymentStatus'];
    //                 this.sponsorDataheader['f063fjobType'] = data['result'][0]['f063fjobType'];
    //                 this.sponsorDataheader['f063fsponsorType'] = data['result'][0]['f063fsponsorType'];
    //                 this.sponsorDataheader['f063freceivableStatus'] = data['result'][0]['f063freceivableStatus'];
    //             }, error => {
    //                 console.log(error);
    //                 this.showIcons();
    //             });
    //     }
    // }
    deletemodule() {
        this.showLastRow = true;
        this.showIcons();
    }

    showIcons() {
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.sponsorList.length > 0) {
            this.showLastRowMinus = true;
            this.showLastRowPlus = true;
            this.listshowLastRowMinus = true;
        }

        if (this.sponsorList.length > 1) {
            if (this.showLastRow == false) {
                this.listshowLastRowPlus = false;
                this.listshowLastRowMinus = true;
                this.showLastRowPlus = true;

            } else {
                this.listshowLastRowPlus = true;
                this.listshowLastRowMinus = false;

            }
        }
        if (this.sponsorList.length == 1 && this.showLastRow == false) {
            this.listshowLastRowMinus = true;
            this.listshowLastRowPlus = false;

        }
        if (this.sponsorList.length == 1 && this.showLastRow == true) {
            this.listshowLastRowMinus = false;
            this.listshowLastRowPlus = true;

        }
        if (this.sponsorList.length == 0 && this.showLastRow == false) {
            this.showLastRowMinus = false;
            this.showLastRowPlus = true;
        }
        if (this.sponsorList.length == 0 && this.showLastRow == true) {
            this.showLastRowMinus = true;
            this.showLastRowPlus = false;
        }
        console.log(this.sponsorList.length);
        console.log("Show last row" + this.showLastRow);
        console.log("showLastRowMinus" + this.showLastRowMinus);
        console.log("showLastRowPlus" + this.showLastRowPlus);
        console.log("listshowLastRowMinus" + this.listshowLastRowMinus);
        console.log("listshowLastRowPlus" + this.listshowLastRowPlus);
    }
    showNewRow() {
        var confirmPop = confirm("Do you want to Add?");
        if (confirmPop == false) {
            return false;
        }
        this.showLastRow = false;
        this.showIcons();
    }
    ngOnInit() {
        this.ajaxCount = 0;
        this.spinner.show();
        this.sponsorData['f063fallowSecond'] = 1;
        this.sponsorform = new FormGroup({
            feeitem: new FormControl('', Validators.required)
        });
        let stafftypeobj = {};
        stafftypeobj['name'] = 'Full Sponsor';
        stafftypeobj['id'] = 1;
        this.fsponsortype.push(stafftypeobj);
        stafftypeobj = {};
        stafftypeobj['name'] = 'Partial';
        stafftypeobj['id'] = 2;
        this.fsponsortype.push(stafftypeobj);
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id < 1) {
            this.showLastRow = false;
            this.showLastRowMinus = false;
            this.showLastRowPlus = true;
        } else {
            this.showLastRow = true;

        }

        //Department dropdown
        this.ajaxCount++;
        this.DepartmentService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.DeptList = data['result']['data'];
            }
        );
        //Fee Category dropdown
        this.ajaxCount++;
        this.FeecategoryService.getActive().subscribe(
            data => {
                this.ajaxCount--;
                this.feecategoryList = data['result'];
            }
        );


        //Activity dropdown
        this.ajaxCount++;
        this.ActivitycodeService.getItemsByLevel().subscribe(
            data => {
                this.ajaxCount--;
                this.activitycodeList = data['result']['data'];
            }
        );


        //Account dropdown
        this.ajaxCount++;
        this.AccountcodeService.getItemsByLevel().subscribe(
            data => {
                this.ajaxCount--;
                this.accountcodeList = data['result']['data'];
            }
        );



        //Fund dropdown
        this.ajaxCount++;
        this.FundService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.fundList = data['result']['data'];
            }
        );


        //Department dropdown
        this.ajaxCount++;
        this.DepartmentService.getActiveItems().subscribe(
            data => {
                this.ajaxCount--;
                this.DeptList = data['result']['data'];
            }
        );


        //Activity dropdown
        this.ajaxCount++;
        this.ActivitycodeService.getItemsByLevel().subscribe(
            data => {
                this.ajaxCount--;
                this.activitycodeList = data['result']['data'];
            }
        );


        //Account dropdown
        this.ajaxCount++;
        this.AccountcodeService.getItemsByLevel().subscribe(
            data => {
                this.ajaxCount--;
                this.accountcodeList = data['result']['data'];
            }
        );



        //Fund dropdown
        this.ajaxCount++;
        this.FundService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.fundList = data['result']['data'];
            }
        );



        //fee tyoe drop down 
        this.ajaxCount++;

        this.FeetypeService.getActiveItems().subscribe(
            data => {
                this.ajaxCount--;
                this.feeItemList = data['result']['data'];
            }, error => {
                console.log(error);
            });


        // sponsorship type dropdown
        this.ajaxCount++;

        this.DefinationmsService.getSponsor('Category').subscribe(
            data => {
                this.ajaxCount--;
                this.ssponsorList = data['result']['data'];
                console.log(this.ssponsorList);
            }, error => {
                console.log(error);
            });


        //Payment status dropdown
        this.ajaxCount++;

        this.DefinationmsService.getPayment('PaymentStatus').subscribe(
            data => {
                this.ajaxCount--;
                this.paymentList = data['result'];
            }, error => {
                console.log(error);
            });



        //Job Type 
        this.ajaxCount++;
        this.DefinationmsService.getJobType('JobType').subscribe(
            data => {
                this.ajaxCount--;
                this.jobtypeList = data['result'];
            }, error => {
                console.log(error);
            });
        console.log(this.id);

        // sponsor type 

        this.ajaxCount++;
        this.DefinationmsService.getSponsorType('SponsorType').subscribe(
            data => {
                this.ajaxCount--;
                this.sponsortypeList = data['result'];
            }, error => {
                console.log(error);
            });
        console.log(this.id);

        //status Of Receivable  

        this.ajaxCount++;
        this.DefinationmsService.getReceivableStatus('ReceivableStatus').subscribe(
            data => {
                this.ajaxCount--;
                this.recivablestatusList = data['result'];
            }, error => {
                console.log(error);
            });
        console.log(this.id);


        //status Of Receivable  

        this.ajaxCount++;
        this.CountryService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.countryList = data['result']['data'];
            }, error => {
                console.log(error);
            });
        if (this.id > 0) {
            this.ajaxCount++;
            this.SponsorService.getItemsDetail(this.id).subscribe(
                data => {
                    this.ajaxCount--;
                    this.sponsorList = data['result'];
                    this.sponsorDataheader['f063fcode'] = data['result'][0]['f063fcode'];
                    this.sponsorDataheader['f063ffirstName'] = data['result'][0]['f063ffirstName'];
                    this.sponsorDataheader['f063flastName'] = data['result'][0]['f063flastName'];
                    this.sponsorDataheader['f063femail'] = data['result'][0]['f063femail'];
                    // this.sponsorDataheader['f063fcontactNo'] = data['result'][0]['f063fcontactNo'];
                    this.sponsorDataheader['f063ffaxNumber'] = data['result'][0]['f063ffaxNumber'];
                    this.sponsorDataheader['f063finchargeOfficer'] = data['result'][0]['f063finchargeOfficer'];
                    this.sponsorDataheader['f063fpostCode'] = data['result'][0]['f063fpostCode'];
                    this.sponsorDataheader['f063ffund'] = data['result'][0]['f063ffund'];
                    this.sponsorDataheader['f063fdepartment'] = data['result'][0]['f063fdepartment'].toString();
                    this.sponsorDataheader['f063factivity'] = data['result'][0]['f063factivity'];
                    this.sponsorDataheader['f063faccount'] = data['result'][0]['f063faccount'];
                    this.sponsorDataheader['f063faddress1'] = data['result'][0]['f063faddress1'];
                    this.sponsorDataheader['f063faddress2'] = data['result'][0]['f063faddress2'];
                    this.sponsorDataheader['f063fcontactNo'] = data['result'][0]['f063fcontactNo'];
                    this.sponsorDataheader['f063fcountry'] = data['result'][0]['f063fcountry'];
                    this.sponsorDataheader['f063fstate'] = data['result'][0]['f063fstate'];
                    this.sponsorDataheader['f063fsponsorshipType'] = data['result'][0]['f063fsponsorshipType'];
                    this.sponsorDataheader['f063fpaymentStatus'] = data['result'][0]['f063fpaymentStatus'];
                    this.sponsorDataheader['f063fjobType'] = data['result'][0]['f063fjobType'];
                    this.sponsorDataheader['f063fsponsorType'] = data['result'][0]['f063fsponsorType'];
                    this.sponsorDataheader['f063freceivableStatus'] = data['result'][0]['f063freceivableStatus'];
                    this.onCountryChange();
                    this.showIcons();
                }, error => {
                    console.log(error);
                });
        }

    }
    onCountryChange() {

        if (this.sponsorDataheader['f063fcountry'] == undefined) {
            this.sponsorDataheader['f063fcountry'] = 0;
        }
        this.ajaxCount++;
        this.StateService.getStates(this.sponsorDataheader['f063fcountry']).subscribe(
            data => {
                this.ajaxCount--;
                this.stateList = data['result'];
                let other = {
                    'f012fid': "1",
                    "f012fshortName": "SH",
                    "f012fstateName": "Others"

                };
                this.stateList.push(other);

            }
        );
    }
    saveSponsor() {
        if (this.sponsorDataheader['f063ffirstName'] == undefined) {
            alert("Enter First Name");
            return false;
        }
        if (this.sponsorDataheader['f063flastName'] == undefined) {
            alert("Enter Last Name");
            return false;
        }
        if (this.sponsorDataheader['f063femail'] == undefined) {
            alert("Enter Email");
            return false;
        }
        if (this.sponsorDataheader['f063ffaxNumber'] == undefined) {
            alert("Enter FAX Number");
            return false;
        }
        if (this.sponsorDataheader['f063fcontactNo'] == undefined) {
            alert("Enter Contact Number");
            return false;
        }
        if (this.sponsorDataheader['f063ffirstName'] == undefined) {
            alert("Enter First Name");
            return false;
        }
        if (this.sponsorDataheader['f063faddress1'] == undefined) {
            alert("Enter Address 1");
            return false;
        }
        if (this.sponsorDataheader['f063faddress2'] == undefined) {
            alert("Enter Address 2");
            return false;
        }
        if (this.sponsorDataheader['f063fpostCode'] == undefined) {
            alert("Enter Post Code");
            return false;
        }
        if (this.sponsorDataheader['f063fcountry'] == undefined) {
            alert("Enter Country");
            return false;
        }
        if (this.sponsorDataheader['f063fstate'] == undefined) {
            alert("Enter State");
            return false;
        }
        if (this.sponsorDataheader['f063ffund'] == undefined) {
            alert("Select Fund");
            return false;
        }
        if (this.sponsorDataheader['f063factivity'] == undefined) {
            alert("Select Activity Code");
            return false;
        }
        if (this.sponsorDataheader['f063fdepartment'] == undefined) {
            alert("Select Department");
            return false;
        }
        if (this.sponsorDataheader['f063faccount'] == undefined) {
            alert("Select Account Code");
            return false;
        }
        if (this.sponsorDataheader['f063fdepartment'] == undefined) {
            alert("Select Department");
            return false;
        }
        if (this.sponsorDataheader['f063fsponsorshipType'] == undefined) {
            alert("Select Sponsorship Type");
            return false;
        }
        if (this.sponsorDataheader['f063freceivableStatus'] == undefined) {
            alert("Select Status Of Invoice");
            return false;
        }
        if (this.sponsorDataheader['f063fpaymentStatus'] == undefined) {
            alert("Select Payment Status");
            return false;
        }
        if (this.sponsorDataheader['f063fjobType'] == undefined) {
            alert("Select Job Type");
            return false;
        }
        if (this.sponsorDataheader['f063fsponsorType'] == undefined) {
            alert("Select Sponsor Type");
            return false;
        }
        if (this.sponsorDataheader['f063finchargeOfficer'] == undefined) {
            alert("Enter Incharge Officer");
            return false;
        }
        if (this.showLastRow == false) {
            var addactivities = this.addSponsorlist();
            if (addactivities == false) {
                return false;
            }

        }
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        let sponsorObject = {};
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        console.log(this.id);
        if (this.id > 0) {
            sponsorObject['f063fid'] = this.id;
        }
        sponsorObject['f063ffirstName'] = this.sponsorDataheader['f063ffirstName'];
        sponsorObject['f063ffaxNumber'] = this.sponsorDataheader['f063ffaxNumber'];
        sponsorObject['f063fcountry'] = this.sponsorDataheader['f063fcountry'];
        sponsorObject['f063fstate'] = this.sponsorDataheader['f063fstate'];
        sponsorObject['f063femail'] = this.sponsorDataheader['f063femail'];
        sponsorObject['f063finchargeOfficer'] = this.sponsorDataheader['f063finchargeOfficer'];
        sponsorObject['f063fpostCode'] = this.sponsorDataheader['f063fpostCode'];
        sponsorObject['f063flastName'] = this.sponsorDataheader['f063flastName'];

        sponsorObject['f063faddress1'] = this.sponsorDataheader['f063faddress1'];
        sponsorObject['f063faddress2'] = this.sponsorDataheader['f063faddress2'];
        sponsorObject['f063fcontactNo'] = this.sponsorDataheader['f063fcontactNo'];

        sponsorObject['f063ffund'] = this.sponsorDataheader['f063ffund'];
        sponsorObject['f063fdepartment'] = this.sponsorDataheader['f063fdepartment'];
        sponsorObject['f063factivity'] = this.sponsorDataheader['f063factivity'];
        sponsorObject['f063faccount'] = this.sponsorDataheader['f063faccount'];
        sponsorObject['f063fsponsorshipType'] = this.sponsorDataheader['f063fsponsorshipType'];
        sponsorObject['f063fpaymentStatus'] = this.sponsorDataheader['f063fpaymentStatus'];

        sponsorObject['f063fjobType'] = this.sponsorDataheader['f063fjobType'];
        sponsorObject['f063fsponsorType'] = this.sponsorDataheader['f063fsponsorType'];
        sponsorObject['f063freceivableStatus'] = this.sponsorDataheader['f063freceivableStatus'];
        sponsorObject['f063fstatus'] = 1;
        sponsorObject['sponsor-details'] = this.sponsorList;
        console.log(sponsorObject);
        if (this.sponsorList.length > 0) {

        } else {
            alert("Fee type is mandatory to be added");
            return false;
        }

        if (this.id > 0) {
            this.SponsorService.updatesponsorItems(sponsorObject, this.id).subscribe(
                data => {

                    this.router.navigate(['studentfinance/sponsor']);
                    alert("Sponsor has been Updated Sucessfully ! ");

                }, error => {
                    console.log(error);
                });
        } else {
            this.SponsorService.insertsponsorItems(sponsorObject).subscribe(
                data => {


                    this.router.navigate(['studentfinance/sponsor']);
                    alert("Sponsor has been Added Sucessfully ! ");

                }, error => {
                    console.log(error);
                });
        }
    }
    deleteSponsorlist(object) {
        var cnf = confirm("Do you really want to delete");
        if (cnf == true) {
            var index = this.sponsorList.indexOf(object);
            this.deleteList.push(this.sponsorList[index]['f063fidDetails']);
            let deleteObject = {};
            deleteObject['id'] = this.deleteList;
            this.SponsorService.getdeleteItems(deleteObject).subscribe(
                data => {
                }, error => {
                    console.log(error);
                })
            if (index > -1) {
                this.sponsorList.splice(index, 1);
            }
            this.showIcons();
        }
    }


    addSponsorlist() {
        console.log(this.sponsorData);
        if (this.sponsorDataheader['f063fsponsorType'] == "Partial") {
            if (this.sponsorData['f063fidFeeType'] == undefined) {
                alert(" Select Fee Type");
                return false;
            }
            if (this.sponsorData['f063ffeeCategory'] == undefined) {
                alert(" Select Fee Category");
                return false;
            }
            if (this.sponsorData['f063fsponsorCode'] == undefined) {
                alert(" Select Sponsor Code");
                return false;
            }
            if (this.sponsorData['f063fallowSecond'] == undefined) {
                alert(" Select Allow Second Sponsor");
                return false;
            }
            var confirmPop = confirm("Do you want to Add?");
            if (confirmPop == false) {
                return false;
            }
        }
        if (this.sponsorDataheader['f063fsponsorType'] == 'Partial') {
            var confirmPop = confirm("Do you want to Add?");
            if (confirmPop == false) {
                return false;
            }
        }
        var dataofCurrentRow = this.sponsorData;
        this.sponsorData = {};
        this.sponsorList.push(dataofCurrentRow);
        this.showIcons();
    }
    onlyNumberKey(event) {
        if (event.charCode == 46) {
        } else {
            return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
        }
    }
    maximize() {
        $("#myModal").addClass("modal");
        $("#overlay1").show();
        $(".maximize-icon").addClass("hidden");
        $(".minimize-icon").removeClass("hidden");
        $(".modal-backdrop").show();

    }
    minimize() {
        $("#myModal").removeClass("modal");
        $("#overlay1").hide();
        $(".maximize-icon").removeClass("hidden");
        $(".minimize-icon").addClass("hidden");
        $(".modal-backdrop").hide();
        $('#myModal').show();

    }
}