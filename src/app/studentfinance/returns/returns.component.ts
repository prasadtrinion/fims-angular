import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ReturnService } from '../service/return.service'
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'Returns',
    templateUrl: 'returns.component.html'
})

export class ReturnsComponent implements OnInit {
    returnList =  [];
    returnData = {};
    studentList=[];
    studentId: number;
    studentName;
    id: number;

    constructor(
        private spinner: NgxSpinnerService,                
        private ReturnService: ReturnService,
        private route: ActivatedRoute,
        private router: Router,

    ) { }

    ngOnInit() {
        this.spinner.show();                                        
        this.returnData['studentId'] = parseInt(sessionStorage.getItem('StudentId'));
        this.studentName = sessionStorage.getItem('StudentName');
        this.returnData['f118fstatus'] = 1;
      
        
        this.ReturnService.getItems().subscribe(
            data => {
                this.spinner.hide();
                let activityData = [];
                activityData = data['result']['data'];
                console.log("asdf");
                for (var i = 0; i < activityData.length; i++) {
                    if (activityData[i]['f118fapprovalStatus'] == 0) {
                        activityData[i]['f118fapprovalStatus'] = 'Pending';
                    } else if (activityData[i]['f118fapprovalStatus'] == 1) {
                        activityData[i]['f118fapprovalStatus'] = 'Approved';
                    } else {
                        activityData[i]['f118fapprovalStatus'] = 'Rejected';
                    }
                }
                this.returnList = activityData;
        }, error => {
            console.log(error);
        });
    }

    addReturns(){
           console.log(this.returnData);
        this.ReturnService.insertReturnItems(this.returnData).subscribe(
            data => {
                console.log(data);
                this.studentList = data['result']['data'];
            }, error => {
                console.log(error);
            });
        

        // console.log(this.id);

        if (this.id > 0) {
            this.ReturnService.getItemsDetail(this.id).subscribe(
                data => {
                    this.returnData = data['result'][0];

                    // converting from string to int for radio button default selection
                    this.returnData['f118fstatus'] = parseInt(data['result'][0]['f118fstatus']);     
                    console.log(this.returnData);
                }, error => {
                    console.log(error);
                });
        }
        console.log(this.id);
    
        console.log(this.returnData);
        // this.feeitemData['f015fupdatedBy'] = 1;

        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.ReturnService.updateReturnItems(this.returnData, this.id).subscribe(
                data => {
                    this.router.navigate(['studentfinance/returns']);
                }, error => {
                    console.log(error);
                });

        } else {
            this.ReturnService.insertReturnItems(this.returnData).subscribe(
                data => {
                    this.router.navigate(['studentfinance/returns']);

                this.returnList = data['todos'];
            }, error => {
                console.log(error);
            });

    }
}
}