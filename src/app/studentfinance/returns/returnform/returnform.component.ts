import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { ReturnService } from '../../service/return.service'
import { StudentService } from '../../service/student.service'
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'ReturnFormComponent',
    templateUrl: 'returnform.component.html'
})

export class ReturnFormComponent implements OnInit {
    returnform: FormGroup;
    returnList = [];
    returnData = {};
    studentList = [];
    id: number;
    ajaxCount: number;
    viewDisabled: boolean;
    saveDisabled: boolean;
    idview: number;
    constructor(
        private ReturnService: ReturnService,
        private StudentService: StudentService,
        private spinner: NgxSpinnerService,
        private route: ActivatedRoute,
        private router: Router

    ) { }
    ngDoCheck() {
        const change = this.ajaxCount;
        if (this.ajaxCount == 0) {
            this.spinner.hide();
        }
    }
    ngOnInit() {
        this.ajaxCount = 0
        this.spinner.show();
        this.returnData['f118fstatus'] = 1;
        this.viewDisabled = false;
        this.returnform = new FormGroup({
            feeitem: new FormControl('', Validators.required)
        });
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.route.paramMap.subscribe((data) => this.idview = + data.get('idview'));
        //student drop down 
        this.ajaxCount++;
        this.StudentService.getActiveItems().subscribe(
            data => {
                this.ajaxCount--;
                this.studentList = data['result']['data'];
            }, error => {
                console.log(error);
            });
        if (this.id > 0) {
            this.ajaxCount++;
            this.ReturnService.getItemsDetail(this.id).subscribe(
                data => {
                    this.ajaxCount--;
                    this.returnData = data['result'][0];

                    // converting from string to int for radio button default selection
                    this.returnData['f118fstatus'] = parseInt(data['result'][0]['f118fstatus']);
                    console.log(this.returnData);
                    if (data['result'][0]['f118fapprovalStatus'] == 1 || data['result'][0]['f118fapprovalStatus'] == 2) {
                        this.viewDisabled = true;
                        $("#target input,select").prop("disabled", true);
                        // $("#target1 input,select").prop("disabled", true);
                    }
                    if (this.idview == 2) {
                        this.viewDisabled = true;
                        $("#target input,select").prop("disabled", true);
                    }
                }, error => {
                    console.log(error);
                });
        }
        console.log(this.id);
    }

    ConvertToFloat(val) {
        return parseFloat(val);
    }

    addReturns() {
        console.log(this.returnData);
        // this.feeitemData['f015fupdatedBy'] = 1;
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.returnData['f118famount'] = this.ConvertToFloat(this.returnData['f118famount']).toFixed(2);
        if (this.id > 0) {
            this.ReturnService.updateReturnItems(this.returnData, this.id).subscribe(
                data => {
                    this.router.navigate(['studentfinance/returns']);
                    alert("Returns has been Updated Successfully");
                }, error => {
                    console.log(error);
                });

        } else {
            this.ReturnService.insertReturnItems(this.returnData).subscribe(
                data => {
                    this.router.navigate(['studentfinance/returns']);
                    alert("Returns has been Added Successfully");

                }, error => {
                    console.log(error);
                });

        }

    }
    onlyNumberKey(event) {
        //console.log(event);
        if (event.charCode == 46) {

        } else {
            return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
        }
    }
}