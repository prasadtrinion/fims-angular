import { Injector } from '@angular/core';
import { Component, OnInit, ViewChild } from '@angular/core';
import { BulkSponsorTaggingService } from "../service/bulksponsortagging.service";
import { IntakeService } from "../service/intake.service";
import { ProgrammeService } from "../service/programme.service";
import { SponsorService } from "../service/sponsor.service";
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { importExpr } from '@angular/compiler/src/output/output_ast';

@Component({
    selector: 'BulksponsortaggingComponent',
    templateUrl: 'bulksponsortagging.component.html'
})

export class BulksponsortaggingFormComponent implements OnInit {
    bulksponsorList = [];
    bulksponsorData = {};
    intakeList = [];
    programmeList = [];
    sponsorList = [];
    ajaxCount: number;
    id: number;
    constructor(

        private BulkSponsorTaggingService: BulkSponsorTaggingService,
        private IntakeService: IntakeService,
        private ProgrammeService: ProgrammeService,
        private SponsorService: SponsorService,
        private route: ActivatedRoute,
        private router: Router,
    ) { }

    ngOnInit() {

        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        //Intake drop down 
        this.ajaxCount++;
        this.IntakeService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.intakeList = data['result']['data'];
            }, error => {
                console.log(error);
            });

        //Programme drop down 
        this.ajaxCount++;

        this.ProgrammeService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.programmeList = data['result']['data'];
            }, error => {
                console.log(error);
            });

        //Sponsor drop down 
        this.ajaxCount++;

        this.SponsorService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.sponsorList = data['result']['data'];
            }, error => {
                console.log(error);
            });

    }
    getList() {

        let bulkSponsorObject = {};
        bulkSponsorObject['sponsor'] = this.bulksponsorData['sponsor'];
        bulkSponsorObject['intake'] = this.bulksponsorData['intake'];
        bulkSponsorObject['program'] = this.bulksponsorData['program'];
        this.BulkSponsorTaggingService.getItemsDetail(bulkSponsorObject).subscribe(
            data => {
                this.bulksponsorList = data['result']['data'];
                console.log(this.bulksponsorData);

            },
            error => {
                console.log(error);
            });
    }

    sendtheStudentSponsorDetails() {
        if (this.bulksponsorData['sponsor'] == '') {
            alert("Select Sponsor");
            return false;
        }
        let studentSelectedArray = [];
        for (var i = 0; i < this.bulksponsorList.length; i++) {
            if (this.bulksponsorList[i]['selectedStudents'] == true) {
                studentSelectedArray.push(this.bulksponsorList[i]['f060fid']);
            }
        }

        let savedObje = {};
        savedObje['idStudent'] = studentSelectedArray;
        savedObje['sponsor'] = this.bulksponsorData['sponsor'];
        savedObje['intake'] = this.bulksponsorData['intake'];
        savedObje['program'] = this.bulksponsorData['program'];

        console.log(savedObje);
        this.BulkSponsorTaggingService.insertbulkSponsorItems(savedObje).subscribe(
            data => {
                this.router.navigate(['studentfinance/bulksponsortagging']);

            }, error => {
                console.log(error);
            });
    }
}