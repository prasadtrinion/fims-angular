import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { FeetypeService } from "../../service/feetype.service";
import { ActivatedRoute, Router } from '@angular/router';
import { FeecategoryService } from '../../service/feecategory.service'
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'FeetypeFormComponent',
    templateUrl: 'feetypeform.component.html'
})

export class FeetypeFormComponent implements OnInit {
    feetypeList = [];
    feetypeData = {};
    feecategoryList = [];
    id: number;
    ajaxCount: number;
    constructor(
        private FeetypeService: FeetypeService,
        private FeecategoryService: FeecategoryService,
        private spinner: NgxSpinnerService,
        private route: ActivatedRoute,
        private router: Router
    ) { }
    ngDoCheck() {
        const change = this.ajaxCount;
        if (this.ajaxCount == 0) {
            this.spinner.hide();
        }
    }
    ngOnInit() {
        this.ajaxCount = 0;
        this.spinner.show();
        this.feetypeData['f120fstatus'] = 1;
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        console.log(this.id);
        //fee category drop down 
        this.ajaxCount++;
        this.FeecategoryService.getActive().subscribe(
            data => {
                this.ajaxCount--;
                this.feecategoryList = data['result'];
            }, error => {
                console.log(error);
            });
        if (this.id > 0) {
            this.ajaxCount++;
            this.FeetypeService.getItemsDetail(this.id).subscribe(
                data => {
                    this.ajaxCount--;
                    this.feetypeData = data['result'][0];
                    this.feetypeData['f120fstatus'] = parseInt(data['result'][0]['f120fstatus']);

                    console.log(this.feetypeData);
                }, error => {
                    console.log(error);
                });
        }


        console.log(this.id);
    }

    addCurrencyratesetup() {
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        console.log(this.feetypeData);

        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.FeetypeService.updatefeetypeItems(this.feetypeData, this.id).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert('Fee Type Code  Already Exist');
                    }
                    else {
                        this.router.navigate(['studentfinance/feetype']);
                        alert("Feetype has been Updated Sucessfully !!");
                    }

                }, error => {
                    console.log(error);
                    alert('Fee Type Code  Already Exist');
                });


        } else {
            this.FeetypeService.insertfeetypeItems(this.feetypeData).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert('Fee Type Code  Already Exist');
                    }
                    else {
                        this.router.navigate(['studentfinance/feetype']);
                        alert("Feetype has been Added Sucessfully !!");
                    }
                }, error => {
                    console.log(error);
                });

        }

    }
    onlyNumberKey(event) {
        console.log(event);
        return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
    }
}