import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { FeetypeService } from "../service/feetype.service";
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'Feetype',
    templateUrl: 'feetype.component.html'
})

export class FeetypeComponent implements OnInit {
    currencyratesetupList =  [];
    currencyratesetupData = {};
    id: number;

    constructor(
        private FeetypeService: FeetypeService,
        private spinner: NgxSpinnerService,        

    ) { }

    ngOnInit() { 
        this.spinner.show();                                                        
        this.FeetypeService.getItems().subscribe(
            data => {
                this.spinner.hide();
                this.currencyratesetupList = data['result']['data'];
        }, error => {
            console.log(error);
        });
    }

}