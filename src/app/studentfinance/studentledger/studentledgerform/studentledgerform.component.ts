import {Injector} from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { StudentledgerService } from '../../service/studentledger.service'
import { ActivatedRoute } from '@angular/router';
import { FeestructureService } from '../../service/feestructure.service'

@Component({
    selector: 'studentledgerFormComponent',
    templateUrl: 'studentledgerform.component.html'
})

export class StudentledgerFormComponent implements OnInit {
    studentledgerList =  [];
    studentledgerData = {};
    intakeList = [];
    studentList = [];
    programmeList = [];
    id: number;
 
    constructor(
        
        private StudentledgerService: StudentledgerService,
        private route: ActivatedRoute,
        private FeestructureService: FeestructureService,
    ) { }

    ngOnInit() { 
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        console.log(this.id);
        
        if(this.id>0) {
            this.StudentledgerService.getItemsDetail(this.id).subscribe(
                data => {
                    console.log(data);
                    
                    this.studentledgerData['f014fid'] = data['result']['f014fid'];

    
            }, error => {
                console.log(error);
            });
        }
        

        console.log(this.id);
    }
    getViewData() {

        if (this.studentledgerData['graduationType'] == "UG") {
            //Intake drop down 

            this.FeestructureService.getUgIntake().subscribe(
                data => {
                    this.intakeList = data['result']; 
                }, error => {
                    console.log(error);
                });

            //Programme drop down 

            this.FeestructureService.getUgPrograms().subscribe(
                data => {
                    this.programmeList = data['result'];
                }, error => {
                    console.log(error);
                });

               
                    this.FeestructureService.getUgStudent().subscribe(
                        data => {
            
                            this.studentList = data['result'];
                        }, error => {
                            console.log(error);
                        });

        }
        else {

            //Programme drop down 

            this.FeestructureService.getPgProgram().subscribe(
                data => {
                    this.programmeList = data['result'];
                }, error => {
                    console.log(error);
                });
                this.FeestructureService.getPgIntake().subscribe(
                    data => {
                        this.intakeList = data['result'];
                    }, error => {
                        console.log(error);
                    });
                this.FeestructureService.getPgStudent().subscribe(
                    data => {
        
                        this.studentList = data['result'];
                    }, error => {
                        console.log(error);
                    });


        }
    }
    addStudent(){
           console.log(this.studentledgerData);
           if(this.id>0) {

           } else {
        this.StudentledgerService.insertStudentledgerItems(this.studentledgerData,this.id).subscribe(
                    data => {
                        this.studentledgerList = data['todos'];
                }, error => {
                    console.log(error);
                });
           }
       

    }
    goToListPage(){
        
    }
}