import { Injector, ViewEncapsulation } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { TabsModule } from 'ngx-bootstrap';
import { StudentledgerService } from '../service/studentledger.service'
import { ActivatedRoute, Router } from '@angular/router';
import { GroupsService } from '../../generalsetup/service/groups.service';
import { StateService } from '../../generalsetup/service/state.service'
import { StudentService } from '../../studentfinance/service/student.service';
import { BankService } from '../../generalsetup/service/bank.service'
import { FeestructureService } from '../service/feestructure.service'
import { DefinationmsService } from '../service/definationms.service';
import { environment } from '../../../environments/environment';

@Component({
    selector: 'studentledger',
    templateUrl: 'studentledger.component.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./studentledger.component.css']
})

export class StudentledgerComponent implements OnInit {
    studentledgerData = {};
    studentList = [];
    invoiceList = [];
    debitnoteList = [];
    creditnoteList = [];
    graduationType = [];
    receiptList = [];
    discountList = [];
    AdvancepaymentList = [];
    intakeList = [];
    statementgroupList=[];
    statementList = [];
    programmeList = [];
    ajaxCount: number;
    spId: number;
    ledgerRegistrationDetails = {};
    id: number;
    downloadUrl: string = environment.api.downloadbase;

    constructor(
        private groupsService: GroupsService,
        private StudentledgerService: StudentledgerService,
        private DefinationmsService: DefinationmsService,

        private StudentService: StudentService,
        private route: ActivatedRoute,
        private router: Router,
        private FeestructureService: FeestructureService,


    ) { }
    getViewData() {

        if (this.studentledgerData['graduationType'] == "UG") {
            //Intake drop down 

            this.FeestructureService.getUgIntake().subscribe(
                data => {
                    this.intakeList = data['result'];
                }, error => {
                    console.log(error);
                });

            //Programme drop down 

            this.FeestructureService.getUgPrograms().subscribe(
                data => {
                    this.programmeList = data['result'];
                }, error => {
                    console.log(error);
                });


            this.FeestructureService.getUgStudent().subscribe(
                data => {

                    this.studentList = data['result'];
                }, error => {
                    console.log(error);
                });

        }
        else {

            //Programme drop down 

            this.FeestructureService.getPgProgram().subscribe(
                data => {
                    this.programmeList = data['result'];
                }, error => {
                    console.log(error);
                });
            this.FeestructureService.getPgIntake().subscribe(
                data => {
                    this.intakeList = data['result'];
                }, error => {
                    console.log(error);
                });
            this.FeestructureService.getPgStudent().subscribe(
                data => {

                    this.studentList = data['result'];
                }, error => {
                    console.log(error);
                });


        }
    }
    ngOnInit() {
        this.ajaxCount++;

        this.DefinationmsService.getScheme('ProgrammeScheme').subscribe(
            data => {
                this.ajaxCount--;

                this.graduationType = data['result'];
            }, error => {
                console.log(error);
            });
        //Student dropdown
        // this.ajaxCount++;
        // this.StudentService.getActiveItems().subscribe(
        //     data => {
        //         this.ajaxCount--;
        //         this.studentList = data['result']['data'];
        //     }
        // );

        // this.ledgerService.getItems().subscribe(
        //     data => {
        //         this.ledgerList = data['result']['data'];
        // }, error => {
        //     console.log(error);
        // });
    }
    showStudent() {
        this.spId = this.studentledgerData['student'];
        this.StudentledgerService.getItemsDetail(this.spId).subscribe(
            data => {
                this.invoiceList = data['result']['invoice'];
                this.debitnoteList = data['result']['debit'];
                this.creditnoteList = data['result']['credit'];
                this.receiptList = data['result']['payment'];
                this.discountList = data['result']['discount'];
                this.AdvancepaymentList = data['result']['advance_payment'];
                this.statementList = data['result']['Account_statement'];
                this.statementgroupList = data['result']['Account_statementbygroup'];

            }, error => {
                console.log(error);
            });
    }
    addPrint(id) {
        var confirmPop = confirm("Do you want to Print?");
        if (confirmPop == false) {
            return false;
        }
        this.spId = this.studentledgerData['student'];
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.StudentledgerService.insertPrintItems(this.spId).subscribe(
            data => {
                // alert('KWAP has been saved');
                window.open(this.downloadUrl + data['name']);
                this.router.navigate(['studentfinance/studentledger']);

            }, error => {
                console.log(error);
            });
    }
}