import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { CurrencysetupService } from '../../service/currencysetup.service'
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
@Component({
    selector: 'CurrencysetupFormComponent',
    templateUrl: 'currencysetupform.component.html'
})

export class CurrencysetupFormComponent implements OnInit {
    currencysetupList = [];
    currencysetupData = {};
    id: number;
    ajaxCount: number;
    constructor(
        private CurrencysetupService: CurrencysetupService,
        private spinner: NgxSpinnerService,
        private route: ActivatedRoute,
        private router: Router
    ) { }
    ngDoCheck() {
        const change = this.ajaxCount;
        if (this.ajaxCount == 0) {
            this.spinner.hide();
        }
    }
    ngOnInit() {
        this.ajaxCount = 0
        this.currencysetupData['f062fcurStatus'] = 1;
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.spinner.show();
        if (this.id > 0) {
            this.ajaxCount++;
            this.CurrencysetupService.getItemsDetail(this.id).subscribe(
                data => {
                    this.ajaxCount--;
                    this.currencysetupData = data['result'];

                    console.log(this.currencysetupData);
                }, error => {
                    console.log(error);
                });
        }
    }
    addCurrencysetup() {
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        this.currencysetupData['f062fcurUpdatedBy'] = 1;
        this.currencysetupData['f062fcurCreatedBy'] = 1;

        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.CurrencysetupService.updatecurrencysetupItems(this.currencysetupData, this.id).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert('Currency Code  Already Exist');
                    }
                    else {
                        this.router.navigate(['studentfinance/currencysetup']);
                        alert("Currency Setup has been Updated Successfully!!");
                    }
                }, error => {
                    console.log(error);
                    alert('Currency Code  Already Exist');
                });
        } else {
            this.CurrencysetupService.insertcurrencysetupItems(this.currencysetupData).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert('Currency Code  Already Exist');
                    }
                    else {
                        this.router.navigate(['studentfinance/currencysetup']);
                        alert("Currency Setup has been Added Successfully!!");
                    }
                }, error => {
                    console.log(error);
                    alert('Currency Code  Already Exist');
                });
        }
    }
    onlyNumberKey(event) {
        //console.log(event);
        if (event.charCode == 46) {

        } else {
            return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
        }
    }
}