import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CurrencysetupService } from '../service/currencysetup.service'
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'Currencysetup',
    templateUrl: 'currencysetup.component.html'
})

export class CurrencysetupComponent implements OnInit {
    currencysetupList = [];
    currencysetupData = {};
    id: number;

    constructor(

        private CurrencysetupService: CurrencysetupService,
        private spinner: NgxSpinnerService,        

    ) { }

    ngOnInit() {
        this.spinner.show();                
        this.CurrencysetupService.getItems().subscribe(
            data => {
                this.spinner.hide();
                this.currencysetupList = data['result']['data'];
            }, error => {
                console.log(error);
            });
    }
}