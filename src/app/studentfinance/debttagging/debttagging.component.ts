import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DebttaggingService } from '../service/debttagging.service'
import { NgxSpinnerService } from 'ngx-spinner';
@Component({
    selector: 'Debttagging',
    templateUrl: 'debttagging.component.html'
})

export class DebttaggingComponent implements OnInit {
    debttaggingList =  [];
    debttaggingData = {};
    id: number;

    constructor(
        private DebttaggingService: DebttaggingService,
        private spinner: NgxSpinnerService,

    ) { }

    ngOnInit() { 
        this.spinner.show();        
        this.DebttaggingService.getItems().subscribe(
            data => {
                this.spinner.hide();
                this.debttaggingList = data['result']['data'];
        }, error => {
            console.log(error);
        });
    }
}