import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { DebttaggingService } from '../../service/debttagging.service'
import { DebitinformationService } from '../../service/debitinformation.service'
import { StudentService } from '../../service/student.service'
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CustomerService } from '../../../accountsrecivable/service/customer.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'DebttaggingFormComponent',
    templateUrl: 'debttaggingform.component.html'
})

export class DebttaggingFormComponent implements OnInit {
    debttaggingform: FormGroup;
    debttaggingList = [];
    debttaggingData = {};
    studentList = [];
    studentData = {};
    debitinfoList = [];
    taxsetupcodeData = {};
    ajaxCount: number;
    id: number;

    constructor(
        private DebttaggingService: DebttaggingService,
        private spinner: NgxSpinnerService,
        private CustomerService: CustomerService,
        private StudentService: StudentService,
        private DebitinformationService: DebitinformationService,
        private route: ActivatedRoute,
        private router: Router

    ) { }
    ngDoCheck() {
        const change = this.ajaxCount;
        if (this.ajaxCount == 0) {
            this.spinner.hide();
        }
    }
    ngOnInit() {
        this.ajaxCount = 0
        this.spinner.show();
        this.debttaggingData['f047fstatus'] = 1;

        this.debttaggingform = new FormGroup({
            feeitem: new FormControl('', Validators.required)
        });
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        //Student drop down 
        this.ajaxCount++;
        this.StudentService.getActiveItems().subscribe(
            data => {
                this.ajaxCount--;
                this.studentList = data['result']['data'];
            }, error => {
                console.log(error);
            });

        // debuitinfo code dropdown
        this.ajaxCount++;
        this.CustomerService.getActiveItems().subscribe(
            data => {
                this.ajaxCount--;
                //console.log(data['result']);
                this.debitinfoList = data['result']['data'];
            }, error => {
                console.log(error);
            });
        if (this.id > 0) {
            this.ajaxCount++;
            this.DebttaggingService.getItemsDetail(this.id).subscribe(
                data => {
                    this.ajaxCount--;
                    this.debttaggingData = data['result'];

                    // converting from string to int for radio button default selection
                    this.debttaggingData['f047fstatus'] = parseInt(data['result']['f047fstatus']);

                    console.log(this.debttaggingData);
                }, error => {
                    console.log(error);
                });
        }
    }
    ConvertToFloat(val) {
        return parseFloat(val);
    }
    addDebttagging() {
        console.log(this.debttaggingData);
        // this.feeitemData['f015fupdatedBy'] = 1;

        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        this.debttaggingData['f047ftotalDebt'] = this.ConvertToFloat(this.debttaggingData['f047ftotalDebt']).toFixed(2);

        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.DebttaggingService.updatedebttaggingItems(this.debttaggingData, this.id).subscribe(
                data => {
                    this.router.navigate(['studentfinance/debttagging']);
                    alert("Debt Tagging has been Updated Successfully !")
                }, error => {
                    console.log(error);
                });

        } else {
            this.DebttaggingService.insertdebttaggingItems(this.debttaggingData).subscribe(
                data => {
                    this.router.navigate(['studentfinance/debttagging']);
                    alert("Debt Tagging has been Added Successfully !")


                }, error => {
                    console.log(error);
                });
        }
    }
    onlyNumberKey(event) {
        //console.log(event);
        if (event.charCode == 46) {
        } else {
            return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
        }
    }
}