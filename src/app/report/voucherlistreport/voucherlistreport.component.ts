import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { VoucherlistreportService } from '../service/voucherlistreport.service'
import { QueryBuilderConfig } from 'angular2-query-builder';
import  {DepartmentService} from '../../generalsetup/service/department.service';
import {Http, Response } from '@angular/http';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
    selector: 'Voucherlistreport',
    templateUrl: 'voucherlistreport.component.html'
})

export class VoucherlistreportComponent implements OnInit {
    ReportList = [];
    ReportData = {};
    id: number;
    queryJson = {};
    displayfields = [];
    displayObject = {};
    newDisplayFields = [];
    outputData = [];
    outputHeader = [];
    yourArray = [];

    constructor(
        private voucherlistreportService: VoucherlistreportService,
        deptService : DepartmentService,
        private httpClient: HttpClient
    ) { 

        this.displayfields=["f014fgl_code","f015fdepartment_name","f057fcode","f058fcode"];

    }

    data = '{"group": {"operator": "AND","rules": []}}';
    query = {
        condition: 'and',
        rules: [
            {field: 'f014fgl_code', operator: 'Ends With',value:'7'}
        ]
      };
      
      config: QueryBuilderConfig = {
        fields: {
            f014fgl_code: {name: 'Glcode', type: 'string', operators: ['=', '<=', '>','in','not in','!=','Ends With','Starts With']},
            f015fdepartment_name: {name: 'Department', type: 'string', operators: ['=', '<=', '>','in','not in','!=','Ends With','Starts With']},
            fund: {name: 'Fund', type: 'string', operators: ['=', '<=', '>','in','not in','!=','Ends With','Starts With']}
          },
        }
        getSql() {
            console.log(this.query);
            console.log(JSON.stringify(this.query));    
            this.queryJson = JSON.stringify(this.query);

          var finaldata = this.computed(this.query);
          console.log(finaldata);
          var yourArray = [];
          $("input:checkbox[name=type]:checked").each(function(){
              yourArray.push($(this).val());
          });
         console.log(yourArray);
         this.displayObject['columns'] = yourArray;
         this.displayObject['where'] = finaldata;
         this.displayObject['download'] = 'no';
         console.log(this.displayObject);
         this.voucherlistreportService.displayReport(this.displayObject).subscribe(
            data => {
                this.outputData = data['result'];
            }, error => {
                console.log(error);
            });

          //console.log(JSON.stringify(this.queryJson));

      }
    htmlEntities(str) {
        return String(str).replace(/</g, '&lt;').replace(/>/g, '&gt;');
    }
    Download(type) {


        this.displayObject['download'] = 'yes';
        this.displayObject['type'] = 'pdf';
        if(type=='PDF') {
            this.displayObject['type'] = 'pdf';
        }
        window.open('http://fims.com/downloads/1.pdf');
        // return this.httpClient.post("", this.displayObject, requestOptions);


        // return this.httpClient.post({
        //     url: 'your/webservice',
        //     data: json, //this is your json data string
        //     headers: {
        //        'Content-type': 'application/json'
        //     },
        //     responseType: 'arraybuffer'
        // }).success(function (data, status, headers, config) {
        //     var blob = new Blob([data], {type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"});
        //     var objectUrl = URL.createObjectURL(blob);
        //     window.open(objectUrl);
        // }).error(function (data, status, headers, config) {
        //     //upload failed
        // });

        // pdf: {  

        
        this.voucherlistreportService.displayReportPDF(this.displayObject).subscribe(
            response => {
               console.log(response);
               
            });


    }

  getFileNameFromHeader(header: string): string {
    if (!header) return null;

    var result: string = header.split(";")[1].trim().split("=")[1];

    return result.replace(/"/g, '');
}

       computed(group) {
           console.log(group);
        if (!group) return "";
        for (var str = "(", i = 0; i < group.rules.length; i++) {
            i > 0 && (str += " " + group.condition + " ");
            str += group.rules[i].rules ?
                this.computed(group.rules[i]) : this.getDataFormat(group.rules[i]);
        }

        return str + ")";
    }


    getDataFormat(dataRule){
        var dataField = '';
        switch(dataRule.operator) {
            case 'Ends With':
            dataField = dataRule.field + "  Like  "+ "'%"+dataRule.value+"'";
            break;
            case 'Starts With':
            dataField = dataRule.field + "  Like  "+ "'"+dataRule.value+"%'";
            break;
            case '=':
            dataField = dataRule.field + " "+ dataRule.operator+ " '"+dataRule.value+"'";
            break;
            case '!=':
            dataField = dataRule.field + " "+ dataRule.operator+ " '"+dataRule.value+"%'";
            break;
            case '>':
            dataField = dataRule.field+ " "+ dataRule.operator+ " '"+dataRule.value+"'";
            break;
            case '<':
            dataField = dataRule.field + " "+ dataRule.operator+ " '"+dataRule.value+"%'";
            break;           
        }
        return dataField;

    }


    // filter = JSON.parse(data);

    // $scope.$watch('filter', function (newValue) {
    //     $scope.json = JSON.stringify(newValue, null, 2);
    //     $scope.output = computed(newValue.group);
    // }, true);

      ngOnInit() { 
      }
    addVoucherlistreport(){
           console.log(this.ReportData);
       

    }
}


