import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FundService } from '../../generalsetup/service/fund.service';
import { BalancesheetreportService } from './../service/balancesheetreport.service';
import { TrialbalancereportService } from './../service/trialbalancereport.service';
import { environment } from '../../../environments/environment';


@Component({
    selector: 'BalancesheetreportComponent',
    templateUrl: 'balancesheetreport.component.html'
})

export class BalancesheetreportComponent implements OnInit {

    balancesheetList = [];
    balancesheetData = {};
    fundList = [];
    fundData = {};
    numberList = [];
    fmonth=[];
    fyear=[];
    id: number;
    downloadUrl: string = environment.api.downloadbase;

    constructor(

        private BalancesheetreportService: BalancesheetreportService,
        private FundService: FundService,
        private TrialbalancereportService:TrialbalancereportService,
        private route: ActivatedRoute,
        private router: Router

    ) { }


    downloadReport(){
        console.log(this.balancesheetData);
        let newobje = {};
        newobje['fromDay'] = this.balancesheetData['fromDay'];
        newobje['toDay'] = this.balancesheetData['toDay'];
        newobje['month'] = this.balancesheetData['month'];
        newobje['year'] = this.balancesheetData['year'];

        this.TrialbalancereportService.downloadReport(newobje).subscribe(
            data => {
                console.log(data['name']);
                window.open(this.downloadUrl+data['name']); 
            }, error => {
                console.log(error);
            });

    }


    ngOnInit() {

        let year = {};
        year['name'] = '2010';
        this.fyear.push(year);
        year = {};
        year['name'] = '2011';
        this.fyear.push(year);
        year = {};
        year['name'] = '2012';
        this.fyear.push(year);
        year = {};
        year['name'] = '2013';
        this.fyear.push(year);
        year = {};
        year['name'] = '2014';
        this.fyear.push(year);
        year = {};
        year['name'] = '2015';
        this.fyear.push(year);
        year = {};
        year['name'] = '2016';
        this.fyear.push(year);
        year = {};
        year['name'] = '2017';
        this.fyear.push(year);
        year = {};
        year['name'] = '2018';
        this.fyear.push(year);
        year = {};
        year['name'] = '2019';
        this.fyear.push(year);

        let month = {};
        month['name'] = 'January ';
        month['value'] = '1';
        this.fmonth.push(month);
        month = {};
        month['value'] = '2';
        month['name'] = 'Febuary ';
        this.fmonth.push(month);
        month = {};
        month['name'] = 'March ';
        month['value'] = '3';
        this.fmonth.push(month);
        month = {};
        month['name'] = 'April ';
        month['value'] = '4';
        this.fmonth.push(month);
        month = {};
        month['name'] = 'May ';
        month['value'] = '5';
        this.fmonth.push(month);
        month = {};
        month['name'] = 'June ';
        month['value'] = '6';
        this.fmonth.push(month);
        month = {};
        month['name'] = 'July ';
        month['value'] = '7';
        this.fmonth.push(month);
        month = {};
        month['name'] = 'August ';
        month['value'] = '8';
        this.fmonth.push(month);
        month = {};
        month['name'] = 'September ';
        month['value'] = '9';
        this.fmonth.push(month);
        month = {};
        month['name'] = 'October ';
        month['value'] = '10';
        this.fmonth.push(month);
        month = {};
        month['name'] = 'November ';
        month['value'] = '11';
        this.fmonth.push(month);
        month = {};
        month['name'] = 'December ';
        month['value'] = '12';
        this.fmonth.push(month);

        this.balancesheetData['f051fstatus'] = 1;
        this.balancesheetData['year'] = '';
        this.balancesheetData['Month'] = '';
        this.balancesheetData['Fromday'] = '';
        this.balancesheetData['Today'] = '';
        for(var i=1;i<30;i++) {
            let number = {};
            number['key'] = i;
            number['value'] = i;
            this.numberList.push(number);
        }
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        

        // Fund dropdown
        this.FundService.getItems().subscribe(
            data => {
                this.fundList = data['result']['data'];
            }, error => {
                console.log(error);
            });


       // console.log(this.id);

        if (this.id > 0) {
            this.BalancesheetreportService.getItemsDetail(this.id).subscribe(
                data => {
                    this.balancesheetData = data['result'][0];
                    if (data['result'][0].f051fstatus === true) {
                       console.log(data);
                        this.balancesheetData['f051fstatus'] = 1;
                    } else {
                        this.balancesheetData['f051fstatus'] = 0;

                    }

                }, error => {
                    console.log(error);
                });
        }
    }

    addBalancesheetreport() {
        console.log(this.balancesheetData);
        this.balancesheetData['f051fupdatedBy'] = 1;
        this.balancesheetData['f051fcreatedBy'] = 1;

        let budgetDataArray = [];
        budgetDataArray.push(this.balancesheetData);
        let budgetDataInsertObject = {};
        budgetDataInsertObject['data'] = budgetDataArray;

        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.BalancesheetreportService.updateBalancesheetreportItems(budgetDataInsertObject, this.id).subscribe(
                data => {
                    this.router.navigate(['report/balancesheetreport']);
                }, error => {
                    console.log(error);
                });


        } else {
            this.BalancesheetreportService.insertBalancesheetreportItems(budgetDataInsertObject).subscribe(
                data => {
                    this.router.navigate(['report/balancesheetreport']);

                }, error => {
                    console.log(error);
                });

        }

    }
}