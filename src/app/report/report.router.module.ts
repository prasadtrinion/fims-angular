import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { VoucherlistreportComponent } from './voucherlistreport/voucherlistreport.component';
import { TrialbalancereportComponent } from  './trialbalancereport/trialbalancereport.component';
import { PnlreportComponent } from  './pnlreport/pnlreport.component';
import { BalancesheetreportComponent } from  './balancesheetreport/balancesheetreport.component';


const routes: Routes = [
  { path: 'voucherlistreport', component: VoucherlistreportComponent },
  { path: 'trialbalancereport', component: TrialbalancereportComponent },
  { path: 'pnlreport', component: PnlreportComponent },
  { path: 'balancesheetreport', component: BalancesheetreportComponent }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  
})
export class ReportRoutingModule { 
  
}