import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DepartmentService } from '../../generalsetup/service/department.service';
import { PnlreportService } from './../service/pnlreport.service';
import { environment } from '../../../environments/environment';

@Component({
    selector: 'TrialbalancereportComponent',
    templateUrl: 'trialbalancereport.component.html'
})

export class TrialbalancereportComponent implements OnInit {

    trialbalancereportList = [];
    trialbalancereportData = {};
    departmentList = [];
    fundData = {};
    faccount=[];
    id: number;
    downloadUrl: string = environment.api.downloadbase;

    constructor(

        private PnlreportService: PnlreportService,
        private DepartmentService: DepartmentService,
        private route: ActivatedRoute,
        private router: Router

    ) { }

    ngOnInit() {
        let accounttypeobj = {};
        accounttypeobj['name'] = 'Level 1';
        accounttypeobj['value'] = '1';
        this.faccount.push(accounttypeobj);
        accounttypeobj = {};
        accounttypeobj['name'] = 'Level 2';
        accounttypeobj['value'] = '2';
        this.faccount.push(accounttypeobj);
        accounttypeobj = {};
        accounttypeobj['name'] = 'Level 3';
        accounttypeobj['value'] = '3';
        this.faccount.push(accounttypeobj);

        this.trialbalancereportData['f051fstatus'] = 1;
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        

        // Fund dropdown
        this.DepartmentService.getItems().subscribe(
            data => {
                this.departmentList = data['result']['data'];
            }, error => {
                console.log(error);
            });


 }

   
    addTrialbalancereport() {
        this.trialbalancereportData['type'] = "BL";
        this.PnlreportService.insertpnlreportItems( this.trialbalancereportData).subscribe(
            data => {
                window.open(this.downloadUrl+data['name']); 
                this.router.navigate(['report/trialbalancereport']);

            }, error => {
                console.log(error);
            });


   
    }
}