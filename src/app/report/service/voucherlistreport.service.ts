import { Injectable } from '@angular/core';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';

const httpOptions = {
    headers: new HttpHeaders({
        'X-Auth-Client': 'qazwsx123',
        'Content-Type': 'application/json'
    }),
    headersFile: new HttpHeaders({
        'X-Auth-Client': 'qazwsx123',
        'Content-Type': 'application/json',
        'responseType': 'arraybuffer',

    })

};
@Injectable()

export class VoucherlistreportService {
    url: string = environment.api.base + environment.api.endPoints.report;
    constructor(private httpClient: HttpClient) { 
    }
    
    getItems() {
        return this.httpClient.get(this.url,httpOptions);
    }

    getItemsDetail(id) {
        return this.httpClient.get(this.url+'/'+id,httpOptions);
    }

    displayReport(reportdata): Observable<any> {
        return this.httpClient.post(this.url, reportdata,httpOptions);
    }
    displayReportPDF(reportdata): Observable<any> {
        return this.httpClient.post(this.url, reportdata,httpOptions);
    }

    updateVoucherlistreportItems(reportdata,id): Observable<any> {
        return this.httpClient.put(this.url+'/'+id, reportdata,httpOptions);
       }
}