

import { Injectable } from '@angular/core';
import { HttpClientModule,HttpHeaders, HttpClient} from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({ 'X-Auth-Client': 'qazwsx123',
    'Content-Type': 'application/json' })
    
  };
@Injectable()

export class TrialbalancereportService {

    url: string = environment.api.base + environment.api.endPoints.trailbalance;
    approveurl: string = environment.api.base + environment.api.endPoints.journalApprove
    constructor(private httpClient: HttpClient) {    
    }
    downloadReport(object) {
        return this.httpClient.post(this.url, object,httpOptions);

    }
    getItems() {
        return this.httpClient.get(this.url+'/'+1,httpOptions);
    }
    getItemsDetail(id) {
        return this.httpClient.get(this.url+'/'+id, httpOptions);
    }
    inserttrialbalancereportItems(trialbalancereportData): Observable<any> {
        return this.httpClient.post(this.url, trialbalancereportData,httpOptions);
    }
    updatetrialbalancereportItems(trialbalancereportData,id): Observable<any> {
        return this.httpClient.put(this.approveurl+'/'+id, trialbalancereportData,httpOptions);
    }
    updatetrialbalancereport(trialbalancereportData): Observable<any> {
        return this.httpClient.post(this.approveurl, trialbalancereportData,httpOptions);
    }
}