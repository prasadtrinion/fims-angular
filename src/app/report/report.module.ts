import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../material/material.module';
import { QueryBuilderModule } from "angular2-query-builder";

import { VoucherlistreportComponent } from './voucherlistreport/voucherlistreport.component';
import { TrialbalancereportComponent } from  './trialbalancereport/trialbalancereport.component';
import { PnlreportComponent } from  './pnlreport/pnlreport.component';

import { HttpClientModule, HttpClient,HTTP_INTERCEPTORS } from '@angular/common/http';


import { VoucherlistreportService } from './service/voucherlistreport.service';
import { TrialbalancereportService } from './service/trialbalancereport.service';
import { PnlreportService } from './service/pnlreport.service';

import { BalancesheetreportService } from './service/balancesheetreport.service';
import { BalancesheetreportComponent } from  './balancesheetreport/balancesheetreport.component';

import { DepartmentService } from './../generalsetup/service/department.service';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { ReportRoutingModule } from './../report/report.router.module';
import { JwtInterceptor } from '../_helpers/jwt.interceptors';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { SharedModule } from "../shared/shared.module";



@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        MaterialModule,
        ReportRoutingModule,
        Ng2SearchPipeModule,
        QueryBuilderModule,
        HttpClientModule,
        NgSelectModule,
        NgMultiSelectDropDownModule,
        SharedModule

    ],
    exports: [],
    declarations: [
        VoucherlistreportComponent,
        TrialbalancereportComponent,
        PnlreportComponent,
        BalancesheetreportComponent
    ],
    providers: [
        VoucherlistreportService,
        TrialbalancereportService,
        DepartmentService,
        PnlreportService,
        BalancesheetreportService,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: JwtInterceptor,
            multi: true
          }

    ],
})
export class ReportModule { }
