import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// import { ApprovallimitComponent } from './approvallimit/approvallimit.component';
// import { ApprovallimitFormComponent } from './approvallimit/approvallimitform/approvallimitform.component';

import { NonpoinvoiceComponent } from './nonpoinvoice/nonpoinvoice.component';
import { NonpoinvoiceFormComponent } from './nonpoinvoice/nonpoinvoiceform/nonpoinvoiceform.component';

import { PaymentvoucherComponent } from './paymentvoucher/paymentvoucher.component';
import { PaymentvoucherFormComponent } from './paymentvoucher/paymentvoucherform/paymentvoucherform.component';

import { NonpoinvoiceapprovalComponent } from './nonpoinvoiceapproval/nonpoinvoiceapproval.component';
import { PaymentvoucherapprovalComponent } from './paymentvoucherapproval/paymentvoucherapproval.component';

import { CreditorsetupComponent } from './creditorsetup/creditorsetup.component';
import { CreditorsetupformComponent } from './creditorsetup/creditorsetupform/creditorsetupform.component';

import { ClaimcustomerComponent } from './claimcustomer/claimcustomer.component';
import { ClaimcustomerFormComponent } from './claimcustomer/claimcustomerform/claimcustomerform.component';

import { BillRegistrationComponent } from "./billregistration/billregistration.component";
import { BillRegistrationformComponent  } from "./billregistration/billregistrationform/billregistrationform.component";

import { BillregistrationapprovalComponent } from "./billregistrationapproval/billregistrationapproval.component";

import { PaymentvouchereditFormComponent } from "./paymentvoucher/paymentvouchereditform/paymentvouchereditform.component";

import { PattycashallocationComponent } from './pattycashallocation/pattycashallocation.component';
import { PattycashallocationlistComponent } from './pattycashallocation/pattycashallocationlist/pattycashallocationlist.component';

import { PattycashapprovalComponent } from './pattycashapproval/pattycashapproval.component';

import { ReembarsementComponent } from "./reembarssemnt/reembarssement.component";

import { PattycashentryComponent } from './pattycashentry/pattycashentry.component';
import { PattycashentryformComponent } from './pattycashentry/pattycashentryform/pattycashentryform.component';

import { EtfvoucherComponent } from "./etfvoucher/etfvoucher.component";
import { EtfvoucherFormComponent } from "./etfvoucher/etfvoucherform/etfvoucherform.component";
import { EtfvoucherapprovalComponent } from "./etfvoucherapproval/etfvoucherapproval.component";

import { EtfvoucherbatchComponent } from "./etfvoucherbatch/etfvoucherbatch.component";
import { EtfvoucherbatchFormComponent } from "./etfvoucherbatch/etfvoucherbatchform/etfvoucherbatchform.component";

const routes: Routes = [
  
  { path: 'etf', component: EtfvoucherComponent },
  { path: 'addnewetf', component: EtfvoucherFormComponent },
  { path: 'editnewetf/:id', component: EtfvoucherFormComponent },
  { path: 'etfapproval', component: EtfvoucherapprovalComponent },
  { path: 'editnewetf/:id/:viewid', component: EtfvoucherFormComponent },

  { path: 'etfbatch', component: EtfvoucherbatchComponent },
  { path: 'addnewetfbatch', component: EtfvoucherbatchFormComponent },
  { path: 'editnewetfbatch/:id', component: EtfvoucherbatchFormComponent },

  { path: 'nonpoinvoice', component: NonpoinvoiceComponent },
  { path: 'addnewnonpoinvoice', component: NonpoinvoiceFormComponent },
  { path: 'editnonpoinvoice/:id', component: NonpoinvoiceFormComponent },
  { path: 'nonpoinvoiceapproval', component: NonpoinvoiceapprovalComponent },

  { path: 'paymentvoucher', component: PaymentvoucherComponent },
  { path: 'addnewpaymentvoucher', component: PaymentvoucherFormComponent },
  { path: 'editpaymentvoucher/:id/:viewid', component: PaymentvouchereditFormComponent },
  { path: 'viewpaymentvoucher/:id/:viewid', component: PaymentvouchereditFormComponent },
  { path: 'paymentvoucherapproval', component: PaymentvoucherapprovalComponent },

  { path: 'creditorsetup', component: CreditorsetupComponent },
  { path: 'addnewcreditorsetup', component: CreditorsetupformComponent },
  { path: 'editcreditorsetup/:id', component: CreditorsetupformComponent },

  { path: 'claimcustomer', component: ClaimcustomerComponent },
  { path: 'addnewclaimcustomer', component: ClaimcustomerFormComponent },
  { path: 'editclaimcustomer/:id', component: ClaimcustomerFormComponent },

  { path: 'billregistration', component: BillRegistrationComponent },
  { path: 'addnewbillgeneration', component: BillRegistrationformComponent },
  { path: 'editbillgeneration/:id', component: BillRegistrationformComponent },

  { path: 'billregistrationapproval', component: BillregistrationapprovalComponent },
  
  { path: 'pattycashallocationlist', component: PattycashallocationlistComponent },
  { path: 'addnewpattycashallocation', component: PattycashallocationComponent },
  { path: 'editpattycashallocation/:id', component: PattycashallocationComponent },

  { path: 'pattycashentry', component: PattycashentryComponent },
  { path: 'addnewpattycashentry', component: PattycashentryformComponent },
  { path: 'editpattycashentry/:id', component: PattycashentryformComponent },
  { path: 'viewpattycashapproval/:id/:idview', component: PattycashentryformComponent },

  { path: 'pattycashentryapproval', component: PattycashapprovalComponent },

  { path: 'pattycashreembrassment', component: ReembarsementComponent },
  { path: 'viewpattycashreembrassment/:id/:idview', component: PattycashentryformComponent },


];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],

})
export class AccountspayableRoutingModule {

}