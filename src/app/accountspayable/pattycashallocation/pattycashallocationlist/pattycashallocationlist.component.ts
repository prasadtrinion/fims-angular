import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { PattycashallocationService } from '../../service/pattycashallocation.service'
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'PattycashallocationlistComponent',
    templateUrl: 'pattycashallocationlist.component.html'
})

export class PattycashallocationlistComponent implements OnInit {
    PattycashallocationList =  [];
    PattycashallocationData = {};
    id: number;

    constructor(
        
        private PattycashallocationService: PattycashallocationService,
        private spinner: NgxSpinnerService,                

    ) { }

    ngOnInit() {  
        this.spinner.show();                                        
        this.PattycashallocationService.getItems().subscribe(
            data => {
                this.spinner.hide();
                this.PattycashallocationList = data['result'];
        }, error => {
            console.log(error);
        });
    }
}