import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { PattycashallocationService } from '../service/pattycashallocation.service'
import { FinancialyearService } from '../../generalledger/service/financialyear.service'
import { DefinationmsService } from '../../studentfinance/service/definationms.service';
import { DepartmentService } from '../../generalsetup/service/department.service'
import { StaffService } from "../../loan/service/staff.service";
import { HttpClientModule, HttpHeaders, HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { UserService } from '../../generalsetup/service/user.service';

@Component({
    selector: 'PattycashallocationComponent',
    templateUrl: 'pattycashallocation.component.html'
})
export class PattycashallocationComponent implements OnInit {
    pattycashallocationList = [];
    pattycashallocationData = {};
    bankList = [];
    financialyearList = [];
    paymentmodeList = [];
    staffList = [];
    deptList = [];
    userList = [];
    ajaxCount: number;
    file;
    id: number;
    constructor(

        private PattycashallocationService: PattycashallocationService,
        private StaffService: StaffService,
        private DefinationmsService: DefinationmsService,
        private FinancialyearService: FinancialyearService,
        private DepartmentService: DepartmentService,
        private spinner: NgxSpinnerService,
        private httpClient: HttpClient,
        private route: ActivatedRoute,
        private router: Router,
        private UserService: UserService


    ) { }
    ngDoCheck() {
        if (this.ajaxCount == 0) {
            this.spinner.hide();
        }
    }
    ngOnInit() {
        this.ajaxCount = 0;
        this.spinner.show();
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        //staff dropdown
        // this.ajaxCount++;
        // this.StaffService.getItems().subscribe(
        //     data => {
        //         this.ajaxCount--;
        //         this.staffList = data['result']['data'];
        //     }, error => {
        //         this.spinner.hide();
        //     }
        // ); 
        this.ajaxCount++;
        this.UserService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.userList = data['result']['data'];
            }, error => {
                console.log(error);
            });
        // Department  dropdown
        this.ajaxCount++;
        this.DepartmentService.getActiveItems().subscribe(
            data => {
                this.ajaxCount--;
                this.deptList = data['result']['data'];
            }, error => {
                console.log(error);
                this.spinner.hide();
            });

        // payment mode dropdown
        this.ajaxCount++;
        this.DefinationmsService.getPaymentmode('PaymentMode').subscribe(
            data => {
                this.ajaxCount--;
                this.paymentmodeList = data['result'];
            }, error => {
                console.log(error);
                this.spinner.hide();
            });
        //financial Year 
        this.ajaxCount++;
        this.FinancialyearService.getActiveItems().subscribe(
            data => {
                this.ajaxCount--;
                this.financialyearList = data['result'];
                if (data['result'].length == 1) {
                    this.pattycashallocationData['f122fallocationYear'] = data['result'][0]['f015fid'];

                }
            }, error => {
                console.log(error);
                this.spinner.hide();
            });
        // financial year
        if (this.id < 0 || this.id == undefined) {
            this.ajaxCount++;
            this.FinancialyearService.getActiveItems().subscribe(
                data => {
                    this.ajaxCount--;
                    this.financialyearList = data['result'];
                    if (data['result'].length == 1) {
                        this.pattycashallocationData['f122fallocationYear'] = data['result'][0]['f015fid'];

                    }
                }, error => {
                    console.log(error);
                    this.spinner.hide();
                });
        }
        this.ajaxCount++;
        this.PattycashallocationService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.pattycashallocationList = data['result'];
            }, error => {
                console.log(error);
                this.spinner.hide();
            });
        if (this.id > 0) {
            this.ajaxCount++;
            this.PattycashallocationService.getItemsDetail(this.id).subscribe(
                data => {
                    this.ajaxCount--;
                    this.pattycashallocationData = data['result'][0];
                    // converting from string to int for radio button default selection
                    this.pattycashallocationData['f122fapprover'] = parseInt(data['result'][0]['f122fapprover']);
                }, error => {
                    console.log(error);
                });
        }
    }
    addpattycashallocation() {
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        console.log(this.pattycashallocationData);
        if (parseInt(this.pattycashallocationData['f122fminAmount']) > parseInt(this.pattycashallocationData['f122fmaxAmount'])) {

            alert("Minimum Transaction Amount cannot be greater than Maximum Transaction Amount");
            return false;
        }
        if (parseInt(this.pattycashallocationData['f122fminReimbursement']) > parseInt(this.pattycashallocationData['f122fmaxReimbursement'])) {

            alert("Minimum Reimbursement Amount cannot be greater than Maximum Reimbursement Amount");
            return false;
        }
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        if (this.id > 0) {
            this.PattycashallocationService.updatePattycashallocationItems(this.pattycashallocationData, this.id).subscribe(
                data => {
                    if (data['status'] == 409) {
                    }
                    else {
                        this.router.navigate(['accountspayable/pattycashallocationlist']);
                        alert("Patty Cash Allocation has been Updated Successfully");
                    }
                }, error => {
                    console.log(error);
                });
        } else {
            this.PattycashallocationService.insertPattycashallocationItems(this.pattycashallocationData).subscribe(
                data => {
                    if (data['status'] == 409) {
                    }
                    else {
                        this.router.navigate(['accountspayable/pattycashallocationlist']);
                        alert("Patty Cash Allocation has been Added Successfully");
                    }
                }, error => {
                    console.log(error);
                });
        }
    }
    onlyNumberKey(event) {
        if (event.charCode == 46) {
        } else {
            return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
        }
    }
}