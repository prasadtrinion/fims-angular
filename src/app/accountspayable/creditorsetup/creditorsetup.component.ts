import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CreditorsetupService } from '../service/creditorsetup.service'
import { NgxSpinnerService } from 'ngx-spinner';


@Component({
    selector: 'CreditorsetupComponent',
    templateUrl: 'creditorsetup.component.html'
})

export class CreditorsetupComponent implements OnInit {
    creditorsetupList =  [];
    creditorsetupData = {};
    id: number;

    constructor(
        private CreditorsetupService: CreditorsetupService,
        private spinner: NgxSpinnerService,                

    ) { }

    ngOnInit() { 
        this.spinner.show();                                        
        this.CreditorsetupService.getItems().subscribe(
            data => {
                this.spinner.hide();
                this.creditorsetupList = data['result']['data'];
        }, error => {
            console.log(error);
        });
    }
}