import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { CreditorsetupService } from '../../service/creditorsetup.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { DefinationmsService } from '../../../studentfinance/service/definationms.service';
import { AccountcodeService } from '../../../generalsetup/service/accountcode.service'
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'CreditorsetupformComponent',
    templateUrl: 'creditorsetupform.component.html'
})

export class CreditorsetupformComponent implements OnInit {
    creditorsetupform: FormGroup;
    creditorsetupList = [];
    creditorsetupData = {};
    paymenttypeList = [];
    typeList = [];
    accountcodeList = [];
    userData = {};
    userList = [];
    ajaxCount: number;
    id: number;
    constructor(
        private AccountcodeService: AccountcodeService,
        private DefinationmsService: DefinationmsService,
        private CreditorsetupService: CreditorsetupService,
        private spinner: NgxSpinnerService,
        private route: ActivatedRoute,
        private router: Router
    ) { }

    ngDoCheck() {
        if (this.ajaxCount == 0) {
            this.spinner.hide();
        }
    }
    ngOnInit() {
        this.ajaxCount = 0;
        this.spinner.show();
        this.creditorsetupData['f095fsetupId'] = '';
        this.creditorsetupData['f095faccountCode'] = '';
        this.creditorsetupData['f095fstatus'] = 1;
        let typeobj = {};
        typeobj['typeName'] = 'Debit';
        this.typeList.push(typeobj);
        typeobj = {};
        typeobj['typeName'] = 'Credit';
        this.typeList.push(typeobj);
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        this.creditorsetupform = new FormGroup({
            creditorsetup: new FormControl('', Validators.required)
        });

        // payment type dropdown
        this.ajaxCount++;
        this.DefinationmsService.getPaymenttype('PaymentVoucherType').subscribe(
            data => {
                this.ajaxCount--;
                this.paymenttypeList = data['result'];
            }, error => {
                console.log(error);
            });
        // account dropdown
        this.ajaxCount++;
        this.AccountcodeService.getItemsByLevel().subscribe(
            data => {
                this.ajaxCount--;
                this.accountcodeList = data['result']['data'];
            }, error => {
                console.log(error);
            });
        if (this.id > 0) {
            this.ajaxCount++;
            this.CreditorsetupService.getItemsDetail(this.id).subscribe(
                data => {
                    this.ajaxCount--;
                    this.creditorsetupData = data['result'][0];
                    // converting from string to int for radio button default selection
                    this.creditorsetupData['f095fstatus'] = parseInt(data['result'][0]['f095fstatus']);
                    console.log(this.creditorsetupData);
                }, error => {
                    console.log(error);
                });
        }
    }

    addCreditorsetup() {
        console.log(this.creditorsetupData);
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.CreditorsetupService.updateCreditorSetupItems(this.creditorsetupData, this.id).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert('Type Setup Description Already Exist');
                    }
                    else {
                        this.router.navigate(['accountspayable/creditorsetup']);

                        alert("Type Setup has been Updated Sucessfully!!");
                    }
                }, error => {
                    alert('Type Setup Description Already Exist');

                    console.log(error);
                    return false;
                });
        } else {
            this.CreditorsetupService.insertCreditorSetupItems(this.creditorsetupData).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert('Type Setup Description Already Exist');
                        return false;
                    }
                    this.router.navigate(['accountspayable/creditorsetup']);

                    alert("Type Setup has been Added Sucessfully !!");
                }, error => {
                    console.log(error);
                });
        }
    }
}