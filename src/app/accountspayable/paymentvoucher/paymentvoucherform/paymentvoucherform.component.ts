import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { PaymentvoucherService } from '../../service/paymentvoucher.service';
import { DefinationmsService } from '../../../studentfinance/service/definationms.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { BanksetupaccountService } from '../../../generalsetup/service/banksetupaccount.service';
import { BankService } from '../../../generalsetup/service/bank.service';
import { VouchergenerationService } from '../../../accountsrecivable/service/vouchergeneration.service';
import { AlertService } from '../../../_services/alert.service';
import { BillRegistrationService } from "../../service/billregistration.service";
import { NgxSpinnerService } from 'ngx-spinner';
import { SupplierregistrationService } from '../../../procurement/service/supplierregistration.service';

@Component({
    selector: 'PaymentvoucherFormComponent',
    templateUrl: 'paymentvoucherform.component.html'
})

export class PaymentvoucherFormComponent implements OnInit {
    paymentvoucherDataheader = {};
    paymentvoucherform: FormGroup;
    paymentvoucherList = [];
    paymentvoucherData = {};
    voucherDetails = [];
    customerList = [];
    definationData = {};
    paymentmodeList = [];
    supplierList = [];
    paymenttypeList = [];
    companyBankList = [];
    bankList = [];
    VouchergenerationList = [];
    typeId: number;
    typeList = [];
    custVoucherList = [];
    studentList = [];
    payType: string;
    custId: number;
    studentId: number;
    valueDate: string;
    billRegistrationList = [];
    id: number;
    newBillRegistration = [];
    sourceList = [];
    viewDisabled: boolean;
    saveBtnDisable: boolean;
    viewid: number;
    ajaxCount:number;
    vendorsupplierList = [];
    originalData = [];
    masterSupplierList = [];
    totalFinal :number;
    constructor(
        private PaymentvoucherService: PaymentvoucherService,
        private BillRegistrationService: BillRegistrationService,
        private BanksetupaccountService: BanksetupaccountService,
        private VouchergenerationService: VouchergenerationService,
        private DefinationmsService: DefinationmsService,
        private spinner: NgxSpinnerService,
        private SupplierregistrationService:SupplierregistrationService,
        private BankService: BankService,
        private route: ActivatedRoute,
        private router: Router
    ) { }
    ngDoCheck() {
        if (this.ajaxCount == 0) {
            this.spinner.hide();
        }
    }
   
    ngOnInit() {

        let sourceObject={};
        // sourceObject['id']=1;
        // sourceObject['name'] = 'GRN';
        // this.sourceList.push(sourceObject);
        sourceObject = {};
        sourceObject['id']=1;
        sourceObject['name'] = 'PO';
        this.sourceList.push(sourceObject);
        sourceObject = {};
        sourceObject['id']=1;
        sourceObject['name'] = 'Warrant';
        this.sourceList.push(sourceObject);

        this.ajaxCount = 0;
        this.spinner.show();
        this.viewDisabled = false;
        this.paymentvoucherData['f093fstatus'] = 1;
        this.paymentvoucherData['f093fpaymentType'] = '';
        this.paymentvoucherData['f093idCustomer'] = '';
        this.paymentvoucherData['f093fpaymentMode'] = '';
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.route.paramMap.subscribe((data) => this.id = + data.get('idview'));


        // this.getVoucherDetails();
        this.valueDate = new Date().toISOString().substr(0, 10);
        console.log(this.valueDate);
        this.paymentvoucherData['f031fdate'] = this.valueDate;

        this.BillRegistrationService.getPaymentStatusItems().subscribe(
            data => {

                for (var i = 0; i < data['result'].length; i++) {
                    let customerArray = [];
                    if (data['result'][i]['f091fcontactPerson1'] != "NUll") {
                        let cus = {};
                        cus['name'] = data['result'][i]['f091fcontactPerson1'];
                        customerArray.push(cus);
                    }
                    if (data['result'][i]['f091fcontactPerson2'] != "NUll") {
                        //customerArray.push(data['result'][i]['f091fcontactPerson2']);
                        let cus = {};
                        cus['name'] = data['result'][i]['f091fcontactPerson2'];
                        customerArray.push(cus);
                    }
                    if (data['result'][i]['f091fcontactPerson3'] != "NUll") {
                        //customerArray.push(data['result'][i]['f091fcontactPerson3']);
                        let cus = {};
                        cus['name'] = data['result'][i]['f091fcontactPerson3'];
                        customerArray.push(cus);
                    }
                    data['result'][i]['customer'] = customerArray;
                    this.newBillRegistration.push(data['result'][i]);

                }
                this.originalData = this.newBillRegistration;
                this.paymentvoucherList = this.newBillRegistration;
                // console.log(this.paymentvoucherList);
            }, error => {
                console.log(error);
            });
        // company bank 
        this.ajaxCount++;
        this.BanksetupaccountService.getActiveCompanyBank().subscribe(
            data => {
            this.ajaxCount--;
                this.companyBankList = data['result']['data'];
                console.log(this.companyBankList);
            }, error => {
                console.log(error);
            });
        // bank   
        this.ajaxCount++;
        this.BankService.getActiveBank().subscribe(
            data => {
            this.ajaxCount--;
                this.bankList = data['result']['data'];
            }, error => {
                console.log(error);
            });
        // payment type dropdown
        this.ajaxCount++;
        this.DefinationmsService.getPaymenttype('PaymentVoucherType').subscribe(
            data => {
            this.ajaxCount--;
                this.paymenttypeList = data['result'];
                console.log(this.paymenttypeList);
            }, error => {
                console.log(error);
            });
    
        // payment mode dropdown
        this.ajaxCount++;
        this.DefinationmsService.getPaymentmode('PaymentMode').subscribe(
            data => {
            this.ajaxCount--;
                this.paymentmodeList = data['result'];
            }, error => {
                console.log(error);
            });

        this.SupplierregistrationService.getActiveItems().subscribe(
            data => {
                this.ajaxCount--;
                this.masterSupplierList = data['result']['data'];
                console.log(this.masterSupplierList);
                for(var i=0;i<this.masterSupplierList.length;i++){
                    this.masterSupplierList[i]['vid'] = this.masterSupplierList[i]['f030fid'];
                    this.masterSupplierList[i]['vname'] = this.masterSupplierList[i]['Vendor'];
                }
                console.log(this.masterSupplierList);

            }
        );
        
        console.log(this.id);
        let typeObject = {};
        typeObject['type'] = "PV";

    }
    
    getVoucherDetails() {
        for (var i = 0; i < this.VouchergenerationList.length; i++) {

            let voucherId = 0;
            voucherId = this.paymentvoucherData['f094idVoucher'];

            console.log(voucherId);


            this.VouchergenerationService.getDetail(voucherId).subscribe(
                data => {
                    this.voucherDetails = data['result'][0];
                    this.paymentvoucherData['f094fpaidAmount'] = 0;
                    this.paymentvoucherData['f094fvoucherDate'] = data['result'][0]['f071fvoucherDate'],
                    this.paymentvoucherData['f094ftotalAmount'] = data['result'][0]['f071fvoucherTotal']
                }
            );
        }

    }

    getVendorName(){
        this.ajaxCount++;
        this.vendorsupplierList = [];
        let supObject = {};

        this.SupplierregistrationService.getActiveItems().subscribe(
            data => {
                this.ajaxCount--;
                this.supplierList = data['result']['data'];
                console.log(this.supplierList);
                for(var i=0;i<this.supplierList.length;i++){
                    this.supplierList[i]['vid'] = this.supplierList[i]['f030fid'];
                    this.supplierList[i]['vname'] = this.supplierList[i]['Vendor'];
                }
                console.log(this.vendorsupplierList);

            }
        );
        
    }

    getOnlyVendorBillRegistration(){
        console.log(this.paymentvoucherDataheader);
        console.log(this.paymentvoucherDataheader['id']);
        this.PaymentvoucherService.getBillRegistrationByVendor(this.paymentvoucherDataheader['id']).subscribe(
            data => {
                this.paymentvoucherList = data['result'];
                for(var i=0;i<this.paymentvoucherList.length;i++) {
                    this.paymentvoucherList[i]['selectedCustomer']= this.paymentvoucherDataheader['id'];
                }
                
            }
        );
        if(this.paymentvoucherDataheader['id']==undefined || this.paymentvoucherDataheader['id']==null) {
            this.paymentvoucherList = this.originalData;
        }

        
        
    }

    prefillReferenceNumber(){
        if(this.paymentvoucherDataheader['id']!=undefined || this.paymentvoucherDataheader['id']!=null) {
            for(var i=0;i<this.paymentvoucherList.length;i++) {
                this.paymentvoucherList[i]['f094freferenceNumber']= this.paymentvoucherDataheader['f093fpaymentRefNo'];
            }
        }
    }

    
    getUserType() {

        this.typeId = this.paymentvoucherDataheader['f093fpaymentType'];

        // to show and hide customer field
        this.DefinationmsService.getItemsDetail(this.typeId).subscribe(
            data => {
                this.typeList = data['result'];
                this.payType = data['result'].f011fdefinitionCode;
                console.log(this.payType);
                // to get voucher list
                if (this.payType == "General Claim") {

                    this.PaymentvoucherService.getClaimCustomerlist(this.typeId).subscribe(
                        data => {
                            // customer list
                            this.VouchergenerationList = [];
                            this.customerList = data['result'];

                            console.log(data['result']);
                        }
                    );
                }
                else if (this.payType == "Student") {
                    this.PaymentvoucherService.getStudentTypeList(this.typeId).subscribe(
                        data => {
                            //student List
                            this.customerList = [];
                            this.VouchergenerationList = [];
                            this.studentList = data['result'];

                            for (var k = 0; k < this.studentList.length; k++) {
                                let customerObject = {};
                                customerObject['f071fidCustomer'] = this.studentList[k]['f071fidCustomer'];
                                customerObject['f022ffirstName'] = this.studentList[k]['f060fname'];
                                this.customerList.push(customerObject);
                            }
                        }
                    );
                }
                else {
                    this.PaymentvoucherService.getVoucherList(this.typeId).subscribe(
                        data => {
                            // voucher list based on payment type
                            this.VouchergenerationList = data['result'];
                            console.log(this.VouchergenerationList);
                        }
                    );
                }
            }, error => {
                console.log(error);
            });
    }

    getVoucher() {
        if (this.payType == "General Claim") {

            this.custId = this.paymentvoucherDataheader['f093idCustomer'];
            console.log(this.custId);
            console.log('general Claim');
            this.PaymentvoucherService.getClaimcustomerVoucher(this.custId).subscribe(
                data => {
                    // voucher list based on customer
                    this.VouchergenerationList = data['result'];
                    console.log(this.VouchergenerationList);
                    return this.payType;

                }, error => {
                    console.log(error);
                });
        }
        if (this.payType == "Student") {

            this.custId = this.paymentvoucherDataheader['f093idCustomer'];
            console.log(this.custId);
            console.log('student1');
            this.PaymentvoucherService.getStudentVoucherList(this.custId).subscribe(
                data => {
                    //voucher list based on student
                    this.VouchergenerationList = data['result'];
                    console.log(this.VouchergenerationList);
                }
            );
        }
    }
    savePaymentvoucher() {

        // if (this.paymentvoucherDataheader['f093fsource'] == undefined) {
        //     alert(" Select Source");
        //     return false;
        // }
        // if (this.paymentvoucherDataheader['f093fpaymentType'] == undefined) {
        //     alert(" Select Payment Voucher Type");
        //     return false;
        // }
        // if (this.paymentvoucherDataheader['f093fidCompanyBank'] == undefined) {
        //     alert(" Select Company Bank");
        //     return false;
        // }
        // if (this.paymentvoucherDataheader['f093fidBank'] == undefined) {
        //     alert(" Select Bank");
        //     return false;
        // }
        if (this.paymentvoucherDataheader['f093fpaymentMode'] == undefined) {
            alert(" Select Payment Mode");
            return false;
        }
        if (this.paymentvoucherDataheader['f093fpaymentRefNo'] == undefined) {
            alert(" Enter the Payment Reference Number");
            return false;
        }
        if (this.paymentvoucherDataheader['f093fdescription'] == undefined) {
            alert(" Enter the Description ");
            return false;
        }
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }

        console.log(this.paymentvoucherDataheader);
        let finalPaymentDetails = []
        this.totalFinal = 0;
        for (var i = 0; i < this.paymentvoucherList.length; i++) {
            let paymentDetails = {};
            paymentDetails['f094idBillRegistration'] = this.paymentvoucherList[i]['f091fid'];
            paymentDetails['f094fvoucherDate'] = this.paymentvoucherList[i]['f091fcreatedDate'];
            paymentDetails['f094ftotalAmount'] = this.paymentvoucherList[i]['f091ftotalAmount'];
            paymentDetails['f094fpaidAmount'] = this.paymentvoucherList[i]['f094fpaidAmount'];
            paymentDetails['f094fcustomerName'] = this.paymentvoucherList[i]['selectedCustomer'];
            this.totalFinal = this.totalFinal + this.paymentvoucherList[i]['f091ftotalAmount'];
            finalPaymentDetails.push(paymentDetails);
        }
        this.paymentvoucherDataheader['f093ftotalAmount'] = this.totalFinal;
        this.paymentvoucherDataheader['paymentVoucher-details'] = finalPaymentDetails;
        this.PaymentvoucherService.insertpaymentvoucherItems(this.paymentvoucherDataheader).subscribe(
            data => {
                if (data['status'] == 409) {
                    alert('Payment Voucher Number Already Exist');

                }
                else {
                    alert('Payment Voucher has been Added Sucessfully');
                    this.router.navigate(['accountspayable/paymentvoucher']);
                }
            }, error => {
                console.log(error);
            });
    }
    deletepaymentvouche(object) {
        var cnf = confirm("Do you really want to delete");
        if (cnf == true) {
            var index = this.paymentvoucherList.indexOf(object);;
            if (index > -1) {
                this.paymentvoucherList.splice(index, 1);
            }
        }
    }
    addPaymentvoucherlist() {
        console.log(this.paymentvoucherData);
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        var dataofCurrentRow = this.paymentvoucherData;
        this.paymentvoucherList.push(dataofCurrentRow);
        this.paymentvoucherData = {};
    }
    onlyNumberKey(event) {
        //console.log(event);
        if (event.charCode == 46) {

        } else {
            return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
        }
    }

}