import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { PaymentvoucherService } from '../../service/paymentvoucher.service';
import { DefinationmsService } from '../../../studentfinance/service/definationms.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { BanksetupaccountService } from '../../../generalsetup/service/banksetupaccount.service';
import { BankService } from '../../../generalsetup/service/bank.service';
import { VouchergenerationService } from '../../../accountsrecivable/service/vouchergeneration.service';
import { AlertService } from '../../../_services/alert.service';
import { BillRegistrationService } from "../../service/billregistration.service";
import { NgxSpinnerService } from 'ngx-spinner';



@Component({
    selector: 'PaymentvouchereditFormComponent',
    templateUrl: 'paymentvouchereditform.component.html'
})

export class PaymentvouchereditFormComponent implements OnInit {
    paymentvoucherDataheader = {};
    paymentvoucherform: FormGroup;
    paymentvoucherList = [];
    paymentvoucherData = {};
    ajaxCount: number;
    voucherDetails = [];
    customerList = [];
    definationData = {};
    paymentmodeList = [];
    paymenttypeList = [];
    companyBankList = [];
    bankList = [];
    VouchergenerationList = [];
    typeId: number;
    typeList = [];
    custVoucherList = [];
    studentList = [];
    payType: string;
    custId: number;
    studentId: number;
    valueDate: string;
    billRegistrationList =  [];
    id: number;
    viewid: number;
    viewDisabled: boolean;
    editDisabled: boolean;
    saveBtnDisable: boolean;
    constructor(
        private PaymentvoucherService: PaymentvoucherService,
        private  BillRegistrationService:  BillRegistrationService,
        private BanksetupaccountService: BanksetupaccountService,
        private VouchergenerationService: VouchergenerationService,
        private DefinationmsService: DefinationmsService,
        private BankService: BankService,
        private AlertService: AlertService,

        private route: ActivatedRoute,
        private router: Router,
        private spinner: NgxSpinnerService,
    ) { }



    ngDoCheck() {
        const change = this.ajaxCount;
        if (this.ajaxCount == 0) {
            this.spinner.hide();
        }
    }

    ngOnInit() {
        this.spinner.show();
        
        
        this.paymentvoucherData['f093fstatus'] = 1;
        this.paymentvoucherData['f093fpaymentType'] = '';
        this.paymentvoucherData['f093idCustomer'] = '';
        this.paymentvoucherData['f093fpaymentMode'] = '';

        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.route.paramMap.subscribe((data) => this.viewid = + data.get('viewid'));
        this.ajaxCount = 0;
if(this.id>0){
    this.ajaxCount++;
    this.PaymentvoucherService.getItemsDetail(this.id).subscribe(
        data => {
            this.ajaxCount--;
            this.saveBtnDisable = false;
                this.viewDisabled = false;
            console.log(data['result']);
            this.voucherDetails = data['result'];
            this.paymentvoucherDataheader['f093fvoucherNo'] = data['result']['f093fvoucherNo'];
            this.paymentvoucherDataheader['f093fpaymentType'] = data['result']['f093fpaymentType'];
            this.paymentvoucherDataheader['f093fidCompanyBank'] = data['result']['f093fidCompanyBank'];
            this.paymentvoucherDataheader['f093fidBank'] = data['result']['f093fidBank'];
            this.paymentvoucherDataheader['f093fpaymentMode'] = data['result']['f093fpaymentMode'];
            this.paymentvoucherDataheader['f093fpaymentRefNo'] = data['result']['f093fpaymentRefNo'];
            this.paymentvoucherDataheader['f093fdescription'] = data['result']['f093fdescription'];
            this.paymentvoucherList = data['result']['details'];

            if (data['result']['f093fapprovalStatus'] == 1) {
                this.viewDisabled = true;
                $("#target input,select").prop("disabled", true);
                $("#target1 input,select").prop("disabled", true);
            }
            if (this.viewid > 1) {
                this.saveBtnDisable = true;
                this.viewDisabled = true;
                $("#target input,select").prop("disabled", true);
                $("#target1 input,select").prop("disabled", true);
            }

            console.log(this.saveBtnDisable);
            console.log(this.viewDisabled);
        }
       
    );
}
        // this.getVoucherDetails();
        this.valueDate = new Date().toISOString().substr(0, 10);
        console.log(this.valueDate);
        this.paymentvoucherData['f031fdate'] = this.valueDate;
        this.ajaxCount++;
        this. BillRegistrationService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.billRegistrationList = data['result']['data'];
                console.log(this.billRegistrationList);
        }, error => {
            console.log(error);
        });


        // company bank
        this.ajaxCount++;
        this.BanksetupaccountService.getActiveCompanyBank().subscribe(
            data => {
                this.ajaxCount--;
                this.companyBankList = data['result']['data'];
            }, error => {
                console.log(error);
            });
        // bank   
        this.ajaxCount++;
        this.BankService.getActiveBank().subscribe(
            data => {
                this.ajaxCount--;
                this.bankList = data['result']['data'];
            }, error => {
                console.log(error);
            });
        // payment type dropdown
        this.ajaxCount++;
        this.DefinationmsService.getPaymenttype('PaymentVoucherType').subscribe(
            data => {
                this.ajaxCount--;
                this.paymenttypeList = data['result'];
                console.log(this.paymenttypeList);
            }, error => {
                console.log(error);
            });
        console.log(this.id);

        // payment mode dropdown
        this.ajaxCount++;
        this.DefinationmsService.getPaymentmode('PaymentMode').subscribe(
            data => {
                this.ajaxCount--;
                this.paymentmodeList = data['result'];
            }, error => {
                console.log(error);
            });
        console.log(this.id);



        let typeObject = {};
        typeObject['type'] = "PV";
        this.ajaxCount++;
        this.PaymentvoucherService.getPaymentNumber(typeObject).subscribe(
            newData => {
                this.ajaxCount--;
                console.log(newData);
                // this.paymentvoucherDataheader['f093fvoucherNo'] = newData['number'];
            }
        );
    }

    getVoucherDetails() {


        // console.log(this.VouchergenerationList);

        for (var i = 0; i < this.VouchergenerationList.length; i++) {

            let voucherId = 0;
            voucherId = this.paymentvoucherData['f094idVoucher'];

            console.log(voucherId);


            this.VouchergenerationService.getDetail(voucherId).subscribe(
                data => {
                    this.voucherDetails = data['result'];
                    this.paymentvoucherData['f094fpaidAmount'] = 0;
                    this.paymentvoucherData['f094fvoucherDate'] = data['result'][0]['f071fvoucherDate'];
                    this.paymentvoucherData['f094ftotalAmount'] = data['result'][0]['f071fvoucherTotal'];
                }
            );
        }

    }
    getUserType() {

        this.typeId = this.paymentvoucherDataheader['f093fpaymentType'];

        // to show and hide customer field
        this.DefinationmsService.getItemsDetail(this.typeId).subscribe(
            data => {
                this.typeList = data['result'];
                this.payType = data['result'].f011fdefinitionCode;
                console.log(this.payType);
                // to get voucher list
                if (this.payType == "General Claim") {

                    this.PaymentvoucherService.getClaimCustomerlist(this.typeId).subscribe(
                        data => {
                            // customer list
                            this.VouchergenerationList = [];
                            this.customerList = data['result'];

                            console.log(data['result']);
                        }
                    );
                }
                else if (this.payType == "Student") {
                    this.PaymentvoucherService.getStudentTypeList(this.typeId).subscribe(
                        data => {
                            //student List
                            this.customerList = [];
                            this.VouchergenerationList = [];
                            this.studentList = data['result'];

                            for(var k=0;k<this.studentList.length;k++) {
                                 let customerObject = {};
                                customerObject['f071fidCustomer'] = this.studentList[k]['f071fidCustomer'];
                                customerObject['f022ffirstName'] = this.studentList[k]['f060fname'];
                                this.customerList.push(customerObject);
                            }
                        }
                    );
                }
                else {
                    this.PaymentvoucherService.getVoucherList(this.typeId).subscribe(
                        data => {
                            // voucher list based on payment type
                            this.VouchergenerationList = data['result'];
                            console.log(this.VouchergenerationList);
                        }
                    );
                }

            }, error => {
                console.log(error);
            });

    }

    getVoucher() {

        if (this.payType == "General Claim") {

            this.custId = this.paymentvoucherDataheader['f093idCustomer'];
            console.log(this.custId);
            console.log('general Claim');
            this.PaymentvoucherService.getClaimcustomerVoucher(this.custId).subscribe(
                data => {
                    // voucher list based on customer
                    this.VouchergenerationList = data['result'];
                    console.log(this.VouchergenerationList);
                    return this.payType;

                }, error => {
                    console.log(error);
                });

        }
        if (this.payType == "Student") {

            this.custId = this.paymentvoucherDataheader['f093idCustomer'];
            console.log(this.custId);
            console.log('student1');
            this.PaymentvoucherService.getStudentVoucherList(this.custId).subscribe(
                data=> {
                    //voucher list based on student
                    this.VouchergenerationList = data['result'];
                    console.log(this.VouchergenerationList);
                }
            );
            

        }
    }
    savePaymentvoucher() {

        let paymentvoucherObject = {};
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            paymentvoucherObject['f093fid'] = this.id;
        }

        paymentvoucherObject['f093fvoucherNo'] = this.paymentvoucherDataheader['f093fvoucherNo'];
        paymentvoucherObject['f093fpaymentType'] = this.paymentvoucherDataheader['f093fpaymentType'];
        paymentvoucherObject['f093idCustomer'] = this.paymentvoucherDataheader['f093idCustomer'];
        paymentvoucherObject['f093fdescription'] = this.paymentvoucherDataheader['f093fdescription'];
        paymentvoucherObject['f093fpaymentMode'] = this.paymentvoucherDataheader['f093fpaymentMode'];
        paymentvoucherObject['f093fpaymentRefNo'] = this.paymentvoucherDataheader['f093fpaymentRefNo'];
        paymentvoucherObject['f093fidCompanyBank'] = this.paymentvoucherDataheader['f093fidCompanyBank'];
        paymentvoucherObject['f093fidBank'] = this.paymentvoucherDataheader['f093fidBank'];
        paymentvoucherObject['f093fstatus'] = this.paymentvoucherDataheader['f093fstatus'];
        paymentvoucherObject['paymentVoucher-details'] = this.paymentvoucherList;

        this.PaymentvoucherService.updatepaymentvoucherItems(paymentvoucherObject,this.id).subscribe(
            data => {
              
                alert('Payment Voucher has been Updated Sucessfully');
                this.router.navigate(['accountspayable/paymentvoucher']);


            }, error => {
                console.log(error);
            });
    }
    deletepaymentvouche(object) {
        var cnf = confirm("Do you really want to delete");
        if (cnf == true) {
            console.log(this.paymentvoucherList);
            var index = this.paymentvoucherList.indexOf(object);;
            if (index > -1) {
                this.paymentvoucherList.splice(index, 1);
            }

        }
    }
    addPaymentvoucherlist() {
        console.log(this.paymentvoucherData);
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        var dataofCurrentRow = this.paymentvoucherData;
        this.paymentvoucherList.push(dataofCurrentRow);
        console.log(this.paymentvoucherList);
        this.paymentvoucherData = {};
    }
    onlyNumberKey(event) {
        //console.log(event);
        if (event.charCode == 46) {

        } else {
            return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
        }
    }
}