import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { PaymentvoucherService } from '../service/paymentvoucher.service'
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'paymentvoucher',
    templateUrl: 'paymentvoucher.component.html'
})

export class PaymentvoucherComponent implements OnInit {
    paymentvoucherList =  [];
    paymentvoucherData = {};
    id: number;

    constructor(
        private PaymentvoucherService: PaymentvoucherService,
        private spinner: NgxSpinnerService,                

    ) { }

    ngOnInit() { 
        this.spinner.show();                                
        this.PaymentvoucherService.getItems().subscribe(
            data => {
                this.spinner.hide();
                let activityData = [];
                activityData = data['result']['data'];
                for (var i = 0; i < activityData.length; i++) {
                    if (activityData[i]['f093fapprovalStatus'] == 0) {
                        activityData[i]['f093fapprovalStatus'] = 'Pending';
                    } else if (activityData[i]['f093fapprovalStatus'] == 1) {
                        activityData[i]['f093fapprovalStatus'] = 'Approved';
                    } else {
                        activityData[i]['f093fapprovalStatus'] = 'Rejected';
                    }
                }
                this.paymentvoucherList = activityData;

        }, error => {
            console.log(error);
        });
    }
}