import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Injector } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BillregistrationapprovalService } from "../service/billregistrationapproval.service";
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'Billregistrationapproval',
    templateUrl: 'billregistrationapproval.component.html'
})
export class BillregistrationapprovalComponent implements OnInit {
    billregistrationapprovalList = [];
    billregistrationapprovalData = {};
    id: number;
    selectAllCheckbox: false;
    type: string;
    title = '';
    reason: string;
    constructor(
        private BillregistrationapprovalService: BillregistrationapprovalService,
        private spinner: NgxSpinnerService,        
        private route: ActivatedRoute,

    ) { }
    ngOnInit() {
        this.getList();
    }
    getList() {
        this.spinner.show();                
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.type = 'billregistration';
        this.title = 'Billregistration Approval';

        this.BillregistrationapprovalService.getItems().subscribe(
            data => {
                this.spinner.hide();
                this.billregistrationapprovalList = data['result'];
            }, error => {
                console.log(error);
            });
    }
    updateBillregistrationRejectedItems() {
        var billapprovalIds = [];
        for (var i = 0; i < this.billregistrationapprovalList.length; i++) {
            if (this.billregistrationapprovalList[i].f091fapprovedStatus == true) {
                billapprovalIds.push(this.billregistrationapprovalList[i].f091fid);
            }
        }
        if (billapprovalIds.length > 0) {

        } else {
            alert("Select atleast one Bill Registration to reject");
            return false;
        }
        var confirmPop = confirm("Do you want to Reject?");
        if (confirmPop == false) {
            return false;
        }
        console.log(this.billregistrationapprovalList);

        var billapprovalUpdate = {};
        billapprovalUpdate['id'] = billapprovalIds;
        // billapprovalUpdate['status'] = '2';
        billapprovalUpdate['reason'] = this.reason;
        this.BillregistrationapprovalService.updateBillregistrationRejectedItems(billapprovalUpdate).subscribe(
            data => {
                alert(" Bill Registration has been Rejected");

                this.getList();
            }, error => {
                console.log(error);
            });
    }
    billregistrationapprovalListData() {
        console.log(this.billregistrationapprovalList);
        var billapproveIds = [];
        for (var i = 0; i < this.billregistrationapprovalList.length; i++) {
            if (this.billregistrationapprovalList[i].f091fapprovedStatus == true) {
                billapproveIds.push(this.billregistrationapprovalList[i].f091fid);
            }
        }
        if (billapproveIds.length > 0) {

        } else {
            alert(" Select atleast one Bill Registration to Approve");
            return false;
        }
        var confirmPop = confirm("Do you want to Approve?");
        if (confirmPop == false) {
            return false;
        }
        var approvaloneUpdate = {};
        approvaloneUpdate['id'] = billapproveIds;
        approvaloneUpdate['status'] = '1';
        approvaloneUpdate['reason'] = this.billregistrationapprovalData['reason'];
        this.BillregistrationapprovalService.updateBillregistrationapprovalItems(approvaloneUpdate).subscribe(
            data => {
                this.billregistrationapprovalData['reason'] = '';

                alert("Bill Registration has been approved successfully");

                this.getList();
            }, error => {
                console.log(error);
            });
    }
    billregApprovalListData() {
        console.log(this.billregistrationapprovalList);
        var rejectoneIds = [];
        for (var i = 0; i < this.billregistrationapprovalList.length; i++) {
            if (this.billregistrationapprovalList[i].f091fapprovedStatus == true) {
                rejectoneIds.push(this.billregistrationapprovalList[i].f091fid);
            }
        }
        if (rejectoneIds.length > 0) {

        } else {
            alert(" Select atleast one Bill Registration to Reject");
            return false;
        }
        if (this.billregistrationapprovalData['reason'] == undefined) {
            alert(' Enter the Description');
            return false;
        }
        console.log(this.billregistrationapprovalData['reason']);
        if (this.billregistrationapprovalData['reason'] == '' || this.billregistrationapprovalData['reason'] == undefined) {
            alert("Enter the reason for Rejection !");
        } else {
            var confirmPop = confirm("Do you want to Reject?");
            if (confirmPop == false) {
                return false;
            }
            var rejecttwoUpdate = {};
            rejecttwoUpdate['id'] = rejectoneIds;
            rejecttwoUpdate['status'] = '2';
            rejecttwoUpdate['reason'] = this.billregistrationapprovalData['reason'];
            this.BillregistrationapprovalService.updateBillregistrationRejectedItems(rejecttwoUpdate).subscribe(
                data => {
                    alert("Bill Registration has been Rejected ! ");
                    this.billregistrationapprovalData['reason'] = '';

                    this.getList();
                }, error => {
                    console.log(error);
                });
        }
    }
}