import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ClaimcustomerService } from '../service/claimcustomer.service'


@Component({
    selector: 'Claimcustomer',
    templateUrl: 'claimcustomer.component.html'
})

export class ClaimcustomerComponent implements OnInit {
    claimcustomerList = [];
    claimcustomerData = {};
    id: number;

    constructor(
        private ClaimcustomerService: ClaimcustomerService,
    ) { }
    ngOnInit() {
        this.ClaimcustomerService.getItems().subscribe(
            data => {
                this.claimcustomerList = data['result']['data'];
        }, error => {
            console.log(error);
        });
    }
}