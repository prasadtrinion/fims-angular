import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { ClaimcustomerService } from '../../service/claimcustomer.service'
import { CountryService } from '../../../generalsetup/service/country.service'
import { StateService } from '../../../generalsetup/service/state.service'

import { ActivatedRoute, Router } from '@angular/router';

@Component({
    selector: 'ClaimcustomerFormComponent',
    templateUrl: 'claimcustomerform.component.html'
})

export class ClaimcustomerFormComponent implements OnInit {
    claimcustomerList = [];
    claimcustomerData = {};
    countryList = [];
    countryData = {};
    stateList = [];
    stateData = [];
    ajaxcount = 0;
    id: number;
    ajaxCount: number;

    constructor(
        private ClaimcustomerService: ClaimcustomerService,
        private CountryService: CountryService,
        private StateService: StateService,
        private route: ActivatedRoute,
        private router: Router
    ) { }

    ngOnInit() {

        this.claimcustomerData['f022fsupplier'] = 1;
        this.claimcustomerData['f022ftaxFree'] = 1;
        this.claimcustomerData['f022fstatus'] = 1;
        this.claimcustomerData['f022fcountry'] = '';
        this.claimcustomerData['f022fstate'] = '';
    
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        // country dropdown
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.CountryService.getItems().subscribe(
            data => {
                this.countryList = data['result']['data'];
            }, error => {
                console.log(error);
            });

        // state dropdown
        this.StateService.getItems().subscribe(
            data => {
                this.stateList = data['result']['data'];
            }, error => {
                console.log(error);
            });

        console.log(this.id);

        if (this.id > 0) {
            this.ClaimcustomerService.getItemsDetail(this.id).subscribe(
                data =>
                {
                    this.claimcustomerData = data['result'][0];
                    // converting from string to int for radio button default selection
                    this.claimcustomerData['f022fstatus'] = parseInt(data['result']['f022fstatus']);
                    console.log(this.claimcustomerData);
                }, error => {
                    console.log(error);
                });
        }
        console.log(this.id);
    }
    onCountryChange() {
        this.ajaxcount++;
        if (this.claimcustomerData['f022fcountry'] == undefined) {
            this.claimcustomerData['f022fcountry'] = 0;
        }
        this.StateService.getStates(this.claimcustomerData['f022fcountry']).subscribe(
            data => {
                this.ajaxcount--;
                this.stateList = data['result'];
                let other = {
                    'f012fid': "-1",
                    "f012fshortName": "SH",
                    "f012fstateName": "Others"
                };
                this.stateList.push(other);
            }
        );
    }
    addClaimcustomer() {
        console.log(this.claimcustomerData);
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.ClaimcustomerService.updateClaimcustomerItems(this.claimcustomerData, this.id).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert(' Already Exist');
                    }
                    else {
                        this.router.navigate(['accountspayable/claimcustomer']);
                        alert(" Claim Customer has been Updated Sucessfully!!");
                    }
                }, error => {
                    alert(' Already Exist');
                    console.log(error);
                    return false;
                });
        } else {
            this.ClaimcustomerService.insertClaimcustomerItems(this.claimcustomerData).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert(' Already Exist');
                        return false;
                    }
                    this.router.navigate(['accountspayable/claimcustomer']);
                    alert("Claim Customer has been Added Sucessfully !!");

                }, error => {
                    console.log(error);
                });
        }

    }
    onlyNumberKey(event) {
        console.log(event);
        return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
    }
}