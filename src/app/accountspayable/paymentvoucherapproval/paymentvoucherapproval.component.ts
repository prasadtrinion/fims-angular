import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Injector } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PaymentvoucherapprovalService } from '../service/paymentvoucherapproval.service'
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'Paymentvoucherapproval',
    templateUrl: 'paymentvoucherapproval.component.html'
})

export class PaymentvoucherapprovalComponent implements OnInit {
    paymentvoucherapprovalList = [];
    paymentvoucherapprovalData = {};
    id: number;
    selectAllCheckbox: false;
    type: string;
    title = '';

    constructor(
        private PaymentvoucherapprovalService: PaymentvoucherapprovalService,
        private spinner: NgxSpinnerService,                
        private route: ActivatedRoute,

    ) { }
    ngOnInit() {
        this.getList();
    }
    getList() {
        this.spinner.show();                                
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.type = 'paymentvoucher';
        this.title = 'Payment Voucher Approval';

        this.PaymentvoucherapprovalService.getItems().subscribe(
            data => {
                this.spinner.hide();
                this.paymentvoucherapprovalList = data['result']['data'];
            }, error => {
                console.log(error);
            });
    }
    updatePaymentvoucherRejectedItems() {
        var approvaloneIds = [];
        for (var i = 0; i < this.paymentvoucherapprovalList.length; i++) {
            if (this.paymentvoucherapprovalList[i].f093fapprovalStatus == true) {
                approvaloneIds.push(this.paymentvoucherapprovalList[i].f093fid);
            }
        }
        if (approvaloneIds.length > 0) {

        } else {
            alert("Select atleast one payment Voucher to Reject");
            return false;
        }

        var confirmPop = confirm("Do you want to Reject?");
        if (confirmPop == false) {
            return false;
        }
        console.log(this.paymentvoucherapprovalList);

        var approvaloneUpdate = {};
        approvaloneUpdate['id'] = approvaloneIds;
        // approvaloneUpdate['reason'] = this.reason;
        this.PaymentvoucherapprovalService.updatePaymentvoucherRejectedItems(approvaloneUpdate).subscribe(
            data => {
                alert("payment Voucher has been Rejected");

                this.getList();
            }, error => {
                console.log(error);
            });
    }
    paymentvoucherapprovalListData() {
        console.log(this.paymentvoucherapprovalList);
        var approvaloneIds = [];
        for (var i = 0; i < this.paymentvoucherapprovalList.length; i++) {
            if (this.paymentvoucherapprovalList[i].f093fapprovalStatus == true) {
                approvaloneIds.push(this.paymentvoucherapprovalList[i].f093fid);
            }
        }
        if (approvaloneIds.length > 0) {

        } else {
            alert("Select atleast one Payment Voucher to Approve");
            return false;
        }

        var confirmPop = confirm("Do you want to Approve?");
        if (confirmPop == false) {
            return false;
        }
        var approvaloneUpdate = {};
        approvaloneUpdate['id'] = approvaloneIds;
        approvaloneUpdate['status'] = '1';
        approvaloneUpdate['reason'] = this.paymentvoucherapprovalData['reason'];
        this.PaymentvoucherapprovalService.updatePaymentvoucherapprovalItems(approvaloneUpdate).subscribe(
            data => {
                this.paymentvoucherapprovalData['reason'] = '';

                alert("Payment Voucher has been approved successfully");

                this.getList();
            }, error => {
                console.log(error);
            });
    }

    rejectpaymentApprovalListData() {
        console.log(this.paymentvoucherapprovalList);
        var rejectoneIds = [];

        for (var i = 0; i < this.paymentvoucherapprovalList.length; i++) {
            if (this.paymentvoucherapprovalList[i].f093fapprovalStatus == true) {
                rejectoneIds.push(this.paymentvoucherapprovalList[i].f093fid);
            }
        }
        if (rejectoneIds.length > 0) {

        } else {
            alert("Select atleast one Payment Voucher to Reject");
            return false;
        }
        if (this.paymentvoucherapprovalData['reason'] == undefined) {
            alert(' Enter the Description');
            return false;
        }
        console.log(this.paymentvoucherapprovalData['reason']);

        if (this.paymentvoucherapprovalData['reason'] == '' || this.paymentvoucherapprovalData['reason'] == undefined) {
            alert("Enter the reason for Rejection !");
        } else {


            var confirmPop = confirm("Do you want to Reject?");
            if (confirmPop == false) {
                return false;
            }

            var rejecttwoUpdate = {};
            rejecttwoUpdate['id'] = rejectoneIds;
            rejecttwoUpdate['status'] = '2';
            rejecttwoUpdate['reason'] = this.paymentvoucherapprovalData['reason'];

            this.PaymentvoucherapprovalService.updatePaymentvoucherRejectedItems(rejecttwoUpdate).subscribe(
                data => {
                    alert("Payment Voucher has been Rejected ! ");
                    this.paymentvoucherapprovalData['reason'] = '';

                    this.getList();
                }, error => {
                    console.log(error);
                });

        }

    }
    selectAll() {
        console.log(this.selectAllCheckbox);
        for (var i = 0; i < this.paymentvoucherapprovalList.length; i++) {
            this.paymentvoucherapprovalList[i].f093fapprovedStatus = this.selectAllCheckbox;
        }
    }

}