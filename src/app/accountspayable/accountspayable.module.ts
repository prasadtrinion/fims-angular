import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HttpClient,HTTP_INTERCEPTORS } from '@angular/common/http';
import { QueryBuilderModule } from "angular2-query-builder";
import { MaterialModule } from '../material/material.module';
import { DataTableModule } from 'angular-6-datatable';
// import { ApprovallimitComponent } from './approvallimit/approvallimit.component';
// import { ApprovallimitFormComponent } from './approvallimit/approvallimitform/approvallimitform.component';
import { CreditorsetupComponent } from './creditorsetup/creditorsetup.component';
import { CreditorsetupformComponent } from './creditorsetup/creditorsetupform/creditorsetupform.component';

import { NonpoinvoiceComponent } from './nonpoinvoice/nonpoinvoice.component';
import { NonpoinvoiceFormComponent } from './nonpoinvoice/nonpoinvoiceform/nonpoinvoiceform.component';

import { ClaimcustomerComponent } from './claimcustomer/claimcustomer.component';
import { ClaimcustomerFormComponent } from './claimcustomer/claimcustomerform/claimcustomerform.component';
import { ClaimcustomerService } from './service/claimcustomer.service';
import { CustomerService } from '../accountsrecivable/service/customer.service';

import { PaymentvoucherService } from './../accountspayable/service/paymentvoucher.service';
import { PaymentvoucherComponent } from './paymentvoucher/paymentvoucher.component';
import { PaymentvoucherFormComponent } from './paymentvoucher/paymentvoucherform/paymentvoucherform.component';
import { PaymentvouchereditFormComponent } from "./paymentvoucher/paymentvouchereditform/paymentvouchereditform.component";
import { PaymentvoucherapprovalComponent } from './paymentvoucherapproval/paymentvoucherapproval.component';
import { PaymentvoucherapprovalService } from '../accountspayable/service/paymentvoucherapproval.service'

import { NonpoinvoiceService } from './../accountspayable/service/nonpoinvoice.service';
import { CreditorsetupService } from './service/creditorsetup.service';

import { BillRegistrationComponent } from "./billregistration/billregistration.component";
import { BillRegistrationformComponent  } from "./billregistration/billregistrationform/billregistrationform.component";
import { BillRegistrationService } from "./service/billregistration.service"
import { BillregistrationapprovalComponent } from "./billregistrationapproval/billregistrationapproval.component";
import { BillregistrationapprovalService } from "./service/billregistrationapproval.service";

import { NonpoinvoiceapprovalComponent } from './nonpoinvoiceapproval/nonpoinvoiceapproval.component';
import { NonpoinvoiceapprovalService } from '../accountspayable/service/nonpoinvoiceapproval.service'

import { PattycashallocationService } from './service/pattycashallocation.service';

import { PattycashallocationComponent } from './pattycashallocation/pattycashallocation.component';
import { PattycashallocationlistComponent } from './pattycashallocation/pattycashallocationlist/pattycashallocationlist.component';
import { PattycashentryService } from './service/pattycashentry.service';
import { PattycashentryComponent } from './pattycashentry/pattycashentry.component';
import { PattycashentryformComponent } from './pattycashentry/pattycashentryform/pattycashentryform.component';
import { PattycashapprovalComponent } from './pattycashapproval/pattycashapproval.component';
import { PattycashapprovalService } from './service/pattycashapproval.service';

import { PattycashReembarssementService } from "./service/reembarssement.service";
import { ReembarsementComponent } from "./reembarssemnt/reembarssement.component";

import { EtfService } from "./service/etf.service";
import { EtfvoucherComponent } from "./etfvoucher/etfvoucher.component";
 import { EtfvoucherFormComponent } from "./etfvoucher/etfvoucherform/etfvoucherform.component";
 import { EtfvoucherapprovalComponent } from "./etfvoucherapproval/etfvoucherapproval.component";
 import { EtfbatchService } from "./service/etfbatch.service";
 import { EtfvoucherbatchComponent } from "./etfvoucherbatch/etfvoucherbatch.component";
 import { EtfvoucherbatchFormComponent } from "./etfvoucherbatch/etfvoucherbatchform/etfvoucherbatchform.component";


import { DefinationmsService} from './../studentfinance/service/definationms.service'
import { TaxsetupcodeService } from '../generalledger/service/taxsetupcode.service'
import { CountryService } from './../generalsetup/service/country.service';
import { StateService } from './../generalsetup/service/state.service';
import { ApprovallimitService } from './service/approvallimit.service';
import { MaininvoiceService } from '../accountsrecivable/service/maininvoice.service';
import { BanksetupaccountService } from '../generalsetup/service/banksetupaccount.service';
import { VouchergenerationService } from '../accountsrecivable/service/vouchergeneration.service';
import { ItemService } from '../accountsrecivable/service/item.service';
import { DepartmentService } from '../generalsetup/service/department.service'
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { AccountspayableRoutingModule } from './accountspayable.router.module';

import { UserService } from '../generalsetup/service/user.service';
import { JwtInterceptor } from '../_helpers/jwt.interceptors';

import { NgSelectModule } from '@ng-select/ng-select';
import { SharedModule } from "../shared/shared.module";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
         DataTableModule,
        ReactiveFormsModule,
        MaterialModule,
        AccountspayableRoutingModule,
        Ng2SearchPipeModule,
        HttpClientModule,
        QueryBuilderModule,
        NgSelectModule,
        SharedModule

    ],
    exports: [],
    declarations: [
        PattycashallocationlistComponent,
        PattycashapprovalComponent,
        PattycashentryformComponent,
        PattycashallocationComponent,
        PattycashentryComponent,
        // ApprovallimitComponent,
        // ApprovallimitFormComponent,
        NonpoinvoiceComponent,
        NonpoinvoiceFormComponent,
        PaymentvoucherComponent,
        PaymentvoucherFormComponent,
        NonpoinvoiceapprovalComponent,
        PaymentvoucherapprovalComponent,
        CreditorsetupComponent,
        CreditorsetupformComponent,
        ClaimcustomerComponent,
        ClaimcustomerFormComponent,
        BillRegistrationComponent,
        BillRegistrationformComponent,
        BillregistrationapprovalComponent,
        PaymentvouchereditFormComponent,
        ReembarsementComponent,
        EtfvoucherComponent,
        EtfvoucherFormComponent,   
        EtfvoucherapprovalComponent,
        EtfvoucherbatchComponent,
        EtfvoucherbatchFormComponent
    ],
    providers: [
        PattycashapprovalService,
        DepartmentService,
        PattycashentryService,
        PattycashallocationService,
        ApprovallimitService,
        NonpoinvoiceService,
        CountryService,
        StateService,
        UserService,
        ItemService,
        EtfService,
        TaxsetupcodeService,
        MaininvoiceService,
        PaymentvoucherService,
        NonpoinvoiceapprovalService,
        PaymentvoucherapprovalService,
        CustomerService,
        DefinationmsService,
        CreditorsetupService,
        ClaimcustomerService,
        BanksetupaccountService,
        BillRegistrationService,
        BillregistrationapprovalService,
        VouchergenerationService,
        EtfbatchService,
        PattycashReembarssementService,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: JwtInterceptor,
            multi: true
          }
        
    ],
})
export class AccountspayableModule { }
