import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { faSearch, faEdit } from '@fortawesome/free-solid-svg-icons';
import { BillRegistrationService } from "../service/billregistration.service";
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'BillRegistrationComponent',
    templateUrl: 'billregistration.component.html'
})

export class BillRegistrationComponent implements OnInit {
    billRegistrationList = [];
    faSearch = faSearch;
    faEdit = faEdit;
    id: number;
    title: string;
    type: string;
    constructor(
        private BillRegistrationService: BillRegistrationService,
        private spinner: NgxSpinnerService,
        private route: ActivatedRoute
    ) { }

    ngOnInit() {
        this.spinner.show();                
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.BillRegistrationService.getItems().subscribe(
            data => {
                this.spinner.hide();
                this.billRegistrationList = data['result']['data'];
            }, error => {
                console.log(error);
            });
    }
}