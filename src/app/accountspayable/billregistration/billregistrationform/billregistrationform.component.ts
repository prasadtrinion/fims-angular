import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TaxsetupcodeService } from '../../../generalledger/service/taxsetupcode.service'
import { ItemsetupService } from '../../../procurement/service/itemsetup.service';
import { AlertService } from '../../../_services/alert.service';
import { BillRegistrationService } from "../../service/billregistration.service";
import { DefinationmsService } from '../../../studentfinance/service/definationms.service';
import { BanksetupaccountService } from '../../../generalsetup/service/banksetupaccount.service';

import { DepartmentService } from '../../../generalsetup/service/department.service';
import { FinancialyearService } from '../../../generalledger/service/financialyear.service';
import { FundService } from '../../../generalsetup/service/fund.service';
import { AccountcodeService } from '../../../generalsetup/service/accountcode.service';
import { ActivitycodeService } from '../../../generalsetup/service/activitycode.service';
import { SupplierregistrationService } from '../../../procurement/service/supplierregistration.service';

import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'BillRegistrationformComponent',
    templateUrl: 'billregistrationform.component.html'
})

export class BillRegistrationformComponent implements OnInit {
    billregistrationList = [];
    billregistrationData = {};
    billregistrationDataheader = {};
    itemList = [];
    taxcodeList = [];
    departmentList = [];
    financialList = [];
    DeptList = [];
    accountcodeList = [];
    fundList = [];
    activitycodeList = [];
    poApprovedList = [];
    itemUnitList = [];
    supplierList = [];
    purchaseMethods = [];
    purchaseOrderList = [];
    poListBasedOnOrderTypeList = [];
    id: number;
    valueDate: string;
    ajaxCount: number;
    gstValue: number;
    Amount: number;
    taxAmount: number;
    totalAmount: number;
    type: string;
    grnlist = [];
    grnitems = [];
    total: number;
    paymenttypeList = [];
    companyBankList = [];
    constructor(
        private BillRegistrationService: BillRegistrationService,
        private TaxsetupcodeService: TaxsetupcodeService,
        private ItemsetupService: ItemsetupService,
        private ActivitycodeService: ActivitycodeService,
        private AccountcodeService: AccountcodeService,
        private FundService: FundService,
        private alertService: AlertService,
        private SupplierregistrationService: SupplierregistrationService,
        private DepartmentService: DepartmentService,
        private FinancialyearService: FinancialyearService,
        private route: ActivatedRoute,
        private DefinationmsService:DefinationmsService,
        private router: Router,
        private BanksetupaccountService:BanksetupaccountService,
        private spinner: NgxSpinnerService,
    ) { }


    ngDoCheck() {
        const change = this.ajaxCount;
        if (this.ajaxCount == 0) {
            this.spinner.hide();
            this.editFunction();
            this.ajaxCount = 10;
        }
    }


    editFunction() {
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.BillRegistrationService.getItemsDetail(this.id).subscribe(
                data => {
                    this.billregistrationList = data['result'];
                    this.billregistrationDataheader = data['result'][0];//['f091freferenceNumber'];
                    for (var i = 0; i < this.billregistrationList.length; i++) {
                        this.billregistrationList[i]['f091funit'] = this.getUOM1(this.billregistrationList[i]['f091fidItem'], i);
                    }
                    console.log(this.billregistrationList);
                }, error => {
                    console.log(error);
                });
        }
    }

    getVendorName(){
        this.SupplierregistrationService.getActiveItems().subscribe(
            data => {
                console.log(data);
                this.supplierList = data['result']['data'];
                console.log(this.supplierList);
            }
        );
    }
    ngOnInit() {
        this.spinner.show();
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        console.log(this.id);
        this.ajaxCount = 0;
        //item dropdown
       

        this.ajaxCount++;
        this.BanksetupaccountService.getActiveCompanyBank().subscribe(
            data => {
            this.ajaxCount--;
                this.companyBankList = data['result']['data'];
                console.log(this.companyBankList);
            }, error => {
                console.log(error);
            });

     
         this.ajaxCount++;
         this.DefinationmsService.getPaymenttype('PaymentVoucherType').subscribe(
             data => {
             this.ajaxCount--;
                 this.paymenttypeList = data['result'];
                 console.log(this.paymenttypeList);
             }, error => {
                 console.log(error);
             });

    }
    getAllGrn() {
        console.log(this.billregistrationDataheader['f091fidVendor']);
        let billRegisterObject = {};
        billRegisterObject['vendorId'] = this.billregistrationDataheader['f091fidVendor'];
        this.BillRegistrationService.getAllGrnByVendorId(billRegisterObject).subscribe(
            data => {

                this.grnlist = data['result'];

            }, error => {
                console.log(error);
            });

    }
    showItem() {
        console.log(this.grnlist);
        let selectedGrn = [];
        for (var i = 0; i < this.grnlist.length; i++) {
            if (this.grnlist[i]['selectedGrn'] == true) {
                selectedGrn.push(this.grnlist[i]['f034fid']);

            }
        }
        let grnIdObject = {};
        grnIdObject['grnId'] = selectedGrn;
        this.BillRegistrationService.getAllGrnDetails(grnIdObject).subscribe(
            data => {
                console.log(data);
                this.grnitems = data['result'];
               
                // this.total = 0;
                for (var i = 0; i < this.grnitems.length; i++) {
                    this.total = (this.total) + this.grnitems[i]['f034fprice'];
                }
                this.billregistrationDataheader['f091ftotalAmount'] = this.total;
            }, error => {
                console.log(error);
            });

    }
    saveBillGeneration() {
        console.log(this.billregistrationList);
        console.log(this.billregistrationDataheader);
        let billRegistrationDetailsList = [];
        console.log(this.grnitems);
        this.total = 0;
        for (var i = 0; i < this.grnitems.length; i++) {
            let billdetails = {};
            billdetails['f091fidGrnDetails'] = this.grnitems[i]['f034fidDetails'];
            billdetails['f091fprice'] = this.grnitems[i]['f034fquantity'];
            billdetails['f091fquantity'] = this.grnitems[i]['f034fquantityReceived'];
            billdetails['f091ftotal'] = this.grnitems[i]['f034fprice'];
            this.total = (this.total) + this.grnitems[i]['f034fprice'];
            billRegistrationDetailsList.push(billdetails);
        }

        console.log(billRegistrationDetailsList);


        let billRegistrationObject = {};

        billRegistrationObject['f091ftotalAmount'] = this.total;
        billRegistrationObject['f091fidVendor'] = this.billregistrationDataheader['f088fidSupplier'];
        billRegistrationObject['bill-details'] = billRegistrationDetailsList;
        //  console.log(billRegistrationObject);
        //  return false;

        if (this.id > 0) {
            this.BillRegistrationService.updateBillregistrationItems(billRegistrationObject, this.id).subscribe(
                data => {

                    this.router.navigate(['accountspayable/billregistration']);
                    this.alertService.success(" Updated Sucessfully ! ");

                }, error => {
                    console.log(error);
                });
        } else {
            this.BillRegistrationService.insertBillregistrationItems(billRegistrationObject).subscribe(
                data => {


                    this.router.navigate(['accountspayable/billregistration']);
                    this.alertService.success(" Added Sucessfully ! ");

                }, error => {
                    console.log(error);
                });
        }
    }
    getUOM() {
        let itemId = this.billregistrationData['f091fidItem'];
        console.log(itemId);
        this.ItemsetupService.getItemsDetail(itemId).subscribe(
            data => {
                console.log(data);

                this.billregistrationData['f091funit'] = data['result'][0]['f029funit'];
            }
        );
    }
    getUOM1(id, index) {
        console.log(id);
        let unit;
        // this.ItemsetupService.getItemsDetail(id).subscribe(
        //     data => {
        //         //this.billregistrationList[index]['f091funit'] = data['result'][0]['f029funit'];
        //     }
        // );
        //return unit;
    }
    deleteData(object) {
        var cnf = confirm("Do you really want to delete");
        if (cnf == true) {
            console.log(this.billregistrationList);
            var index = this.billregistrationList.indexOf(object);;
            if (index > -1) {
                this.billregistrationList.splice(index, 1);
            }
        }
    }

    addData() {
        console.log(this.billregistrationData);
        var dataofCurrentRow = this.billregistrationData;
        this.billregistrationData = {};
        this.billregistrationList.push(dataofCurrentRow);
    }
    onlyNumberKey(event) {
        console.log(event);
        return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
    }

}