import { Injectable } from '@angular/core';
import { HttpClientModule, HttpHeaders, HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({
        'X-Auth-Client': 'qazwsx123',
        'Content-Type': 'application/json'
    })

};

@Injectable()

export class BillRegistrationService {

    url:string = environment.api.base + environment.api.endPoints.billRegistration;
    pendingUrl : string = environment.api.base + environment.api.endPoints.getBillRegistrationPendingList;
    PaymentApprovalStatusurl: string = environment.api.base + environment.api.endPoints.PaymentApprovalStatus;
    // creditNoteApproveurl: string = environment.api.base + environment.api.endPoints.creditNoteApprove;
    getGrnBasedOnVendorurl: string = environment.api.base + environment.api.endPoints.getGrnBasedOnVendorurl;
    allGrnDetailsUrl: string = environment.api.base + environment.api.endPoints.allGrnDetailsUrl;


    constructor(private httpClient: HttpClient) {
    }
    getItems() {
        return this.httpClient.get(this.url,httpOptions);
    }

    getPaymentStatusItems() {
        return this.httpClient.get(this.pendingUrl,httpOptions);
    }
    getAllGrnByVendorId(vendorId){
        return this.httpClient.post(this.getGrnBasedOnVendorurl, vendorId,httpOptions);
    }

    getAllGrnDetails(grnids){
        return this.httpClient.post(this.allGrnDetailsUrl, grnids,httpOptions);
    }

    getItemsDetail(id) {
        return this.httpClient.get(this.url+'/'+id,httpOptions);
    }

    insertBillregistrationItems(billregistrationData): Observable<any> {
        return this.httpClient.post(this.url, billregistrationData,httpOptions);
    }

    updateBillregistrationItems(billregistrationData,id): Observable<any> {
        return this.httpClient.put(this.url+'/'+id, billregistrationData,httpOptions);
       }
}