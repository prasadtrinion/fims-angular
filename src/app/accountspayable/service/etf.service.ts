import { Injectable } from '@angular/core';
import { HttpClientModule,HttpHeaders, HttpClient} from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({ 'X-Auth-Client': 'qazwsx123',
    'Content-Type': 'application/json' })
    
  };
  
@Injectable()

export class EtfService {
    url: string = environment.api.base + environment.api.endPoints.approvallimit;
    etfurl: string = environment.api.base + environment.api.endPoints.etf;
    getEtfApprovalsurl : string = environment.api.base + environment.api.endPoints.getEtfApprovals;
    etfApproveurl: string = environment.api.base + environment.api.endPoints.etfApprove;
    approveEtfDataurl : string = environment.api.base + environment.api.endPoints.approveEtfData;
    constructor(private httpClient: HttpClient) { 
        
    }
    
    getItems(id) {
        return this.httpClient.get(this.getEtfApprovalsurl+'/'+id,httpOptions);
    }

    getItemsDetail(id) {
        return this.httpClient.get(this.etfurl+'/'+id,httpOptions);
    }

    insertEtf(approvallimitData): Observable<any> {
        return this.httpClient.post(this.etfurl, approvallimitData,httpOptions);
    }
    approveEtf(approvallimitData): Observable<any> {
        return this.httpClient.post(this.approveEtfDataurl, approvallimitData,httpOptions);
    }

    updateApprovallimitItems(approvallimitData,id): Observable<any> {
        return this.httpClient.post(this.etfurl+'/'+id, approvallimitData,httpOptions);
       }
}