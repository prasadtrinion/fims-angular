import { Injectable } from '@angular/core';
import { HttpClientModule, HttpHeaders, HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({
        'X-Auth-Client': 'qazwsx123',
        'Content-Type': 'application/json'
    })

};
@Injectable()

export class PattycashapprovalService {

    url: string = environment.api.base + environment.api.endPoints.getApprovedPattyCashEntry
    approvalPuturl: string = environment.api.base + environment.api.endPoints.approvePettyCashEntry
    rejectUrl: string = environment.api.base + environment.api.endPoints.approvePettyCashEntry;
    //  approveoneurl: string = environment.api.base + environment.api.endPoints.rejectOne
    constructor(private httpClient: HttpClient) {
    }
    getItems() {
        return this.httpClient.get(this.url + '/' + 0, httpOptions);
    }
    getItemsDetail(id) {
        return this.httpClient.get(this.url + '/' + id, httpOptions);
    }
    insertPattycashapprovalItems(pattycashapprovalData): Observable<any> {
        return this.httpClient.post(this.approvalPuturl, pattycashapprovalData, httpOptions);
    }
    updatePattycashapprovalItems(pattycashapprovalData): Observable<any> {
        return this.httpClient.post(this.approvalPuturl, pattycashapprovalData, httpOptions);
    }
    updatePattycashapprovalRejectedItems(pattycashapprovalData): Observable<any> {
        return this.httpClient.post(this.rejectUrl, pattycashapprovalData, httpOptions);
    }
    rejectPattycashapproval(pattycashapprovalData): Observable<any> {
        return this.httpClient.post(this.rejectUrl, pattycashapprovalData, httpOptions);
    }
}
