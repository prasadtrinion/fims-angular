import { Injectable } from '@angular/core';
import { HttpClientModule,HttpHeaders, HttpClient} from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({ 'X-Auth-Client': 'qazwsx123',
    'Content-Type': 'application/json' })
    
  };
  
@Injectable()

export class PaymentvoucherService {
    url: string = environment.api.base + environment.api.endPoints.paymentvoucher;
    getpaymentNumberUrl : string = environment.api.base + environment.api.endPoints.generateNumber;
    getClaimCustomerUrl : string = environment.api.base + environment.api.endPoints.getClaimcustomerVoucher;
    getClaimCustomertypeUrl : string = environment.api.base + environment.api.endPoints.getClaimcustomerType;
    getVoucherListUrl : string = environment.api.base + environment.api.endPoints.getVoucherList;
    getStudentTypeUrl : string = environment.api.base + environment.api.endPoints.getStudentType; 
    getStudentVoucherUrl : string = environment.api.base + environment.api.endPoints.getStudentVoucher; 
    getBillRegistrationByVendorUrl : string = environment.api.base + environment.api.endPoints.getBillRegistrationByVendor; 


    constructor(private httpClient: HttpClient) { 
        
    }

    getItems() {
        return this.httpClient.get(this.url,httpOptions);
    }

    getBillRegistrationByVendor(id){
        return this.httpClient.get(this.getBillRegistrationByVendorUrl+'/'+id,httpOptions);
    }

    getItemsDetail(id) {
        return this.httpClient.get(this.url+'/'+id,httpOptions);
    }

    getClaimcustomerVoucher(id) {
        return this.httpClient.get(this.getClaimCustomerUrl+'/'+id,httpOptions);
    }
    getClaimCustomerlist(typeId) {
        return this.httpClient.get(this.getClaimCustomertypeUrl+'/'+typeId,httpOptions);

    }
    getStudentTypeList(typeId){
        return this.httpClient.get(this.getStudentTypeUrl+'/'+typeId,httpOptions);
        
    }
    getStudentVoucherList(idStu){
        return this.httpClient.get(this.getStudentVoucherUrl+'/'+idStu,httpOptions);

    }
    
    getVoucherList(idCust){
        return this.httpClient.get(this.getVoucherListUrl+'/'+idCust,httpOptions);

    }
    getPaymentNumber(data) {
        return this.httpClient.post(this.getpaymentNumberUrl, data,httpOptions);

    }
    insertpaymentvoucherItems(paymentvoucherData): Observable<any> {
        return this.httpClient.post(this.url, paymentvoucherData,httpOptions);
    }

    updatepaymentvoucherItems(paymentvoucherData,id): Observable<any> {
        return this.httpClient.put(this.url+'/'+id, paymentvoucherData,httpOptions);
       }
}