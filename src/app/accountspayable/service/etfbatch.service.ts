import { Injectable } from '@angular/core';
import { HttpClientModule,HttpHeaders, HttpClient} from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({ 'X-Auth-Client': 'qazwsx123',
    'Content-Type': 'application/json' })
    
  };
  
@Injectable()

export class EtfbatchService {
    etfGroupingurl: string = environment.api.base + environment.api.endPoints.etfGrouping;
    constructor(private httpClient: HttpClient) { 
        
    }
    
    getItems() {
        return this.httpClient.get(this.etfGroupingurl,httpOptions);
    }
    getItemsDetail(id) {
        return this.httpClient.get(this.etfGroupingurl+'/'+id,httpOptions);
    }
    insertEtfBatch(approvallimitData): Observable<any> {
        return this.httpClient.post(this.etfGroupingurl, approvallimitData,httpOptions);
    }

   
}