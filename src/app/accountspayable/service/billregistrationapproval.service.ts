import { Injectable } from '@angular/core';
import { HttpClientModule, HttpHeaders, HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({
        'X-Auth-Client': 'qazwsx123',
        'Content-Type': 'application/json'
    })

};

@Injectable()

export class BillregistrationapprovalService {

    getPaymentApprovalListurl: string = environment.api.base + environment.api.endPoints.getBillRegistrationApprovedList;
    PaymentApprovalStatusurl: string = environment.api.base + environment.api.endPoints.billRegistrationApproval;
    // creditNoteApproveurl: string = environment.api.base + environment.api.endPoints.creditNoteApprove;
    rejectUrl : string = environment.api.base + environment.api.endPoints.cashApplicationReject;

    constructor(private httpClient: HttpClient) {
    }
    getItems() {
        return this.httpClient.get(this.getPaymentApprovalListurl + '/' + 0, httpOptions);
    }
    getItemsDetail(id) {
        return this.httpClient.get(this.getPaymentApprovalListurl + '/' + id, httpOptions);
    }
    insertBillregistrationapprovalItems(approvalData): Observable<any> {
        return this.httpClient.post(this.PaymentApprovalStatusurl, approvalData,httpOptions);
    }
    updateBillregistrationapprovalItems(approvalData): Observable<any> {
        return this.httpClient.post(this.PaymentApprovalStatusurl, approvalData,httpOptions);
    }

    updateBillregistrationRejectedItems(approvalData): Observable<any> {
        return this.httpClient.post(this.PaymentApprovalStatusurl, approvalData,httpOptions);
    }
    rejectBillregistrationapprovalTwo(approvalData): Observable<any> {
        return this.httpClient.post(this.rejectUrl, approvalData,httpOptions);
    }

    getBillregistrationapprovalDetailsById(id) {
        return this.httpClient.get(this.getPaymentApprovalListurl + '/' + id, httpOptions);
    }

    updateApproval(approvalData): Observable<any> {
        return this.httpClient.post(this.PaymentApprovalStatusurl, approvalData, httpOptions);
    }


}