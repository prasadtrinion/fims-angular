import { Injectable } from '@angular/core';
import { HttpClientModule, HttpHeaders, HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({
        'X-Auth-Client': 'qazwsx123',
        'Content-Type': 'application/json'
    })

};

@Injectable()

export class NonpoinvoiceapprovalService {

    nonpoinvoiceurl:string = environment.api.base + environment.api.endPoints.nonpoinvoiceapproavl;
    approvalStatusurl: string = environment.api.base + environment.api.endPoints.approvalStatus;

    constructor(private httpClient: HttpClient) {
    }
    getItems() {
            return this.httpClient.get(this.nonpoinvoiceurl + '/' + 0, httpOptions);
    }
    getItemsDetail(id) {
        return this.httpClient.get(this.nonpoinvoiceurl+'/' + id, httpOptions);
    }
    getNonpoinvoiceDetailsById(id) {
        return this.httpClient.get(this.nonpoinvoiceurl+'/'+id,httpOptions);
}

    updateApproval(approvalData): Observable<any> {
            return this.httpClient.post(this.approvalStatusurl, approvalData, httpOptions);
    }


}