import { Injectable } from '@angular/core';
import { HttpClientModule, HttpHeaders, HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({
        'X-Auth-Client': 'qazwsx123',
        'Content-Type': 'application/json'
    })

};

@Injectable()

export class PaymentvoucherapprovalService {

    getPaymentApprovalListurl:string = environment.api.base + environment.api.endPoints.getPaymentApprovalList;
    PaymentApprovalStatusurl: string = environment.api.base + environment.api.endPoints.PaymentApprovalStatus;
    // creditNoteApproveurl: string = environment.api.base + environment.api.endPoints.creditNoteApprove;
    rejectUrl : string = environment.api.base + environment.api.endPoints.PaymentApprovalStatus;

    constructor(private httpClient: HttpClient) {
    }
    getItems() {
            return this.httpClient.get(this.getPaymentApprovalListurl + '/' + 0, httpOptions);
    }

    getItemsApprovedOnly(id) {
        return this.httpClient.get(this.getPaymentApprovalListurl + '/' + id, httpOptions);
}
    getItemsDetail(id) {
        return this.httpClient.get(this.getPaymentApprovalListurl+'/' + id, httpOptions);
    }
    insertPaymentvoucherapprovalItems(approvalData): Observable<any> {
        return this.httpClient.post(this.PaymentApprovalStatusurl, approvalData,httpOptions);
    }
    updatePaymentvoucherapprovalItems(approvalData): Observable<any> {
        return this.httpClient.post(this.PaymentApprovalStatusurl, approvalData,httpOptions);
    }
    updatePaymentvoucherRejectedItems(approvalData): Observable<any> {
        return this.httpClient.post(this.PaymentApprovalStatusurl, approvalData,httpOptions);
    }
    rejectApprovalTwo(approvalData): Observable<any> {
        return this.httpClient.post(this.rejectUrl, approvalData,httpOptions);
    }
    getPaymentvoucherapprovalDetailsById(id) {
        return this.httpClient.get(this.getPaymentApprovalListurl+'/'+id,httpOptions);
}

    updateApproval(approvalData): Observable<any> {
            return this.httpClient.post(this.PaymentApprovalStatusurl, approvalData, httpOptions);
    }


}