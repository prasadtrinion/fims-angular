
import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
// import { ApprovalService } from '../service/approval.service'
import { PattycashReembarssementService } from "../service/reembarssement.service";
import { AlertService } from '../../_services/alert.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'ReembarssementComponent',
    templateUrl: 'reembarssement.component.html'
})

export class ReembarsementComponent implements OnInit {
    rejecttwoList = [];
    pattycashapprovalList = [];
    pattycashapprovalData = {};
    selectAllCheckbox = true;
    reason: string;

    constructor(
        private PattycashReembarssementService: PattycashReembarssementService,
        private spinner: NgxSpinnerService,
        private AlertService: AlertService
    ) { }

    ngOnInit() {
        this.getListData();
    }
    getListData() {
        this.spinner.show();
        this.PattycashReembarssementService.getItems().subscribe(
            data => {
                this.spinner.hide();
                this.pattycashapprovalList = data['result'];
            }, error => {
                console.log(error);
            });
    }
    pattycashapprovalListData() {
        console.log(this.pattycashapprovalList);
        var approvaloneIds = [];
        for (var i = 0; i < this.pattycashapprovalList.length; i++) {
            if (this.pattycashapprovalList[i].Status == true) {
                approvaloneIds.push(this.pattycashapprovalList[i].f123fid);
            }
        }
        if (approvaloneIds.length > 0) {
        } else {
            alert("Select atleast one petty Cash to Reimbursement");
            return false;
        }

        var confirmPop = confirm("Do you want to Reimbursement?");
        if (confirmPop == false) {
            return false;
        }
        var approvaloneUpdate = {};
        approvaloneUpdate['id'] = approvaloneIds;
        approvaloneUpdate['reambers'] = 1;
        this.PattycashReembarssementService.insertPattycashapprovalItems(approvaloneUpdate).subscribe(
            data => {
                alert(" Petty Cash has been Reimbursement successfully");

                this.getListData();
            }, error => {
                console.log(error);
            });
    }

}
