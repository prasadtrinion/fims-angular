import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Injector } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { EtfService } from '../service/etf.service'
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'Etfvoucherapproval',
    templateUrl: 'etfvoucherapproval.component.html'
})

export class EtfvoucherapprovalComponent implements OnInit {
    paymentvoucherapprovalList = [];
    paymentvoucherapprovalData = {};
    id: number;
    selectAllCheckbox: false;
    type: string;
    title = '';
    etfDetails = [];

    constructor(
        private EtfService: EtfService,
        private spinner: NgxSpinnerService,                
        private route: ActivatedRoute,

    ) { }
    ngOnInit() {
        this.getList();
    }
    getList() {
        this.EtfService.getItems(0).subscribe(
            data => {
                console.log(data);
                // this.etfDetails = data['data'];
                let activityData = [];
                activityData = data['data'];
                console.log("asdf");
                for (var i = 0; i < activityData.length; i++) {
                    if (activityData[i]['f136fstatus'] == 0) {
                        activityData[i]['f136fstatus'] = 'Pending';
                    } else if (activityData[i]['f136fstatus'] == 1) {
                        activityData[i]['f136fstatus'] = 'Approved';
                    } else {
                        activityData[i]['f136fstatus'] = 'Rejected';
                    }
                }
                this.etfDetails = activityData;
            }, error => {
                console.log(error);
            });
        
    }
    approveEtf() {
        let idEtf = [];
        let approveEtf = {};
        for(var i=0;i<this.etfDetails.length;i++) {
            if(this.etfDetails[i]['voucherChecked']==true) {
                idEtf.push(this.etfDetails[i]['f136fid']);
            }
        }    
        approveEtf['id'] = idEtf;
        approveEtf['reason']='';
        approveEtf['status']=1;
        this.EtfService.approveEtf(approveEtf).subscribe(
            data => {
                this.getList();
            }, error => {
                console.log(error);
            });
        

        console.log(idEtf);

    }
    paymentvoucherapprovalListData() {
        
    }

    rejectpaymentApprovalListData() {
       

    }
    selectAll() {
        console.log(this.selectAllCheckbox);
        for (var i = 0; i < this.paymentvoucherapprovalList.length; i++) {
            this.paymentvoucherapprovalList[i].f093fapprovedStatus = this.selectAllCheckbox;
        }
    }

}