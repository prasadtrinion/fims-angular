import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { PaymentvoucherService } from '../../service/paymentvoucher.service';
import { PaymentvoucherapprovalService } from '../../service/paymentvoucherapproval.service';
import { BanksetupaccountService } from '../../../generalsetup/service/banksetupaccount.service';

import { DefinationmsService } from '../../../studentfinance/service/definationms.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { EtfService } from '../../service/etf.service'
import { NgxSpinnerService } from 'ngx-spinner';
import { SupplierregistrationService } from '../../../procurement/service/supplierregistration.service';

@Component({
    selector: 'EtfvoucherFormComponent',
    templateUrl: 'etfvoucherform.component.html'
})

export class EtfvoucherFormComponent implements OnInit {
    paymentvoucherDataheader = {};
    paymentvoucherform: FormGroup;

    payType: string;
    custId: number;
    studentId: number;
    valueDate: string;
    billRegistrationList = [];
    id: number;
    newBillRegistration = [];
    sourceList = [];
    viewDisabled: boolean;
    saveBtnDisable: boolean;
    viewid: number;
    ajaxCount: number;
    vendorsupplierList = [];
    originalData = [];
    paymentvoucherList = [];
    paymenttypeList = [];
    paymentmodeList = [];
    companyBankList = [];
    totalAmount = 0;
    constructor(
        private PaymentvoucherService: PaymentvoucherService,
        private DefinationmsService: DefinationmsService,
        private spinner: NgxSpinnerService,
        private SupplierregistrationService: SupplierregistrationService,
        private EtfService: EtfService,
        private PaymentvoucherapprovalService: PaymentvoucherapprovalService,
        private BanksetupaccountService: BanksetupaccountService,
        private route: ActivatedRoute,
        private router: Router
    ) { }
    ngDoCheck() {
        if (this.ajaxCount == 0) {
            this.spinner.hide();
        }
    }

    ngOnInit() {

        // company bank 
        this.ajaxCount++;
        this.BanksetupaccountService.getActiveCompanyBank().subscribe(
            data => {
                this.ajaxCount--;
                this.companyBankList = data['result']['data'];
                console.log(this.companyBankList);
            }, error => {
                console.log(error);
            });

        this.ajaxCount++;
        this.DefinationmsService.getPaymenttype('PaymentVoucherType').subscribe(
            data => {
                this.ajaxCount--;
                this.paymenttypeList = data['result'];
                console.log(this.paymenttypeList);
            }, error => {
                console.log(error);
            });

        // payment mode dropdown
        this.ajaxCount++;
        this.DefinationmsService.getPaymentmode('PaymentMode').subscribe(
            data => {
                this.ajaxCount--;
                this.paymentmodeList = data['result'];
            }, error => {
                console.log(error);
            });

        this.PaymentvoucherapprovalService.getItemsApprovedOnly(1).subscribe(
            data => {
                this.spinner.hide();
                this.paymentvoucherList = data['result']['data'];
                for (var i = 0; i < this.paymentvoucherList.length; i++) {
                    this.paymentvoucherList[i]['f137fvoucherId'] = this.paymentvoucherList[i]['f093fid'];
                    this.paymentvoucherList[i]['f137fpayToId'] = this.paymentvoucherList[i]['f093fidSupplier'];
                    this.paymentvoucherList[i]['f137fpayToBank'] = this.paymentvoucherList[i]['f093fidBank'];
                    this.paymentvoucherList[i]['f137fpayToaccount'] = this.paymentvoucherList[i]['f030faccountNumber'];
                    this.paymentvoucherList[i]['f137fpayToEmail'] = this.paymentvoucherList[i]['vendorEmail'];

                }
            }, error => {
                console.log(error);
            });
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.ajaxCount++;
            this.EtfService.getItemsDetail(this.id).subscribe(
                data => {
                    this.ajaxCount--;
                    this.paymentvoucherList = data['result'];
                    this.paymentvoucherDataheader['f136fbatchId'] = data['result'][0]['f136fbatchId'];
                    this.paymentvoucherDataheader['f136fvoucherType'] = data['result'][0]['f136fvoucherType'];
                    this.paymentvoucherDataheader['f136fpaymentMode'] = data['result'][0]['f136fpaymentMode'];
                    this.paymentvoucherDataheader['f136fuumBank'] = data['result'][0]['f136fuumBank'];
                    this.paymentvoucherDataheader['f136freferenceNumber'] = data['result'][0]['f136freferenceNumber'];
                    this.paymentvoucherDataheader['f136fpayDate'] = data['result'][0]['f136fpayDate'];
                    this.paymentvoucherDataheader['f136fletterReference'] = data['result'][0]['f136fletterReference'];

                }, error => {
                    console.log(error);
                });
        }

    }

    savePaymentvoucher() {
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        let indobject = {};
        let detailsArray = [];
        console.log(this.paymentvoucherList);
        console.log(this.paymentvoucherDataheader);
        this.totalAmount = 0;
        for (var i = 0; i < this.paymentvoucherList.length; i++) {
            if (this.paymentvoucherList[i]['selectedvalue'] == true) {
                detailsArray.push(this.paymentvoucherList[i]);
                this.totalAmount = this.totalAmount + this.paymentvoucherList[i]['f093ftotalAmount'];

            }
        }
        this.paymentvoucherDataheader['f136famount'] = this.totalAmount;
        this.paymentvoucherDataheader['details'] = detailsArray;
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.EtfService.updateApprovallimitItems(this.paymentvoucherDataheader, this.id).subscribe(
                data => {
                    this.router.navigate(['accountspayable/etf']);
                    alert("SAB has been Updated Successfully");
                }, error => {
                    console.log(error);
                });
        } else {
            this.EtfService.insertEtf(this.paymentvoucherDataheader).subscribe(
                data => {
                    this.router.navigate(['accountspayable/etf']);
                    alert("SAB has been Added Successfully");
                }, error => {
                    console.log(error);
                });
        }

    }



}