import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { EtfService } from '../service/etf.service'
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'etfvoucher',
    templateUrl: 'etfvoucher.component.html'
})

export class EtfvoucherComponent implements OnInit {
    etfvoucherList =  [];
    etfvoucherData = {};
    id: number;
    etfDetails = [];
    constructor(
        private EtfService: EtfService,
        private spinner: NgxSpinnerService,                
    ) { }

    ngOnInit() { 
        this.EtfService.getItems(5).subscribe(
            data => {
                console.log(data);
                // this.etfDetails = data['data'];
                let activityData = [];
                activityData = data['data'];
                console.log("asdf");
                for (var i = 0; i < activityData.length; i++) {
                    if (activityData[i]['f136fstatus'] == 0) {
                        activityData[i]['f136fstatus'] = 'Pending';
                    } else if (activityData[i]['f136fstatus'] == 1) {
                        activityData[i]['f136fstatus'] = 'Approved';
                    } else {
                        activityData[i]['f136fstatus'] = 'Rejected';
                    }
                }
                this.etfDetails = activityData;
            }, error => {
                console.log(error);
            });
    }
}