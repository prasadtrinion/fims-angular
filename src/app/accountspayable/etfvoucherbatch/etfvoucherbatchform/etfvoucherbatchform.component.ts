import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { PaymentvoucherService } from '../../service/paymentvoucher.service';
import { PaymentvoucherapprovalService } from '../../service/paymentvoucherapproval.service';
import { BanksetupaccountService } from '../../../generalsetup/service/banksetupaccount.service';

import { DefinationmsService } from '../../../studentfinance/service/definationms.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { EtfbatchService } from '../../service/etfbatch.service'
import { EtfService } from '../../service/etf.service'

import { NgxSpinnerService } from 'ngx-spinner';
import { SupplierregistrationService } from '../../../procurement/service/supplierregistration.service';

@Component({
    selector: 'EtfvoucherbatchFormComponent',
    templateUrl: 'etfvoucherbatchform.component.html'
})

export class EtfvoucherbatchFormComponent implements OnInit {
    paymentvoucherDataheader = {};
    paymentvoucherform: FormGroup;

    payType: string;
    custId: number;
    studentId: number;
    valueDate: string;
    billRegistrationList = [];
    id: number;
    newBillRegistration = [];
    sourceList = [];
    viewDisabled: boolean;
    saveBtnDisable: boolean;
    viewid: number;
    ajaxCount: number;
    vendorsupplierList = [];
    originalData = [];
    paymentvoucherList = [];
    etfDetails = [];
    companyBankList = [];
    paymenttypeList = [];
    paymentmodeList = [];
    constructor(
        private PaymentvoucherService: PaymentvoucherService,
        private DefinationmsService: DefinationmsService,
        private spinner: NgxSpinnerService,
        private EtfService: EtfService,
        private BanksetupaccountService: BanksetupaccountService,
        private SupplierregistrationService: SupplierregistrationService,
        private EtfbatchService: EtfbatchService,
        private PaymentvoucherapprovalService: PaymentvoucherapprovalService,
        private route: ActivatedRoute,
        private router: Router
    ) { }
    ngDoCheck() {
        if (this.ajaxCount == 0) {
            this.spinner.hide();
        }
    }

    ngOnInit() {
        // company bank 
        this.ajaxCount++;
        this.BanksetupaccountService.getActiveCompanyBank().subscribe(
            data => {
                this.ajaxCount--;
                this.companyBankList = data['result']['data'];
                console.log(this.companyBankList);
            }, error => {
                console.log(error);
            });

        this.ajaxCount++;
        this.DefinationmsService.getPaymenttype('PaymentVoucherType').subscribe(
            data => {
                this.ajaxCount--;
                this.paymenttypeList = data['result'];
                console.log(this.paymenttypeList);
            }, error => {
                console.log(error);
            });

        // payment mode dropdown
        this.ajaxCount++;
        this.DefinationmsService.getPaymentmode('PaymentMode').subscribe(
            data => {
                this.ajaxCount--;
                this.paymentmodeList = data['result'];
            }, error => {
                console.log(error);
            });

        this.EtfService.getItems(1).subscribe(
            data => {
                console.log(data);
                // this.etfDetails = data['data'];

                let activityData = [];
                activityData = data['data'];
                console.log("asdf");
                for (var i = 0; i < activityData.length; i++) {
                    if (activityData[i]['f136fstatus'] == 0) {
                        activityData[i]['f136fstatus'] = 'Pending';
                    } else if (activityData[i]['f136fstatus'] == 1) {
                        activityData[i]['f136fstatus'] = 'Approved';
                    } else {
                        activityData[i]['f136fstatus'] = 'Rejected';
                    }
                }
                this.etfDetails = activityData;
                for (var i = 0; i < this.etfDetails.length; i++) {
                    this.etfDetails[i]['f139fidEtf'] = this.etfDetails[i]['f136fid'];
                    this.etfDetails[i]['f139famount'] = this.etfDetails[i]['f136famount'];
                }
            }, error => {
                console.log(error);
            });
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.ajaxCount++;
            this.EtfbatchService.getItemsDetail(this.id).subscribe(
                data => {
                    this.ajaxCount--;
                    this.paymentvoucherList = data['result'];
                    this.paymentvoucherDataheader['f138freferenceNumber'] = data['result'][0]['f138freferenceNumber'];
                    this.paymentvoucherDataheader['f138fdescription'] = data['result'][0]['f138fdescription'];
                    this.paymentvoucherDataheader['f138famount'] = data['result'][0]['f138famount'];

                }, error => {
                    console.log(error);
                });
        }

    }
    savePaymentvoucher() {
        console.log(this.etfDetails);
        console.log(this.paymentvoucherDataheader);
        let indobject = {};
        let detailsArray = [];
        console.log(this.etfDetails);
        console.log(this.paymentvoucherDataheader);
        for (var i = 0; i < this.etfDetails.length; i++) {
            if (this.etfDetails[i]['voucherChecked'] == true) {
                detailsArray.push(this.etfDetails[i]);
            }
        }
        this.paymentvoucherDataheader['details'] = detailsArray;
        this.EtfbatchService.insertEtfBatch(this.paymentvoucherDataheader).subscribe(
            data => {
                this.router.navigate(['accountspayable/etfbatch']);
            }, error => {
                console.log(error);
            });


    }



}