import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { EtfbatchService } from '../service/etfbatch.service'
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'etfvoucherbatch',
    templateUrl: 'etfvoucherbatch.component.html'
})

export class EtfvoucherbatchComponent implements OnInit {
    etfvoucherList =  [];
    etfvoucherData = {};
    id: number;
    etfGroupDetails = [];

    constructor(
        private EtfbatchService: EtfbatchService,
        private spinner: NgxSpinnerService,                

    ) { }

    ngOnInit() { 
        this.EtfbatchService.getItems().subscribe(
            data => {
                console.log(data);
                // this.etfGroupDetails = data['data'];
                let activityData = [];
                activityData = data['data'];
                console.log("asdf");
                for (var i = 0; i < activityData.length; i++) {
                    if (activityData[i]['f138fstatus'] == 0) {
                        activityData[i]['f138fstatus'] = 'Pending';
                    } else if (activityData[i]['f138fstatus'] == 1) {
                        activityData[i]['f138fstatus'] = 'Approved';
                    } else {
                        activityData[i]['f138fstatus'] = 'Rejected';
                    }
                }
                this.etfGroupDetails = activityData;
            }, error => {
                console.log(error);
            });
    }
}