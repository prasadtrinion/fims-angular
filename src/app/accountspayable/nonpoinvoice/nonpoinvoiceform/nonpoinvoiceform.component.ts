import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { NonpoinvoiceService } from '../../service/nonpoinvoice.service';
import { GlcodeService } from '../../../generalsetup/service/glcode.service'
import { ItemService } from '../../../accountsrecivable/service/item.service';
import { TaxsetupcodeService } from '../../../generalledger/service/taxsetupcode.service';
import { MaininvoiceService } from '../../../accountsrecivable/service/maininvoice.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
    selector: 'NonpoinvoiceFormComponent',
    templateUrl: 'nonpoinvoiceform.component.html'
})

export class NonpoinvoiceFormComponent implements OnInit {
    nonpoinvoiceDataheader = {};
    nonpoinvoiceform: FormGroup;
    nonpoinvoiceList = [];
    nonpoinvoiceData = {};
    itemList = [];
    taxcodeList = [];
    glcodeList = [];
    glcodeData = {};
    maininvoiceList = [];
    maininvoiceData = {};
    id: number;

    constructor(
        private NonpoinvoiceService: NonpoinvoiceService,
        private GlcodeService: GlcodeService,
        private ItemService: ItemService,
        private TaxsetupcodeService: TaxsetupcodeService,
        private MaininvoiceService: MaininvoiceService,
        private route: ActivatedRoute,
        private router: Router
    ) { }

    ngOnInit() {
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        // Gl Code dropdown
        this.GlcodeService.getGlItems().subscribe(
            data => {
                this.glcodeList = data['result']['data'];
            }, error => {
                console.log(error);
            });

        // item Code dropdown
        this.ItemService.getItems().subscribe(
            data => {
                this.itemList = data['result'];
            }, error => {
                console.log(error);
            });

        // Tax Code dropdown
        this.TaxsetupcodeService.getItems().subscribe(
            data => {
                this.taxcodeList = data['result']['data'];
            }, error => {
                console.log(error);
            });
        //invoice dropdown
        this.MaininvoiceService.getItems().subscribe(
            data => {
                this.maininvoiceList = data['result']['data'];
            }
        );
    }

    saveNonpoinvoice() {
        let nonpoinvoiceObject = {};
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            nonpoinvoiceObject['f091fid'] = this.id;
        }
        nonpoinvoiceObject['f091fidInvoice'] = this.nonpoinvoiceDataheader['f091fidInvoice'];
        nonpoinvoiceObject['f091fidVendor'] = this.nonpoinvoiceDataheader['f091fidVendor'];
        nonpoinvoiceObject['f091finvoiceDate'] = this.nonpoinvoiceDataheader['f091finvoiceDate'];
        nonpoinvoiceObject['f091freceivedDate'] = this.nonpoinvoiceDataheader['f091freceivedDate'];
        nonpoinvoiceObject['f091famount'] = this.nonpoinvoiceDataheader['f091famount'];
        nonpoinvoiceObject['NonPoInvoice-Details'] = this.nonpoinvoiceList;

        this.NonpoinvoiceService.insertnonpoinvoiceItems(nonpoinvoiceObject).subscribe(
            data => {
                this.router.navigate(['accountspayable/nonpoinvoice']);

            }, error => {
                console.log(error);
            });
    }
    deleteNonpoinvoice(object) {
        var cnf = confirm("Do you really want to delete");
        if (cnf == true) {
            console.log(this.nonpoinvoiceList);
            var index = this.nonpoinvoiceList.indexOf(object);;
            if (index > -1) {
                this.nonpoinvoiceList.splice(index, 1);
            }
        }
    }
    addnonpoinvoicelist() {
        console.log(this.nonpoinvoiceData);
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        var dataofCurrentRow = this.nonpoinvoiceData;
        this.nonpoinvoiceList.push(dataofCurrentRow);
        console.log(this.nonpoinvoiceList);
        this.nonpoinvoiceData = {};
    }
}