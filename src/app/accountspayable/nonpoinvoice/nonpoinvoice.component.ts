import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NonpoinvoiceService } from '../service/nonpoinvoice.service'


@Component({
    selector: 'nonpoinvoice',
    templateUrl: 'nonpoinvoice.component.html'
})

export class NonpoinvoiceComponent implements OnInit {
    nonpoinvoiceList = [];
    nonpoinvoiceData = {};
    id: number;

    constructor(
        private NonpoinvoiceService: NonpoinvoiceService

    ) { }

    ngOnInit() {
        this.NonpoinvoiceService.getItems().subscribe(
            data => {
                this.nonpoinvoiceList = data['result']['data'];
            }, error => {
                console.log(error);
            });
    }
}