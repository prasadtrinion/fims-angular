import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Injector } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NonpoinvoiceapprovalService } from '../service/nonpoinvoiceapproval.service'


@Component({
    selector: 'Nonpoinvoiceapproval',
    templateUrl: 'nonpoinvoiceapproval.component.html'
})

export class NonpoinvoiceapprovalComponent implements OnInit {
    nonpoinvoiceapprovalList = [];
    nonpoinvoiceapprovalData = {};
    id: number;
    selectAllCheckbox: false;
    type :string;
    title = '';

    constructor(

        private NonpoinvoiceapprovalService: NonpoinvoiceapprovalService,
        private route: ActivatedRoute,
        
    ) { }
    ngOnInit() {
        this.getList();
    }
    getList() {
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
            this.type='nonpoinvoice';
            this.title='Nonpoinvoice Approval';
          
         this.NonpoinvoiceapprovalService.getItems().subscribe(
            data => {
                this.nonpoinvoiceapprovalList = data['result'];
        }, error => {
            console.log(error);
        });
    }
    nonpoinvoiceapprovalListData() {
        let approveDarrayList = [];
        console.log(this.nonpoinvoiceapprovalList);
        for (var i = 0; i < this.nonpoinvoiceapprovalList.length; i++) {
            if (this.nonpoinvoiceapprovalList[i].f079fstatus == true) {
                approveDarrayList.push(this.nonpoinvoiceapprovalList[i].f091fid);
            }
        }
        var approvalObject = {};
        approvalObject['id'] = approveDarrayList;

        this.NonpoinvoiceapprovalService.updateApproval(approvalObject).subscribe(
            data => {
                this.getList();
            }, error => {
                console.log(error);
            });
    }
}