import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { PattycashentryService } from '../service/pattycashentry.service'
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'PattycashentryComponent',
    templateUrl: 'pattycashentry.component.html'
})

export class PattycashentryComponent implements OnInit {
    pattycashentryList =  [];
    pattycashentryData = {};
    id: number;

    constructor(
        
        private PattycashentryService: PattycashentryService,
        private spinner: NgxSpinnerService,                

    ) { }

    ngOnInit() { 
        this.spinner.show();                        
        this.PattycashentryService.getItems().subscribe(
            data => {
                this.spinner.hide();
                let activityData = [];
                activityData = data['result'];
                console.log("asdf");
                for (var i = 0; i < activityData.length; i++) {
                    if (activityData[i]['f123fstatus'] == 0) {
                        activityData[i]['f123fstatus'] = 'Pending';
                    } else if (activityData[i]['f123fstatus'] == 1) {
                        activityData[i]['f123fstatus'] = 'Approved';
                    } else {
                        activityData[i]['f123fstatus'] = 'Rejected';
                    }
                }
                this.pattycashentryList = activityData;
        }, error => {
            console.log(error);
        });
    }
}