import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { PattycashentryService } from '../../service/pattycashentry.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { DefinationmsService } from '../../../studentfinance/service/definationms.service';
import { PattycashallocationService } from "../../service/pattycashallocation.service";
import { DepartmentService } from '../../../generalsetup/service/department.service'
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'PattycashentryformComponent',
    templateUrl: 'pattycashentryform.component.html'
})

export class PattycashentryformComponent implements OnInit {
    pattycashentryform: FormGroup;
    pattycashentryList = [];
    pattycashentryData = {};
    paymenttypeList = [];
    paymentmodeList = [];
    typeList = [];
    accountcodeList = [];
    userData = {};
    userList = [];
    transactionList = [];
    deptList = [];
    pettycashIdList = [];
    ajaxCount: number;
    viewDisabled: boolean;
    id: number;
    idview: number;
    constructor(
        private DefinationmsService: DefinationmsService,
        private PattycashentryService: PattycashentryService,
        private DepartmentService: DepartmentService,
        private PattycashallocationService: PattycashallocationService,
        private spinner: NgxSpinnerService,
        private route: ActivatedRoute,
        private router: Router
    ) { }
    ngDoCheck() {
        if (this.ajaxCount == 0) {
            this.spinner.hide();
        }
    }
    ngOnInit() {
        this.ajaxCount = 0;
        this.spinner.show();
        this.viewDisabled = false;
        this.pattycashentryData['f095fsetupId'] = '';
        this.pattycashentryData['f095faccountCode'] = '';
        this.pattycashentryData['f123fstatus'] = 1;
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.route.paramMap.subscribe((data) => this.idview = + data.get('idview'));

        this.pattycashentryform = new FormGroup({
            pattycashentry: new FormControl('', Validators.required)
        });

        //petty cash id dropdown
        this.PattycashallocationService.getItems().subscribe(
            data => {
                this.pettycashIdList = data['result'];
            }, error => {
                console.log(error);
            })
        // payment mode dropdown
        this.ajaxCount++;
        this.DefinationmsService.getPaymentmode('PaymentMode').subscribe(
            data => {
                this.ajaxCount--;
                this.paymentmodeList = data['result'][0];
            }, error => {
                console.log(error);
                this.spinner.hide();
            });
        // Transaction dropdown
        this.ajaxCount++;
        this.DefinationmsService.getTransaction('Transaction').subscribe(
            data => {
                this.ajaxCount--;
                this.transactionList = data['result'];
            }, error => {
                console.log(error);
                this.spinner.hide();
            });

        // Department  dropdown
        this.ajaxCount++;
        this.DepartmentService.getActiveItems().subscribe(
            data => {
                this.ajaxCount--;
                this.deptList = data['result']['data'];
            }, error => {
                console.log(error);
                this.spinner.hide();
            });

        if (this.id > 0) {
            this.ajaxCount++;
            this.PattycashentryService.getItemsDetail(this.id).subscribe(
                data => {
                    this.ajaxCount--;
                    this.pattycashentryData = data['result'][0];

                    // converting from string to int for radio button default selection
                    this.pattycashentryData['f123fstatus'] = parseInt(data['result'][0]['f123fstatus']);
                    console.log(this.pattycashentryData);
                    if (data['result'][0]['f123fstatus'] == 1 || data['result'][0]['f123fstatus'] == 2) {
                        this.viewDisabled = true;
                        $("#target input,select").prop("disabled", true);
                        $("#target1 input,select").prop("disabled", true);
                    }
                    if (this.idview == 2) {
                        this.viewDisabled = true;
                        $("#target input,select").prop("disabled", true);
                        $("#target1 input,select").prop("disabled", true);
                    }
                }, error => {
                    console.log(error);
                });
        }
    }

    ConvertToFloat(val) {
        return parseFloat(val);
    }

    addPattycashentry() {
        console.log(this.pattycashentryData);

        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        this.pattycashentryData['f123famount'] = this.ConvertToFloat(this.pattycashentryData['f123famount']).toFixed(2);

        if (this.id > 0) {
            this.PattycashentryService.updatePattycashentryItems(this.pattycashentryData, this.id).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert('Petty Cash Description Already Exist');
                    }
                    else {
                        this.router.navigate(['accountspayable/pattycashentry']);
                        alert(" Petty Cash Entry has been Updated Sucessfully!!");
                    }
                }, error => {
                    alert(' Description Already Exist');
                    console.log(error);
                    return false;
                });
        } else {
            this.PattycashentryService.insertPattycashentryItems(this.pattycashentryData).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert(' Petty Cash Description Already Exist');
                        return false;
                    }
                    this.router.navigate(['accountspayable/pattycashentry']);
                    alert(" Petty Cash Entry has been Added Sucessfully !!");
                }, error => {
                    console.log(error);
                });
        }
    }
    onlyNumberKey(event) {
        //console.log(event);
        if (event.charCode == 46) {

        } else {
            return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
        }
    }
}