
import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
// import { ApprovalService } from '../service/approval.service'
import { PattycashapprovalService } from "../service/pattycashapproval.service";
import { AlertService } from './../../_services/alert.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'PattycashapprovalComponent',
    templateUrl: 'pattycashapproval.component.html'
})

export class PattycashapprovalComponent implements OnInit {
    rejecttwoList = [];
    pattycashapprovalList = [];
    pattycashapprovalData = {};
    selectAllCheckbox = true;
    reason: string;

    constructor(
        private PattycashapprovalService: PattycashapprovalService,
        private spinner: NgxSpinnerService,                        
        private AlertService: AlertService
    ) { }

    ngOnInit() {
        this.getListData();
    }
    getListData() {
        this.spinner.show();                        
        this.PattycashapprovalService.getItems().subscribe(
            data => {
                this.spinner.hide();
                this.pattycashapprovalList = data['result'];
            }, error => {
                console.log(error);
            });
    }
    updatepattycashRejectedItems() {
        var approvaloneIds = [];
        for (var i = 0; i < this.pattycashapprovalList.length; i++) {
            if (this.pattycashapprovalList[i].f052fapprovalStatus == true) {
                approvaloneIds.push(this.pattycashapprovalList[i].f123fid);
            }
        }
        if (approvaloneIds.length > 0) {
        } else {
            alert("Select atleast one Petty cash to reject");
            return false;
        }
        if (this.pattycashapprovalData['reason'] == undefined) {
            alert(' Enter the Description');
            return false;
        }
        var confirmPop = confirm("Do you want to Reject?");
        if (confirmPop == false) {
            return false;
        }
        console.log(this.pattycashapprovalList);

        var approvaloneUpdate = {};
        approvaloneUpdate['id'] = approvaloneIds;
        approvaloneUpdate['status']=2;
        approvaloneUpdate['reason'] = '';
        this.PattycashapprovalService.updatePattycashapprovalItems(approvaloneUpdate).subscribe(
            data => {
                alert("Petty Cash has been Rejected");

                this.getListData();
            }, error => {
                console.log(error);
            });
    }
    pattycashapprovalListData() {
        console.log(this.pattycashapprovalList);
        var approvaloneIds = [];
        for (var i = 0; i < this.pattycashapprovalList.length; i++) {
            if (this.pattycashapprovalList[i].f052fapprovalStatus == true) {
                approvaloneIds.push(this.pattycashapprovalList[i].f123fid);
            }
        }
        if (approvaloneIds.length > 0) {
        } else {
            alert("Select atleast one petty Cash to Approve");
            return false;
        }

        var confirmPop = confirm("Do you want to Approve?");
        if (confirmPop == false) {
            return false;
        }
        var approvaloneUpdate = {};
        approvaloneUpdate['id'] = approvaloneIds;
        approvaloneUpdate['status']=1;
        approvaloneUpdate['reason'] = this.pattycashapprovalData['reason'];
        this.PattycashapprovalService.updatePattycashapprovalItems(approvaloneUpdate).subscribe(
            data => {
                this.pattycashapprovalData['reason'] = '';

                alert(" Petty Cash has been approved successfully");

                this.getListData();
            }, error => {
                console.log(error);
            });
    }
    rejecttwoListData() {
        console.log(this.pattycashapprovalList);
        var rejectoneIds = [];

        for (var i = 0; i < this.pattycashapprovalList.length; i++) {
            if (this.pattycashapprovalList[i].f052fapprovalStatus == true) {
                rejectoneIds.push(this.pattycashapprovalList[i].f123fid);
            }
        }
        if (rejectoneIds.length > 0) {
        } else {
            alert(" Select atleast one Cash Advance to Reject");
            return false;
        }
        console.log(this.pattycashapprovalData['reason']);

        if (this.pattycashapprovalData['reason'] == '' || this.pattycashapprovalData['reason'] == undefined) {
            alert("Provide the reason for Rejection !");
        } else {
            var confirmPop = confirm("Do you want to Reject?");
            if (confirmPop == false) {
                return false;
            }
            var rejecttwoUpdate = {};
            rejecttwoUpdate['id'] = rejectoneIds;
            rejecttwoUpdate['status'] = 2;
            rejecttwoUpdate['reason'] = this.pattycashapprovalData['reason'];
            this.PattycashapprovalService.updatePattycashapprovalRejectedItems(rejecttwoUpdate).subscribe(
                data => {
                    alert("Petty Cash has been Rejected ! ");
                    this.pattycashapprovalData['reason'] = '';
                    this.getListData();
                }, error => {
                    console.log(error);
                });
        }

    }


}
