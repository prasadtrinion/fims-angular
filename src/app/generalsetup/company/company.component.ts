import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CompanyService } from '../service/company.service'
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'Company',
    templateUrl: 'company.component.html'
})
export class CompanyComponent implements OnInit {
    companyList = [];
    companyData = {};
    id: number;
    constructor(
        private CompanyService: CompanyService,
        private spinner: NgxSpinnerService
    ) { }

    ngOnInit() {
            this.spinner.show();
        this.CompanyService.getItems().subscribe(
            data => {
                this.spinner.hide();
                this.companyList = data['result']['data'];
            }, error => {
                console.log(error);
            });
    }
    addCompany() {
        console.log(this.companyData);
        this.CompanyService.insertCompanyItems(this.companyData).subscribe(
            data => {
                this.companyList = data['todos'];
            }, error => {
                console.log(error);
            });

    }
}