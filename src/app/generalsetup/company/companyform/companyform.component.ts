import { Injector } from '@angular/core';
import { Component, OnInit, ViewChild } from '@angular/core';
import { CompanyService } from '../../service/company.service'
import { StateService } from '../../service/state.service'
import { ActivatedRoute, Router } from '@angular/router';
import { CountryService } from '../../service/country.service'


@Component({
    selector: 'CompanyFormComponent',
    templateUrl: 'companyform.component.html'
})
export class CompanyFormComponent implements OnInit {
    countryList = [];
    stateList = [];
    companyList = [];
    companyidparentList=[];
    taxtype=[];
    ajaxcount = 0;
    companyData = {};
    

    id: number;
    constructor(
        private companyService: CompanyService,
        private countryService: CountryService,
        private StateService: StateService,
        private route: ActivatedRoute,
        private router: Router
    ) { }

    ngOnInit() {

        this.companyData['f013fcountryCode'] = '';
        this.companyData['f013fstateCode'] = '';

        let taxtypeobj = {};
        taxtypeobj['id']=1;
        taxtypeobj['name'] = 'No';
        this.taxtype.push(taxtypeobj);
        taxtypeobj = {};
        taxtypeobj['id']=2;
        taxtypeobj['name'] = 'Yes';
        this.taxtype.push(taxtypeobj);



        //country dropdown
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.countryService.getItems().subscribe(
            data => {
                this.countryList = data['result']['data'];
            }, error => {
                console.log(error);
            });
        //State dropdown 
      
            this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
            this.companyService.getItems().subscribe(
                data => {
                    this.companyidparentList = data['result']['data'];
                }, error => {
                    console.log(error);
                });
            console.log(this.id);

        if (this.id > 0) {
            this.companyService.getItemsDetail(this.id).subscribe(
                data => {
                    this.companyData = data['result'][0];
                    this.onCountryChange();
                    // this.companyData['f081fstatus'] = parseInt(data['result'][0]['f081fstatus']);

                }, error => {
                    console.log(error);
                });
        }
    }
    onCountryChange(){
        this.ajaxcount ++;
        if(this.companyData['f013countryName'] == undefined){
            this.companyData['f013countryName'] = 0;
        }
        this.StateService.getStates(this.companyData['f013countryName']).subscribe(
            data=>{
                this.ajaxcount --;
                    this.stateList = data['result'];
                    let other = {
                        'f012fid' : "-1",
                        "f012fshortName":"SH",
                        "f012fstateName":"Others"

                    };
                    this.stateList.push(other);

            }
        );
    }
    addcompany() {
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        this.companyData['f013fcreatedBy'] = 1;
        this.companyData['f013fupdatedBy'] = 1;
           
           this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
           if(this.id>0) {
            this.companyService.updatestateItems(this.companyData,this.id).subscribe(
                data => {
                    this.router.navigate(['generalsetup/company']);
                    alert("Company has been Updated Successfully")
                }, error => {
                    console.log(error);
                });


        } else {

            this.companyService.insertCompanyItems(this.companyData).subscribe(
                data => {
                    this.router.navigate(['generalsetup/company']);
                    alert("Company has been Added Successfully")

                }, error => {
                    console.log(error);
                });

        }
    }
    onlyNumberKey(event) {
         console.log(event);
        return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
    }

}