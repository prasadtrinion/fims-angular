import {Injector} from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { VocherService } from '../../service/vocher.service'

@Component({
    selector: 'VocherFormComponent',
    templateUrl: 'vocherform.component.html'
})

export class VocherFormComponent implements OnInit {
    vocherList =  [];
    vocherData = {};

    constructor(
        
        private vocherService: VocherService
    ) { }

    ngOnInit() { 
    }
    addVocher(){
           console.log(this.vocherData);
        this.vocherService.insertVocherItems(this.vocherData).subscribe(
            data => {
                this.vocherList = data['todos'];
        }, error => {
            console.log(error);
        });

    }
    goToListPage(){
        
    }
}