import {Injector} from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { VocherService } from '../service/vocher.service'

@Component({
    selector: 'Vocher',
    templateUrl: 'vocher.component.html'
})

export class VocherComponent implements OnInit {
    vocherList =  [];
    vocherData = {};
    id: number;
    constructor(
        
        private vocherService: VocherService
    ) { }

    ngOnInit() { 
        
        this.vocherService.getItems().subscribe(
            data => {
                this.vocherList = data['result']['data'];
        }, error => {
            console.log(error);
        });
    }


    addVocher(){
           console.log(this.vocherData);
        this.vocherService.insertVocherItems(this.vocherData).subscribe(
            data => {
                this.vocherList = data['todos'];
        }, error => {
            console.log(error);
        });

    }
}