import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CreditorsetupService } from '../service/creditorsetup.service'


@Component({
    selector: 'CreditorsetupComponent',
    templateUrl: 'creditorsetup.component.html'
})

export class CreditorsetupComponent implements OnInit {
    creditorsetupList =  [];
    creditorsetupData = {};
    id: number;

    constructor(
        
        private CreditorsetupService: CreditorsetupService

    ) { }

    ngOnInit() { 
        
        this.CreditorsetupService.getItems().subscribe(
            data => {
                this.creditorsetupList = data['result']['data'];
        }, error => {
            console.log(error);
        });
    }

    addApprovallimit(){
           console.log(this.creditorsetupData);
        this.CreditorsetupService.insertCreditorSetupItems(this.creditorsetupData).subscribe(
            data => { 
                this.creditorsetupList = data['todos'];
            }, error => {
                console.log(error);
            });

    }
}