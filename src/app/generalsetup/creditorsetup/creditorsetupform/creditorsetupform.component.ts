import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { CreditorsetupService } from '../../service/creditorsetup.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { DefinationmsService } from '../../../studentfinance/service/definationms.service';
import { AccountcodeService } from '../../../generalsetup/service/accountcode.service'



@Component({
    selector: 'CreditorsetupformComponent',
    templateUrl: 'creditorsetupform.component.html'
})

export class CreditorsetupformComponent implements OnInit {
    creditorsetupform: FormGroup;
    creditorsetupList = [];
    creditorsetupData = {};
    paymenttypeList = [];
    accountcodeList = [];
    userData = {};
    userList = [];

    id: number;
    constructor(
        private AccountcodeService: AccountcodeService,
        private DefinationmsService: DefinationmsService,
        private CreditorsetupService: CreditorsetupService,
        private route: ActivatedRoute,
        private router: Router
    ) { }

    ngOnInit() {
        this.creditorsetupData['f095fsetupId']='';
        this.creditorsetupData['f095faccountCode']='';
        this.creditorsetupData['f095fstatus'] = 1;
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        
        this.creditorsetupform = new FormGroup({
            creditorsetup: new FormControl('', Validators.required)
        });
        
        // payment type dropdown
        this.DefinationmsService.getPaymenttype('PaymentVoucherType').subscribe(
            data => {
                this.paymenttypeList = data['result'];
            }, error => {
                console.log(error);
            });
            // account dropdown
        this.AccountcodeService.getItemsByLevel().subscribe(
            data => {
                this.accountcodeList = data['result']['data'];
        }, error => {
            console.log(error);
        });

        console.log(this.id);

        if (this.id > 0) {
            this.CreditorsetupService.getItemsDetail(this.id).subscribe(
                data => {
                    this.creditorsetupData = data['result'][0];
                    if(data['result'][0].f095fstatus==='1') {
                    console.log(data);
                    this.creditorsetupData['f095fstatus'] = 1;
                } else {
                    this.creditorsetupData['f095fstatus'] = 0;

                }
                console.log(this.creditorsetupData);
                }, error => {
                    console.log(error);
                });
        }


        console.log(this.id);
    }

    addCreditorsetup() {
        console.log(this.creditorsetupData);

           this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if(this.id>0) {
            this.CreditorsetupService.updateCreditorSetupItems(this.creditorsetupData,this.id).subscribe(
                data => {
                    this.router.navigate(['generalsetup/creditorsetup']);
            }, error => {
                console.log(error);
            });

       
    } else {
        this.CreditorsetupService.insertCreditorSetupItems(this.creditorsetupData).subscribe(
            data => {
                this.router.navigate(['generalsetup/creditorsetup']);

        }, error => {
            console.log(error);
        });

    }

}
}