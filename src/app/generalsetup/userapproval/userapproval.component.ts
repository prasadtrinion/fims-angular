import {Injector} from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { UserapprovalService } from '../service/userapproval.service'
import { AlertService } from './../../_services/alert.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'Userapproval',
    templateUrl: 'userapproval.component.html'
})

export class UserapprovalComponent implements OnInit {
    userapprovalList =  [];
    userapprovalData = {};
    selectAllCheckbox = true;
    id: number;

    constructor(
        private UserapprovalService: UserapprovalService,
        private spinner: NgxSpinnerService

    ) { }

    ngOnInit() {    
        this.getListData();
     }

    getListData(){
        this.spinner.show();
        this.UserapprovalService.getItems().subscribe(
            data => {
                this.spinner.hide();
                this.userapprovalList = data['data']['data'];
        }, error => {
            console.log(error);
        });
    }
    
    userapprovalListData() {
        console.log(this.userapprovalList);
        var userapprovalIds = [];
        for (var i = 0; i < this.userapprovalList.length; i++) {
            if(this.userapprovalList[i].userstatus==true) {
                userapprovalIds.push(this.userapprovalList[i].f014fid);
            }
        }
        if(userapprovalIds.length <= 0){
            alert('No user selected');
            return false;
        }
        var confirmPop = confirm("Do you want to Approve?");
        if (confirmPop == false) {
            return false;
        }
        var UserapprovalUpdate = {};
        UserapprovalUpdate['id'] = userapprovalIds;
        this.UserapprovalService.updateUserapproval(UserapprovalUpdate).subscribe(
            data => {
                this.getListData();
                alert("Approved Successfully")
        }, error => {
            console.log(error);
        });
    }
    selectAll() { 
        console.log("asdf");
        console.log(this.selectAllCheckbox);
        for (var i = 0; i < this.userapprovalList.length; i++) {
                this.userapprovalList[i].f014fstatus = this.selectAllCheckbox;
        }
        //console.log(this.budgetcostcenterapprovalList);
      }

   
}