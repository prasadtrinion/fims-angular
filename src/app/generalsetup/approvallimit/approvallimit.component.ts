import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ApprovallimitService } from '../../accountspayable/service/approvallimit.service'


@Component({
    selector: 'Approvallimit',
    templateUrl: 'approvallimit.component.html'
})

export class ApprovallimitComponent implements OnInit {
    approvallimitList =  [];
    approvallimitData = {};
    id: number;

    constructor(
        
        private ApprovallimitService: ApprovallimitService

    ) { }

    ngOnInit() { 
        
        this.ApprovallimitService.getItems().subscribe(
            data => {
                this.approvallimitList = data['result']['data'];
        }, error => {
            console.log(error);
        });
    }

    addApprovallimit(){
           console.log(this.approvallimitData);
        this.ApprovallimitService.insertApprovallimitItems(this.approvallimitData).subscribe(
            data => { 
                this.approvallimitList = data['todos'];
            }, error => {
                console.log(error);
            });

    }
}