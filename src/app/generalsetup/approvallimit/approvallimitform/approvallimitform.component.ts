import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { ApprovallimitService } from '../../../accountspayable/service/approvallimit.service'
import { UserService } from '../../../generalsetup/service/user.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { DefinationmsService } from '../../../studentfinance/service/definationms.service';

@Component({
    selector: 'ApprovallimitFormComponent',
    templateUrl: 'approvallimitform.component.html'
})

export class ApprovallimitFormComponent implements OnInit {
    approvallimitform: FormGroup;
    approvallimitList = [];
    approvallimitData = {};
    userData = {};
    userList = [];
    moduleList = [];
    id: number;
    constructor(
        private ApprovallimitService: ApprovallimitService,
        private UserService: UserService,
        private route: ActivatedRoute,
        private router: Router,
        private DefinationmsService: DefinationmsService,

    ) { }

    ngOnInit() {
        
        this.approvallimitData['f020fidUser']='';
        this.approvallimitData['f020fstatus'] = 1;
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        
        this.approvallimitform = new FormGroup({
            approvallimit: new FormControl('', Validators.required)
        });
         //user dropdown
         this.UserService.getItems().subscribe(
            data => {
                this.userList = data['result']['data'];
            }
        );
        this.DefinationmsService.getScheme('ApprovalType').subscribe(
            data => {

                this.moduleList = data['result'];
            }, error => {
                console.log(error);
            });

        console.log(this.id);

        if (this.id > 0) {
            this.ApprovallimitService.getItemsDetail(this.id).subscribe(
                data => {
                    this.approvallimitData = data['result'];
                    this.approvallimitData['f020fidUser'] = parseInt(this.approvallimitData['f020fidUser']);
                    if(data['result'].f020fstatus===true) {
                    console.log(data);
                    this.approvallimitData['f020fstatus'] = 1;
                } else {
                    this.approvallimitData['f020fstatus'] = 0;

                }
                console.log(this.approvallimitData);
                }, error => {
                    console.log(error);
                });
        }


        console.log(this.id);
    }

    addApprovallimit() {
        var cnf = confirm("Do you want to save?");
        if(cnf == false){
            return false;
        }
        console.log(this.approvallimitData);

           this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if(this.id>0) {
            this.ApprovallimitService.updateApprovallimitItems(this.approvallimitData,this.id).subscribe(
                data => {
                    this.router.navigate(['generalsetup/approvallimit']);
            }, error => {
                console.log(error);
            });

       
    } else {
        this.ApprovallimitService.insertApprovallimitItems(this.approvallimitData).subscribe(
            data => {
                this.router.navigate(['generalsetup/approvallimit']);

        }, error => {
            console.log(error);
        });

    }

}
}