import { Injector, Injectable } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FundService } from '../../service/fund.service'
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { AlertService } from '../../../_services/alert.service';

@Injectable()
@Component({
    selector: 'FundFormComponent',
    templateUrl: 'fundform.component.html'
})

export class FundFormComponent implements OnInit {
    fundList = [];
    fundData = {};
    id: number;
    constructor(
        private FundService: FundService,
        private route: ActivatedRoute,
        private router: Router,
        private spinnerService: Ng4LoadingSpinnerService,
        private alertService:AlertService
       ) { 
    }

    ngOnInit() {
         this.fundData['f057fstatus'] = 1;
        
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        console.log(this.id);

        if (this.id > 0) {
            this.FundService.getItemsDetail(this.id).subscribe(
                data => {
                    this.fundData = data['result'][0];
                    
                    // converting from string to int for radio button default selection
                    this.fundData['f057fstatus'] = parseInt(data['result'][0]['f057fstatus']);
                    
                console.log(this.fundData);
                }, error => {
                    console.log(error);     
                });
        }   


        console.log(this.id);
    }

    addFund() {
        // this.spinnerService.show();
        console.log(this.fundData);
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
           this.fundData['f057fupdatedBy']=1;
           this.fundData['f057fcreatedBy']=1;
           this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if(this.id>0) {
            this.FundService.updateFundItems(this.fundData,this.id).subscribe(
                data => {
                    if(data['status'] == 409){
                        alert('Fund Code Already Exist');
                    }
                    else{
                         this.router.navigate(['generalsetup/fund']);
                         alert("Fund has been Updated Sucessfully !!");

                    }

            }, error => {
                alert('Fund Code Already Exist');
                return false;
                // console.log(error);
            });
    } else {
        this.FundService.insertFundItems(this.fundData).subscribe(
            data => {
                    if(data['status'] == 409){
                        alert("Fund Code Already Exist");
                    }
                    else{
                        this.router.navigate(['generalsetup/fund']);
                        alert("Fund has been Added Sucessfully ! ");

                    }

        }, error => {
            console.log(error);
        });
    }
}
onlyNumberKey(event) {
    console.log(event);
    return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
}
}