import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { FundService } from '../service/fund.service';
import { faSearch, faEdit } from '@fortawesome/free-solid-svg-icons';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'Fund',
    templateUrl: 'fund.component.html'
})
export class FundComponent implements OnInit {
    public fundList =  [];
    fundData = {};
    id: number;
    faSearch = faSearch;
    faEdit = faEdit;
    constructor( 
        private FundService: FundService,
        private spinner: NgxSpinnerService

    ) { 
        
    }
    ngOnInit() { 
        
        this.spinner.show();
        
        this.FundService.getDataItems().subscribe(
            data => {
                this.spinner.hide();
                
                this.fundList = data['result']['data'];
        }, error => {
            console.log(error);
        });
    }
    addFund(){
        
           console.log(this.fundData);
        this.FundService.insertFundItems(this.fundData).subscribe(
            data => {
                this.fundList = data['todos'];
            }, error => {
                console.log(error);
            });
    }
    
}