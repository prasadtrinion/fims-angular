import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NewaccountcodeService } from '../../service/newaccountcode.service'
import { DefinationmsService} from '../../../studentfinance/service/definationms.service'
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
    selector: 'LeveloneComponent',
    templateUrl: 'levelone.component.html'
})

export class LeveloneComponent implements OnInit {
    leveloneList = [];
    leveloneData = {};
    leveloneDataheader = {};
    categoryList = [];
    type=[];
    baltype=[];
    id: number;
    constructor(
        private NewaccountcodeService: NewaccountcodeService,
        private DefinationmsService:DefinationmsService,
        private route: ActivatedRoute,
        private router: Router
    ) { }

    ngOnInit() {

        let typeobj = {};
        typeobj['id']='1';
        typeobj['name'] = 'PL';
        this.type.push(typeobj);
        typeobj = {};
        typeobj['id']='2';
        typeobj['name'] = 'Balance Sheet';
        this.type.push(typeobj);


        let baltypeobj = {};
        baltypeobj['id']='1';
        baltypeobj['name'] = 'Credit';
        this.baltype.push(baltypeobj);
        baltypeobj = {};
        baltypeobj['id']='2';
        baltypeobj['name'] = 'Debit';
        this.baltype.push(baltypeobj);

        this.getList();
    }

    saveLevelone() {

       
        // this.leveloneData['f059ftype']='';
        // this.leveloneData['f059fbalanceType']='';
        console.log("asdf");

        let leveloneObject = {};
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            leveloneObject['f059fid'] = this.id;
        }

        leveloneObject['data'] = this.leveloneList;

        let leveloneArray = [];
        for(var i=0;i<this.leveloneList.length;i++) {
            if(i==0) {
                leveloneArray.push(this.leveloneList[i]['f059fcode']);
            } else {
                var a = leveloneArray.indexOf(this.leveloneList[i]['f059fcode']);
                if(a>=0) {
                    alert("Cannot allow duplicates of AccountCode");
                    return false;
                } else {
                leveloneArray.push(this.leveloneList[i]['f059fcode']);
                }
            }
            if(this.leveloneList[i]['f059fcode'].length!=1) {
                alert("Please add 1 digit Group");
                return false;
            }
            
            
        }

       
        //dropdown  category
        // this.DefinationmsService.getCategoryItems('category').subscribe(
        //     data => {
        //         this.categoryList = data['result'];
        //     }, error => {
        //         console.log(error);
        //     });
        // console.log(this.id);

        this.NewaccountcodeService.insertnewaccountcodeItems(leveloneObject).subscribe(
            data => {
                if(data['status'] == 409){
                    alert('Group Code Already Exist');
                    return false;
                }

                this.getList();
               alert("Account General has been Updated Successfully")

            }, error => {
                console.log(error);
            });
    }

    getList() {


        this.leveloneData['f059fstatus'] = 1;

        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        console.log(this.id);
        let parentId =0;
        let levelId = 0;

        this.NewaccountcodeService.getItemsDetail(parentId,levelId).subscribe(
            data => {
                this.leveloneList = data['result']['data'];
                console.log(this.leveloneList);
            }, error => {
                console.log(error);
            });


        console.log(this.id);

    }

    onBlurMethod(object) {
        console.log(object);
        sessionStorage.removeItem("accountleveloneparent");
        sessionStorage.removeItem("accountlevelonecode");
        sessionStorage.removeItem("accountparentCode");
        sessionStorage.setItem("accountparentCode", object['f059fcode']);

        sessionStorage.setItem("accountleveloneparent", object['f059fid']);
        sessionStorage.setItem("accountlevelonecode",object['f059fcode']);

    }
    deletelevelone(object) {
        var cnf = confirm("Do you really want to delete");
        if (cnf == true) {
            console.log(this.leveloneList);
            var index = this.leveloneList.indexOf(object);;
            if (index > -1) {
                this.leveloneList.splice(index, 1);
            }
            //  this.onBlurMethod();
        }
    }
    addLevelone() {

        console.log(this.leveloneData);
        if (this.leveloneData['f059faccountActivity'] == undefined) {
            alert('Enter Activity');
            return false;
        }
        if (this.leveloneData['f059fcode'] == undefined) {
            alert('Enter Group Code');
            return false;
        }
        if (this.leveloneData['f059fname'] == undefined) {
            alert('Enter Account Code Description');
            return false;
        }
        if (this.leveloneData['f059ftype'] == undefined) {
            alert('Select Account Code Category');
            return false;
        }
        if (this.leveloneData['f059fbalanceType'] == undefined) {
            alert('Select Account Code Type');
            return false;
        }
        var confirmPop = confirm("Do you want to Add?");
        if (confirmPop == false) {
            return false;
        }
        this.leveloneData['f059fupdatedBy'] = 1;
        this.leveloneData['f059fcreatedBy'] = 1;
        this.leveloneData['f059fparentCode'] = 0;
        this.leveloneData['f059frefCode'] = 0;
        this.leveloneData['f059fstatus'] = 0;
        if(this.leveloneData['f059fcode'].length!=1) {
            alert("Please add 1 digit Group Category");
            return false;
        }
      
        
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        var dataofCurrentRow = this.leveloneData;
        this.leveloneList.push(dataofCurrentRow);
        console.log(this.leveloneData);
        this.leveloneData = {};

    
}
    onlyNumberKey(event) {
        console.log(event);
       return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
   }

}


