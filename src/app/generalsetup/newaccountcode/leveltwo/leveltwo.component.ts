import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NewaccountcodeService } from '../../service/newaccountcode.service'
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
    selector: 'LeveltwoComponent',
    templateUrl: 'leveltwo.component.html'
})

export class LeveltwoComponent implements OnInit {
    leveltwoList = [];
    leveltwoData = {};
    id: number;
    levelone:number;
    constructor(
        private NewaccountcodeService: NewaccountcodeService,
        private route: ActivatedRoute,
        private router: Router
    ) { }

    ngOnInit() {
        this.getList();
    }
    onBlurMethod(object) {
        console.log(object);
        sessionStorage.removeItem("accountleveltwoparent");
        sessionStorage.removeItem("accountleveltwocode");
        sessionStorage.setItem("accountleveltwoparent", object['f059fid']);
        sessionStorage.setItem("accountleveltwocode",object['f059fcode']);

    }

    getList() {
        this.levelone = parseInt(sessionStorage.getItem("accountlevelonecode"));

        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        console.log(this.id);
        this.leveltwoData['f059frefCode'] = sessionStorage.getItem("accountleveloneparent");
        this.leveltwoData['f059fparentCode'] = sessionStorage.getItem("accountleveloneparent");
        this.leveltwoData['f059flevelStatus'] = 1;
        let parentId =sessionStorage.getItem("accountleveloneparent");
        let levelId = 1;
        this.leveltwoData['f059fstatus'] = 1;

        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        console.log(this.id);
       
            this.NewaccountcodeService.getItemsDetail(parentId,levelId).subscribe(
                data => {
                    this.leveltwoList = data['result']['data'];
                }, error => {
                    console.log(error);
                });
    
        
        console.log(this.id);
    }
    saveLeveltwo() {
        let leveltwoObject = {};
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            leveltwoObject['f059fid'] = this.id;
        }

        leveltwoObject['data'] = this.leveltwoList;
        // for(var i=0;i<this.leveltwoList.length;i++) {
        //     // if(this.leveltwoList[i]['f059fcode'].length!=2) {
        //     //     alert("Please add 2 digit Sub Group Category");
        //     //     return false;
        //     // }
        // }
 console.log("asdf");
        let leveloneArray = [];
        for(var i=0;i<this.leveltwoList.length;i++) {
            if(i==0) {
                leveloneArray.push(this.leveltwoList[i]['f059fcode']);
            } else {
                var a = leveloneArray.indexOf(this.leveltwoList[i]['f059fcode']);
                if(a>=0) {
                    alert("Cannot allow duplicates of AccountCode");
                    return false;
                } else {
                leveloneArray.push(this.leveltwoList[i]['f059fcode']);
                }
            }
            if(this.leveltwoList[i]['f059fcode'].length!=1) {
                alert("Please add 1 digit Group");
                return false;
            }
            
            
        }



        this.NewaccountcodeService.insertnewaccountcodeItems(leveltwoObject).subscribe(
            data => {
                if(data['status'] == 409){
                    alert('Sub Category Code Already Exist');
                    return false;
                }
                this.getList();
                alert("Account Specific has been Saved Successfully")

            }, error => {
                console.log(error);
            });
    }

    deleteleveltwo(object)
     {
        var cnf = confirm("Do you really want to delete");
        if(cnf==true){
            console.log(this.leveltwoList);
            var index = this.leveltwoList.indexOf(object);;
            if (index > -1) {
                this.leveltwoList.splice(index, 1);
            }
            //  this.onBlurMethod();
        }
    }
    addLeveltwo()
    {
        console.log(this.leveltwoData);
        if (this.leveltwoData['f059fcode'] == undefined) {
            alert('Please Enter Account Specific Sub Category Code');
            return false;
        }
        var confirmPop = confirm("Do you want to Add?");
        if (confirmPop == false) {
            return false;
        }
        this.leveltwoData['f059fupdatedBy'] = 1;
        this.leveltwoData['f059fcreatedBy'] = 1;
        this.leveltwoData['f059fstatus'] = 1;
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if(this.leveltwoData['f059fcode'].length!=1) {
            alert("Please add 1 digit Sub Group Category");
            return false;
        }
        // if(data['status'] == 409){
        //     alert('Sub Category Code Already Exist');
        //     return false;
        // }
        this.leveltwoData['f059frefCode'] = sessionStorage.getItem("accountleveloneparent");
        this.leveltwoData['f059fparentCode'] = sessionStorage.getItem("accountleveloneparent");
        this.leveltwoData['f059flevelStatus'] = 1;

        var dataofCurrentRow = this.leveltwoData;
        this.leveltwoList.push(dataofCurrentRow);
        console.log(this.leveltwoData);
        this.leveltwoData = {};
    }
    onlyNumberKey(event) {
        console.log(event);
       return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
   }
}
