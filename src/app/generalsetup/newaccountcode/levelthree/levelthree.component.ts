import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NewaccountcodeService } from '../../service/newaccountcode.service'
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
    selector: 'LevelthreeComponent',
    templateUrl: 'levelthree.component.html'
})

export class LevelthreeComponent implements OnInit {
    levelthreeList = [];
    levelthreeData = {};
    id: number;
    levelone:number;
    leveltwo:number;
    constructor(
        private NewaccountcodeService: NewaccountcodeService,
        private route: ActivatedRoute,
        private router: Router
    ) { }

    ngOnInit() {
       
    this.getList();
    }

    getList(){

        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        console.log(this.id);

        console.log(this.id);
        this.levelthreeData['f059frefCode'] = sessionStorage.getItem("accountleveltwoparent");
        this.levelthreeData['f059fparentCode'] = sessionStorage.getItem("accountleveltwoparent");
        this.levelthreeData['f059flevelStatus'] = 2;
        let parentId =sessionStorage.getItem("accountleveltwoparent");

        this.levelone = parseInt(sessionStorage.getItem("accountlevelonecode"));
        this.leveltwo = parseInt(sessionStorage.getItem("accountleveltwocode"));
        this.levelthreeData['f059fstatus'] = 1;

        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        console.log(this.id);
        let levelId = 2;
       
            this.NewaccountcodeService.getItemsDetail(parentId, levelId).subscribe(
                data => {
                    this.levelthreeList = data['result']['data'];
                }, error => {
                    console.log(error);
                });
        


        console.log(this.id);
    }
    saveLevelthree() {
        let levelthreeObject = {};
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            levelthreeObject['f059fid'] = this.id;
        }

        levelthreeObject['data'] = this.levelthreeList;
        for(var i=0;i<this.levelthreeList.length;i++) {
            // if(this.levelthreeList[i]['f059fcode'].length!=3) {
            //     alert("Please add 3 digit Sub Group Category");
            //     return false;
            // }
        }

        let leveloneArray = [];
        for(var i=0;i<this.levelthreeList.length;i++) {
            if(i==0) {
                leveloneArray.push(this.levelthreeList[i]['f059fcode']);
            } else {
                var a = leveloneArray.indexOf(this.levelthreeList[i]['f059fcode']);
                if(a>=0) {
                    alert("Cannot allow duplicates of AccountCode");
                    return false;
                } else {
                leveloneArray.push(this.levelthreeList[i]['f059fcode']);
                }
            }
            if(this.levelthreeList[i]['f059fcode'].length!=3) {
                alert("Please add 3 digit Group");
                return false;
            }
            
            
        }

        this.NewaccountcodeService.insertnewaccountcodeItems(levelthreeObject).subscribe(
            data => {
                if(data['status'] == 409){
                    alert('Category Code Already Exist');
                    return false;
                }
                this.getList();
                // alert("Data has been updated");
                alert("Account Details has been Updated Successfully")

            }, error => {
                console.log(error);
            });
    }

    deletelevelthree(object) {
        var cnf = confirm("Do you really want to delete");
        if (cnf == true) {
            console.log(this.levelthreeList);
            var index = this.levelthreeList.indexOf(object);;
            if (index > -1) {
                this.levelthreeList.splice(index, 1);
            }
            //  this.onBlurMethod();
        }
    }
    addLevelthree() {

        console.log(this.levelthreeData);
        // if (this.levelthreeData['levelone'] == undefined) {
        //     alert('Please Enter Group Category');
        //     return false;
        // }
        // if (this.levelthreeData['leveltwo'] == undefined) {
        //     alert('Please Enter Account Detail Sub Category');
        //     return false;
        // }
        if (this.levelthreeData['f059fcode'] == undefined) {
            alert('Please Enter Account Detail Category Code');
            return false;
        }
        if (this.levelthreeData['f059fname'] == undefined) {
            alert('Please Enter Account Detail Description');
            return false;
        }
        var confirmPop = confirm("Do you want to Add?");
        if (confirmPop == false) {
            return false;
        }

        this.levelthreeData['f059fupdatedBy'] = 1;
        this.levelthreeData['f059fcreatedBy'] = 1;

        this.levelthreeData['f059fstatus'] = 2;
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if(this.levelthreeData['f059fcode'].length!=3) {
            alert("Please add 3 digit Sub Group Category");
            return false;
        }
        this.levelthreeData['f059frefCode'] = sessionStorage.getItem("accountleveltwoparent");
        this.levelthreeData['f059fparentCode'] = sessionStorage.getItem("accountleveltwoparent");
        this.levelthreeData['f059flevelStatus'] = 2;

        var dataofCurrentRow = this.levelthreeData;
        this.levelthreeList.push(dataofCurrentRow);
        console.log(this.levelthreeData);
        this.levelthreeData = {};

    }
    onlyNumberKey(event) {
        console.log(event);
       return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
   }

}
