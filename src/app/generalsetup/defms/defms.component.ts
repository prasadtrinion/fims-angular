import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DefinationmsService } from '../../studentfinance/service/definationms.service'

@Component({
    selector: 'defms',
    templateUrl: 'defms.component.html'
})
export class DefmsComponent implements OnInit {
    defmsList = [];
    defmsData = {};

    constructor(
        
        private DefinationmsService: DefinationmsService
    ) { }

    ngOnInit() { 
         var accountCode = 'AccountCategory';
        this.DefinationmsService.getAllDefinationMsData().subscribe(
            data => {
                this.defmsList = data['result']['data'];
                console.log(this.defmsList);
        }, error => {
            console.log(error);
        });
    }
    adddefms(){
        console.log(this.defmsData);
     this.DefinationmsService.insertDefinationmsItems(this.defmsData).subscribe(
         data => {
             this.defmsList = data['todos'];
     }, error => {
         console.log(error);
     });
 }
}
