import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DefinationmsService } from '../../../studentfinance/service/definationms.service'
import { DeftypemsService } from '../../service/deftypems.service'
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
    selector: 'DefmsformComponent',
    templateUrl: 'defmsform.component.html'
})

export class DefmsformComponent implements OnInit {
    defmsform: FormGroup;
    defmsList = [];
    defmsData = {};
    deftypeList = [];
    id: number;


    constructor(

        private DefinationmsService: DefinationmsService,
        private DeftypemsService: DeftypemsService,
        private route: ActivatedRoute,
        private router: Router
    ) { }

    ngOnInit() {
        this.defmsData['f011fidDefinitiontypems']='';
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.defmsform = new FormGroup({
            defms: new FormControl('', Validators.required)
        });

        console.log(this.id);

        if (this.id > 0) {
            this.DefinationmsService.getItemsDetail(this.id).subscribe(
                data => {
                    this.defmsData = data['result'];
                    console.log(this.defmsData);
                }, error => {
                    console.log(error);
                });
        }
        console.log(this.id);

        //drop down deftype
        this.DeftypemsService.getItems().subscribe(
            data => {
                this.deftypeList = data['result']['data'];
            }, error => {
                console.log(error);
            });
    }

    addDefms() {
        console.log(this.defmsData);
        this.DefinationmsService.insertDefinationmsItems(this.defmsData).subscribe(
            data => {
                this.defmsList = data['todos'];
            }, error => {
                console.log(error);
            });




    }
}



