import {Injector} from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { DeftypemsService } from '../service/deftypems.service'

@Component({
    selector: 'deftypems',
    templateUrl: 'deftypems.component.html'
})

export class DeftypemsComponent implements OnInit {
    deftypemsList =  [];
    deftypemsData = {};

    constructor(
        
        private deftypemsService: DeftypemsService
    ) { }

    ngOnInit() { 
        
        this.deftypemsService.getItems().subscribe(
            data => {
                this.deftypemsList = data['result']['data'];

        }, error => {
            console.log(error);
        });
    }
    adddeftypems(){
        console.log(this.deftypemsData);
     this.deftypemsService.insertDeftypemsItems(this.deftypemsData).subscribe(
         data => {
             this.deftypemsList = data['todos'];
     }, error => {
         console.log(error);
     });

 }
}