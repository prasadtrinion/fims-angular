import {Injector} from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { DefinationmsService } from '../../../studentfinance/service/definationms.service'
@Component({
    selector: 'DeftypemsFormComponent',
    templateUrl: 'deftypemsform.component.html'
})

export class DeftypemsFormComponent implements OnInit {
    deftypemsList =  [];
    deftypemsData = {};

    constructor(
        
        private DefinationmsService: DefinationmsService
    ) { }

    ngOnInit() { 
    }
    addDeftypems(){
           console.log(this.deftypemsData);
        this.DefinationmsService.insertDefinationmsItems(this.deftypemsData).subscribe(
            data => {
                this.deftypemsList = data['todos'];
        }, error => {
            console.log(error);
        });

    }
}