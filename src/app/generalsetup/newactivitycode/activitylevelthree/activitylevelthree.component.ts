import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NewactivitycodeService } from '../../service/newactivitycode.service'
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
    selector: 'ActivitylevelthreeComponent',
    templateUrl: 'activitylevelthree.component.html'
})

export class ActivitylevelthreeComponent implements OnInit {
    activitylevelthreeList = [];
    activitylevelthreeData = {};
    activitylevelone:string;
    activityleveltwo:string;
    id: number;
    constructor(
        private NewactivitycodeService: NewactivitycodeService,
        private route: ActivatedRoute,
        private router: Router
    ) { }

    ngOnInit() {
        this.activitylevelone = sessionStorage.getItem("levelonecode");
        this.activityleveltwo = sessionStorage.getItem("leveltwocode");
        this.activitylevelthreeData['f058fstatus'] = 1;

        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        console.log(this.id);

        console.log(this.id);
        this.activitylevelthreeData['f058frefCode'] = sessionStorage.getItem("leveltwoparent");
        this.activitylevelthreeData['f058fparentCode'] = sessionStorage.getItem("leveltwoparent");
        this.activitylevelthreeData['f058flevelStatus'] = 2;
        let parentId =sessionStorage.getItem("leveltwoparent");
        let levelId = 2;
       
            this.NewactivitycodeService.getItemsDetail(parentId,levelId).subscribe(
                data => {
                    this.activitylevelthreeList = data['result']['data'];
                }, error => {
                    console.log(error);
                });
    
        console.log(this.id);
    }
    saveactivitylevelthree() {
        let levelthreeObject = {};
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            levelthreeObject['f058fid'] = this.id;
        }

        levelthreeObject['data'] = this.activitylevelthreeList;
        for(var i=0;i<this.activitylevelthreeList.length;i++) {
            if(this.activitylevelthreeList[i]['f058fcode'].length!=3) {
                alert("Please add 3 digit Sub Group");
                return false;
            }
        }
        this.NewactivitycodeService.insertnewactivitycodeItems(levelthreeObject).subscribe(
            data => {
                this.router.navigate(['generalsetup/activitylevelthree']);
                alert("Sub Siri Details has been Updated Successfully");

            }, error => {
                console.log(error);
            });
    }
    deletelevelthree(object) {
        var cnf = confirm("Do you really want to delete");
        if(cnf==true){
            console.log(this.activitylevelthreeList);
            var index = this.activitylevelthreeList.indexOf(object);;
            if (index > -1) {
                this.activitylevelthreeList.splice(index, 1);
            }
            //  this.onBlurMethod();
        }
    }
    addactivitylevelthree() {

        console.log(this.activitylevelthreeData);
        
        if (this.activitylevelthreeData['f058fcode'] == undefined) {
            alert('Please Enter Sub Siri Code');
            return false;
        }
        if (this.activitylevelthreeData['f058fname'] == undefined) {
            alert('Please Enter Sub Siri Description');
            return false;
        }
        var confirmPop = confirm("Do you want to Add?");
        if (confirmPop == false) {
            return false;
        }
        // this.activitylevelthreeData['f058fparentCode'] = 2;
        // this.activitylevelthreeData['f058frefCode'] = 2;
        this.activitylevelthreeData['f058fstatus'] = 2;
        this.activitylevelthreeData['f058fupdatedBy'] = 1;
        this.activitylevelthreeData['f058fcreatedBy'] = 1;
        if(this.activitylevelthreeData['f058fcode'].length!=3) {
            alert("Please add 3 digit Group");
            return false;
        }
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.activitylevelthreeData['f058frefCode'] = sessionStorage.getItem("leveltwoparent");
        this.activitylevelthreeData['f058fparentCode'] = sessionStorage.getItem("leveltwoparent");
        this.activitylevelthreeData['f058flevelStatus'] = 2;

        var dataofCurrentRow = this.activitylevelthreeData;
        this.activitylevelthreeList.push(dataofCurrentRow);
        console.log(this.activitylevelthreeData);
        this.activitylevelthreeData = {};
    }
    onlyNumberKey(event) {
        console.log(event);
       return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
   }

}
