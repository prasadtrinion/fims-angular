import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NewactivitycodeService } from '../../service/newactivitycode.service'
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
    selector: 'ActivityleveloneComponent',
    templateUrl: 'activitylevelone.component.html'
})

export class ActivityleveloneComponent implements OnInit {
    activityleveloneList = [];
    activityleveloneData = {};
    id: number;
    constructor(
        private NewactivitycodeService: NewactivitycodeService,
        private route: ActivatedRoute,
        private router: Router
    ) { }


    ngOnInit() {
        this.activityleveloneData['f058fstatus'] = 1;

        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.getList();
    }
    getList() {
        console.log(this.id);
        let parentId =0;
        let levelId = 0;
      
        this.NewactivitycodeService.getItemsDetail(parentId,levelId).subscribe(
            data => {
                this.activityleveloneList = data['result']['data'];
            }, error => {
                console.log(error);
            });

    }
    onBlurMethod(object) {
        console.log(object);
        sessionStorage.removeItem("leveloneparent");
        sessionStorage.removeItem("levelonecode");
        sessionStorage.removeItem("parentCode");
        sessionStorage.setItem("parentCode", object['f058fcode']);

        sessionStorage.setItem("leveloneparent", object['f058fid']);
        sessionStorage.setItem("levelonecode",object['f058fcode']);

    }

    saveactivityLevelone() {
        let leveloneObject = {};
        
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            leveloneObject['f058fid'] = this.id;
        }

        for(var i=0;i<this.activityleveloneList.length;i++) {
            if(this.activityleveloneList[i]['f058fcode'].length!=2) {
                alert("Please add 2 digit Group");
                return false;
            }
        }
        leveloneObject['data'] = this.activityleveloneList;

        this.NewactivitycodeService.insertnewactivitycodeItems(leveloneObject).subscribe(
            data => {
                this.getList();
                // alert("Data has been updated");
                alert("Group Code has been Updated Successfully")
            }, error => {
                console.log(error);
            });
    }

    deletelevelone(object) {
        var cnf = confirm("Do you really want to delete");
        if(cnf==true){
            console.log(this.activityleveloneList);
            var index = this.activityleveloneList.indexOf(object);;
            if (index > -1) {
                this.activityleveloneList.splice(index, 1);
            }
        }
    }
    addactivityLevelone() {
        
        if (this.activityleveloneData['f058fcode'] == undefined) {
            alert('Please Enter Group Code');
            return false;
        }
        if (this.activityleveloneData['f058fname'] == undefined) {
            alert('Please Enter Group Description');
            return false;
        }
        var confirmPop = confirm("Do you want to Add?");
        if (confirmPop == false) {
            return false;
        }
 
        this.activityleveloneData['f058fparentCode'] = 0;
        this.activityleveloneData['f058frefCode'] = 0;
        this.activityleveloneData['f058fstatus'] = 0;

        if(this.activityleveloneData['f058fcode'].length!=2) {
            alert("Please add 2 digit Group");
            return false;
        }

        console.log(this.activityleveloneData);
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        var dataofCurrentRow = this.activityleveloneData;
        this.activityleveloneList.push(dataofCurrentRow);
        console.log(this.activityleveloneData);
        this.activityleveloneData = {};
        //this.saveactivityLevelone();

    }
    onlyNumberKey(event) {
        console.log(event);
       return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
   }

}


    