import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NewactivitycodeService } from '../../service/newactivitycode.service'
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
    selector: 'ActivityleveltwoComponent',
    templateUrl: 'activityleveltwo.component.html'
})

export class ActivityleveltwoComponent implements OnInit {
    activityleveltwoList = [];
    activityleveltwoData = {};
    activitylevelone:string;
    id: number;

    constructor(
        private NewactivitycodeService: NewactivitycodeService,
        private route: ActivatedRoute,
        private router: Router
    ) { }

    ngOnInit() {
        this.getList();
    }
    getList(){
        this.activitylevelone = sessionStorage.getItem("levelonecode");
        this.activityleveltwoData['f058fstatus'] = 1;

        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        console.log(this.id);
        this.activityleveltwoData['f058frefCode'] = sessionStorage.getItem("leveloneparent");
        this.activityleveltwoData['f058fparentCode'] = sessionStorage.getItem("leveloneparent");
        this.activityleveltwoData['f058flevelStatus'] = 1;
        let parentId =sessionStorage.getItem("leveloneparent");
        let levelId = 1;
       
            this.NewactivitycodeService.getItemsDetail(parentId,levelId).subscribe(
                data => {
                    this.activityleveltwoList = data['result']['data'];
                }, error => {
                    console.log(error);
                });
    
        console.log(this.id);
    }
    onBlurMethod(object) {
        console.log(object);
        sessionStorage.removeItem("leveltwoparent");
        sessionStorage.removeItem("leveltwocode");
        sessionStorage.setItem("leveltwoparent", object['f058fid']);
        sessionStorage.setItem("leveltwocode",object['f058fcode']);

    }
    
    saveactivityleveltwo() {
        let leveltwoObject = {};
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            leveltwoObject['f058fid'] = this.id;
        }

        leveltwoObject['data'] = this.activityleveltwoList;
        for(var i=0;i<this.activityleveltwoList.length;i++) {
            if(this.activityleveltwoList[i]['f058fcode'].length!=2) {
                alert("Please add 2 digit Sub Group");
                return false;
            }
        }
        this.NewactivitycodeService.insertnewactivitycodeItems(leveltwoObject).subscribe(
            data => {
                this.getList();
                // alert("Data has been updated");
                alert("Sub Group Details has been Updated Successfully")
            }, error => {
                console.log(error);
            });
    }
    deleteleveltwo(object) {
        var cnf = confirm("Do you really want to delete");
        if(cnf==true){
            console.log(this.activityleveltwoList);
            var index = this.activityleveltwoList.indexOf(object);;
            if (index > -1) {
                this.activityleveltwoList.splice(index, 1);
            }
            //  this.onBlurMethod();
        }
    }
    addactivityLeveltwo() {

        console.log(this.activityleveltwoData);
       
        if (this.activityleveltwoData['f058fcode'] == undefined) {
            alert('Please Enter Sub Group Code');
            return false;
        }
        if (this.activityleveltwoData['f058fname'] == undefined) {
            alert('Enter Sub Siri Description');
            return false;
        }
        var confirmPop = confirm("Do you want to Add?");
        if (confirmPop == false) {
            return false;
        }
        if(this.activityleveltwoData['f058fcode'].length!=2) {
            alert("Please add 2 digit Sub Group");
            return false;
        }

        this.activityleveltwoData['f058fstatus'] = 0;
        this.activityleveltwoData['f058fupdatedBy'] = 1;
        this.activityleveltwoData['f058fcreatedBy'] = 1;
        this.activityleveltwoData['f058frefCode'] = sessionStorage.getItem("leveloneparent");
        this.activityleveltwoData['f058fparentCode'] = sessionStorage.getItem("leveloneparent");
        this.activityleveltwoData['f058flevelStatus'] = 1;

        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        var dataofCurrentRow = this.activityleveltwoData;
        this.activityleveltwoList.push(dataofCurrentRow);
        console.log(this.activityleveltwoData);
        this.activityleveltwoData = {};

    }
    onlyNumberKey(event) {
        console.log(event);
       return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
   }

}
