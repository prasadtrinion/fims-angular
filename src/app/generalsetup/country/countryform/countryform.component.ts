import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { CountryService } from '../../service/country.service'
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({

    selector: 'CountryformComponent',
    templateUrl: 'countryform.component.html'

})

export class CountryFormComponent implements OnInit {
    countryform: FormGroup;
    countryList = [];
    countryidparentList = [];
    CountryData = {};
    id: number;
    constructor(
        private CountryService: CountryService,
        private route: ActivatedRoute,
        private router: Router
    ) { }

    ngOnInit() {
        this.CountryData['f013fstatus'] = 1;

        this.countryform = new FormGroup({
            country: new FormControl('', Validators.required),
            iso : new FormControl('', Validators.required),
            shortname : new FormControl('', Validators.required),
        });
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.CountryService.getItems().subscribe(
            data => {
                this.countryidparentList = data['result']['data'];
            }, error => {
                console.log(error);
            });
        console.log(this.id);

        if (this.id > 0) {
            this.CountryService.getItemsDetail(this.id).subscribe(
                data => {
                    this.CountryData = data['result'];
                    // if (data['result'].f013fstatus === true) {
                    //     console.log(data);
                    //     this.CountryData['f013fstatus'] = 1;
                    // } else {
                    //     this.CountryData['f013fstatus'] = 0;

                    // }

                    // this.CountryData['f013fstatus'] = parseInt(data['result'][0]['f013fstatus']);

                    console.log(this.CountryData);
                }, error => {
                    console.log(error);
                });
        } console.log(this.id);
       
    }


    addCountry() {
        console.log(this.CountryData);
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        this.CountryData['f013fupdatedBy'] = 1;
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        if (this.id > 0) {
            this.CountryService.updatecountryItems(this.CountryData, this.id).subscribe(
                data => {
                    if(data['status'] == 409){
                        alert('Country Name Already Exist');
                    }
                    else{
                        this.router.navigate(['generalsetup/country']);
                        alert("Country has been Updated Successfully");
                    }
                    
                }, error => {
                    alert('Country Name Already Exist');
                    return false;
                    // console.log(error);
                });


        } else {
            this.CountryService.insertcountryItems(this.CountryData).subscribe(
                data => {
                    if(data['status'] == 409){
                        alert('Country Name Already Exist');
                    }
                    else{
                        this.router.navigate(['generalsetup/country']);
                        alert("Country has been Added Successfully");

                    }
                   
                }, error => {
                    console.log(error);
                });
        }

    }
}