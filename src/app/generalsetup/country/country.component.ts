import {Injector} from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { CountryService } from '../service/country.service'
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'Country',
    templateUrl: 'country.component.html'
})

export class CountryComponent implements OnInit {
    countryList = [];
    countryData = {};
    id: number;

    constructor(
        
        private CountryService: CountryService,
        private spinnerService: Ng4LoadingSpinnerService,
        private spinner: NgxSpinnerService

    ) { }

    ngOnInit() {
        this.spinner.show(); 
        this.spinnerService.show();
        this.CountryService.getItems().subscribe(
            data => {
                this.spinner.hide();
                this.spinnerService.hide();
                this.countryList = data['result']['data'];
        }, error => {
            console.log(error);
        });
    }

    addcountry(){
           console.log(this.countryData);
        this.CountryService.insertcountryItems(this.countryData).subscribe(
            data => {
                this.countryList = data['todos'];
        }, error => {
            console.log(error);
        });

    }
}