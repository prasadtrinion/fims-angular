import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { GLCodeService } from "../service/glcodesetup.service";
import { faSearch, faEdit } from '@fortawesome/free-solid-svg-icons';
import { NgxSpinnerService } from 'ngx-spinner';


@Component({
    selector: 'GLCodeSetupComponent',
    templateUrl: 'glcodesetup.component.html'
})

export class GLCodeSetupComponent implements OnInit {
    glcodeList =  [];
    glcodeData = {};
    faSearch = faSearch;
    faEdit = faEdit;
    
    id: number;

    constructor(
        private GLCodeService: GLCodeService,
        private spinner: NgxSpinnerService,

    ) { }
    ngOnInit() { 
        // this.spinner.show();                
        this.GLCodeService.getItems().subscribe(
            data => {
                // this.spinner.hide();
                this.glcodeList = data['result']['data'];
        }, error => {
            console.log(error);
        });
    }
}