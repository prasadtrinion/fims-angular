import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { GLCodeService } from "../../service/glcodesetup.service";
import { FundService } from '../../../generalsetup/service/fund.service';
import { ActivitycodeService } from '../../../generalsetup/service/activitycode.service';
import { AccountcodeService } from '../../../generalsetup/service/accountcode.service';
import { DepartmentService } from '../../../generalsetup/service/department.service';
import { FeeitemService } from '../../../studentfinance/service/feeitem.service';
import { DefinationmsService } from '../../../studentfinance/service/definationms.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'GlCodeSetupFormComponent',
    templateUrl: 'glcodesetupform.component.html'
})

export class GlCodeSetupFormComponent implements OnInit {
    glcodeList = [];
    glcodeData = {};
    DeptList = [];
    fundList = [];
    feeItemList = [];
    accountcodeList = [];
    activitycodeList = [];
    paymenttypeList = [];
    id: number;
    ajaxCount: number;
    constructor(

        private ActivitycodeService: ActivitycodeService,
        private AccountcodeService: AccountcodeService,
        private DepartmentService: DepartmentService,
        private FundService: FundService,
        private route: ActivatedRoute,
        private FeeitemService: FeeitemService,
        private GLCodeService: GLCodeService,
        private DefinationmsService: DefinationmsService,
        private spinner: NgxSpinnerService,
        private router: Router
    ) { }
    ngDoCheck() {
        if (this.ajaxCount == 0) {
            this.spinner.hide();
        }
    }
    ngOnInit() {
        this.ajaxCount = 0
        this.spinner.show();
        this.glcodeData['f140fstatus'] = 1;
        // payment type dropdown
        this.ajaxCount++;
        this.DefinationmsService.getPaymenttype('PaymentVoucherType').subscribe(
            data => {
                this.ajaxCount--;
                this.paymenttypeList = data['result'];
            }, error => {
                console.log(error);
            });
        // fund dropdown
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.FundService.getItems().subscribe(
            data => {
                this.fundList = data['result']['data'];
            }, error => {
                console.log(error);
            });

        // Fee Item dropdown
        this.FeeitemService.getItems().subscribe(
            data => {
                this.feeItemList = data['result']['data'];
            }, error => {
                console.log(error);
            });
        // activity dropdown
        this.ActivitycodeService.getItemsByLevel().subscribe(
            data => {
                this.activitycodeList = data['result']['data'];
            }, error => {
                console.log(error);
            });
        // account dropdown
        this.AccountcodeService.getItemsByLevel().subscribe(
            data => {
                this.accountcodeList = data['result']['data'];
            }, error => {
                console.log(error);
            });

        // department dropdown
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.DepartmentService.getItems().subscribe(
            data => {
                this.DeptList = data['result']['data'];
            }, error => {
                console.log(error);
            });

        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.ajaxCount++;
            this.GLCodeService.getItemsDetail(this.id).subscribe(
                data => {
                    this.ajaxCount--;
                    this.glcodeData = data['result'][0];
                    this.glcodeData['f140fstatus'] = parseInt(this.glcodeData['f140fstatus']);
                }, error => {
                    console.log(error);
                });
        }
    }

    addRevenueDetails() {
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.GLCodeService.updateGLCodeItems(this.glcodeData, this.id).subscribe(
                data => {
                    this.router.navigate(['generalsetup/glcodesetup']);
                    alert("GL Code Setup has been Updated Sucessfully !!");
                }, error => {
                    console.log(error);
                });


        } else {
            this.GLCodeService.insertGlCodeItems(this.glcodeData).subscribe(
                data => {
                    this.router.navigate(['generalsetup/glcodesetup']);
                    alert("GL Code Setup has been Added Sucessfully !!");


                }, error => {
                    console.log(error);
                });

        }

    }
}