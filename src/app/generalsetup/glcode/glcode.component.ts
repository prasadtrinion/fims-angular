import {Injector} from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { GlcodeService } from '../service/glcode.service'
import { Router } from '@angular/router';

@Component({
    selector: 'Glcode',
    templateUrl: 'glcode.component.html'
})

export class GlcodeComponent implements OnInit {
    glList =  [];
    glData = {};
    id : number;

    constructor(
        
        private glcodeService: GlcodeService,
        private route: Router

    ) { }

    ngOnInit() { 

        this.glcodeService.getItems().subscribe(
            data => {
                this.glList = data['result']['data'];
        }, error => {
            console.log(error);
        });
    }

    addGlCode(){
           console.log(this.glData);
        this.glcodeService.insertGlItems(this.glData).subscribe(
            data => {
               console.log
        }, error => {
            console.log(error);
        });
        console.log('save');
        this.route.navigate(['/glcode']);

    }
}