import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { GlcodeService } from '../../service/glcode.service'
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
    selector: 'GlcodeFormComponent',
    templateUrl: 'glcodeform.component.html'
})

export class GlcodeFormComponent implements OnInit {
    glCodeForm: FormGroup;
    submitted = false;
    error = '';
    glList = [];
    activity = {};
    parentCode = 0;
    previousLevelCode = 0;
    previousLevel = false;
    activityCodeList = [];
    options = [
        { name: "DR", value: 'DR' },
        { name: "CR", value: 'CR' }
    ]
    debtorlist = [
        { f041fname: "Assets", value: 'Assets' },
        { f041fname: "Profilt & Loss", value: 'Profilt & Loss' }
    ]
    glData = {};
    id: number;

    constructor(
        private formBuilder: FormBuilder,
        private glcodeService: GlcodeService,
        private route: ActivatedRoute,
        private router: Router
    ) { }

    getList(id) {
        this.parentCode = id;
        this.activity['f058frefCode'] = id;
        if (id == 0) {
            this.previousLevel = false;
        } else {
            this.previousLevel = true;
        }

        // this.glcodeService.getItemsDetail(id).subscribe(
        //     data => {
        //         this.activityCodeList = data['result']['data'];
        //     }, error => {
        //         console.log(error);
        //     });
    }

    ngOnInit() {
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.getList(this.id);
    }


    onSubmit() {
        this.submitted = true;
        // stop here if form is invalid
        if (this.glCodeForm.invalid) {
            return;
        }
        this.glcodeService.insertGlItems(this.glData).subscribe(
            data => {
                this.glList = data['todos'];
            }, error => {
                console.log(error);
            });
    }

    addGlCode() {
        console.log(this.previousLevelCode);
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));


        var activityPost = {};
        activityPost['f058fcode'] = this.activity['code'];
        activityPost['f058fname'] = this.activity['name'];
        activityPost['f058ftype'] = this.activity['type'];
        activityPost['f058fdescription'] = this.activity['description']['f041fname'];
        activityPost['f058fstatus'] = 1;
        activityPost['f058frefCode'] = this.id;

        activityPost['f058fshortCode'] = this.previousLevelCode;
        activityPost['f058fcreatedBy'] = 1;
        activityPost['f058fupdatedBy'] = 1;

        this.glcodeService.insertActivityCode(activityPost).subscribe(
            data => {
                this.getList(this.id);
                this.activity = {};

            }, error => {
                console.log(error);
            });

    }

    loadDataForNextSet(id) {
        // 
        this.router.navigate(['generalsetup/addnewglcode/' + id]);
        this.previousLevelCode = this.parentCode;
        this.getList(id);
    }
    goToListPage() {
        this.router.navigate(['generalsetup/glcode']);
    }
}