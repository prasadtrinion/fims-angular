import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { UserrejectService } from '../service/userreject.service'
import { AlertService } from './../../_services/alert.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'Userreject',
    templateUrl: 'userreject.component.html'
})

export class UserrejectComponent implements OnInit {
    rejectoneList = [];
    userrejectList = [];
    userrejectData = {};
    approvalData = {};
    selectAllCheckbox = true;

    constructor(

        private UserrejectService: UserrejectService,
        private AlertService: AlertService,
        private spinner: NgxSpinnerService
    ) { }

    ngOnInit() {
        this.getListData();
    }
    getListData() {
        this.spinner.show();
        this.UserrejectService.getItemsDetail(0).subscribe(
            data => {
                this.spinner.hide();
                this.userrejectList = data['data']['data'];
            }, error => {
                console.log(error);
            });
    } 

    // userrejectListData() {
    //     console.log(this.userrejectList);
    //     var userrejectIds = [];
    //     for (var i = 0; i < this.userrejectList.length; i++) {
    //         if(this.userrejectList[i].f017fapprovedStatus==true) {
    //             approvaloneIds.push(this.userrejectList[i].f063fid);
    //         }
    //     }
    //     var userrejectUpdate = {};
    //     userrejectUpdate['id'] = userrejectIds;
    //     userrejectUpdate['reason'] = this.userrejectData['f063fapprove1Reson'];

    //     this.UserrejectService.updateUserrejectItems(userrejectUpdate).subscribe(
    //         data => {
    //             this.AlertService.success(" Updated Sucessfully ! ");

    //             this.getListData();
    //     }, error => {
    //         console.log(error);
    //     });
    // }



    userrejectListData() {
        console.log(this.userrejectList);

        var userrejectIds = [];
        for (var i = 0; i < this.userrejectList.length; i++) {
            if (this.userrejectList[i].rejectStatus == true) {
                userrejectIds.push(this.userrejectList[i].f014fid);
            }
        }
        if (userrejectIds.length < 1) {
            alert("Please select atleast one user to Reject");
            return false;
        }
        if (this.approvalData['reason'] == undefined) {
            alert('Enter the Description');
            return false;
        }
        var confirmPop = confirm("Do you want to Reject?");
        if (confirmPop == false) {
            return false;
        }
        var budgetincrementDataObject = {}
        budgetincrementDataObject['id'] = userrejectIds;
        // budgetincrementDataObject['status'] = '';
        budgetincrementDataObject['reason'] = this.approvalData['reason'];
        // userrejectUpdate['reason'] = this.userrejectData['f063fapprove1Reson'];
        this.UserrejectService.rejectUserreject(budgetincrementDataObject).subscribe(
            data => {
                this.getListData();
                this.approvalData['reason'] = '';
                alert("Rejected Successfully")
            }, error => {
                console.log(error);
            });

    }



}
