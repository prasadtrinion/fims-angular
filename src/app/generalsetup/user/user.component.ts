
import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { UserService } from '../service/user.service'
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'User',
    templateUrl: 'user.component.html'
})

export class UserComponent implements OnInit {
    userList = [];
    userData = {};
    id: number;

    constructor(
        private UserService: UserService,
        private spinner: NgxSpinnerService
    ) { }

    ngOnInit() {
        this.spinner.show();
        this.UserService.getItems().subscribe(
            data => {
                this.spinner.hide();
                let CurrentUser = localStorage.getItem('staffId');
                console.log(CurrentUser);

                let newuserData = [];
                newuserData = data['result']['data'];
                console.log("asdf");
                for (var i = 0; i < newuserData.length; i++) {
                    if (newuserData[i]['f014fstatus'] == 0) {
                        newuserData[i]['f014fstatus'] = 'Pending';
                    } else if (newuserData[i]['f014fstatus'] == 1) {
                        newuserData[i]['f014fstatus'] = 'Approved';
                    } else {
                        newuserData[i]['f014fstatus'] = 'Rejected';
                    }
                }

                for (var i = 0; i < newuserData.length; i++) {
                    if (newuserData[i]['f014fidStaff'] == parseInt(CurrentUser)) {
                        newuserData.splice(i, 1);
                    }
                }

                this.userList = newuserData;

                console.log(this.userList);


                var index = this.userList.indexOf(CurrentUser);
                if (index > -1) {
                    this.userList.splice(index, 1);
                }


            }, error => {
                console.log(error);
            });
    }

    addUser() {
        console.log(this.userData);
        this.UserService.insertuserItems(this.userData).subscribe(
            data => {
                this.userList = data['todos'];
            }, error => {
                console.log(error);
            });

    }
}