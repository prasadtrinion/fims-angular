import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { UserService } from '../../service/user.service'
import { RoleService } from '../../service/role.service'
import { StaffService } from "../../../loan/service/staff.service";
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';

@Component({
    selector: 'UserFormComponent',
    templateUrl: 'userform.component.html'
})

export class UserFormComponent implements OnInit {
    userform: FormGroup;
    roleList = [];
    staffList = [];
    userList = [];
    userData = {};
    dropdownSettings = {};
    selectedRoleId = {};
    roleIdArray = [];
    idStaff: number;
    edit:boolean;
    roleedit:boolean;
    useredit:boolean;
    passwordedit:boolean;
    selectedRole=[];
    
    id: number;
    constructor(

        private UserService: UserService,
        private RoleService: RoleService,
        private StaffService: StaffService,
        private route: ActivatedRoute,
        private router: Router,

    ) { }

    ngOnInit() {
        // this.userData['f014fstatus']=1;
        this.userData['f014fidRole'] = '';
        this.edit = false;
        this.passwordedit = false;
        this.roleedit = false;
        this.useredit = false;
        console.log(this.edit);


        this.userform = new FormGroup({
            user: new FormControl('', Validators.required),

        });

        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
         //staff dropdown
         this.StaffService.getItems().subscribe(
            data => {
                this.staffList = data['result']['data'];
            }
        );
        if (this.id > 0) {

             //staff dropdown
        // this.StaffService.getNewItems().subscribe(
        //     data => {
        //         this.staffList = data['result']['data'];
        //     }
        // );
            this.passwordedit = true;
            this.useredit = true;

            console.log(this.edit);

            this.UserService.getItemsDetail(this.id).subscribe(
                data => {
                    
                    this.userData = data['result'];
                    if(parseInt(this.userData['f014fstatus']) ==0){
                        this.edit = false;
                    }
                    else{
                        this.edit = true;
                        this.roleedit = false;
                    }
                 this.userData['f014fidStaff'] = this.userData['f014fidStaff'].toString();
                    // this.roleList = data['result']['roles'];
                    for (var i = 0; i < this.userData['roles'].length; i++) {
                        this.selectedRole.push(JSON.parse(this.userData['roles'][i]['f014fidRole'],this.userData['roles'][i]['f021froleName']));
                    }
                    this.userData['f014fidRole'] = this.selectedRole;
                    console.log(this.userData);
                    this.getStaffDetails();
                }, error => {
                    console.log(error);
                });
        }
       
        // role dropdown
        this.RoleService.getActiveItems().subscribe(
            data => {
                this.roleList = data['result']['data'];
            }, error => {
                console.log(error);
            });

        

    }

    getStaffDetails() {

 
        let idStaff = this.userData['f014fidStaff'];
        this.StaffService.getEmail(idStaff).subscribe(
            data => {
                this.userData['f014femail'] = data['result']['employeeemail'];
                let email = data['result']['employeeemail'];
                let newuserID = email.split('@');
                this.userData['f014fuserName'] = newuserID[0];
            }
        );
    }
    addUser() {

        // this.userData['f014fidRole'] = 1;
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.userData['f014fid'] = this.id;
        } 

            console.log(this.userData);

            this.UserService.insertuserItems(this.userData).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert('user Name Already Exist');
                    }
                    else {
                        this.router.navigate(['generalsetup/user']);
                        alert("User has been Saved Successfully")
                    }
                }, error => {
                    console.log(error);
                });

        }
}