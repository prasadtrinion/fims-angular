import { Injectable } from '@angular/core';
import { HttpClientModule,HttpHeaders, HttpClient} from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({ 'X-Auth-Client': 'qazwsx123',
    'Content-Type': 'application/json' })
    
  };
  
@Injectable()

export class BankService {
    url: string = environment.api.base + environment.api.endPoints.bank;
    cashbankurl: string = environment.api.base + environment.api.endPoints.cashbank;
    bankActiveListURL : string = environment.api.base + environment.api.endPoints.bankActiveList;

    constructor(private httpClient: HttpClient) { 
        
    }
    
    getItems() {
        return this.httpClient.get(this.url,httpOptions);
    }
    getCashBankItems() {
        return this.httpClient.get(this.cashbankurl,httpOptions);
    }
    getItemsDetail(id) {

        return this.httpClient.get(this.url+'/'+id,httpOptions);
    }
    getActiveBank(){

        return this.httpClient.get(this.bankActiveListURL,httpOptions);

    }
    insertBankItems(bankData): Observable<any> {

        return this.httpClient.post(this.url, bankData,httpOptions);
    }

    updateBankItems(bankData,id): Observable<any> {

        return this.httpClient.put(this.url+'/'+id, bankData,httpOptions);
       }
}