import { Injectable } from '@angular/core';
import { HttpClientModule,HttpHeaders, HttpClient} from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({ 'X-Auth-Client': 'qazwsx123',
    'Content-Type': 'application/json' })
    
  };

@Injectable()

export class CountryService {
    url: string = environment.api.base + environment.api.endPoints.country;
    GetactiveListUrl = environment.api.base + environment.api.endPoints.activeCountryList;
    constructor(private httpClient: HttpClient) { 
        
    }
    
    getItems() {
        return this.httpClient.get(this.url,httpOptions);
    }

    getItemsDetail(id) {
        return this.httpClient.get(this.url+'/'+id, httpOptions);
    }

    getActiveCountryList (){
        return this.httpClient.get(this.GetactiveListUrl,httpOptions);

    }

   

    insertcountryItems(countryData): Observable<any> {
        return this.httpClient.post(this.url, countryData,httpOptions);
    }

    updatecountryItems(countryData,id): Observable<any> {
        return this.httpClient.put(this.url+'/'+id, countryData,httpOptions);
    }
}