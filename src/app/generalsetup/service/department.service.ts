import { Injectable } from '@angular/core';
import { HttpClientModule, HttpHeaders, HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({
        'X-Auth-Client': 'qazwsx123',
        'Content-Type': 'application/json'
    })
};

@Injectable()

export class DepartmentService {
    url: string = environment.api.base + environment.api.endPoints.department;
    costCenterByFinancialYearUrl: string = environment.api.base + environment.api.endPoints.costCenterByFinancialYear;
    departmentViewUrl: string = environment.api.base + environment.api.endPoints.departmentCodeView;
    unitViewUrl: string = environment.api.base + environment.api.endPoints.unitCodeView;
    ActiveListurl: string = environment.api.base + environment.api.endPoints.cashadvanceActive;
    getNonAllocatedCostcenterurl: string = environment.api.base + environment.api.endPoints.getNonAllocatedCostcenter;
    zerodepartment: string = environment.api.base + environment.api.endPoints.getZeroDepartments;
    unitcode: string = environment.api.base + environment.api.endPoints.getByUnitCode;


    constructor(private httpClient: HttpClient) {

    }

    getItems() {
        return this.httpClient.get(this.url, httpOptions);
    }
    getActiveItems() {
        return this.httpClient.get(this.ActiveListurl, httpOptions);
    }
    getZerodepartment() {
        return this.httpClient.get(this.zerodepartment, httpOptions);
    }

    getItemsDetail(id) {
        return this.httpClient.get(this.url + '/' + id, httpOptions);
    }
    getByUnitCodeItems() {
        return this.httpClient.get(this.unitcode, httpOptions);
    }
    getByUnitCodePrItems(id) {
        return this.httpClient.get(this.unitcode+'/'+id, httpOptions);
    }
    insertDeptItems(deptData): Observable<any> {
        console.log(httpOptions);

        return this.httpClient.post(this.url, deptData, httpOptions);
    }

    getCostCenterByIdFinancialYear(id) {
        return this.httpClient.get(this.costCenterByFinancialYearUrl + '/' + id, httpOptions);
    }

    pendingCostCenterByIdFinancialYear(id) {
        return this.httpClient.get(this.getNonAllocatedCostcenterurl + '/' + id, httpOptions);
    }

    updatedepartmentItems(deptData, id): Observable<any> {
        return this.httpClient.put(this.url + '/' + id, deptData, httpOptions);
    }

    getdepartmentcodes() {
        return this.httpClient.get(this.departmentViewUrl, httpOptions);
    }

    getunitcodes() {
        return this.httpClient.get(this.unitViewUrl, httpOptions);
    }
}