import { Injectable } from '@angular/core';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';

const httpOptions = {
    headers: new HttpHeaders({
        'X-Auth-Client': 'qazwsx123',
        'Content-Type': 'application/json'
    })

};
@Injectable()

export class AccountcodeService {
    url: string = environment.api.base + environment.api.endPoints.accountCode;
    ActiveListurl: string = environment.api.base + environment.api.endPoints.cashadvanceActive;
    constructor(private httpClient: HttpClient) {

    }
    getItems() {
        let headersd = new Headers();
        headersd.append('Api-User-Agent', 'Example/1.0');

        return this.httpClient.get(environment.api.base+'accountCode', httpOptions);
    }
    getActiveItems() {
        return this.httpClient.get(this.ActiveListurl,httpOptions);
   }

    getItemsByLevel() {
        let headersd = new Headers();
        headersd.append('Api-User-Agent', 'Example/1.0');

        return this.httpClient.get(this.url, httpOptions);
    }

    insertaccountCode(accountCode): Observable<any> {
        return this.httpClient.post(environment.api.base+'accountCode', accountCode, httpOptions);
    }

    // getItemsDetail(id) {
    //     return this.httpClient.get('http://fims.mtcsbdeveloper.asia/v1/accountCode/' + id, httpOptions);
    // }
    getItemsDetail(parentId,levelId) {
        return this.httpClient.get(environment.api.base+'accountParentLevelList/' + parentId +'/'+levelId, httpOptions);
    }

    getItemsById(parentId) {
        return this.httpClient.get(environment.api.base+'master/T059faccountCode/' + parentId , httpOptions);
    }


    insertAccountcodeItems(accountcodeData): Observable<any> {
        return this.httpClient.post(this.url, accountcodeData, httpOptions);
    }

    updateAccountcodeItems(accountcodeData): Observable<any> {
        return this.httpClient.put(this.url, accountcodeData, httpOptions);
    }
}