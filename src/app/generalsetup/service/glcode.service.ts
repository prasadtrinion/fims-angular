import { Injectable } from '@angular/core';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';

const httpOptions = {
    headers: new HttpHeaders({
        'X-Auth-Client': 'qazwsx123',
        'Content-Type': 'application/json'
    })

};
@Injectable()

export class GlcodeService {
    url: string = environment.api.base + environment.api.endPoints.glcode;
    constructor(private httpClient: HttpClient) {

    }

    getGlItems() {
        return this.httpClient.get(environment.api.base+'master/T014fglcode',  httpOptions);

    }

    getItems() {
        let headersd = new Headers();
        headersd.append('Api-User-Agent', 'Example/1.0');

        return this.httpClient.get(environment.api.base+'activityCode', httpOptions);
    }

    insertActivityCode(activityCode): Observable<any> {
        return this.httpClient.post(environment.api.base+'activityCode', activityCode, httpOptions);
    }

    getItemsDetail(parentId,levelId) {
        return this.httpClient.get(environment.api.base+'activityParentLevelList/' + parentId +'/'+levelId, httpOptions);
    }

    getItemsById(parentId) {
        return this.httpClient.get(environment.api.base+'master/T058factivityCode/' + parentId , httpOptions);
    }


    insertGlItems(glData): Observable<any> {
        return this.httpClient.post(this.url, glData, httpOptions);
    }

    updateGlItems(glData): Observable<any> {
        return this.httpClient.put(this.url, glData, httpOptions);
    }
}