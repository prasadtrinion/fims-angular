import { Injectable } from '@angular/core';
import { HttpClientModule,HttpHeaders, HttpClient} from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({ 'X-Auth-Client': 'qazwsx123',
    'Content-Type': 'application/json' })
    
  };
@Injectable()

export class StateService {
    url: string = environment.api.base + environment.api.endPoints.state;
    GetActiveListUrl = environment.api.base + environment.api.endPoints.activeStateList;
    stateUrl = environment.api.base + environment.api.endPoints.countryStates;

    constructor(private httpClient: HttpClient) { 
        
    }
    
    getItems() {
        return this.httpClient.get(this.url,httpOptions);
    }

    getItemsDetail(id) {
        return this.httpClient.get(this.url+'/'+id,httpOptions);
    }
    getStates(id) {
        return this.httpClient.get(this.stateUrl+'/'+id,httpOptions);
    }

    getActiveStateList(){
        return this.httpClient.get(this.GetActiveListUrl,httpOptions);
    }

    insertstateItems(stateData): Observable<any> {
        return this.httpClient.post(this.url, stateData,httpOptions);
    }

    updatestateItems(stateData,id): Observable<any> {
        return this.httpClient.put(this.url+'/'+id, stateData,httpOptions);
       }
}