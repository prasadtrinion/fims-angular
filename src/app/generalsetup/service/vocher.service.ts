import { Injectable } from '@angular/core';
import { HttpClientModule,HttpHeaders, HttpClient} from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';

const ParseHeaders = {
    headers: new HttpHeaders({
     'Content-Type'  : 'application/json'
    })
   };
   

@Injectable()

export class VocherService {
    url: string = 'http://localhost:8080/v1/vocher';
    constructor(private httpClient: HttpClient) { 
        
    }
    
    getItems() {
        return this.httpClient.get(this.url);
    }

    getItemsDetail(id) {
        return this.httpClient.get('http://localhost:8080/v1/vocher/'+id);
    }
    
    insertVocherItems(VocherData): Observable<any> {
        return this.httpClient.post(this.url, VocherData);
    }

    updateVocherItems(VocherData) {

        return this.httpClient.post('http://localhost/testing/update.php', VocherData);
       }
}