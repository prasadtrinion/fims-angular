import { Injectable } from '@angular/core';
import { HttpClientModule,HttpHeaders, HttpClient} from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({ 'X-Auth-Client': 'qazwsx123',
    'Content-Type': 'application/json' })
    
  };
  
@Injectable()

export class BanksetupaccountService {
    url: string = environment.api.base + environment.api.endPoints.banksetupaccount;

    companyBankActiveListURL : string = environment.api.base + environment.api.endPoints.companyBankActiveList;

    constructor(private httpClient: HttpClient) { 
        
    }
    
    getItems() {
        return this.httpClient.get(this.url,httpOptions);
    }

    getItemsDetail(id) {
        return this.httpClient.get(this.url+'/'+id,httpOptions);
    }

    getActiveCompanyBank(){
        return this.httpClient.get(this.companyBankActiveListURL,httpOptions);

    }

    insertBanksetupaccountItems(banksetupaccountData): Observable<any> {
        return this.httpClient.post(this.url, banksetupaccountData,httpOptions);
    }

    updateBanksetupaccountItems(banksetupaccountData,id): Observable<any> {
        return this.httpClient.put(this.url+'/'+id, banksetupaccountData,httpOptions);
       }
}