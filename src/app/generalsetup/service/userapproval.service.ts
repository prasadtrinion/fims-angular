import { Injectable } from '@angular/core';
import { HttpClientModule, HttpHeaders, HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({
        'X-Auth-Client': 'qazwsx123',
        'Content-Type': 'application/json'
    })
};
@Injectable()
export class UserapprovalService {
    getApproveUserurl: string = environment.api.base + environment.api.endPoints.getApproveUser
     approveurl: string = environment.api.base + environment.api.endPoints.userApproval
    constructor(private httpClient: HttpClient) {

    }
    getItems() {
        return this.httpClient.get(this.getApproveUserurl, httpOptions);
    }
    getItemsDetail(id) {
        return this.httpClient.get(this.getApproveUserurl, httpOptions);
    }
    insertUserapprovalItems(userapprovalData): Observable<any> {
        return this.httpClient.post(this.getApproveUserurl, userapprovalData, httpOptions);
    }
    updateUserapprovalItems(userapprovalData, id): Observable<any> {
        return this.httpClient.put(this.approveurl + '/' + id, userapprovalData, httpOptions);
    }
    updateUserapproval(userapprovalData): Observable<any> {
        return this.httpClient.post(this.approveurl, userapprovalData, httpOptions);
    }
}

