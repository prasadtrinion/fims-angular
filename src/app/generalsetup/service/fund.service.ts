import { Injectable } from '@angular/core';
import { HttpClientModule,HttpHeaders, HttpClient} from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';

const httpOptions = {
    headers: new HttpHeaders({ 'X-Auth-Client': 'qazwsx123',
    'Content-Type': 'application/json' })
    
  };
  
@Injectable()

export class FundService {
    url: string = environment.api.base + environment.api.endPoints.fund;
    ActiveListurl: string = environment.api.base + environment.api.endPoints.ActiveFund

    constructor(private httpClient: HttpClient) { 
          
    }
    getDataItems() {
        return this.httpClient.get(this.url,httpOptions);
    }

    getItems() {
        return this.httpClient.get(this.ActiveListurl,httpOptions);
    }
    getActiveItems() {
        return this.httpClient.get(this.ActiveListurl,httpOptions);
    }
    getItemsDetail(id) {
        return this.httpClient.get(this.url+'/'+id,httpOptions);
    }

    insertFundItems(fundData): Observable<any> {
        
        return this.httpClient.post(this.url, fundData,httpOptions);
    }

    updateFundItems(fundData,id): Observable<any> {
        
        return this.httpClient.put(this.url+'/'+id,fundData,httpOptions);
       }
}