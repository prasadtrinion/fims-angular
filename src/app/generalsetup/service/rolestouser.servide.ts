
import { Injectable } from '@angular/core';
import { HttpClientModule,HttpHeaders, HttpClient} from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({ 'X-Auth-Client': 'qazwsx123',
    'Content-Type': 'application/json' })
    
  };

@Injectable()

export class RoletoUserService {
    url: string = environment.api.base + environment.api.endPoints.createUserRole;
    getbyIdItemsurl:string = environment.api.base + environment.api.endPoints.getUserRoleById;
    getListurl:string = environment.api.base + environment.api.endPoints.getUserRole;

    constructor(private httpClient: HttpClient) { 
        
    }
    
    getItems() {
        return this.httpClient.get(this.getListurl,httpOptions);
    }
    getItemsDetail(id) {
        return this.httpClient.get(this.getbyIdItemsurl+'/'+id,httpOptions);
    }

   
    insertRolestoUserItems(roleData): Observable<any> {
        return this.httpClient.post(this.url, roleData,httpOptions);

    }

    updateRolestoUserItems(roleData,id): Observable<any> {
        return this.httpClient.put(this.url+'/'+id, roleData,httpOptions);
       }
}