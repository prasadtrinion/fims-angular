import { Injectable } from '@angular/core';
import { HttpClientModule,HttpHeaders, HttpClient} from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({ 'X-Auth-Client': 'qazwsx123',
    'Content-Type': 'application/json' })
    
  };
  
@Injectable()

export class NewaccountcodeService {
    url: string = environment.api.base + environment.api.endPoints.newaccountcode;
    url1: string = environment.api.base + environment.api.endPoints.accountcodereport;
    constructor(private httpClient: HttpClient) { 
        
    }
    
    getItems() {
        return this.httpClient.get(this.url,httpOptions);
    }
    downloadReport() {
        return this.httpClient.get(this.url1,httpOptions);
    }

    // getItemsDetail(id) {
    //     return this.httpClient.get(this.url+'/'+id,httpOptions);
    // }
    getItemsDetail(parentId,levelId) {
        return this.httpClient.get(environment.api.base+'accountParentLevelList/' + parentId +'/'+levelId, httpOptions);
    }

    insertnewaccountcodeItems(newaccountcodeData): Observable<any> {
        return this.httpClient.post(this.url, newaccountcodeData,httpOptions);
    }

    updatenewaccountcodeItems(newaccountcodeData,id): Observable<any> {
        return this.httpClient.put(this.url+'/'+id,newaccountcodeData,httpOptions);
       }
}