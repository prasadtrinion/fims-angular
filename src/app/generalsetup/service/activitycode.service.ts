import { Injectable } from '@angular/core';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';

const httpOptions = {
    headers: new HttpHeaders({
        'X-Auth-Client': 'qazwsx123',
        'Content-Type': 'application/json'
    })

};
@Injectable()

export class ActivitycodeService {
    url: string = environment.api.base + environment.api.endPoints.activityCode;
    constructor(private httpClient: HttpClient) {

    }

    getItems() {
        let headersd = new Headers();
        headersd.append('Api-User-Agent', 'Example/1.0');

        return this.httpClient.get(environment.api.base+'activityCode', httpOptions);
    }

    getItemsByLevel() {
        // let headersd = new Headers();
        // headersd.append('Api-User-Agent', 'Example/1.0');

        return this.httpClient.get(environment.api.base+'master/T058factivityCode', httpOptions);
    }

    insertActivityCode(activityCode): Observable<any> {
        return this.httpClient.post(environment.api.base+'activityCode', activityCode, httpOptions);
    }

    getItemsDetail(id) {
        return this.httpClient.get(environment.api.base+'activityCode/' + id, httpOptions);
    }

    getId(id) {
        return this.httpClient.get(environment.api.base+'activityCode/' + id, httpOptions);
    }


    insertActivitycodeItems(activitycodeData): Observable<any> {
        return this.httpClient.post(this.url, activitycodeData, httpOptions);
    }

    updateActivitycodeItems(activitycodeData): Observable<any> {
        return this.httpClient.put(this.url, activitycodeData, httpOptions);
    }
}