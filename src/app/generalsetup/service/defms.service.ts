import { Injectable } from '@angular/core';
import { HttpClientModule,HttpHeaders, HttpClient} from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({ 'X-Auth-Client': 'qazwsx123',
    'Content-Type': 'application/json' })
    
  };

@Injectable()

export class DefmsService {
    url: string = 'https://api.myjson.com/bins/17t43i';
    constructor(private httpClient: HttpClient) { 
        
    }
    
    getItems() {
        return this.httpClient.get(this.url);
    }

    getItemsDetail(id) {
        return this.httpClient.get('https://api.myjson.com/bins/17t43i'+id);
    }

    insertdefmsItems(defmsData): Observable<any> {
        return this.httpClient.post(this.url, defmsData);
    }

    updatedefmsItems(defmsData) {

        return this.httpClient.post('http://localhost/testing/update.php', defmsData);
       }
}