import {Injector} from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { RolemenuService } from '../service/rolemenu.service';

@Component({
    selector: 'Rolemenu',
    templateUrl: 'rolemenu.component.html'
})

export class RolemenuComponent implements OnInit {
    roleMenuList =  [];
    mData = {};
    id : number;

    constructor(
        private rolemenuService: RolemenuService
    ) { }

    ngOnInit() { 
        this.rolemenuService.getItems().subscribe(
            data => {
                this.roleMenuList = data['result']['data'];
        }, error => {
            console.log(error);
        });
    }

  
}