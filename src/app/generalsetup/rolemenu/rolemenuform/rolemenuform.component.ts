import {Injector} from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { RolemenuService } from '../../service/rolemenu.service';
import { ActivatedRoute,Router } from '@angular/router';
import { MenuService } from '../../service/menu.service';
import { RoleService } from '../../service/role.service';

@Component({
    selector: 'RolemenuFormComponent',
    templateUrl: 'rolemenuform.component.html'
})

export class RolemenuFormComponent implements OnInit {
    menuList =  [];
    roleList = [];
    roleData = {};
    roleMenuData = {};
    id: number;
    budgetMenus = [];
    arMenus = [];
    apMenus = [];
    glMenus = [];
    result = [];
    maintanenceMenus = [];
    investmentMenus = [];
    reportMenus = [];
    sfMenus = [];
    procurementMenus = [];
    assetMenus = [];
    loanMenus = [];
    cashadvancesMenus = [];
    payrollMenus = [];

    constructor(
        private roleService: RoleService,
        private rolemenuService: RolemenuService,
        private route: ActivatedRoute,
        private menuService : MenuService,
        private router:Router

    ) { }

    ngOnInit() { 
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        console.log(this.id);
        this.menuService.getItems().subscribe(
            data => {
                this.menuList = data['result']['data'];
                var i;
                for(let i of this.menuList){
                    if(i.f011fmodule == 'budget'){
                        this.budgetMenus.push(i);
                    }
                 }
                 console.log(this.budgetMenus);
                 var i;
                for(let i of this.menuList){
                    if(i.f011fmodule == 'ar'){
                        this.arMenus.push(i);
                    }
                 }
                 var i;
                for(let i of this.menuList){
                    if(i.f011fmodule == 'ap'){
                        this.apMenus.push(i);
                    }
                 }
                 var i;
                for(let i of this.menuList){
                    if(i.f011fmodule == 'sf'){
                        this.sfMenus.push(i);
                    }
                 }
                 var i;
                for(let i of this.menuList){
                    if(i.f011fmodule == 'maintainence'){
                        this.maintanenceMenus.push(i);
                    }
                 }
                 var i;
                for(let i of this.menuList){
                    if(i.f011fmodule == 'report'){
                        this.reportMenus.push(i);
                    }
                 }
                 var i;
                for(let i of this.menuList){
                    if(i.f011fmodule == 'gl'){
                        this.glMenus.push(i);
                    }
                 }
                 var i;
                for(let i of this.menuList){
                    if(i.f011fmodule == 'investment'){
                        this.investmentMenus.push(i);
                    }
                 }
                 for(let i of this.menuList){
                    if(i.f011fmodule == 'procurement'){
                        this.procurementMenus.push(i);
                    }
                 }
                 for(let i of this.menuList){
                    if(i.f011fmodule == 'assets'){
                        this.assetMenus.push(i);
                    }
                 }

                 for(let i of this.menuList){
                    if(i.f011fmodule == 'cashadvances'){
                        this.cashadvancesMenus.push(i);
                    }
                 }

                 for(let i of this.menuList){
                    if(i.f011fmodule == 'loan'){
                        this.loanMenus.push(i);
                    }
                 }

                 for(let i of this.menuList){
                    if(i.f011fmodule == 'payroll'){
                        this.payrollMenus.push(i);
                    }
                 }
                 if(this.id>0) {
                    this.rolemenuService.getItemsDetail(this.id).subscribe(
                        data => {
                            console.log(data);
                            this.result = data['result'];
        
                            for(var i=0;i<this.budgetMenus.length;i++) {

                                for(var j=0;j<this.result.length;j++) {
                                    if(this.budgetMenus[i]['f011fid']==this.result[j]['f022fidMenu'])
                                    this.budgetMenus[i]['menuStatus'] = true;
                                }
                                
                            }

                            for(var i=0;i<this.maintanenceMenus.length;i++) {

                                for(var j=0;j<this.result.length;j++) {
                                    if(this.maintanenceMenus[i]['f011fid']==this.result[j]['f022fidMenu'])
                                    this.maintanenceMenus[i]['menuStatus'] = true;
                                }
                                
                            }

                            for(var i=0;i<this.glMenus.length;i++) {

                                for(var j=0;j<this.result.length;j++) {
                                    if(this.glMenus[i]['f011fid']==this.result[j]['f022fidMenu'])
                                    this.glMenus[i]['menuStatus'] = true;
                                }
                                
                            }




                            for(var i=0;i<this.arMenus.length;i++) {
                                this.arMenus[i]['menuStatus'] = false;

                                for(var j=0;j<this.result.length;j++) {
                                    if(this.arMenus[i]['f011fid']==this.result[j]['f022fidMenu'])
                                    this.arMenus[i]['menuStatus'] = true;
                                }
                                
                            }

                            for(var i=0;i<this.apMenus.length;i++) {
                                this.apMenus[i]['menuStatus'] = false;

                                for(var j=0;j<this.result.length;j++) {
                                    if(this.apMenus[i]['f011fid']==this.result[j]['f022fidMenu'])
                                    this.apMenus[i]['menuStatus'] = true;
                                }
                                
                            }


                            for(var i=0;i<this.investmentMenus.length;i++) {
                                this.investmentMenus[i]['menuStatus'] = false;

                                for(var j=0;j<this.result.length;j++) {
                                    if(this.investmentMenus[i]['f011fid']==this.result[j]['f022fidMenu'])
                                    this.investmentMenus[i]['menuStatus'] = true;
                                }
                                
                            }



                            for(var i=0;i<this.procurementMenus.length;i++) {
                                this.procurementMenus[i]['menuStatus'] = false;

                                for(var j=0;j<this.result.length;j++) {
                                    if(this.procurementMenus[i]['f011fid']==this.result[j]['f022fidMenu'])
                                    this.procurementMenus[i]['menuStatus'] = true;
                                }
                                
                            }


                            for(var i=0;i<this.sfMenus.length;i++) {
                                this.sfMenus[i]['menuStatus'] = false;
                                for(var j=0;j<this.result.length;j++) {
                                    if(this.sfMenus[i]['f011fid']==this.result[j]['f022fidMenu'])
                                    this.sfMenus[i]['menuStatus'] = true;
                                }
                                
                            }

                            for(var i=0;i<this.assetMenus.length;i++) {
                                this.assetMenus[i]['menuStatus'] = false;
                                for(var j=0;j<this.result.length;j++) {
                                    if(this.assetMenus[i]['f011fid']==this.result[j]['f022fidMenu'])
                                    this.assetMenus[i]['menuStatus'] = true;
                                }
                                
                            }

                            for(var i=0;i<this.loanMenus.length;i++) {
                                this.loanMenus[i]['menuStatus'] = false;
                                for(var j=0;j<this.result.length;j++) {
                                    if(this.loanMenus[i]['f011fid']==this.result[j]['f022fidMenu'])
                                    this.loanMenus[i]['menuStatus'] = true;
                                }
                                
                            }

                            for(var i=0;i<this.cashadvancesMenus.length;i++) {
                                this.cashadvancesMenus[i]['menuStatus'] = false;
                                for(var j=0;j<this.result.length;j++) {
                                    if(this.cashadvancesMenus[i]['f011fid']==this.result[j]['f022fidMenu'])
                                    this.cashadvancesMenus[i]['menuStatus'] = true;
                                }
                                
                            }

                            for(var i=0;i<this.payrollMenus.length;i++) {
                                this.payrollMenus[i]['menuStatus'] = false;
                                for(var j=0;j<this.result.length;j++) {
                                    if(this.payrollMenus[i]['f011fid']==this.result[j]['f022fidMenu'])
                                    this.payrollMenus[i]['menuStatus'] = true;
                                }
                                
                            }




                            $.each(this.procurementMenus, function(i, val){
                                console.log(val['f011fid']);
                                if(val['menuStatus']==true ) {
                                    $("input[value='" + val['f011fid'] + "']").prop('checked', true);
                            
                                }

                             });

                            console.log(this.budgetMenus);
                            $.each(this.budgetMenus, function(i, val){
                                console.log(val['f011fid']);
                                if(val['menuStatus']==true ) {
                                    $("input[value='" + val['f011fid'] + "']").prop('checked', true);
                            
                                }

                             });

                             $.each(this.arMenus, function(i, val){
                                console.log(val['f011fid']);
                                if(val['menuStatus']==true ) {
                                    $("input[value='" + val['f011fid'] + "']").prop('checked', true);
                            
                                }

                             });

                             $.each(this.apMenus, function(i, val){
                                console.log(val['f011fid']);
                                if(val['menuStatus']==true ) {
                                    $("input[value='" + val['f011fid'] + "']").prop('checked', true);
                            
                                }

                             });

                             $.each(this.maintanenceMenus, function(i, val){
                                console.log(val['f011fid']);
                                if(val['menuStatus']==true ) {
                                    $("input[value='" + val['f011fid'] + "']").prop('checked', true);
                            
                                }

                             });

                             $.each(this.glMenus, function(i, val){
                                console.log(val['f011fid']);
                                if(val['menuStatus']==true ) {
                                    $("input[value='" + val['f011fid'] + "']").prop('checked', true);
                            
                                }

                             });

                             $.each(this.sfMenus, function(i, val){
                                console.log(val['f011fid']);
                                if(val['menuStatus']==true ) {
                                    $("input[value='" + val['f011fid'] + "']").prop('checked', true);
                            
                                }

                             });

                             $.each(this.investmentMenus, function(i, val){
                                console.log(val['f011fid']);
                                if(val['menuStatus']==true ) {
                                    $("input[value='" + val['f011fid'] + "']").prop('checked', true);
                            
                                }

                             });
                             
                             $.each(this.assetMenus, function(i, val){
                                console.log(val['f011fid']);
                                if(val['menuStatus']==true ) {
                                    $("input[value='" + val['f011fid'] + "']").prop('checked', true);
                            
                                }

                             });

                             $.each(this.cashadvancesMenus, function(i, val){
                                console.log(val['f011fid']);
                                if(val['menuStatus']==true ) {
                                    $("input[value='" + val['f011fid'] + "']").prop('checked', true);
                            
                                }

                             });

                             $.each(this.loanMenus, function(i, val){
                                console.log(val['f011fid']);
                                if(val['menuStatus']==true ) {
                                    $("input[value='" + val['f011fid'] + "']").prop('checked', true);
                            
                                }

                             });

                             $.each(this.payrollMenus, function(i, val){
                                console.log(val['f011fid']);
                                if(val['menuStatus']==true ) {
                                    $("input[value='" + val['f011fid'] + "']").prop('checked', true);
                            
                                }

                             });

                             

        
            
                    }, error => {
                        console.log(error);
                    });
                   
                }

            }, error => {
                console.log(error);
            });


            this.roleService.getItems().subscribe(
                data => {
                    this.roleList = data['result']['data'];
                }, error => {
                    console.log(error);
                });

        
        
        

        console.log(this.id);
    }
    addRolemenu(){
         var menu = [];
         var allVals = [];
           console.log(this.budgetMenus);
          console.log($("input[name=budget]:checked"));
          $('input[name=budget]:checked').each(function() {
            allVals.push($(this).val());
          });
          console.log(allVals);

          

        // for(var i=0;i<this.budgetMenus.length;i++) {
        //     if(this.budgetMenus[i]['menuStatus']==true) {
        //         menu.push(this.budgetMenus[i]['f011fid']);
        //     }
        // }
        // for(var i=0;i<this.apMenus.length;i++) {
        //     if(this.apMenus[i]['menuStatus']==true) {
        //         menu.push(this.apMenus[i]['f011fid']);
        //     }
        // }
        // for(var i=0;i<this.arMenus.length;i++) {
        //     if(this.arMenus[i]['menuStatus']==true) {
        //         menu.push(this.arMenus[i]['f011fid']);
        //     }
        // }
        // for(var i=0;i<this.glMenus.length;i++) {
        //     if(this.glMenus[i]['menuStatus']==true) {
        //         menu.push(this.glMenus[i]['f011fid']);
        //     }
        // }
        // for(var i=0;i<this.maintanenceMenus.length;i++) {
        //     if(this.maintanenceMenus[i]['menuStatus']==true) {
        //         menu.push(this.maintanenceMenus[i]['f011fid']);
        //     }
        // }
        // for(var i=0;i<this.sfMenus.length;i++) {
        //     if(this.sfMenus[i]['menuStatus']==true) {
        //         menu.push(this.sfMenus[i]['f011fid']);
        //     }
        // }
        // for(var i=0;i<this.procurementMenus.length;i++) {
        //     if(this.procurementMenus[i]['menuStatus']==true) {
        //         menu.push(this.procurementMenus[i]['f011fid']);
        //     }
        // }
        // for(var i=0;i<this.investmentMenus.length;i++) {
        //     if(this.investmentMenus[i]['menuStatus']==true) {
        //         menu.push(this.investmentMenus[i]['f011fid']);
        //     }
        // }

        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        console.log(menu);
        let roleObject = {};
        roleObject['f022fidRole'] = this.id;
        roleObject['f022fidMenu'] = allVals; 
        roleObject['f022fupdatedBy'] = 1; 
        roleObject['f022fcreatedBy'] = 1; 
        roleObject['f022fstatus'] = 1;
           
        this.rolemenuService.insertrolemenuItems(roleObject).subscribe(
                    data => {
                   alert("Menu has been updated for Role");
                   this.router.navigate(['generalsetup/role']);

                }, error => {
                    console.log(error);
                });
         
    }
    
}