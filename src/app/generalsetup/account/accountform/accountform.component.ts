import {Injector} from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { AccountService } from '../../service/account.service'
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'AccountFormComponent',
    templateUrl: 'accountform.component.html'
})

export class AccountFormComponent implements OnInit {
    accountList =  [];
    accountData = {};
    id: number;

    constructor(
        
        private accountService: AccountService,
        private route: ActivatedRoute,

    ) { }

    ngOnInit() { 
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        console.log(this.id);
        
        if(this.id>0) {
            this.accountService.getItemsDetail(this.id).subscribe(
                data => {
                    console.log(data);
                    this.accountData['f016fsubAccUnderLedger'] = data['result']['f016fsubAccUnderLedger'];
                    this.accountData['f016fenableTds'] = data['result']['f016fenableTds'];

                    this.accountData['f016fallowCustomFields'] = data['result']['f016fallowCustomFields'];
                    this.accountData['f016fallowChequePrinting'] = data['result']['f016fallowChequePrinting'];

                    this.accountData['f016fbudgetYn'] = data['result']['f016fbudgetYn'];
                    this.accountData['f016fmqyBudgeting'] = data['result']['f016fmqyBudgeting'];
                    this.accountData['f016fstatus'] = data['result']['f016fstatus'];

                    this.accountData['f016fid'] = data['result']['f016fid'];

    
            }, error => {
                console.log(error);
            });
        }
        

        console.log(this.id);
    }
    addAccount(){
           console.log(this.accountData);
    
           this.accountData['f016fupdatedBy']=1;
        this.accountService.insertAccountItems(this.accountData).subscribe(
            data => {
                this.accountList = data['todos'];
        }, error => {
            console.log(error);
        });

    }
}