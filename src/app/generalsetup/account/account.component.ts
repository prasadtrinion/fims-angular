import {Injector} from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { AccountService } from '../service/account.service'

@Component({
    selector: 'Account',
    templateUrl: 'account.component.html'
})

export class AccountComponent implements OnInit {
    accountList =  [];
    accountData = {};
    id: number;

    constructor(
        
        private accountService: AccountService
    ) { }

    ngOnInit() { 
        
        this.accountService.getItems().subscribe(
            data => {
                this.accountList = data['result']['data'];
        }, error => {
            console.log(error);
        });
    }

    addAccount(){
           console.log(this.accountData);
        this.accountService.insertAccountItems(this.accountData).subscribe(
            data => {
                this.accountList = data['todos'];
        }, error => {
            console.log(error);
        });

    }
}