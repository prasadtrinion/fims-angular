import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { ActivitycodeService } from '../../service/activitycode.service'
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
    selector: 'ActivitycodeFormComponent',
    templateUrl: 'activitycodeform.component.html'
})

export class ActivitycodeFormComponent implements OnInit {
    ActivitycodeForm: FormGroup;
    submitted = false;
    error = '';
    activitycodeList = [];
    activity = {};
    parentCode = 0;
    previousLevelCode = 0;
    previousLevel = false;
    activityCodeList = [];
    options = [
        { name: "DR", value: 'DR' },
        { name: "CR", value: 'CR' }
    ]
    debtorlist = [
        { f041fname: "Assets", value: 'Assets' },
        { f041fname: "Profilt & Loss", value: 'Profilt & Loss' }
    ]
    activitycodeData = {};
    id: number;

    constructor(
        private formBuilder: FormBuilder,
        private ActivitycodeService: ActivitycodeService,
        private route: ActivatedRoute,
        private router: Router
    ) { }

    getList(id) {
        this.parentCode = id;
        this.activity['f058frefCode'] = id;
        if (id == 0) {
            this.previousLevel = false;
        } else {
            this.previousLevel = true;
        }

        this.ActivitycodeService.getItemsDetail(id).subscribe(
            data => {
                this.activityCodeList = data['result']['data'];
            }, error => {
                console.log(error);
            });
    }

    ngOnInit() {
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.getList(this.id);
    }


    onSubmit() {
        this.submitted = true;
        // stop here if form is invalid
        if (this.ActivitycodeForm.invalid) {
            return;
        }
        this.ActivitycodeService.insertActivitycodeItems(this.activitycodeData).subscribe(
            data => {
                this.activitycodeList = data['todos'];
            }, error => {
                console.log(error);
            });
    }

    addActivitycode() {
        console.log(this.previousLevelCode);
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));


        var activityPost = {};
        activityPost['f058fcode'] = this.activity['code'];
        activityPost['f058fname'] = this.activity['name'];
        activityPost['f058ftype'] = this.activity['type'];
        activityPost['f058fdescription'] = this.activity['description']['f041fname'];
        activityPost['f058fstatus'] = 1;
        activityPost['f058frefCode'] = this.id;

        activityPost['f058fshortCode'] = this.previousLevelCode;
        activityPost['f058fcreatedBy'] = 1;
        activityPost['f058fupdatedBy'] = 1;

        this.ActivitycodeService.insertActivityCode(activityPost).subscribe(
            data => {
                this.getList(this.id);
                this.activity = {};

            }, error => {
                console.log(error);
            });

    }

    loadDataForNextSet(id) {
        // 
        this.router.navigate(['generalsetup/addnewactivitycode/' + id]);
        this.previousLevelCode = this.parentCode;
        this.getList(id);
    }
    goToListPage() {
        this.router.navigate(['generalsetup/activitycode']);
    }
}