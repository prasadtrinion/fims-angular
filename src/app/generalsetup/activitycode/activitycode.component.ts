import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { GlcodeService } from './../service/glcode.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
    selector: 'Activitycode',
    templateUrl: 'activitycode.component.html'
})

export class ActivitycodeComponent implements OnInit {
    glCodeForm: FormGroup;
    submitted = false;
    error = '';
    glList = [];
    activity = {};
    parentCode = 0;
    previousLevelCode = 0;
    previousLevel = false;
    activityCodeList = [];
    options = [
        { name: "DR", value: 'DR' },
        { name: "CR", value: 'CR' }
    ]
    debtorlist = [
        { f041fname: "Assets", value: 'Assets' },
        { f041fname: "Profilt & Loss", value: 'Profilt & Loss' }
    ]
    glData = {};
    id: number;
    parentId:number;
    level:number;

    constructor(
        private formBuilder: FormBuilder,
        private glcodeService: GlcodeService,
        private route: ActivatedRoute,
        private router: Router
    ) { }

    getList(parentId,levelId,parentCode,parentLevelId=0) {
        console.log(parentCode);// 02


        this.activity['f058fcode'] = parentCode;
        this.activity['f058frefCode'] = parentId;
        this.activity['mainParentLevel'] = parentLevelId;
        
        console.log(this.activity);

        if (parentId == 0) {
            this.previousLevel = false;
        } else {
            this.previousLevel = true;
        }
        this.glcodeService.getItemsDetail(parentId,levelId).subscribe(
            data => {
                this.activityCodeList = data['result']['data'];
            }, error => {
                console.log(error);
            });
    }

    ngOnInit() {
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.route.paramMap.subscribe((data) => this.parentId = + data.get('parentId'));
        this.route.paramMap.subscribe((data) => this.level = + data.get('level'));
        this.getList(this.id,this.level,0);
    }


    onSubmit() {
        this.submitted = true;
        // stop here if form is invalid
        if (this.glCodeForm.invalid) {
            return;
        }
        this.glcodeService.insertGlItems(this.glData).subscribe(
            data => {
                this.glList = data['todos'];
            }, error => {
                //console.log(error);
            });
    }

    addGlCode() {
        //console.log(this.previousLevelCode);
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.route.paramMap.subscribe((data) => this.parentId = + data.get('parentId'));
        this.route.paramMap.subscribe((data) => this.level = + data.get('level'));


        var activityPost = {};
        var mainparent = this.activity['f058fcode'];
        console.log(mainparent);

        activityPost['f058fcode'] = this.activity['code'];
        activityPost['f058fname'] = this.activity['name'];
        activityPost['f058ftype'] = this.activity['type'];
        activityPost['f058fdescription'] = this.activity['description'];
        activityPost['f058fstatus'] = 1;
        activityPost['f058frefCode'] = this.id;
        activityPost['f058fparentCode'] = this.id;
        activityPost['f058flevelStatus'] = this.level;

        activityPost['f058fshortCode'] = this.previousLevelCode;
        activityPost['f058fcreatedBy'] = 1;
        activityPost['f058fupdatedBy'] = 1;
        
        this.glcodeService.insertActivityCode(activityPost).subscribe(
            data => {

                this.getList(this.id,this.level,mainparent,this.activity['f058fcode']);
                this.activity['name']='';
                this.activity['description']='';

            }, error => {
                console.log(error);
            });

    }

    loadPreviousDataSet(activityCodeData) {
        this.glcodeService.getItemsById(activityCodeData.f058fparentCode).subscribe(
            data => {
                this.loadDataForNextSet(data['result']['f058fparentCode'],
                    data['result']['f058flevelStatus']-1,
                    data['result']['f058frefCode'],data['result']['f058fcode']);
            }, error => {
            });


    }

    loadDataForNextSet(parentId,level,reference,parentCode,completeObject =null) {
        console.log(parentCode);
        level = level + 1;
        this.router.navigate(['generalsetup/activitycode/' + parentId +'/'+level +'/'+reference]);
        if(completeObject==null) {
            this.getList(parentId,level,parentCode,completeObject);
        } else {
            this.getList(parentId,level,parentCode,completeObject.f058fparentCode);
        }


    }
    goToListPage() {
        this.router.navigate(['generalsetup/glcode']);
    }
}