import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DepartmentService } from '../service/department.service'
import { NgxSpinnerService } from 'ngx-spinner';


@Component({
    selector: 'Department',
    templateUrl: 'department.component.html'
})

export class DepartmentComponent implements OnInit {
    deptList =  [];
    deptData = {};
    id: number;

    constructor(
        
        private DepartmentService: DepartmentService,
        private spinner: NgxSpinnerService


    ) { }

    ngOnInit() { 
        this.spinner.show();
        
        this.DepartmentService.getItems().subscribe(
            data => {
                this.spinner.hide();
                this.deptList = data['result']['data'];
        }, error => {
            console.log(error);
        });
    }

    addDepartment(){
           console.log(this.deptData);
        this.DepartmentService.insertDeptItems(this.deptData).subscribe(
            data => {
                this.deptList = data['todos'];
            }, error => {
                console.log(error);
            });

    }
}