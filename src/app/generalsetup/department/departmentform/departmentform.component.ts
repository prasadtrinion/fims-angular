import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { DepartmentService } from '../../service/department.service'
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { BudgetcostcenterService } from "../../../budget/service/budgetcostcenter.service";

@Component({
    selector: 'DepartmentFormComponent',
    templateUrl: 'departmentform.component.html'
})

export class DepartmentFormComponent implements OnInit {
    deptform: FormGroup;
    deptList = [];
    deptData = {};
    id: number;
    departmentCodes = [];
    budgetcostcenterList=[];
    unitCodes = [];
    constructor(
        private BudgetcostcenterService: BudgetcostcenterService,
        private DepartmentService: DepartmentService,
        private spinnerService: Ng4LoadingSpinnerService,
        private route: ActivatedRoute,
        private router: Router

    ) { }

    ngOnInit() {
        this.deptData['f015fstatus'] = 1;
        this.deptData['f015fdeptCode'] = '';
        this.deptData['f015funit'] = '';

        this.deptform = new FormGroup({
            dept: new FormControl('', Validators.required)
        });
        //costcenter
        this.BudgetcostcenterService.getActiveCostCenter().subscribe(
            data => {
                this.budgetcostcenterList = data['result'];
                console.log(this.budgetcostcenterList);
            }, error => {
                console.log(error);
            });
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        console.log(this.id);
        this.DepartmentService.getdepartmentcodes().subscribe(
            data => {
                this.departmentCodes = data['result']['data'];
               
            console.log(this.departmentCodes);
            }, error => {
                console.log(error);
            });
            this.DepartmentService.getunitcodes().subscribe(
                data => {
                    this.unitCodes = data['result']['data'];
                   
                console.log(this.unitCodes);
                }, error => {
                    console.log(error);
                });
        if (this.id > 0) {
            this.DepartmentService.getItemsDetail(this.id).subscribe(
                data => {
                    this.deptData = data['result'];
                   
                console.log(this.deptData);
                }, error => {
                    console.log(error);
                });
        }


        console.log(this.id);
    }

    addDepartment() {
        console.log(this.deptData);
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
           this.deptData['f015fupdatedBy']=1;
           this.deptData['f015fdepartmentCode'] = this.deptData['f015fdeptCode'] + this.deptData['f015funit'];

           this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if(this.id>0) {
        // this.deptData['f015fdeptCode'] = this.deptData['f015fdeptCode']['f015fdepartmentCode'];
        // this.deptData['f015funit'] = this.deptData['f015funit']['f015funitCode'];
            
            this.DepartmentService.updatedepartmentItems(this.deptData,this.id).subscribe(
                data => {
                    this.router.navigate(['generalsetup/department']);
                    alert("Cost Center has been Updated Successfully")
            }, error => {
                alert('Cost Center Code Already Exist');
                return false;
                // console.log(error);
            });

    } else {
        // this.deptData['f015fdeptCode'] = this.deptData['f015fdeptCode']['f015fdepartmentCode'];
        // this.deptData['f015funit'] = this.deptData['f015funit']['f015funitCode'];
        this.DepartmentService.insertDeptItems(this.deptData).subscribe(
            data => {
                if(data['status'] == 409){
                    alert('Cost Center Code Already Exist');
                }
                else{
                    this.router.navigate(['generalsetup/department']);
                    alert("Cost Center has been Added Successfully")

                }

        }, error => {
            console.log(error);
        });

    }

}
onlyNumberKey(event) {
    console.log(event);
    return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
}
}
