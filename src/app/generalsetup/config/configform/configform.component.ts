import {Injector} from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { ConfigService } from '../../service/config.service'
import { FinancialyearService } from '../../../generalledger/service/financialyear.service'
import { ActivatedRoute,Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
    selector: 'ConfigformComponent',
    templateUrl: 'configform.component.html'

})
export class ConfigFormComponent implements OnInit {
    configform: FormGroup;
    configList = [];
    configData = {};
    configidparentList=[];
    financialyearList=[];

    id: number;
    constructor(
        private ConfigService: ConfigService,
        private FinancialyearService: FinancialyearService,
         private route: ActivatedRoute,
         private router:Router
    ) { }

    ngOnInit() {
        this.configData['f011fidFinancialYear']='';
        this.configData['f011fbudgetSplit']='';
        
        this.configform = new FormGroup({
            config: new FormControl('', Validators.required),
            // config : new FormControl('', Validators.required),
        });
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

         //financialyear  dropdown
         this.FinancialyearService.getItems().subscribe(
            data => {
                this.financialyearList = data['result']['data'];
            }, error => {
                console.log(error);
            });
            

        this.ConfigService.getItems().subscribe(
            data => {
                this.configList = data['result']['data'];
            }, error => {
                console.log(error);
            });
        
        
        if (this.id > 0) {
            this.ConfigService.getItemsDetail(this.id).subscribe(
                data => {
                    this.configData = data['result'];
                }, error => {
                    console.log(error);
                });
        } console.log(this.id);

    }              
    addConfig() {
        console.log(this.configData);
        this.configData['f011fupdatedBy'] = 1;

        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        if (this.id > 0) {
            this.ConfigService.updateconfigItems(this.configData, this.id).subscribe(
                data => {
                    this.router.navigate(['generalsetup/config']);
                }, error => {
                    console.log(error);
                });


        } else {
            this.ConfigService.insertConfigItems(this.configData).subscribe(
                data => {
                    this.router.navigate(['generalsetup/config']);
                }, error => {
                    console.log(error);
                });
        }

    }
}