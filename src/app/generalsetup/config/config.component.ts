import {Injector} from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { ConfigService } from '../service/config.service'

@Component({
    selector: 'Config',
    templateUrl: 'config.component.html'
})

export class ConfigComponent implements OnInit {
    configList =  [];
    configData = {};
    id: number;

    constructor(
        private ConfigService: ConfigService
    ) { }

   ngOnInit() {

        this.ConfigService.getItems().subscribe(
            data => {
                this.configList = data['result']['data'];
                console.log(this.configList);
            }, error => {
                console.log(error);
            });
    }
    addConfig() {
        console.log(this.configData);
        this.ConfigService.insertConfigItems(this.configData).subscribe(
            data => {
                this.configList = data['todos'];
            }, error => {
                console.log(error);
            });

    }
}