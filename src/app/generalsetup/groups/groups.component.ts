import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { GroupsService } from '../service/groups.service'


@Component({
    selector: 'Groups',
    templateUrl: 'groups.component.html'
})

export class GroupComponent implements OnInit {
    groupsList = [];
    groupsData = {};
    id: number;

    constructor(

        private groupsService: GroupsService,
    ) { }

    ngOnInit() {

        this.groupsService.getItems().subscribe(
            data => {
                this.groupsList = data['result']['data'];
            }, error => {
                console.log(error);
            });
    }
    addGroups() {
        console.log(this.groupsData);
        this.groupsService.insertGroupsItems(this.groupsData).subscribe(
            data => {
                this.groupsList = data['todos'];
            }, error => {
                console.log(error);
            });


    }
}


