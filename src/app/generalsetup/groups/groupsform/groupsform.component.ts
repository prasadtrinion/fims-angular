import {Injector} from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { GroupsService } from '../../service/groups.service';
import { ActivatedRoute,Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
    selector: 'GroupsFormComponent',
    templateUrl: 'groupsform.component.html'
})

export class GroupsFormComponent implements OnInit {
    groupsForm: FormGroup;
    submitted = false;
    error = '';
    groupidparentList=[];
    groupsList =  [];
    groupsData = {};
    id: number;

    constructor(
        private formBuilder: FormBuilder,
        private GroupsService: GroupsService,
        private route: ActivatedRoute,
        private router:Router
    ) { }

    ngOnInit() { 
        this.groupsData['f012fstatus'] = 1;
        this.GroupsService.getItems().subscribe(
            data => {
                this.groupidparentList = data['result']['data'];
                var ledget = {};
                ledget['f012fid'] = 6;
                ledget['f012fgroupName'] = "Liabilities";
                this.groupidparentList.push(ledget);
                var ledget = {};
                ledget['f012fid'] = 34;
                ledget['f012fgroupName'] = "Assets";
                this.groupidparentList.push(ledget);
        }, error => {
            console.log(error);
        });
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        
        console.log(this.id);
        
        if(this.id>0) {
            this.GroupsService.getItemsDetail(this.id).subscribe(
                data => {
                    this.groupsData = data['result'];
                    if(data['result'].f012fstatus===true) {
                        console.log(data);
                        this.groupsData['f012fstatus'] = 1;
                    } else {
                        this.groupsData['f012fstatus'] = 0;

                    }
                    console.log(this.groupsData);
    
            }, error => {
                console.log(error);
            });
        } console.log(this.id);
       
    }
    

//     onSubmit() {
//         this.submitted = true;
//         // stop here if form is invalid
//         if (this.groupsForm.invalid) {
//             return;
//         }
//         this.GroupsService.insertGroupsItems(this.groupsData).subscribe(
//             data => {
//                 this.groupsList = data['todos'];
//         }, error => {
//             console.log(error);
//         });
//    }

addGroups(){
        console.log(this.groupsData);
        this.groupsData['f012fcreatedBy']=1;
        this.groupsData['f012fupdatedBy']=1;

        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if(this.id>0) {
            this.GroupsService.updateGroupsItems(this.groupsData,this.id).subscribe(
                data => {
                    this.router.navigate(['generalsetup/groups']);
            }, error => {
                console.log(error);
            });
    } else {
        this.GroupsService.insertGroupsItems(this.groupsData).subscribe(
            data => {
                this.router.navigate(['generalsetup/groups']);
            }, error => {
            console.log(error);
        });
    }
 }
   
}
