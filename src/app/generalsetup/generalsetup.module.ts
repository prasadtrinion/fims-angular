import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../material/material.module';

import { HttpClientModule, HttpClient, HTTP_INTERCEPTORS } from '@angular/common/http';
import { DataTableModule } from 'angular-6-datatable';
import { NguiAutoCompleteModule } from '@ngui/auto-complete';
import { AccountComponent } from './account/account.component';
import { AccountFormComponent } from './account/accountform/accountform.component';

import { CompanyComponent } from './company/company.component';
import { CompanyFormComponent } from './company/companyform/companyform.component';

import { ConfigComponent } from './config/config.component';
import { ConfigFormComponent } from './config/configform/configform.component';

import { CountryComponent } from './country/country.component';
import { CountryFormComponent } from './country/countryform/countryform.component';

import { DefmsComponent } from './defms/defms.component';
import { DefmsformComponent } from './defms/defmsform/defmsform.component';

import { DeftypemsComponent } from './deftypems/deftypems.component';
import { DeftypemsFormComponent } from './deftypems/deftypemsform/deftypemsform.component';

import { DepartmentComponent } from './department/department.component';
import { DepartmentFormComponent } from './department/departmentform/departmentform.component';

import { GlcodeComponent } from './glcode/glcode.component';
import { GlcodeFormComponent } from './glcode/glcodeform/glcodeform.component';

import { GroupComponent } from './groups/groups.component';
import { GroupsFormComponent } from './groups/groupsform/groupsform.component';

import { MenuComponent } from './menu/menu.component';
import { MenuFormComponent } from './menu/menuform/menuform.component';

import { StateComponent } from './state/state.component';
import { StateFormComponent } from './state/stateform/stateform.component';

import { VocherComponent } from './vocher/vocher.component';
import { VocherFormComponent } from './vocher/vocherform/vocherform.component';

import { UserComponent } from './user/user.component';
import { UserFormComponent } from './user/userform/userform.component';

import { RoleComponent } from './role/role.component';
import { RoleFormComponent } from './role/roleform/roleform.component';

import { RolemenuComponent } from './rolemenu/rolemenu.component';
import { RolemenuFormComponent } from './rolemenu/rolemenuform/rolemenuform.component';

import { UserroleComponent } from './userrole/userrole.component';
import { UserroleFormComponent } from './userrole/userroleform/userroleform.component';

import { BankComponent } from './bank/bank.component';
import { BankFormComponent } from './bank/bankform/bankform.component';

import { CreditorsetupComponent } from "./creditorsetup/creditorsetup.component";
import { CreditorsetupformComponent } from "./creditorsetup/creditorsetupform/creditorsetupform.component";
import { CreditorsetupService } from "./service/creditorsetup.service";

import { BanksetupaccountComponent } from './banksetupaccount/banksetupaccount.component';
import { BanksetupaccountFormComponent } from './banksetupaccount/banksetupaccountform/banksetupaccountform.component';

import { FundComponent } from './fund/fund.component';
import { FundFormComponent } from './fund/fundform/fundform.component';

import { CurrencysetupComponent } from './currencysetup/currencysetup.component';
import { CurrencysetupFormComponent } from './currencysetup/currencysetupform/currencysetupform.component';

import { ActivitycodeComponent } from './activitycode/activitycode.component';
import { ActivitycodeFormComponent } from './activitycode/activitycodeform/activitycodeform.component';

import { AccountcodeComponent } from './accountcode/accountcode.component';
import { AccountcodeFormComponent } from './accountcode/accountcodeform/accountcodeform.component';
import { AccountCodereportComponent } from './accountcodereport/accountcodereport.component';

import { TaxsetupcodeComponent } from './taxsetupcode/taxsetupcode.component';
import { TaxsetupcodeFormComponent } from './taxsetupcode/taxsetupcodeform/taxsetupcodeform.component';

import { UserapprovalComponent } from './userapproval/userapproval.component';
import { UserrejectComponent } from './userreject/userreject.component';
import { UserapprovalService } from './service/userapproval.service';
import { UserrejectService } from './service/userreject.service';

import { DeftypemsService } from './service/deftypems.service';
import { CompanyService } from './service/company.service';
import { DefmsService } from './service/defms.service';
import { GlcodeService } from './service/glcode.service';
import { GroupsService } from './service/groups.service';
import { MenuService } from './service/menu.service';
import { UserService } from './service/user.service';
import { ConfigService } from './service/config.service';
import { CountryService } from './service/country.service';
import { StateService } from './service/state.service';
import { RoleService } from './service/role.service';
import { UserroleService } from './service/userrole.service';
import { GeneralsetupRoutingModule } from './generalsetup.router.module';
import { DepartmentService } from './service/department.service';
import { AccountService } from './service/account.service';
import { VocherService } from './service/vocher.service';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { RolemenuService } from './service/rolemenu.service';
import { BankService } from './service/bank.service';
import { BanksetupaccountService } from './service/banksetupaccount.service';
import { TaxsetupcodeService } from '../generalledger/service/taxsetupcode.service'
import { FundService } from './service/fund.service';
import { AlertService } from '../_services/alert.service';

import { NewaccountcodeService } from './service/newaccountcode.service';
import { LeveloneComponent } from './newaccountcode/levelone/levelone.component';
import { LeveltwoComponent } from './newaccountcode/leveltwo/leveltwo.component';
import { LevelthreeComponent } from './newaccountcode/levelthree/levelthree.component';

import { ApprovallimitComponent } from '../generalsetup/approvallimit/approvallimit.component';
import { ApprovallimitFormComponent } from '../generalsetup/approvallimit/approvallimitform/approvallimitform.component';
import { ApprovallimitService } from '../accountspayable/service/approvallimit.service';

import { NewactivitycodeService } from './service/newactivitycode.service';
import { ActivityleveloneComponent } from './newactivitycode/activitylevelone/activitylevelone.component';
import { ActivityleveltwoComponent } from './newactivitycode/activityleveltwo/activityleveltwo.component';
import { ActivitylevelthreeComponent } from './newactivitycode/activitylevelthree/activitylevelthree.component';

import { GlcodegenerationComponent } from './glcodegeneration/glcodegeneration.component';
import { GlcodegenerationFormComponent } from './glcodegeneration/glcodegenerationform/glcodegenerationform.component';
import { GlcodegenerationService } from '../generalledger/service/glcodegeneration.service';

import { RoletoUserService } from "./service/rolestouser.servide";
import { RolestoUserComponent } from "./rolestouser/rolestouser.component";
import { RolestoUserFormComponent } from "./rolestouser/rolestouserform/rolestouserform.component";

import { GLCodeService } from "./service/glcodesetup.service";
import { GLCodeSetupComponent } from "./glcodesetup/glcodesetup.component";
import { GlCodeSetupFormComponent } from "./glcodesetup/glcodesetupform/glcodesetupform.component";

import { CurrencysetupService } from './service/currencysetup.service';
import { ActivitycodeService } from './service/activitycode.service';
import { AccountcodeService } from './service/accountcode.service';
import { FinancialyearService } from '../generalledger/service/financialyear.service'
import { DefinationmsService } from '../studentfinance/service/definationms.service'
import { Ng4LoadingSpinnerModule } from 'ng4-loading-spinner';
import { JwtInterceptor } from '../_helpers/jwt.interceptors';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { CommonFundComponent } from "../shared/commonfund.component";
// import { ArraySortPipe } from "./sort.pipe";
import { NgSelectModule } from '@ng-select/ng-select';
import { SharedModule } from "../shared/shared.module";
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';



@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        MaterialModule,
        GeneralsetupRoutingModule,
        HttpClientModule,
        DataTableModule,
        Ng2SearchPipeModule,
        NguiAutoCompleteModule,
        NgSelectModule,
        NgMultiSelectDropDownModule,
        Ng4LoadingSpinnerModule.forRoot(),
        FontAwesomeModule,
        SharedModule,


    ],
    exports: [],
    declarations: [
        CompanyComponent,
        CompanyFormComponent,
        //    / ArraySortPipe,
        BankComponent,
        BankFormComponent,

        BanksetupaccountComponent,
        BanksetupaccountFormComponent,

        DefmsComponent,
        DefmsformComponent,

        DeftypemsComponent,
        DeftypemsFormComponent,

        GroupComponent,
        GroupsFormComponent,

        MenuComponent,
        MenuFormComponent,

        CreditorsetupComponent,
        CreditorsetupformComponent,

        GlcodegenerationFormComponent,
        GlcodegenerationComponent,

        GlcodeComponent,
        GlcodeFormComponent,

        DepartmentComponent,
        DepartmentFormComponent,

        AccountComponent,
        AccountFormComponent,
        AccountCodereportComponent,

        UserComponent,
        UserFormComponent,

        VocherComponent,
        VocherFormComponent,

        CountryComponent,
        CountryFormComponent,

        StateComponent,
        StateFormComponent,

        RoleComponent,
        RoleFormComponent,

        RolemenuComponent,
        RolemenuFormComponent,

        UserroleComponent,
        UserroleFormComponent,

        CurrencysetupComponent,
        CurrencysetupFormComponent,

        FundComponent,
        FundFormComponent,

        ActivitycodeComponent,
        ActivitycodeFormComponent,

        AccountcodeComponent,
        AccountcodeFormComponent,

        LeveloneComponent,
        LeveltwoComponent,
        LevelthreeComponent,

        ConfigComponent,
        ConfigFormComponent,

        TaxsetupcodeComponent,
        TaxsetupcodeFormComponent,

        ApprovallimitFormComponent,
        ApprovallimitComponent,

        ActivityleveloneComponent,
        ActivityleveltwoComponent,
        ActivitylevelthreeComponent,

        UserapprovalComponent,
        UserrejectComponent,
        CommonFundComponent,

        RolestoUserComponent,
        RolestoUserFormComponent,

        GlCodeSetupFormComponent,
        GLCodeSetupComponent
    ],
    providers: [
        CompanyService,
        DefmsService,
        DeftypemsService,
        GlcodeService,
        GroupsService,
        MenuService,
        UserService,
        ConfigService,
        CountryService,
        StateService,
        DepartmentService,
        AccountService,
        VocherService,
        RoleService,
        UserroleService,
        RolemenuService,
        BankService,
        BanksetupaccountService,
        FundService,
        CurrencysetupService,
        ActivitycodeService,
        AccountcodeService,
        FinancialyearService,
        NewaccountcodeService,
        GlcodegenerationService,
        NewactivitycodeService,
        TaxsetupcodeService,
        AlertService,
        UserapprovalService,
        UserrejectService,
        DefinationmsService,
        ApprovallimitService,
        RoletoUserService,
        GLCodeService,
        CreditorsetupService,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: JwtInterceptor,
            multi: true
        }

    ],
})
export class GeneralsetupModule {
    ngOnInit() { }
}
