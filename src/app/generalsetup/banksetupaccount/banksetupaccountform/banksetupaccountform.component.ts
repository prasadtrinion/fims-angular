import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { BanksetupaccountService } from '../../service/banksetupaccount.service'
import { BankService } from '../../service/bank.service'
import { GlcodeService } from '../../service/glcode.service'
import { DefinationmsService } from '../../../studentfinance/service/definationms.service'
import { StateService } from '../../service/state.service'
import { CountryService } from '../../service/country.service'
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { FundService } from '../../../generalsetup/service/fund.service';
import { AccountcodeService } from '../../../generalsetup/service/accountcode.service'
import { ActivitycodeService } from '../../../generalsetup/service/activitycode.service';
import { DepartmentService } from '../../../generalsetup/service/department.service';
@Component({
    selector: 'BanksetupaccountFormComponent',
    templateUrl: 'banksetupaccountform.component.html'
})

export class BanksetupaccountFormComponent implements OnInit {
    banksetupform: FormGroup;
    banksetupaccountList = [];
    banksetupaccountData = {};
    bankList = [];
    banktype=[];
    bankData = {};
    glList = [];
    glData = {};
    fundList = [];
    activitycodeList = [];
    accountcodeList = [];
    AccountTypeList = [];
    countryList = [];
    stateList = [];
    DeptList = [];
    ajaxcount = 0;
    id: number;
    ajaxCount: number;
    constructor(
        private BanksetupaccountService: BanksetupaccountService,
        private BankService: BankService,
        private FundService: FundService,
        private AccountcodeService: AccountcodeService,
        private ActivitycodeService: ActivitycodeService,
        private DepartmentService: DepartmentService,
        private DefinationmsService: DefinationmsService,
        private StateService: StateService,
        private CountryService: CountryService,
        private route: ActivatedRoute,
        private router: Router
    ) { }
    ngOnInit() {
        this.banksetupaccountData['f041fidBank'] = '';
        this.banksetupaccountData['f041fprocessing'] = '';
        this.banksetupaccountData['f041fidGlcode'] = '';
        this.banksetupaccountData['f041faccountType'] = '';
        this.banksetupaccountData['f041fcountry'] = '';
        this.banksetupaccountData['f041fstate'] = '';
         this.banksetupaccountData['f041fstatus'] = 1;

         let fbanktypeobj = {};
         fbanktypeobj['id'] = 1;
         fbanktypeobj['name'] = 'Daily';
        this.banktype.push(fbanktypeobj);
        fbanktypeobj = {};
        fbanktypeobj['id'] = 2;
        fbanktypeobj['name'] = 'Monthly';
        this.banktype.push(fbanktypeobj);
        fbanktypeobj = {};
        fbanktypeobj['id'] = 3;
        fbanktypeobj['name'] = 'Yearly';
        this.banktype.push(fbanktypeobj);


        this.banksetupform = new FormGroup({
            banksetup: new FormControl('', Validators.required)
        });
        //banksetup account dropdown
        this.BankService.getActiveBank().subscribe(
            data => {
                this.bankList = data['result']['data'];
                console.log(this.bankList);
            }, error => {
                console.log(error);
            });
         //Department dropdown
         this.ajaxCount = 0;
         this.ajaxCount++;
         this.DepartmentService.getItems().subscribe(
             data => {
                 this.ajaxCount--;
                 this.DeptList = data['result']['data'];
             }
         );
 
 
         //Activity dropdown
         this.ajaxCount++;
         this.ActivitycodeService.getItemsByLevel().subscribe(
             data => {
                 this.ajaxCount--;
                 this.activitycodeList = data['result']['data'];
             }
         );
 
         //Account dropdown
         this.ajaxCount++;
         this.AccountcodeService.getItemsByLevel().subscribe(
             data => {
                 this.ajaxCount--;
                 this.accountcodeList = data['result']['data'];
             }
         );
 
 
         //Fund dropdown
         this.ajaxCount++;
         this.FundService.getActiveItems().subscribe(
             data => {
                 this.ajaxCount--;
                 this.fundList = data['result']['data'];
             }
         );
        //country dropdown
        this.CountryService.getActiveCountryList().subscribe(
            data => {
                this.countryList = data['result']['data'];
            }, error => {
                console.log(error);
            });
        //State dropdown 
        // this.StateService.getItems().subscribe(
        //     data => {
        //         this.stateList = data['result']['data'];
        //     }, error => {
        //         console.log(error);
        //     });

        // Tax Account Type 
        this.DefinationmsService.getAccountType('AccountType').subscribe(
            data => {
                this.AccountTypeList = data['result'];
            }, error => {
                console.log(error);
            });


        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.BanksetupaccountService.getItemsDetail(this.id).subscribe(
                data => {
                    console.log(data);
                    this.banksetupaccountData = data['result'][0];
                    this.banksetupaccountData['f041fstatus'] = parseInt(data['result'][0]['f041fstatus']);

                    // this.banksetupaccountData['f041fcountry'] = this.banksetupaccountData['countryName'];
                    // this.banksetupaccountData['f041fidBank'] = this.banksetupaccountData['Bank'];
                    // this.banksetupaccountData['f041fidGlcode'] = this.banksetupaccountData['Glcode'];
                    // this.banksetupaccountData['f041fstate'] = this.banksetupaccountData['stateName'];

                    this.onCountryChange();

                    console.log(this.banksetupaccountData);
                }, error => {
                    console.log(error);
                });
        }
    }
    
    onCountryChange(){
        this.ajaxcount ++;
        if(this.banksetupaccountData['f041fcountry'] == undefined){
            this.banksetupaccountData['f041fcountry'] = 0;
        }
        this.StateService.getStates(this.banksetupaccountData['f041fcountry']).subscribe(
            data=>{
                this.ajaxcount --;
                    this.stateList = data['result'];
                    let other = {
                        'f012fid' : "-1",
                        "f012fshortName":"SH",
                        "f012fstateName":"Others"

                    };
                    this.stateList.push(other);

            }
        );
    }
    addBanksetupaccount() {
        console.log(this.banksetupaccountData);
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }

        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.BanksetupaccountService.updateBanksetupaccountItems(this.banksetupaccountData, this.id).subscribe(
                data => {
                    this.router.navigate(['generalsetup/banksetupaccount']);
                    alert("Company Bank Setup has been Updated Successfully");
                }, error => {
                    alert('Account Number Already Exist');
                    return false;
                    // console.log(error);
                });

        } else {
            this.BanksetupaccountService.insertBanksetupaccountItems(this.banksetupaccountData).subscribe(
                data => {
                    if(data['status'] == 409){
                        alert('Account Number Already Exist');
                        return false;
                    }
                    this.router.navigate(['generalsetup/banksetupaccount']);
                    alert("Company Bank Setup has been Added Successfully");

                }, error => {
                    console.log(error);
                });
        }
    }
    onlyNumberKey(event) {
        console.log(event);
        return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
    }
}