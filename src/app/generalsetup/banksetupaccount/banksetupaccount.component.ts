import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { BanksetupaccountService } from '../service/banksetupaccount.service'
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'Banksetupaccount',
    templateUrl: 'banksetupaccount.component.html'
})

export class BanksetupaccountComponent implements OnInit {
    banksetupaccountList =  [];
    banksetupaccountData = {};
    id: number;

    constructor(
        
        private BanksetupaccountService: BanksetupaccountService,
        private spinner: NgxSpinnerService

    ) { }

    ngOnInit() {
        this.spinner.show();
        this.BanksetupaccountService.getItems().subscribe(
            data => {
                this.spinner.hide();
                this.banksetupaccountList = data['result']['data'];
                

        }, error => {
            console.log(error);
        });
    }

    addBanksetupaccount(){
           console.log(this.banksetupaccountData);
        this.BanksetupaccountService.insertBanksetupaccountItems(this.banksetupaccountData).subscribe(
            data => {
                this.banksetupaccountList = data['todos'];
            }, error => {
                console.log(error);
            });

    }
}