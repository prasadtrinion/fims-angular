import {Injector} from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { RoleService } from '../../service/role.service';
import { ActivatedRoute ,Router} from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
    selector: 'RoleFormComponent',
    templateUrl: 'roleform.component.html'
})

export class RoleFormComponent implements OnInit {
    roleform: FormGroup;
    roleList =  [];
    roleData = {};
    roleidparentList = [];
    id: number;
    constructor(
        
        private roleService: RoleService,
        private route: ActivatedRoute,
        private router:Router
    ) { }

    ngOnInit() { 
        this.roleData['f021fstatus'] = 1;
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        this.roleform = new FormGroup({
            role: new FormControl('', Validators.required)
        });
        // console.log(this.id);
        
        if(this.id>0) {
            this.roleService.getItemsDetail(this.id).subscribe(
                data => {
                    console.log(data);
                    this.roleData = data['result'];
                    if (data['result'].f021fstatus === true) {
                        console.log(data);
                        this.roleData['f021fstatus'] = 1;
                    } else {
                        this.roleData['f021fstatus'] = 0;

                    }
                    console.log(data);
                    this.roleList = data['result']['data'];

                }, error => {
                    console.log(error);
                });
        }
        console.log(this.id);
    }
    addRole(){
        console.log(this.roleData);
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        this.roleData['f021fupdatedBy']=1;
        this.roleData['f021fcreatedBy'] = 1;
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.roleService.updateroleItems(this.roleData, this.id).subscribe(
                data => {
                    if(data['status'] == 409){
                        alert('Role Name Already Exist');
                    }
                    else{
                        this.router.navigate(['generalsetup/role']);
                        alert("Role has been Updated Successfully");
                    }
                   
                }, error => {
                    alert('Role Name Already Exist');
                    return false;
                    // console.log(error);
                });
        } else {
            this.roleService.insertroleItems(this.roleData).subscribe(
                data => {
                    if(data['status'] == 409){
                        alert('Role Name Already Exist');
                    }
                    else{
                        this.router.navigate(['generalsetup/role']);
                        alert("Role has been Added Successfully");

                    }
                   
                }, error => {
                    console.log(error);
                });

        }


    }
}