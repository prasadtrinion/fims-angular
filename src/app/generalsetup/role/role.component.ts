import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { RoleService } from '../service/role.service'
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'role',
    templateUrl: 'role.component.html'
})

export class RoleComponent implements OnInit {
    roleList = [];
    roleData = {};

    constructor(
        
        private roleService: RoleService,
        private spinner: NgxSpinnerService
    ) { }

    ngOnInit() {
        this.spinner.show();        
        this.roleService.getItems().subscribe(
            data => {
                this.spinner.hide();
                this.roleList = data['result']['data'];
        }, error => {
            console.log(error);
        });
    }
    addMenu(){
        console.log(this.roleData);
     this.roleService.insertroleItems(this.roleData).subscribe(
         data => {
             this.roleList = data['todos'];
     }, error => {
         console.log(error);
     });

 }
}