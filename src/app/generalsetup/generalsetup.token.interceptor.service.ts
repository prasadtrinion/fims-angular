import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { GlcodeService } from './service/glcode.service';

@Injectable()
export class AuthTokenInterceptorService implements HttpInterceptor {
    constructor(public authService: GlcodeService) { }
    
    intercept(request: HttpRequest<any>, next: HttpHandler) {
    console.log('request');
    request = request.clone({
        setHeaders: {
            Authorization: `Beareradsfdf`
        }
    });
        
    return next.handle(request);
  }
}