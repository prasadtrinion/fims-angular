import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AccountComponent } from './account/account.component';
import { AccountFormComponent } from './account/accountform/accountform.component';

import { CompanyComponent } from './company/company.component';
import { CompanyFormComponent } from './company/companyform/companyform.component';

import { ConfigComponent } from './config/config.component';
import { ConfigFormComponent } from './config/configform/configform.component';

import { CountryComponent } from './country/country.component';
import { CountryFormComponent } from './country/countryform/countryform.component';

import { DefmsComponent } from './defms/defms.component';
import { DefmsformComponent } from './defms/defmsform/defmsform.component';

import { CreditorsetupComponent } from "./creditorsetup/creditorsetup.component";
import { CreditorsetupformComponent } from "./creditorsetup/creditorsetupform/creditorsetupform.component";

import { DeftypemsComponent } from './deftypems/deftypems.component';
import { DeftypemsFormComponent } from './deftypems/deftypemsform/deftypemsform.component';

import { DepartmentComponent } from './department/department.component';
import { DepartmentFormComponent } from './department/departmentform/departmentform.component';

import { GlcodeComponent } from './glcode/glcode.component';
import { GlcodeFormComponent } from './glcode/glcodeform/glcodeform.component';

import { GroupComponent } from './groups/groups.component';
import { GroupsFormComponent } from './groups/groupsform/groupsform.component';

import { MenuComponent } from './menu/menu.component';
import { MenuFormComponent } from './menu/menuform/menuform.component';

import { StateComponent } from './state/state.component';
import { StateFormComponent } from './state/stateform/stateform.component';

import { VocherComponent } from './vocher/vocher.component';
import { VocherFormComponent } from './vocher/vocherform/vocherform.component';

import { RoleComponent } from './role/role.component';
import { RoleFormComponent } from './role/roleform/roleform.component';

import { RolemenuComponent } from './rolemenu/rolemenu.component';
import { RolemenuFormComponent } from './rolemenu/rolemenuform/rolemenuform.component';

import { UserroleComponent } from './userrole/userrole.component';
import { UserroleFormComponent } from './userrole/userroleform/userroleform.component';

import { UserComponent } from './user/user.component';
import { UserFormComponent } from './user/userform/userform.component';

import { BankComponent } from './bank/bank.component';
import { BankFormComponent } from './bank/bankform/bankform.component';

import { FundComponent } from './fund/fund.component';
import { FundFormComponent } from './fund/fundform/fundform.component';

import { TaxsetupcodeComponent } from './taxsetupcode/taxsetupcode.component';
import { TaxsetupcodeFormComponent } from './taxsetupcode/taxsetupcodeform/taxsetupcodeform.component';

import { CurrencysetupComponent } from './currencysetup/currencysetup.component';
import { CurrencysetupFormComponent } from './currencysetup/currencysetupform/currencysetupform.component';

import { ActivitycodeComponent } from './activitycode/activitycode.component';
import { ActivitycodeFormComponent } from './activitycode/activitycodeform/activitycodeform.component';

import { AccountcodeComponent } from './accountcode/accountcode.component';
import { AccountcodeFormComponent } from './accountcode/accountcodeform/accountcodeform.component';
import { AccountCodereportComponent } from './accountcodereport/accountcodereport.component';

import { BanksetupaccountComponent } from './banksetupaccount/banksetupaccount.component';
import { BanksetupaccountFormComponent } from './banksetupaccount/banksetupaccountform/banksetupaccountform.component';

import { GlcodegenerationComponent } from './glcodegeneration/glcodegeneration.component';
import { GlcodegenerationFormComponent } from './glcodegeneration/glcodegenerationform/glcodegenerationform.component';

import { ApprovallimitComponent } from '../generalsetup/approvallimit/approvallimit.component';
import { ApprovallimitFormComponent } from '../generalsetup/approvallimit/approvallimitform/approvallimitform.component';

import { LeveloneComponent } from './newaccountcode/levelone/levelone.component';
import { LeveltwoComponent } from './newaccountcode/leveltwo/leveltwo.component';
import { LevelthreeComponent } from './newaccountcode/levelthree/levelthree.component';

import { ActivityleveloneComponent } from './newactivitycode/activitylevelone/activitylevelone.component';
import { ActivityleveltwoComponent } from './newactivitycode/activityleveltwo/activityleveltwo.component';
import { ActivitylevelthreeComponent } from './newactivitycode/activitylevelthree/activitylevelthree.component';

import { UserapprovalComponent } from './userapproval/userapproval.component';
import { UserrejectComponent } from './userreject/userreject.component';

import { RolestoUserComponent } from "./rolestouser/rolestouser.component";
import { RolestoUserFormComponent } from "./rolestouser/rolestouserform/rolestouserform.component";

import { GLCodeSetupComponent } from "./glcodesetup/glcodesetup.component";
import { GlCodeSetupFormComponent } from "./glcodesetup/glcodesetupform/glcodesetupform.component";

const routes: Routes = [

  { path: 'account', component: AccountComponent },
  { path: 'addnewaccount', component: AccountFormComponent },
  { path: 'editaccount/:id', component: AccountFormComponent },

  { path: 'creditorsetup', component: CreditorsetupComponent },
  { path: 'addnewcreditorsetup', component: CreditorsetupformComponent },
  { path: 'editcreditorsetup/:id', component: CreditorsetupformComponent },

  { path: 'company', component: CompanyComponent },
  { path: 'addnewcompany', component: CompanyFormComponent },
  { path: 'editcompany/:id', component: CompanyFormComponent },

  { path: 'config', component: ConfigComponent },
  { path: 'addnewconfig', component: ConfigFormComponent },
  { path: 'editconfig/:id', component: ConfigFormComponent },

  { path: 'country', component: CountryComponent },
  { path: 'addnewcountry', component: CountryFormComponent },
  { path: 'editcountry/:id', component: CountryFormComponent },

  { path: 'defms', component: DefmsComponent },
  { path: 'addnewdefms', component: DefmsformComponent },
  { path: 'editdeftypems/:id', component: DefmsformComponent },

  { path: 'deftypems', component: DeftypemsComponent },
  { path: 'addnewdeftypems', component: DeftypemsFormComponent },
  { path: 'editdeftypems/:id', component: DeftypemsFormComponent },

  { path: 'department', component: DepartmentComponent },
  { path: 'addnewdepartment', component: DepartmentFormComponent },
  { path: 'editdepartment/:id', component: DepartmentFormComponent },

  { path: 'glcode', component: GlcodeComponent },
  { path: 'addnewglcode/:id', component: GlcodeFormComponent },
  { path: 'editglcode/:id', component: GlcodeFormComponent },

  { path: 'activitycode/:id/:level/:parentId', component: ActivitycodeComponent },
  { path: 'addnewactivitycode/:id', component: ActivitycodeFormComponent },
  { path: 'editactivitycode/:id', component: ActivitycodeFormComponent },

  { path: 'accountcode/:id/:level/:parentId', component: AccountcodeComponent },
  { path: 'accountcodereport', component: AccountCodereportComponent },
  { path: 'addnewaccountcode/:id', component: AccountcodeFormComponent },
  { path: 'editaccountcode/:id', component: AccountcodeFormComponent },

  { path: 'groups', component: GroupComponent },
  { path: 'addnewgroups', component: GroupsFormComponent },
  { path: 'editgroup/:id', component: GroupsFormComponent },

  { path: 'menu', component: MenuComponent },
  { path: 'addnewmenu', component: MenuFormComponent },
  { path: 'editmenu/:id', component: MenuFormComponent },

  { path: 'state', component: StateComponent },
  { path: 'addnewstate', component: StateFormComponent },
  { path: 'editstate/:id', component: StateFormComponent },

  { path: 'vocher', component: VocherComponent },
  { path: 'addnewvocher', component: VocherFormComponent },
  { path: 'editvocher/:id', component: VocherFormComponent },

  { path: 'role', component: RoleComponent },
  { path: 'addnewrole', component: RoleFormComponent },
  { path: 'editrole/:id', component: RoleFormComponent },

  { path: 'userrole', component: UserroleComponent },
  { path: 'addnewuserrole', component: UserroleFormComponent },
  { path: 'edituserrole/:id', component: UserroleFormComponent },

  { path: 'rolemenu', component: RolemenuComponent },
  { path: 'addnewrolemenu/:id', component: RolemenuFormComponent },
  { path: 'editrolemenu/:id', component: RolemenuFormComponent },

  { path: 'bank', component: BankComponent },
  { path: 'addnewbank', component: BankFormComponent },
  { path: 'editbank/:id', component: BankFormComponent },

  { path: 'banksetupaccount', component: BanksetupaccountComponent },
  { path: 'addnewbanksetupaccount', component: BanksetupaccountFormComponent },
  { path: 'editbanksetupaccount/:id', component: BanksetupaccountFormComponent },

  { path: 'currencysetup', component: CurrencysetupComponent },
  { path: 'addnewcurrencysetup', component: CurrencysetupFormComponent },
  { path: 'editcurrencysetup/:id', component: CurrencysetupFormComponent },

  { path: 'fund', component: FundComponent },
  { path: 'addnewfund', component: FundFormComponent },
  { path: 'editfund/:id', component: FundFormComponent },

  { path: 'levelone', component: LeveloneComponent },
  { path: 'leveltwo', component: LeveltwoComponent },
  { path: 'levelthree', component: LevelthreeComponent },

  { path: 'activitylevelone', component: ActivityleveloneComponent },
  { path: 'activityleveltwo', component: ActivityleveltwoComponent },
  { path: 'activitylevelthree', component: ActivitylevelthreeComponent },

  { path: 'user', component: UserComponent },
  { path: 'addnewuser', component: UserFormComponent },
  { path: 'edituser/:id', component: UserFormComponent },

  { path: 'approvallimit', component: ApprovallimitComponent },
  { path: 'addnewapprovallimit', component: ApprovallimitFormComponent },
  { path: 'editapprovallimit/:id', component: ApprovallimitFormComponent },

  { path: 'taxsetupcode', component: TaxsetupcodeComponent },
  { path: 'addnewtaxsetupcode', component: TaxsetupcodeFormComponent },
  { path: 'edittaxsetupcode/:id', component: TaxsetupcodeFormComponent },

  { path: 'glcodegeneration', component: GlcodegenerationComponent },
  { path: 'addnewglcodegeneration', component: GlcodegenerationFormComponent },
  { path: 'editglcodegeneration/:id', component: GlcodegenerationFormComponent },

  { path: 'userapproval', component: UserapprovalComponent },
  { path: 'userreject', component: UserrejectComponent },

  { path: 'roletouser', component: RolestoUserComponent },
  { path: 'addroletouser', component: RolestoUserFormComponent },
  { path: 'editroletouser/:id', component: RolestoUserFormComponent },

  { path: 'glcodesetup', component: GLCodeSetupComponent },
  { path: 'addglcodesetup', component: GlCodeSetupFormComponent },
  { path: 'editglcodesetup/:id', component: GlCodeSetupFormComponent },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GeneralsetupRoutingModule { }