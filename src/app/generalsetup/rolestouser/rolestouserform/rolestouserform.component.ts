import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { AlertService } from '../../../_services/alert.service';
import { ActivatedRoute, Router } from '@angular/router';
import { RoletoUserService } from "../../service/rolestouser.servide";
import { FormGroup, FormControl, Validators, PatternValidator } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { StaffService } from "../../../loan/service/staff.service";
import { RoleService } from "../../service/role.service";
import { valid } from 'semver';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { INVALID } from '@angular/forms/src/model';

@Component({
    selector: 'RolestoUserFormComponent',
    templateUrl: 'rolestouserform.component.html'
})

export class RolestoUserFormComponent implements OnInit {
    legalinformationform: FormGroup;
    RoletoUserList = [];
    RoletoUserData = {};
    RoletoUserDataheader = {};
    deleteList = [];
    StaffList = [];
    roleList = [];
    selectedRole = [];
    id: number;
    showLastRow: boolean;
    showLastRowMinus: boolean;
    showLastRowPlus: boolean;
    listshowLastRowMinus: boolean;
    listshowLastRowPlus: boolean;
    ajaxCount: number;
    roleedit: boolean;

    constructor(
        private AlertService: AlertService,
        private spinner: NgxSpinnerService,
        private RoletoUserService: RoletoUserService,
        private RoleService: RoleService,
        private StaffService: StaffService,
        private route: ActivatedRoute,
        private router: Router

    ) { }

    ngOnInit() {
        this.RoletoUserDataheader['f023fstatus'] = 1;
        this.roleedit = false;
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id < 1) {
            this.showLastRow = false;
            this.showLastRowMinus = false;
            this.showLastRowPlus = true;
        } else {
            this.showLastRow = true;

        }
        //Staff Dropdown
        this.StaffService.getAllItems().subscribe(
            data => {
                this.StaffList = data['result']['data'];
            }, error => {
                console.log(error);
            });

        //Role Dropdown
        this.RoleService.getActiveItems().subscribe(
            data => {
                this.roleList = data['result']['data'];
            }, error => {
                console.log(error);
            });
        if (this.id > 0) {
            this.RoletoUserService.getItemsDetail(this.id).subscribe(
                data => {
                    this.RoletoUserDataheader = data['result']['data'];
                    this.RoletoUserDataheader['f023fstatus'] = parseInt(this.RoletoUserDataheader['f023fstatus']);

                    this.selectedRole.push(JSON.parse(this.RoletoUserDataheader['f023fidUser']));
                    this.RoletoUserDataheader['f023fidUser'] = this.selectedRole;
                }, error => {
                    console.log(error);
                });
        }
    }

    addRolestoUser() {

        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        let RoletoUserObj={};
        RoletoUserObj['idRole'] = this.RoletoUserDataheader['f023fidRole'];
        RoletoUserObj['idUser'] = this.RoletoUserDataheader['f023fidUser'];

        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        if (this.id > 0) {
            this.RoletoUserService.updateRolestoUserItems(RoletoUserObj, this.id).subscribe(
                data => {

                    this.router.navigate(['generalsetup/roletouser']);
                    alert("Role to User has been Updated Sucessfully !");

                }, error => {
                    console.log(error);
                });
        } else {
            this.RoletoUserService.insertRolestoUserItems(RoletoUserObj).subscribe(
                data => {

                    this.router.navigate(['generalsetup/roletouser']);
                    alert("Role to User has been Added Sucessfully !");

                }, error => {
                    console.log(error);
                });
        }
    }
}


