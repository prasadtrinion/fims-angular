import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { RoleService } from '../service/role.service'
import { RoletoUserService } from "../service/rolestouser.servide";
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'rolestouser',
    templateUrl: 'rolestouser.component.html'
})

export class RolestoUserComponent implements OnInit {
    RoletoUserList = [];
    RoletoUserData = {};

    constructor(
        
        private RoletoUserService: RoletoUserService,
        private spinner: NgxSpinnerService
    ) { }

    ngOnInit() {
        this.spinner.show();        
        this.RoletoUserService.getItems().subscribe(
            data => {
                this.spinner.hide();
                this.RoletoUserList = data['result']['data'];
        }, error => {
            console.log(error);
        });
    }
}