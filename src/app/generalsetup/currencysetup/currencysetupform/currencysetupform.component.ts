import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { CurrencysetupService } from '../../service/currencysetup.service'
import { ActivatedRoute, Router } from '@angular/router';

@Component({
    selector: 'CurrencysetupFormComponent',
    templateUrl: 'currencysetupform.component.html'
})

export class CurrencysetupFormComponent implements OnInit {
    currencysetupList = [];
    currencysetupData = {};
    id: number;
    constructor(
        private CurrencysetupService: CurrencysetupService,
        private route: ActivatedRoute,
        private router: Router
    ) { }

    ngOnInit() {
        this.currencysetupData['f062fcurStatus'] = 1;
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        console.log(this.id);

        if (this.id > 0) {
            this.CurrencysetupService.getItemsDetail(this.id).subscribe(
                data => {
                    this.currencysetupData = data['result'];
                    // converting from string to int for radio button default selection
                    this.currencysetupData['f062fcurStatus'] = parseInt(data['result'][0]['f062fcurStatus']);
                    // if (data['result'].f062fcurStatus === true) {
                    //     console.log(data);
                    //     this.currencysetupData['f062fcurStatus'] = 1;
                    // } else {
                    //     this.currencysetupData['f062fcurStatus'] = 0;

                    // }
                    console.log(this.currencysetupData);
                }, error => {
                    console.log(error);
                });
        }


        console.log(this.id);
    }

    addCurrencysetup() {
        console.log(this.currencysetupData);
        this.currencysetupData['f062fcurUpdatedBy'] = 1;
        this.currencysetupData['f062fcurCreatedBy'] = 1;

        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.CurrencysetupService.updatecurrencysetupItems(this.currencysetupData, this.id).subscribe(
                data => {
                    this.router.navigate(['generalsetup/currencysetup']);
                }, error => {
                    console.log(error);
                });


        } else {
            this.CurrencysetupService.insertcurrencysetupItems(this.currencysetupData).subscribe(
                data => {
                    this.router.navigate(['generalsetup/currencysetup']);

                }, error => {
                    console.log(error);
                });

        }

    }
}