import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CurrencysetupService } from '../service/currencysetup.service'


@Component({
    selector: 'Currencysetup',
    templateUrl: 'currencysetup.component.html'
})

export class CurrencysetupComponent implements OnInit {
    currencysetupList =  [];
    currencysetupData = {};
    id: number;

    constructor(
        
        private CurrencysetupService: CurrencysetupService

    ) { }

    ngOnInit() { 
        
        this.CurrencysetupService.getItems().subscribe(
            data => {
                this.currencysetupList = data['result']['data'];
        }, error => {
            console.log(error);
        });
    }

    addCurrencysetup(){
           console.log(this.currencysetupData);
        this.CurrencysetupService.insertcurrencysetupItems(this.currencysetupData).subscribe(
            data => {
                this.currencysetupList = data['todos'];
            }, error => {
                console.log(error);
            });

    }
}