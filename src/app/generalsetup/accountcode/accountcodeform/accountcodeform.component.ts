import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AccountcodeService } from '../../service/accountcode.service'

@Component({
    selector: 'AccountcodeFormComponent',
    templateUrl: 'accountcodeform.component.html'
})

export class AccountcodeFormComponent implements OnInit {
    AccountcodeForm: FormGroup;
    submitted = false;
    error = '';
    accountcodeList = [];
    
    account = {};
    parentCode = 0;
    previousLevelCode = 0;
    previousLevel = false;
    accountCodeList = [];
    options = [
        { name: "DR", value: 'DR' },
        { name: "CR", value: 'CR' }
    ]
    debtorlist = [
        { f041fname: "Assets", value: 'Assets' },
        { f041fname: "Profilt & Loss", value: 'Profilt & Loss' }
    ]
    accountcodeData = {};
    id: number;

    constructor(
        private formBuilder: FormBuilder,
        private AccountcodeService: AccountcodeService,
        private route: ActivatedRoute,
        private router: Router
    ) { }
    
    ngOnInit() {
    }
        //     this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        //     this.getList(this.id);
        // }
    // getList(id) {
    //     this.parentCode = id;
    //     this.account['f059frefCode'] = id;
    //     if (id == 0) {
    //         this.previousLevel = false;
    //     } else {
    //         this.previousLevel = true;
    //     }

    // //     this.AccountcodeService.getItemsDetail(id).subscribe(
    // //         data => {
    // //             this.accountCodeList = data['result']['data'];
    // //         }, error => {
    // //             console.log(error);
    // //         });
    // // }
    // }

    // ngOnInit() {
    //     this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
    //     this.getList(this.id);
    // }


    // onSubmit() {
    //     this.submitted = true;
    //     // stop here if form is invalid
    //     if (this.AccountcodeForm.invalid) {
    //         return;
    //     }
    //     this.AccountcodeService.insertAccountcodeItems(this.accountcodeData).subscribe(
    //         data => {
    //             this.accountcodeList = data['todos'];
    //         }, error => {
    //             console.log(error);
    //         });
    // }

    // addAccountcode() {
    //     console.log(this.previousLevelCode);
    //     this.route.paramMap.subscribe((data) => this.id = + data.get('id'));


    //     var AccountPost = {};
    //     AccountPost['f059fcode'] = this.account['code'];
    //     AccountPost['f059fname'] = this.account['name'];
    //     AccountPost['f059ftype'] = this.account['type'];
    //     AccountPost['f059fdescription'] = this.account['description']['f041fname'];
    //     AccountPost['f059fstatus'] = 1;
    //     AccountPost['f059frefCode'] = this.id;

    //     AccountPost['f059fshortCode'] = this.previousLevelCode;
    //     AccountPost['f059fcreatedBy'] = 1;
    //     AccountPost['f059fupdatedBy'] = 1;

    //     this.AccountcodeService.insertAccountcode(AccountPost).subscribe(
    //         data => {
    //             this.getList(this.id);
    //             this.account = {};

    //         }, error => {
    //             console.log(error);
    //         });

    // }

    // loadDataForNextSet(id) {
    //     // 
    //     this.router.navigate(['generalsetup/addnewaccountcode/' + id]);
    //     this.previousLevelCode = this.parentCode;
    //     this.getList(id);
    // }
    // goToListPage() {
    //     this.router.navigate(['generalsetup/accountcode']);
    // }
}