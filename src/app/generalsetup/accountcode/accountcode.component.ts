import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { AccountcodeService } from './../service/accountcode.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
    selector: 'Accountcode',
    templateUrl: 'accountcode.component.html'
})

export class AccountcodeComponent implements OnInit {
    glCodeForm: FormGroup;
    submitted = false;
    error = '';
    glList = [];
    activity = {};
    parentCode = 0;
    previousLevelCode = 0;
    previousLevel = false;
    activityCodeList = [];
    options = [
        { name: "DR", value: 'DR' },
        { name: "CR", value: 'CR' }
    ]
    debtorlist = [
        { f041fname: "Assets", value: 'Assets' },
        { f041fname: "Profilt & Loss", value: 'Profilt & Loss' }
    ]
    glData = {};
    id: number;
    parentId:number;
    level:number;

    constructor(
        private formBuilder: FormBuilder,
        private accountService: AccountcodeService,
        private route: ActivatedRoute,
        private router: Router
    ) { }

    
    getList(parentId,levelId,parentCode) {
        this.parentCode = parentId;

        this.activity['f059fcode'] = parentCode;
        this.activity['f059frefCode'] = parentId;
        if (parentId == 0) {
            this.previousLevel = false;
        } else {
            this.previousLevel = true;
        }
        console.log(parentId);
        console.log(levelId);

        this.accountService.getItemsDetail(parentId,levelId).subscribe(
            data => {
                this.activityCodeList = data['result']['data'];
            }, error => {
                console.log(error);
            });
    }

    // ngOnInit() {
    // }
    //     this.parentCode = parentId;

    //     this.activity['f058fcode'] = parentCode;
    //     this.activity['f058frefCode'] = parentId;
    //     if (parentId == 0) {
    //         this.previousLevel = false;
    //     } else {
    //         this.previousLevel = true;
    //     }
    //     console.log(parentId);
    //     console.log(levelId);

    //     this.accountService.getItemsDetail(parentId,levelId).subscribe(
    //         data => {
    //             this.activityCodeList = data['result']['data'];
    //         }, error => {
    //             console.log(error);
    //         });
    // }

    ngOnInit() {
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.route.paramMap.subscribe((data) => this.parentId = + data.get('parentId'));
        this.route.paramMap.subscribe((data) => this.level = + data.get('level'));
       
        console.log(this.parentId);
        console.log(this.level);
        this.getList(this.id,this.level,0);
    }


    // onSubmit() {
    //     this.submitted = true;
    //     // stop here if form is invalid
    //     if (this.glCodeForm.invalid) {
    //         return;
    //     }
    //     this.accountService.insertGlItems(this.glData).subscribe(
    //         data => {
    //             this.glList = data['todos'];
    //         }, error => {
    //             console.log(error);
    //         });
    // }

    addGlCode() {
        console.log(this.previousLevelCode);
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.route.paramMap.subscribe((data) => this.parentId = + data.get('parentId'));
        this.route.paramMap.subscribe((data) => this.level = + data.get('level'));


        var activityPost = {};
        activityPost['f059fcode'] = this.activity['code'];
        activityPost['f059fname'] = this.activity['name'];
        activityPost['f059ftype'] = this.activity['type'];
        activityPost['f059fdescription'] = this.activity['description'];
        activityPost['f059fstatus'] = 1;
        activityPost['f059frefCode'] = this.id;
        activityPost['f059fparentCode'] = this.id;
        activityPost['f059flevelStatus'] = this.level;
        activityPost['f059fallowNegative'] = this.activity['f059fallowNegative'];
        activityPost['f059fshortCode'] = this.previousLevelCode;
        activityPost['f059fcreatedBy'] = 1;
        activityPost['f059fupdatedBy'] = 1;

        this.accountService.insertaccountCode(activityPost).subscribe(
            data => {
                this.getList(this.id,this.level,this.activity['code']);
                this.activity = {};

            }, error => {
                console.log(error);
            });

    }

    loadPreviousDataSet(activityCodeData) {
        this.accountService.getItemsById(activityCodeData.f059fparentCode).subscribe(
            data => {
                console.log(data);
                this.loadDataForNextSet(data['result']['f059fparentCode'],
                    data['result']['f059flevelStatus']-1,
                    data['result']['f059frefCode'],data['result']['f059fcode']);
            }, error => {
                console.log(error);
            });
    }

    loadDataForNextSet(parentId,level,reference,parentCode) {
        console.log(parentId);
        console.log(level);
        console.log(reference);
        level = level + 1;
        this.router.navigate(['generalsetup/accountcode/' + parentId +'/'+level +'/'+reference]);
        this.previousLevelCode = this.parentCode;
        this.getList(parentId,level,parentCode);
    }
    // goToListPage() {
    //     this.router.navigate(['generalsetup/glcode']);
    // }
}