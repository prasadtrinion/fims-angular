import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MenuService } from '../service/menu.service'


@Component({
    selector: 'Menu',
    templateUrl: 'menu.component.html'
})

export class MenuComponent implements OnInit {
    menuList = [];
    menuData = {};

    constructor(

        private menuService: MenuService
    ) { }

    ngOnInit() {

        this.menuService.getItems().subscribe(
            data => {
                this.menuList = data['result']['data'];
            }, error => {
                console.log(error);
            });
    }
    addMenu() {
        console.log(this.menuData);
        this.menuService.insertMenuItems(this.menuData).subscribe(
            data => {
                this.menuList = data['todos'];
            }, error => {
                console.log(error);
            });

    }
}