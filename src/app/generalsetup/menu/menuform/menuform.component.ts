import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { MenuService } from '../../service/menu.service';
import { ActivatedRoute, Router } from '@angular/router';


@Component({
    selector: 'MenuFormComponent',
    templateUrl: 'menuform.component.html'
})

export class MenuFormComponent implements OnInit {
    menuList = [];
    menuidparentList = [];
    menuData = {};
    id: number;
    token;
    moduleList = [];
    moduleNames = [];

    constructor(

        private MenuService: MenuService,
        private route: ActivatedRoute,
        private router: Router
    ) { }

    ngOnInit() {
        this.menuData['f011fidParent'] = '';
        this.moduleNames['budget'] = "Budget";
        this.moduleNames['maintainence'] = "Maintainence";
        this.moduleNames['ar'] = "Accounts Receivable";
        this.moduleNames['ap'] = "Accounts Payable";
        this.moduleNames['sf'] = "Student Finance";
        this.moduleNames['gl'] = "General Ledger";
        this.moduleNames['investment'] = "Investment";
        this.moduleNames['report'] = "Report";
        this.moduleNames['procurement'] = "Procurement";
        this.moduleNames['payroll'] = "Payroll";
        this.moduleNames['loan'] = "Loan";
        this.moduleNames['assets'] = "Assets";
        this.token = sessionStorage.getItem('f014ftoken');
        
        this.MenuService.getModules(this.token).subscribe(
            data => {
                
                this.moduleList = data['result']['data'];
                // console.log(this.moduleList);
            }, error => {
                console.log(error);
            });

        this.menuData['f011fstatus'] = 1;
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.MenuService.getItems().subscribe(
            data => {
                let menuList = data['result']['data'];
                var i;
                for(let i of menuList){
                    if(i.f011fidParent == 0){
                        this.menuidparentList.push(i);
                    }
                 }
                //  console.log(this.menuidparentList);
            }, error => {
                console.log(error);
            });
        console.log(this.id);

        if (this.id > 0) {
            this.MenuService.getItemsDetail(this.id).subscribe(
                data => {
                    this.menuData = data['result'];
                    if (data['result'].f011fstatus === true) {
                        // console.log(data);
                        this.menuData['f011fstatus'] = 1;
                    } else {
                        this.menuData['f011fstatus'] = 0;

                    }
                    console.log(data);
                    this.menuList = data['result']['data'];

                }, error => {
                    console.log(error);
                });
        }
        // console.log(this.id);
    }
    addMenu() {
        console.log(this.menuData);
        this.menuData['f011fcreatedBy'] = 1;
        this.menuData['f011fupdatedBy'] = 1;
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.MenuService.updatemenuItems(this.menuData, this.id).subscribe(
                data => {
                    this.router.navigate(['generalsetup/menu']);
                }, error => {
                    console.log(error);
                });
        } else {
            this.MenuService.insertMenuItems(this.menuData).subscribe(
                data => {
                    this.router.navigate(['generalsetup/menu']);
                }, error => {
                    console.log(error);
                });

        }


    }
}