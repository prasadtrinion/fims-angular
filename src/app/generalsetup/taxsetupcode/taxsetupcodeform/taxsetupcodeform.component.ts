import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { TaxsetupcodeService } from '../../../generalledger/service/taxsetupcode.service'
import { AccountcodeService } from '../../../generalsetup/service/accountcode.service'
import { ActivatedRoute, Router } from '@angular/router';

@Component({
    selector: 'TaxsetupcodeFormComponent',
    templateUrl: 'taxsetupcodeform.component.html'
})

export class TaxsetupcodeFormComponent implements OnInit {
    taxsetupcodeList = [];
    taxsetupcodeData = {};
    accountcodeList = [];
    ftaxtype = [];
    accountcodeData = {};

    id: number;
    constructor(
        private TaxsetupcodeService: TaxsetupcodeService,
        private AccountcodeService: AccountcodeService,
        private route: ActivatedRoute,
        private router: Router
    ) { }

    ngOnInit() {
        this.taxsetupcodeData['f081fstatus'] = 1;

        this.taxsetupcodeData['f081fidAccountCode'] = '';

        let taxtypeobj = {};
        taxtypeobj['id'] = 1;
        taxtypeobj['name'] = 'GST';
        this.ftaxtype.push(taxtypeobj);
        taxtypeobj = {};
        taxtypeobj['id'] = 2;
        taxtypeobj['name'] = 'SST';
        this.ftaxtype.push(taxtypeobj);

        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        // account dropdown
        this.AccountcodeService.getItemsByLevel().subscribe(
            data => {
                this.accountcodeList = data['result']['data'];
            }, error => {
                console.log(error);
            });

        console.log(this.id);

        if (this.id > 0) {
            this.TaxsetupcodeService.getItemsDetail(this.id).subscribe(
                data => {
                    this.taxsetupcodeData = data['result'][0];

                    this.taxsetupcodeData['f081fstatus'] = parseInt(data['result'][0]['f081fstatus']);


                    // if (data['result'].f081fstatus == '1') {
                    //     console.log(data);
                    //     this.taxsetupcodeData['f081fstatus'] = 1;
                    // } else {
                    //     this.taxsetupcodeData['f081fstatus'] = 0;

                    // }
                }, error => {
                    console.log(error);
                });
        }
        console.log(this.id);
    }

    addTaxsetupcode() {
        console.log(this.taxsetupcodeData);
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        if (this.taxsetupcodeData['f081fpercentage'] > 100) {
            alert('Percentage cannot be greater than 100');
            return false;
        }

        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.TaxsetupcodeService.updateTaxsetupcodeItems(this.taxsetupcodeData, this.id).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert('Code Already Exist');
                    }
                    else {
                        this.router.navigate(['generalsetup/taxsetupcode']);
                        alert("Tax Setup Code has been Updated Successfully")
                    }
                }, error => {
                    alert('Code Already Exist');
                    return false;
                    // console.log(error);
                });


        } else {
            this.TaxsetupcodeService.insertTaxsetupcodeItems(this.taxsetupcodeData).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert('Code Already Exist');
                    }
                    else {
                        this.router.navigate(['generalsetup/taxsetupcode']);
                        alert("Tax Setup Code has been Added Successfully")

                    }

                }, error => {
                    console.log(error);
                });

        }

    }
    onlyNumberKey(event) {
        if (event.charCode == 46) {

        } else {
            return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
        }
    }
}
