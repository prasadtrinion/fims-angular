

import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { TaxsetupcodeService } from '../../generalledger/service/taxsetupcode.service'
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'Taxsetupcode',
    templateUrl: 'taxsetupcode.component.html'
})

export class TaxsetupcodeComponent implements OnInit {
    taxsetupcodeList =  [];
    taxsetupcodeData = {};
    id: number;

    constructor(
        
        private TaxsetupcodeService: TaxsetupcodeService,
        private spinner: NgxSpinnerService

    ) { }

    ngOnInit() {
        this.spinner.show();        
        this.TaxsetupcodeService.getItems().subscribe(
            data => {
                this.spinner.hide();
                this.taxsetupcodeList = data['result']['data'];
        }, error => {
            console.log(error);
        });
    }

    addTaxsetupcode(){
           console.log(this.taxsetupcodeData);
        this.TaxsetupcodeService.insertTaxsetupcodeItems(this.taxsetupcodeData).subscribe(
            data => {
                this.taxsetupcodeList = data['todos'];
            }, error => {
                console.log(error);
            });

    }
}