
import {Injector} from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { StateService } from '../service/state.service'
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'State',
    templateUrl: 'state.component.html'
})

export class StateComponent implements OnInit {
    stateList =  [];
    stateData = {};
    id: number;

    constructor(
        private StateService: StateService,
        private spinner: NgxSpinnerService
    ) { }

    ngOnInit() {
        this.spinner.show();
        this.StateService.getItems().subscribe(
            data => {
                this.spinner.hide();
                this.stateList = data['result']['data'];
        }, error => {
            console.log(error);
        });
    }

    addState(){
           console.log(this.stateData);
        this.StateService.insertstateItems(this.stateData).subscribe(
            data => {
                this.stateList = data['todos'];
        }, error => {
            console.log(error);
        });

    }
}