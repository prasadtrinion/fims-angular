import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { StateService } from '../../service/state.service'
import { CountryService } from '../../service/country.service'
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
    selector: 'StateFormComponent',
    templateUrl: 'stateform.component.html'
})

export class StateFormComponent implements OnInit {
    stateform: FormGroup;
    countryList = [];
    stateList = [];
    stateData = {};
    id: number;
    constructor(

        private StateService: StateService,
        private countryService: CountryService,
        private route: ActivatedRoute,
        private router: Router
    ) { }

    ngOnInit() {
        this.stateData['f012fidCountry'] = '';

        this.stateform = new FormGroup({
            state: new FormControl('', Validators.required),

        });
        this.stateData['f012fstatus'] = 1;
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        this.countryService.getActiveCountryList().subscribe(
            data => {
                this.countryList = data['result']['data'];
                console.log(this.countryList);

            }, error => {
                console.log(error);
            });
        console.log(this.id);

        if (this.id > 0) {
            this.StateService.getItemsDetail(this.id).subscribe(
                data => {

                    this.stateData = data['result'];
                    // console.log(this.stateData);
                    // this.stateData['f012fidCountry'] = this.stateData['f013fcountryName'];

                    // this.stateData['f012fidCountry'] = this.stateData['f013fcountryName'];
                    console.log(this.stateData);

                }, error => {
                    console.log(error);
                });
        }

        // console.log(this.id);
    }
    addState() {
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }

        // this.stateData['f012fidCountry'] = this.stateData['f012fidCountry']['f013fid'];

        this.stateData['f012fupdatedBy'] = 1;

        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.StateService.updatestateItems(this.stateData, this.id).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert('State Name Already Exist');
                    }
                    else {
                        this.router.navigate(['generalsetup/state']);
                        alert("State has been Updated Successfully");
                    }

                }, error => {
                    alert('State Name Already Exist');
                    return false;
                    // console.log(error);
                });


        } else {

            this.StateService.insertstateItems(this.stateData).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert('State Name Already Exist');
                    }
                    else {
                        this.router.navigate(['generalsetup/state']);
                        alert("State has been Added Successfully");
                    }
                }, error => {
                    console.log(error);
                });

        }
    }
}