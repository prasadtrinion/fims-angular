import { Component, OnInit } from '@angular/core';
import { NewaccountcodeService } from './../service/newaccountcode.service';
import { environment } from "../../../environments/environment";
@Component({
    selector: 'AccountCodereportComponent',
    templateUrl: 'accountcodereport.component.html'
})

export class AccountCodereportComponent implements OnInit {
    downloadUrl: string = environment.api.downloadbase;
    constructor(

        private NewaccountcodeService: NewaccountcodeService,
     
    ) { }

    ngOnInit() {
     
    }
    downloadReport(){
        this.NewaccountcodeService.downloadReport().subscribe(
            data => {
                window.open(this.downloadUrl + data['name']); 

            }, error => {
                console.log(error);
            });
    }
}