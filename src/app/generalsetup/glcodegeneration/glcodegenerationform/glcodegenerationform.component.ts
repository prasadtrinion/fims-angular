import { Injector } from '@angular/core';
import {Component, OnInit,Directive, Input, ViewChild} from '@angular/core';import { GlcodegenerationService } from '../../../generalledger/service/glcodegeneration.service'
import { ActivatedRoute, Router } from '@angular/router';
import { FundService } from '../../../generalsetup/service/fund.service'
import { ActivitycodeService } from '../../../generalsetup/service/activitycode.service'
import { AccountcodeService } from '../../../generalsetup/service/accountcode.service'
import { DepartmentService } from '../../../generalsetup/service/department.service'
import { CommonFundComponent } from "../../../shared/commonfund.component";
@Component({
    selector: 'GlcodegenerationFormComponent',
    templateUrl: 'glcodegenerationform.component.html'
})

export class GlcodegenerationFormComponent implements OnInit {
    @ViewChild(CommonFundComponent) fund;
    glcodegenerationList = [];
    glcodegenerationData = {};
    DeptList = [];
    deptData = [];
    fundList = [];
    fundData = {};
    accountcodeList = [];
    accountcodeData = {};
    activitycodeList = [];
    activitycodeData = {};
    id: number;

    constructor(
        private GlcodegenerationService: GlcodegenerationService,
        private ActivitycodeService: ActivitycodeService,
        private AccountcodeService: AccountcodeService,
        private DepartmentService: DepartmentService,
        private FundService: FundService,
        private route: ActivatedRoute,
        private router: Router
    ) { }

    ngOnInit() {
        this.glcodegenerationData['f014fstatus'] = 1;
        // fund dropdown
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.FundService.getItems().subscribe(
            data => {
                this.fundList = data['result']['data'];
        }, error => {
            console.log(error);
        });
        // activity dropdown
        this.ActivitycodeService.getItemsByLevel().subscribe(
            data => {
                console.log(data);
                this.activitycodeList = data['result']['data'];
        }, error => {
            console.log(error);
        });
        // account dropdown
        this.AccountcodeService.getItemsByLevel().subscribe(
            data => {
                this.accountcodeList = data['result']['data'];
                for(var i=0;i<this.accountcodeList.length;i++) {
                    if(this.accountcodeList[i]['f059fid']=='1046') {
                        this.accountcodeList[i]['f059fcompleteCode'] = '00000';
                    }
                }
        }, error => {
            console.log(error);
        });

        // department dropdown
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.DepartmentService.getItems().subscribe(
            data => {
                this.DeptList = data['result']['data'];
        }, error => {
            console.log(error);
        });

        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        console.log(this.id);

        if (this.id > 0) {
            this.GlcodegenerationService.getItemsDetail(this.id).subscribe(
                data => {
                    this.glcodegenerationData = data['result']['data'][0];
                    if(data['result'].f014fstatus===true) {
                    console.log(data);
                    this.glcodegenerationData['f014fstatus'] = 1;
                } else {
                    this.glcodegenerationData['f014fstatus'] = 0;

                }
                console.log(this.glcodegenerationData);
                }, error => {
                    console.log(error);
                });
        }


        console.log(this.id);
    }

    addGlcodegeneration() {
        this.glcodegenerationData['f014ffundId'] = this.fund.idFund;

        console.log(this.glcodegenerationData);
    //        this.glcodegenerationData['f014fupdatedBy']=1;
    //        this.glcodegenerationData['f014fcreatedBy']=1;
    //        this.glcodegenerationData['f014fstatus'] = 1;


    //        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
    //     if(this.id>0) {
    //         this.GlcodegenerationService.updateGlcodegenerationItems(this.glcodegenerationData,this.id).subscribe(
    //             data => {
    //                 this.router.navigate(['generalsetup/glcodegeneration']);
    //         }, error => {
    //             console.log(error);
    //         });

       
    // } else {
    //     this.GlcodegenerationService.insertGlcodegenerationItems(this.glcodegenerationData).subscribe(
    //         data => {
    //             this.router.navigate(['generalsetup/glcodegeneration']);

    //     }, error => {
    //         console.log(error);
    //     });

    // }

}
}