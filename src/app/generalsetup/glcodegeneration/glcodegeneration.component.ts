import { Component, OnInit , ViewChild, ElementRef} from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { GlcodegenerationService } from '../../generalledger/service/glcodegeneration.service'

declare var $;
@Component({
    selector: 'Glcodegeneration',
    templateUrl: 'glcodegeneration.component.html'
})

export class GlcodegenerationComponent implements OnInit {
    @ViewChild('dataTable') table: ElementRef;
    glcodegenerationList =  [];
    glcodegenerationData = {};
    id: number;
    dataTable:any;

    constructor(
        private GlcodegenerationService: GlcodegenerationService

    ) { }

    ngOnInit() { 
        this.dataTable = $(this.table.nativeElement);
        this.dataTable.DataTable();
        this.GlcodegenerationService.getItems().subscribe(
            data => {
                this.glcodegenerationList = data['result']['data'];
        }, error => {
            console.log(error);
        });
    }

    addGlcodegeneration(){
           console.log(this.glcodegenerationData);
        this.GlcodegenerationService.insertGlcodegenerationItems(this.glcodegenerationData).subscribe(
            data => {
                this.glcodegenerationList = data['todos'];
            }, error => {
                console.log(error);
            });

    }
}