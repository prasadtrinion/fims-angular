import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { BankService } from '../../service/bank.service'
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
    selector: 'BankFormComponent',
    templateUrl: 'bankform.component.html'
})
export class BankFormComponent implements OnInit {
    bankform: FormGroup;
    bankList = [];
    bankData = {};
    id: number;
    constructor(
        private BankService: BankService,
        private route: ActivatedRoute,
        private router: Router
    ) { }

    ngOnInit() {
        this.bankData['f042fstatus'] = 1;
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.bankform = new FormGroup({
            bank: new FormControl('', Validators.required)
        });
        
        console.log(this.id);

        if (this.id > 0) {
            this.BankService.getItemsDetail(this.id).subscribe(
                data => {
                    this.bankData = data['result'];
                    console.log(this.bankData);
                }, error => {
                    console.log(error);
                });
        }
        console.log(this.id);
    }
    addBank() {
        console.log(this.bankData);
        //    this.bankData['f042fupdatedBy']=1;
        //    this.bankData['f042fcreatedBy']=1;
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }

        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.BankService.updateBankItems(this.bankData, this.id).subscribe(
                data => {
                    
                        if(data['status'] == 409){
                            alert('Bank Code Already Exist');
                        }
                        else{
                            this.router.navigate(['generalsetup/bank']);
                            alert("Bank has been Updated Successfully")
                        }
                   
                }, error => {
                    alert('Bank Code Already Exist');
                    return false;
                    // console.log(error);
                });
        } else {
            this.BankService.insertBankItems(this.bankData).subscribe(
                data => {
                    if(data['status'] == 409){
                        alert('Bank Code Already Exist');
                    }
                    else{
                        this.router.navigate(['generalsetup/bank']);
                        alert("Bank has been Added Successfully")

                    }
                   

                }, error => {
                    console.log(error);
                });

        }

    }
}