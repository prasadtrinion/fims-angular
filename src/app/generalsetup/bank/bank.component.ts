import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { BankService } from '../service/bank.service'
import { faSearch, faEdit } from '@fortawesome/free-solid-svg-icons';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'Bank',
    templateUrl: 'bank.component.html'
})
export class BankComponent implements OnInit {
    bankList =  [];
    bankData = {};
    id: number;
    faSearch = faSearch;
    faEdit = faEdit;
    constructor(
        private BankService: BankService,
        private spinner: NgxSpinnerService
    ) { }
    ngOnInit() { 
        this.spinner.show();
        this.BankService.getItems().subscribe(
            data => {
                this.spinner.hide();
                this.bankList = data['result']['data'];
        }, error => {
            console.log(error);
        });
    }
    addBank(){
           console.log(this.bankData);
        this.BankService.insertBankItems(this.bankData).subscribe(
            data => { 
                this.bankList = data['todos'];
            }, error => {
                console.log(error);
            });

    }
}