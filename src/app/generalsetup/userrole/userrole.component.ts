import {Injector} from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { UserroleService } from '../service/userrole.service';

@Component({
    selector: 'Userrole',
    templateUrl: 'userrole.component.html'
})

export class UserroleComponent implements OnInit {
    uroleList = [];
    uroleData = {};
    id : number;

    constructor(
        private UserroleService: UserroleService
    ) { }

    ngOnInit() { 
        this.UserroleService.getItems().subscribe(
            data => {
                this.uroleList = data['result']['data'];
        }, error => {
            console.log(error);
        });
    }
}
