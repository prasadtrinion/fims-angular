import {Injector} from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { UserroleService } from '../../service/userrole.service';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'UserroleFormComponent',
    templateUrl: 'userroleform.component.html'
})

export class UserroleFormComponent implements OnInit {
    uroleList = [];
    uroleData = {};
    id: number;

    constructor(
        
        private userroleService: UserroleService,
        private route: ActivatedRoute,
    ) { }

    ngOnInit() { 
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        console.log(this.id);
        
        if(this.id>0) {
            this.userroleService.getItemsDetail(this.id).subscribe(
                data => {
                    console.log(data);
                    this.uroleList = data['result']['data'];


    
            }, error => {
                console.log(error);
            });
        }
        

        console.log(this.id);
    }
    addUserrole(){
           console.log(this.uroleData);
           if(this.id>0) {

           } else {
        this.userroleService.insertuserroleItems(this.uroleData).subscribe(
                    data => {
                        this.uroleList = data['todos'];
                }, error => {
                    console.log(error);
                });
           }
       

    }
    goToListPage(){
        
    }
}