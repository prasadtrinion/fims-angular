import { Component, OnInit } from '@angular/core';
import { LayoutService } from "../layout.service";
@Component({
    selector: 'budget-layout',
    templateUrl: 'budgetlayout.component.html',
    styleUrls: ['./budgetlayout.component.css']

})

export class BudgetlayoutComponent implements OnInit {
    setupList = [];
    transactionList = [];
    reportList = [];
    token : string;
    module = 'budget';
    link: string;
    constructor(
        private layoutService:LayoutService
        ) { }

    ngOnInit() { 
        this.token = sessionStorage.getItem('f014ftoken');
        this.link = this.module +'/'+ this.token;
        this.layoutService.getMenus(this.link).subscribe(
            data => {
                //console.log(data);
                this.setupList = data['result']['setup'];
                this.transactionList = data['result']['transaction'];
                this.reportList = data['result']['report'];
            }, error => {
                console.log(error);
            });
    }
}