import { Component, OnInit } from '@angular/core';
import { LayoutService } from "../layout.service";
@Component({
    selector: 'report-layout',
    templateUrl: 'reportlayout.component.html'
})

export class ReportlayoutComponent implements OnInit {
    menuList = [];
    token : string;
    module = 'report';
    link: string;
    constructor(
        private layoutService:LayoutService
        ) { }

    ngOnInit() { 
        this.token = sessionStorage.getItem('f014ftoken');
        this.link = this.module +'/'+ this.token;
        this.layoutService.getMenus(this.link).subscribe(
            data => {
                //console.log(data);
                this.menuList = data['result']['data'];
            }, error => {
                console.log(error);
            });
    }
}