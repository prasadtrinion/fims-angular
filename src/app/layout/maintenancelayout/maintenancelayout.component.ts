import { Component, OnInit } from '@angular/core';
import { LayoutService } from "../layout.service";

@Component({
    selector: 'maintenance-layout',
    templateUrl: 'maintenancelayout.component.html',
    styleUrls: ['./maintenancelayout.component.css']
})

export class MaintenanceLayoutComponent implements OnInit {
    menuList = [];
    setupList = [];
    transactionList = [];
    reportList=[];
    token : string;
    module = 'maintainence';
    link: string;
    constructor(
        private layoutService:LayoutService,

        ) { }

    ngOnInit() { 
        this.token = sessionStorage.getItem('f014ftoken');
        this.link = this.module +'/'+ this.token;
        this.layoutService.getMenus(this.link).subscribe(
            data => {
                //console.log(data);
                this.menuList = data['result']['data'];
                this.setupList = data['result']['setup'];
                this.transactionList = data['result']['transaction'];
                this.reportList = data['result']['report'];
            }, error => {
                console.log(error);
            });
    }
}