import { Component, OnInit, ViewEncapsulation, Renderer } from '@angular/core';
import { PanelMenuModule } from 'primeng/panelmenu';
import { MenuModule } from 'primeng/menu';
import { LayoutService } from "../layout.service";

@Component({
  selector: 'app-voucher-layout',
  templateUrl: 'gllayout.component.html',
  styleUrls: ['gllayout.component.css']
})

export class GllayoutComponent implements OnInit {
  constructor(private render: Renderer, private layoutService: LayoutService) { }
  menuList = [];
  setupList = [];
  transactionList = [];
  reportList = [];
  token: string;
  module = 'gl';
  link: string;
  list = [];
  Menuitems = [];
  status: boolean = false;
  selected;
  id;
  addClass(id: any) {
    this.id = id;
  }


  ngOnInit() {
    this.token = sessionStorage.getItem('f014ftoken');
    this.link = this.module + '/' + this.token;
    this.layoutService.getMenus(this.link).subscribe(
      data => {
        //console.log(data);
        if (sessionStorage.getItem('StudentId') == null) {
          this.menuList = data['result']['data'];
          this.setupList = data['result']['setup'];
          this.transactionList = data['result']['transaction'];
          this.reportList = data['result']['report'];
        }
        else {
          this.menuList.push({
            f011flink: "/student/returns",
            f011fmenuName: 'Returns'
          });
        }
      }, error => {
        console.log(error);
      });
    // its just list data from here down
    this.Menuitems = [
      {
        label: 'Parent 1',
        items: [
          { label: 'Parent 11', url: '#' },
          { label: 'Parent 12', url: '#' }
        ]
      },
      {
        label: 'Parent 2',
        items: [
          { label: 'Parent 21', url: '#' },
          { label: 'Parent 22', url: '#' },
          { label: 'Parent 23', url: '#' }
        ]
      },
      {
        label: 'Parent 3',
        items: [
          { label: 'Parent 31', url: '#' },
          { label: 'Parent 32', url: '#' },
          { label: 'Parent 33', url: '#' }
        ]
      }
    ];

    this.list = [
      {
        title: 'childless',
        children: []
      },
      {
        title: 'great grandparent',
        children: [
          {
            title: 'childless grandsibiling',
            children: []
          },
          {
            title: 'grandparent',
            children: [
              {
                title: 'childless sibiling',
                children: []
              },
              {
                title: 'another childless sibiling',
                children: []
              },
              {
                title: 'parent',
                children: [
                  {
                    title: 'child',
                    children: []
                  },
                  {
                    title: 'another child',
                    children: []
                  },
                ]
              },
              {
                title: 'another parent',
                children: [
                  {
                    title: 'child',
                    children: []
                  },
                ]
              },
            ]
          },
          {
            title: 'another grandparent',
            children: [
              {
                title: 'parent',
                children: [
                  {
                    title: 'child',
                    children: []
                  },
                  {
                    title: 'another child',
                    children: []
                  },
                  {
                    title: 'a third child',
                    children: []
                  },
                  {
                    title: 'teen mother',
                    children: [
                      {
                        title: 'accident',
                        children: []
                      },
                    ]
                  },
                ]
              },
            ]
          },
        ]
      },
    ];

  }

}


