import { Injectable } from '@angular/core';
import { HttpClientModule,HttpHeaders, HttpClient} from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({ 'X-Auth-Client': 'qazwsx123',
    'Content-Type': 'application/json' })
    
  };
  
@Injectable()

export class LayoutService {
    url: string = environment.api.base + environment.api.endPoints.rolemenu;
    url1: string = environment.api.base + environment.api.endPoints.roleModule;
    url2: string = environment.api.base + environment.api.endPoints.rolemenu1;
    
    constructor(private httpClient: HttpClient) { 
     
    }
    
    getMenus(link) {
        return this.httpClient.get(this.url+'/'+link,httpOptions);
    }
    getModules(token) {
        return this.httpClient.get(this.url1+'/'+token,httpOptions);
    }
    getMenus1(link) {
        return this.httpClient.get(this.url2+'/'+link,httpOptions);
    }
    // getItemsDetail(id) {
    //     return this.httpClient.get(this.url+'/'+id,httpOptions);
    // }

    // insertFundItems(fundData): Observable<any> {
    //     return this.httpClient.post(this.url, fundData,httpOptions);
    // }

    // updateFundItems(fundData,id): Observable<any> {
    //     return this.httpClient.put(this.url+'/'+id,fundData,httpOptions);
    //    }
}