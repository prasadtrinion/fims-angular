import { Component, OnInit } from '@angular/core';
import { LayoutService } from "../layout.service";

@Component({
    selector: 'payroll-layout',
    templateUrl: 'payrolllayout.component.html',
    styleUrls: ['./payrolllayout.component.css']
})

export class PayrolllayoutComponent implements OnInit {
    menuList = [];
    setupList = [];
    transactionList = [];
    reportList = [];
    token : string;
    module = 'payroll';
    link: string;
    constructor(private layoutService:LayoutService) { }

    ngOnInit() { 
        this.token = sessionStorage.getItem('f014ftoken');
        this.link = this.module +'/'+ this.token;
        this.layoutService.getMenus(this.link).subscribe(
            data => {
                //console.log(data);
                this.menuList = data['result']['data'];
                this.setupList = data['result']['setup'];
                this.transactionList = data['result']['transaction'];
                this.reportList = data['result']['report'];
            }, error => {
                console.log(error);
            });
    }
}