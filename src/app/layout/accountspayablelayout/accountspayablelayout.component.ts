import { Component, OnInit } from '@angular/core';
import { LayoutService } from "../layout.service";

@Component({
    selector: 'accountspayable-layout',
    templateUrl: 'accountspayablelayout.component.html',
    styleUrls: ['./accountspayablelayout.component.css']
})

export class AccountspayablelayoutComponent implements OnInit {
    setupList = [];
    transactionList = [];
    reportList = [];
    menuList = [];
    token : string;
    module = 'ap';
    link: string;
    constructor(private layoutService:LayoutService) { }

    ngOnInit() { 
        this.token = sessionStorage.getItem('f014ftoken');
        this.link = this.module +'/'+ this.token;
        this.layoutService.getMenus(this.link).subscribe(
            data => {
                //console.log(data);
                this.menuList = data['result']['data'];
                this.setupList = data['result']['setup'];
                this.transactionList = data['result']['transaction'];
                this.reportList = data['result']['report'];
            }, error => {
                console.log(error);
            });
    }
}