import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LayoutService } from "../layout.service";
import { faUserCircle } from '@fortawesome/free-solid-svg-icons';


@Component({
    selector: 'app-header',
    templateUrl: 'header.component.html',
    styleUrls: ['header.component.css']
})

export class HeaderComponent implements OnInit {
    moduleList = [];
    newmoduleList = [];
    moduleclass = [];
    moduleNames = new Array();
    moduleLinks = new Array();
    faUserCircle = faUserCircle;
    token :any;
    menus:string;
    userName;
    userId;
    constructor(private route: ActivatedRoute,
        private router: Router,private layoutService:LayoutService) {
    }

    ngOnInit() {
        // console.log(this.moduleNames);
        // console.log(this.router.url);
        this.userName = localStorage.getItem('username');
        this.userId = localStorage.getItem('staffId');
        this.moduleNames['budget'] = "Budget";
        this.moduleNames['maintainence'] = "Maintenance";
        this.moduleNames['ar'] = "AR";
        this.moduleNames['ap'] = "AP";
        this.moduleNames['sf'] = "Student Finance";
        this.moduleNames['gl'] = "General Ledger";
        this.moduleNames['investment'] = "Investment";
        this.moduleNames['report'] = "Report";
        this.moduleNames['procurement'] = "Procurement";
        this.moduleNames['payroll'] = "Payroll";
        this.moduleNames['assets'] = "Assets";
        this.moduleNames['loan'] = "Loan";
        this.moduleNames['payroll'] = "Payroll";
        this.moduleNames['cashadvances'] = "Cash Advances";


        this.moduleclass['budget'] = "";
        this.moduleclass['maintainence'] = "";
        this.moduleclass['ar'] = "";
        this.moduleclass['ap'] = "";
        this.moduleclass['sf'] = "";
        this.moduleclass['gl'] = "";
        this.moduleclass['investment'] = "";
        this.moduleclass['report'] = "";
        this.moduleclass['procurement'] = "";
        this.moduleclass['payroll'] = "";
        this.moduleclass['assets'] = "";
        this.moduleclass['loan'] = "";
        this.moduleclass['payroll'] = "";
        this.moduleclass['cashadvances'] = "";


        this.moduleLinks['budget'] = "/budget";
        this.moduleLinks['budgetstatus'] = true;
        this.moduleLinks['maintainence'] = "/generalsetup";
        this.moduleLinks['ar'] = "/accountsrecivable";
        this.moduleLinks['ap'] = "/accountspayable";
        this.moduleLinks['sf'] = "/studentfinance";
        this.moduleLinks['gl'] = "/generalledger";
        this.moduleLinks['investment'] = "/investment";
        this.moduleLinks['report'] = "/report";
        this.moduleLinks['procurement'] = "/procurement";
        this.moduleLinks['payroll'] = "/payroll";
        this.moduleLinks['assets'] = "/assets";
        this.moduleLinks['loan'] = "/loan";
        this.moduleLinks['payroll'] = "/payroll";
        this.moduleLinks['cashadvances'] = "/cashadvances";
        
       

        switch(this.router.url) {
            case '/assets' : this.menus = "assets";
            this.moduleclass['assets'] = "active";

            break;
            case '/loan' : this.menus = "loan";
            this.moduleclass['loan'] = "active";

            break;            
            case '/generalsetup' : this.menus = "maintainence";
            this.moduleclass['maintainence'] = "active";

                                   break;
                                   case '/budget' : this.menus = "budget";
                                   this.moduleclass['budget'] = "active";
                                   break;
                                   case '/accountsrecivable' : this.menus = "ar";
                                   this.moduleclass['ar'] = "active";

                                   break;
                                   case '/accountspayable' : this.menus = "ap";
                                   this.moduleclass['ap'] = "active";

                                   break;
                                   case '/studentfinance' : this.menus = "sf";
                                   this.moduleclass['sf'] = "active";

                                   break;
                                   case '/generalledger' : this.menus = "gl";
                                   this.moduleclass['gl'] = "active";

                                   break;
                                   case '/investment' : this.menus = "investment";
                                   this.moduleclass['investment'] = "active";

                                   break;
                                   case '/procurement' : this.menus = "procurement";
                                   this.moduleclass['procurement'] = "active";

                                   break;
                                   case '/payroll' : this.menus = "payroll";
                                   this.moduleclass['payroll'] = "active";

                                   break;    
                                   case '/assets' : this.menus = "assets";
                                   this.moduleclass['assets'] = "active";

                                   break;   
                                   case '/cashadvances' : this.menus = "cashadvances";
                                   this.moduleclass['cashadvances'] = "active";

                                   break;                                                               
        }

        // console.log(this.menus);
        this.token = sessionStorage.getItem('f014ftoken');
        
        this.layoutService.getModules(this.token).subscribe(
            data => {
                
                this.moduleList = data['result']['data'];
                
                // console.log(this.moduleList);
                // console.log(this.moduleLinks);
                // console.log(this.moduleNames);

                switch(this.router.url) {
                    case '/assets' : this.moduleLinks['class'] = 'Active';
                    break;
                    case '/loan' : this.menus = "loan";
                    break;            
                    case '/generalsetup' : this.menus = "maintainence";
                                           break;
                                           case '/budget' : this.menus = "budget";
                                           break;
                                           case '/accountsrecivable' : this.menus = "ar";
                                           break;
                                           case '/accountspayable' : this.menus = "ap";
                                           break;
                                           case '/studentfinance' : this.menus = "sf";
                                           break;
                                           case '/generalledger' : this.menus = "gl";
                                           break;
                                           case '/investment' : this.menus = "investment";
                                           break;
                                           case '/procurement' : this.menus = "procurement";
                                           break;
                                           case '/payroll' : this.menus = "payroll";
                                           break;    
                                           case '/assets' : this.menus = "assets";
                                           break;   
                                           case '/cashadvances' : this.menus = "cashadvances";
                                           break;                                                               
                }

             
                this.newmoduleList = data['result']['data'];
                // console.log(this.moduleList);
            }, error => {
                console.log(error);
            });
            
     }
     sutrim (asdf) {
         return asdf.trim();
     }
    logout(){
        sessionStorage.clear();
        this.router.navigateByUrl('/login');

    }
}