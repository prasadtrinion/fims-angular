import { Component, OnInit } from '@angular/core';
import { LayoutService } from "../layout.service";

@Component({
    selector: 'vendor-layout',
    templateUrl: 'vendorlayout.component.html'
})

export class VendorlayoutComponent implements OnInit {
    menuList = [];
    token : string;
    link: string;
    expire = true;
    expireDays;
    constructor(private layoutService:LayoutService) { }

    ngOnInit() { 
        if(parseInt(sessionStorage.getItem('expireDays'))<14){
            this.expire = false;
        this.expireDays = parseInt(sessionStorage.getItem('expireDays'));
console.log(this.expireDays);
            
            }
        
    }
}