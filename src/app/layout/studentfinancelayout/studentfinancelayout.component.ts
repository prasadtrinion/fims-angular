import { Component, OnInit } from '@angular/core';
import { LayoutService } from "../layout.service";

@Component({
    selector: 'studentfinance-layout',
    templateUrl: 'studentfinancelayout.component.html',
    styleUrls: ['./studentfinancelayout.component.css']

})

export class StudentfinancelayoutComponent implements OnInit {
    menuList = [];
    setupList = [];
    transactionList = [];
    token : string;
    module = 'sf';
    link: string;
    constructor(
        private layoutService:LayoutService,

        ) { }

    ngOnInit() { 
        this.token = sessionStorage.getItem('f014ftoken');
        this.link = this.module +'/'+ this.token;
        this.layoutService.getMenus(this.link).subscribe(
            data => {
                //console.log(data);
                this.menuList = data['result']['data'];
                this.setupList = data['result']['setup'];
                this.transactionList = data['result']['transaction'];
            }, error => {
                console.log(error);
            });
    }
}