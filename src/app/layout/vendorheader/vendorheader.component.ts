import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LayoutService } from "../layout.service";
import { faUserCircle } from '@fortawesome/free-solid-svg-icons';


@Component({
    selector: 'vendor-header',
    templateUrl: 'vendorheader.component.html',
    styleUrls: ['vendorheader.component.css']
})

export class VendorHeaderComponent implements OnInit {
    faUserCircle = faUserCircle;
    token :any;
    expireDays;
    expire;
    enddate;
    constructor(private route: ActivatedRoute,
        private router: Router,private layoutService:LayoutService) {
    }

    ngOnInit() {
        if(parseInt(sessionStorage.getItem('expireDays'))<14){
        this.expire = true;
        this.expireDays = parseInt(sessionStorage.getItem('expireDays'));
        this.enddate = sessionStorage.getItem('endDate');

        }
            
     }
    logout(){
        sessionStorage.clear();
        this.router.navigateByUrl('/vendorlogin');

    }
}