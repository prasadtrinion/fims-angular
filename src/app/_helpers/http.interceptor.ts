import { Injectable } from '@angular/core';  
import {  
    Http,  
    ConnectionBackend,  
    RequestOptions,  
    RequestOptionsArgs,  
    Response  
} from '@angular/http';  
import { Observable } from 'rxjs';  
  
@Injectable()  
export class HttpService extends Http {  
    constructor(backend: ConnectionBackend, defaultOptions: RequestOptions) {  
        let _token = sessionStorage.getItem('auth_token'); // get the custom token from the local storage  
        defaultOptions.headers.set('Authorization', 'Bearer ${_token}');  
        super(backend, defaultOptions);  
    }  
  
    get(url: string, optionsArgs?: RequestOptionsArgs): Observable<Response> {  
        console.log("Interceptor: ", url, optionsArgs);  
//let ¬_token = sessionStorage.getItem('auth_token');  
//if (typeof url === "string") { // this means we have to add the token to the options not in url  
//if (!optionsArgs) {  
// let's make option object  
//optionsArgs = {headers: new Headers()};  
//}  
//optionsArgs.headers.set('Authorization', `Bearer ${_token}`);  
//}   
//else {  
// we have to add the token to the url object  
//url.headers.set('Authorization', `Bearer ${_token}`);  
//}  
        return super.get(url, optionsArgs);  
    }  
    private catchException (self: HttpService) {  
    return (resp: Response) => {  
      console.log(resp);  
      if (resp.status === 401 || resp.status === 403) {  
        // user not authenticated  
        console.log(resp);  
      }  
      return Observable.throw(resp);  
    };  
  }  
}  