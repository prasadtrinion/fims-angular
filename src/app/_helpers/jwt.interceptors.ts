import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { NgxSpinnerService } from 'ngx-spinner';
import 'rxjs/add/observable/throw';
import { map } from 'rxjs/operators';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
    constructor(
        private spinner: NgxSpinnerService,
    ) { }
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        request = request.clone({
            setHeaders: { 
                'X-Auth-Token': '\''+this.getToken()+'\''
            }
        });
return next.handle(request).pipe(map(event => {
    if (event instanceof HttpResponse || event instanceof HttpErrorResponse) {
        this.spinner.hide();
    }
    else {
        this.spinner.show();
    }
    return event;
}));
        // return next.handle(request );
        
    }
    getToken(){
        if(sessionStorage.length!=0){
        return sessionStorage.getItem('f014ftoken');
        }
        else{
            return "NO";
        }
    }
    

}