import { Injector } from '@angular/core';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AssetpreregistrationService } from '../../service/assetpreregistration.service'

import { AccountcodeService } from '../../../generalsetup/service/accountcode.service'
import { AssettypeService } from '../../service/assettype.service'
import { AssetsService } from '../../service/assets.service'
import { SubcategoryService } from "../../service/subcategory.service";
import { TypeService } from "../../service/type.service";
import { AssetItemService } from "../../service/assetitem.service";
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { GrnService } from "../../../procurement/service/grn.service";
import { PurchaserequistionentryService } from '../../../procurement/service/purchaserequistionentry.service';
import { ItemsetupService } from '../../../procurement/service/itemsetup.service';
import { DepartmentService } from '../../../generalsetup/service/department.service'
import { PurchaseorderService } from '../../../procurement/service/purchaseorder.service';

@Component({
    selector: 'AssetpreregistrationFormComponent',
    templateUrl: 'assetpreregistrationform.component.html'
})

export class AssetpreregistrationFormComponent implements OnInit {
    assetpreregform: FormGroup;
    assetpreregList = [];
    assetpreregData = {};
    assetpreregDataheader = {};
    assetssList = [];
    DeptList = [];
    deptData = {};
    assettypeList = [];
    accountcodeList = [];
    subcategorylistList = [];
    grnListFullList = [];
    assetitemList = [];
    ajaxCount: number;
    id: number;
    grnList = [];
    itemList = [];
    subcategoryList = [];
    qtn: number;
    finalAssetPreReg = [];
    grnitemList = [];

    constructor(
        private AssetpreregistrationService: AssetpreregistrationService,
        private AssetsService: AssetsService,
        private SubcategoryService: SubcategoryService,
        private TypeService: TypeService,
        private AccountcodeService: AccountcodeService,
        private AssetItemService: AssetItemService,
        private route: ActivatedRoute,
        private router: Router,
        private GrnService: GrnService,
        private ItemsetupService: ItemsetupService,
        private PurchaserequistionentryService: PurchaserequistionentryService,
        private DepartmentService: DepartmentService,
        private PurchaseorderService: PurchaseorderService

    ) { }

    ngDoCheck() {

        const change = this.ajaxCount;
        // console.log(this.ajaxCount);
        if (this.ajaxCount == 0) {
            //this.editFunction();
            this.ajaxCount = 10;
        }
    }

    getItemDetails() {
        let substring = '';
        this.GrnService.getGrnDetails(this.assetpreregDataheader['f092fidGrn']).subscribe(
            data => {
                console.log(data);
                this.grnitemList = data['result'];
                // for(var i=0;i<this.grnitemList.length;i++) {
                //     var subcategoryName = this.grnitemList[i]['subCategoryName'];
                //     substring = "asset";
                //     if(subcategoryName.includes(substring)) {
                //         this.grnitemList[i]['typeofRedirect'] = 'Asset';
                //     }
                //     substring = "stock";
                //     if(subcategoryName.includes(substring)) {
                //         this.grnitemList[i]['typeofRedirect'] = 'Stock';
                //     }
                //     console.log(this.grnitemList);

                // }
            }
        );
      

    }



    ngOnInit() {

        this.ajaxCount = 0;
        //  SubcategoryService

        this.GrnService.getGrnItems().subscribe(
            data => {

                this.grnListFullList = data['result'];
                for (var i = 0; i < this.grnListFullList.length; i++) {
                    if (this.grnListFullList[i]['f034fapprovalStatus'] == '1') {
                        this.grnList.push(this.grnListFullList[i]);
                    }
                }
                this.grnList = this.grnListFullList;
                console.log(this.grnList);
            }, error => {
                console.log(error);
            });


        // this.DepartmentService.getItems().subscribe(
        //     data => {
        //         this.ajaxCount--;
        //         this.DeptList = data['result']['data'];
        //     }, error => {
        //         console.log(error);
        //     });

        // this.ajaxCount++;
        // this.SubcategoryService.getItems().subscribe(
        //     data => {
        //         this.ajaxCount--;
        //         this.subcategorylistList = data['result']['data'];
        //     }, error => {
        //         console.log(error);
        //     });
        // // assets category
        // this.AssetItemService.getItems().subscribe(
        //     data => {
        //         this.itemList = data['result']['data'];
        //     }, error => {
        //         console.log(error);
        //     });

        //  this.SubcategoryService.getItems().subscribe(
        //     data => {
        //         this.subcategoryList = data['result']['data'];
        //     }, error => {
        //         console.log(error);
        //     });
        // this.ajaxCount++;
        // this.AssetsService.getItems().subscribe(
        //     data => {
        //         this.ajaxCount--;
        //         this.assetssList = data['result']['data'];
        //     }, error => {
        //         console.log(error);
        //     });
        // console.log(this.id);
        // // Assettype 
        // this.ajaxCount++;
        // this.TypeService.getItems().subscribe(
        //     data => {
        //         this.ajaxCount--;
        //         this.assettypeList = data['result']['data'];
        //     }, error => {
        //         console.log(error);
        //     });

        // this.AccountcodeService.getItemsByLevel().subscribe(
        //     data => {
        //         this.ajaxCount--;
        //         this.accountcodeList = data['result']['data'];
        //     }, error => {
        //         console.log(error);
        //     });


        // //Assettype 
        // this.ajaxCount++;
        // this.AssetItemService.getItems().subscribe(
        //     data => {
        //         this.ajaxCount--;
        //         this.assetitemList = data['result']['data'];
        //     }, error => {
        //         console.log(error);
        //     });


        // this.ItemsetupService.getItems().subscribe(
        //     data => {
        //         this.ajaxCount--;
        //         console.log(this.ajaxCount);

        //         this.itemList = data['result']['data'];
        //         console.log(this.itemList);
        //     }
        // );



    }
    saveAssetpreregistration() {
        console.log(this.finalAssetPreReg);
        if(this.assetpreregDataheader['f092fidGrn']==undefined){
            alert("Select GRN");
            return false;
        }
        for(var i=0; i< this.finalAssetPreReg.length;i++){
        if(this.finalAssetPreReg[i]['f092fidCategory']==undefined){
            alert("Select Asset Category");
            return false;
        }
        if(this.finalAssetPreReg[i]['f092fidSubCategory']==undefined){
            alert("Select Asset Sub Category");
            return false;
        }
        if(this.finalAssetPreReg[i]['f092fidAssetType']==undefined){
            alert("Select Asset Type");
            return false;
        }
        if(this.finalAssetPreReg[i]['f092fidAssetItem']==undefined){
            alert("Select Asset Item");
            return false;
        }
    }
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        let newFinalGrn = [];
        for(var i=0;i<this.finalAssetPreReg.length;i++) {
            let grnobject = {}
            grnobject['f085fstatus'] = 1;
            grnobject['f085fitemName'] = this.finalAssetPreReg[i]['itemName'];
            grnobject['f085fidItem'] = this.finalAssetPreReg[i]['f092fidAssetItem'];
            grnobject['f085freferrer'] = 0;
            grnobject['f085fsoCode'] = this.finalAssetPreReg[i]['f034fsoCode'];
            grnobject['f085fgrnNumber'] = this.assetpreregDataheader['f092fidGrn'];
            grnobject['f085fpoNumber'] = this.finalAssetPreReg[i]['f034freferenceNumber']
            grnobject['f085fsubCategory'] = this.finalAssetPreReg[i]['f092fidSubCategory'];
            grnobject['f085fidAssetType'] = this.finalAssetPreReg[i]['f092fidAssetType'];
            grnobject['f085fidAssetCategory'] = this.finalAssetPreReg[i]['f092fidCategory'];
            grnobject['f085fidDepartment'] = this.finalAssetPreReg[i]['f034fidDepartment'];
            grnobject['f085faccountCode'] = this.finalAssetPreReg[i]['f034faccountCode'];
            newFinalGrn.push(grnobject);
        }
        console.log(newFinalGrn);

        // let savedObj = {};
        // savedObj['data'] = this.assetpreregList;
        // console.log(savedObj);

        this.PurchaserequistionentryService.preAssetInformation(newFinalGrn).subscribe(
            data => {
                console.log(data);
                alert("Asset Pre-Registration has been saved successfully");
                this.router.navigate(['assets/updateasset']);

            }
        );

    }


    deleteassetprereg(object) {
        var cnf = confirm("Do you really want to delete");
        if (cnf == true) {
            var index = this.assetpreregList.indexOf(object);
            if (index > -1) {
                this.assetpreregList.splice(index, 1);
            }
        }
    }

    addassetpreregistrationlist() {
        console.log(this.assetpreregData);
        var dataofCurrentRow = this.assetpreregData;
        this.assetpreregData = {};
        this.assetpreregList.push(dataofCurrentRow);
        console.log(this.assetpreregList);

    }


}


