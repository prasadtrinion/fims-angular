import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AssetpreregistrationService } from '../service/assetpreregistration.service'

@Component({
    selector: 'Assetpreregistration',
    templateUrl: 'assetpreregistration.component.html'
})
export class AssetpreregistrationComponent implements OnInit {
    assetpreregList = [];
    assetpreregData = {};
    id: number;
    constructor(
        private AssetpreregistrationService: AssetpreregistrationService
    ) { }

    ngOnInit() {

        this.AssetpreregistrationService.getItems().subscribe(
            data => {
                this.assetpreregList = data['data'];
            }, error => {
                console.log(error);
            });
    }
    addAssetpreregistration() {
        console.log(this.assetpreregData);
        this.AssetpreregistrationService.insertAssetpreregistrationItems(this.assetpreregData).subscribe(
            data => {
                this.assetpreregList = data['todos'];
            }, error => {
                console.log(error);
            });

    }
}