import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { StockDistrubutionService } from "../service/stockdistrubution.service";
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'StockDistrubutionComponent',
    templateUrl: 'stockdistrubution.component.html'
})
export class StockDistrubutionComponent implements OnInit {
    stockdistrubutionList = [];
    stockdistrubutionData = {};
    id: number;
    constructor(
        private StockDistrubutionService: StockDistrubutionService,
        private spinner: NgxSpinnerService
    ) { }

    ngOnInit() {
        this.spinner.show();
        this.StockDistrubutionService.getItems().subscribe(
            data => {
                this.spinner.hide();
                let applystockData = [];
                applystockData = data['result'];
                for (var i = 0; i < applystockData.length; i++) {
                    if (applystockData[i]['f099fapprovalStatus'] == 1) {
                        applystockData[i]['f099fapprovalStatus'] = 'Approved';
                    } else if (applystockData[i]['f099fapprovalStatus'] == 2) {
                        applystockData[i]['f099fapprovalStatus'] = 'Rejected';
                    } else {
                        applystockData[i]['f099fapprovalStatus'] = 'Pending';
                    }
                }
                this.stockdistrubutionList = applystockData;
            }, error => {
                console.log(error);
            });
    }
}