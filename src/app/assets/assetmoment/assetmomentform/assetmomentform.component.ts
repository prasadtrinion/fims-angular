import { Injector } from '@angular/core';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AssetmomentService } from '../../service/assetmoment.service'
import { DepartmentService } from '../../../generalsetup/service/department.service'
import { CustomerService } from '../../../accountsrecivable/service/customer.service';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { AlertService } from './../../../_services/alert.service';
import { AssetinformationService } from '../../service/assetinformation.service'
import { StaffService } from "../../../loan/service/staff.service";
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'AssetmomentFormComponent',
    templateUrl: 'assetmomentform.component.html'
})

export class AssetmomentFormComponent implements OnInit {
    assetmomentform: FormGroup;
    assetmomentList = [];
    assetmomentData = {};
    DeptList = [];
    recevierList = [];
    ftype = [];
    id: number;
    idview: number;
    viewDisabled: boolean;
    editDisabled: boolean;
    ajaxCount: number;
    assetinformationList = [];
    constructor(

        private AssetmomentService: AssetmomentService,
        private DepartmentService: DepartmentService,
        private route: ActivatedRoute,
        private AlertService: AlertService,
        private router: Router,
        private CustomerService: CustomerService,
        private AssetinformationService: AssetinformationService,
        private StaffService: StaffService,
        private spinner: NgxSpinnerService,

    ) { }

    ngDoCheck() {
        const change = this.ajaxCount;
        if (this.ajaxCount == 0) {
            this.spinner.hide();
        }
    }
    ngOnInit() {
        this.ajaxCount = 0
        this.spinner.show();
        this.viewDisabled = false;
        this.editDisabled = false;
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.route.paramMap.subscribe((data) => this.idview = + data.get('idview'));

        let stafftypeobj = {};
        stafftypeobj['id'] = 1;
        stafftypeobj['name'] = 'Transfer';
        this.ftype.push(stafftypeobj);
        stafftypeobj = {};
        stafftypeobj['id'] = 2;
        stafftypeobj['name'] = 'Loan';
        this.ftype.push(stafftypeobj);

        this.assetmomentData['f087fidType'];
        this.assetmomentData['f087fidReceiver'];
        this.assetmomentData['f087fstatus'] = 1;


        this.assetmomentform = new FormGroup({
            assetsmoment: new FormControl('', Validators.required),

        });
        this.ajaxCount++;
        this.StaffService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.recevierList = data['result']['data'];
            }, error => {
                console.log(error);
            });
        //  Department 
        this.ajaxCount++;
        this.DepartmentService.getActiveItems().subscribe(
            data => {
                this.ajaxCount--;
                this.DeptList = data['result']['data'];
            }, error => {
                console.log(error);
            });
        this.ajaxCount++;
        this.AssetinformationService.getAssetCodeItems().subscribe(
            data => {
                this.ajaxCount--;
                this.assetinformationList = data['result'];
            }, error => {
                console.log(error);
            });

        if (this.id > 0) {
            this.ajaxCount++;
            this.AssetmomentService.getItemsDetail(this.id).subscribe(
                data => {
                    this.ajaxCount--;
                    this.AssetinformationService.getAllAssetCodeItems().subscribe(
                        data => {
                            this.ajaxCount--;
                            this.assetinformationList = data['data']['data'];
                        }, error => {
                            console.log(error);
                        });
                    this.assetmomentData = data['result']['data'][0];
                    this.assetmomentData['f087fstatus'] = parseInt(this.assetmomentData['f087fstatus']);
                    if (data['result']['data'][0]['f087fapproval1Status'] == 1 && data['result']['data'][0]['f087fapproval2Status'] == 1) {
                        this.viewDisabled = true;
                        setTimeout(
                            function () {
                                $("#target input,select").prop("disabled", true);
                                $("#target1 input,select").prop("disabled", true);
                            }, 2000);

                    }
                    else if (data['result']['data'][0]['f087fapproval1Status'] == 2 && data['result']['data'][0]['f087fapproval2Status'] == 2) {
                        this.viewDisabled = true;
                        setTimeout(
                            function () {
                                $("#target input,select").prop("disabled", true);
                                $("#target1 input,select").prop("disabled", true);
                            }, 2000);

                    }
                    else if (data['result']['data'][0]['f087fapproval1Status'] == 1 && data['result']['data'][0]['f087fapproval2Status'] == 2) {
                        this.viewDisabled = true;
                        setTimeout(
                            function () {
                                $("#target input,select").prop("disabled", true);
                                $("#target1 input,select").prop("disabled", true);
                            }, 2000);

                    }
                    else if (data['result']['data'][0]['f087fapproval1Status'] == 2 && data['result']['data'][0]['f087fapproval2Status'] == 0) {
                        this.viewDisabled = true;
                        setTimeout(
                            function () {
                                $("#target input,select").prop("disabled", true);
                                $("#target1 input,select").prop("disabled", true);
                            }, 2000);

                    }

                    if (this.idview == 2) {
                        this.viewDisabled = true;

                        $("#target input,select").prop("disabled", true);
                        $("#target1 input,select").prop("disabled", true);
                    }
                    this.editDisabled = true;
                }, error => {
                    console.log(error);
                });
        }
    }

    dateComparison() {

        let startDate = Date.parse(this.assetmomentData['f087fstartDate']);
        let endDate = Date.parse(this.assetmomentData['f087ffromDate']);
        if (startDate > endDate) {
            alert("End Date cannot be Less than Start Date");
            this.assetmomentData['f087ffromDate'] = "";
        }

    }

    addAssetmoment() {

        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.AssetmomentService.updateAssetmomentItems(this.assetmomentData, this.id).subscribe(
                data => {
                    this.router.navigate(['assets/assetmoment']);
                    alert("Movement has been Updated Successfully !!");

                }, error => {
                    console.log(error);
                });

        } else {

            this.AssetmomentService.insertAssetmomentItems(this.assetmomentData).subscribe(
                data => {
                    this.router.navigate(['assets/assetmoment']);
                    alert("Movement has been Added Successfully !!");


                }, error => {
                    console.log(error);
                });

        }
    }

}