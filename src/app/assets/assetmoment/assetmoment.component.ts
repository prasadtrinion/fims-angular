import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AssetmomentService } from '../service/assetmoment.service'
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'Assetmoment',
    templateUrl: 'assetmoment.component.html'
})
export class AssetmomentComponent implements OnInit {
    assetmomentList = [];
    assetmomentData = {};
    id: number;
    constructor(
        private AssetmomentService: AssetmomentService,
        private spinner: NgxSpinnerService
    ) { }

    ngOnInit() {
        this.spinner.show();
        this.AssetmomentService.getItems().subscribe(
            data => {
                this.spinner.hide();
                let activityData = [];
                activityData = data['result']['data'];
                console.log("asdf");
                for (var i = 0; i < activityData.length; i++) {
                    if (activityData[i]['f087fapproval1Status'] == 0) {
                        activityData[i]['f087fapproval1Status'] = 'Pending';
                    }
                    if (activityData[i]['f087fapproval2Status'] == 0) {
                        activityData[i]['f087fapproval2Status'] = 'Pending';
                    }
                    if (activityData[i]['f087fapproval1Status'] == 1) {
                        activityData[i]['f087fapproval1Status'] = 'Approved';
                    }
                    if (activityData[i]['f087fapproval2Status'] == 1) {
                        activityData[i]['f087fapproval2Status'] = 'Approved';
                    }
                    if (activityData[i]['f087fapproval1Status'] == 2) {
                        activityData[i]['f087fapproval1Status'] = 'Rejected';
                        activityData[i]['f087fapproval2Status'] = '';
                    }
                    if (activityData[i]['f087fapproval2Status'] == 2) {
                        activityData[i]['f087fapproval2Status'] = 'Rejected';
                    }

                }
                this.assetmomentList = activityData;
            }, error => {
                console.log(error);
            });
    }
}