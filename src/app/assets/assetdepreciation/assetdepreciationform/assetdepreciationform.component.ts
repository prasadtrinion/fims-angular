import { Injector } from '@angular/core';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AssetdepreciationService } from '../../service/assetdepreciation.service'
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { AlertService } from './../../../_services/alert.service';
import { AssetinformationService } from '../../service/assetinformation.service'

@Component({
    selector: 'AssetdepreciationFormComponent',
    templateUrl: 'assetdepreciationform.component.html'
})

export class AssetdepreciationFormComponent implements OnInit {
    assetdepreciationform: FormGroup;
    depreciationList = [];
    depreciationData = {};
    id: number;
    numberList = [];
    assetinformationList = [];
    constructor(

        private AssetdepreciationService: AssetdepreciationService,
        private AlertService: AlertService,
        private route: ActivatedRoute,
        private router: Router,
        private AssetinformationService:AssetinformationService

    ) { }
    ngOnInit() {

        this.depreciationData['f088fday'];
        this.depreciationData['f088fmonth'];

        this.AssetinformationService.getItems().subscribe(
            data => {
                this.assetinformationList = data['data']['data'];
            }, error => {
                console.log(error);
            });

        for (var i = 1; i < 30; i++) {
            let number = {};
            number['key'] = i;
            number['value'] = i;
            this.numberList.push(number);
        }
        this.assetdepreciationform = new FormGroup({
            utility: new FormControl('', Validators.required),

        });
        this.depreciationData['f088fstatus'] = 1;
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        if (this.id > 0) {
            this.AssetdepreciationService.getItemsDetail(this.id).subscribe(
                data => {
                    this.depreciationData = data['result'][0];
                    this.depreciationData['f088fstatus'] = parseInt(this.depreciationData['f088fstatus']);

                }, error => {
                    console.log(error);
                });
        }
        console.log(this.id);
    }
    addAssetdepreciation() {

        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.AssetdepreciationService.updateAssetdepreciationItems(this.depreciationData, this.id).subscribe(
                data => {
                    if (data['status'] == 409) {
                    }
                    else {
                        this.router.navigate(['assets/assetdepreciation']);
                        this.AlertService.success("Updated Sucessfully !!")
                    }
                }, error => {
                    console.log(error);
                });
        } else {
            this.AssetdepreciationService.insertAssetdepreciationItems(this.depreciationData).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert('Asset Number Type Already Exist');
                    }
                    else {
                        this.router.navigate(['assets/assetdepreciation']);
                        this.AlertService.success("Added Sucessfully !!")
                    }
                }, error => {
                    console.log(error);
                });
        }
    }

}