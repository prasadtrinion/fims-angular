import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AssetdepreciationService } from '../service/assetdepreciation.service'

@Component({
    selector: 'Assetdepreciation',
    templateUrl: 'assetdepreciation.component.html'
})
export class AssetdepreciationComponent implements OnInit {
    depriciationList = [];
    depriciationData = {};
    id: number;
    constructor(
        private AssetdepreciationService: AssetdepreciationService
    ) { }

    ngOnInit() {

        this.AssetdepreciationService.getItems().subscribe(
            data => {
                this.depriciationList = data['result']['data'];
                for(var i=0;i<this.depriciationList.length;i++) {
                    this.depriciationList[i]['f088fmonth'] = this.getName(this.depriciationList[i]['f088fmonth']);
                }
            }, error => {
                console.log(error);
            });
    }

    getName (name) {
        var text = '';
        switch(name) {
            case '1' : text =  "January";break;
            case '2' : text =  "February";break;
            case '3' : text = "March";break;
            case '4' : text =  "April";break;
            case '5' : text =  "May";break;
            case '6' : text = "June";break;
            case '7' : text =  "July";break;
            case '8' : text =  "August";break;
            case '9' : text = "September";break;
            case '10' : text =  "October";break;
            case '11' : text =  "November";break;
            case '12' : text = "December";break;                                    
        }
        return text;
    }

    addAssetdepreciation() {
        console.log(this.depriciationData);
        this.AssetdepreciationService.insertAssetdepreciationItems(this.depriciationData).subscribe(
            data => {
                this.depriciationList = data['todos'];
            }, error => {
                console.log(error);
            });

    }
}