import { Injector } from '@angular/core';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AssetinformationService } from '../../service/assetinformation.service'
import { AssetsService } from '../../service/assets.service'
import { DepartmentService } from '../../../generalsetup/service/department.service'
import { AccountcodeService } from '../../../generalsetup/service/accountcode.service'
import { SubcategoryService } from "../../service/subcategory.service";
import { AssetItemService } from "../../service/assetitem.service";
import { TempassetinformationService } from "../../service/tempassetinformation.service";
import { StaffService } from "../../../loan/service/staff.service";

import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { TypeService } from "../../service/type.service";

@Component({
    selector: 'AssetregistrationFormComponent',
    templateUrl: 'assetregistrationform.component.html'
})

export class AssetregistrationFormComponent implements OnInit {
    assetinformationform: FormGroup;
    assetinformationList = [];
    assetinformationData = {};
    assetinformationDataheader = {};
    assetssList = [];
    DeptList = [];
    deptData = {};
    assettypeList = [];
    accountcodeList = [];
    categoryList = []
    ajaxCount: number;
    id: number;
    typeList = [];
    campusList = [];
    blockList = [];
    buildingList = [];
    roomList = [];
    subcategoryList = [];
    itemList = [];
    smartStaffList = [];
    constructor(
        private AssetinformationService: AssetinformationService,
        private AssetsService: AssetsService,
        private TempassetinformationService: TempassetinformationService,
        private AccountcodeService: AccountcodeService,
        private DepartmentService: DepartmentService,
        private route: ActivatedRoute,
        private router: Router,
        private StaffService:StaffService,
        private TypeService: TypeService,
        private SubcategoryService: SubcategoryService,
        private AssetItemService: AssetItemService

    ) { }

    ngOnInit() {
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        
        this.ajaxCount++;
        this.AssetinformationService.getlocationItems(0).subscribe(
            data => {
                this.ajaxCount--;
                this.campusList = data['result']['data'];
            }, error => {
                console.log(error);
            });

        this.ajaxCount = 0;
        //  Cost Center
        this.ajaxCount++;
        this.DepartmentService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.DeptList = data['result']['data'];
            }, error => {
                console.log(error);
            });

        this.ajaxCount++;
        this.AssetinformationService.getlocationItems(2).subscribe(
            data => {
                this.ajaxCount--;
                this.blockList = data['result']['data'];
            }, error => {
                console.log(error);
            });

            this.AssetinformationService.getlocationItems(1).subscribe(
                data => {
                    this.ajaxCount--;
                    this.buildingList = data['result']['data'];
                }, error => {
                    console.log(error);
                });
    
            this.ajaxCount = 0;
         
            this.ajaxCount++;
            this.AssetinformationService.getlocationItems(3).subscribe(
                data => {
                    this.ajaxCount--;
                    this.roomList = data['result']['data'];
                }, error => {
                    console.log(error);
                });
                this.StaffService.getItems().subscribe(
                    data => {
                        this.smartStaffList = data['result']['data'];
                    })

        if (this.id > 0) {
            this.TempassetinformationService.getItemsForAssetRegistration(this.id).subscribe(
                data => {
                    console.log(data);
                    this.assetinformationData = data['result'][0];
                    this.assetinformationData['f133fassetOwner'] = parseInt(this.assetinformationData['f133fassetOwner'])
                    console.log(this.assetinformationData);
                }, error => {
                    console.log(error);
                });
        }
        console.log(this.id);

    }

    saveAssetinformation() {
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        let updatePreRegistration = {};
        let updatedId = [];
                updatedId.push(this.id);
           
        updatePreRegistration['status']=1;
        updatePreRegistration['id']=updatedId;

        this.TempassetinformationService.updatePreRegistration(updatePreRegistration).subscribe(
            data => {
                this.router.navigate(['/assets/tempregistration/0']);

            }
        );
    }

}