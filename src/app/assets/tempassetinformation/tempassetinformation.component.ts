import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { TempassetinformationService } from '../service/tempassetinformation.service'
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'Tempassetinformation',
    templateUrl: 'tempassetinformation.component.html'
})
export class TempassetinformationComponent implements OnInit {
    assetinformationList = [];
    preRegisteredDetails = [];
    assetinformationData = {};
    elementType: 'url' | 'canvas' | 'img' = 'url';
    value: string = 'Techiediaries';
    id: number;
    constructor(
        private TempassetinformationService: TempassetinformationService,
        private spinner: NgxSpinnerService,
        private router: Router,
        private route: ActivatedRoute,

    ) { }

    ngOnInit() {
       this.getDetails();
    }
    getDetails(){
        this.spinner.show();
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        this.TempassetinformationService.getPreregistrationItems(this.id).subscribe(
            data => {
                this.spinner.hide();
                console.log(data);
                this.preRegisteredDetails = data['result'];
            }
        );
    }
    saveAssetinformation(){
        console.log(this.preRegisteredDetails);
        let updatePreRegistration = {};
        let updatedId = [];
        for(var i=0;i<this.preRegisteredDetails.length;i++) {
            if(this.preRegisteredDetails[i]['selectedAsset']==true) {
                updatedId.push(this.preRegisteredDetails[i]['f133fid']);
            }
        }
        updatePreRegistration['status']=1;
        updatePreRegistration['id']=updatedId;

        this.TempassetinformationService.updatePreRegistration(updatePreRegistration).subscribe(
            data => {
                this.spinner.hide();
                console.log(data);
                this.getDetails();

            }
        );

    }
}