import { Injector } from '@angular/core';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AssetinformationService } from '../../service/assetinformation.service'
import { AssetsService } from '../../service/assets.service'
import { DepartmentService } from '../../../generalsetup/service/department.service'
import { AccountcodeService } from '../../../generalsetup/service/accountcode.service'
import { TempassetinformationService } from "../../service/tempassetinformation.service";
import { StaffService } from "../../../loan/service/staff.service";

import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';

@Component({
    selector: 'TempassetinformationFormComponent',
    templateUrl: 'tempassetinformationform.component.html'
})

export class TempassetinformationFormComponent implements OnInit {
    assetinformationform: FormGroup;
    grnInformation = [];
    ajaxCount: number;
    id: number;
    preregistrationList = [];
    categoryName:string;
    subCategoryName:string;
    itemName:string;
    numberofTime = [];
    depreciationList = [];
    smartStaffList = [];
    assetNameCheckbox = false;
    assetorderNumber = false;
    buildingList = [];
    campusList = [];
    blockList = [];
    roomList = [];
    assetacqusitionDate = false;
    assetexpiryDate = false;
    assetdepreciation = false;
    assetnetValue = false;
    assetmonthlyDepreciation = false;
    assetinstallationCost = false;
    assetbrandName = false;
    assetassetOwner = false;
    assetcampus = false;
    assetblock = false;
    assetbuilding = false;
    assetroom = false;
    constructor(
        private AssetinformationService: AssetinformationService,
        private AssetsService: AssetsService,
        private DepartmentService: DepartmentService,
        private TempassetinformationService: TempassetinformationService,
        private route: ActivatedRoute,
        private StaffService:StaffService,
        private router: Router,

    ) { }
   
    getDepreciatedValue(row) {
        console.log(row);

        let depreciationElement = this.preregistrationList[row]['f133fdepreicationCode'];
        for(var i=0;i<this.preregistrationList.length;i++) {
            this.preregistrationList[i]['f133fdepreicationCode'] =this.preregistrationList[0]['f133fdepreicationCode'];
        }
        this.AssetinformationService.getDepreciationValue(depreciationElement).subscribe(
            data => {
                console.log(data);

                var multipleFactor = parseInt(data['result']['f015flifeSpan']) * 12;
                var depreciationCostPerMonth = ((this.preregistrationList[row]['f133finstallCost']) * (data['result']['f015fdepreciationRate']/100) )/ multipleFactor; 
                var date = new Date(this.preregistrationList[row]['f133fAcquisitionDate']);
                var newdate = new Date(date);
                newdate.setDate(newdate.getDate());
                var dd = newdate.getDate();
                console.log(dd);
                var mm = newdate.getMonth();
                console.log(mm);
                var y = newdate.getFullYear() + parseInt(data['result']['f015flifeSpan']);
                console.log(y);
                this.preregistrationList[row]['f133fexpireDate'] = new Date(y+'-'+mm+'-'+dd).toISOString().substr(0, 10);
                this.preregistrationList[row]['f133fyearDeprCost'] = depreciationCostPerMonth.toFixed(2);
            })

    }
    assignName(){
        console.log(this.assetNameCheckbox);
        if(this.assetNameCheckbox==true){
            for(var i=0;i<this.preregistrationList.length;i++) {
                this.preregistrationList[i]['f133fassetDescription'] =this.preregistrationList[0]['f133fassetDescription'];
            }
        }
    }
    orderNumber(){
        console.log(this.assetorderNumber);
        if(this.assetorderNumber==true){
            for(var i=0;i<this.preregistrationList.length;i++) {
                this.preregistrationList[i]['f133fserialNumber'] =this.preregistrationList[0]['f133fserialNumber'];
            }
        }
    }
    acqusitionDate(){
        if(this.assetacqusitionDate==true){
            for(var i=0;i<this.preregistrationList.length;i++) {
                this.preregistrationList[i]['f133fAcquisitionDate'] =this.preregistrationList[0]['f133fAcquisitionDate'];
            }
        }
    }
    expiryDate(){
        if(this.assetexpiryDate==true){
            for(var i=0;i<this.preregistrationList.length;i++) {
                this.preregistrationList[i]['f133fexpireDate'] =this.preregistrationList[0]['f133fexpireDate'];
            }
        }
    }

    netValue(){
        if(this.assetnetValue==true){
            for(var i=0;i<this.preregistrationList.length;i++) {
                this.preregistrationList[i]['f133fnetValue'] =this.preregistrationList[0]['f133fnetValue'];
            }
        }
    }

    brandName() {
        if(this.assetbrandName==true){
            for(var i=0;i<this.preregistrationList.length;i++) {
                this.preregistrationList[i]['f133fbrandName'] =this.preregistrationList[0]['f133fbrandName'];
            }
        }
    }

    assetOwner() {
        if(this.assetassetOwner==true){
            for(var i=0;i<this.preregistrationList.length;i++) {
                this.preregistrationList[i]['f133fassetOwner'] =this.preregistrationList[0]['f133fassetOwner'];
            }
        }
    }

    campus() {
        if(this.assetcampus==true){
            for(var i=0;i<this.preregistrationList.length;i++) {
                this.preregistrationList[i]['f133fcurrentCampus'] =this.preregistrationList[0]['f133fcurrentCampus'];
            }
        }
    }

    block() {
        if(this.assetblock==true){
            for(var i=0;i<this.preregistrationList.length;i++) {
                this.preregistrationList[i]['f133fcurrentBlock'] =this.preregistrationList[0]['f133fcurrentBlock'];
            }
        }
    }

    building() {
        if(this.assetbuilding==true){
            for(var i=0;i<this.preregistrationList.length;i++) {
                this.preregistrationList[i]['f133fcurrentBuilding'] =this.preregistrationList[0]['f133fcurrentBuilding'];
            }
        }
    }

    room() {
        if(this.assetroom==true){
            for(var i=0;i<this.preregistrationList.length;i++) {
                this.preregistrationList[i]['f133fcurrentRoom'] =this.preregistrationList[0]['f133fcurrentRoom'];
            }
        }
    }


    depreciation(){
        if(this.assetdepreciation==true){
            for(var i=0;i<this.preregistrationList.length;i++) {
                this.preregistrationList[i]['f133fdepreicationCode'] =this.preregistrationList[0]['f133fdepreicationCode'];
            }
        }
    }
    monthlyDepreciation(){
        if(this.assetmonthlyDepreciation==true){
            for(var i=0;i<this.preregistrationList.length;i++) {
                this.preregistrationList[i]['f133fyearDeprCost'] =this.preregistrationList[0]['f133fyearDeprCost'];
            }
        }
    }
    installationCost(){
        if(this.assetinstallationCost==true){
            for(var i=0;i<this.preregistrationList.length;i++) {
                this.preregistrationList[i]['f133finstallCost'] =this.preregistrationList[0]['f133finstallCost'];
            }
        }
    }
    ngOnInit() {
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.ajaxCount = 0;
        //  Cost Center 
       this.getDetails();
       this.TempassetinformationService.getDepreciationCode().subscribe(
        data => {
            this.depreciationList = data['result']['data'];
        })
        this.StaffService.getAllItems().subscribe(
            data => {
                this.smartStaffList = data['result']['data'];
            });
            this.ajaxCount++;
            this.AssetinformationService.getlocationItems(0).subscribe(
                data => {
                    this.ajaxCount--;
                    this.campusList = data['result']['data'];
                }, error => {
                    console.log(error);
                });
    
            this.ajaxCount = 0;
            //  Cost Center 
            this.ajaxCount++;
            this.AssetinformationService.getlocationItems(2).subscribe(
                data => {
                    this.ajaxCount--;
                    this.blockList = data['result']['data'];
                }, error => {
                    console.log(error);
                });
    
                this.AssetinformationService.getlocationItems(1).subscribe(
                    data => {
                        this.ajaxCount--;
                        this.buildingList = data['result']['data'];
                    }, error => {
                        console.log(error);
                    });
        
                this.ajaxCount = 0;
                //  Cost Center 
                this.ajaxCount++;
                this.AssetinformationService.getlocationItems(3).subscribe(
                    data => {
                        this.ajaxCount--;
                        this.roomList = data['result']['data'];
                    }, error => {
                        console.log(error);
                    });
    }

    getDetails(){
        this.ajaxCount++;

          this.TempassetinformationService.getItems(this.id).subscribe(
            data => {
                this.ajaxCount--;
                console.log(this.grnInformation);
                this.grnInformation = data['result'][0];
                this.categoryName = this.grnInformation['categoryName'];
                this.subCategoryName = this.grnInformation['subCategoryName'];
                this.itemName = this.grnInformation['itemName'];
                this.numberofTime = Array(this.grnInformation['f034fquantityReceived']).fill(0).map((x,i)=>i);
                console.log(this.preregistrationList);
                for(var i=0;i<this.numberofTime.length;i++) {
                    let newpreregisteredObject = {};
                    newpreregisteredObject['categoryName'] = this.categoryName;
                    newpreregisteredObject['subCategoryName'] = this.subCategoryName;
                    newpreregisteredObject['itemName'] = this.itemName;
                    newpreregisteredObject['f133fcategoryType'] = this.grnInformation['f034fitemType'];
                    newpreregisteredObject['f133fserialNumber'] = '';
                    newpreregisteredObject['f133fassetDescription'] = '';
                    newpreregisteredObject['f133forderNumber'] = this.grnInformation['poReference'];

                    newpreregisteredObject['f133fAcquisitionDate'] =  this.grnInformation['f034fcreatedDtTm'];
                    newpreregisteredObject['f133fexpireDate'] = '';
                    newpreregisteredObject['f133fyearDeprCost'] = '';
                    newpreregisteredObject['f133fassetOwner'] = '';
                    newpreregisteredObject['f133finstallCost'] = this.grnInformation['f034fprice'];
                    newpreregisteredObject['f133fnetValue'] = this.grnInformation['f034fprice'];
                    newpreregisteredObject['f133fbrandName'] = '';
                    newpreregisteredObject['f133faccountCode'] = this.grnInformation['f034faccountCode'];
                    newpreregisteredObject['f133fassetOwner'] = '';

                    newpreregisteredObject['f133fsubCategory'] = data['result'][0]['f034fitemCategory'];
                    newpreregisteredObject['f133fidAssetCategory'] = data['result'][0]['f034fitemSubCategory'];
                    newpreregisteredObject['f133fidItem'] = data['result'][0]['f034fidItem'];
                    newpreregisteredObject['f133fidGrnDetail'] = this.id;
                    newpreregisteredObject['f133fidDepartment'] = data['result'][0]['f034fidDepartment'];
                    this.preregistrationList.push(newpreregisteredObject);
                }
                console.log(this.preregistrationList);
            }, error => {
                console.log(error);
            });

    }

    assetDescription(){
        for(var i=0;i<this.preregistrationList.length;i++) {
            this.preregistrationList[i]['f133fassetDescription'] =this.preregistrationList[0]['f133fassetDescription'];
        }
    }
  

    addAssetinformation() {
        let assetInformationObject = {};
        assetInformationObject['data'] = this.preregistrationList;
        console.log(assetInformationObject);
        this.TempassetinformationService.postAssetTempInformation(assetInformationObject).subscribe(
            data => {
                 alert("Asset Pre Information has been Saved");
                 this.router.navigate(['/assets/tempregistration/0']);
            }
        )


    }

}