import { Injector } from '@angular/core';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AssetsService } from '../../service/assets.service'
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { AccountcodeService } from '../../../generalsetup/service/accountcode.service';
import { DepreciationsetupService } from '../../service/depreciationsetup.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'DepreciationSetupFormComponent',
    templateUrl: 'depreciationsetupform.component.html'
})

export class DepreciationSetupFormComponent implements OnInit {
    assetsform: FormGroup;
    depreciationData = {};
    depreciationList = [];
    accountcodeList = [];
    id: number;
    ajaxCount: number;
    selectedItem = [];
    constructor(

        private DepreciationsetupService: DepreciationsetupService,
        private spinner: NgxSpinnerService,
        private AccountcodeService: AccountcodeService,
        private route: ActivatedRoute,
        private router: Router

    ) { }

    ngDoCheck() {
        const change = this.ajaxCount;
        if (this.ajaxCount == 0) {
            this.editFunction();
            this.spinner.hide();
            this.ajaxCount = 10;
        }
    }
    editFunction() {
        this.ajaxCount++;
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        if (this.id > 0) {
            this.DepreciationsetupService.getItemsDetail(this.id).subscribe(
                data => {
                    this.ajaxCount--;
                    this.depreciationData = data['result'];
                    this.depreciationData['f015fstatus'] = parseInt(this.depreciationData['f015fstatus']);
                }, error => {
                    console.log(error);
                });
        }

    }

    ngOnInit() {
        this.ajaxCount = 0
        this.spinner.show();
        this.assetsform = new FormGroup({
            utility: new FormControl('', Validators.required),

        });
        this.depreciationData['f015fstatus'] = 1;

        // Account Code Dropdown 
        this.AccountcodeService.getItemsByLevel().subscribe(
            data => {
                this.spinner.hide();
                this.accountcodeList = data['result']['data'];
            }, error => {
                console.log(error);
            });
    }
    addDepreciationSetup() {
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.DepreciationsetupService.updatedepreciationsetupItems(this.depreciationData, this.id).subscribe(
                data => {
                    // if (data['status'] == 409) {
                    //     alert('Asset Category Code Already Exist');
                    // }
                    // else {
                    this.router.navigate(['assets/depreciationsetup']);
                    alert("Depreciation Setup has been Updated Successfully")

                    // }

                }, error => {
                    // alert('Asset Category Code Already Exist');
                    return false;
                });

        } else {

            this.DepreciationsetupService.inseretdepreciationsetupItems(this.depreciationData).subscribe(
                data => {
                    // if (data['status'] == 409) {
                    //     alert('Asset Category Code Already Exist');
                    // }
                    // else {
                    this.router.navigate(['assets/depreciationsetup']);
                    alert("Depreciation Setup has been Added Successfully")
                    // }

                }, error => {
                    console.log(error);
                });
        }
    }
    onlyNumberKey(event) {
        if (event.charCode == 46) {

        } else {
            return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
        }
    }
}