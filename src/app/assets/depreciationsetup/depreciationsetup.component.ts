import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { DepreciationsetupService } from '../service/depreciationsetup.service';
import { AlertService } from './../../_services/alert.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
    selector: 'depreciationsetup',
    templateUrl: 'depreciationsetup.component.html'
})

export class DepreciationsetupComponent implements OnInit {

    momentapproval2List = [];
    momentapproval2Data = {};
    selectAllCheckbox = true;
    accountcodeList = [];
    depreciation = {}
    depreciationList = [];
    deleteList = [];
    id: number;
    showLastRow: boolean;
    showLastRowMinus: boolean;
    showLastRowPlus: boolean;
    listshowLastRowMinus: boolean;
    listshowLastRowPlus: boolean;
    constructor(

        private DepreciationsetupService: DepreciationsetupService,
        private spinner: NgxSpinnerService,
        private router: Router,
        private route: ActivatedRoute,
    ) { }

    ngOnInit() {
        this.spinner.show();

        this.DepreciationsetupService.getItems().subscribe(
            data => {
                console.log(data);
                this.spinner.hide();
                this.depreciationList = data['result']['data'];
            }, error => {
                console.log(error);
            });
    }
}
