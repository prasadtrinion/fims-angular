import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AssetsService } from '../service/assets.service'
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'Assets',
    templateUrl: 'assets.component.html'
})
export class AssetsComponent implements OnInit {
    assetsList = [];
    assetsData = {};
    id: number;
    constructor(
        private AssetsService: AssetsService,
        private spinner: NgxSpinnerService
    ) { }

    ngOnInit() {
        this.spinner.show();

        this.AssetsService.getItems().subscribe(
            data => {
                this.spinner.hide();
                this.assetsList = data['result']['data'];
            }, error => {
                console.log(error);
            });
    }
}