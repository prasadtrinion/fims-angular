import { Injector } from '@angular/core';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AssetsService } from '../../service/assets.service'
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { ItemsubcategoryService } from "../../../procurement/service/itemsubcategory.service";
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'AssetsFormComponent',
    templateUrl: 'assetsform.component.html'
})

export class AssetsFormComponent implements OnInit {
    assetsform: FormGroup;
    assetsList = [];
    assetsData = {};
    itemgroupList = [];
    id: number;
    ajaxCount: number;
    selectedItem = [];
    constructor(

        private AssetsService: AssetsService,
        private spinner: NgxSpinnerService,
        private ItemsubcategoryService: ItemsubcategoryService,
        private route: ActivatedRoute,
        private router: Router

    ) { }

    ngDoCheck() {
        const change = this.ajaxCount;
        if (this.ajaxCount == 0) {
            this.editFunction();
            this.spinner.hide();
            this.ajaxCount = 10;
        }
    }
    editFunction() {
        this.ajaxCount++;
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        if (this.id > 0) {
            this.AssetsService.getItemsDetail(this.id).subscribe(
                data => {
        this.ajaxCount--;
                    this.assetsData = data['result'][0];
                    this.assetsData['f087fstatus'] = parseInt(this.assetsData['f087fstatus']);
                }, error => {
                    console.log(error);
                });
        }

    }

    ngOnInit() {
        this.ajaxCount = 0
        this.spinner.show();
        this.assetsform = new FormGroup({
            utility: new FormControl('', Validators.required),

        });
        this.assetsData['f087fstatus'] = 1;

        // Item Group Dropdown 
        this.ajaxCount++;
        this.ItemsubcategoryService.getActiveItems().subscribe(
            data => {
                this.ajaxCount--;
                this.itemgroupList = data['result']['data'];
            }, error => {
                console.log(error);
            });
    }



    addAssets() {
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        // this.assetsData['f087fidItemGroup'] = this.assetsData['f087fidItemGroup']['f028fid'];
        if(this.assetsData['f087fpercentage'] > 100){
            alert("Percentage should not be greater than 100");
            return false;
        }
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.AssetsService.updateAssetsItems(this.assetsData, this.id).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert('Asset Category Code Already Exist');
                    }
                    else {
                        this.router.navigate(['assets/assets']);
                        alert("Asset Category has been Updated Successfully")

                    }

                }, error => {
                    alert('Asset Category Code Already Exist');
                    return false;
                });

        } else {

            this.AssetsService.insertAssetsItems(this.assetsData).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert('Asset Category Code Already Exist');
                    }
                    else {
                        this.router.navigate(['assets/assets']);
                        alert("Asset Category has been Added Successfully")
                    }

                }, error => {
                    console.log(error);
                });
        }
    }

}