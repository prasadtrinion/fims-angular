import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { StoreService } from '../service/store.service'

@Component({
    selector: 'Store',
    templateUrl: 'store.component.html'
})
export class StoreComponent implements OnInit {
    storeList = [];
    storeData = {};
    id: number;
    constructor(
        private StoreService: StoreService
    ) { }

    ngOnInit() {

        this.StoreService.getItems().subscribe(
            data => {
                this.storeList = data['result']['data'];
            }, error => {
                console.log(error);
            });
    }
}