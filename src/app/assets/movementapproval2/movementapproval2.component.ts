import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { Momentapproval2Service } from '../service/momentapproval2.service'
import { AlertService } from './../../_services/alert.service';
import { NgxSpinnerService } from 'ngx-spinner';
@Component({
    selector: 'movementapproval2',
    templateUrl: 'movementapproval2.component.html'
})

export class Momentapproval2Component implements OnInit {

    momentapproval2List = [];
    momentapproval2Data = {};
    rejectData = {};
    selectAllCheckbox = true;

    constructor(

        private Momentapproval2Service: Momentapproval2Service,
        private spinner: NgxSpinnerService,
    ) { }

    ngOnInit() {
        this.getListData();
    }
    getListData() {
        this.spinner.show();
        this.Momentapproval2Service.getItemsDetail().subscribe(
            data => {
                this.spinner.hide();
                this.momentapproval2List = data['result'];
            }, error => {
                console.log(error);
            });
    }

    momentapproval2ListData() {
        console.log(this.momentapproval2List);
        var approvaloneIds = [];
        for (var i = 0; i < this.momentapproval2List.length; i++) {
            if (this.momentapproval2List[i].f087fapprovedStatus == true) {
                approvaloneIds.push(this.momentapproval2List[i].f087fid);
            }
        }
        if (approvaloneIds.length < 1) {
            alert("select atleast one type to Approve");
            return false;
        }
        var confirmPop = confirm("Do you want to Approve?");
        if (confirmPop == false) {
            return false;
        }
        var approvaloneUpdate = {};
        approvaloneUpdate['id'] = approvaloneIds;
        approvaloneUpdate['reason'] = '';
        approvaloneUpdate['status'] = 1;
        this.Momentapproval2Service.updateMomentapproval2Items(approvaloneUpdate).subscribe(
            data => {
                alert("Approved Successfully")
                this.rejectData['reason'] = '';
                this.getListData();
            }, error => {
                console.log(error);
            });
    }
    rejectmomentapprovalListData() {
        console.log(this.momentapproval2List);
        var approvaloneIds = [];
        for (var i = 0; i < this.momentapproval2List.length; i++) {
            if (this.momentapproval2List[i].f087fapprovedStatus == true) {
                approvaloneIds.push(this.momentapproval2List[i].f087fid);
            }
        }
        if (approvaloneIds.length < 1) {
            alert(" select atleast one type to Reject");
            return false;
        }
        if (this.rejectData['reason'] == undefined) {
            alert(' Enter the Description');
            return false;
        }
        var confirmPop = confirm("Do you want to Reject?");
        if (confirmPop == false) {
            return false;
        }
        var rejectoneUpdate = {};
        rejectoneUpdate['id'] = approvaloneIds;
        rejectoneUpdate['status'] = 2;
        rejectoneUpdate['reason'] = this.rejectData['reason'];
        this.Momentapproval2Service.updateMomentapproval2Items(rejectoneUpdate).subscribe(
            data => {
                this.getListData();
                this.rejectData['reason'] = '';
                alert("Rejected Successfully");

            }, error => {
                console.log(error);
            });

    }
    selectAll() {
        console.log("asdf");
        console.log(this.selectAllCheckbox);
        for (var i = 0; i < this.momentapproval2List.length; i++) {
            this.momentapproval2List[i].f087fapprovedStatus = this.selectAllCheckbox;
        }
        console.log(this.momentapproval2List);
    }

}
