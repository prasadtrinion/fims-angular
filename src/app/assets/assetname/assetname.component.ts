import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AssetnameService } from '../service/assetname.service'

@Component({
    selector: 'Assetname',
    templateUrl: 'assetname.component.html'
})
export class AssetnameComponent implements OnInit {
    assetnameList = [];
    assetnameData = {};
    id: number;
    constructor(
        private AssetnameService: AssetnameService
    ) { }

    ngOnInit() {

        this.AssetnameService.getItems().subscribe(
            data => {
                this.assetnameList = data['result']['data'];
            }, error => {
                console.log(error);
            });
    }
    addAssetname() {
        console.log(this.assetnameData);
        this.AssetnameService.insertAssetnameItems(this.assetnameData).subscribe(
            data => {
                this.assetnameList = data['todos'];
            }, error => {
                console.log(error);
            });

    }
}