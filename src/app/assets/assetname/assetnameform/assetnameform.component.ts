import { Injector } from '@angular/core';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AssetnameService } from '../../service/assetname.service'
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { AlertService } from './../../../_services/alert.service';

@Component({
    selector: 'AssetnameFormComponent',
    templateUrl: 'assetnameform.component.html'
})

export class AssetnameFormComponent implements OnInit {
    assetnameform: FormGroup;
    assetnameList = [];
    assetnameData = {};
    id: number;
    constructor(

        private AssetnameService: AssetnameService,
        private route: ActivatedRoute,
        private AlertService: AlertService,
        private router: Router

    ) { }

    ngOnInit() {
        this.assetnameform = new FormGroup({
            utility: new FormControl('', Validators.required),

        });
        this.assetnameData['f087fstatus'] = 1;
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        if (this.id > 0) {
            this.AssetnameService.getItemsDetail(this.id).subscribe(
                data => {
                    this.assetnameData = data['result'];
                    this.assetnameData['f087fstatus'] = parseInt(this.assetnameData['f087fstatus']);
                    console.log(this.assetnameData);
                }, error => {
                    console.log(error);
                });
        }


        console.log(this.id);
    }



    addAssetname() {

        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.AssetnameService.updateAssetnameItems(this.assetnameData, this.id).subscribe(
                data => {
                    if (data['status'] == 409) {
                        // alert('Asset Description Already Exist');
                    }
                    else {
                        this.router.navigate(['assets/assetname']);
                        this.AlertService.success("Updated Sucessfully !!")
                    }

                }, error => {
                    console.log(error);
                });

        } else {

            this.AssetnameService.insertAssetnameItems(this.assetnameData).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert('Asset Description Already Exist');
                    }
                    else {
                        this.router.navigate(['assets/assetname']);
                        this.AlertService.success("Added Sucessfully !!")
                    }
                }, error => {
                    console.log(error);
                });

        }
    }

}