import { Injectable } from '@angular/core';
import { HttpClientModule,HttpHeaders, HttpClient} from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({ 'X-Auth-Client': 'qazwsx123',
    'Content-Type': 'application/json' })
    
  };
@Injectable()

export class DisposalapprovalService {

     url: string = environment.api.base + environment.api.endPoints.disposalallVerifiedDetails;
     approvalurl: string = environment.api.base + environment.api.endPoints.getEndorsmentList;
     approvalPuturl: string = environment.api.base + environment.api.endPoints.disposalApproval;
     approveoneurl: string = environment.api.base + environment.api.endPoints.sellrejectOne
    constructor(private httpClient: HttpClient) {    
    }
    getItems() {
        return this.httpClient.get(this.url,httpOptions);
    }   
    getItemsDetail() {
        return this.httpClient.get(this.url, httpOptions);
    }
    getApproveDetail() {
        return this.httpClient.get(this.approvalurl, httpOptions);
    }
    insertDisposalapprovalItems(disposalapprovalData): Observable<any> {
        return this.httpClient.post(this.url, disposalapprovalData,httpOptions);
    }
    updateDisposalapprovalItems(disposalapprovalData): Observable<any> {
        return this.httpClient.post(this.approvalPuturl, disposalapprovalData,httpOptions);
    }
    rejectDisposalapproval(disposalapprovalData): Observable<any> {
        return this.httpClient.post(this.approveoneurl, disposalapprovalData,httpOptions);
    }
}