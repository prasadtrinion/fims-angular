import { Injectable } from '@angular/core';
import { HttpClientModule,HttpHeaders, HttpClient} from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({ 'X-Auth-Client': 'qazwsx123',
    'Content-Type': 'application/json' })
    
  };
@Injectable()

export class MomentapprovalService {


     url: string = environment.api.base + environment.api.endPoints.assetMovementApproval
     approvalPuturl: string = environment.api.base + environment.api.endPoints.putApproval
     approveoneurl: string = environment.api.base + environment.api.endPoints.sellrejectOne
    constructor(private httpClient: HttpClient) {    
    }
    getItems() {
        return this.httpClient.get(this.url,httpOptions);
    }   
    getItemsDetail(id) {
        return this.httpClient.get(this.url+'/'+id, httpOptions);
    }
    insertMomentapprovalItems(momentapprovalData): Observable<any> {
        return this.httpClient.post(this.url, momentapprovalData,httpOptions);
    }
    updateMomentapprovalItems(momentapprovalData): Observable<any> {
        return this.httpClient.post(this.approvalPuturl, momentapprovalData,httpOptions);
    }
    rejectMomentapproval(momentapprovalData): Observable<any> {
        return this.httpClient.post(this.approveoneurl, momentapprovalData,httpOptions);
    }
}