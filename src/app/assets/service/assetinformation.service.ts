import { Injectable } from '@angular/core';
import { HttpClientModule,HttpHeaders, HttpClient} from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({ 'X-Auth-Client': 'qazwsx123',
    'Content-Type': 'application/json' })
    
  };
  
@Injectable()

export class AssetinformationService {
    url: string = environment.api.base + environment.api.endPoints.assetInformation;
    movementAssetcodeurl: string = environment.api.base + environment.api.endPoints.assetNotInMovement;
    allAssetcodeurl: string = environment.api.base + environment.api.endPoints.getAssetInformation;
    maintenanceAssetcodeurl: string = environment.api.base + environment.api.endPoints.assetNotInMaintenance;
    requistionAssetcodeurl: string = environment.api.base + environment.api.endPoints.assetNotInDisposalRequistion;
    locationurl: string = environment.api.base + environment.api.endPoints.assetLevelList;
    depreciationurl: string = environment.api.base + environment.api.endPoints.depreciationSetup;

    constructor(private httpClient: HttpClient) { 
        
    }

    getDepreciationValue(row) {
        return this.httpClient.get(this.depreciationurl+'/'+row,httpOptions);
    }
    
    getItems() {
        return this.httpClient.get(this.url,httpOptions);
    }
    getMaintenanceAssetCodeItems() {
        return this.httpClient.get(this.maintenanceAssetcodeurl,httpOptions);
    }
    getAllAssetCodeItems() {
        return this.httpClient.get(this.allAssetcodeurl,httpOptions);
    }
    getRequistionAssetCodeItems() {
        return this.httpClient.get(this.requistionAssetcodeurl,httpOptions);
    }
    getAssetCodeItems() {
        return this.httpClient.get(this.movementAssetcodeurl,httpOptions);
    }
    getlocationItems(id) {
        return this.httpClient.get(this.locationurl+'/'+id,httpOptions);
    }

    getItemsDetail(id) {
        return this.httpClient.get(this.url+'/'+id,httpOptions);
    }

    insertAssetinformationItems(assetinformationData): Observable<any> {
        return this.httpClient.post(this.url, assetinformationData,httpOptions);
    }

    updateAssetinformationItems(assetinformationData,id): Observable<any> {
        return this.httpClient.put(this.url+'/'+id, assetinformationData,httpOptions);
       }
}