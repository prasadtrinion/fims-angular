import { Injectable } from '@angular/core';
import { HttpClientModule, HttpHeaders, HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({
        'X-Auth-Client': 'qazwsx123',
        'Content-Type': 'application/json'
    })

};

@Injectable()

export class SubcategoryService {
    url: string = environment.api.base + environment.api.endPoints.subacetgory;
    ActiveListurl: string = environment.api.base + environment.api.endPoints.activeAssetSubCategory;
    getUrl : string = environment.api.base + environment.api.endPoints.generateNumber;
    BasedonCategoryUrl : string = environment.api.base + environment.api.endPoints.getAssetSubCategoryByAssetCategory;

    constructor(private httpClient: HttpClient) {

    }

    getRunningNumber(data) {
        return this.httpClient.post(this.getUrl, data,httpOptions);

    }

    getItems() {
        return this.httpClient.get(this.url, httpOptions);
    }
    getActiveItems() {
        return this.httpClient.get(this.ActiveListurl, httpOptions);
    }

    getItemsDetail(id) {
        return this.httpClient.get(this.url + '/' + id, httpOptions);
    }
    getBasedonCategoryUrl(id) {
        return this.httpClient.get(this.BasedonCategoryUrl + '/' + id, httpOptions);
    }
    insertSubcategoryItems(subcategoryData): Observable<any> {
        return this.httpClient.post(this.url, subcategoryData, httpOptions);
    }

    updateSubcategoryItems(subcategoryData, id): Observable<any> {
        return this.httpClient.put(this.url + '/' + id, subcategoryData, httpOptions);
    }
}