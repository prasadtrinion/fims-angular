import { Injectable } from '@angular/core';
import { HttpClientModule,HttpHeaders, HttpClient} from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({ 'X-Auth-Client': 'qazwsx123',
    'Content-Type': 'application/json' })
    
  };
  
@Injectable()

export class AssetsService {
    url: string = environment.api.base + environment.api.endPoints.assets;
    ActiveListurl: string = environment.api.base + environment.api.endPoints.activeAssetCategory;
    BasedonSubCategoryurl: string = environment.api.base + environment.api.endPoints.getAssetByItemSubCategory;
    
    constructor(private httpClient: HttpClient) {    
    }
    getItems() {
        return this.httpClient.get(this.url,httpOptions);
    }
    getActiveItems() {
        return this.httpClient.get(this.ActiveListurl,httpOptions);
    }

    getItemsDetail(id) {
        return this.httpClient.get(this.url+'/'+id,httpOptions);
    }
    BasedonSubCategory(id) {
        return this.httpClient.get(this.BasedonSubCategoryurl+'/'+id,httpOptions);
    }
    insertAssetsItems(assetsData): Observable<any> {
        return this.httpClient.post(this.url, assetsData,httpOptions);
    }

    updateAssetsItems(assetsData,id): Observable<any> {
        return this.httpClient.put(this.url+'/'+id, assetsData,httpOptions);
       }
}