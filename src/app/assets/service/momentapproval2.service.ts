import { Injectable } from '@angular/core';
import { HttpClientModule,HttpHeaders, HttpClient} from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({ 'X-Auth-Client': 'qazwsx123',
    'Content-Type': 'application/json' })
    
  };
@Injectable()

export class Momentapproval2Service {


     url: string = environment.api.base + environment.api.endPoints.assetMovementApproval2
     approvalPuturl: string = environment.api.base + environment.api.endPoints.putApproval2
     approveoneurl: string = environment.api.base + environment.api.endPoints.sellrejectOne
     
    constructor(private httpClient: HttpClient) {    
    }
    getItems() {
        return this.httpClient.get(this.url+'/'+2,httpOptions);
    }   

    getItemsDetail() {
        return this.httpClient.get(this.url+'/'+0, httpOptions);
    }
    insertMomentapproval2Items(momentapproval2Data): Observable<any> {
        return this.httpClient.post(this.url, momentapproval2Data,httpOptions);
    }
    updateMomentapproval2Items(momentapproval2Data): Observable<any> {
        return this.httpClient.post(this.approvalPuturl, momentapproval2Data,httpOptions);
    }
    rejectMomentapproval2(momentapproval2Data): Observable<any> {
        return this.httpClient.post(this.approveoneurl, momentapproval2Data,httpOptions);
    }
}