import { Injectable } from '@angular/core';
import { HttpClientModule, HttpHeaders, HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({
        'X-Auth-Client': 'qazwsx123',
        'Content-Type': 'application/json'
    })

};

@Injectable()

export class ManualgrnService {
    url: string = environment.api.base + environment.api.endPoints.grnDonation;
    deleteurl: string = environment.api.base + environment.api.endPoints.deleteGrnDonationDetail;
    constructor(private httpClient: HttpClient) {
    }
    getItems() {
        return this.httpClient.get(this.url, httpOptions);
    }
    deleteitems(id) {
        return this.httpClient.get(this.deleteurl + '/' + id, httpOptions);
    }
    getItemsDetail(id) {
        return this.httpClient.get(this.url + '/' + id, httpOptions);
    }
    getManualgrnDetailsById(id) {
        return this.httpClient.get(this.url + '/' + id, httpOptions);
    }
    insertManualgrnItems(manualgrnData): Observable<any> {
        return this.httpClient.post(this.url, manualgrnData, httpOptions);
    }

    updateManualgrnItems(manualgrnData, id): Observable<any> {
        return this.httpClient.put(this.url + '/' + id, manualgrnData, httpOptions);
    }
}