import { Injectable } from '@angular/core';
import { HttpClientModule,HttpHeaders, HttpClient} from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({ 'X-Auth-Client': 'qazwsx123',
    'Content-Type': 'application/json' })
    
  };
@Injectable()

export class UpdateassetapprovalService {

     url: string = environment.api.base + environment.api.endPoints.assetInformation
     approvalPuturl: string = environment.api.base + environment.api.endPoints.assetMovementApproval
     approveoneurl: string = environment.api.base + environment.api.endPoints.sellrejectOne
     
     
    constructor(private httpClient: HttpClient) {    
    }
    getItems() {
        return this.httpClient.get(this.url+'/'+2,httpOptions);
    }   
    getItemsDetail(id) {
        return this.httpClient.get(this.url+'/'+id, httpOptions);
    }
    insertAssetapprovalItems(assetapprovalData): Observable<any> {
        return this.httpClient.post(this.url, assetapprovalData,httpOptions);
    }
    updateAssetapprovalItems(assetapprovalData): Observable<any> {
        return this.httpClient.post(this.approvalPuturl, assetapprovalData,httpOptions);
    }
    rejectAssetapproval(assetapprovalData): Observable<any> {
        return this.httpClient.post(this.approveoneurl, assetapprovalData,httpOptions);
    }

    updateAssetapprovalItemsForApproval(assetapprovalData): Observable<any> {
        return this.httpClient.post(environment.api.base+'approveAssetUpdate', assetapprovalData,httpOptions);
    }

}