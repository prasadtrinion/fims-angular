import { Injectable } from '@angular/core';
import { HttpClientModule,HttpHeaders, HttpClient} from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({ 'X-Auth-Client': 'qazwsx123',
    'Content-Type': 'application/json' })
    
  };
  
@Injectable()

export class TempassetinformationService {
    url: string = environment.api.base + environment.api.endPoints.grnDetailsById;
    tempAssetInformationurl: string = environment.api.base + environment.api.endPoints.tempAssetInformation;
    getAssetInformationMirrorData: string = environment.api.base + environment.api.endPoints.getAssetInformationMirrorData;
    approveAssetInformationMirrorData : string  = environment.api.base + environment.api.endPoints.approveAssetInformationMirrorData;
    depreciationSetupUrl : string  = environment.api.base + environment.api.endPoints.depreciationSetup;
    T133fassetInformationUrl : string  = environment.api.base + environment.api.endPoints.T133fassetInformation;
    stockRegistrationMirrorUrl : string  = environment.api.base + environment.api.endPoints.stockRegistrationMirror;

    constructor(private httpClient: HttpClient) { 
        
    }
    
    getItems(id) {
        return this.httpClient.get(this.url+'/'+id,httpOptions);
    }
    getDepreciationCode(){
        return this.httpClient.get(this.depreciationSetupUrl,httpOptions);

    }
    postStockTempInformation(StoreData): Observable<any> {
        return this.httpClient.post(this.stockRegistrationMirrorUrl, StoreData,httpOptions);
    }
    getItemsForAssetRegistration(id){
        return this.httpClient.get(this.T133fassetInformationUrl+'/'+id,httpOptions);
    }
    getPreregistrationItems(id){
        return this.httpClient.get(this.getAssetInformationMirrorData+'/'+id,httpOptions);
    }
    postAssetTempInformation(StoreData): Observable<any> {
        return this.httpClient.post(this.tempAssetInformationurl, StoreData,httpOptions);
    }
    updatePreRegistration(StoreData): Observable<any> {
        return this.httpClient.post(this.approveAssetInformationMirrorData, StoreData,httpOptions);
    }
    
}