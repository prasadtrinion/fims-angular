import { Injectable } from '@angular/core';
import { HttpClientModule,HttpHeaders, HttpClient} from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({ 'X-Auth-Client': 'qazwsx123',
    'Content-Type': 'application/json' })
    
  };
  
@Injectable()

export class AssetpreregistrationService {
    url: string = environment.api.base + environment.api.endPoints.assetPreRegistration;
    constructor(private httpClient: HttpClient) {    
    }
    getItems() {
        return this.httpClient.get(this.url,httpOptions);
    }

    getItemsDetail(id) {
        return this.httpClient.get(this.url+'/'+id,httpOptions);
    }
    insertAssetpreregistrationItems(assetpreregData): Observable<any> {
        return this.httpClient.post(this.url, assetpreregData,httpOptions);
    }
    updateAssetpreregistrationItems(assetpreregData,id): Observable<any> {
        return this.httpClient.put(this.url+'/'+id, assetpreregData,httpOptions);
       }
}