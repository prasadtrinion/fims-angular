import { Injectable } from '@angular/core';
import { HttpClientModule, HttpHeaders, HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({
        'X-Auth-Client': 'qazwsx123',
        'Content-Type': 'application/json'
    })

};
@Injectable()

export class ApplystockapprovalService {

    url: string = environment.api.base + environment.api.endPoints.getApplyStockApprovedList
    approvalPuturl: string = environment.api.base + environment.api.endPoints.ApplyStockApproval

    constructor(private httpClient: HttpClient) {
    }
    getItems() {
        return this.httpClient.get(this.url + '/' + 0, httpOptions);
    }
    getItemsDetail(id) {
        return this.httpClient.get(this.url + '/' + id, httpOptions);
    }
    insertApplystockapprovalItems(applystockapprovalData): Observable<any> {
        return this.httpClient.post(this.url, applystockapprovalData, httpOptions);
    }

    updateApplystockapprovalItems(applystockapprovalData): Observable<any> {
        return this.httpClient.post(this.approvalPuturl, applystockapprovalData, httpOptions);
    }
    updateApplystockRejectedItems(applystockapprovalData): Observable<any> {
        return this.httpClient.post(this.approvalPuturl, applystockapprovalData, httpOptions);
    }
}