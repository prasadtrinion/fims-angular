import { Injectable } from '@angular/core';
import { HttpClientModule,HttpHeaders, HttpClient} from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({ 'X-Auth-Client': 'qazwsx123',
    'Content-Type': 'application/json' })
    
  };
  
@Injectable()

export class UpdateAssetService {

    url: string = environment.api.base + environment.api.endPoints.UpdateAsset;
    urledit: string = environment.api.base + environment.api.endPoints.UpdateAssetEdit;
    urlupdate: string = environment.api.base + environment.api.endPoints.updateAssetInformation
    
    constructor(private httpClient: HttpClient) { 
        
    }
    getUpdateItems() {
        return this.httpClient.get(this.url+'/'+3,httpOptions);
    }
    getItems() {
        return this.httpClient.get(this.url+'/'+0,httpOptions);
    }
    getItemsDetail(id) {
        return this.httpClient.get(this.urledit+'/'+id,httpOptions);
    }

    insertUpdateAssetItems(assetsData): Observable<any> {
        return this.httpClient.post(this.url, assetsData,httpOptions);
    }

    updateUpdateAssetItems(assetsData,id): Observable<any> {
        return this.httpClient.put(this.urlupdate+'/'+id, assetsData,httpOptions);
       }
}