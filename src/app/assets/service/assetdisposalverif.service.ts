import { Injectable } from '@angular/core';
import { HttpClientModule,HttpHeaders, HttpClient} from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({ 'X-Auth-Client': 'qazwsx123',
    'Content-Type': 'application/json' })
    
  };
@Injectable()

export class AssetdisposalverifService {
     disposalListurl : string = environment.api.base + environment.api.endPoints.allDisposalDetails;
     url: string = environment.api.base + environment.api.endPoints.disposalVerification;
     approveoneurl: string = environment.api.base + environment.api.endPoints.sellrejectOne
     listofAssetDisposal : string = environment.api.base + environment.api.endPoints.assetDisposalList;
    constructor(private httpClient: HttpClient) {    
    }
    getItems() {
        return this.httpClient.get(this.url,httpOptions);
    }   
    getItemsDetailForDisposal() {
        return this.httpClient.get(this.disposalListurl, httpOptions);
    }
    getItemsDisposal(id) {
        return this.httpClient.get(this.listofAssetDisposal+'/'+id,httpOptions);
    }   
    
    insertAssetdisposalverifItems(verificationData): Observable<any> {
        return this.httpClient.post(this.url, verificationData,httpOptions);
    }
}