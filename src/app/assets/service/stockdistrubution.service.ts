import { Injectable } from '@angular/core';
import { HttpClientModule, HttpHeaders, HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({
        'X-Auth-Client': 'qazwsx123',
        'Content-Type': 'application/json'
    })

};

@Injectable()

export class StockDistrubutionService {
    url: string = environment.api.base + environment.api.endPoints.getApplyStockApprovedList
    Distrubuteurl: string = environment.api.base + environment.api.endPoints.stockDistribution
    constructor(private httpClient: HttpClient) {

    }

    getItems() {
        return this.httpClient.get(this.url + '/' + 1, httpOptions);
    }

    getItemsDetail(id) {
        return this.httpClient.get(this.Distrubuteurl + '/' + id, httpOptions);
    }
    getApplyloanDetailsById(id) {
        return this.httpClient.get(this.url + '/' + id, httpOptions);
    }

    insertApplystockItems(assetdisposalData): Observable<any> {
        return this.httpClient.post(this.url, assetdisposalData, httpOptions);
    }

    updateApplystockItems(assetdisposalData, id): Observable<any> {
        return this.httpClient.put(this.url + '/' + id, assetdisposalData, httpOptions);
    }
}