import { Injectable } from '@angular/core';
import { HttpClientModule,HttpHeaders, HttpClient} from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({ 'X-Auth-Client': 'qazwsx123',
    'Content-Type': 'application/json' })
    
  };
  
@Injectable()

export class DisposalrequisitionService {
    url: string = environment.api.base + environment.api.endPoints.disposalRequisition;
    urlendorse: string = environment.api.base + environment.api.endPoints.endorseDisposal
    urlverification: string = environment.api.base + environment.api.endPoints.disposalVerification;
    deleteverification: string = environment.api.base + environment.api.endPoints.deleteDisposalVerDetail;
    deleteRequistion: string = environment.api.base + environment.api.endPoints.deleteDisposalReqDetail;

    constructor(private httpClient: HttpClient) { 
        
    }
    
    getItems() {
        return this.httpClient.get(this.url,httpOptions);
    }
    deleteverificationItems(id) {
        return this.httpClient.get(this.deleteverification+'/'+id,httpOptions);
    }
    deleteRequistionItems(id) {
        return this.httpClient.get(this.deleteRequistion+'/'+id,httpOptions);
    }
    getVerificationItems(id) {
        return this.httpClient.get(this.urlverification+'/'+id,httpOptions);
    }
    insertVerificationItems(data) {
        return this.httpClient.post(this.urlverification,data,httpOptions);
    }
    getItemsDetail(id) {
        return this.httpClient.get(this.url+'/'+id,httpOptions);
    }
    getAssetdisposalDetailsById(id) {
        return this.httpClient.get(this.url+'/'+id,httpOptions);
}

    insertDisposalrequisitionItems(disposalreqData): Observable<any> {
        return this.httpClient.post(this.url, disposalreqData,httpOptions);
    }
    
    updateDisposalEndorsement(disposalreqData): Observable<any> {
        return this.httpClient.post(this.urlendorse, disposalreqData,httpOptions);
    }
    updateDisposalrequisitionItems(disposalreqData,id): Observable<any> {
        return this.httpClient.put(this.url+'/'+id, disposalreqData,httpOptions);
       }
}