import { Injectable } from '@angular/core';
import { HttpClientModule,HttpHeaders, HttpClient} from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({ 'X-Auth-Client': 'qazwsx123',
    'Content-Type': 'application/json' })
    
  };
  
@Injectable()

export class DisposalendorsementService {
    url: string = environment.api.base + environment.api.endPoints.assettype;
    verificationurl: string = environment.api.base + environment.api.endPoints.disposalVerification;

    constructor(private httpClient: HttpClient) {    
    }
    getItems() {
        return this.httpClient.get(this.verificationurl,httpOptions);
    }
    getItemsDetail(id) {
        return this.httpClient.get(this.url+'/'+id,httpOptions);
    }
    insertDisposalendorsementItems(endorsementData): Observable<any> {
        return this.httpClient.post(this.url,endorsementData,httpOptions);
    }
    updateDisposalendorsementItems(endorsementData,id): Observable<any> {
        return this.httpClient.put(this.url+'/'+id, endorsementData,httpOptions);
       }
}