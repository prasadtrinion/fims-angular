import { Injectable } from '@angular/core';
import { HttpClientModule,HttpHeaders, HttpClient} from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({ 'X-Auth-Client': 'qazwsx123',
    'Content-Type': 'application/json' })
    
  };
  
@Injectable()

export class LocationService {
    url: string = environment.api.base + environment.api.endPoints.newAssetCode;
    Deleteurl: string = environment.api.base + environment.api.endPoints.deleteAssetLocationData;
    constructor(private httpClient: HttpClient) { 
        
    }
    getItems() {
        return this.httpClient.get(this.url,httpOptions);
    }
    deleteItems(deleteObject) {
        return this.httpClient.post(this.Deleteurl,deleteObject, httpOptions);
    }
    getItemsDetail(parentId,levelId) {
        return this.httpClient.get(environment.api.base+'assetParentLevelList/' + parentId +'/'+levelId, httpOptions);
    }

    insertnewaccountcodeItems(locationData): Observable<any> {
        return this.httpClient.post(this.url, locationData,httpOptions);
    }

    updateLocationItems(locationData,id): Observable<any> {
        return this.httpClient.put(this.url+'/'+id,locationData,httpOptions);
       }
}
