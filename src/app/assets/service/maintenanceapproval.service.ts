import { Injectable } from '@angular/core';
import { HttpClientModule,HttpHeaders, HttpClient} from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({ 'X-Auth-Client': 'qazwsx123',
    'Content-Type': 'application/json' })
    
  };
@Injectable()

export class MaintenanceapprovalService {

     url: string = environment.api.base + environment.api.endPoints.getMaintenanceApprovedList 
     approvalPuturl: string = environment.api.base + environment.api.endPoints.assetMaintenanceApproval
    constructor(private httpClient: HttpClient) {    
    }
    getItems() {
        return this.httpClient.get(this.url+'/'+0,httpOptions);
    }   
    getItemsDetail(id) {
        return this.httpClient.get(this.url+'/'+id, httpOptions);
    }
    insertMaintenanceapprovalItems(maintenanceapprovalData): Observable<any> {
        return this.httpClient.post(this.url, maintenanceapprovalData,httpOptions);
    }
    updateMaintenanceapprovalItems(maintenanceapprovalData): Observable<any> {
        return this.httpClient.post(this.approvalPuturl, maintenanceapprovalData,httpOptions);
    }
}