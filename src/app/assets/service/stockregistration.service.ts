import { Injectable } from '@angular/core';
import { HttpClientModule, HttpHeaders, HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({
        'X-Auth-Client': 'qazwsx123',
        'Content-Type': 'application/json'
    })

};

@Injectable()

export class StockRegistrationService {
    url: string = environment.api.base + environment.api.endPoints.stockRegistration;
    ActiveListurl: string = environment.api.base + environment.api.endPoints.assetitemActive;
    getMirrorStockRegistrationActiveListurl: string = environment.api.base + environment.api.endPoints.getMirrorStockRegistrationActiveList;

    constructor(private httpClient: HttpClient) {

    }

    getItems() {
        return this.httpClient.get(this.url, httpOptions);
    }
    getActiveItems() {
        return this.httpClient.get(this.ActiveListurl, httpOptions);
    }

    getTempRegistration(id){
return this.httpClient.get(this.getMirrorStockRegistrationActiveListurl+'/'+id,httpOptions);
    }
    getItemsDetail(id) {
        return this.httpClient.get(this.url + '/' + id, httpOptions);
    }
    updatePreRegistrationForStock(subcategoryData): Observable<any> {
        return this.httpClient.post(this.getMirrorStockRegistrationActiveListurl, subcategoryData, httpOptions);
    }

    insertAssetItemItems(subcategoryData): Observable<any> {
        return this.httpClient.post(this.url, subcategoryData, httpOptions);
    }

    updateAssetItemItems(subcategoryData, id): Observable<any> {
        return this.httpClient.put(this.url + '/' + id, subcategoryData, httpOptions);
    }
}