import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { UpdateassetapprovalService } from '../service/updateassetapproval.service';
import { UpdateAssetService } from "../service/updateasset.service";
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'updateassetapproval',
    templateUrl: 'updateassetapproval.component.html'
})

export class UpdateassetapprovalComponent implements OnInit {

    assetapprovalList = [];
    assetapprovalData = {};
    selectAllCheckbox = true;
    assetsList = [];
    approvalData = [];

    constructor(

        private UpdateassetapprovalService: UpdateassetapprovalService,
        private UpdateAssetService: UpdateAssetService,
        private spinner: NgxSpinnerService,
        private router: Router
    ) { }

    ngOnInit() {
        this.getListData();
    }
    getListData() {
        this.spinner.show();
        this.UpdateAssetService.getUpdateItems().subscribe(
            data => {
                this.spinner.hide();
                this.assetapprovalList = data['result']['data'];
            }, error => {
                console.log(error);
            });
    }

    assetapprovalListData() {
        var approvaloneIds = [];
        for (var i = 0; i < this.assetapprovalList.length; i++) {
            if (this.assetapprovalList[i].f087fapprovedStatus == true) {
                approvaloneIds.push(this.assetapprovalList[i].f085fid);
            }
        }
        if (approvaloneIds.length < 1) {
            alert("Select atleast one Update Asset to Approve");
            return false;
        }
        var confirmPop = confirm("Do you want to Approve?");
        if (confirmPop == false) {
            return false;
        }
        var approvaloneUpdate = {};
        approvaloneUpdate['id'] = approvaloneIds;
        approvaloneUpdate['f087freason'] = '';
        approvaloneUpdate['status'] = 1;
        this.UpdateassetapprovalService.updateAssetapprovalItemsForApproval(approvaloneUpdate).subscribe(
            data => {
                alert(" Approved Sucessfully ! ");
                this.getListData();
                this.assetapprovalData['f087freason'] = '';
            }, error => {
                console.log(error);
            });
    }
    regectApprovalListData() {
        var approvaloneIds = [];
        for (var i = 0; i < this.assetapprovalList.length; i++) {
            if (this.assetapprovalList[i].f087fapprovedStatus == true) {
                approvaloneIds.push(this.assetapprovalList[i].f085fid);
            }
        }
        if (approvaloneIds.length < 1) {
            alert("Select atleast one Update Asset to Reject");
            return false;
        }
        if (this.assetapprovalData['reason'] == undefined) {
            alert('Enter the Description');
            return false;
        }
        var confirmPop = confirm("Do you want to Reject?");
        if (confirmPop == false) {
            return false;
        }
        var budgetActivityDataObject = {}
        budgetActivityDataObject['id'] = approvaloneIds;
        budgetActivityDataObject['status'] = 2;
        budgetActivityDataObject['reason'] = this.assetapprovalData['reason'];


        this.UpdateassetapprovalService.updateAssetapprovalItemsForApproval(budgetActivityDataObject).subscribe(
            data => {
                this.getListData();
                this.assetapprovalData['f087freason'] = '';
                alert(" Rejected Sucessfully ! ");
            }, error => {
                console.log(error);
            });
    }
}
