import { Injector } from '@angular/core';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';

@Component({
    selector: 'LocationFormComponent',
    templateUrl: 'locationform.component.html'
})

export class LocationFormComponent implements OnInit {
    locationform: FormGroup;
    locationList = [];
    locationData = {};
    id: number;
    constructor(

        private route: ActivatedRoute,
        private router: Router

    ) { }

    ngOnInit() {
        this.locationform = new FormGroup({
            utility: new FormControl('', Validators.required),

        });
        this.locationData['f039fstatus'] = 1;
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        console.log(this.id);
    }
}