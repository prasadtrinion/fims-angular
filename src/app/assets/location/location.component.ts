import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LocationService } from "../service/location.service";

@Component({
    selector: 'Location',
    templateUrl: 'location.component.html'
})
export class LocationComponent implements OnInit {
    locationList = [];
    locationData = {};
    id: number;
    constructor(
        private LocationService: LocationService
    ) { }

    ngOnInit() {

        this.LocationService.getItems().subscribe(
            data => {
                this.locationList = data['result']['data'];
            }, error => {
                console.log(error);
            });
    }
    
}