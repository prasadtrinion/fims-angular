import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AssetinformationService } from '../service/assetinformation.service'
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'Assetinformation',
    templateUrl: 'assetinformation.component.html'
})
export class AssetinformationComponent implements OnInit {
    assetinformationList = [];
    assetinformationData = {};
    elementType: 'url' | 'canvas' | 'img' = 'url';
    value: string = 'Techiediaries';
    id: number;
    constructor(
        private AssetinformationService: AssetinformationService,
        private spinner: NgxSpinnerService
    ) { }

    ngOnInit() {
        this.spinner.show();
        this.AssetinformationService.getItems().subscribe(
            data => {
                this.spinner.hide();
                this.assetinformationList = data['data']['data'];
                for (var i = 0; i < this.assetinformationList.length; i++) {
                    var deprecatedValueperannum = (this.assetinformationList[i]['f085fyearDeprCost'] / 12);
                    var date1 = new Date(this.assetinformationList[i]['f085fAcquisitionDate']);
                    var date2 = new Date();
                    var dd = date2.getDate();
                    var timeDiff = Math.abs(date2.getTime() - date1.getTime());
                    var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
                    console.log(diffDays);
                    if ((diffDays - dd) > 0) {
                        var numberofMonthExtended = (diffDays - dd) / 30;
                        var deprecatedValueOfAsset = (numberofMonthExtended * deprecatedValueperannum);
                        if (deprecatedValueOfAsset < 0) {
                            deprecatedValueOfAsset = 0;
                        }
                        this.assetinformationList[i]['depreciatedValue'] = deprecatedValueOfAsset;
                    } else {
                        this.assetinformationList[i]['depreciatedValue'] = 0;

                    }
                }
            }, error => {
                console.log(error);
            });
    }


    printqr()
    {
        
        var prtContent = document.getElementById("p");
var WinPrint = window.open('','','left=0,top=0');
WinPrint.document.write(prtContent.innerHTML);
WinPrint.document.close();
WinPrint.focus();
//window.print();
    }
}