import { Injector } from '@angular/core';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AssetinformationService } from '../../service/assetinformation.service'
import { AssetsService } from '../../service/assets.service'
import { DepartmentService } from '../../../generalsetup/service/department.service'
import { AccountcodeService } from '../../../generalsetup/service/accountcode.service'
import { SubcategoryService } from "../../service/subcategory.service";
import { AssetItemService } from "../../service/assetitem.service";

import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { TypeService } from "../../service/type.service";

@Component({
    selector: 'AssetinformationFormComponent',
    templateUrl: 'assetinformationform.component.html'
})

export class AssetinformationFormComponent implements OnInit {
    assetinformationform: FormGroup;
    assetinformationList = [];
    assetinformationData = {};
    assetinformationDataheader = {};
    assetssList = [];
    DeptList = [];
    deptData = {};
    assettypeList = [];
    accountcodeList = [];
    categoryList = []
    ajaxCount: number;
    id: number;
    typeList = [];
    campusList = [];
    blockList = [];
    buildingList = [];
    roomList = [];
    subcategoryList = [];
    itemList = [];
    constructor(
        private AssetinformationService: AssetinformationService,
        private AssetsService: AssetsService,
        private DepartmentService: DepartmentService,
        private AccountcodeService: AccountcodeService,
        private route: ActivatedRoute,
        private router: Router,
        private TypeService: TypeService,
        private SubcategoryService: SubcategoryService,
        private AssetItemService: AssetItemService

    ) { }

    ngOnInit() {
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        this.ajaxCount = 0;
        //  Cost Center 
        this.ajaxCount++;
        this.DepartmentService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.DeptList = data['result']['data'];
            }, error => {
                console.log(error);
            });

        this.ajaxCount = 0;
        //  Cost Center 
        this.ajaxCount++;
        this.AssetinformationService.getlocationItems(0).subscribe(
            data => {
                this.ajaxCount--;
                this.campusList = data['result']['data'];
            }, error => {
                console.log(error);
            });

        this.ajaxCount = 0;
        //  Cost Center 
        this.ajaxCount++;
        this.AssetinformationService.getlocationItems(2).subscribe(
            data => {
                this.ajaxCount--;
                this.blockList = data['result']['data'];
            }, error => {
                console.log(error);
            });


        this.AssetItemService.getItems().subscribe(
            data => {
                this.itemList = data['result']['data'];
            }, error => {
                console.log(error);
            });


        this.ajaxCount = 0;
        //  Cost Center 
        this.ajaxCount++;
        this.AssetinformationService.getlocationItems(1).subscribe(
            data => {
                this.ajaxCount--;
                this.buildingList = data['result']['data'];
            }, error => {
                console.log(error);
            });

        this.ajaxCount = 0;
        //  Cost Center 
        this.ajaxCount++;
        this.AssetinformationService.getlocationItems(3).subscribe(
            data => {
                this.ajaxCount--;
                this.roomList = data['result']['data'];
            }, error => {
                console.log(error);
            });

        this.SubcategoryService.getItems().subscribe(
            data => {
                this.subcategoryList = data['result']['data'];
            }, error => {
                console.log(error);
            });

        // assets category
        this.ajaxCount++;
        this.AssetsService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.categoryList = data['result']['data'];
                console.log(this.categoryList);
            }, error => {
                console.log(error);
            });
        console.log(this.id);

        this.TypeService.getItems().subscribe(
            data => {
                this.typeList = data['result']['data'];
            }, error => {
                console.log(error);
            });


        //account code
        this.ajaxCount++;
        this.AccountcodeService.getItemsByLevel().subscribe(
            data => {
                this.ajaxCount--;
                this.accountcodeList = data['result']['data'];
            }, error => {
                console.log(error);
            });
        if (this.id > 0) {
            this.AssetinformationService.getItemsDetail(this.id).subscribe(
                data => {
                    this.assetinformationData = data['result'][0];
                    console.log(this.assetinformationData);
                }, error => {
                    console.log(error);
                });
        }
        console.log(this.id);

    }

    addAssetinformation() {
        this.AssetinformationService.insertAssetinformationItems(this.assetinformationData).subscribe(
            data => {
                if (data['status'] == 409) {
                    alert('Asset Type Already Exist');
                }
                else {
                    this.router.navigate(['assets/assetinformation']);
                }

            }, error => {
                console.log(error);
            });

    }

}