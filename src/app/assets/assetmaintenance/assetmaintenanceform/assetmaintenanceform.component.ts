import { Injector } from '@angular/core';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AssetmaintenanceService } from '../../service/assetmaintenance.service'
import { AssetinformationService } from '../../service/assetinformation.service'
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'AssetmaintenanceFormComponent',
    templateUrl: 'assetmaintenanceform.component.html'
})

export class AssetmaintenanceFormComponent implements OnInit {
    maintenanceform: FormGroup;
    maintenanceList = [];
    maintenanceData = {};
    assetinformationList = [];
    viewDisabled: boolean;
    editDisabled: boolean;
    id: number;
    idview: number;
    ajaxCount: number;
    constructor(

        private AssetmaintenanceService: AssetmaintenanceService,
        private AssetinformationService: AssetinformationService,
        private spinner: NgxSpinnerService,
        private route: ActivatedRoute,
        private router: Router

    ) { }
    ngDoCheck() {
        const change = this.ajaxCount;
        if (this.ajaxCount == 0) {
            this.spinner.hide();
        }
    }
    ngOnInit() {
        this.ajaxCount = 0
        this.spinner.show();
        this.viewDisabled = false;
        this.editDisabled = false;
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.route.paramMap.subscribe((data) => this.idview = + data.get('idview'));
        this.maintenanceform = new FormGroup({
            utility: new FormControl('', Validators.required),
        });
        //assets
        this.ajaxCount++;
        this.AssetinformationService.getMaintenanceAssetCodeItems().subscribe(
            data => {
                this.ajaxCount--;
                this.assetinformationList = data['result'];
            }, error => {
                console.log(error);
            });
        this.maintenanceData['f089fstatus'] = 1;
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        if (this.id > 0) {
            this.ajaxCount++;
            this.AssetmaintenanceService.getItemsDetail(this.id).subscribe(
                data => {
                    this.ajaxCount--;
                    this.AssetinformationService.getAllAssetCodeItems().subscribe(
                        data => {
                            this.assetinformationList = data['data']['data'];
                        }, error => {
                            console.log(error);
                        });
                    this.maintenanceData = data['result']['data'][0];
                    // this.maintenanceData['f089fstatus'] = parseInt(this.maintenanceData['f089fstatus']);
                    if (data['result']['data'][0]['f089fapprovalStatus'] == 1 || data['result']['data'][0]['f089fapprovalStatus'] == 2) {
                        this.viewDisabled = true;
                        setTimeout(
                            function () {
                                $("#target input,select").prop("disabled", true);
                                $("#target1 input,select").prop("disabled", true);
                            }, 2000);

                    }
                    if (this.idview == 2) {
                        this.viewDisabled = true;

                        $("#target input,select").prop("disabled", true);
                        $("#target1 input,select").prop("disabled", true);
                    }
                    if (data['result']['data'][0]['f089fapprovalStatus'] == 1) {
                        this.AssetinformationService.getAllAssetCodeItems().subscribe(
                            data => {
                                this.assetinformationList = data['data']['data'];
                            }, error => {
                                console.log(error);
                            });
                    }
                    this.editDisabled = true
                }, error => {
                    console.log(error);
                });
        }
    }
    addAssetmaintenance() {
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.AssetmaintenanceService.updateAssetmaintenanceItems(this.maintenanceData, this.id).subscribe(
                data => {
                    this.router.navigate(['assets/assetmaintenance']);
                    alert("Maintenance has been Updated Sucessfully !!");
                }, error => {
                    console.log(error);
                });
        } else {
            this.AssetmaintenanceService.insertAssetmaintenanceItems(this.maintenanceData).subscribe(
                data => {
                    this.router.navigate(['assets/assetmaintenance']);
                    alert("Maintenance has been Added Sucessfully !!");

                }, error => {
                    console.log(error);
                });
        }
    }

}