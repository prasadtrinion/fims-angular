import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AssetmaintenanceService } from '../service/assetmaintenance.service'
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'Assetmaintenance',
    templateUrl: 'assetmaintenance.component.html'
})
export class AssetmaintenanceComponent implements OnInit {
    maintenanceList = [];
    maintenanceData = {};
    id: number;
    constructor(
        private AssetmaintenanceService: AssetmaintenanceService,
        private spinner: NgxSpinnerService
    ) { }

    ngOnInit() {
        this.spinner.show();
        this.AssetmaintenanceService.getItems().subscribe(
            data => {
                this.spinner.hide();
                let activityData = [];
                activityData = data['result']['data'];
                console.log("asdf");
                for (var i = 0; i < activityData.length; i++) {
                    if (activityData[i]['f089fapprovalStatus'] == 0) {
                        activityData[i]['f089fapprovalStatus'] = 'Pending';
                    } else if (activityData[i]['f089fapprovalStatus'] == 1) {
                        activityData[i]['f089fapprovalStatus'] = 'Approved';
                    } else {
                        activityData[i]['f089fapprovalStatus'] = 'Rejected';
                    }
                }
                this.maintenanceList = activityData;
            }, error => {
                console.log(error);
            });
    }
}