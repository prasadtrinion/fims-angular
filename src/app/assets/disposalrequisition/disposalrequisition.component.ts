import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DisposalrequisitionService } from '../service/disposalrequisition.service'
import { NgxSpinnerService } from 'ngx-spinner';


@Component({
    selector: 'Disposalrequisition',
    templateUrl: 'disposalrequisition.component.html'
})
export class DisposalrequisitionComponent implements OnInit {
    disposalreqList = [];
    disposalreqData = {};
    id: number;
    constructor(
        private DisposalrequisitionService: DisposalrequisitionService,
        private spinner: NgxSpinnerService,
    ) { }

    ngOnInit() {
        this.spinner.show();
        this.DisposalrequisitionService.getItems().subscribe(
            data => {
                this.spinner.hide();
                this.disposalreqList = data['data']['data'];
            }, error => {
                console.log(error);
            });
    }
}