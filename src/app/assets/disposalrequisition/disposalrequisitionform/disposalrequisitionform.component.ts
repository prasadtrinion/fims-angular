import { Injector } from '@angular/core';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DisposalrequisitionService } from '../../service/disposalrequisition.service'
import { StaffService } from "../../../loan/service/staff.service";
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { AssetinformationService } from '../../service/assetinformation.service'
import { AssettypeService } from '../../service/assettype.service'
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'DisposalrequisitionFormComponent',
    templateUrl: 'disposalrequisitionform.component.html'
})

export class DisposalrequisitionFormComponent implements OnInit {
    disposalreqform: FormGroup;
    disposalreqList = [];
    disposalreqData = {};
    deleteList = [];
    disposalreqDataheader = {};
    staffList = [];
    assetssList = [];
    assetinformationList1 = [];
    ajaxCount: number;
    assetinformationList = [];
    disposalverification = [];
    id: number;
    disposalList = [];
    viewDisabled: boolean;
    idview: number;
    showLastRow: boolean;
    showLastRowMinus: boolean;
    showLastRowPlus: boolean;
    listshowLastRowMinus: boolean;
    listshowLastRowPlus: boolean;
    editDisabled: boolean;
    constructor(
        private DisposalrequisitionService: DisposalrequisitionService,
        private StaffService: StaffService,
        private route: ActivatedRoute,
        private router: Router,
        private AssetinformationService: AssetinformationService,
        private AssettypeService: AssettypeService,
        private spinner: NgxSpinnerService,

    ) { }
    showIcons() {
        // this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.disposalreqList.length > 0) {
            this.showLastRowMinus = true;
            this.showLastRowPlus = true;
            this.listshowLastRowMinus = true;
        }
        if (this.disposalreqList.length > 1) {
            if (this.showLastRow == false) {
                this.listshowLastRowPlus = false;
                this.listshowLastRowMinus = true;
                this.showLastRowPlus = true;
            } else {
                this.listshowLastRowPlus = true;
                this.listshowLastRowMinus = false;
            }
        }
        if (this.disposalreqList.length == 1 && this.showLastRow == false) {
            this.listshowLastRowMinus = true;
            this.listshowLastRowPlus = false;

        }
        if (this.disposalreqList.length == 1 && this.showLastRow == true) {
            this.listshowLastRowMinus = false;
            this.listshowLastRowPlus = true;

        }
        if (this.disposalreqList.length == 0 && this.showLastRow == false) {
            this.showLastRowMinus = false;
            this.showLastRowPlus = true;
        }
        if (this.disposalreqList.length == 0 && this.showLastRow == true) {
            this.showLastRowMinus = true;
            this.showLastRowPlus = false;
        }
        if (this.idview == 2 || this.viewDisabled == true) {
            this.listshowLastRowPlus = false;
        }

    }
    deletemodule() {
        this.showLastRow = true;
        this.showIcons();
    }
    showNewRow() {
        var confirmPop = confirm("Do you want to Add?");
        if (confirmPop == false) {
            return false;
        }
        this.showLastRow = false;
        this.showIcons();
    }
    ngDoCheck() {
        const change = this.ajaxCount;
        if (this.ajaxCount == 0) {
            this.editFunction();
            this.spinner.hide();
            this.ajaxCount = 10;
        }
    }
    editFunction() {
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.route.paramMap.subscribe((data) => this.idview = + data.get('idview'));
        this.ajaxCount++;
        if (this.id > 0) {
            this.DisposalrequisitionService.getVerificationItems(this.id).subscribe(
                data => {
                    this.ajaxCount--;
                    this.AssetinformationService.getAllAssetCodeItems().subscribe(
                        data => {
                            this.ajaxCount--;
                            this.assetinformationList = data['data']['data'];
                        }, error => {
                            console.log(error);
                        });
                    if (data['result'] == '') {
                        this.prefillRequisition();
                    } else {
                        this.disposalverification = data['result'];
                        let datanew = [];
                        this.disposalreqDataheader['f086fidRequisition'] = data['result'][0]['f087fidRequisition'];
                        this.disposalreqDataheader['f086fdescription'] = data['result'][0]['f087fdescription'];
                        if (data['result'][0]['f087fstatus'] == 0) {
                            this.disposalreqDataheader['f086fverificationStatus'] = 'Entry';
                        }
                        else {
                            this.disposalreqDataheader['f086fverificationStatus'] = 'Verified';
                        }

                        this.disposalreqDataheader['f087fmeetingNumber'] = data['result'][0]['f087fmeetingNumber'];
                        this.disposalreqDataheader['f087fremarks'] = data['result'][0]['f087fremarks'];
                        for (var i = 0; i < this.disposalverification.length; i++) {
                            var creditnoteObject = {};
                            creditnoteObject['f086fidAsset'] = this.disposalverification[i]['f087fidAsset'];
                            creditnoteObject['f086fdescription'] = this.disposalverification[i]['f087fdescription'];
                            creditnoteObject['f086fnetValue'] = this.disposalverification[i]['f087fnetValue'];
                            creditnoteObject['f086fassetCode'] = this.disposalverification[i]['f087fassetCode'];
                            creditnoteObject['f086ftype'] = this.disposalverification[i]['f087ftype'];
                            datanew.push(creditnoteObject);

                        }
                        if (data['result'][0]['f087fstatus'] == 1) {
                            this.AssetinformationService.getAllAssetCodeItems().subscribe(
                                data => {
                                    this.ajaxCount--;
                                    this.assetinformationList = data['data']['data'];
                                }, error => {
                                    console.log(error);
                                });
                        }
                        this.disposalreqDataheader['f087fidRequisition'] = this.id;
                        this.disposalreqList = datanew;
                        if (this.idview == 2 || data['result'][0]['f086fisEndorsement'] == 1) {
                            this.viewDisabled = true;
                            this.editDisabled = true;
                            setTimeout(function () {
                                $("#target input,select").prop("disabled", true);
                                $("#target1 input,select").prop("disabled", true);
                            }, 3000);
                        }
                        this.showIcons();
                    }
                });
        }
    }
    prefillRequisition() {
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.route.paramMap.subscribe((data) => this.idview = + data.get('idview'));
        if (this.id > 0) {
            this.DisposalrequisitionService.getItemsDetail(this.id).subscribe(
                data => {
                    this.AssetinformationService.getAllAssetCodeItems().subscribe(
                        data => {
                            this.ajaxCount--;
                            this.assetinformationList = data['data']['data'];
                        }, error => {
                            console.log(error);
                        });
                    this.disposalreqList = data['result'];
                    this.disposalreqDataheader['f086fdescription'] = data['result'][0]['parentDescription'];
                    this.deleteDisposalrequisition['f086fidAsset'] = data['result'][0]['f086fidAsset']
                    if (this.idview == 2 || data['result'][0]['f086fisEndorsement'] == 1) {
                        this.viewDisabled = true;
                        $("#target input,select").prop("disabled", true);
                        $("#target1 input,select").prop("disabled", true);
                    }
                    this.showIcons();
                    this.editDisabled = true;
                }, error => {
                    console.log(error);
                });
        }
    }
    ngOnInit() {
        this.ajaxCount = 0
        this.spinner.show();
        this.editDisabled = false;
        this.viewDisabled = false;
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id < 1) {
            this.showLastRow = false;
            this.showLastRowMinus = false;
            this.showLastRowPlus = true;
        } else {
            this.showLastRow = true;

        }
        this.disposalreqDataheader['f086frequestBy'] = localStorage.getItem("username");
        this.disposalreqDataheader['f086fverificationStatus'] = 'Entry';
        this.ajaxCount = 0;

        // assets category
        this.ajaxCount++;
        this.AssetinformationService.getRequistionAssetCodeItems().subscribe(
            data => {
                this.ajaxCount--;
                this.assetinformationList1 = data['result'];
            }, error => {
                console.log(error);
            });
        // asset codecategory
        this.ajaxCount++;
        this.AssetinformationService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.assetinformationList = data['data']['data'];
            }, error => {
                console.log(error);
            });

        this.ajaxCount++;

        this.AssettypeService.getItems().subscribe(
            data => {
                this.ajaxCount--;

                this.disposalList = data['data']['data'];
            }, error => {
                console.log(error);
            });
        // staff dropdown
        this.ajaxCount++;
        this.StaffService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.staffList = data['result']['data'];
            }, error => {
                console.log(error);
            });
        this.showIcons();
    }

    getAssetValues() {
        for (var i = 0; i < this.assetinformationList.length; i++) {
            if (this.assetinformationList[i]['f085fid'] == this.disposalreqData['f086fidAsset']) {
                this.disposalreqData['f086fdescription'] = this.assetinformationList[i]['f085fassetDescription'];
                this.disposalreqData['f086fassetCode'] = this.assetinformationList[i]['f085fassetCode'];


            }
        }
    }
    saveVerification() {

        // if (this.idview == 2) {
        if (this.disposalreqDataheader['f087fmeetingNumber'] == undefined) {
            alert('Enter Meeting Number');
            return false;
        }
        else if (this.disposalreqDataheader['f087fremarks'] == undefined) {
            alert('Enter Reason');
            return false;
        }
        if (this.disposalreqList.length > 0) {

        } else {
            alert("Please add one details");
            return false;
        }
        if (this.showLastRow == false) {
            var addactivities = this.adddisposalrequisitionlist();
            if (addactivities = false) {
                return false;
            }
        }

        let disposalNewObj = {};

        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            disposalNewObj['f087fid'] = this.id;
        }
        var saveDetails = [];
        disposalNewObj['f087fstatus'] = 0;
        disposalNewObj['f087fdescription'] = this.disposalreqDataheader['f087fdescription'];
        disposalNewObj['f087fverificationStatus'] = this.disposalreqDataheader['f087fverificationStatus'];
        disposalNewObj['f087fmeetingNumber'] = this.disposalreqDataheader['f087fmeetingNumber'];
        disposalNewObj['f087fremarks'] = this.disposalreqDataheader['f087fremarks'];
        disposalNewObj['f087fidRequisition'] = this.id;
        for (var i = 0; i < this.disposalreqList.length; i++) {
            var creditnoteObject = {}
            if (this.id > 0) {
                creditnoteObject['f087fidDetails'] = this.id;
            }
            creditnoteObject['f087fidAsset'] = this.disposalreqList[i]['f086fidAsset'];
            creditnoteObject['f087fdescription'] = this.disposalreqList[i]['f086fdescription'];
            creditnoteObject['f087fnetValue'] = this.disposalreqList[i]['f086fnetValue'];
            creditnoteObject['f087fassetCode'] = this.disposalreqList[i]['f086fassetCode'];
            creditnoteObject['f087ftype'] = this.disposalreqList[i]['f086ftype'];
            saveDetails.push(creditnoteObject);
        }
        disposalNewObj["verification-details"] = saveDetails;
        this.DisposalrequisitionService.insertVerificationItems(disposalNewObj).subscribe(
            data => {
                this.router.navigate(['assets/disposalendorsement']);
            }, error => {
                console.log(error);
            });
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        let finaldisposeArray = [];
        finaldisposeArray.push(this.id);
        let finalobject = {};
        finalobject['id'] = finaldisposeArray;
        var confirmPop = confirm("Do you want to Endorse?");
        if (confirmPop == false) {
            return false;
        }
        this.DisposalrequisitionService.updateDisposalEndorsement(finalobject).subscribe(
            data => {
                alert("Endorsed Successfully");
                this.router.navigate(['assets/disposalendorsement']);
            }
        );
    }
    saveDisposalrequisition() {
        if (this.disposalreqDataheader['f086fdescription'] == undefined) {
            alert('Enter Description');
            return false;
        }
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        if (this.showLastRow == false) {
            var addactivities = this.adddisposalrequisitionlist();
            if (addactivities = false) {
                return false;
            }
        }
        let disposalNewObj = {};
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            disposalNewObj['f086fid'] = this.id;
        }
        if (this.disposalreqList.length > 0) {

        } else {
            alert("Add atleast one details");
            return false;
        }
        disposalNewObj['f086fstatus'] = 0;
        disposalNewObj['f086fdescription'] = this.disposalreqDataheader['f086fdescription'];
        disposalNewObj['f086fverificationStatus'] = this.disposalreqDataheader['f086fverificationStatus'];
        disposalNewObj['f086fisEndorsement'] = 0;
        disposalNewObj['disposal-details'] = this.disposalreqList;
        this.DisposalrequisitionService.insertDisposalrequisitionItems(disposalNewObj).subscribe(
            data => {
                this.router.navigate(['assets/disposalrequisition']);
                alert(" Disposal Requisition has been Added Sucessfully ! ");
            }, error => {
                console.log(error);
            });
    }
    deleteDisposalrequisition(object) {
        var cnf = confirm("Do you really want to delete");
        if (cnf == true) {
            var index = this.disposalreqList.indexOf(object);
            if (this.disposalreqList[index]['f087fidDetails'] != undefined) {
                this.DisposalrequisitionService.deleteverificationItems(this.disposalreqList[index]['f087fidDetails']).subscribe(
                    data => {
                    }, error => {
                        console.log(error);
                    });
            }
            else {
                this.DisposalrequisitionService.deleteRequistionItems(this.disposalreqList[index]['f086fidDetails']).subscribe(
                    data => {
                    }, error => {
                        console.log(error);
                    });
            }
            if (index > -1) {
                this.disposalreqList.splice(index, 1);
            }
            this.showIcons();
        }
    }

    adddisposalrequisitionlist() {
        if (this.disposalreqData['f086fidAsset'] == undefined) {
            alert('Select Asset Code');
            return false;
        }
        if (this.disposalreqData['f086fdescription'] == undefined) {
            alert('Enter Description');
            return false;
        }
        if (this.disposalreqData['f086fnetValue'] == undefined) {
            alert('Enter Net Value');
            return false;
        }
        if (this.disposalreqData['f086ftype'] == undefined) {
            alert('Select Type');
            return false;
        }
        var confirmPop = confirm("Do you want to Add?");
        if (confirmPop == false) {
            return false;
        }
        var dataofCurrentRow = this.disposalreqData;
        this.disposalreqData = {};
        this.disposalreqList.push(dataofCurrentRow);
        this.showIcons();
    }
    onlyNumberKey(event) {
        if (event.charCode == 46) {

        } else {
            return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
        }
    }
    maximize() {
        $("#myModal").addClass("modal");
        $("#overlay1").show();
        $(".maximize-icon").addClass("hidden");
        $(".minimize-icon").removeClass("hidden");
        $(".modal-backdrop").show();

    }
    minimize() {
        $("#myModal").removeClass("modal");
        $("#overlay1").hide();
        $(".maximize-icon").removeClass("hidden");
        $(".minimize-icon").addClass("hidden");
        $(".modal-backdrop").hide();
        $('#myModal').show();

    }
}
