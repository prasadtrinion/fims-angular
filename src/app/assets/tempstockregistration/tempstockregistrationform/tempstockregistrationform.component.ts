import { Injector } from '@angular/core';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AssetsService } from '../../service/assets.service'
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { ItemsubcategoryService } from "../../../procurement/service/itemsubcategory.service";
import { SubcategoryService } from "../../service/subcategory.service";
import { StockRegistrationService } from "../../service/stockregistration.service";
import { SetupstoreService } from "../../service/setupstore.service";
import { AssetItemService } from "../../service/assetitem.service";
import { TempassetinformationService } from "../../service/tempassetinformation.service";

import { NgxSpinnerService } from 'ngx-spinner';
@Component({
    selector: 'TempstockRegistrationFormComponent',
    templateUrl: 'tempstockregistrationform.component.html'
})

export class TempstockRegistrationFormComponent implements OnInit {
    assetsform: FormGroup;
    setupList = [];
    stockRegData = {};
    stockRegList = [];
    itemgroupList = [];
    categoryList = [];
    subcategoryList = [];
    assetItemList = [];
    id: number;
    grnInformation = [];
    preregistrationList = [];
    ajaxCount: number;
    valueDate: string;
    categoryName:string;
    subCategoryName:string;
    itemName:string;
    constructor(

        private AssetsService: AssetsService,
        private ItemsubcategoryService: ItemsubcategoryService,
        private SetupstoreService: SetupstoreService,
        private SubcategoryService: SubcategoryService,
        private StockRegistrationService: StockRegistrationService,
        private AssetItemService: AssetItemService,
        private spinner: NgxSpinnerService,
        private TempassetinformationService:TempassetinformationService,
        private route: ActivatedRoute,
        private router: Router

    ) { }

    ngDoCheck() {
        const change = this.ajaxCount;
        if (this.ajaxCount == 0) {
            this.spinner.hide();
            this.ajaxCount = 10;
        }
    }
    ngOnInit() {
        this.ajaxCount++;
        this.SetupstoreService.getActiveItems().subscribe(
            data => {
                this.ajaxCount--;
                this.setupList = data['result'];
            }, error => {
                console.log(error);
            });


        this.getDetails();

    }
    getDetails(){
        this.ajaxCount++;
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

          this.TempassetinformationService.getItems(this.id).subscribe(
            data => {
                this.ajaxCount--;
                this.grnInformation = data['result'][0];
                this.categoryName = this.grnInformation['categoryName'];
                this.subCategoryName = this.grnInformation['subCategoryName'];
                this.itemName = this.grnInformation['itemName'];
                for(var i=0;i<1;i++) {
                    let newpreregisteredObject = {};
                    newpreregisteredObject['categoryName'] = this.categoryName;
                    newpreregisteredObject['subCategoryName'] = this.subCategoryName;
                    newpreregisteredObject['itemName'] = this.itemName;
                    newpreregisteredObject['f133fcategoryType'] = this.grnInformation['f034fitemType'];
   
                    newpreregisteredObject['f134fassetCategory'] = data['result'][0]['f034fitemCategory'];
                    newpreregisteredObject['f134fassetSubCategory'] = data['result'][0]['f034fitemSubCategory'];
                    newpreregisteredObject['f134fassetItem'] = data['result'][0]['f034fidItem'];
                    newpreregisteredObject['f134fquantity'] = this.grnInformation['f034fquantityReceived'];
                    newpreregisteredObject['f134fstore'] = '';
                    this.preregistrationList.push(newpreregisteredObject);
                }
                console.log(this.preregistrationList);
            }, error => {
                console.log(error);
            });

    }
    addAssetinformation() {
        let assetInformationObject = {};
        assetInformationObject['data'] = this.preregistrationList;
        console.log(assetInformationObject);
        this.TempassetinformationService.postStockTempInformation(assetInformationObject).subscribe(
            data => {
                 alert("Stock Registration has been Saved");
                 this.router.navigate(['/assets/tempstockregistration/0']);
            }
        )
    }

}