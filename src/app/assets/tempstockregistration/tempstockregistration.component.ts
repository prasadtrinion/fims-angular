import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { StockRegistrationService } from "../service/stockregistration.service";

@Component({
    selector: 'TempstockRegistration',
    templateUrl: 'tempstockregistration.component.html'
})
export class TempstockRegistrationComponent implements OnInit {
    itemList = [];
    itemData = {};
    id: number;
    constructor(
        private StockRegistrationService: StockRegistrationService,
        private spinner: NgxSpinnerService

    ) { }

    ngOnInit() {
        this.spinner.show();
       this.getDetails();
    }
    getDetails(){
        this.StockRegistrationService.getTempRegistration(0).subscribe(
            data => {
                this.spinner.hide();
                this.itemList = data['result']['data'];
            }, error => {
                console.log(error);
            });
    }
    saveAssetinformation(){
        console.log(this.itemList);
        let updatePreRegistration = {};
        let updatedId = [];
        for(var i=0;i<this.itemList.length;i++) {
            if(this.itemList[i]['selectedstock']==true) {
                updatedId.push(this.itemList[i]['f134fid']);
            }
        }
        updatePreRegistration['status']=1;
        updatePreRegistration['stockId']=updatedId;

        this.StockRegistrationService.updatePreRegistrationForStock(updatePreRegistration).subscribe(
            data => {
                this.spinner.hide();
                console.log(data);
                this.getDetails();

            }
        );

    }
}