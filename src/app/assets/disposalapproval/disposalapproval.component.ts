import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { DisposalapprovalService } from '../service/disposalapproval.service'
import { AlertService } from './../../_services/alert.service';
import { NgxSpinnerService } from 'ngx-spinner';
@Component({
    selector: 'Disposalapproval',
    templateUrl: 'disposalapproval.component.html'
})

export class DisposalapprovalComponent implements OnInit {

    approvalList = [];
    approvalData = {};
    selectAllCheckbox = true;

    constructor(
        private DisposalapprovalService: DisposalapprovalService,
        private AlertService: AlertService,
        private spinner: NgxSpinnerService
    ) { }

    ngOnInit() {
        this.getListData();
    }
    getListData() {
        this.spinner.show();
        this.DisposalapprovalService.getApproveDetail().subscribe(
            data => {
                this.spinner.hide();
                this.approvalList = data['result'];
            }, error => {
                console.log(error);
            });
    }

    disposalapprovalListData() {
        let approveDarrayList = [];
        console.log(this.approvalList);
        for (var i = 0; i < this.approvalList.length; i++) {
            if (this.approvalList[i].approvalStatus == true) {
                approveDarrayList.push(this.approvalList[i].f087fidDetails);
            }
        }
        if (approveDarrayList.length < 1) {
            alert("Select atleast one Disposal to Approve");
            return false;
        }
        var confirmPop = confirm("Do you want to Approve?");
        if (confirmPop == false) {
            return false;
        }
        var approvalObject = {};
        approvalObject['id'] = approveDarrayList;
        approvalObject['status'] = '1';
        // approvalObject['reason']='';
        this.DisposalapprovalService.updateDisposalapprovalItems(approvalObject).subscribe(
            data => {
                this.getListData();
                alert("Approved Sucessfully");
            }, error => {
                console.log(error);
            });

    }
    selectAll() {
        console.log("asdf");
        console.log(this.selectAllCheckbox);
        for (var i = 0; i < this.approvalList.length; i++) {
            this.approvalList[i].f087fapprovedStatus = this.selectAllCheckbox;
        }
        console.log(this.approvalList);
    }

}
