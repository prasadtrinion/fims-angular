import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../material/material.module';
import { Ng4LoadingSpinnerModule } from 'ng4-loading-spinner';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { DataTableModule } from "angular-6-datatable";
import { NguiAutoCompleteModule } from '@ngui/auto-complete';

import { HttpClientModule, HttpClient, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AssetsService } from './service/assets.service';
import { AssetsComponent } from './assets/assets.component';
import { AssetsFormComponent } from './assets/assetsform/assetsform.component';

import { SubcategoryService } from './service/subcategory.service';
import { SubcategoryComponent } from './subcategory/subcategory.component';
import { SubcategoryFormComponent } from './subcategory/subcategoryform/subcategoryform.component';

import { AssettypeService } from './service/assettype.service';
import { AssettypeComponent } from './assettype/assettype.component';
import { AssettypeFormComponent } from './assettype/assettypeform/assettypeform.component';

import { AssetmomentService } from './service/assetmoment.service';
import { AssetmomentComponent } from './assetmoment/assetmoment.component';
import { AssetmomentFormComponent } from './assetmoment/assetmomentform/assetmomentform.component';

import { DisposalaccountcodeComponent } from './disposalaccountcode/disposalaccountcode.component';
import { DisposalaccountcodeFormComponent } from './disposalaccountcode/disposalaccountcodeform/disposalaccountcodeform.component';

import { AssetmaintenanceService } from './service/assetmaintenance.service';
import { AssetmaintenanceComponent } from './assetmaintenance/assetmaintenance.component';
import { AssetmaintenanceFormComponent } from './assetmaintenance/assetmaintenanceform/assetmaintenanceform.component';

import { AssetdepreciationService } from './service/assetdepreciation.service';
import { AssetdepreciationComponent } from './assetdepreciation/assetdepreciation.component';
import { AssetdepreciationFormComponent } from './assetdepreciation/assetdepreciationform/assetdepreciationform.component';

import { AssetdisposalService } from './service/assetdisposal.service';
import { AssetdisposalComponent } from './assetdisposal/assetdisposal.component';
import { AssetdisposalFormComponent } from './assetdisposal/assetdisposalform/assetdisposalform.component';

import { AssetnameService } from './service/assetname.service';
import { AssetnameComponent } from './assetname/assetname.component';
import { AssetnameFormComponent } from './assetname/assetnameform/assetnameform.component';

import { MomentapprovalService } from './service/momentapproval.service';
import { MomentapprovalComponent } from './momentapproval/momentapproval.component';

import { Momentapproval2Service } from './service/momentapproval2.service';
import { Momentapproval2Component } from './movementapproval2/movementapproval2.component';

import { UpdateassetapprovalComponent } from './updateassetapproval/updateassetapproval.component';
import { UpdateassetapprovalService } from './service/updateassetapproval.service';

import { MaintenanceapprovalService } from './service/maintenanceapproval.service';
import { MaintenanceapprovalComponent } from './maintenanceapproval/maintenanceapproval.component';
import { DisposalverificationComponent } from './disposalverification/disposalverification.component';
import { CustomerService } from '../accountsrecivable/service/customer.service';
import { AssetdisposalverifService } from './service/assetdisposalverif.service';

import { RequestoutofstockService } from './service/requestoutofstock.service';

import { StoreService } from './service/store.service';
import { StoreComponent } from './store/store.component';

import { AccountcodeService } from '../generalsetup/service/accountcode.service'
import { DisposalaccountcodeService } from './service/disposalaccountcode.service'

import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { AssetsRoutingModule } from './assets.router.module';

import { AssetinformationService } from './service/assetinformation.service';
import { AssetinformationComponent } from './assetinformation/assetinformation.component';
import { AssetinformationFormComponent } from './assetinformation/assetinformationform/assetinformationform.component';

import { TempassetinformationService } from './service/tempassetinformation.service';
import { TempassetinformationComponent } from './tempassetinformation/tempassetinformation.component';
import { TempassetinformationFormComponent } from './tempassetinformation/tempassetinformationform/tempassetinformationform.component';
import { AssetregistrationFormComponent } from './tempassetinformation/assetregistrationform/assetregistrationform.component';
import { TempstockRegistrationFormComponent } from './tempstockregistration/tempstockregistrationform/tempstockregistrationform.component';
import { TempstockRegistrationComponent } from './tempstockregistration/tempstockregistration.component';

import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { RoomService } from './service/room.service';
import { RoomComponent } from './room/room.component';
import { RoomFormComponent } from './room/roomform/roomform.component';

import { BuildingService } from './service/building.service';
import { BuildingComponent } from './building/building.component';
import { BuildingFormComponent } from './building/buildingform/buildingform.component';

import { JwtInterceptor } from '../_helpers/jwt.interceptors';

import { LocationComponent } from "./location/location.component";
import { LocationFormComponent } from "./location/locationform/locationform.component";

import { LocationService } from "./service/location.service";
import { LocationnzeroComponent } from './locationn/locationnzero/locationnzero.component';
import { LocationnoneComponent } from './locationn/locationnone/locationnone.component';
import { LocationntwoComponent } from './locationn/locationntwo/locationntwo.component';
import { LocationnthreeComponent } from './locationn/locationnthree/locationnthree.component';

import { TypeComponent } from "./type/type.component";
import { TypeFormComponent } from "./type/typeform/typeform.component";
import { TypeService } from "./service/type.service";

import { AssetItemComponent } from "./assetitem/assetitem.component";
import { AssetItemFormComponent } from "./assetitem/assetitemform/assetitemform.component";
import { AssetItemService } from "./service/assetitem.service";

import { SetupstoreComponent } from "./setupstore/setupstore.component";
import { SetupstoreFormComponent} from "./setupstore/setupstoreform/setupstoreform.component";
import { SetupstoreService} from "./service/setupstore.service";
import { DepartmentService } from "../generalsetup/service/department.service";
import { LoannameService } from "../loan/service/loanname.service";


import { UpdateAssetFormComponent } from "./updateasset/updateassetform/updateassetform.component";
import { UpdateAssetComponent } from "./updateasset/updateasset.component";
import { UpdateAssetService } from "./service/updateasset.service";

import { AssetpreregistrationService } from './service/assetpreregistration.service';
import { AssetpreregistrationComponent } from './assetpreregistration/assetpreregistration.component';
import { AssetpreregistrationFormComponent } from './assetpreregistration/assetpreregistrationform/assetpreregistrationform.component';

import { DisposalrequisitionService } from './service/disposalrequisition.service';
import { DisposalrequisitionComponent } from './disposalrequisition/disposalrequisition.component';
import { DisposalrequisitionFormComponent } from './disposalrequisition/disposalrequisitionform/disposalrequisitionform.component';

import { DisposalendorsementService } from './service/disposalendorsement.service';
import { DisposalendorsementComponent } from './disposalendorsement/disposalendorsement.component';

import { DisposalapprovalService } from './service/disposalapproval.service';
import { DisposalapprovalComponent } from './disposalapproval/disposalapproval.component';

import { ApplystockapprovalService } from './service/applystockapproval.service';
import { ApplystockapprovalComponent } from './applystockapproval/applystockapproval.component';

import { PreorderlevelsetupComponent } from "./preorderlevelsetup/preorderlevelsetup.component";
import { PreOrderLevelSetupFormComponent } from "./preorderlevelsetup/preorderlevelsetupform/preorderlevelsetupform.component";
import { PreOrderLevelSetupService } from "./service/preorderlevelsetup.service";

import { ApplyStockComponent } from "./applystock/applystock.component";
import { ApplyStockFormComponent } from "./applystock/applystockform/applystockform.component";
import { ApplyStockService } from "./service/applystock.service";

import { ManualgrnComponent } from "./manualgrn/manualgrn.component";
import { ManualgrnFormComponent } from "./manualgrn/manualgrnform/manualgrnform.component";
import { ManualgrnService } from "./service/manualgrn.service";
import { BlacklisttempService } from "../loan/service/blacklisttemp.service";

import { DepreciationsetupComponent } from "./depreciationsetup/depreciationsetup.component";
import { DepreciationSetupFormComponent } from "./depreciationsetup/depreciationsetupform/depreciationsetupform.component";
import { DepreciationsetupService } from "./service/depreciationsetup.service";

import { StockRegistrationComponent } from "../assets/stockregistration/stockregistration.component";
import { StockRegistrationFormComponent } from "../assets/stockregistration/stockregistrationform/stockregistrationform.component";
import { StockRegistrationService } from "./service/stockregistration.service";

import { StockDistrubutionComponent } from "../assets/stockdistrubution/stockdistrubution.component";
import { StockDistrubutionService } from "./service/stockdistrubution.service";

import { SharedModule } from "../shared/shared.module";
import { NgxQRCodeModule } from 'ngx-qrcode2';
import { NgSelectModule } from '@ng-select/ng-select';
import { importExpr } from '@angular/compiler/src/output/output_ast';

@NgModule({
    imports: [
        CommonModule,
        DataTableModule,
        FormsModule,
        ReactiveFormsModule,
        MaterialModule,
        AssetsRoutingModule,
        HttpClientModule,
        TabsModule.forRoot(),
        Ng2SearchPipeModule,
        HttpClientModule,
        FontAwesomeModule,
        NguiAutoCompleteModule,
        Ng4LoadingSpinnerModule,
        NgSelectModule,
        SharedModule,
        NgxQRCodeModule
    ],
    exports: [],
    declarations: [
        ApplystockapprovalComponent,
        ManualgrnComponent,
        ManualgrnFormComponent,
        LocationnzeroComponent,
        LocationnoneComponent,
        LocationntwoComponent,
        LocationnthreeComponent,
        DepreciationsetupComponent,
        AssetsComponent,
        AssetsFormComponent,
        SubcategoryComponent,
        SubcategoryFormComponent,
        AssettypeComponent,
        AssettypeFormComponent,
        DisposalaccountcodeComponent,
        DisposalaccountcodeFormComponent,
        AssetmomentComponent,
        AssetmomentFormComponent,
        MomentapprovalComponent,
        UpdateassetapprovalComponent,
        AssetmaintenanceComponent,
        AssetmaintenanceFormComponent,
        MaintenanceapprovalComponent,
        AssetdepreciationComponent,
        AssetdepreciationFormComponent,
        AssetdisposalComponent,
        AssetdisposalFormComponent,
        DisposalverificationComponent,
        AssetnameComponent,
        AssetnameFormComponent,
        StoreComponent,
        AssetinformationFormComponent,
        AssetinformationComponent,
        TempassetinformationComponent,
        TempassetinformationFormComponent,
        StockDistrubutionComponent,
        RoomComponent,
        RoomFormComponent,
        BuildingComponent,
        BuildingFormComponent,
        LocationComponent,
        LocationFormComponent,
        TypeComponent,
        TypeFormComponent,
        AssetItemComponent,
        AssetItemFormComponent,
        UpdateAssetFormComponent,
        UpdateAssetComponent,
        AssetpreregistrationComponent,
        AssetpreregistrationFormComponent,
        DisposalrequisitionFormComponent,
        DisposalrequisitionComponent,
        DisposalendorsementComponent,
        DisposalapprovalComponent,
        Momentapproval2Component,
        AssetregistrationFormComponent,
        PreorderlevelsetupComponent,
        PreOrderLevelSetupFormComponent,
        ApplyStockComponent,
        ApplyStockFormComponent,
        SetupstoreComponent,
        SetupstoreFormComponent,
        StockRegistrationComponent,
        StockRegistrationFormComponent,
        TempstockRegistrationFormComponent,
        TempstockRegistrationComponent,
        DepreciationSetupFormComponent,
    ],
    providers: [
        ApplystockapprovalService,
        DepreciationsetupService,
        ManualgrnService,
        BlacklisttempService,
        AssetsService,
        SubcategoryService,
        AssettypeService,
        AccountcodeService,
        TempassetinformationService,
        DisposalaccountcodeService,
        AssetmomentService,
        MomentapprovalService,
        UpdateassetapprovalService,
        AssetmaintenanceService,
        MaintenanceapprovalService,
        AssetdepreciationService,
        AssetdisposalService,
        AssetdisposalverifService,
        RequestoutofstockService,
        AssetnameService,
        StoreService,
        CustomerService,
        LocationService,
        AssetinformationService,
        RoomService,
        TypeService,
        AssetItemService,
        UpdateAssetService,
        SetupstoreService,
        BuildingService,
        AssetpreregistrationService,
        DisposalrequisitionService,
        DisposalendorsementService,
        DisposalapprovalService,
        PreOrderLevelSetupService,
        ApplyStockService,
        Momentapproval2Service,
        DepartmentService,
        StockRegistrationService,
        LoannameService,
        StockDistrubutionService,
       
        {
            provide: HTTP_INTERCEPTORS,
            useClass: JwtInterceptor,
            multi: true
        }

    ],
})
export class AssetsModule {
    ngOnInit() { }
}
