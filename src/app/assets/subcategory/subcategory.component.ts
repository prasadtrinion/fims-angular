import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SubcategoryService } from '../service/subcategory.service'
import { AccountcodeService } from '../../generalsetup/service/accountcode.service'
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'Subcategory',
    templateUrl: 'subcategory.component.html'
})
export class SubcategoryComponent implements OnInit {
    subcategoryList = [];
    subcategoryData = {};
    id: number;
    constructor(
        private SubcategoryService: SubcategoryService,
        private spinner: NgxSpinnerService
    ) { }

    ngOnInit() {
        this.spinner.show();
        this.SubcategoryService.getItems().subscribe(
            data => {
                this.spinner.hide();
                this.subcategoryList = data['result']['data'];
            }, error => {
                console.log(error);
            });
    }
}