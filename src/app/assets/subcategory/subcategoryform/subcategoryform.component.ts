import { Injector } from '@angular/core';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SubcategoryService } from '../../service/subcategory.service'
import { AssetsService } from '../../service/assets.service'
import { ItemsubcategoryService } from "../../../procurement/service/itemsubcategory.service";
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'SubcategoryFormComponent',
    templateUrl: 'subcategoryform.component.html'
})
export class SubcategoryFormComponent implements OnInit {
    subcategoryList = [];
    subcategoryData = {};
    assetsList = [];
    categoryList = [];
    assetssList = [];
    itemgroupList = [];
    ajaxCount: number;
    id: number;
    constructor(
        private SubcategoryService: SubcategoryService,
        private AssetsService: AssetsService,
        private ItemsubcategoryService: ItemsubcategoryService,
        private spinner: NgxSpinnerService,
        private route: ActivatedRoute,
        private router: Router
    ) { }

    ngDoCheck() {
        const change = this.ajaxCount;
        if (this.ajaxCount == 0) {
            this.spinner.hide();
        }
    }
    ngOnInit() {
        this.ajaxCount = 0
        this.spinner.show();
        this.subcategoryData['f089fstatus'] = 1;

        // Item Group Dropdown 
        this.ajaxCount++;
        this.ItemsubcategoryService.getActiveItems().subscribe(
            data => {
                this.ajaxCount--;
                this.itemgroupList = data['result']['data'];
            }, error => {
                console.log(error);
            });
        //Category Dropdown
        this.ajaxCount++;
        this.AssetsService.getActiveItems().subscribe(
            data => {
                this.ajaxCount--;
                this.categoryList = data['result']['data'];
            }, error => {
                console.log(error);
            });
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.ajaxCount++;
            this.SubcategoryService.getItemsDetail(this.id).subscribe(
                data => {
                    this.ajaxCount--;
                    this.subcategoryData = data['result'][0];
                    this.subcategoryData['f089fstatus'] = parseInt(this.subcategoryData['f089fstatus']);
                }, error => {
                    console.log(error);
                });
        } else {
            var type = {};
            type['type'] = 'ASC';
            this.SubcategoryService.getRunningNumber(type).subscribe(
                data => {
                    this.subcategoryData['f089faccountNumber'] = data['number'];
                }, error => {
                    console.log(error);
                });
        }
    }
    addSubcategory() {
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        if (this.id > 0) {
            this.SubcategoryService.updateSubcategoryItems(this.subcategoryData, this.id).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert('Asset Sub Category Code Already Exist');
                    }
                    else {
                        this.router.navigate(['assets/subcategory']);
                        alert("Asset Sub Category has been Updated Successfully")
                    }
                }, error => {
                    alert('Assets Sub Category Code Already Exist');
                    return false;
                });
        } else {
            this.SubcategoryService.insertSubcategoryItems(this.subcategoryData).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert('Asset Sub Category Code Already Exist');
                    }
                    else {
                        this.router.navigate(['assets/subcategory']);
                        alert("Asset Sub Category has been Added Successfully")
                    }
                }, error => {
                    console.log(error);
                });
        }
    }
    onlyNumberKey(event) {
        return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
    }
}