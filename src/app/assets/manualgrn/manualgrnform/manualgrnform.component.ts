import { Injector } from '@angular/core';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ManualgrnService } from '../../service/manualgrn.service'
import { AssetsService } from '../../service/assets.service'
import { StaffService } from "../../../loan/service/staff.service";
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { FundService } from '../../../generalsetup/service/fund.service';
import { AccountcodeService } from '../../../generalsetup/service/accountcode.service'
import { ActivitycodeService } from '../../../generalsetup/service/activitycode.service';
import { DepartmentService } from '../../../generalsetup/service/department.service';
import { AssetinformationService } from '../../service/assetinformation.service'
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'ManualgrnFormComponent',
    templateUrl: 'manualgrnform.component.html'
})

export class ManualgrnFormComponent implements OnInit {
    disposalreqform: FormGroup;
    manualList = [];
    deleteList = [];
    manualData = {};
    manualDataheader = {};
    staffList = [];
    assetssList = [];
    ajaxCount: number;
    id: number;
    DeptList = [];
    accountcodeList = [];
    fundList = [];
    activitycodeList = [];
    assetinformationList = [];
    disposalreqList = [];
    ftype = [];
    showLastRow: boolean;
    showLastRowMinus: boolean;
    showLastRowPlus: boolean;
    listshowLastRowMinus: boolean;
    listshowLastRowPlus: boolean;
    constructor(
        private ManualgrnService: ManualgrnService,
        private AssetsService: AssetsService,
        private StaffService: StaffService,
        private route: ActivatedRoute,
        private router: Router,
        private DepartmentService: DepartmentService,
        private spinner: NgxSpinnerService,
        private ActivitycodeService: ActivitycodeService,
        private AccountcodeService: AccountcodeService,
        private FundService: FundService,
        private AssetinformationService: AssetinformationService


    ) { }
    showIcons() {
        // this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.disposalreqList.length > 0) {
            this.showLastRowMinus = true;
            this.showLastRowPlus = true;
            this.listshowLastRowMinus = true;
        }

        if (this.disposalreqList.length > 1) {
            if (this.showLastRow == false) {
                this.listshowLastRowPlus = false;
                this.listshowLastRowMinus = true;
                this.showLastRowPlus = true;

            } else {
                this.listshowLastRowPlus = true;
                this.listshowLastRowMinus = false;

            }
        }
        if (this.disposalreqList.length == 1 && this.showLastRow == false) {
            this.listshowLastRowMinus = true;
            this.listshowLastRowPlus = false;

        }
        if (this.disposalreqList.length == 1 && this.showLastRow == true) {
            this.listshowLastRowMinus = false;
            this.listshowLastRowPlus = true;

        }
        if (this.disposalreqList.length == 0 && this.showLastRow == false) {
            this.showLastRowMinus = false;
            this.showLastRowPlus = true;
        }
        if (this.disposalreqList.length == 0 && this.showLastRow == true) {
            this.showLastRowMinus = true;
            this.showLastRowPlus = false;
        }
    }
    deletemodule() {
        this.showLastRow = true;
        this.showIcons();
    }
    showNewRow() {
        var confirmPop = confirm("Do you want to Add?");
        if (confirmPop == false) {
            return false;
        }
        this.showLastRow = false;
        this.showIcons();
    }
    ngDoCheck() {
        const change = this.ajaxCount;
        if (this.ajaxCount == 0) {
            this.editFunction();
            this.spinner.hide();
            this.ajaxCount = 10;
        }
    }
    editFunction() {
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.ajaxCount++;
            this.ManualgrnService.getItemsDetail(this.id).subscribe(
                data => {
                    this.ajaxCount--;
                    this.manualDataheader['f121forderType'] = data['result']['data'][0]['f121forderType'];
                    this.manualDataheader['f121freferenceNumber'] = data['result']['data'][0]['f121freferenceNumber'];
                    this.manualDataheader['f121fapproveDate'] = data['result']['data'][0]['f121fapproveDate'];
                    this.manualDataheader['f121fdonor'] = data['result']['data'][0]['f121fdonor'];
                    this.manualDataheader['f121fvalue'] = data['result']['data'][0]['f121fvalue'];
                    this.disposalreqList = data['result']['data'];
                    this.showIcons();
                }, error => {
                    console.log(error);
                });
        }
    }
    ngOnInit() {
        this.ajaxCount = 0
        this.spinner.show();
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id < 1) {
            this.showLastRow = false;
            this.showLastRowMinus = false;
            this.showLastRowPlus = true;
        } else {
            this.showLastRow = true;

        }
        let stafftypeobj = {};
        stafftypeobj['name'] = 'Asset';
        this.ftype.push(stafftypeobj);
        stafftypeobj = {};
        stafftypeobj['name'] = 'Procurement';
        this.ftype.push(stafftypeobj);
        this.ajaxCount = 0;
        this.manualDataheader['f121forderType'] = 'DONOR';
        // assets category
        this.ajaxCount++;
        this.AssetsService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.assetssList = data['result']['data'];
            }, error => {
                console.log(error);
            });

        // staff dropdown
        this.ajaxCount++;
        this.StaffService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.staffList = data['result']['data'];
            }, error => {
                console.log(error);
            });

        //Department dropdown
        this.ajaxCount++;
        this.DepartmentService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.DeptList = data['result']['data'];
            }
        );


        //Activity dropdown
        this.ajaxCount++;
        this.ActivitycodeService.getItemsByLevel().subscribe(
            data => {
                this.ajaxCount--;
                this.activitycodeList = data['result']['data'];
            }
        );


        //Account dropdown
        this.ajaxCount++;
        this.AccountcodeService.getItemsByLevel().subscribe(
            data => {
                this.ajaxCount--;
                this.accountcodeList = data['result']['data'];
            }
        );

        //Fund dropdown
        this.ajaxCount++;
        this.FundService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.fundList = data['result']['data'];
            }
        );

        this.AssetinformationService.getItems().subscribe(
            data => {
                this.assetinformationList = data['data']['data'];
            }, error => {
                console.log(error);
            });
        this.showIcons();
    }

    adddisposalrequisitionlist() {

        this.disposalreqList.push(this.manualData);
        this.manualData = {};
    }
    ConvertToFloat(val) {
        return parseFloat(val);
    }
    getAmount() {
        if (parseFloat(this.manualData['f121fquantity']) && parseFloat(this.manualData['f121fprice'])) {
            let FinalTotal = parseFloat(this.manualData['f121fquantity']) * parseFloat(this.manualData['f121fprice'])
            this.manualData['f121ftotal'] = FinalTotal;
        } else {
            this.manualData['f121ftotal'] = 0;

        }
        // this.getRefNumber();
        this.showIcons();
    }
    getAmount1() {
        var Total = 0;
        for (var i = 0; i < this.disposalreqList.length; i++) {
            let FinalTotal = parseFloat(this.disposalreqList[i]['f121fquantity']) * parseFloat(this.disposalreqList[i]['f121fprice'])
            this.disposalreqList[i]['f121ftotal'] = FinalTotal;
            Total = Total + this.disposalreqList[i]['f121ftotal'];
        }
        this.manualDataheader['f121fvalue'] = this.ConvertToFloat(Total.toFixed(2));
    }
    saveManualgrn() {

        if (this.manualDataheader['f121fapproveDate'] == undefined) {
            alert("Select Approval Date");
            return false;
        }
        if (this.manualDataheader['f121fdonor'] == undefined) {
            alert("Enter Donor");
            return false;
        }
        if (this.showLastRow == false) {
            var addactivities = this.addmanualgrnlist();
            if (addactivities = true) {
                return false;
            }
        }
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        let assetdisposalObject = {};
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            assetdisposalObject['f121fid'] = this.id;
        }
        assetdisposalObject['f121forderType'] = this.manualDataheader['f121forderType'];
        assetdisposalObject['f121fapproveDate'] = this.manualDataheader['f121fapproveDate'];
        assetdisposalObject['f121fdonor'] = this.manualDataheader['f121fdonor'];
        assetdisposalObject['f121fvalue'] = this.manualDataheader['f121fvalue'];
        assetdisposalObject['grnDonation-details'] = this.disposalreqList;
        for (var i = 0; i < assetdisposalObject['grnDonation-details'].length; i++) {
            assetdisposalObject['grnDonation-details'][i]['f121fprice'] = this.ConvertToFloat(assetdisposalObject['grnDonation-details'][i]['f121fprice']).toFixed(2);
            assetdisposalObject['grnDonation-details'][i]['f121ftotal'] = this.ConvertToFloat(assetdisposalObject['grnDonation-details'][i]['f121ftotal']).toFixed(2);
        }
        this.ManualgrnService.insertManualgrnItems(assetdisposalObject).subscribe(
            data => {
                this.router.navigate(['assets/manualgrn']);
                if (this.id > 0) {
                    alert("GRN DONATION has been Updated Sucessfully!");
                }
                else {
                    alert("GRN DONATION has been Added Sucessfully!");
                }
            }, error => {
                console.log(error);
            });

    }
    deleteManualgrn(object) {
        var cnf = confirm("Do you really want to delete");
        if (cnf == true) {
            var index = this.disposalreqList.indexOf(object);
            this.deleteList.push(this.disposalreqList[index]['f121fidDetails']);

            this.ManualgrnService.deleteitems(this.disposalreqList[index]['f121fidDetails']).subscribe(
                data => {
                }, error => {
                    console.log(error);
                });
            if (index > -1) {
                this.disposalreqList.splice(index, 1);
            }
            this.showIcons();
            this.getAmount1();
        }
    }

    addmanualgrnlist() {

        if (this.manualData['f121fassetType'] == undefined) {
            alert("Select Asset Type");
            return false;
        }
        if (this.manualData['f121ffundCode'] == undefined) {
            alert("Select Fund");
            return false;
        }
        if (this.manualData['f121factivityCode'] == undefined) {
            alert("Select Activity Code");
            return false;
        }
        if (this.manualData['f121fdepartmentCode'] == undefined) {
            alert("Select Department");
            return false;
        }
        if (this.manualData['f121faccountCode'] == undefined) {
            alert("Select Account Code");
            return false;
        }
        if (this.manualData['f121fquantity'] == undefined) {
            alert("Enter Quantity");
            return false;
        }
        if (this.manualData['f121fprice'] == undefined) {
            alert("Enter Price");
            return false;
        }
        var confirmPop = confirm("Do you want to Add?");
        if (confirmPop == false) {
            return false;
        }
        var dataofCurrentRow = this.manualData;
        this.manualData = {};
        this.disposalreqList.push(dataofCurrentRow);
        this.showIcons();
        this.getAmount1();
    }
    onlyNumberKey(event) {
        if (event.charCode == 46) {

        } else {
            return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
        }
    }
}
