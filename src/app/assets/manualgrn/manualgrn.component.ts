import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ManualgrnService } from '../service/manualgrn.service'
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'Manualgrn',
    templateUrl: 'manualgrn.component.html'
})
export class ManualgrnComponent implements OnInit {
    manualgrnList = [];
    manualgrnData = {};
    manualgrnListNew = [];
    id: number;
    constructor(
        private ManualgrnService: ManualgrnService,
        private spinner: NgxSpinnerService,
    ) { }
    ngOnInit() {
        this.spinner.show();
        this.ManualgrnService.getItems().subscribe(
            data => {
                this.spinner.hide();
                this.manualgrnList = data['data']['data'];
            }, error => {
                console.log(error);
            });
    }
}