import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { TypeService } from "../service/type.service";
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'Type',
    templateUrl: 'type.component.html'
})
export class TypeComponent implements OnInit {
    typeList = [];
    typeData = {};
    id: number;
    constructor(
        private TypeService: TypeService,
        private spinner: NgxSpinnerService

    ) { }

    ngOnInit() {
        this.spinner.show();
        this.TypeService.getItems().subscribe(
            data => {
                this.spinner.hide();
                this.typeList = data['result']['data'];
            }, error => {
                console.log(error);
            });
    }
}