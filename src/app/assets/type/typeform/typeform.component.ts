import { Injector } from '@angular/core';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TypeService } from "../../service/type.service";
import { AssetsService } from '../../service/assets.service'
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { ItemsubcategoryService } from "../../../procurement/service/itemsubcategory.service";
import { SubcategoryService } from "../../service/subcategory.service";
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'TypeFormComponent',
    templateUrl: 'typeform.component.html'
})

export class TypeFormComponent implements OnInit {
    assetsform: FormGroup;
    typeList = [];
    typeData = {};
    itemgroupList = [];
    categoryList = [];
    subcategoryList = [];
    id: number;
    ajaxCount: number;
    constructor(

        private AssetsService: AssetsService,
        private ItemsubcategoryService: ItemsubcategoryService,
        private TypeService: TypeService,
        private SubcategoryService: SubcategoryService,
        private spinner: NgxSpinnerService,
        private route: ActivatedRoute,
        private router: Router

    ) { }

    ngDoCheck() {
        const change = this.ajaxCount;
        if (this.ajaxCount == 0) {
            this.spinner.hide();
        }
    }
    ngOnInit() {
        this.ajaxCount = 0
        this.spinner.show();
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.assetsform = new FormGroup({
            utility: new FormControl('', Validators.required),

        });
        this.typeData['f090fstatus'] = 1;

        // Item Group Dropdown 
        this.ajaxCount++;
        this.ItemsubcategoryService.getActiveItems().subscribe(
            data => {
                this.ajaxCount--;
                this.itemgroupList = data['result']['data'];
            }, error => {
                console.log(error);
            });

        // Category Dropdown 
        this.ajaxCount++;
        this.AssetsService.getActiveItems().subscribe(
            data => {
                this.ajaxCount--;
                this.categoryList = data['result']['data'];
            }, error => {
                console.log(error);
            });

        // SubCategory Dropdown
        this.ajaxCount++;
        this.SubcategoryService.getActiveItems().subscribe(
            data => {
                this.ajaxCount--;
                this.subcategoryList = data['result']['data'];
            }, error => {
                console.log(error);
            });

        if (this.id > 0) {
            this.ajaxCount++;
            this.TypeService.getItemsDetail(this.id).subscribe(
                data => {
                    this.ajaxCount--;
                    this.typeData = data['result'][0];
                    this.typeData['f090fstatus'] = parseInt(this.typeData['f090fstatus']);
                }, error => {
                    console.log(error);
                });
        }
    }

    addType() {
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        if (this.id > 0) {
            this.TypeService.updateTypeItems(this.typeData, this.id).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert('Asset Sub Category and Asset Type Code Already Exist');
                    }
                    else {
                        this.router.navigate(['assets/type']);
                        alert("Asset Item Type has been Updated Successfully")
                    }
                }, error => {
                    alert('Asset Sub Category and Asset Type Code Already Exist');
                    return false;
                });
        } else {
            this.TypeService.insertTypeItems(this.typeData).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert('Asset Sub Category and Asset Type Code Already Exist');
                    }
                    else {
                        this.router.navigate(['assets/type']);
                        alert("Asset Item Type has been Added Successfully")
                    }
                }, error => {
                    return false;
                });
        }
    }

}