import { Injector } from '@angular/core';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AssettypeService } from '../../service/assettype.service';
import { AccountcodeService } from '../../../generalsetup/service/accountcode.service';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'AssettypeFormComponent',
    templateUrl: 'assettypeform.component.html'
})

export class AssettypeFormComponent implements OnInit {
    assetstypeform: FormGroup;
    assettypeList = [];
    assettypeData = {};
    accountcodeList = [];
    id: number;
    ajaxCount: number;
    constructor(

        private AssettypeService: AssettypeService,
        private AccountcodeService: AccountcodeService,
        private spinner: NgxSpinnerService,
        private route: ActivatedRoute,
        private router: Router

    ) { }
    ngDoCheck() {
        const change = this.ajaxCount;
        if (this.ajaxCount == 0) {
            this.spinner.hide();
        }
    }
    ngOnInit() {
        this.ajaxCount = 0
        this.spinner.show();
        this.assettypeData['f086fstatus'] = 1;
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        // account code
        this.ajaxCount++;
        this.AccountcodeService.getItemsByLevel().subscribe(
            data => {
                this.ajaxCount--;
                this.accountcodeList = data['result']['data'];
            }, error => {
                console.log(error);
            });
        this.assetstypeform = new FormGroup({
            assettype: new FormControl('', Validators.required),
        });
        if (this.id > 0) {
            this.ajaxCount++;
            this.AssettypeService.getItemsDetail(this.id).subscribe(
                data => {
                    this.ajaxCount--;
                    this.assettypeData = data['result'][0];
                    this.assettypeData['f090fstatus'] = parseInt(this.assettypeData['f090fstatus']);
                }, error => {
                    console.log(error);
                });
        }
    }
    addAssettype() {
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.AssettypeService.updateAssettypeItems(this.assettypeData, this.id).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert('Disposal Code Already Exist');
                    }
                    else {
                        this.router.navigate(['assets/assettype']);
                        alert("Disposal Type has been Updated Sucessfully !!");
                    }
                }, error => {
                    console.log(error);
                    alert('Disposal Code Already Exist');
                });

        } else {

            this.AssettypeService.insertAssettypeItems(this.assettypeData).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert('Disposal Code Already Exist');
                    }
                    else {
                        this.router.navigate(['assets/assettype']);
                        alert("Disposal Type has been Added Sucessfully !!");
                    }
                }, error => {
                    console.log(error);
                });
        }
    }
}