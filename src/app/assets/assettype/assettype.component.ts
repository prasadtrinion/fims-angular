import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AssettypeService } from '../service/assettype.service'
import { NgxSpinnerService } from 'ngx-spinner';


@Component({
    selector: 'Assettype',
    templateUrl: 'assettype.component.html'
})
export class AssettypeComponent implements OnInit {
    assettypeList = [];
    assettypeData = {};
    id: number;
    constructor(
        private AssettypeService: AssettypeService,
        private spinner: NgxSpinnerService
    ) { }

    ngOnInit() {
        this.spinner.show();
        this.AssettypeService.getItems().subscribe(
            data => {
                this.spinner.hide();
                this.assettypeList = data['data']['data'];
            }, error => {
                console.log(error);
            });
    }
}