import { Injector } from '@angular/core';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TypeService } from "../../service/type.service";
import { AssetsService } from '../../service/assets.service'
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { ItemsubcategoryService } from "../../../procurement/service/itemsubcategory.service";
import { SubcategoryService } from "../../service/subcategory.service";
import { AssetItemService } from "../../service/assetitem.service";
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'AssetItemFormComponent',
    templateUrl: 'assetitemform.component.html'
})

export class AssetItemFormComponent implements OnInit {
    assetsform: FormGroup;
    typeList = [];
    itemData = {};
    itemList = [];
    itemgroupList = [];
    categoryList = [];
    subcategoryList = [];
    id: number;
    categoryId: number;
    ajaxCount: number;
    constructor(

        private AssetsService: AssetsService,
        private ItemsubcategoryService: ItemsubcategoryService,
        private TypeService: TypeService,
        private SubcategoryService: SubcategoryService,
        private AssetItemService: AssetItemService,
        private spinner: NgxSpinnerService,
        private route: ActivatedRoute,
        private router: Router

    ) { }

    ngDoCheck() {
        const change = this.ajaxCount;
        if (this.ajaxCount == 0) {
            this.spinner.hide();
        }
    }
    ngOnInit() {
        this.ajaxCount = 0
        this.spinner.show();
        this.assetsform = new FormGroup({
            utility: new FormControl('', Validators.required),

        });
        this.itemData['f088fstatus'] = 1;

        // Item Group Dropdown 
        this.ajaxCount++;
        this.ItemsubcategoryService.getActiveItems().subscribe(
            data => {
                this.ajaxCount--;
                this.itemgroupList = data['result']['data'];
            }, error => {
                console.log(error);
            });
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        if (this.id > 0) {
            this.ajaxCount++;
            this.AssetItemService.getItemsDetail(this.id).subscribe(
                data => {
                    this.ajaxCount--;
                    this.itemData = data['result'][0];
                    this.itemData['f088fstatus'] = parseInt(this.itemData['f088fstatus']);
                    this.getAssetCategory();
                    this.getAssetSubCategory();
                    this.getAssetType();
                }, error => {
                    console.log(error);
                });

        }
    }

    getAssetCategory() {
        // Category Dropdown 
        this.ajaxCount++;
        this.categoryId = this.itemData['f088fidItemGroup'];
        this.AssetsService.BasedonSubCategory(this.categoryId).subscribe(
            data => {
                this.ajaxCount--;
                this.categoryList = data['result'];
            }, error => {
                console.log(error);
            });

    }
    getAssetSubCategory() {
        // Category Dropdown 
        this.ajaxCount++;
        this.categoryId = this.itemData['f088fidCategory'];
        this.SubcategoryService.getBasedonCategoryUrl(this.categoryId).subscribe(
            data => {
                this.ajaxCount--;
                this.subcategoryList = data['result'];
            }, error => {
                console.log(error);
            });

    }
    getAssetType() {
        this.ajaxCount++;
        this.categoryId = this.itemData['f088fidSubCategory'];
        this.TypeService.getBasedonSubCat(this.categoryId).subscribe(
            data => {
                this.ajaxCount--;
                this.typeList = data['result'];
            }, error => {
                console.log(error);
            });
    }
    addItem() {

        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }

        if (this.id > 0) {
            this.AssetItemService.updateAssetItemItems(this.itemData, this.id).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert('Asset Item Code Already Exist');
                    }
                    else {
                        this.router.navigate(['assets/assetitem']);
                        alert("Asset Item has been Updated Successfully")
                    }

                }, error => {
                    alert('Asset Item Code Already Exist');
                    return false;
                });

        } else {

            this.AssetItemService.insertAssetItemItems(this.itemData).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert('Asset Item Code Already Exist');
                    }
                    else {
                        this.router.navigate(['assets/assetitem']);
                        alert("Asset Item has been Added Successfully")
                    }

                }, error => {
                    console.log(error);
                });

        }
    }

}