import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AssetItemService } from "../service/assetitem.service";
import { NgxSpinnerService } from 'ngx-spinner';


@Component({
    selector: 'AssetItem',
    templateUrl: 'assetitem.component.html'
})
export class AssetItemComponent implements OnInit {
    itemList = [];
    itemData = {};
    id: number;
    constructor(
        private AssetItemService: AssetItemService,
        private spinner: NgxSpinnerService

    ) { }

    ngOnInit() {
        this.spinner.show();
        this.AssetItemService.getItems().subscribe(
            data => {
                this.spinner.hide();
                this.itemList = data['result']['data'];
            }, error => {
                console.log(error);
            });
    }
}