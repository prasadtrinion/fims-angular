import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AssetsService } from '../../service/assets.service'
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { UpdateAssetService } from "../../service/updateasset.service";
import { SubcategoryService } from "../../service/subcategory.service";
import { DepartmentService } from '../../../generalsetup/service/department.service';
import { TypeService } from "../../service/type.service";
import { AccountcodeService } from '../../../generalsetup/service/accountcode.service'
import { AssetinformationService } from '../../service/assetinformation.service'
import { AssetItemService } from "../../service/assetitem.service";
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'UpdateAssetFormComponent',
    templateUrl: 'updateassetform.component.html'
})

export class UpdateAssetFormComponent implements OnInit {
    assetsform: FormGroup;
    updateassetList = [];
    updateassetsData = {};
    itemgroupList = [];
    categoryList = [];
    subcategoryList = [];
    costcenterList = [];
    typeList = [];
    accountcodeList = [];
    DeptList = [];
    campusList = [];
    blockList = [];
    buildingList = [];
    roomList = [];
    itemList = [];
    viewDisabled: boolean;
    id: number;
    idview: number;
    ajaxCount: number;
    constructor(

        private AssetsService: AssetsService,
        private SubcategoryService: SubcategoryService,
        private UpdateAssetService: UpdateAssetService,
        private DepartmentService: DepartmentService,
        private TypeService: TypeService,
        private route: ActivatedRoute,
        private router: Router,
        private AccountcodeService: AccountcodeService,
        private AssetinformationService: AssetinformationService,
        private AssetItemService: AssetItemService,
        private spinner: NgxSpinnerService,

    ) { }

    ngDoCheck() {
        const change = this.ajaxCount;
        if (this.ajaxCount == 0) {
            this.spinner.hide();
        }
    }

    ngOnInit() {
        this.ajaxCount = 0
        this.spinner.show();
        this.assetsform = new FormGroup({
            utility: new FormControl('', Validators.required),

        });
        this.updateassetsData['f085fAcquisitionDate'] = new Date().toISOString().substr(0, 10);
        this.updateassetsData['f085fapproveDate'] = new Date().toISOString().substr(0, 10);
        this.updateassetsData['f085fexpireDate'] = new Date().toISOString().substr(0, 10);

        this.updateassetsData['f091fapprovalStatus'] = 1;
        this.viewDisabled = false;
        this.ajaxCount++;
        // Category Dropdown 
        this.AssetsService.getActiveItems().subscribe(
            data => {
                this.ajaxCount--;
                this.categoryList = data['result']['data'];
            }, error => {
                console.log(error);
            });
        this.ajaxCount++;
        this.DepartmentService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.DeptList = data['result']['data'];
            }, error => {
                console.log(error);
            });
        this.ajaxCount++;
        this.AssetinformationService.getlocationItems(0).subscribe(
            data => {
                this.ajaxCount--;
                this.campusList = data['result']['data'];
            }, error => {
                console.log(error);
            });

        this.ajaxCount++;
        this.AssetinformationService.getlocationItems(2).subscribe(
            data => {
                this.ajaxCount--;
                this.blockList = data['result']['data'];
            }, error => {
                console.log(error);
            });
        this.ajaxCount++;
        this.AssetinformationService.getlocationItems(1).subscribe(
            data => {
                this.ajaxCount--;
                this.buildingList = data['result']['data'];
            }, error => {
                console.log(error);
            });
        this.ajaxCount++;
        this.AssetinformationService.getlocationItems(3).subscribe(
            data => {
                this.ajaxCount--;
                this.roomList = data['result']['data'];
            }, error => {
                console.log(error);
            });

        this.ajaxCount++;
        this.AccountcodeService.getItemsByLevel().subscribe(
            data => {
                this.ajaxCount--;
                this.accountcodeList = data['result']['data'];
            }, error => {
                console.log(error);
            });


        // type Dropdown 
        this.ajaxCount++;
        this.TypeService.getActiveItems().subscribe(
            data => {
                this.ajaxCount--;
                this.typeList = data['result']['data'];
            }, error => {
                console.log(error);
            });
        this.ajaxCount++;
        this.AssetItemService.getActiveItems().subscribe(
            data => {
                this.ajaxCount--;
                this.itemList = data['result']['data'];
            }, error => {
                console.log(error);
            });

        // SubCategory Dropdown 
        this.ajaxCount++;
        this.SubcategoryService.getActiveItems().subscribe(
            data => {
                this.ajaxCount--;
                this.subcategoryList = data['result']['data'];
            }, error => {
                console.log(error);
            });
        // CostCenter Dropdown 
        this.ajaxCount++;
        this.DepartmentService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.costcenterList = data['result']['data'];
            }, error => {
                console.log(error);
            });

        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.route.paramMap.subscribe((data) => this.idview = + data.get('idview'));
        if (this.id > 0) {
            this.ajaxCount++;
            this.UpdateAssetService.getItemsDetail(this.id).subscribe(
                data => {
                    this.ajaxCount--;
                    this.updateassetsData = data['result']['data'][0];
                    this.updateassetsData['f085fsubCategory'] = parseInt(this.updateassetsData['f085fsubCategory']);
                    this.updateassetsData['f087fcategoryDiscription'] = parseInt(this.updateassetsData['f087fcategoryDiscription']);
                    if (data['result']['data'][0]['f085fapprovalStatus'] == 1 || data['result']['data'][0]['f085fapprovalStatus'] == 2) {
                        this.viewDisabled = true;
                        setTimeout(
                            function () {
                                $("#target input,select").prop("disabled", true);
                                $("#target1 input,select").prop("disabled", true);
                            }, 2000);
                    }
                    if (this.idview == 2) {
                        this.viewDisabled = true;

                        $("#target input,select").prop("disabled", true);
                        $("#target1 input,select").prop("disabled", true);
                    }
                }, error => {
                    console.log(error);
                });
        }
    }
    dateComparison() {

        let startDate = Date.parse(this.updateassetsData['f085fexpireDate']);
        let endDate = Date.parse(this.updateassetsData['f085fapproveDate']);
        if (startDate < endDate) {
            alert("Expire Date cannot be less than Approve Date!");
            this.updateassetsData['f085fexpireDate'] = "";
        }

    }
    addAssetinformation() {
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        this.updateassetsData['f085fassetCode'] = '--';
        this.updateassetsData['f085fstatus'] = 3;
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.UpdateAssetService.updateUpdateAssetItems(this.updateassetsData, this.id).subscribe(
                data => {
                    if (data['status'] == 409) {
                    }
                    else {
                        this.router.navigate(['assets/updateasset']);
                        alert("Updated Asset has been Updated Sucessfully !!")
                    }

                }, error => {
                    console.log(error);
                });
        }
    }
    onlyNumberKey(event) {
        if (event.charCode == 46) {

        } else {
            return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
        }
    }
}