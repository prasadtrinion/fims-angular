import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UpdateAssetService } from "../service/updateasset.service";
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'UpdateAsset',
    templateUrl: 'updateasset.component.html'
})
export class UpdateAssetComponent implements OnInit {
    assetsList = [];
    assetsData = {};
    id: number;
    constructor(
        private UpdateAssetService: UpdateAssetService,
        private spinner: NgxSpinnerService,
    ) { }

    ngOnInit() {
        this.spinner.show();
        this.UpdateAssetService.getItems().subscribe(
            data => {
                this.spinner.hide();
                let activityData = [];
                activityData = data['result']['data'];
                for (var i = 0; i < activityData.length; i++) {
                    if (activityData[i]['f085fapprovalStatus'] == 0) {
                        activityData[i]['f085fapprovalStatus'] = 'Pending';
                    } else if (activityData[i]['f085fapprovalStatus'] == 1) {
                        activityData[i]['f085fapprovalStatus'] = 'Approved';
                    } else {
                        activityData[i]['f085fapprovalStatus'] = 'Rejected';
                    }
                }
                this.assetsList = activityData;
            }, error => {
                console.log(error);
            });
    }
    
}