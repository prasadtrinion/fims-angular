
import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { ApplystockapprovalService } from "../service/applystockapproval.service";
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'ApplystockapprovalComponent',
    templateUrl: 'applystockapproval.component.html'
})

export class ApplystockapprovalComponent implements OnInit {
    rejecttwoList = [];
    applystockapprovalList = [];
    applystockapprovalData = {};
    selectAllCheckbox = true;
    reason: string;

    constructor(
        private ApplystockapprovalService: ApplystockapprovalService,
        private spinner: NgxSpinnerService
    ) { }

    ngOnInit() {
        this.getListData();
    }
    getListData() {
        this.spinner.show();
        this.ApplystockapprovalService.getItemsDetail(0).subscribe(
            data => {
                this.spinner.hide();
                this.applystockapprovalList = data['result'];
                // this.selectAll();
            }, error => {
                console.log(error);
            });
    }
    applystockapprovalListData() {
        console.log(this.applystockapprovalList);
        var approvaloneIds = [];
        for (var i = 0; i < this.applystockapprovalList.length; i++) {
            if (this.applystockapprovalList[i].f099fapprovalStatus == true) {
                approvaloneIds.push(this.applystockapprovalList[i].f099fid);
            }
        }
        if (approvaloneIds.length > 0) {

        } else {
            alert(" select atleast one Applystock to Approve");
            return false;
        }

        var confirmPop = confirm("Do you want to Approve?");
        if (confirmPop == false) {
            return false;
        }
        var approvaloneUpdate = {};
        approvaloneUpdate['status'] = 1;

        approvaloneUpdate['id'] = approvaloneIds;
        approvaloneUpdate['reason'] = "";
        this.ApplystockapprovalService.updateApplystockapprovalItems(approvaloneUpdate).subscribe(
            data => {
                this.applystockapprovalData['reason'] = '';

                alert("Apply stock has been approved successfully");

                this.getListData();
            }, error => {
                console.log(error);
            });
    }
    updateApplystockRejectedItems() {
        console.log(this.applystockapprovalList);
        var rejectoneIds = [];

        for (var i = 0; i < this.applystockapprovalList.length; i++) {
            if (this.applystockapprovalList[i].f099fapprovalStatus == true) {
                rejectoneIds.push(this.applystockapprovalList[i].f099fid);
            }
        }
        if (rejectoneIds.length > 0) {

        } else {
            alert(" Select atleast one Applystock approval to Reject");
            return false;
        }
        if (this.applystockapprovalData['reason'] == undefined) {
            alert(' Enter the Description');
            return false;
        }
        console.log(this.applystockapprovalData['reason']);

        if (this.applystockapprovalData['reason'] == '' || this.applystockapprovalData['reason'] == undefined) {
            alert("Enter the Description for Rejection !");
        } else {


            var confirmPop = confirm("Do you want to Reject?");
            if (confirmPop == false) {
                return false;
            }
            var rejecttwoUpdate = {};
            rejecttwoUpdate['status'] = 2;
            rejecttwoUpdate['id'] = rejectoneIds;
            rejecttwoUpdate['reason'] = this.applystockapprovalData['reason'];

            this.ApplystockapprovalService.updateApplystockRejectedItems(rejecttwoUpdate).subscribe(
                data => {
                    alert("Applystock approval has been Rejected ! ");
                    this.applystockapprovalData['reason'] = '';

                    this.getListData();
                }, error => {
                    console.log(error);
                });
        }
    }
}
