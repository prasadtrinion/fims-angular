import {Injector} from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { AssetdisposalverifService } from '../service/assetdisposalverif.service'
import { AlertService } from './../../_services/alert.service';
@Component({
    selector: 'Disposalverification',
    templateUrl: 'disposalverification.component.html'
})

export class DisposalverificationComponent implements OnInit {
   
    disposalList =  [];
    disposalData = {};
    disposalaccountcodeData = {};
    selectAllCheckbox = true;

    constructor(
        
        private AssetdisposalverifService: AssetdisposalverifService,
        private AlertService: AlertService
    ) { }

    ngOnInit() {    
       this.getListData();
    }
    getListData(){
        this.AssetdisposalverifService.getItemsDetailForDisposal().subscribe(
            data => {
                this.disposalList = data['result'];
                this.selectAll();
        }, error => {
            console.log(error);
        });
    }
    
    approvalListData() {
        console.log(this.disposalList);
        var approvaloneIds = [];
        for (var i = 0; i < this.disposalList.length; i++) {
            if(this.disposalList[i].f086fstatus==true) {
                approvaloneIds.push(this.disposalList[i].f086fidDetails);
            }
        }
        //f087fid
        var approvaloneUpdate = {};
        approvaloneUpdate['f087fmeetingNumber'] = this.disposalaccountcodeData['f087fmeetingNumber'];
        approvaloneUpdate['f087fstatus'] = 0;
        approvaloneUpdate['f087ftype'] = this.disposalaccountcodeData['f087ftype'];;
        approvaloneUpdate['f087fremarks'] = this.disposalaccountcodeData['f087fremarks'];;
        approvaloneUpdate['disposal-details'] = approvaloneIds;
        this.AssetdisposalverifService.insertAssetdisposalverifItems(approvaloneUpdate).subscribe(
            data => {
                this.AlertService.success(" Updated Sucessfully ! ");
                //this.getListData();
        }, error => {
            console.log(error);
        });
    }
    // rejectAssetdisposalverifListData(){
    //     console.log(this.disposalList);
    //     var rejectoneIds = [];
    //     for (var i = 0; i < this.disposalList.length; i++) {
    //         if(this.disposalList[i].f017fapprovedStatus==true) {
    //             rejectoneIds.push(this.disposalList[i].f070fid);
    //         }
    //     }
    //     var rejectoneUpdate = {};
    //     rejectoneUpdate['id'] = rejectoneIds;
    //     rejectoneUpdate['reason'] = this.disposalData['f063fapprove1Reson'];
    //     this.AssetdisposalverifService.rejectAssetdisposalverif(rejectoneUpdate).subscribe(
    //         data => {
    //             this.getListData();
    //     }, error => {
    //         console.log(error);
    //     });
    
    // }
    // selectAll() {
    //     console.log("asdf");
    //     console.log(this.selectAllCheckbox);
    //     for (var i = 0; i < this.disposalList.length; i++) {
    //             this.disposalList[i].f017fapprovedStatus = this.selectAllCheckbox;
    //     }
    //     console.log(this.disposalList);
    //   }

      selectAll() {
        console.log("asdf");
        console.log(this.selectAllCheckbox);
        for (var i = 0; i < this.disposalList.length; i++) {
                this.disposalList[i].f086fstatus = false;
        }
        console.log(this.disposalList);
      }

    }
