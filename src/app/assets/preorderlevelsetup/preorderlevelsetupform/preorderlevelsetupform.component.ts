import { Injector } from '@angular/core';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TypeService } from "../../service/type.service";
import { AssetsService } from '../../service/assets.service'
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { SubcategoryService } from "../../service/subcategory.service";
import { AssetItemService } from "../../service/assetitem.service";
import { PreOrderLevelSetupService } from "../../service/preorderlevelsetup.service";
import { NgxSpinnerService } from 'ngx-spinner';
@Component({
    selector: 'PreOrderLevelSetup',
    templateUrl: 'preorderlevelsetupform.component.html'
})

export class PreOrderLevelSetupFormComponent implements OnInit {
    assetsform: FormGroup;
    typeList = [];
    preorderData = {};
    preorderList = [];
    categoryList = [];
    subcategoryList = [];
    assetitemList = [];
    id: number;
    ajaxCount: number;
    constructor(

        private AssetsService: AssetsService,
        private TypeService: TypeService,
        private SubcategoryService: SubcategoryService,
        private AssetItemService: AssetItemService,
        private PreOrderLevelSetupService: PreOrderLevelSetupService,
        private spinner: NgxSpinnerService,
        private route: ActivatedRoute,
        private router: Router

    ) { }

    ngDoCheck() {
        const change = this.ajaxCount;
        if (this.ajaxCount == 0) {
            this.spinner.hide();
        }
    }
    ngOnInit() {
        this.ajaxCount = 0
        this.spinner.show();
        this.assetsform = new FormGroup({
            utility: new FormControl('', Validators.required),

        });
        this.preorderData['f098fstatus'] = 1;

        // Category Dropdown 
        this.ajaxCount++;
        this.AssetsService.getActiveItems().subscribe(
            data => {
                this.ajaxCount--;
                this.categoryList = data['result']['data'];
            }, error => {
                console.log(error);
            });

        // SubCategory Dropdown 
        this.ajaxCount++;
        this.SubcategoryService.getActiveItems().subscribe(
            data => {
                this.ajaxCount--;
                this.subcategoryList = data['result']['data'];
            }, error => {
                console.log(error);
            });

        // Type Dropdown
        this.ajaxCount++;
        this.TypeService.getActiveItems().subscribe(
            data => {
                this.ajaxCount--;
                this.typeList = data['result']['data'];
            }, error => {
                console.log(error);
            });
        this.ajaxCount++;
        this.AssetItemService.getActiveItems().subscribe(
            data => {
                this.ajaxCount--;
                this.assetitemList = data['result']['data'];
            }, error => {
                console.log(error);
            });
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        if (this.id > 0) {
            this.ajaxCount++;
            this.PreOrderLevelSetupService.getItemsDetail(this.id).subscribe(
                data => {
                    this.ajaxCount--;
                    this.preorderData = data['result'][0];
                    this.preorderData['f098fstatus'] = parseInt(this.preorderData['f098fstatus']);
                }, error => {
                    console.log(error);
                });
        }
    }
    addPreorderlevel() {
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        if (this.id > 0) {
            this.PreOrderLevelSetupService.updatePreOrderLevelItems(this.preorderData, this.id).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert('Code Stock Already Exist');
                    }
                    else {
                        this.router.navigate(['assets/preorderlevelsetup']);
                        alert("PreOrder Stock Setup has been Updated Sucessfully !!");
                    }
                }, error => {
                    console.log(error);
                    alert('Code Stock Already Exist');
                });

        } else {

            this.PreOrderLevelSetupService.insertPreOrderLevelItems(this.preorderData).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert('Code Stock Already Exist');
                    }
                    else {
                        this.router.navigate(['assets/preorderlevelsetup']);
                        alert("PreOrder Stock Setup has been Added Sucessfully !!");
                    }
                }, error => {
                    console.log(error);
                });

        }
    }
    onlyNumberKey(event) {
        if (event.charCode == 46) {

        } else {
            return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
        }
    }
}