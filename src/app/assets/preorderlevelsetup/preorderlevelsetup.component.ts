import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { PreOrderLevelSetupService } from "../service/preorderlevelsetup.service";
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'PreOrderLevelSetup',
    templateUrl: 'preorderlevelsetup.component.html'
})
export class PreorderlevelsetupComponent implements OnInit {
    preorderData = {};
    preorderList = [];
    id: number;
    constructor(
        private PreOrderLevelSetupService: PreOrderLevelSetupService,
        private spinner: NgxSpinnerService
    ) { }

    ngOnInit() {
        this.spinner.show();
        this.PreOrderLevelSetupService.getItems().subscribe(
            data => {
                this.spinner.hide();
                this.preorderList = data['result']['data'];
            }, error => {
                console.log(error);
            });
    }
}