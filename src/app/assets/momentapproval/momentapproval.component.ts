import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { MomentapprovalService } from '../service/momentapproval.service'
import { AlertService } from './../../_services/alert.service';
import { NgxSpinnerService } from 'ngx-spinner';
@Component({
    selector: 'Momentapproval',
    templateUrl: 'momentapproval.component.html'
})

export class MomentapprovalComponent implements OnInit {

    momentapprovalList = [];
    momentapprovalData = {};
    rejectData = {};
    selectAllCheckbox = true;

    constructor(

        private MomentapprovalService: MomentapprovalService,
        private spinner: NgxSpinnerService,
    ) { }

    ngOnInit() {
        this.getListData();
    }
    getListData() {
        this.spinner.show();
        this.MomentapprovalService.getItemsDetail(0).subscribe(
            data => {
                this.spinner.hide();
                this.momentapprovalList = data['result'];
                for (var i = 0; i < this.momentapprovalList.length; i++) {
                    this.momentapprovalList[i]['approvalStatus'] = false;
                }
            }, error => {
                console.log(error);
            });
    }

    momentapprovalListData() {
        console.log(this.momentapprovalList);
        var approvaloneIds = [];
        for (var i = 0; i < this.momentapprovalList.length; i++) {
            if (this.momentapprovalList[i]['approvalStatus'] == true) {
                approvaloneIds.push(this.momentapprovalList[i]['f087fid']);
            }
        }
        if (approvaloneIds.length < 1) {
            alert("select atleast one Type to Approve");
            return false;
        }
        var confirmPop = confirm("Do you want to Approve?");
        if (confirmPop == false) {
            return false;
        }
        var approvaloneUpdate = {};
        approvaloneUpdate['id'] = approvaloneIds;
        approvaloneUpdate['status'] = 1;
        approvaloneUpdate['reason'] = ''
        this.MomentapprovalService.updateMomentapprovalItems(approvaloneUpdate).subscribe(
            data => {
                alert("Approved Sucessfully")
                this.getListData();
                this.rejectData['reason'] = '';
            }, error => {
                console.log(error);
            });
    }
    rejectmomentapprovalListData() {
        var approvaloneIds = [];

        console.log(this.momentapprovalList);
        for (var i = 0; i < this.momentapprovalList.length; i++) {
            if (this.momentapprovalList[i]['approvalStatus'] == true) {
                approvaloneIds.push(this.momentapprovalList[i]['f087fid']);
            }
        }
        if (approvaloneIds.length < 1) {
            alert("select atleast one Type to Reject");
            return false;
        }
        if (this.rejectData['reason'] == undefined) {
            alert('Enter the Description');
            return false;
        }
        var confirmPop = confirm("Do you want to Reject?");
        if (confirmPop == false) {
            return false;
        }
        var rejectoneUpdate = {};
        rejectoneUpdate['id'] = approvaloneIds;
        rejectoneUpdate['status'] = 2;
        rejectoneUpdate['reason'] = this.rejectData['reason'];
        this.MomentapprovalService.updateMomentapprovalItems(rejectoneUpdate).subscribe(
            data => {
                this.getListData();
                this.rejectData['reason'] = '';
                alert("Rejected Successfully");

            }, error => {
                console.log(error);
            });

    }

}
