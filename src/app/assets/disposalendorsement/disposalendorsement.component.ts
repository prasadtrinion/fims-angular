import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DisposalendorsementService } from '../service/disposalendorsement.service'
import { DisposalrequisitionService } from '../service/disposalrequisition.service'
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'Disposalendorsement',
    templateUrl: 'disposalendorsement.component.html'
})
export class DisposalendorsementComponent implements OnInit {
    endorsementList = [];
    endorsementData = {};
    disposalreqList = [];
    id: number;
    constructor(
        private DisposalendorsementService: DisposalendorsementService,
        private DisposalrequisitionService: DisposalrequisitionService,
        private spinner: NgxSpinnerService

    ) { }

    ngOnInit() {
        this.spinner.show();
        this.DisposalendorsementService.getItems().subscribe(
            data => {
                this.spinner.hide();
                this.disposalreqList = data['data']['data'];
            }, error => {
                console.log(error);
            });
    }
}