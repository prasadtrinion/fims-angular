import { Injector } from '@angular/core';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { SetupstoreService } from "../../service/setupstore.service";
import { DepartmentService } from "../../../generalsetup/service/department.service";
import { LoannameService } from "../../../loan/service/loanname.service";
import { StaffService } from "../../../loan/service/staff.service";
import { UserService } from "../../../generalsetup/service/user.service";
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'SetupstoreFormComponent',
    templateUrl: 'setupstoreform.component.html'
})

export class SetupstoreFormComponent implements OnInit {
    setupstoreform: FormGroup;
    setupstoreList = [];
    setupstoreData = {};
    departmentList = [];
    staffList = [];
    staffList1 = [];
    id: number;
    ajaxCount: number;
    constructor(

        private route: ActivatedRoute,
        private router: Router,
        private SetupstoreService: SetupstoreService,
        private DepartmentService: DepartmentService,
        private LoannameService: LoannameService,
        private StaffService: StaffService,
        private UserService: UserService,
        private spinner: NgxSpinnerService,
    ) { }

    ngDoCheck() {
        const change = this.ajaxCount;
        if (this.ajaxCount == 0) {
            this.spinner.hide();
        }
    }
    ngOnInit() {
        this.ajaxCount = 0
        this.spinner.show();
        this.setupstoreData['f097fstatus'] = 1;

        this.setupstoreform = new FormGroup({
            setupstore: new FormControl('', Validators.required),
        });


        //  Dropdown 
        this.ajaxCount++;
        this.DepartmentService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.departmentList = data['result']['data'];
            }, error => {
                console.log(error);
            });


        // staff Dropdown 
        this.ajaxCount++;
        this.StaffService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.staffList = data['result']['data'];
            }, error => {
                console.log(error);
            });

        //user dropdown
        this.ajaxCount++;
        this.UserService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.staffList1 = data['result']['data'];
            }, error => {
                console.log(error);
            });

        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        if (this.id > 0) {
            this.ajaxCount++;
            this.SetupstoreService.getItemsDetail(this.id).subscribe(
                data => {
                    this.ajaxCount--;
                    this.setupstoreData = data['result'][0];
                    this.setupstoreData['f097fstatus'] = parseInt(this.setupstoreData['f097fstatus']);
                }, error => {
                    console.log(error);
                });
        }
    }
    addSetupstore() {
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.SetupstoreService.updateSetupstoreItems(this.setupstoreData, this.id).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert('Store Code Already Exist');
                    }
                    else {
                        this.router.navigate(['assets/setupstore']);
                        alert("Setup Store has been Updated Successfully");
                    }
                }, error => {
                    alert('Store Code Already Exist');
                    console.log(error);
                });


        } else {

            this.SetupstoreService.insertSetupstoreItems(this.setupstoreData).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert('Store Code Already Exist');
                    }
                    else {
                        this.router.navigate(['assets/setupstore']);
                        alert("Setup Store has been Added Successfully");
                    }
                }, error => {
                    console.log(error);
                });

        }
    }

}