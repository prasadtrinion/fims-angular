import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SetupstoreService } from '../service/setupstore.service'
import { NgxSpinnerService } from 'ngx-spinner';
@Component({
    selector: 'Setupstore',
    templateUrl: 'setupstore.component.html'
})
export class SetupstoreComponent implements OnInit {
    setupstoreList = [];
    setupstoreData = {};
    id: number;
    constructor(
        private SetupstoreService: SetupstoreService,
        private spinner: NgxSpinnerService
    ) { }

    ngOnInit() {
        this.spinner.show();
        this.SetupstoreService.getItems().subscribe(
            data => {
                this.spinner.hide();
                this.setupstoreList = data['result']['data'];
            }, error => {
                console.log(error);
            });
    }
}