import { Injector } from '@angular/core';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApplyStockService } from "../../service/applystock.service";
import { AssetsService } from '../../service/assets.service'
import { SubcategoryService } from "../../service/subcategory.service";
import { AssetItemService } from "../../service/assetitem.service";
import { DepartmentService } from "../../../generalsetup/service/department.service";
import { StaffService } from "../../../loan/service/staff.service";
import { TypeService } from "../../service/type.service";
import { StockDistrubutionService } from "../../service/stockdistrubution.service";
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'ApplyStockFormComponent',
    templateUrl: 'applystockform.component.html'
})

export class ApplyStockFormComponent implements OnInit {
    assetdisposalform: FormGroup;
    aplystockList = [];
    aplystockData = {};
    categoryList = [];
    distrubuteList = [];
    subcategoryList = [];
    typeList = [];
    designation = '';
    assetitemList = [];
    smartStaffList = [];
    applystockDataheader = {};
    blacklistempList = [];
    deleteList = [];
    departmentList = [];
    ajaxCount: number;
    id: number;
    viewid: number;
    currentEmpDesignation
    idview: number;
    showLastRow: boolean;
    showLastRowMinus: boolean;
    showLastRowPlus: boolean;
    listshowLastRowMinus: boolean;
    listshowLastRowPlus: boolean;
    showReference: boolean = false;
    viewDisabled: boolean;
    constructor(
        private ApplyStockService: ApplyStockService,
        private AssetsService: AssetsService,
        private SubcategoryService: SubcategoryService,
        private AssetItemService: AssetItemService,
        private TypeService: TypeService,
        private DepartmentService: DepartmentService,
        private StaffService: StaffService,
        private spinner: NgxSpinnerService,
        private StockDistrubutionService: StockDistrubutionService,
        private route: ActivatedRoute,
        private router: Router

    ) { }

    ngDoCheck() {
        this.showIcons();
        const change = this.ajaxCount;
        if (this.ajaxCount == 0) {
            this.spinner.hide();
            this.ajaxCount = 10;
        }
    }
    deletemodule() {
        this.showLastRow = true;
        this.showIcons();
    }

    showIcons() {
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.route.paramMap.subscribe((data) => this.viewid = + data.get('viewid'));
        if (this.aplystockList.length > 0) {
            this.showLastRowMinus = true;
            this.listshowLastRowMinus = true;
            this.showLastRowPlus = true;
        }
        if (this.aplystockList.length > 1) {
            if (this.showLastRow == false) {
                this.listshowLastRowPlus = false;
                this.listshowLastRowMinus = true;
                this.showLastRowPlus = true;

            } else {
                this.listshowLastRowPlus = true;
                this.listshowLastRowMinus = false;

            }
        }
        if (this.aplystockList.length == 1 && this.showLastRow == false) {
            this.listshowLastRowMinus = true;
            this.listshowLastRowPlus = false;

        }
        if (this.aplystockList.length == 1 && this.showLastRow == true) {
            this.listshowLastRowMinus = false;
            this.listshowLastRowPlus = true;

        }
        if (this.aplystockList.length == 0 && this.showLastRow == false) {
            this.showLastRowMinus = false;
            this.showLastRowPlus = true;
        }
        if (this.aplystockList.length == 0 && this.showLastRow == true) {
            this.showLastRowMinus = true;
            this.showLastRowPlus = false;
        }
        if (this.viewid == 2 || this.idview == 2 || this.viewDisabled == true) {
            this.listshowLastRowPlus = false;
            this.showLastRowMinus = false;
        }
        if (this.viewDisabled == true) {
            this.listshowLastRowPlus = false;
            this.listshowLastRowMinus = false;
        }


    }
    showNewRow() {
        var confirmPop = confirm("Do you want to Add?");
        if (confirmPop == false) {
            return false;
        }
        this.showLastRow = false;
        this.showIcons();
    }
    editFunction() {
        this.ajaxCount++;
        // this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.showReference = true;
            this.ApplyStockService.getItemsDetail(this.id).subscribe(
                data => {
                    this.ajaxCount--;
                    this.applystockDataheader = data['result'];

                    this.applystockDataheader['f099fapplicantName'] = data['result']['f099fapplicantName'];
                    this.applystockDataheader['f099fposition'] = "1"; //data['result'][0]['f052fidStaff'];
                    this.applystockDataheader['f099fdepartment'] = data['result'][0]['f099fdepartment'];
                    this.applystockDataheader['f099frequiredDate'] = data['result'][0]['f099frequiredDate'];
                    this.applystockDataheader['f099fstatus'] = data['result']['f099fstatus'];

                    this.aplystockList = data['result'];
                    // for (var i = 0; i < this.cashadvanceappList.length; i++) {
                    //     this.cashadvanceappList[i]['f052fitem'] = parseInt(this.cashadvanceappList[i]['f052fitem']).toString();
                    // }
                    // this.getAllpurchaseGlList()
                    // this.cashadvanceappList = data['cash-details'];
                    //console.log(this.cashadvanceappDataheader);
                    this.route.paramMap.subscribe((data) => this.idview = + data.get('idview'));
                    this.route.paramMap.subscribe((data) => this.viewid = + data.get('viewid'));
                    if (data['result'][0]['f099fapprovalStatus'] == 1 || data['result'][0]['f099fapprovalStatus'] == 2 || data['result'][0]['f099fapprovalStatus'] == 3) {
                        this.viewDisabled = true;
                        setTimeout(function () {

                            $("#target input,ng-select,select").prop("disabled", true);
                            $("#target1 input,ng-select,select").prop("disabled", true);

                        }, 3000);
                    }
                    if (this.idview == 2) {
                        this.viewDisabled = true;
                        setTimeout(function () {

                            $("#target input,ng-select,select").prop("disabled", true);
                            $("#target1 input,ng-select,select").prop("disabled", true);

                        }, 3000);

                        this.showIcons();
                    }
                    if (this.viewid == 2) {
                        this.viewDisabled = true;
                        setTimeout(function () {

                            $("#target input,ng-select,select").prop("disabled", true);
                            $("#target1 input,ng-select,select").prop("disabled", true);

                        }, 3000);

                        this.showIcons();
                    }

                }, error => {
                    //console.log(error);
                });
        }

    }
    ngOnInit() {
        this.ajaxCount = 0;
        this.spinner.show();
        this.viewDisabled = false;
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.route.paramMap.subscribe((data) => this.viewid = + data.get('viewid'));
        this.editFunction();
        if (this.id < 1) {
            this.showLastRow = false;
            this.showLastRowMinus = false;
            this.showLastRowPlus = true;
        } else {
            this.showLastRow = true;

        }
        this.aplystockData['f043fempName'] = localStorage.getItem('staffId');
        this.ajaxCount = 0;
        this.ajaxCount++;
        this.StaffService.getAllItems().subscribe(
            data => {
                this.ajaxCount--;
                this.smartStaffList = data['result']['data'];
                var staffId = localStorage.getItem('staffId');
                this.applystockDataheader['f099fapplicantName'] = parseInt(staffId);

                // looping of all employee
                for (var i = 0; i < this.smartStaffList.length; i++) {
                    if (this.smartStaffList[i]['f034fstaffId'] == staffId) {
                        this.designation = this.smartStaffList[i]['employeedesignationname'];
                    }
                }
            }, error => {
                console.log(error);
            });


        //  Dropdown 
        this.ajaxCount++;
        this.DepartmentService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.departmentList = data['result']['data'];
            }, error => {
                console.log(error);
            });

        // Category Dropdown 
        this.ajaxCount++;
        this.AssetsService.getActiveItems().subscribe(
            data => {
                this.ajaxCount--;
                this.categoryList = data['result']['data'];
            }, error => {
                console.log(error);
            });

        // SubCategory Dropdown 
        this.ajaxCount++;
        this.SubcategoryService.getActiveItems().subscribe(
            data => {
                this.ajaxCount--;
                this.subcategoryList = data['result']['data'];
            }, error => {
                console.log(error);
            });

        // Type Dropdown 
        this.ajaxCount++;
        this.TypeService.getActiveItems().subscribe(
            data => {
                this.ajaxCount--;
                this.typeList = data['result']['data'];
            }, error => {
                console.log(error);
            });

        // Item Dropdown 
        this.ajaxCount++;
        this.AssetItemService.getActiveItems().subscribe(
            data => {
                this.ajaxCount--;
                this.assetitemList = data['result']['data'];
            }, error => {
                console.log(error);
            });
        this.showIcons();
    }
    Ditrubutestock() {
        var confirmPop = confirm("Do you want to Distribute?");
        if (confirmPop == false) {
            return false;
        }
        this.StockDistrubutionService.getItemsDetail(this.id).subscribe(
            data => {
                alert("Stock Distributed Sucessfully");
                this.router.navigate(['assets/stockdistrubution']);
            }, error => {
                console.log(error);
            });
    }

    saveApplystock() {
        if (this.applystockDataheader['f099fdepartment'] == undefined) {
            alert(" Select Apply Stock from Store");
            return false;
        }
        if (this.applystockDataheader['f099frequiredDate'] == undefined) {
            alert(" Select Required Date");
            return false;
        }
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        if (this.showLastRow == false) {
            var addactivities = this.addapplystock();
            if (addactivities = true) {
                return false;
            }
        }
        let assetdisposalObject = {};
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            assetdisposalObject['f099fid'] = this.id;
        }

        assetdisposalObject['f099fapplicantName'] = this.applystockDataheader['f099fapplicantName'];
        assetdisposalObject['f099fposition'] = "1";//this.applystockDataheader['f086fdisposalName'];
        assetdisposalObject['f099fdepartment'] = this.applystockDataheader['f099fdepartment'];
        assetdisposalObject['f099frequiredDate'] = this.applystockDataheader['f099frequiredDate'];

        assetdisposalObject['stock-details'] = this.aplystockList;
        if (this.id > 0) {
            this.ApplyStockService.updateApplystockItems(assetdisposalObject, this.id).subscribe(
                data => {

                    this.router.navigate(['assets/applystock']);
                    alert("Apply Stock has been Updated Successfully !!");

                }, error => {
                    console.log(error);
                });
        } else {
            this.ApplyStockService.insertApplystockItems(assetdisposalObject).subscribe(
                data => {
                    this.router.navigate(['assets/applystock']);
                    alert("Apply Stock has been Added Successfully !!");
                }, error => {
                    console.log(error);
                });
        }
    }

    deleteapplystock(object) {
        var cnf = confirm("Do you really want to delete");
        if (cnf == true) {
            var index = this.aplystockList.indexOf(object);
            this.deleteList.push(this.aplystockList[index]['f099fidDetails']);
            let deleteObject = {};
            deleteObject['id'] = this.deleteList;
            this.ApplyStockService.deleteItems(deleteObject).subscribe(
                data => {
                }, error => {
                    console.log(error);
                });
            if (index > -1) {
                this.aplystockList.splice(index, 1);
            }
            this.showIcons();
        }
    }

    addapplystock() {
        if (this.aplystockData['f099fkodStock'] == undefined) {
            alert(" Enter the Code Stock");
            return false;
        }

        if (this.aplystockData['f099fidCategory'] == undefined) {
            alert(" Select Asset Category");
            return false;
        }
        if (this.aplystockData['f099fidSubCategory'] == undefined) {
            alert(" Select Asset Sub Category");
            return false;
        }
        if (this.aplystockData['f099fidAssetType'] == undefined) {
            alert(" Select Assete Item Type");
            return false;
        }
        if (this.aplystockData['f099fidItem'] == undefined) {
            alert(" Select Asset Item");
            return false;
        }
        if (this.aplystockData['f099fcurrentStock'] == undefined) {
            alert(" Enter the Current Stock");
            return false;
        }
        if (this.aplystockData['f099frequestedQty'] == undefined) {
            alert(" Enter the Requested Stock");
            return false;
        }
        var confirmPop = confirm("Do you want to Add?");
        var dataofCurrentRow = this.aplystockData;
        this.aplystockData = {};
        this.aplystockList.push(dataofCurrentRow);
        this.showIcons();
    }
    onlyNumberKey(event) {
        if (event.charCode == 46) {

        } else {
            return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
        }
    }
    maximize() {
        $("#myModal").addClass("modal");
        $("#overlay1").show();
        $(".maximize-icon").addClass("hidden");
        $(".minimize-icon").removeClass("hidden");
        $(".modal-backdrop").show();

    }
    minimize() {
        $("#myModal").removeClass("modal");
        $("#overlay1").hide();
        $(".maximize-icon").removeClass("hidden");
        $(".minimize-icon").addClass("hidden");
        $(".modal-backdrop").hide();
        $('#myModal').show();

    }
}
