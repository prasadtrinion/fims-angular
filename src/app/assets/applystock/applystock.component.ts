import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ApplyStockService } from "../service/applystock.service";
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'ApplyStockComponent',
    templateUrl: 'applystock.component.html'
})
export class ApplyStockComponent implements OnInit {
    applystockList = [];
    applystockData = {};
    id: number;
    constructor(
        private ApplyStockService: ApplyStockService,
        private spinner: NgxSpinnerService,
    ) { }

    ngOnInit() {
        this.spinner.show();
        this.ApplyStockService.getItems().subscribe(
            data => {
                this.spinner.hide();
                let applystockData = [];
                applystockData = data['result']['data'];
                for (var i = 0; i < applystockData.length; i++) {
                    if (applystockData[i]['f099fapprovalStatus'] == 1) {
                        applystockData[i]['f099fapprovalStatus'] = 'Approved';
                    } else if (applystockData[i]['f099fapprovalStatus'] == 2) {
                        applystockData[i]['f099fapprovalStatus'] = 'Rejected';
                    } else if (applystockData[i]['f099fapprovalStatus'] == 3) {
                        applystockData[i]['f099fapprovalStatus'] = 'Distributed';
                    } else {
                        applystockData[i]['f099fapprovalStatus'] = 'Pending';
                    }
                }
                this.applystockList = applystockData;
            }, error => {
                console.log(error);
            });
    }
}