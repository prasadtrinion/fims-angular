import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DisposalaccountcodeService } from '../service/disposalaccountcode.service'
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'Disposalaccountcode',
    templateUrl: 'disposalaccountcode.component.html'
})
export class DisposalaccountcodeComponent implements OnInit {
    disposalaccountcodeList = [];
    disposalaccountcodeData = {};
    id: number;
    constructor(
        private DisposalaccountcodeService: DisposalaccountcodeService,
        private spinner: NgxSpinnerService
    ) { }

    ngOnInit() {
        this.spinner.show();
        this.DisposalaccountcodeService.getItems().subscribe(
            data => {
                this.spinner.hide();
                this.disposalaccountcodeList = data['result']['data'];
            }, error => {
                console.log(error);
            });
    }
}