import { Injector } from '@angular/core';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DisposalaccountcodeService } from '../../service/disposalaccountcode.service'
import { AccountcodeService } from '../../../generalsetup/service/accountcode.service'
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { AssettypeService } from "../../service/assettype.service";
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'DisposalaccountcodeFormComponent',
    templateUrl: 'disposalaccountcodeform.component.html'
})

export class DisposalaccountcodeFormComponent implements OnInit {
    disposalaccountcodeform: FormGroup;
    disposalaccountcodeList = [];
    disposalaccountcodeData = {};
    accountcodeList = [];
    disposalList = [];
    id: number;
    ajaxCount: number;
    constructor(

        private DisposalaccountcodeService: DisposalaccountcodeService,
        private AccountcodeService: AccountcodeService,
        private AssettypeService: AssettypeService,
        private spinner: NgxSpinnerService,
        private route: ActivatedRoute,
        private router: Router

    ) { }

    ngDoCheck() {
        const change = this.ajaxCount;
        if (this.ajaxCount == 0) {
            this.spinner.hide();
        }
    }

    ngOnInit() {
        this.ajaxCount = 0
        this.spinner.show();
        this.disposalaccountcodeData['f033fstatus'] = 1;
        this.disposalaccountcodeform = new FormGroup({
            utility: new FormControl('', Validators.required),

        });
        //account code
        this.ajaxCount++;
        this.AccountcodeService.getItemsByLevel().subscribe(
            data => {
                this.ajaxCount--;
                this.accountcodeList = data['result']['data'];
            }, error => {
                console.log(error);
            });
        //DEFMS
        this.ajaxCount++;
        this.AssettypeService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.disposalList = data['data']['data'];
            }, error => {
                console.log(error);

            });

        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        if (this.id > 0) {
            this.ajaxCount++;
            this.DisposalaccountcodeService.getItemsDetail(this.id).subscribe(
                data => {
                    this.ajaxCount--;
                    this.disposalaccountcodeData = data['result'][0];
                    this.disposalaccountcodeData['f033fstatus'] = parseInt(this.disposalaccountcodeData['f033fstatus']);
                }, error => {
                    console.log(error);
                });
        }
    }
    addDisposalaccountcode() {
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.DisposalaccountcodeService.updateDisposalaccountcodeItems(this.disposalaccountcodeData, this.id).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert('Code Already Exist');
                    }
                    else {
                        alert("Disposal Account Code has been Updated Sucessfully !!");
                        this.router.navigate(['assets/disposalaccountcode']);
                    }

                }, error => {
                    console.log(error);
                    alert('Code Already Exist');
                });

        } else {

            this.DisposalaccountcodeService.insertDisposalaccountcodeItems(this.disposalaccountcodeData).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert('Code Already Exist');
                    }
                    else {
                        alert("Disposal Account Code has been Added Sucessfully !!");
                        this.router.navigate(['assets/disposalaccountcode']);
                    }
                }, error => {
                    console.log(error);
                });

        }
    }

}