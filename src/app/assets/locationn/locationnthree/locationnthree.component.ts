import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LocationService } from '../../service/location.service'
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'locationnthreeComponent',
    templateUrl: 'locationnthree.component.html'
})

export class LocationnthreeComponent implements OnInit {
    locationnthreeList = [];
    locationnthreeData = {};
    deleteList=[];
    id: number;
    levelone: string;
    leveltwo: string;
    levelthree: string;
    showLastRow: boolean;
    showLastRowMinus: boolean;
    showLastRowPlus: boolean;
    listshowLastRowMinus: boolean;
    listshowLastRowPlus: boolean;
    constructor(
        private LocationService: LocationService,
        private route: ActivatedRoute,
        private spinner: NgxSpinnerService,
        private router: Router
    ) { }

    ngOnInit() {
        this.spinner.show();
        this.getList();
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id < 1) {
            this.showLastRow = false;
            this.showLastRowMinus = false;
            this.listshowLastRowPlus = false;
            this.showLastRowPlus = true;
            this.listshowLastRowMinus = true;
        } else {
            this.showLastRow = true;
        }
        this.showIcons();
    }
    deletemodule() {
        this.showLastRow = true;
        this.showIcons();
    }

    showIcons() {
        if (this.locationnthreeList.length > 0) {
            this.showLastRowMinus = true;
            this.showLastRowPlus = true;
            this.listshowLastRowMinus = true;
        }

        if (this.locationnthreeList.length > 1) {
            if (this.showLastRow == false) {
                this.listshowLastRowPlus = false;
                this.listshowLastRowMinus = true;
                this.showLastRowPlus = true;

            } else {
                this.listshowLastRowPlus = true;
                this.listshowLastRowMinus = false;

            }
        }
        if (this.locationnthreeList.length == 1 && this.showLastRow == false) {
            this.listshowLastRowMinus = true;
            this.listshowLastRowPlus = false;

        }
        if (this.locationnthreeList.length == 1 && this.showLastRow == true) {
            this.listshowLastRowMinus = false;
            this.listshowLastRowPlus = true;

        }
        if (this.locationnthreeList.length == 0 && this.showLastRow == false) {
            this.showLastRowMinus = false;
            this.showLastRowPlus = true;
        }
        if (this.locationnthreeList.length == 0 && this.showLastRow == true) {
            this.showLastRowMinus = true;
            this.showLastRowPlus = false;
        }
    }
    showNewRow() {
        this.showLastRow = false;
        this.showIcons();
    }
    getList() {

        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.locationnthreeData['f059frefCode'] = sessionStorage.getItem("locationlevelthreeparent");
        this.locationnthreeData['f059fparentCode'] = sessionStorage.getItem("locationlevelthreeparent");
        this.locationnthreeData['f059flevelStatus'] = 3;
        let parentId = sessionStorage.getItem("locationlevelthreeparent");

        this.levelone = (sessionStorage.getItem("locationlevelonecode"));
        this.leveltwo = (sessionStorage.getItem("locationleveltwocode"));
        this.levelthree = (sessionStorage.getItem("locationlevelthreecode"));

        this.locationnthreeData['f059fstatus'] = 1;

        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        let levelId = 3;

        this.LocationService.getItemsDetail(parentId, levelId).subscribe(
            data => {
                this.spinner.hide();
                this.locationnthreeList = data['result']['data'];
            }, error => {
                console.log(error);
            });
    }
    saveLocationnthree() {
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        if (this.showLastRow == false) {
            var addactivities = this.addLocationnthree();
            if (addactivities == false) {
                return false;
            }

        }

        let levelthreeObject = {};
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            levelthreeObject['f059fid'] = this.id;
        }

        levelthreeObject['data'] = this.locationnthreeList;
        for (var i = 0; i < this.locationnthreeList.length; i++) {
        }
        this.LocationService.insertnewaccountcodeItems(levelthreeObject).subscribe(
            data => {
                if (data['status'] == 409) {
                    alert('Room Number Already Exist');
                }
                else {
                    this.getList();
                    alert("Data has been updated");
                }
            }, error => {
                console.log(error);
            });
    }
    deletelevelthree(object) {
        var cnf = confirm("Do you really want to delete");
        if (cnf == true) {
            var index = this.locationnthreeList.indexOf(object);
            this.deleteList.push(this.locationnthreeList[index]['f059fid']);
            let deleteObject = {};
            deleteObject['id'] = this.deleteList;
            this.LocationService.deleteItems(deleteObject).subscribe(
                data => {
                }, error => {
                    console.log(error);
                });
            if (index > -1) {
                this.locationnthreeList.splice(index, 1);
            }
            this.showIcons();
        }
    }

    addLocationnthree() {
        this.locationnthreeData['f059fupdatedBy'] = 1;
        this.locationnthreeData['f059fcreatedBy'] = 1;

        this.locationnthreeData['f059fstatus'] = 3;
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.locationnthreeData['f059fcode'] == undefined) {
            alert("Enter Room Number");
            return false;
        }
        if (this.locationnthreeData['f059fname'] == undefined) {
            alert("Enter Room  Description");
            return false;
        }
        var confirmPop = confirm("Do you want to Add?");
        if (confirmPop == false) {
            return false;
        }
        this.locationnthreeData['f059frefCode'] = sessionStorage.getItem("locationlevelthreeparent");
        this.locationnthreeData['f059fparentCode'] = sessionStorage.getItem("locationlevelthreeparent");
        this.locationnthreeData['f059flevelStatus'] = 3;

        var dataofCurrentRow = this.locationnthreeData;
        this.locationnthreeList.push(dataofCurrentRow);
        this.locationnthreeData = {};
        this.showIcons();

    }

}
