import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LocationService } from '../../service/location.service'
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'LocationntwoComponent',
    templateUrl: 'locationntwo.component.html'
})

export class LocationntwoComponent implements OnInit {
    locationtwoList = [];
    locationtwoData = {};
    deleteList=[];
    id: number;
    levelone: string;
    leveltwo: string;
    showLastRow: boolean;
    showLastRowMinus: boolean;
    showLastRowPlus: boolean;
    listshowLastRowMinus: boolean;
    listshowLastRowPlus: boolean;
    constructor(
        private LocationService: LocationService,
        private route: ActivatedRoute,
        private spinner: NgxSpinnerService,
        private router: Router
    ) { }

    ngOnInit() {
        this.spinner.show();
        this.getList();
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id < 1) {
            this.showLastRow = false;
            this.showLastRowMinus = false;
            this.listshowLastRowPlus = false;
            this.showLastRowPlus = true;
            this.listshowLastRowMinus = true;
        } else {
            this.showLastRow = true;
        }
        this.showIcons();
    }
    deletemodule() {
        this.showLastRow = true;
        this.showIcons();
    }

    showIcons() {
        if (this.locationtwoList.length > 0) {
            this.showLastRowMinus = true;
            this.showLastRowPlus = true;
            this.listshowLastRowMinus = true;
        }

        if (this.locationtwoList.length > 1) {
            if (this.showLastRow == false) {
                this.listshowLastRowPlus = false;
                this.listshowLastRowMinus = true;
                this.showLastRowPlus = true;

            } else {
                this.listshowLastRowPlus = true;
                this.listshowLastRowMinus = false;

            }
        }
        if (this.locationtwoList.length == 1 && this.showLastRow == false) {
            this.listshowLastRowMinus = true;
            this.listshowLastRowPlus = false;

        }
        if (this.locationtwoList.length == 1 && this.showLastRow == true) {
            this.listshowLastRowMinus = false;
            this.listshowLastRowPlus = true;

        }
        if (this.locationtwoList.length == 0 && this.showLastRow == false) {
            this.showLastRowMinus = false;
            this.showLastRowPlus = true;
        }
        if (this.locationtwoList.length == 0 && this.showLastRow == true) {
            this.showLastRowMinus = true;
            this.showLastRowPlus = false;
        }
    }
    showNewRow() {
        this.showLastRow = false;
        this.showIcons();
    }
    onBlurMethod(object) {
        sessionStorage.removeItem("locationlevelthreeparent");
        sessionStorage.removeItem("locationlevelthreecode");
        sessionStorage.removeItem("locationparentCode");
        sessionStorage.setItem("locationparentCode", object['f059fcode']);
        sessionStorage.setItem("locationlevelthreeparent", object['f059fid']);
        sessionStorage.setItem("locationlevelthreecode", object['f059fcode']);
    }
    getList() {
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.locationtwoData['f059frefCode'] = sessionStorage.getItem("locationleveltwoparent");
        this.locationtwoData['f059fparentCode'] = sessionStorage.getItem("locationleveltwoparent");
        this.locationtwoData['f059flevelStatus'] = 2;
        let parentId = sessionStorage.getItem("locationleveltwoparent");

        this.levelone = sessionStorage.getItem("locationlevelonecode");
        this.leveltwo = sessionStorage.getItem("locationleveltwocode");
        this.locationtwoData['f059fstatus'] = 1;

        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        let levelId = 2;

        this.LocationService.getItemsDetail(parentId, levelId).subscribe(
            data => {
                this.spinner.hide();
                this.locationtwoList = data['result']['data'];
            }, error => {
                console.log(error);
            });
    }
    saveLocationtwo() {
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        if (this.showLastRow == false) {
            var addactivities = this.addLocationtwo();
            if (addactivities == false) {
                return false;
            }

        }
        let levelthreeObject = {};
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            levelthreeObject['f059fid'] = this.id;
        }

        levelthreeObject['data'] = this.locationtwoList;
        for (var i = 0; i < this.locationtwoList.length; i++) {
        }
        this.LocationService.insertnewaccountcodeItems(levelthreeObject).subscribe(
            data => {
                if (data['status'] == 409) {
                    alert('Buliding Level Code Already Exist');
                }
                else {
                    this.getList();
                    alert("Data has been updated");
                }
            }, error => {
                console.log(error);
            });
    }
    deleteleveltwo(object) {
        var cnf = confirm("Do you really want to delete");
        if (cnf == true) {
            var index = this.locationtwoList.indexOf(object);
            this.deleteList.push(this.locationtwoList[index]['f059fid']);
            let deleteObject = {};
            deleteObject['id'] = this.deleteList;
            this.LocationService.deleteItems(deleteObject).subscribe(
                data => {
                }, error => {
                    console.log(error);
                });
            if (index > -1) {
                this.locationtwoList.splice(index, 1);
            }
            this.showIcons();
        }
    }

    addLocationtwo() {
        this.locationtwoData['f059fupdatedBy'] = 1;
        this.locationtwoData['f059fcreatedBy'] = 1;

        this.locationtwoData['f059fstatus'] = 2;
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.locationtwoData['f059frefCode'] = sessionStorage.getItem("locationleveltwoparent");
        this.locationtwoData['f059fparentCode'] = sessionStorage.getItem("locationleveltwoparent");
        this.locationtwoData['f059flevelStatus'] = 2;
        if (this.locationtwoData['f059fcode'] == undefined) {
            alert("Enter Building Level Code");
            return false;
        }
        if (this.locationtwoData['f059fname'] == undefined) {
            alert("Enter Building Level Description");
            return false;
        }
        var confirmPop = confirm("Do you want to Add?");
        if (confirmPop == false) {
            return false;
        }
        var dataofCurrentRow = this.locationtwoData;
        this.locationtwoList.push(dataofCurrentRow);
        this.locationtwoData = {};
        this.showIcons();
    }

}
