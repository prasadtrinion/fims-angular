import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LocationService } from '../../service/location.service'
import { NgxSpinnerService } from 'ngx-spinner';

import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
    selector: 'LocationnoneComponent',
    templateUrl: 'locationnone.component.html'
})

export class LocationnoneComponent implements OnInit {
    locationoneList = [];
    locationoneData = {};
    locationoneDataheader = {};
    categoryList = [];
    deleteList = [];
    id: number;
    levelone: string;
    showLastRow: boolean;
    showLastRowMinus: boolean;
    showLastRowPlus: boolean;
    listshowLastRowMinus: boolean;
    listshowLastRowPlus: boolean;
    constructor(
        private LocationService: LocationService,
        private spinner: NgxSpinnerService,
        private route: ActivatedRoute,
        private router: Router
    ) { }

    ngOnInit() {
        this.spinner.show();
        this.getList();
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id < 1) {
            this.showLastRow = false;
            this.showLastRowMinus = false;
            this.listshowLastRowPlus = false;
            this.showLastRowPlus = true;
            this.listshowLastRowMinus = true;
        } else {
            this.showLastRow = true;
        }
        this.showIcons();
    }
    deletemodule() {
        this.showLastRow = true;
        this.showIcons();
    }

    showIcons() {
        if (this.locationoneList.length > 0) {
            this.showLastRowMinus = true;
            this.showLastRowPlus = true;
            this.listshowLastRowMinus = true;
        }

        if (this.locationoneList.length > 1) {
            if (this.showLastRow == false) {
                this.listshowLastRowPlus = false;
                this.listshowLastRowMinus = true;
                this.showLastRowPlus = true;

            } else {
                this.listshowLastRowPlus = true;
                this.listshowLastRowMinus = false;

            }
        }
        if (this.locationoneList.length == 1 && this.showLastRow == false) {
            this.listshowLastRowMinus = true;
            this.listshowLastRowPlus = false;

        }
        if (this.locationoneList.length == 1 && this.showLastRow == true) {
            this.listshowLastRowMinus = false;
            this.listshowLastRowPlus = true;

        }
        if (this.locationoneList.length == 0 && this.showLastRow == false) {
            this.showLastRowMinus = false;
            this.showLastRowPlus = true;
        }
        if (this.locationoneList.length == 0 && this.showLastRow == true) {
            this.showLastRowMinus = true;
            this.showLastRowPlus = false;
        }
    }
    showNewRow() {
        this.showLastRow = false;
        this.showIcons();
    }

    saveLocationone() {
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        if (this.showLastRow == false) {
            var addactivities = this.addLocationone();
            if (addactivities == false) {
                return false;
            }

        }
        let leveloneObject = {};
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            leveloneObject['f059fid'] = this.id;
        }

        leveloneObject['data'] = this.locationoneList;

        for (var i = 0; i < this.locationoneList.length; i++) {
        }
        this.LocationService.insertnewaccountcodeItems(leveloneObject).subscribe(
            data => {
                if (data['status'] == 409) {
                    alert('Building Code Already Exist');
                }
                else {
                    this.getList();
                    alert("Data has been updated");
                }
            }, error => {
                alert('Building Code Already Exist');
                console.log(error);
            });
    }

    getList() {
        this.locationoneData['f059fstatus'] = 1;

        this.levelone = sessionStorage.getItem("locationlevelonecode");

        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.locationoneData['f059frefCode'] = sessionStorage.getItem("locationleveloneparent");
        this.locationoneData['f059fparentCode'] = sessionStorage.getItem("locationleveloneparent");
        this.locationoneData['f059flevelStatus'] = 1;
        let parentId = sessionStorage.getItem("locationleveloneparent");
        let levelId = 1;
        this.locationoneData['f059fstatus'] = 1;

        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.LocationService.getItemsDetail(parentId, levelId).subscribe(
            data => {
                this.spinner.hide();
                this.locationoneList = data['result']['data'];
            }, error => {
                console.log(error);
            });
    }

    onBlurMethod(object) {
        sessionStorage.removeItem("locationleveltwoparent");
        sessionStorage.removeItem("locationleveltwocode");
        sessionStorage.removeItem("locationparentCode");
        sessionStorage.setItem("locationparentCode", object['f059fcode']);
        sessionStorage.setItem("locationleveltwoparent", object['f059fid']);
        sessionStorage.setItem("locationleveltwocode", object['f059fcode']);

    }
    deletelevelone(object) {
        var cnf = confirm("Do you really want to delete");
        if (cnf == true) {
            var index = this.locationoneList.indexOf(object);
            this.deleteList.push(this.locationoneList[index]['f059fid']);
            let deleteObject = {};
            deleteObject['id'] = this.deleteList;
            this.LocationService.deleteItems(deleteObject).subscribe(
                data => {
                }, error => {
                    console.log(error);
                });
            if (index > -1) {
                this.locationoneList.splice(index, 1);
            }
            this.showIcons();
        }
    }
    addLocationone() {
        this.locationoneData['f059fupdatedBy'] = 1;
        this.locationoneData['f059fcreatedBy'] = 1;
        this.locationoneData['f059frefCode'] = sessionStorage.getItem("locationleveloneparent");
        this.locationoneData['f059fparentCode'] = sessionStorage.getItem("locationleveloneparent");
        this.locationoneData['f059fstatus'] = 0;
        if (this.locationoneData['f059fcode'] == undefined) {
            alert("Enter Building Code");
            return false;
        }
        if (this.locationoneData['f059fname'] == undefined) {
            alert("Enter Building Description");
            return false;
        }
        var confirmPop = confirm("Do you want to Add?");
        if (confirmPop == false) {
            return false;
        }
        this.locationoneData['f059flevelStatus'] = 1;

        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        var dataofCurrentRow = this.locationoneData;
        this.locationoneList.push(dataofCurrentRow);
        this.locationoneData = {};
        this.showIcons();
    }

}


