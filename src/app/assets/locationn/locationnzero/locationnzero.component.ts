import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LocationService } from '../../service/location.service'
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'LocationnzeroComponent',
    templateUrl: 'locationnzero.component.html'
})

export class LocationnzeroComponent implements OnInit {
    locationzeroList = [];
    locationzeroData = {};
    locationzeroDataheader = {};
    categoryList = [];
    deleteList=[];
    showLastRow: boolean;
    showLastRowMinus: boolean;
    showLastRowPlus: boolean;
    listshowLastRowMinus: boolean;
    listshowLastRowPlus: boolean;
    id: number;
    constructor(
        private LocationService: LocationService,
        private spinner: NgxSpinnerService,

        private route: ActivatedRoute,
        private router: Router
    ) { }

    ngOnInit() {
        this.spinner.show();
        this.getList();
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id < 1) {
            this.showLastRow = false;
            this.showLastRowMinus = false;
            this.listshowLastRowPlus = false;
            this.showLastRowPlus = true;
            this.listshowLastRowMinus =true;
        } else {
            this.showLastRow = true;
        }
        this.showIcons();
    }
    deletemodule() {
        this.showLastRow = true;
        this.showIcons();
    }

    showIcons() {
        if (this.locationzeroList.length > 0) {
            this.showLastRowMinus = true;
            this.showLastRowPlus = true;
            this.listshowLastRowMinus = true;
        }

        if (this.locationzeroList.length > 1) {
            if (this.showLastRow == false) {
                this.listshowLastRowPlus = false;
                this.listshowLastRowMinus = true;
                this.showLastRowPlus = true;

            } else {
                this.listshowLastRowPlus = true;
                this.listshowLastRowMinus = false;

            }
        }
        if (this.locationzeroList.length == 1 && this.showLastRow == false) {
            this.listshowLastRowMinus = true;
            this.listshowLastRowPlus = false;

        }
        if (this.locationzeroList.length == 1 && this.showLastRow == true) {
            this.listshowLastRowMinus = false;
            this.listshowLastRowPlus = true;

        }
        if (this.locationzeroList.length == 0 && this.showLastRow == false) {
            this.showLastRowMinus = false;
            this.showLastRowPlus = true;
        }
        if (this.locationzeroList.length == 0 && this.showLastRow == true) {
            this.showLastRowMinus = true;
            this.showLastRowPlus = false;
        }
    }
    showNewRow() {
        this.showLastRow = false;
        this.showIcons();
    }
    getList() {
        this.locationzeroData['f059fstatus'] = 1;

        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        let parentId = 0;
        let levelId = 0;

        this.LocationService.getItemsDetail(parentId, levelId).subscribe(
            data => {
                this.spinner.hide();
                this.locationzeroList = data['result']['data'];
            }, error => {
                console.log(error);
            });
    }

    deletelevelone(object) {

        var cnf = confirm("Do you really want to delete");
        var index = this.locationzeroList.indexOf(object);
        this.deleteList.push(this.locationzeroList[index]['f059fid']);        
        let deleteObject = {};
        deleteObject['id'] = this.deleteList;
        this.LocationService.deleteItems(deleteObject).subscribe(
            data => {
            }, error => {
                console.log(error);
            });
        if (index > -1) {
            this.locationzeroList.splice(index, 1);
        }
    }
    saveLocationnzero() {
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        if (this.showLastRow == false) {
            var addactivities = this.addLocationnzero();
            if (addactivities == false) {
                return false;
            }
        }
        let leveloneObject = {};
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            leveloneObject['f059fid'] = this.id;
        }

        leveloneObject['data'] = this.locationzeroList;
        this.LocationService.insertnewaccountcodeItems(leveloneObject).subscribe(
            data => {
                if (data['status'] == 409) {
                    alert('Code Already Exist');
                }
                else {
                    this.getList();
                    alert("Data has been updated");
                }
            }, error => {
                console.log(error);
            });
            // this.deletelevelone(1);
    }

    onBlurMethod(object) {
        sessionStorage.removeItem("locationleveloneparent");
        sessionStorage.removeItem("locationlevelonecode");
        sessionStorage.removeItem("locationparentCode");
        sessionStorage.setItem("locationparentCode", object['f059fcode']);
        sessionStorage.setItem("locationleveloneparent", object['f059fid']);
        sessionStorage.setItem("locationlevelonecode", object['f059fcode']);
    }


    addLocationnzero() {
        this.locationzeroData['f059fupdatedBy'] = 1;
        this.locationzeroData['f059fcreatedBy'] = 1;
        this.locationzeroData['f059fparentCode'] = 0;
        this.locationzeroData['f059frefCode'] = 0;
        this.locationzeroData['f059fstatus'] = 0;
        if (this.locationzeroData['f059fcode'] == undefined) {
            alert("Please add Code");
            return false;
        }
        if (this.locationzeroData['f059fname'] == undefined) {
            alert("Enter Description");
            return false;
        }

        var confirmPop = confirm("Do you want to Add?");
        if (confirmPop == false) {
            return false;
        }
        var dataofCurrentRow = this.locationzeroData;
        this.locationzeroList.push(dataofCurrentRow);
        this.locationzeroData = {};
        this.showIcons();
    }

}


