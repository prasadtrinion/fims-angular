import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { MaintenanceapprovalService } from '../service/maintenanceapproval.service'
import { AlertService } from './../../_services/alert.service';
import { NgxSpinnerService } from 'ngx-spinner';
@Component({
    selector: 'Maintenanceapproval',
    templateUrl: 'maintenanceapproval.component.html'
})

export class MaintenanceapprovalComponent implements OnInit {

    maintenanceapprovalList = [];
    maintenanceapprovalData = {};
    maintenanceRejectData = {};
    selectAllCheckbox = true;

    constructor(

        private MaintenanceapprovalService: MaintenanceapprovalService,
        private spinner: NgxSpinnerService,
        private AlertService: AlertService
    ) { }

    ngOnInit() {
        this.spinner.show();
        this.getListData();
    }
    getListData() {
        this.MaintenanceapprovalService.getItemsDetail(0).subscribe(
            data => {
                this.spinner.hide();
                this.maintenanceapprovalList = data['result'];
            }, error => {
                console.log(error);
            });
    }

    MaintenanceapprovalListData() {
        var approvaloneIds = [];
        for (var i = 0; i < this.maintenanceapprovalList.length; i++) {
            if (this.maintenanceapprovalList[i].f089fapprovalStatus == true) {
                approvaloneIds.push(this.maintenanceapprovalList[i].f089fid);
            }
        }
        if (approvaloneIds.length < 1) {
            alert("Select atleast one Asset Code to Approve");
            return false;
        }
        var confirmPop = confirm("Do you want to Approve?");
        if (confirmPop == false) {
            return false;
        }
        var approvaloneUpdate = {};
        approvaloneUpdate['id'] = approvaloneIds;
        approvaloneUpdate['reason'] = '';
        approvaloneUpdate['status'] = 1;
        this.MaintenanceapprovalService.updateMaintenanceapprovalItems(approvaloneUpdate).subscribe(
            data => {
                alert(" Approved Sucessfully ! ");
                this.maintenanceRejectData['reason'] = '';
                this.getListData();
            }, error => {
                console.log(error);
            });
    }


    rejectmomentapprovalListData() {
        var approvaloneIds = [];
        for (var i = 0; i < this.maintenanceapprovalList.length; i++) {
            if (this.maintenanceapprovalList[i].f089fapprovalStatus == true) {
                approvaloneIds.push(this.maintenanceapprovalList[i].f089fid);
            }
        }
        if (approvaloneIds.length < 1) {
            alert("Select atleast one Asset Code to Reject");
            return false;
        }
        if (this.maintenanceRejectData['reason'] == undefined) {
            alert('Enter the Description');
            return false;
        }
        var confirmPop = confirm("Do you want to Reject?");
        if (confirmPop == false) {
            return false;
        }
        var rejectoneUpdate = {};
        rejectoneUpdate['id'] = approvaloneIds;
        rejectoneUpdate['status'] = 2;
        rejectoneUpdate['reason'] = this.maintenanceRejectData['reason'];
        this.MaintenanceapprovalService.updateMaintenanceapprovalItems(rejectoneUpdate).subscribe(
            data => {
                this.getListData();
                this.maintenanceRejectData['reason'] = '';
                alert("Rejected Sucessfully");

            }, error => {
                console.log(error);
            });

    }
}
