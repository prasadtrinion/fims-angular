import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AssetdisposalService } from '../service/assetdisposal.service'

@Component({
    selector: 'Assetdisposal',
    templateUrl: 'assetdisposal.component.html'
})
export class AssetdisposalComponent implements OnInit {
    assetdisposalList = [];
    assetdisposalData = {};
    id: number;
    constructor(
        private AssetdisposalService: AssetdisposalService
    ) { }

    ngOnInit() {

        this.AssetdisposalService.getItems().subscribe(
            data => {
                this.assetdisposalList = data['data']['data'];
            }, error => {
                console.log(error);
            });
    }
}