import { Injector } from '@angular/core';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AssetdisposalService } from '../../service/assetdisposal.service'
import { AssetsService } from '../../service/assets.service'
import { AlertService } from '../../../_services/alert.service';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';

@Component({
    selector: 'AssetdisposalFormComponent',
    templateUrl: 'assetdisposalform.component.html'
})

export class AssetdisposalFormComponent implements OnInit {
    assetdisposalform: FormGroup;
    assetdisposalList = [];
    assetdisposalData = {};
    assetdisposalDataheader = {};
    assetssList = [];
    ajaxCount: number;
    id: number;
    constructor(
        private AssetdisposalService: AssetdisposalService,
        private AssetsService: AssetsService,
        private AlertService: AlertService,
        private route: ActivatedRoute,
        private router: Router

    ) { }
    ngDoCheck() {
        const change = this.ajaxCount;
        if (this.ajaxCount == 0) {
            this.editFunction();
            this.ajaxCount = 10;
        }
    }
    editFunction() {
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {

            this.AssetdisposalService.getItemsDetail(this.id).subscribe(
                data => {
                    this.assetdisposalList = data['result'];
                    this.assetdisposalDataheader['f086fdisposalName'] = data['result'][0]['f086fdisposalName'];
                    this.assetdisposalDataheader['f086fdescription'] = data['result'][0]['f086fdescription'];
                }, error => {
                    console.log(error);
                });
        }
    }
    ngOnInit() {
        this.ajaxCount = 0;
        // assets category
        this.ajaxCount++;
        this.AssetsService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.assetssList = data['result']['data'];
            }, error => {
                console.log(error);
            });
    }

    saveAssetdisposal() {
        let assetdisposalObject = {};
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            assetdisposalObject['f086fid'] = this.id;
        }
        assetdisposalObject['f086fdisposalName'] = this.assetdisposalDataheader['f086fdisposalName'];
        assetdisposalObject['f086fdescription'] = this.assetdisposalDataheader['f086fdescription'];
        assetdisposalObject['f086fstatus'] = 0;
        assetdisposalObject['disposal-details'] = this.assetdisposalList;
        if (this.id > 0) {
            this.AssetdisposalService.updateAssetdisposalItems(assetdisposalObject, this.id).subscribe(
                data => {

                    this.router.navigate(['assets/assetdisposal']);
                    this.AlertService.success(" Updated Sucessfully ! ");

                }, error => {
                    console.log(error);
                });
        } else {
            this.AssetdisposalService.insertAssetdisposalItems(assetdisposalObject).subscribe(
                data => {


                    this.router.navigate(['assets/assetdisposal']);
                    this.AlertService.success(" Added Sucessfully ! ");

                }, error => {
                    console.log(error);
                });
        }
    }
    deleteassetdisposal(object) {
        var cnf = confirm("Do you really want to delete");
        if (cnf == true) {
            var index = this.assetdisposalList.indexOf(object);
            if (index > -1) {
                this.assetdisposalList.splice(index, 1);
            }
        }
    }

    addassetdisposallist() {
        var dataofCurrentRow = this.assetdisposalData;
        this.assetdisposalData = {};
        this.assetdisposalList.push(dataofCurrentRow);

    }
}
