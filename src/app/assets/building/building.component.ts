import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { BuildingService } from '../service/building.service'

@Component({
    selector: 'Building',
    templateUrl: 'building.component.html'
})
export class BuildingComponent implements OnInit {
    buildingList = [];
    buildingData = {};
    id: number;
    constructor(
        private BuildingService: BuildingService
    ) { }

    ngOnInit() {

        this.BuildingService.getItems().subscribe(
            data => {
                this.buildingList = data['result']['data'];
            }, error => {
                console.log(error);
            });
    }
    addBuilding() {
        console.log(this.buildingData);
        this.BuildingService.insertBuildingItems(this.buildingData).subscribe(
            data => {
                this.buildingList = data['todos'];
            }, error => {
                console.log(error);
            });

    }
}