import { Injector } from '@angular/core';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BuildingService } from '../../service/building.service'
import { FormGroup, FormBuilder, Validators,FormControl  } from '@angular/forms';
import { AlertService } from './../../../_services/alert.service';

@Component({
    selector: 'BuildingFormComponent',
    templateUrl: 'buildingform.component.html'
})

export class BuildingFormComponent implements OnInit {
    buildingform: FormGroup; 
    buildingList = [];
    buildingData = {};
    id: number;
    constructor(

        private BuildingService: BuildingService,
        private AlertService: AlertService,
        private route: ActivatedRoute,
        private router: Router

    ) { }

    ngOnInit() {
       
        this.buildingform = new FormGroup({
            building: new FormControl('', Validators.required),
            
        });
        this.buildingData['f087fstatus'] = 1;
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        if (this.id > 0) {
            this.BuildingService.getItemsDetail(this.id).subscribe(
                data => {
                    this.buildingData = data['result'][0];
                    this.buildingData['f087fstatus'] = parseInt(this.buildingData['f087fstatus']);
                console.log(this.buildingData);
                }, error => {
                    console.log(error);
                });
        }


        console.log(this.id);
    }
    
    addBuilding() {
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.BuildingService.updateBuildingItems(this.buildingData, this.id).subscribe(
                data => {
                    if (data['status'] == 409) {
                    }
                    else {
                    this.router.navigate(['assets/building']);
                    this.AlertService.success("Updated Sucessfully !!")
                    }

                }, error => {
                    console.log(error);
                });

        } else {

            this.BuildingService.insertBuildingItems(this.buildingData).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert('Room Already Exist');
                    }
                    else {
                    this.router.navigate(['assets/building']);
                    this.AlertService.success("Added Sucessfully !!")
                    }

                }, error => {
                    console.log(error);
                });

        }
    }

}