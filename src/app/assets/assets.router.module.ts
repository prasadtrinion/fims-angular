import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AssetsComponent } from './assets/assets.component';
import { AssetsFormComponent } from './assets/assetsform/assetsform.component';

import { SubcategoryComponent } from './subcategory/subcategory.component';
import { SubcategoryFormComponent } from './subcategory/subcategoryform/subcategoryform.component';

import { AssettypeComponent } from './assettype/assettype.component';
import { AssettypeFormComponent } from './assettype/assettypeform/assettypeform.component';

import { DisposalaccountcodeComponent } from './disposalaccountcode/disposalaccountcode.component';
import { DisposalaccountcodeFormComponent } from './disposalaccountcode/disposalaccountcodeform/disposalaccountcodeform.component';

import { AssetmomentComponent } from './assetmoment/assetmoment.component';
import { AssetmomentFormComponent } from './assetmoment/assetmomentform/assetmomentform.component';

import { AssetmaintenanceComponent } from './assetmaintenance/assetmaintenance.component';
import { AssetmaintenanceFormComponent } from './assetmaintenance/assetmaintenanceform/assetmaintenanceform.component';

import { MaintenanceapprovalComponent } from './maintenanceapproval/maintenanceapproval.component';
import { MomentapprovalComponent } from './momentapproval/momentapproval.component';

import { Momentapproval2Component } from './movementapproval2/movementapproval2.component';

import { AssetdepreciationComponent } from './assetdepreciation/assetdepreciation.component';
import { AssetdepreciationFormComponent } from './assetdepreciation/assetdepreciationform/assetdepreciationform.component';

import { AssetdisposalComponent } from './assetdisposal/assetdisposal.component';
import { AssetdisposalFormComponent } from './assetdisposal/assetdisposalform/assetdisposalform.component';

import { AssetnameComponent } from './assetname/assetname.component';
import { AssetnameFormComponent } from './assetname/assetnameform/assetnameform.component';

import { AssetinformationComponent } from './assetinformation/assetinformation.component';
import { AssetinformationFormComponent } from './assetinformation/assetinformationform/assetinformationform.component';

import { TempassetinformationComponent } from './tempassetinformation/tempassetinformation.component';
import { TempassetinformationFormComponent } from './tempassetinformation/tempassetinformationform/tempassetinformationform.component';

import { RoomComponent } from './room/room.component';
import { RoomFormComponent } from './room/roomform/roomform.component';

import { SetupstoreComponent } from "./setupstore/setupstore.component";
import { SetupstoreFormComponent} from "./setupstore/setupstoreform/setupstoreform.component";

import { UpdateassetapprovalComponent} from './updateassetapproval/updateassetapproval.component';

import { BuildingComponent } from './building/building.component';
import { BuildingFormComponent } from './building/buildingform/buildingform.component';

import { DisposalverificationComponent } from './disposalverification/disposalverification.component';
import { StoreComponent } from './store/store.component';

import { LocationnzeroComponent } from './locationn/locationnzero/locationnzero.component';
import { LocationnoneComponent } from './locationn/locationnone/locationnone.component';
import { LocationntwoComponent } from './locationn/locationntwo/locationntwo.component';
import { LocationnthreeComponent } from './locationn/locationnthree/locationnthree.component';

import { TypeComponent } from "./type/type.component";
import { TypeFormComponent } from "./type/typeform/typeform.component";

import { AssetItemComponent } from "./assetitem/assetitem.component";
import { AssetItemFormComponent } from "./assetitem/assetitemform/assetitemform.component";

import { UpdateAssetFormComponent } from "./updateasset/updateassetform/updateassetform.component";
import { UpdateAssetComponent } from "./updateasset/updateasset.component";

import { AssetpreregistrationComponent } from './assetpreregistration/assetpreregistration.component';
import { AssetpreregistrationFormComponent } from './assetpreregistration/assetpreregistrationform/assetpreregistrationform.component';

import { DisposalrequisitionComponent } from './disposalrequisition/disposalrequisition.component';
import { DisposalrequisitionFormComponent } from './disposalrequisition/disposalrequisitionform/disposalrequisitionform.component';
import { DisposalendorsementComponent } from './disposalendorsement/disposalendorsement.component';
import { DisposalapprovalComponent } from './disposalapproval/disposalapproval.component';

import { PreorderlevelsetupComponent } from "./preorderlevelsetup/preorderlevelsetup.component";
import { PreOrderLevelSetupFormComponent } from "./preorderlevelsetup/preorderlevelsetupform/preorderlevelsetupform.component";

import { ApplyStockComponent } from "./applystock/applystock.component";
import { ApplyStockFormComponent } from "./applystock/applystockform/applystockform.component";

import { ManualgrnComponent } from "./manualgrn/manualgrn.component";
import { ManualgrnFormComponent } from "./manualgrn/manualgrnform/manualgrnform.component";

import { StockRegistrationComponent } from "../assets/stockregistration/stockregistration.component";
import { StockRegistrationFormComponent } from "../assets/stockregistration/stockregistrationform/stockregistrationform.component";

import { DepreciationsetupComponent } from "./depreciationsetup/depreciationsetup.component";
import { DepreciationSetupFormComponent } from "./depreciationsetup/depreciationsetupform/depreciationsetupform.component";

import { ApplystockapprovalComponent } from './applystockapproval/applystockapproval.component';

import { StockDistrubutionComponent } from "../assets/stockdistrubution/stockdistrubution.component";
import { AssetregistrationFormComponent } from './tempassetinformation/assetregistrationform/assetregistrationform.component';

import { TempstockRegistrationFormComponent } from './tempstockregistration/tempstockregistrationform/tempstockregistrationform.component';
import { TempstockRegistrationComponent } from './tempstockregistration/tempstockregistration.component';

const routes: Routes = [


  { path: 'assets', component: AssetsComponent },
  { path: 'addnewassets', component: AssetsFormComponent },
  { path: 'editassets/:id', component: AssetsFormComponent },

  { path: 'depreciationsetup', component: DepreciationsetupComponent},
  { path: 'adddepreciationsetup', component: DepreciationSetupFormComponent},
  { path: 'editdepreciationsetup/:id', component: DepreciationSetupFormComponent},

  { path: 'subcategory', component: SubcategoryComponent },
  { path: 'addnewsubcategory', component: SubcategoryFormComponent },
  { path: 'editsubcategory/:id', component: SubcategoryFormComponent },

  { path: 'setupstore', component: SetupstoreComponent },
  { path: 'addnewsetupstore', component: SetupstoreFormComponent },
  { path: 'editsetupstore/:id', component: SetupstoreFormComponent },

  { path: 'assettype', component: AssettypeComponent },
  { path: 'addnewassettype', component: AssettypeFormComponent },
  { path: 'editassettype/:id', component: AssettypeFormComponent },

  { path: 'disposalaccountcode', component: DisposalaccountcodeComponent },
  { path: 'addnewdisposalaccountcode', component: DisposalaccountcodeFormComponent },
  { path: 'editdisposalaccountcode/:id', component: DisposalaccountcodeFormComponent },

  { path: 'assetmoment', component: AssetmomentComponent },
  { path: 'addnewassetmoment', component: AssetmomentFormComponent },
  { path: 'editassetmoment/:id', component: AssetmomentFormComponent },

  { path: 'momentapproval', component: MomentapprovalComponent },
  { path: 'momentapproval2', component: Momentapproval2Component },

  { path: 'assetmaintenance', component: AssetmaintenanceComponent },
  { path: 'addnewassetmaintenance', component: AssetmaintenanceFormComponent },
  { path: 'editassetmaintenance/:id', component: AssetmaintenanceFormComponent },

  { path: 'assetdepreciation', component: AssetdepreciationComponent },
  { path: 'addnewassetdepreciation', component: AssetdepreciationFormComponent },
  { path: 'editassetdepreciation/:id', component: AssetdepreciationFormComponent },

  { path: 'assetdisposal', component: AssetdisposalComponent },
  { path: 'addnewassetdisposal', component: AssetdisposalFormComponent },
  { path: 'editassetdisposal/:id', component: AssetdisposalFormComponent },

  { path: 'assetname', component: AssetnameComponent },
  { path: 'addnewassetname', component: AssetnameFormComponent },
  { path: 'editassetname/:id', component: AssetnameFormComponent },

  { path: 'maintenanceapproval', component: MaintenanceapprovalComponent },
  { path: 'disposalverification', component: DisposalverificationComponent },
  { path: 'store', component: StoreComponent },

  { path: 'assetinformation', component: AssetinformationComponent },
  { path: 'addnewassetinformation', component: AssetinformationFormComponent },

  { path: 'editassetinformation/:id', component: AssetinformationFormComponent },
  { path: 'assetregistration/:id', component: AssetregistrationFormComponent },

  { path: 'room', component: RoomComponent },
  { path: 'addnewroom', component: RoomFormComponent },
  { path: 'editroom/:id', component: RoomFormComponent },

  { path: 'building', component: BuildingComponent },
  { path: 'addnewbuilding', component: BuildingFormComponent },
  { path: 'editbuilding/:id', component: BuildingFormComponent },

  { path: 'editassetinformation/:id', component: AssetinformationFormComponent},

  { path: 'locationnzero', component: LocationnzeroComponent },
  { path: 'locationnone', component: LocationnoneComponent },
  { path: 'locationntwo', component: LocationntwoComponent },
  { path: 'locationnthree', component: LocationnthreeComponent },

  { path: 'type', component: TypeComponent },
  { path: 'addnewtype', component: TypeFormComponent },
  { path: 'edittype/:id', component: TypeFormComponent},

  { path: 'assetitem', component: AssetItemComponent },
  { path: 'addnewassetitem', component: AssetItemFormComponent },
  { path: 'editassetitem/:id', component: AssetItemFormComponent},

  { path: 'updateasset', component: UpdateAssetComponent },
  { path: 'addnewupdateasset', component: UpdateAssetFormComponent },
  { path: 'editupdateasset/:id', component: UpdateAssetFormComponent},
  { path: 'viewupdateasset/:id/:idview', component: UpdateAssetFormComponent},

  { path : 'updateassetapproval', component:UpdateassetapprovalComponent},
  
  { path: 'assetpreregistration', component: AssetpreregistrationComponent },
  { path: 'addnewassetpreregistration', component: AssetpreregistrationFormComponent },
  { path: 'editassetpreregistration/:id', component: AssetpreregistrationFormComponent},

  { path: 'disposalrequisition', component: DisposalrequisitionComponent },
  { path: 'addnewdisposalrequisition', component: DisposalrequisitionFormComponent },
  { path: 'editdisposalrequisition/:id', component: DisposalrequisitionFormComponent},
  { path: 'viewdisposalrequisition/:id/:idview', component: DisposalrequisitionFormComponent},

  { path: 'disposalendorsement', component: DisposalendorsementComponent },
  { path: 'disposalapproval', component: DisposalapprovalComponent },

  { path: 'preorderlevelsetup', component: PreorderlevelsetupComponent },
  { path: 'addnewpreorderlevelsetup', component: PreOrderLevelSetupFormComponent },
  { path: 'editpreorderlevelsetup/:id', component: PreOrderLevelSetupFormComponent},

  { path: 'applystock', component: ApplyStockComponent },
  { path: 'addnewapplystock', component: ApplyStockFormComponent },
  { path: 'editapplystock/:id', component: ApplyStockFormComponent},
  { path: 'viewapplystock/:id/:idview', component: ApplyStockFormComponent},

  { path: 'manualgrn', component: ManualgrnComponent },
  { path: 'addnewmanualgrn', component: ManualgrnFormComponent },
  { path: 'editmanualgrn/:id', component: ManualgrnFormComponent},

  { path: 'stockregistration', component: StockRegistrationComponent },
  { path: 'addnewstockregistration', component: StockRegistrationFormComponent },
  { path: 'editstockregistration/:id', component: StockRegistrationFormComponent},

  { path: 'applystockapproval', component: ApplystockapprovalComponent },  

  { path: 'stockdistrubution', component: StockDistrubutionComponent },
  { path: 'viewstockditrubution/:id/:viewid', component: ApplyStockFormComponent},

  { path: 'getgrnid/:id', component: TempassetinformationFormComponent},
  { path: 'tempregistration/:id', component: TempassetinformationComponent},

  { path: 'grntempstock/:id', component: TempstockRegistrationFormComponent},
  { path: 'tempstockregistration/:id', component: TempstockRegistrationComponent},


];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],

})
export class AssetsRoutingModule {

}