import { Injector } from '@angular/core';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RoomService } from '../../service/room.service'
import { FormGroup, FormBuilder, Validators,FormControl  } from '@angular/forms';
import { AlertService } from './../../../_services/alert.service';

@Component({
    selector: 'RoomFormComponent',
    templateUrl: 'roomform.component.html'
})

export class RoomFormComponent implements OnInit {
    roomform: FormGroup; 
    roomList = [];
    roomData = {};
    id: number;
    constructor(

        private RoomService: RoomService,
        private AlertService: AlertService,
        private route: ActivatedRoute,
        private router: Router

    ) { }

    ngOnInit() {
    
        this.roomform = new FormGroup({
            room: new FormControl('', Validators.required),
            
        });
        this.roomData['f087fstatus'] = 1;
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        if (this.id > 0) {
            this.RoomService.getItemsDetail(this.id).subscribe(
                data => {
                    this.roomData = data['result'][0];
                    this.roomData['f087fstatus'] = parseInt(this.roomData['f087fstatus']);
                console.log(this.roomData);
                }, error => {
                    console.log(error);
                });
        }
        console.log(this.id);
    }
    
    addRoom() {

        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.RoomService.updateRoomItems(this.roomData, this.id).subscribe(
                data => {
                    if (data['status'] == 409) {
                    }
                    else {
                    this.router.navigate(['assets/room']);
                    this.AlertService.success("Updated Sucessfully !!")
                    }
                }, error => {
                    console.log(error);
                });

        } else {
            this.RoomService.insertRoomItems(this.roomData).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert('Room Already Exist');
                    }
                    else {
                    this.router.navigate(['assets/room']);
                    this.AlertService.success("Added Sucessfully !!")
                    }

                }, error => {
                    console.log(error);
                });
        }
    }
}