import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { RoomService } from '../service/room.service'

@Component({
    selector: 'Room',
    templateUrl: 'room.component.html'
})
export class RoomComponent implements OnInit {
    roomList = [];
    roomData = {};
    id: number;
    constructor(
        private RoomService: RoomService
    ) { }

    ngOnInit() {

        this.RoomService.getItems().subscribe(
            data => {
                this.roomList = data['result']['data'];
            }, error => {
                console.log(error);
            });
    }
}