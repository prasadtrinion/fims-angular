import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { StockRegistrationService } from "../service/stockregistration.service";

@Component({
    selector: 'StockRegistration',
    templateUrl: 'stockregistration.component.html'
})
export class StockRegistrationComponent implements OnInit {
    itemList = [];
    itemData = {};
    id: number;
    constructor(
        private StockRegistrationService: StockRegistrationService,
        private spinner: NgxSpinnerService

    ) { }

    ngOnInit() {
        this.spinner.show();
        this.StockRegistrationService.getItems().subscribe(
            data => {
                this.spinner.hide();
                this.itemList = data['result']['data'];
            }, error => {
                console.log(error);
            });
    }
}