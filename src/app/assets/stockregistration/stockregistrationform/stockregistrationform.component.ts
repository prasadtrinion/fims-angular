import { Injector } from '@angular/core';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AssetsService } from '../../service/assets.service'
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { ItemsubcategoryService } from "../../../procurement/service/itemsubcategory.service";
import { SubcategoryService } from "../../service/subcategory.service";
import { StockRegistrationService } from "../../service/stockregistration.service";
import { SetupstoreService } from "../../service/setupstore.service";
import { AssetItemService } from "../../service/assetitem.service";
import { NgxSpinnerService } from 'ngx-spinner';
@Component({
    selector: 'StockRegistrationFormComponent',
    templateUrl: 'stockregistrationform.component.html'
})

export class StockRegistrationFormComponent implements OnInit {
    assetsform: FormGroup;
    setupList = [];
    stockRegData = {};
    stockRegList = [];
    itemgroupList = [];
    categoryList = [];
    subcategoryList = [];
    assetItemList = [];
    id: number;
    ajaxCount: number;
    valueDate: string;
    constructor(

        private AssetsService: AssetsService,
        private ItemsubcategoryService: ItemsubcategoryService,
        private SetupstoreService: SetupstoreService,
        private SubcategoryService: SubcategoryService,
        private StockRegistrationService: StockRegistrationService,
        private AssetItemService: AssetItemService,
        private spinner: NgxSpinnerService,
        private route: ActivatedRoute,
        private router: Router

    ) { }

    ngDoCheck() {
        const change = this.ajaxCount;
        if (this.ajaxCount == 0) {
            this.spinner.hide();
            this.ajaxCount = 10;
        }
    }
    ngOnInit() {
        this.ajaxCount = 0
        this.spinner.show();
        this.assetsform = new FormGroup({
            utility: new FormControl('', Validators.required),

        });
        this.stockRegData['f124fstatus'] = 1;

        this.valueDate = new Date().toISOString().substr(0, 10);
        this.stockRegData['f124fdate'] = this.valueDate;

        // Item Group Dropdown 
        this.ajaxCount++;
        this.ItemsubcategoryService.getActiveItems().subscribe(
            data => {
                this.ajaxCount--;
                this.itemgroupList = data['result']['data'];
            }, error => {
                console.log(error);
            });

        // Category Dropdown 
        this.ajaxCount++;
        this.AssetsService.getActiveItems().subscribe(
            data => {
                this.ajaxCount--;
                this.categoryList = data['result']['data'];
            }, error => {
                console.log(error);
            });

        // SubCategory Dropdown 
        this.ajaxCount++;
        this.SubcategoryService.getActiveItems().subscribe(
            data => {
                this.ajaxCount--;
                this.subcategoryList = data['result']['data'];
            }, error => {
                console.log(error);
            });

        // Store Dropdown
        this.ajaxCount++;
        this.SetupstoreService.getActiveItems().subscribe(
            data => {
                this.ajaxCount--;
                this.setupList = data['result'];
            }, error => {
                console.log(error);
            });

        //Asset Item dropdown
        this.ajaxCount++;
        this.AssetItemService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.assetItemList = data['result']['data'];
            }, error => {
                console.log(error);
            });
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        if (this.id > 0) {
            this.ajaxCount++;
            this.StockRegistrationService.getItemsDetail(this.id).subscribe(
                data => {
                    this.ajaxCount--;
                    this.stockRegData = data['result'][0];
                    this.stockRegData['f124fstatus'] = parseInt(this.stockRegData['f124fstatus']);
                }, error => {
                    console.log(error);
                });
        }
    }
    addItem() {
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        if (this.id > 0) {
            this.StockRegistrationService.updateAssetItemItems(this.stockRegData, this.id).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert('Asset Item Code Already Exist');
                    }
                    else {
                        this.router.navigate(['assets/stockregistration']);
                        alert("Stock Registration has been Updated Successfully")
                    }
                }, error => {
                    alert('Asset Item Code Already Exist');
                    return false;
                });
        } else {
            this.StockRegistrationService.insertAssetItemItems(this.stockRegData).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert('Asset Item Code Already Exist');
                    }
                    else {
                        this.router.navigate(['assets/stockregistration']);
                        alert("Stock Registration has been Added Successfully")
                    }
                }, error => {
                    console.log(error);
                });
        }
    }
    onlyNumberKey(event) {
        if (event.charCode == 46) {

        } else {
            return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
        }
    }
}