import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { BudgetyearService } from "../service/budgetyear.service";
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'Funddonation',
    templateUrl: 'funddonation.component.html'
})
export class FundDonationComponent implements OnInit {
    budgetyearList = [];
    id: number;
    constructor(
        private BudgetyearService: BudgetyearService,
        private spinner:NgxSpinnerService
    ) { }
    
    ngOnInit() {
        this.spinner.show();
        this.BudgetyearService.getItems().subscribe(
            data => {
                this.spinner.hide();
                this.budgetyearList = data['result']['data'];
            }, error => {
                console.log(error);
            });
    }
}