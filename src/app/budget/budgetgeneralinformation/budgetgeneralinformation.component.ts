import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { BudgetgeneralinformationService } from '../../budget/service/budgetgeneralinformation.service'


@Component({
    selector: 'budgetgeneralinformation',
    templateUrl: 'budgetgeneralinformation.component.html'
})

export class BudgetgeneralinformationComponent implements OnInit {
    budgetgeneralinfoList = [];
    budgetgeneralinfodata = {};
    id: number;

    constructor(
        
        private BudgetgeneralinformationService: BudgetgeneralinformationService

    ) { }

    ngOnInit() { 
        
        this.BudgetgeneralinformationService.getItems().subscribe(
            data => {
                this.budgetgeneralinfoList = data['result']['data'];
        }, error => {
            console.log(error);
        });
    }
}