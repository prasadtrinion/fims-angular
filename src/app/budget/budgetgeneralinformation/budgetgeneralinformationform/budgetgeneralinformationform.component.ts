import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BudgetgeneralinformationService } from '../../service/budgetgeneralinformation.service'
import { SemesterService } from '../../service/semester.service'

@Component({
    selector: 'BudgetgeneralinformationFormComponent',
    templateUrl: 'budgetgeneralinformationform.component.html'
})

export class BudgetgeneralinformationFormComponent implements OnInit {
    budgetgeneralinfoList = [];
    budgetgeneralinfodata = {};
    semesterList = [];
    semesterdata = {};
    id: number;
    constructor(
        private BudgetgeneralinformationService: BudgetgeneralinformationService,
        private semesterService: SemesterService,
        private route: ActivatedRoute,
        private router: Router

    ) { }

    ngOnInit() {
        this.budgetgeneralinfodata['f053fstatus'] = 1;
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        console.log(this.id);
        if (this.id > 0) {
            //semester dropdown
            this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
            this.semesterService.getItems().subscribe(
                data => {
                    this.semesterList = data['result']['data'];
                }, error => {
                    console.log(error);
                });
            this.BudgetgeneralinformationService.getItemsDetail(this.id).subscribe(
                data => {
                    this.budgetgeneralinfodata = data['result'];
                    if (data['result'].f053fstatus === true) {
                        console.log(data);
                        this.budgetgeneralinfodata['f053fstatus'] = 1;
                    } else {
                        this.budgetgeneralinfodata['f053fstatus'] = 0;
                    }
                    console.log(this.budgetgeneralinfodata);
                }, error => {
                    console.log(error);
                });
        }
        console.log(this.id);
    }

    addBudgetgeneralinformation() {
        console.log(this.budgetgeneralinfodata);
        this.budgetgeneralinfodata['f053fupdatedBy'] = 1;
        this.budgetgeneralinfodata['f053fcreatedBy'] = 1;

        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.BudgetgeneralinformationService.updateBudgetgeneralinfoItems(this.budgetgeneralinfodata, this.id).subscribe(
                data => {
                    this.router.navigate(['budget/budgetgeneralinformation']);
                }, error => {
                    console.log(error);
                });
        } else {
            this.BudgetgeneralinformationService.insertBudgetgeneralinfoItems(this.budgetgeneralinfodata).subscribe(
                data => {
                    this.router.navigate(['budget/budgetgeneralinformation']);
                }, error => {
                    console.log(error);
                });
        }
    }
}

