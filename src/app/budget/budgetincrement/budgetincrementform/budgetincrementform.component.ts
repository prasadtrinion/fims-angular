import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DepartmentService } from '../../../generalsetup/service/department.service';
import { BudgetincrementService } from '../../service/budgetincrement.service';
import { BudgetactivitiesService } from '../../service/budgetactivities.service'
import { AccountcodeService } from '../../../generalsetup/service/accountcode.service'
import { ActivitycodeService } from '../../../generalsetup/service/activitycode.service';
import { BudgetyearService } from "../../service/budgetyear.service";
import { NgxSpinnerService } from 'ngx-spinner';


@Component({
    selector: 'BudgetincrementFormComponent',
    templateUrl: 'budgetincrementform.component.html'
})

export class BudgetIncrementFormComponent implements OnInit {
    budgetincrementList = [];
    budgetincrementData = {};
    budgetincrementDataheader = {};
    newbudgetincrementList = [];
    finantialyearList = [];
    finantialyearData = {}
    f055fidFinancialYear = {};
    budgetcostcenterList = [];
    activitycodeList = [];
    glcodeList = [];
    glcodeData = {};
    deptList = [];
    fundList = [];
    accountcodeList = [];
    deptData = {};
    adjusttype = [];
    budgetcostcenterLists = [];
    deleteList = [];
    id: number;
    saveBtnDisable: boolean;
    viewDisabled: boolean;
    idview: number;
    allocatedAmount: number = 0;
    ajaxCount: number;
    incrementAmount: number;
    configListOfDepartmentSplit: number;
    showLastRow: boolean;
    showLastRowMinus: boolean;
    showLastRowPlus: boolean;
    listshowLastRowMinus: boolean;
    listshowLastRowPlus: boolean;
    balanceAmount: number;
    arrayIncrementList = [];

    constructor(
        private BudgetactivitiesService: BudgetactivitiesService,
        private BudgetincrementService: BudgetincrementService,
        private DepartmentService: DepartmentService,
        private AccountcodeService: AccountcodeService,
        private ActivitycodeService: ActivitycodeService,
        private BudgetyearService: BudgetyearService,
        private route: ActivatedRoute,
        private spinner: NgxSpinnerService,
        private router: Router

    ) { }

    ngDoCheck() {

        this.route.paramMap.subscribe((data) => this.idview = + data.get('idview'));
        if (this.ajaxCount == 0) {
            this.spinner.hide();
        }
        if (this.ajaxCount == 0) {
            this.ajaxCount = 20;
            if (this.id > 0) {

                this.ajaxCount++;
                this.BudgetincrementService.getItemsDetail(this.id).subscribe(
                    data => {
                        this.ajaxCount--;

                        this.budgetincrementDataheader['f055fidFinancialyear'] = data['result']['data'][0]['f055fidFinancialYear'];
                        this.budgetincrementDataheader['f055fdepartment'] = data['result']['data'][0]['f055fdepartment'];
                        this.budgetincrementDataheader['f055ffund'] = data['result']['data'][0]['f055ffund'];
                        this.budgetincrementDataheader['f055fadjustmentType'] = data['result']['data'][0]['f055fadjustmentType'];
                        this.budgetincrementDataheader['allocatedAmount'] = data['result']['data'][0]['allocatedAmount'];
                        this.budgetincrementDataheader['balanceAmount'] = data['result']['data'][0]['balanceAmount'];

                        this.arrayIncrementList = data['result']['data'];
                        this.newbudgetincrementList = data['result']['data'];
                        if (this.idview == 2) {
                            this.viewDisabled = true;

                            $("#target input,select").prop("disabled", true);
                            $("#target1 input,select").prop("disabled", true);
                        }
                        if (this.newbudgetincrementList[0]['f055fapprovalStatus'] == 1 || this.newbudgetincrementList[0]['f055fapprovalStatus'] == 2) {
                            this.saveBtnDisable = true;
                            this.viewDisabled = true;
                            $("#target input,select").prop("disabled", true);
                            $("#target1 input,select").prop("disabled", true);

                        }
                        this.incrementAmount = 0;
                        for (var i = 0; i < this.newbudgetincrementList.length; i++) {
                            this.incrementAmount = this.ConvertToInt(this.incrementAmount) + this.ConvertToInt(this.newbudgetincrementList[i]['f055famount']);
                        }
                        this.allocatedAmount = this.incrementAmount;
                        this.getDetails();
                        this.getListOfPendingCostCenter();
                        this.getCostCenter();
                        this.showIcons();
                    }, error => {
                        console.log(error);
                    });
            }
        }
    }
    deletemodule() {
        this.showLastRow = true;
        this.showIcons();
    }

    showIcons() {
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.newbudgetincrementList.length > 0) {
            this.showLastRowMinus = true;
            this.listshowLastRowMinus = true;
        }
        if (this.newbudgetincrementList.length > 1) {
            if (this.showLastRow == false) {
                this.listshowLastRowPlus = false;
                this.listshowLastRowMinus = true;
                this.showLastRowPlus = true;
            } else {
                this.listshowLastRowPlus = true;
                this.listshowLastRowMinus = false;
            }
        }
        if (this.newbudgetincrementList.length == 1 && this.showLastRow == false) {
            this.listshowLastRowMinus = true;
            this.listshowLastRowPlus = false;

        }
        if (this.newbudgetincrementList.length == 1 && this.showLastRow == true) {
            this.listshowLastRowMinus = false;
            this.listshowLastRowPlus = true;

        }
        if (this.newbudgetincrementList.length == 0 && this.showLastRow == false) {
            this.showLastRowMinus = false;
            this.showLastRowPlus = true;
        }
        if (this.newbudgetincrementList.length == 0 && this.showLastRow == true) {
            this.showLastRowMinus = true;
            this.showLastRowPlus = false;
        }

        var totalAmountAllocated = 0;
        for (var i = 0; i < this.newbudgetincrementList.length; i++) {
            totalAmountAllocated = this.ConvertToInt(this.newbudgetincrementList[i]['f055famount']) + this.ConvertToInt(totalAmountAllocated);
        }
        this.balanceAmount = this.ConvertToInt(this.allocatedAmount) - totalAmountAllocated;

    }
    showNewRow() {
        this.showLastRow = false;
        this.showIcons();
    }

    ngOnInit() {
        this.spinner.show();

        this.viewDisabled = false;
        let adjusttypeobj = {};
        adjusttypeobj['name'] = 'Increment';
        adjusttypeobj['id'] = 1;
        this.adjusttype.push(adjusttypeobj);
        adjusttypeobj = {};
        adjusttypeobj['name'] = 'Decrement';
        adjusttypeobj['id'] = 2;
        this.adjusttype.push(adjusttypeobj);

        this.budgetincrementData['f055fstatus'] = 1;
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id < 1) {
            this.showLastRow = false;
            this.showLastRowMinus = false;
            this.showLastRowPlus = true;
        } else {
            this.showLastRow = true;
        }
        this.ajaxCount = 0;

        //Activity dropdown
        this.ajaxCount++;
        this.ActivitycodeService.getItemsByLevel().subscribe(
            data => {
                this.ajaxCount--;
                this.activitycodeList = data['result']['data'];
            }
        );

        //Account dropdown
        this.ajaxCount++;
        this.AccountcodeService.getItemsByLevel().subscribe(
            data => {
                this.ajaxCount--;
                this.accountcodeList = data['result']['data'];
            }
        );

        // //fyear dropdown
        this.ajaxCount++;
        this.BudgetyearService.getActiveItems().subscribe(
            data => {
                this.ajaxCount--;
                this.finantialyearList = data['result'];
                let curYear = new Date().getFullYear();
                for (var i = 0; i < this.finantialyearList.length; i++) {
                    if (curYear == parseInt(this.finantialyearList[i]['f110fyear'])) {
                        this.budgetincrementDataheader['f055fidFinancialyear'] = this.finantialyearList[i]['f110fid'];
                        this.getListOfPendingCostCenter();
                        break;
                    }
                }
                if (data['result'].length == 1) {
                    this.budgetincrementDataheader['f055fidFinancialyear'] = data['result'][0]['f110fid'];
                    this.getListOfPendingCostCenter();

                }
               
            }, error => {
                console.log(error);
            });
    }

    getListOfPendingCostCenter() {
        this.ajaxCount++;
        var idfinancialYear = this.budgetincrementDataheader['f055fidFinancialyear'];
        this.DepartmentService.pendingCostCenterByIdFinancialYear(idfinancialYear).subscribe(
            data => {
                this.ajaxCount--;
                var depArray = data['result']['Department'];
                this.budgetcostcenterLists = depArray;
                this.fundList = data['result']['Fund'];

            }, error => {
            });
    }
    getCostCenter() {
        this.ajaxCount++;
        var idfinancialYear = this.budgetincrementDataheader['f055fidFinancialyear'];
        this.DepartmentService.getCostCenterByIdFinancialYear(idfinancialYear).subscribe(
            data => {
                this.ajaxCount--;
                var depArray = data['result'];
                for (var i = 0; i < depArray.length; i++) {
                    depArray[i]['combined'] = depArray[i]['f015fdepartment_code'] + ' - ' + depArray[i]['f015fdepartment_name']
                }
            }, error => {
            });
    }
    getAmount() {
        let idfinancialYear = this.budgetincrementDataheader['f055fidFinancialyear'];
        let iddept = this.budgetincrementDataheader['f055fdepartment'];
        let idfund = this.budgetincrementDataheader['f055ffund'];
        this.ajaxCount++;
        this.BudgetactivitiesService.getAllocatedAmountBasedOnFinancialYearAndDepartment(idfinancialYear, iddept, idfund).subscribe(
            data => {
                this.ajaxCount--;
                if (data['result']['data'].length == 0) {
                } else {
                    this.allocatedAmount = data['result']['data'][0]['f051famount'];
                }
            }, error => {
            });
    }
    getGLCode() {
        var idfinancialYear = this.budgetincrementDataheader['f055fidFinancialyear'];
        var iddept = this.budgetincrementDataheader['f055fidDepartment'];
        if (idfinancialYear == undefined || idfinancialYear == '') {
        }
        // if(iddept==undefined || iddept=='') {
        //     alert('Please select the financial Year');
        //     return false;
        // }
        this.ajaxCount++;
        this.BudgetactivitiesService.getAllGlCode(iddept, idfinancialYear).subscribe(
            data => {
                this.ajaxCount--;
                this.glcodeList = data['result']['data'];
            }, error => {
                console.log(error);
            });
    }

    ConvertToInt(val) {
        return parseInt(val);
    }
    onBlurMethod() {
        var finaltotal = 0;
        for (var i = 0; i < this.budgetincrementList.length; i++) {
            var totalamount = (this.ConvertToInt(this.budgetincrementList[i]['f072fquantity'])) * (this.ConvertToInt(this.budgetincrementList[i]['f072fprice']));
            var gstamount = (this.ConvertToInt(this.budgetincrementList[i]['f072fgst'])) / 100 * (totalamount);
            this.budgetincrementList[i]['f072ftotal'] = gstamount + totalamount;
            finaltotal = finaltotal + this.budgetincrementList[i]['f072ftotal'];
        }

        this.budgetincrementDataheader['f071finvoiceTotal'] = finaltotal;
    }
    deletebudgetincrement(object) {
        var cnf = confirm("Do you really want to delete");
        if (cnf == true) {
            var index = this.newbudgetincrementList.indexOf(object);
            this.deleteList.push(this.newbudgetincrementList[index]['f055fidDetails']);
            let deleteObject = {};
            deleteObject['id'] = this.deleteList;
            this.ajaxCount++;

            this.BudgetincrementService.getDeleteItems(deleteObject).subscribe(
                data => {
                    this.ajaxCount--;

                }, error => {
                    console.log(error);
                });
            if (index > -1) {
                this.newbudgetincrementList.splice(index, 1);
            }
            this.onBlurMethod();
            this.showIcons();
        }
    }
    savebudgetincrement() {

        if (this.budgetincrementDataheader['f055ffund'] == undefined) {
            alert('Select Fund');
            return false;
        }
        if (this.budgetincrementDataheader['f055fdepartment'] == undefined) {
            alert('Select Cost Center');
            return false;
        }
        for (var i = 0; i < this.newbudgetincrementList.length; i++) {
            if (this.newbudgetincrementList[i]['f055fadjustmentType'] == undefined) {
                alert('Select Adjustment Type');
                return false;
            }
            if (this.newbudgetincrementList[i]['f055fverimentAmount'] == undefined) {
                alert('Enter Virement Amount(RM)');
                return false;
            }
        }

        let budgetamount = {};

        for (var i = 0; i < this.newbudgetincrementList.length; i++) {
            console.log(this.newbudgetincrementList);
            if (this.newbudgetincrementList[i]['f055fverimentAmount'] > this.allocatedAmount) {
                alert("Virement Amount should not greater than Budget Allocated Amount");
                return false;
            }
            if (this.newbudgetincrementList[i]['f055fverimentAmount'] > this.newbudgetincrementList[i]['f054famount']) {
                alert("Virement Amount should not greater than Amount ");
                return false;
            }
        }

        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }

        this.budgetincrementDataheader['f055fidFinancialyear'];
        this.budgetincrementDataheader['allocatedAmount'];
        this.budgetincrementDataheader['balanceAmount'];
        this.budgetincrementDataheader['f055ffund'];
        this.budgetincrementDataheader['f055fdepartment'];
        this.budgetincrementDataheader['increment-details'] = this.newbudgetincrementList;
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        if (this.id > 0) {
            this.BudgetincrementService.updateBudgetincrementItems(this.budgetincrementDataheader, this.id).subscribe(
                data => {
                    this.router.navigate(['budget/budgetincrement']);
                    alert("Budget Adjustment has been Updated Successfully!")
                }, error => {
                    console.log(error);
                });
        } else {
            this.BudgetincrementService.insertBudgetincrementItems(this.budgetincrementDataheader).subscribe(
                data => {
                    this.router.navigate(['budget/budgetincrement']);
                    alert("Budget Adjustment has been Added Successfully!")

                }, error => {
                    console.log(error);
                });
        }
    }
    getDetails() {
        if (this.budgetincrementDataheader['f055fdepartment'] != undefined &&
            this.budgetincrementDataheader['f055ffund'] != undefined) {
            let budgetincrementdata = {};
            var iddept = budgetincrementdata['department'] = this.budgetincrementDataheader['f055fdepartment'];
            budgetincrementdata['financialYear'] = this.budgetincrementDataheader['f055fidFinancialyear'];
            budgetincrementdata['fund'] = this.budgetincrementDataheader['f055ffund'];
            this.getAmount();
            var departmentCode = iddept.substring(0, 3);;
            this.ajaxCount++;
            this.BudgetactivitiesService.getCostCenterBasedOnDepartmentCode(departmentCode).subscribe(
                data => {
                    this.ajaxCount--;
                    this.budgetcostcenterList = data['result']['data'];
                    this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
                    this.ajaxCount++;
                    this.BudgetincrementService.getDetails(budgetincrementdata).subscribe(
                        data => {
                            this.ajaxCount--;
                            this.newbudgetincrementList = data['result'];
                            for (var i = 0; i < this.arrayIncrementList.length; i++) {
                                this.newbudgetincrementList[i]['f055fidDetails'] = this.arrayIncrementList[i]['f055fidDetails'];
                                this.newbudgetincrementList[i]['f055fadjustmentType'] = this.arrayIncrementList[i]['f055fadjustmentType'];
                                this.newbudgetincrementList[i]['f055fverimentAmount'] = this.arrayIncrementList[i]['f055fverimentAmount'];
                            }
                        }
                    );
                }, error => {
                });
        }
    }
    onlyNumberKey(event) {
        if (event.charCode == 46) {

        } else {
            return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
        }
    }
    maximize() {
        $("#myModal").addClass("modal");
        $("#overlay1").show();
        $(".maximize-icon").addClass("hidden");
        $(".minimize-icon").removeClass("hidden");
        $(".modal-backdrop").show();

    }
    minimize() {
        $("#myModal").removeClass("modal");
        $("#overlay1").hide();
        $(".maximize-icon").removeClass("hidden");
        $(".minimize-icon").addClass("hidden");
        $(".modal-backdrop").hide();
        $('#myModal').show();

    }

}
