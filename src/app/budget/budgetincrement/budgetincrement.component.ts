import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { BudgetincrementService } from '../service/budgetincrement.service'
import { QueryBuilderConfig } from 'angular2-query-builder';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'Budgetincrement',
    templateUrl: 'budgetincrement.component.html'
})

export class BudgetIncrementComponent implements OnInit {
    budgetincrementList = [];
    budgetcostcenterList = [];
    budgetincrementData = {};
    id: number;
    constructor(
        private BudgetincrementService: BudgetincrementService,
        private spinner: NgxSpinnerService
    ) {
    }
    ngOnInit() {
        this.spinner.show();
        this.BudgetincrementService.getItems().subscribe(
            data => {
                this.spinner.hide();
                let activityData = [];
                activityData = data['data']['data'];
                for (var i = 0; i < activityData.length; i++) {
                    if (activityData[i]['f055fapprovalStatus'] == 0) {
                        activityData[i]['f055fapprovalStatus'] = 'Pending';
                    } else if (activityData[i]['f055fapprovalStatus'] == 1) {
                        activityData[i]['f055fapprovalStatus'] = 'Approved';
                    } else {
                        activityData[i]['f055fapprovalStatus'] = 'Rejected';
                    }
                }
                this.budgetincrementList = activityData;
            }, error => {
                console.log(error);
            });
    }
}