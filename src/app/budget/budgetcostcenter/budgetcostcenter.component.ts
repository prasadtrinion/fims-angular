import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { BudgetcostcenterService } from '../../budget/service/budgetcostcenter.service'
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'Budgetcostcenter',
    templateUrl: 'budgetcostcenter.component.html'
})

export class BudgetcostcenterComponent implements OnInit {
    budgetcostcenterList = [];
    budgetcostcenterData = {};
    id: number;

    constructor(
        private BudgetcostcenterService: BudgetcostcenterService,
        private spinner:NgxSpinnerService
    ) { }
    ngOnInit() {
        this.spinner.show();

        this.BudgetcostcenterService.getItems().subscribe(
            data => {
                this.spinner.hide();
                let activityData = [];
                activityData = data['result']['data'];
                for (var i = 0; i < activityData.length; i++) {
                    if (activityData[i]['f051fapprovalStatus'] == 0) {
                        activityData[i]['f051fapprovalStatus'] = 'Pending';
                    } else if (activityData[i]['f051fapprovalStatus'] == 1) {
                        activityData[i]['f051fapprovalStatus'] = 'Approved';
                    } else {
                        activityData[i]['f051fapprovalStatus'] = 'Rejected';
                    }
                }
                this.budgetcostcenterList = activityData;
            }, error => {
                console.log(error);
            });
    }
}