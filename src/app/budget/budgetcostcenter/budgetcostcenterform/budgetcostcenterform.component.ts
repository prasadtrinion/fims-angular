import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BudgetcostcenterService } from '../../service/budgetcostcenter.service'
import { DepartmentService } from '../../../generalsetup/service/department.service'
import { FundService } from '../../../generalsetup/service/fund.service';
import { BudgetyearService } from "../../service/budgetyear.service";
import { NgxSpinnerService } from 'ngx-spinner';
@Component({
    selector: 'BudgetcostcenterFormComponent',
    templateUrl: 'budgetcostcenterform.component.html'
})

export class BudgetcostcenterFormComponent implements OnInit {

    budgetcostcenterList = [];
    budgetcostcenterData = {};
    finantialyearList = [];
    finantialyearData = {};
    deptList = [];
    deptData = {};
    id: number;
    idview: number;
    ajaxCount: number;
    DeptList = [];
    fundList = [];
    viewDisabled: boolean;

    constructor(
        private BudgetcostcenterService: BudgetcostcenterService,
        private DepartmentService: DepartmentService,
        private FundService: FundService,
        private BudgetyearService: BudgetyearService,
        private route: ActivatedRoute,
        private router: Router,
        private spinner: NgxSpinnerService

    ) { }

    ngDoCheck() {
        const change = this.ajaxCount;
        if (this.ajaxCount == 0) {
            this.spinner.hide();
        }
    }

    ngOnInit() {

        this.viewDisabled = false;
        this.budgetcostcenterData['f051fstatus'] = 1;
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.route.paramMap.subscribe((data) => this.idview = + data.get('idview'));
       
        this.spinner.show();
        //Department dropdown
        this.ajaxCount = 0;
        this.ajaxCount++;
        this.DepartmentService.getZerodepartment().subscribe(
            data => {
                this.ajaxCount--;
                this.DeptList = data['result']['data'];
            }
        );
        // //fyear dropdown
        this.ajaxCount++;
        this.BudgetyearService.getActiveItems().subscribe(
            data => {
                this.finantialyearList = data['result'];
                this.ajaxCount--;
                
                let curYear = new Date().getFullYear();
                for(var i=0;i<this.finantialyearList.length;i++){
                    if(curYear == parseInt(this.finantialyearList[i]['f110fyear'])){
                        this.budgetcostcenterData['f051fidFinancialyear'] = this.finantialyearList[i]['f110fid'];
                        break;
                    }
                }
                if (data['result'].length == 1) {
                    this.budgetcostcenterData['f051fidFinancialyear'] = data['result'][0]['f110fid'];
                }
            }, error => {

            });
        //Fund dropdown
        this.ajaxCount++;
        this.FundService.getActiveItems().subscribe(
            data => {
                this.ajaxCount--;
                this.fundList = data['result']['data'];
            }
        );

        if (this.id > 0) {
            this.BudgetcostcenterService.getItemsDetail(this.id).subscribe(
                data => {
                    this.budgetcostcenterData = data['result'][0];
                    var numberamount = data['result'][0]['f051famount'];
                    this.budgetcostcenterData['f051famount'] = parseFloat(numberamount).toFixed(2);

                    if (data['result'][0]['f051fstatus'] == 1) {
                        this.budgetcostcenterData['f051fstatus'] = 1;
                    } else {
                        this.budgetcostcenterData['f051fstatus'] = 0;
                    }
                    if (data['result'][0]['f051fapprovalStatus'] == 1 || data['result'][0]['f051fapprovalStatus'] == 2) {
                        this.viewDisabled = true;
                        $("#target input,select").prop("disabled", true);
                    }
                    if (this.idview > 0) {
                        this.viewDisabled = true;
                        $("#target input,select").prop("disabled", true);
                    }
                }, error => {
                    console.log(error);
                });
        }
    }
    addBudgetcostcenter() {
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        this.budgetcostcenterData['f051fupdatedBy'] = 1;
        this.budgetcostcenterData['f051fcreatedBy'] = 1;

        let budgetDataArray = [];
        budgetDataArray.push(this.budgetcostcenterData);
        let budgetDataInsertObject = {};
        budgetDataInsertObject['data'] = budgetDataArray;

        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        if (this.id > 0) {
            this.BudgetcostcenterService.updateBudgetcostcenterItems(this.budgetcostcenterData, this.id).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert('Budget Year Already Exist');
                        return false;
                    }
                    this.router.navigate(['budget/budgetcostcenter']);
                    alert("Budget Allocation By Cost Center has been Updated Successfully")
                }, error => {

                    console.log(error);
                });
        } else {
            this.BudgetcostcenterService.insertBudgetcostcenterItems(this.budgetcostcenterData).subscribe(
                data => {
                    if (data['status'] == '409') {
                        alert("This Cost Center has already been allocated with Budget")
                    } else {
                        this.router.navigate(['budget/budgetcostcenter']);
                        alert("Budget Allocation By Cost Center has been Added Successfully")
                    }
                }, error => {

                    console.log(error);
                });
        }
    }
    onlyNumberKey(event) {
        if (event.charCode == 46) {

        } else {
            return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
        }
    }
}