import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BudgetactivitiesService } from '../../service/budgetactivities.service'
import { DepartmentService } from '../../../generalsetup/service/department.service'
import { FinantialyearService } from '../../../generalledger/service/finantialyear.service'
import { GlcodeService } from '../../../generalsetup/service/glcode.service'


@Component({
    selector: 'BudgetcontrolFormComponent',
    templateUrl: 'budgetcontrolform.component.html'
})

export class BudgetcontrolFormComponent implements OnInit {
    budgetactivitiesList = [];
    budgetactivitiesData = {};
    budgetactivitiesDataheader = {};
    newbudgetactivitiesList = [];
    finantialyearList = [];
    finantialyearData = {};
    f054fidFinancialyear = {};
    configList = [];
    configData = {};
    glcodeList = [];
    glcodeData = {};
    deptList = [];
    deptData = {};
    id: number;
    allocatedAmount: number;
    allocationAmount: number;
    balanceAmount: number;
    ajaxCount: number;
    saveBtnDisable: boolean;
    configListOfDepartmentSplit: number;

    constructor(
        private BudgetactivitiesService: BudgetactivitiesService,
        private DepartmentService: DepartmentService,
        private finantialyearService: FinantialyearService,
        private GlcodeService: GlcodeService,
        private route: ActivatedRoute,
        private router: Router
    ) { }

    ngDoCheck() {
        const change = this.ajaxCount;
        console.log(this.ajaxCount);
        if (this.ajaxCount == 0 && this.newbudgetactivitiesList.length > 0) {
            console.log();
            if (this.newbudgetactivitiesList[0]['f054fapprovalStatus'] == 1) {
                console.log("asdf");
                $("#target input,select").prop("disabled", true);
                $("#target1 input,select").prop("disabled", true);

            }
        }
    }
    ngOnInit() {

        this.allocatedAmount = 0;
        this.allocationAmount = 0;
        this.budgetactivitiesDataheader['f054fstatus'] = 1;
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.ajaxCount = 0;
        this.ajaxCount++;

        // financial year dropdown
        this.ajaxCount++;
        this.finantialyearService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.finantialyearList = data['result']['data'];
            }, error => {
                console.log(error);
            });

        // Gl Code dropdown

        this.ajaxCount++;
        this.GlcodeService.getGlItems().subscribe(
            data => {
                this.ajaxCount--;
                this.glcodeList = data['result']['data'];
                if (this.id > 0) {
                    this.ajaxCount++;
                    this.BudgetactivitiesService.getItemsDetail(this.id).subscribe(
                        data => {
                            this.ajaxCount--;
                            this.budgetactivitiesDataheader['f054fidFinancialyear'] = data['result']['data'][0]['f054fidFinancialyear'];
                            this.budgetactivitiesDataheader['f054fidDepartment'] = data['result']['data'][0]['f054fidDepartment'];
                            this.newbudgetactivitiesList = data['result']['data'];
                            console.log(this.newbudgetactivitiesList);
                            this.getAmount();
                            this.getCostCenter();

                        }, error => {
                            console.log(error);
                        });
                }
            }, error => {
            });
    }

    getAllocatedAmountBasedOnFinancialYearAndDepartment(idfinancial, iddepat) {
    }

    getCostCenter() {
        this.ajaxCount++;
        var idfinancialYear = this.budgetactivitiesDataheader['f054fidFinancialyear'];
        this.DepartmentService.getCostCenterByIdFinancialYear(idfinancialYear).subscribe(
            data => {
                this.ajaxCount--;
                this.deptList = data['result']['data'];
            }, error => {
                console.log(error);
            });
    }

    getAmount() {
        console.log("asdf");
        this.ajaxCount++;
        console.log(this.budgetactivitiesDataheader);
        let idfinancialYear = this.budgetactivitiesDataheader['f054fidFinancialyear']
        let iddept = this.budgetactivitiesDataheader['f054fidDepartment']

        this.BudgetactivitiesService.getAllocatedAmountBasedOnFinancialYearAndDepartment(idfinancialYear, iddept, 0).subscribe(
            data => {
                this.ajaxCount--;
                console.log(data['result']['data']);
                if (data['result']['data'].length == 0) {
                    this.allocatedAmount = 0;
                    alert('No budget has been allocated for this Department')
                } else {
                    this.allocatedAmount = data['result']['data'][0]['f051famount'];
                    this.balanceAmount = this.allocatedAmount;
                }
                this.getBalanceAmount();
            }, error => {
                console.log(error);
            });
    }

    saveBudgetactivities() {
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        console.log(this.newbudgetactivitiesList);

        this.budgetactivitiesDataheader['f054fidFinancialyear'];
        this.budgetactivitiesDataheader['f054fidDepartment'];
        this.budgetactivitiesDataheader['f054ftotalAmount'] = this.allocatedAmount - this.balanceAmount;
        this.budgetactivitiesDataheader['activity-details'] = this.newbudgetactivitiesList;

        if (this.id > 0) {
            this.BudgetactivitiesService.updateBudgetactivitiesItems(this.budgetactivitiesDataheader, this.id).subscribe(
                data => {
                    this.router.navigate(['budget/budgetcontrol']);
                }, error => {
                    console.log(error);
                });
        } else {
            this.BudgetactivitiesService.insertBudgetactivitiesItems(this.budgetactivitiesDataheader).subscribe(
                data => {
                    this.router.navigate(['budget/budgetcontrol']);
                }, error => {
                    console.log(error);
                });
        }
    }
    ConvertToInt(val) {
        return parseInt(val);
    }
    deletebudgetactivites(object) {
        var cnf = confirm("Do you really want to delete");
        if (cnf == true) {
            console.log(this.newbudgetactivitiesList);
            var index = this.newbudgetactivitiesList.indexOf(object);;
            if (index > -1) {
                this.newbudgetactivitiesList.splice(index, 1);
            }
            this.getBalanceAmount();

        }
    }
    getBalanceAmount() {
        var totalAmountAllocated = 0;

        for (var i = 0; i < this.newbudgetactivitiesList.length; i++) {
            totalAmountAllocated = this.ConvertToInt(this.newbudgetactivitiesList[i]['f054famount']) + this.ConvertToInt(totalAmountAllocated);
        }
        if (this.budgetactivitiesData['f054famount'] > 0) {
            this.allocationAmount = this.allocationAmount + this.ConvertToInt(this.budgetactivitiesData['f054famount']);
        }
        console.log(totalAmountAllocated);
        this.balanceAmount = (this.ConvertToInt(this.allocatedAmount) - this.ConvertToInt(totalAmountAllocated));
    }
    addBudgetactivities() {

        if (this.allocatedAmount == 0) {
            alert("Cannot distribute the budget amount / Budget is not allocated for this department");
            return false;
        }

        if (this.budgetactivitiesData['f054fidGlcode'] == undefined) {
            alert('Select GL Code');
            return false;
        }
        if (this.budgetactivitiesData['f054famount'] == undefined) {
            alert('Enter Amount');
            return false;
        }
        if (this.budgetactivitiesData['f054fq1'] == undefined) {
            alert('Enter Q1');
            return false;
        }
        if (this.budgetactivitiesData['f054fq2'] == undefined) {
            alert('Enter Q2');
            return false;
        }
        if (this.budgetactivitiesData['f054fq3'] == undefined) {
            alert('Enter Q3');
            return false;
        }

        if (this.budgetactivitiesData['f054fq4'] == undefined) {
            this.budgetactivitiesData['f054fq4'] = 0;
        }
        if ((this.ConvertToInt(this.budgetactivitiesData['f054famount'])) !==
            ((this.ConvertToInt(this.budgetactivitiesData['f054fq1'])) +
                (this.ConvertToInt(this.budgetactivitiesData['f054fq2'])) +
                (this.ConvertToInt(this.budgetactivitiesData['f054fq3'])))) {
            alert('Amount should be equal to its Quater amount');
            return false;
        }
        console.log(this.allocatedAmount);
        var totalAmountAllocated = 0;
        for (var i = 0; i < this.newbudgetactivitiesList.length; i++) {

            if (this.newbudgetactivitiesList[i]['f054fidGlcode'] == this.budgetactivitiesData['f054fidGlcode']) {
                alert("This GL Code has already been allocated, Change GL Code");
                return false;
            }
            totalAmountAllocated = this.ConvertToInt(this.newbudgetactivitiesList[i]['f054famount']) + this.ConvertToInt(totalAmountAllocated);
        }

        if (totalAmountAllocated + this.ConvertToInt(this.budgetactivitiesData['f054famount']) > this.allocatedAmount) {
            alert("Allocation of amount has been exceeded");
            return false;
        }
        console.log(this.allocationAmount);
        this.allocationAmount = this.allocationAmount + this.ConvertToInt(this.budgetactivitiesData['f054famount']);
        console.log(totalAmountAllocated);
        console.log(this.allocationAmount);
        this.balanceAmount = (this.ConvertToInt(this.allocatedAmount) - this.ConvertToInt(this.allocationAmount));
        console.log(this.balanceAmount);
        this.budgetactivitiesData['f054fupdatedBy'] = 1;
        this.budgetactivitiesData['f054fcreatedBy'] = 1;
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        var dataofCurrentRow = this.budgetactivitiesData;
        this.newbudgetactivitiesList.push(dataofCurrentRow);
        console.log(this.budgetactivitiesData);
        this.budgetactivitiesData = {};
    }
}
