import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { BudgetactivitiesService } from '../service/budgetactivities.service'
import { QueryBuilderConfig } from 'angular2-query-builder';


@Component({
    selector: 'Budgetcontrol',
    templateUrl: 'budgetcontrol.component.html'
})

export class BudgetcontrolComponent implements OnInit {
    budgetactivitiesList = [];
    budgetactivitiesData = {};
    id: number;
    allocatedAmount: number;
    constructor(
        private BudgetactivitiesService: BudgetactivitiesService

    ) {
    }
    query = {
        condition: 'and',
        rules: [
            { field: 'age', operator: '<=', value: 'Bob' },
            { field: 'gender', operator: '>=', value: 'm' }
        ]
    };

    config: QueryBuilderConfig = {
        fields: {
            age: { name: 'Age', type: 'number' },
            gender: {
                name: 'Gender',
                type: 'category',
                options: [
                    { name: 'Male', value: 'm' },
                    { name: 'Female', value: 'f' }
                ]
            }
        }
    }
    getSql() {
        console.log(this.config);
    }

    ngOnInit() {

        this.BudgetactivitiesService.getItems().subscribe(
            data => {
                this.budgetactivitiesList = data['result'];
            }, error => {
                console.log(error);
            });

        this.allocatedAmount = 100;
    }
}