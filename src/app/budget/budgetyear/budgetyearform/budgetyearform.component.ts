import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BudgetyearService } from "../../service/budgetyear.service";
import { DefinationmsService } from '../../../studentfinance/service/definationms.service';

@Component({
    selector: 'budgetyearFormComponent',
    templateUrl: 'budgetyearform.component.html'
})

export class budgetyearFormComponent implements OnInit {
    budgetyearList = [];
    budgetyearData = {};
    id: number;

    constructor(
        private BudgetyearService: BudgetyearService,
        private route: ActivatedRoute,
        private router: Router,
        private DefinationmsService: DefinationmsService,

    ) { }
    ngOnInit() {
        this.budgetyearData['f110fstatus'] = 1;
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.BudgetyearService.getItemsDetail(this.id).subscribe(
                data => {
                    this.budgetyearData = data['result'][0];
                    this.budgetyearData['f110fstatus'] = parseInt(data['result'][0]['f110fstatus']);
                }, error => {
                    console.log(error);
                });
        }
    }
    checkDuplication() {
        let duplicationObj = {};
        if (this.id > 0) {
            duplicationObj['id'] = this.id;
        }
        duplicationObj['key'] = 'Budget Year';
        duplicationObj['budgetYear'] = this.budgetyearData['f110fyear'];
        if(this.budgetyearData['f110fyear'] != undefined && this.budgetyearData['f110fyear'] != ""){
        this.DefinationmsService.checkDuplication(duplicationObj).subscribe(
            data => {
                if (parseInt(data['result']) == 1) {
                    alert("Budget Year Already Exist");
                    this.budgetyearData['f110fyear'] = '';
                }
            }, error => {
                console.log(error);
            });
        }
    }
    checkvalidyear() {
        this.checkDuplication();
        let d = new Date();
        let n = d.getFullYear();
        if (parseInt(this.budgetyearData['f110fyear']) < n) {
            alert('Enter a valid budget year');
            return false;
        }
    }
    addfinancialyear() {
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.BudgetyearService.updatebudgetyearItems(this.budgetyearData, this.id).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert('Budget Year Already Exist');
                        return false;
                    }
                    this.router.navigate(['budget/budgetyear']);
                    alert("Budget Year has been Updated Successfully");
                }, error => {
                    alert('Budget Year Already Exist');
                    return false;
                });
        } else {
            this.BudgetyearService.insertbudgetyearItems(this.budgetyearData).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert('Budget Year Already Exist');
                        return false;
                    }
                    this.router.navigate(['budget/budgetyear']);
                    alert("Budget Year has been Added Successfully");

                }, error => {
                    console.log(error);
                });
        }
    }
    onlyNumberKey(event) {
        return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
    }
}