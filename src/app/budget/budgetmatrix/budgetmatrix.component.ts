import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { BudgetyearService } from "../service/budgetyear.service";
import { NgxSpinnerService } from 'ngx-spinner';
import { FundService } from '../../generalsetup/service/fund.service';

@Component({
    selector: 'BudgetMatrix',
    templateUrl: 'budgetmatrix.component.html'
})
export class BudgetMatrixComponent implements OnInit {
    matrixList = [];
    matrixList1 = [];
    fundList = [];
    fundList1 = [];
    tempMatrixList = [];
    id: number;
    fundObj = {};
    constructor(
        private BudgetyearService: BudgetyearService,
        private spinner: NgxSpinnerService,
        private FundService: FundService,

    ) { }

    ngOnInit() {

        this.getList();
    }
    getList() {
        this.FundService.getActiveItems().subscribe(
            data => {
                this.fundList = data['result']['data'];
                this.BudgetyearService.getbudgettransfer().subscribe(
                    data => {
                        this.tempMatrixList = data['data']['data'];
                        console.log(this.tempMatrixList);  
                        this.matrixList = [];
                        for (var i = 0; i < this.fundList.length; i++) {
                            this.matrixList1 = [];
                            for (var j = 0; j < this.fundList.length; j++) {
                                this.fundObj = {};
                                let status = 0;
                                if(this.tempMatrixList[i][j] != null){
                                 status = parseInt(this.tempMatrixList[i][j]['f141fstatus']);
                                }
                                this.fundObj['f141fstatus'] = isNaN(status)? 0: status;
                                this.fundObj['fromFund'] = this.fundList[i]['f057fcode'];
                                this.fundObj['toFund'] = this.fundList[j]['f057fcode'];
                                this.matrixList1.push(this.fundObj);
                                // console.log(this.matrixList1);  
                            }
                            this.matrixList.push(this.matrixList1);
                        }
                        console.log(this.matrixList);  

                    });
                
            }
        );
        
    }
    save() {
        let saveObj = {};
        saveObj['funds'] = this.matrixList;
        this.BudgetyearService.insertbudgettransfer(saveObj).subscribe(
            data => {
                this.getList();
            });
    }
}