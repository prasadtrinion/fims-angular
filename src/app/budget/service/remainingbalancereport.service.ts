

import { Injectable } from '@angular/core';
import { HttpClientModule,HttpHeaders, HttpClient} from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({ 'X-Auth-Client': 'qazwsx123',
    'Content-Type': 'application/json' })
    
  };
@Injectable()

export class RemainingBalancereportService {

    url: string = environment.api.base + environment.api.endPoints.remainingBalance
    approveurl: string = environment.api.base + environment.api.endPoints.journalApprove
    constructor(private httpClient: HttpClient) {    
    }
    getItems() {
        return this.httpClient.get(this.url+'/'+1,httpOptions);
    }
    getItemsDetail(id) {
        return this.httpClient.get(this.url+'/'+id, httpOptions);
    }
    insertBalancesheetreportItems(remainingbalanceData): Observable<any> {
        return this.httpClient.post(this.url, remainingbalanceData,httpOptions);
    }
    updateBalancesheetreportItems(balancesheetData,id): Observable<any> {
        return this.httpClient.put(this.approveurl+'/'+id, balancesheetData,httpOptions);
    }
    updateBalancesheetreportreport(balancesheetData): Observable<any> {
        return this.httpClient.post(this.approveurl, balancesheetData,httpOptions);
    }
}