import { Injectable } from '@angular/core';
import { HttpClientModule,HttpHeaders, HttpClient} from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({ 'X-Auth-Client': 'qazwsx123',
    'Content-Type': 'application/json' })
    
  };

@Injectable()

export class BudgetVirementService {
    url: string = environment.api.base + environment.api.endPoints.budgetVeriment;
    urlbudgetVerimentAmount : string = environment.api.base + environment.api.endPoints.budgetVerimentAmount;
    urlgetAllGls : string = environment.api.base + environment.api.endPoints.getAllGls;
    deleteurl : string = environment.api.base + environment.api.endPoints.deleteBudgetVerimentDetails;
    transferfromurl : string = environment.api.base + environment.api.endPoints.getfromtransferfunds;
    transfertourl : string = environment.api.base + environment.api.endPoints.gettotransferfunds;
    constructor(private httpClient: HttpClient) { 
        
    }
    getDeleteItems(deleteObject){
        return this.httpClient.post(this.deleteurl,deleteObject,httpOptions);
    }
    getItems() {
        return this.httpClient.get(this.url,httpOptions);
    }
    getAllGls(id) {
        return this.httpClient.get(this.urlgetAllGls+'/'+id,httpOptions);
    }
    getFromTranferrableFunds(fund) {
        return this.httpClient.get(this.transferfromurl+'/'+fund,httpOptions);
    }
    getToTranferrableFunds(fund) {
        return this.httpClient.get(this.transfertourl+'/'+fund,httpOptions);
    }
    getItemsDetail(id) {
        return this.httpClient.get(this.url+'/'+id,httpOptions);
    }
    insertBudgetvirementItems(budgetvirementData): Observable<any> {
        return this.httpClient.post(this.url, budgetvirementData,httpOptions);
    }
    getDepartmentBudgetAmount(deptObject) {
        return this.httpClient.post(this.urlbudgetVerimentAmount,deptObject,httpOptions);
    }
    updateBudgetvirementItems(budgetvirementData,id): Observable<any> {
        return this.httpClient.put(this.url+'/'+id, budgetvirementData,httpOptions);
       }
}