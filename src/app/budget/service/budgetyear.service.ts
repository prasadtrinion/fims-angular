import { Injectable } from '@angular/core';
import { HttpClientModule,HttpHeaders, HttpClient} from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({ 'X-Auth-Client': 'qazwsx123',
    'Content-Type': 'application/json' })
    
  };
@Injectable()

export class BudgetyearService {
    url: string = environment.api.base + environment.api.endPoints.budgetyear
    ActiveListurl: string = environment.api.base + environment.api.endPoints.ActiveFinancialYear
    budgettransferurl: string = environment.api.base + environment.api.endPoints.budgettransfer

    constructor(private httpClient: HttpClient) {    
    }
    getItems() {
        return this.httpClient.get(this.url,httpOptions);
    }
    getActiveItems() {
        return this.httpClient.get(this.ActiveListurl,httpOptions);
    }
    getItemsDetail(id) {
        return this.httpClient.get(this.url+'/'+id, httpOptions);
    }
    insertbudgetyearItems(budgetyearData): Observable<any> {
        return this.httpClient.post(this.url, budgetyearData,httpOptions);
    }
    insertbudgettransfer(data): Observable<any> {
        return this.httpClient.post(this.budgettransferurl, data,httpOptions);
    }
    getbudgettransfer() {
        return this.httpClient.get(this.budgettransferurl,httpOptions);
    }
    updatebudgetyearItems(budgetyearData,id): Observable<any> {
        return this.httpClient.put(this.url+'/'+id, budgetyearData,httpOptions);
    }
}