import { Injectable } from '@angular/core';
import { HttpClientModule, HttpHeaders, HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({
        'X-Auth-Client': 'qazwsx123',
        'Content-Type': 'application/json'
    })

};

@Injectable()

export class BudgetfiluploadService {
    url: string = environment.api.base + environment.api.endPoints.budgetfileimport;
    tableurl: string = environment.api.base + 'getTables';
    columnurl: string = environment.api.base + 'getColumns';
    constructor(private httpClient: HttpClient) {

    }
    getItems() {
        return this.httpClient.get(this.url, httpOptions);
    }
    getTables() {
        return this.httpClient.get(this.tableurl, httpOptions);
    }
    getColumns(table) {
        return this.httpClient.get(this.columnurl + '/' + table, httpOptions);
    }
    importfile(data) {
        return this.httpClient.post(this.url, data, httpOptions);
    }
    insertBudgetfiluploadlItems(BudgetfiluploadData): Observable<any> {
        return this.httpClient.post(this.url, BudgetfiluploadData, httpOptions);
    }
    updateBudgetfiluploadItems(BudgetfiluploadData, id): Observable<any> {
        return this.httpClient.put(this.url + '/' + id, BudgetfiluploadData, httpOptions);
    }
}