import { Injectable } from '@angular/core';
import { HttpClientModule, HttpHeaders, HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({
        'X-Auth-Client': 'qazwsx123',
        'Content-Type': 'application/json'
    })

};

@Injectable()

export class DepartmentbudgetService {
    url: string = environment.api.base + environment.api.endPoints.departmentbudget;
    constructor(private httpClient: HttpClient) {

    }
    getItems() {
        return this.httpClient.get(this.url, httpOptions);
    }
    getItemsDetail(id) {
        return this.httpClient.get(this.url + '/' + id, httpOptions);
    }
    insertDepartmentbudgetItems(Departmentbudgetdata): Observable<any> {
        return this.httpClient.post(this.url, Departmentbudgetdata, httpOptions);
    }
    updateDepartmentbudgetItems(Departmentbudgetdata, id): Observable<any> {
        return this.httpClient.put(this.url + '/' + id, Departmentbudgetdata, httpOptions);
    }
}