import { Injectable } from '@angular/core';
import { HttpClientModule, HttpHeaders, HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({
        'X-Auth-Client': 'qazwsx123',
        'Content-Type': 'application/json'
    })
};

@Injectable()

export class BudgetcostcenterapprovalService {
    url: string = environment.api.base + environment.api.endPoints.budgetcostcenterapproval;
    budgetApprovalurl: string = environment.api.base + environment.api.endPoints.CostcenterApprovalStatus;

    constructor(private httpClient: HttpClient) {
    }
    getItems() {
        return this.httpClient.get(this.url, httpOptions);
    }
    getItemsDetail(id) {
        return this.httpClient.get(this.url + '/' + id, httpOptions);
    }
    insertBudgetcostcenterItems(budgetcostcenterapprovalData): Observable<any> {
        return this.httpClient.post(this.url, budgetcostcenterapprovalData, httpOptions);
    }
    updateBudgetcostcenterItems(budgetcostcenterapprovalData): Observable<any> {
        return this.httpClient.post(this.budgetApprovalurl, budgetcostcenterapprovalData, httpOptions);
    }
}