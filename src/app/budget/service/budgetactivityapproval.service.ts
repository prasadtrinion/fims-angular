import { Injectable } from '@angular/core';
import { HttpClientModule, HttpHeaders, HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({
        'X-Auth-Client': 'qazwsx123',
        'Content-Type': 'application/json'
    })

};

@Injectable()

export class BudgetactivityapprovalService {
    budgeturl: string = environment.api.base + environment.api.endPoints.budgetActivityList;
    budgetApprovalurl: string = environment.api.base + environment.api.endPoints.budgetActivityApproval;

    constructor(private httpClient: HttpClient) {
    }
    getItems() {
        return this.httpClient.get(this.budgeturl, httpOptions);
    }
    getItemsDetail() {
        return this.httpClient.get(this.budgeturl + '/0', httpOptions);
    }
    insertBudgetactivityItems(budgetactivityapprovalData): Observable<any> {
        return this.httpClient.post(this.budgeturl, budgetactivityapprovalData, httpOptions);
    }
    updateBudgetactivityApproval(budgetactivityapprovalData): Observable<any> {
        return this.httpClient.put(this.budgetApprovalurl, budgetactivityapprovalData, httpOptions);
    }
}