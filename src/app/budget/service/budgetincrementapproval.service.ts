import { Injectable } from '@angular/core';
import { HttpClientModule, HttpHeaders, HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({
        'X-Auth-Client': 'qazwsx123',
        'Content-Type': 'application/json'
    })

};

@Injectable()

export class BudgetincrementapprovalService {
    url: string = environment.api.base + environment.api.endPoints.budgetincrementapproval;
    budgetApprovalurl: string = environment.api.base + environment.api.endPoints.IncrementApprovalStatus;

    constructor(private httpClient: HttpClient) {
    }
    getItems() {
        return this.httpClient.get(this.url, httpOptions);
    }
    getItemsDetail() {
        return this.httpClient.get(this.url + '/0', httpOptions);
    }
    insertBudgetincrementItems(budgetincrementapprovalData): Observable<any> {
        return this.httpClient.post(this.url, budgetincrementapprovalData, httpOptions);
    }
    updateBudgetincrementItems(budgetincrementapprovalData): Observable<any> {
        return this.httpClient.put(this.budgetApprovalurl, budgetincrementapprovalData, httpOptions);
    }

}