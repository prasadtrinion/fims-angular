import { Injectable } from '@angular/core';
import { HttpClientModule, HttpHeaders, HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({
        'X-Auth-Client': 'qazwsx123',
        'Content-Type': 'application/json'
    })

};

@Injectable()

export class BudgetactivitiesService {
    url: string = environment.api.base + environment.api.endPoints.budgetDepartmentActivity;
    urlList: string = environment.api.base + environment.api.endPoints.budgetactivitiesList;
    urlgetCostcenterAmount: string = environment.api.base + environment.api.endPoints.getCostcenterAmount;
    urlgetActivityGlCodes: string = environment.api.base + environment.api.endPoints.getActivityDetails;
    urlpostBudgetActivity: string = environment.api.base + environment.api.endPoints.postBudgetActivity
    urlDeptCode: string = environment.api.base + environment.api.endPoints.getByUnitCode;
    deleteurl: string = environment.api.base + environment.api.endPoints.deleteBudgetActivityDetails;

    constructor(private httpClient: HttpClient) {

    }
    getDeleteItems(deleteObject) {
        return this.httpClient.post(this.deleteurl,deleteObject, httpOptions);
    }
    getCostCenterBasedOnDepartmentCode(deptCode) {
        return this.httpClient.get(this.urlDeptCode + '/' + deptCode, httpOptions);
    }
    getItems() {
        return this.httpClient.get(this.url, httpOptions);
    }
    getItemsDetail(id) {
        return this.httpClient.get(this.urlpostBudgetActivity + '/' + id, httpOptions);
    }
    insertBudgetactivitiesItems(budgetactivitiesDataheader): Observable<any> {
        return this.httpClient.post(this.urlpostBudgetActivity, budgetactivitiesDataheader, httpOptions);
    }
    updateBudgetactivitiesItems(budgetactivitiesData, id): Observable<any> {
        return this.httpClient.put(this.urlpostBudgetActivity + '/' + id, budgetactivitiesData, httpOptions);
    }
    getAllocatedAmountBasedOnFinancialYearAndDepartment(idfinancial, iddept, idfund) {
        return this.httpClient.get(this.urlgetCostcenterAmount + '/' + idfinancial + '/' + iddept + '/' + idfund, httpOptions);
    }
    getDetails(idyear, idfund, idcostcenter) {
        return this.httpClient.get(this.urlList + '/' + idyear + '/' + idfund + '/' + idcostcenter, httpOptions);

    }
    getAllGlCode(iddept, idfinancial) {
        return this.httpClient.get(this.urlgetActivityGlCodes + '/' + iddept + '/' + idfinancial, httpOptions);
    }
}