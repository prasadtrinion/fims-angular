import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SummaryStatementreportService } from "../service/summarystatementreport.service";
import { BudgetyearService } from "../service/budgetyear.service";
import { DepartmentService } from '../../generalsetup/service/department.service'
import { NgxSpinnerService } from 'ngx-spinner';
import { environment } from '../../../environments/environment';

@Component({
    selector: 'SummaryStatementreportComponent',
    templateUrl: 'summarystatementreport.component.html'
})

export class SummaryStatementreportComponent implements OnInit {

    summarystatementList = [];
    summarystatementData = {};
    fundList = [];
    numberList = [];
    finantialyearList = [];
    budgetcostcenterList = [];
    id: number;
    ajaxCount: number;
    downloadUrl: string = environment.api.downloadbase;
    constructor(
        private SummaryStatementreportService: SummaryStatementreportService,
        private BudgetyearService: BudgetyearService,
        private DepartmentService: DepartmentService,
        private route: ActivatedRoute,
        private router: Router,
        private spinner: NgxSpinnerService,

    ) { }
    downloadReport() {
        console.log(this.summarystatementData);
        let newobje = {};
        newobje['financialYear'] = this.summarystatementData['financialYear'];
        newobje['departmentCode'] = this.summarystatementData['departmentCode'];
        newobje['fundCode'] = this.summarystatementData['fundCode'];

        this.SummaryStatementreportService.insertBalancesheetreportItems(newobje).subscribe(
            data => {
                console.log(data['name']);
                window.open(this.downloadUrl + data['name']);
            }, error => {
                console.log(error);
            });
    }

    ngDoCheck() {
        const change = this.ajaxCount;
        if (this.ajaxCount == 0) {
            this.spinner.hide();
        }
    }

    
    ngOnInit() {
        this.spinner.show();
        this.summarystatementData['financialYear'] = '';
        for (var i = 1; i < 30; i++) {
            let number = {};
            number['key'] = i;
            number['value'] = i;
            this.numberList.push(number);
        }

        this.ajaxCount = 0;
        // financial year dropdown
        this.ajaxCount++;
        this.BudgetyearService.getActiveItems().subscribe(
            data => {
                this.ajaxCount--;
                this.finantialyearList = data['result'];
                console.log(data['result']);
                if (data['result'].length == 1) {
                    this.summarystatementData['financialYear'] = data['result'][0]['f110fid'];
                    this.getListOfPendingCostCenter();

                }
            }, error => {
            });
    }
    getListOfPendingCostCenter() {
        this.ajaxCount++;
        var idfinancialYear = this.summarystatementData['financialYear'];
        this.DepartmentService.pendingCostCenterByIdFinancialYear(idfinancialYear).subscribe(
            data => {
                this.ajaxCount--;
                var depArray = data['result']['Department'];
                this.budgetcostcenterList = depArray;

                this.fundList = data['result']['Fund'];
                console.log(data);
            }, error => {
            });
    }
}
