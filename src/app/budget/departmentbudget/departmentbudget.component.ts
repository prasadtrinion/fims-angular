import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DepartmentbudgetService } from '../service/departmentbudget.service'


@Component({
    selector: 'Departmentbudget',
    templateUrl: 'departmentbudget.component.html'
})

export class DepartmentbudgetComponent implements OnInit {
    DepartmentbudgetList = [];
    DepartmentbudgetData = {};
    id: number

    constructor(
        private DepartmentbudgetService: DepartmentbudgetService

    ) { }

    ngOnInit() { 
        
        this.DepartmentbudgetService.getItems().subscribe(
            data => {
                this.DepartmentbudgetList = data['result']['data'];
        }, error => {
            console.log(error);
        });
    }
}