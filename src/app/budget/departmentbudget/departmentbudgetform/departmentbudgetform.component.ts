import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DepartmentbudgetService } from '../../service/departmentbudget.service'

@Component({
    selector: 'DepartmentbudgetFormComponent',
    templateUrl: 'departmentbudgetform.component.html'
})

export class DepartmentbudgetFormComponent implements OnInit {
    departmentbudgetList = [];
    departmentbudgetdata = {};
    id: number;
    constructor(
        private DepartmentbudgetService: DepartmentbudgetService,
        private route: ActivatedRoute,
        private router: Router

    ) { }

    ngOnInit() {
        this.departmentbudgetdata['f052fstatus'] = 1;
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        console.log(this.id);
        if (this.id > 0) {
            this.DepartmentbudgetService.getItemsDetail(this.id).subscribe(
                data => {
                    this.departmentbudgetdata = data['result'];
                    if (data['result'].f052fstatus === true) {
                        console.log(data);
                        this.departmentbudgetdata['f052fstatus'] = 1;
                    } else {
                        this.departmentbudgetdata['f052fstatus'] = 0;
                    }
                }, error => {
                    console.log(error);
                });
        }
    }
    addDepartmentbudget() {
        this.departmentbudgetdata['f052fupdatedBy'] = 1;
        this.departmentbudgetdata['f052fcreatedBy'] = 1;
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.DepartmentbudgetService.updateDepartmentbudgetItems(this.departmentbudgetdata, this.id).subscribe(
                data => {
                    this.router.navigate(['budget/departmentbudget']);
                }, error => {
                    console.log(error);
                });
        } else {
            this.DepartmentbudgetService.insertDepartmentbudgetItems(this.departmentbudgetdata).subscribe(
                data => {
                    this.router.navigate(['budget/departmentbudget']);
                }, error => {
                    console.log(error);
                });
        }
    }
}