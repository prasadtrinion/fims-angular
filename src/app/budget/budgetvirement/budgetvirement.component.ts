import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { BudgetVirementService } from '../service/budgetvirement.service'
import { QueryBuilderConfig } from 'angular2-query-builder';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'Budgetvirement',
    templateUrl: 'budgetvirement.component.html'
})

export class BudgetvirementComponent implements OnInit {
    budgetvirementList = [];
    budgetvirementData = {};
    id: number;
    constructor(
                private BudgetVirementService: BudgetVirementService,
                private spinner: NgxSpinnerService

              ) {
     }
    ngOnInit() { 
        this.spinner.show();
        this.BudgetVirementService.getItems().subscribe(
            data => {
                this.spinner.hide();
                let activityData = [];
                activityData = data['result']['data'];
                for (var i = 0; i < activityData.length; i++) {
                    if (activityData[i]['f056fapproval'] == 0) {
                        activityData[i]['f056fapproval'] = 'Pending';
                    } else if (activityData[i]['f056fapproval'] == 1) {
                        activityData[i]['f056fapproval'] = 'Approved';
                    } else {
                        activityData[i]['f056fapproval'] = 'Rejected';
                    }
                }
                this.budgetvirementList = activityData;
            }, error => {
                console.log(error);
            });
    }
}