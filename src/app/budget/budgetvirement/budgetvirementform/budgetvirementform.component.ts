import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BudgetVirementService } from '../../service/budgetvirement.service'
import { DepartmentService } from '../../../generalsetup/service/department.service'
import { BudgetactivitiesService } from '../../service/budgetactivities.service';
import { BudgetyearService } from '../../service/budgetyear.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'BudgetvirementFormComponent',
    templateUrl: 'budgetvirementform.component.html'
})

export class BudgetvirementFormComponent implements OnInit {
    budgetvirementList = [];
    budgetvirementData = {};
    budgetvirementDataheader = {};
    newbudgetvirementList = [];
    fyearList = [];
    f056fidFinancialYear = {};
    varimenttype = [];
    deptList = [];
    deptData = {};
    glcodeListLastRow = [];
    DeptList = [];
    fundList = [];
    transferfundList = [];
    budgetcostcenterList = [];
    id: number;
    ajaxCount: number;
    idview: number;
    virementAmount: number;
    editStatus: number;
    costCenterFund = [];
    costCenterDepartment = [];
    costCenterActivity = [];
    costCenterAccount = [];
    budgetcostcentertoList = [];
    viewDisabled: boolean;
    deletedIds = [];
    finantialyearList = [];
    deleteList = [];
    showLastRow: boolean;
    showLastRowMinus: boolean;
    showLastRowPlus: boolean;
    listshowLastRowMinus: boolean;
    listshowLastRowPlus: boolean;
    saveBtnDisable: boolean;
    showDetails:any;
    constructor(
        private BudgetVirementService: BudgetVirementService,
        private BudgetactivitiesService: BudgetactivitiesService,
        private DepartmentService: DepartmentService,
        private BudgetyearService: BudgetyearService,
        private route: ActivatedRoute,
        private spinner: NgxSpinnerService,
        private router: Router,
    ) { }


    showIcons() {
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.newbudgetvirementList.length > 0) {
            this.showLastRowMinus = true;
            this.showLastRowPlus = true;
            this.listshowLastRowMinus = true;
        }
        if (this.newbudgetvirementList.length > 1) {
            if (this.showLastRow == false) {
                this.listshowLastRowPlus = false;
                this.listshowLastRowMinus = true;
                this.showLastRowPlus = true;

            } else {
                this.listshowLastRowPlus = true;
                this.listshowLastRowMinus = false;

            }
        }
        if (this.newbudgetvirementList.length == 1 && this.showLastRow == false) {
            this.listshowLastRowMinus = true;
            this.listshowLastRowPlus = false;

        }
        if (this.newbudgetvirementList.length == 1 && this.showLastRow == true) {
            this.listshowLastRowMinus = false;
            this.listshowLastRowPlus = true;

        }
        if (this.newbudgetvirementList.length == 0 && this.showLastRow == false) {
            this.showLastRowMinus = false;
            this.showLastRowPlus = true;
        }
        if (this.newbudgetvirementList.length == 0 && this.showLastRow == true) {
            this.showLastRowMinus = true;
            this.showLastRowPlus = false;
        }
        if (this.viewDisabled == true || this.newbudgetvirementList[0]['f056fapproval'] == 1 || this.newbudgetvirementList[0]['f056fapproval'] == 2) {
            this.listshowLastRowPlus = false;
        }
    }

    ngDoCheck() {
        const change = this.ajaxCount;
        if (this.ajaxCount == 0) {
            this.editFunction();
            this.spinner.hide();
            this.ajaxCount = 10;
        }
        this.route.paramMap.subscribe((data) => this.idview = + data.get('idview'));
        if (this.ajaxCount == 0 && this.budgetvirementList.length > 0) {
            this.ajaxCount = 20;
            this.showIcons();
            if (this.newbudgetvirementList[0]['f056fapproval'] == 1 || this.newbudgetvirementList[0]['f056fapproval'] == 2) {
                this.saveBtnDisable = true;
                this.viewDisabled = true;

                $("#target input,select").prop("disabled", true);
                $("#target1 input,select").prop("disabled", true);
            }
        }
        if (this.ajaxCount == 0 && this.newbudgetvirementList.length > 0) {
            this.ajaxCount = 20;
            if (this.idview > 1) {
                this.saveBtnDisable = true;
                this.viewDisabled = true;

                $("#target input,select").prop("disabled", true);
                $("#target1 input,select").prop("disabled", true);
            }
        }
    }

    editFunction() {
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.route.paramMap.subscribe((data) => this.idview = + data.get('idview'));
        if (this.id > 0) {
            this.showDetails = 0;
            this.BudgetVirementService.getItemsDetail(this.id).subscribe(
                data => {
                    this.newbudgetvirementList = data['result'];
                    this.budgetvirementDataheader['f056fidFinancialYear'] = data['result'][0]['f056fidFinancialYear'];
                    this.budgetvirementDataheader['f056ffromDepartment'] = data['result'][0]['f056ffromDepartment'];
                    this.budgetvirementDataheader['f056ffromFundCode'] = data['result'][0]['f056ffromFundCode'];
                    this.budgetvirementDataheader['f056ffromAccountCode'] = data['result'][0]['f056ffromAccountCode'];
                    this.budgetvirementDataheader['f056ffromActivityCode'] = data['result'][0]['f056ffromActivityCode'];
                    this.budgetvirementDataheader['f056ffromDepartmentCode'] = data['result'][0]['f056ffromDepartmentCode'];
                    this.budgetvirementDataheader['f056freason'] = data['result'][0]['f056freason'];
                    this.budgetvirementDataheader['f056fverimentType'] = parseInt(data['result'][0]['f056fverimentType']);
                    this.budgetvirementDataheader['f056ftotalAmount'] = this.ConvertToFloat(data['result'][0]['f056ftotalAmount']);

                    this.editStatus = data['result'][0]['f056fapproval'];
                    this.getGlList();
                    this.getAmount();
                    if (data['result'][0]['f056fapproval'] == 1 || this.newbudgetvirementList[0]['f056fapproval'] == 2) {
                        this.viewDisabled = true;

                        $("#target input,select").prop("disabled", true);
                        $("#target1 input,select").prop("disabled", true);
                    }
                    if (this.idview == 2) {
                        this.viewDisabled = true;

                        $("#target input,select").prop("disabled", true);
                        $("#target1 input,select").prop("disabled", true);
                    }
                    this.virementAmount = 0;
                    for (var i = 0; i < this.newbudgetvirementList.length; i++) {
                        this.virementAmount = this.ConvertToFloat(this.virementAmount) + this.ConvertToFloat(this.newbudgetvirementList[i]['f056famount']);
                    }
                    this.showIcons();
                    this.getTransferrableFunds();
                }, error => {
                    console.log(error);
                });
        }

    }
    deletemodule() {
        this.showLastRow = true;
        this.showIcons();
    }

    showNewRow() {
        var confirmPop = confirm("Do you want to Add?");
        if (confirmPop == false) {
            return false;
        }
        this.showLastRow = false;
        this.showIcons();
    }
    getTransferrableFunds() { 
        if(this.budgetvirementDataheader['f056fverimentType'] == 1){

        this.BudgetVirementService.getFromTranferrableFunds(this.budgetvirementDataheader['f056ffromFundCode']).subscribe(
            data => {
                this.transferfundList = data['result']['data'];
            });
        }
        else{
            this.BudgetVirementService.getToTranferrableFunds(this.budgetvirementDataheader['f056ffromFundCode']).subscribe(
                data => {
                    this.transferfundList = data['result']['data'];
                });
        }        
    }
    ngOnInit() {
        this.spinner.show();

        let varimentypeobj = {};
        varimentypeobj['name'] = 'One To Many';
        varimentypeobj['id'] = 1;
        this.varimenttype.push(varimentypeobj);
        varimentypeobj = {};
        varimentypeobj['name'] = 'Many To One';
        varimentypeobj['id'] = 2;
        this.varimenttype.push(varimentypeobj);

        this.viewDisabled = false;
        this.ajaxCount = 0;
        this.budgetvirementData['f056fstatus'] = 1;
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        if (this.id < 1) {
            this.showLastRow = false;
            this.showLastRowMinus = false;
            this.showLastRowPlus = true;
        } else {
            this.showLastRow = true;

        }
        // financial year dropdown
        this.ajaxCount++;
        this.BudgetyearService.getActiveItems().subscribe(
            data => {
                this.ajaxCount--;
                this.finantialyearList = data['result'];
                let curYear = new Date().getFullYear();
                for (var i = 0; i < this.finantialyearList.length; i++) {
                    if (curYear == parseInt(this.finantialyearList[i]['f110fyear'])) {
                        this.budgetvirementData['f056ftoIdFinancialYear'] = this.finantialyearList[i]['f110fid'];
                        this.budgetvirementDataheader['f056fidFinancialYear'] = this.finantialyearList[i]['f110fid'];
                        this.newbudgetvirementList['f056ftoIdFinancialYear'] = this.finantialyearList[i]['f110fid'];
                        this.getGlList();
                        break;
                    }
                }
                if (data['result'].length == 1) {
                        this.budgetvirementData['f056ftoIdFinancialYear'] =  data['result'][0]['f110fid'];
                        this.budgetvirementDataheader['f056fidFinancialYear'] = data['result'][0]['f110fid'];
                        this.newbudgetvirementList['f056ftoIdFinancialYear'] = data['result'][0]['f110fid'];
                    this.getGlList();

                }

            }, error => {
            });
    }

    getGlList() {
        this.ajaxCount++;

        var idfinancialYear = this.budgetvirementDataheader['f056fidFinancialYear'];
        this.BudgetVirementService.getAllGls(idfinancialYear).subscribe(
            data => {
                this.ajaxCount--;

                var depArray = data['result']['Department'];
                this.budgetcostcenterList = depArray;
                this.costCenterFund = data['result']['Fund'];
                this.costCenterAccount = data['result']['Account'];
                this.costCenterActivity = data['result']['Activity'];
            }, error => {
                console.log(error);
            }
        );
    }
    getRowDeptCode() {

        var idfinancialYear = this.budgetvirementDataheader['f056fidFinancialYear'];
        var iddept = this.budgetvirementData['f056ftoDepartment'];
        this.ajaxCount++;
        this.BudgetactivitiesService.getAllGlCode(iddept, idfinancialYear).subscribe(
            data => {
                this.ajaxCount--;

                this.glcodeListLastRow = data['result']['data'];
            }, error => {
                console.log(error);
            });
    }

    getAmount() {
        let costCenterObject = {};
        costCenterObject['FinancialYear'] = this.budgetvirementDataheader['f056fidFinancialYear'];
        costCenterObject['Department'] = this.budgetvirementDataheader['f056ffromDepartment'];
        costCenterObject['AccountCode'] = this.budgetvirementDataheader['f056ffromAccountCode'];
        costCenterObject['ActivityCode'] = this.budgetvirementDataheader['f056ffromActivityCode'];
        costCenterObject['DepartmentCode'] = this.budgetvirementDataheader['f056ffromDepartmentCode'];
        costCenterObject['FundCode'] = this.budgetvirementDataheader['f056ffromFundCode'];
        this.ajaxCount++;

        this.BudgetVirementService.getDepartmentBudgetAmount(costCenterObject).subscribe(
            data => {
                this.ajaxCount--;

                if (data['result']['data'].length == 0) {
                    //    alert("Amount has not been allocated to this GL Code");
                    this.budgetvirementDataheader['f056famount'] = 0;
                } else {
                    this.budgetvirementDataheader['f056famount'] = data['result']['data'][0]['f054famount'];
                }
            }, error => {
                console.log(error);
            }
        );
    }
    ConvertToFloat(val) {
        return parseFloat(val);
    }
    getCostCenter() {
        var idfinancialYear = this.budgetvirementDataheader['f056fidFinancialYear'];
        this.ajaxCount++;

        this.DepartmentService.getCostCenterByIdFinancialYear(idfinancialYear).subscribe(
            data => {
                this.ajaxCount--;

                this.deptList = data['result']['data'];
                this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
                if (this.id > 0) {
                    this.getGlList();
                }
            }, error => {
                console.log(error);
            });
    }
    getBudgetDepartmentAmount() {
        this.ajaxCount++;

        this.BudgetVirementService.getDepartmentBudgetAmount(this.budgetvirementDataheader).subscribe(
            data => {
                this.ajaxCount--;

                if (data['result']['data'].length == 0) {
                    alert("No amount");
                } else {
                    this.virementAmount = this.ConvertToFloat(data['result']['data'][0]['f054famount']);
                }
            }, error => {
                console.log(error);
            });
    }
    onBlurMethod() {
        var finaltotal = 0;
        for (var i = 0; i < this.budgetvirementList.length; i++) {
            var totalamount = (this.ConvertToFloat(this.budgetvirementList[i]['f072fquantity'])) * (this.ConvertToFloat(this.budgetvirementList[i]['f072fprice']));
            var gstamount = (this.ConvertToFloat(this.budgetvirementList[i]['f072fgst'])) / 100 * (totalamount);
            this.budgetvirementList[i]['f072ftotal'] = gstamount + totalamount;
            finaltotal = finaltotal + this.budgetvirementList[i]['f072ftotal'];
        }
        this.budgetvirementDataheader['f071finvoiceTotal'] = finaltotal;
    }
    deletebudgetvirement(object) {
        var cnf = confirm("Do you really want to delete");
        if (cnf == true) {
            var index = this.newbudgetvirementList.indexOf(object);
            this.deleteList.push(this.newbudgetvirementList[index]['f056fidDetails']);
            let deleteObject = {};
            deleteObject['id'] = this.deleteList;
            this.BudgetVirementService.getDeleteItems(deleteObject).subscribe(
                data => {
                }, error => {
                    console.log(error);
                });
            if (index > -1) {
                this.deletedIds = object['f056fid'];
                this.newbudgetvirementList.splice(index, 1);
            }
            this.onBlurMethod();
        }
    }
    saveBudgetviremnt() {
        if (this.budgetvirementDataheader['f056fverimentType'] == undefined) {
            alert('Select Virement Type');
            return false;
        }
        if (this.budgetvirementDataheader['f056ftotalAmount'] == undefined) {
            alert('Enter Amount(RM)');
            return false;
        }
        if (this.budgetvirementDataheader['f056freason'] == undefined) {
            alert('Enter Description');
            return false;
        }
        if (this.budgetvirementDataheader['f056fidFinancialYear'] == undefined) {
            alert('Select Budget Year');
            return false;
        }
        if (this.budgetvirementDataheader['f056ffromFundCode'] == undefined) {
            alert('Select Fund');
            return false;
        }
        if (this.budgetvirementDataheader['f056ffromActivityCode'] == undefined) {
            alert('Select Activity Code');
            return false;
        }
        if (this.budgetvirementDataheader['f056ffromDepartment'] == undefined) {
            alert('Select Cost Center');
            return false;
        }

        if (this.budgetvirementDataheader['f056ffromAccountCode'] == undefined) {
            alert('Select Account Code');
            return false;
        }
        if (this.showLastRow == false) {
            var addactivities = this.addBudgetvirement();
            if (addactivities == false) {
                return false;
            }
        }
        let amount = 0;
        for (var i = 0; i < this.newbudgetvirementList.length; i++) {
            amount = this.ConvertToFloat(amount) + this.ConvertToFloat(this.newbudgetvirementList[i]['f056famount']);
        }

        if (this.ConvertToFloat(this.budgetvirementDataheader['f056ftotalAmount']) < amount) {
            alert("Distributed amount is less than the Allocated Amount");
            return false;
        }
        if (this.ConvertToFloat(this.budgetvirementDataheader['f056ftotalAmount']) > amount) {
            alert("Distributed amount is greater than the Allocated Amount");
            return false;
        }
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        this.budgetvirementDataheader['veriment-details'] = this.newbudgetvirementList;
        this.budgetvirementDataheader['deletedIds'] = this.deletedIds;
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {

            this.BudgetVirementService.updateBudgetvirementItems(this.budgetvirementDataheader, this.id).subscribe(
                data => {
                    this.router.navigate(['budget/budgetvirement']);
                    alert("Budget Virement has been Updated Successfully");

                }, error => {
                    console.log(error);
                });
        } else {
            this.BudgetVirementService.insertBudgetvirementItems(this.budgetvirementDataheader).subscribe(
                data => {
                    this.router.navigate(['budget/budgetvirement']);
                    alert("Budget Virement has been Added Successfully");

                }, error => {
                    console.log(error);
                });
        }
    }
    addBudgetvirement() {
        this.budgetvirementData['f056fupdatedBy'] = 1;
        this.budgetvirementData['f056fcreatedBy'] = 1;
        if (this.budgetvirementData['f056ftoIdFinancialYear'] == undefined) {
            alert('Select Budget Year');
            return false;
        }
        if (this.budgetvirementData['f056ftoFundCode'] == undefined) {
            alert('Select Fund');
            return false;
        }
        if (this.budgetvirementData['f056ftoActivityCode'] == undefined) {
            alert('Select Activity Code');
            return false;
        }
        if (this.budgetvirementData['f056ftoAccountCode'] == undefined) {
            alert('Select Account Code');
            return false;
        }
        if (this.budgetvirementData['f056famount'] == undefined) {
            alert('Enter Amount');
            return false;
        }
        var confirmPop = confirm("Do you want to Add?");
        if (confirmPop == false) {
            return false;
        }
        var totalAmountAllocated = 0;

        for (var i = 0; i < this.newbudgetvirementList.length; i++) {
            totalAmountAllocated = this.ConvertToFloat(this.newbudgetvirementList[i]['f056famount']) + this.ConvertToFloat(totalAmountAllocated);
        }
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        var dataofCurrentRow = this.budgetvirementData;
        this.newbudgetvirementList.push(dataofCurrentRow);
        this.budgetvirementData = {};
        this.showIcons();
    }
    onlyNumberKey(event) {
        if (event.charCode == 46) {
        } else {
            return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
        }
    }
    maximize() {
        $("#myModal").addClass("modal");
        $("#overlay1").show();
        $(".maximize-icon").addClass("hidden");
        $(".minimize-icon").removeClass("hidden");
        $(".modal-backdrop").show();

    }
    minimize() {
        $("#myModal").removeClass("modal");
        $("#overlay1").hide();
        $(".maximize-icon").removeClass("hidden");
        $(".minimize-icon").addClass("hidden");
        $(".modal-backdrop").hide();
        $('#myModal').show();

    }
}
