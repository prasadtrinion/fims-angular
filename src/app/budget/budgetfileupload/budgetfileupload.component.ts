import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { BudgetfiluploadService } from '../service/budgetfileupload.service'
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient} from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Component({
    selector: 'BudgetfileuploadComponent',
    templateUrl: 'budgetfileupload.component.html'
})
export class BudgetfileuploadComponent implements OnInit {
    uploadData= {};
    childTable;
    childColumn;
    parentTable;
    parentColumn;
    tableList = [];
    childList =[];
    parentList = [];
    file:any;
    fileName; 
    url: string = environment.api.filebase;
    constructor(
        private BudgetfiluploadService: BudgetfiluploadService,
        private route: ActivatedRoute,
        private router: Router,
        private httpClient: HttpClient
    ) { }

    ngOnInit() {
        this.BudgetfiluploadService.getTables().subscribe(
            data => {
                this.tableList = data['result'];
            }, error => {
                console.log(error);
            });
      }
      getChildColumns(){
        this.BudgetfiluploadService.getColumns(this.childTable).subscribe(
            data => {
                this.childList = data['result'];
            }, error => {
                console.log(error);
            });
      }
      getParentColumns(){
        this.BudgetfiluploadService.getColumns(this.parentTable).subscribe(
            data => {
                this.parentList = data['result'];
            }, error => {
                console.log(error);
            });
      }
    uploadFile() {
        let fd = new FormData();
        fd.append('file',this.file);
        this.httpClient.post(this.url, fd)
        .subscribe(
            data => {
                this.fileName = data['name'];
                this.BudgetfiluploadService.importfile(data).subscribe(
                    data => {
                        alert('Data imported successfully');
                    }, error => {
                        console.log(error);
                    });
            }, error => {
                console.log(error);
            });
    }
    fileSelected(event){
        this.file = event.target.files[0];
        console.log(this.file);
    }
}