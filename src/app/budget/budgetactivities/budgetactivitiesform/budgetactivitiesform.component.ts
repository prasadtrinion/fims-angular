import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BudgetactivitiesService } from '../../service/budgetactivities.service'
import { DepartmentService } from '../../../generalsetup/service/department.service'
import { GlcodeService } from '../../../generalsetup/service/glcode.service'
import { FundService } from '../../../generalsetup/service/fund.service';
import { AccountcodeService } from '../../../generalsetup/service/accountcode.service';
import { ActivitycodeService } from '../../../generalsetup/service/activitycode.service';
import { BudgetyearService } from '../../service/budgetyear.service';
import { PurchaserequistionentryService } from "../../../procurement/service/purchaserequistionentry.service";
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'BudgetactivitiesFormComponent',
    templateUrl: 'budgetactivitiesform.component.html'
})

export class BudgetactivitiesFormComponent implements OnInit {
    budgetactivitiesList = [];
    budgetactivitiesData = {};
    budgetactivitiesDataheader = {};
    newbudgetactivitiesList = [];
    finantialyearList = [];
    finantialyearData = {};
    f054fidFinancialyear = {};
    configList = [];
    configData = {};
    glcodeList = [];
    DeptList = [];
    newglcodeList = [];
    deptData = {};
    fundList = [];
    activitycodeList = [];
    accountcodeList = [];
    budgetcostcenterList = [];
    budgetcostcenterLists = [];
    socodeList = [];
    id: number;
    saveBtnDisable: boolean;
    allocatedAmount;
    allocationAmount: number;
    deletedIds = [];
    deleteList = [];
    balanceAmount: number;
    totalAmount: number;
    ajaxCount: number;
    idview: number;
    budgettype = [];
    varimenttype = [];
    initalDeptServicelist = [];
    configListOfDepartmentSplit: number;
    viewDisabled: boolean;
    remainingAmount: number;
    showLastRow: boolean;
    showLastRowMinus: boolean;
    showLastRowPlus: boolean;
    listshowLastRowMinus: boolean;
    listshowLastRowPlus: boolean;
    currentAllocation: any = 0;
    remainingAllocation: any;
    constructor(

        private BudgetactivitiesService: BudgetactivitiesService,
        private DepartmentService: DepartmentService,
        private GlcodeService: GlcodeService,
        private BudgetyearService: BudgetyearService,
        private FundService: FundService,
        private AccountcodeService: AccountcodeService,
        private ActivitycodeService: ActivitycodeService,
        private PurchaserequistionentryService: PurchaserequistionentryService,
        private route: ActivatedRoute,
        private spinner: NgxSpinnerService,
        private router: Router

    ) { }
    increaseCurrentAllocation(amount) {
        if (!isNaN(amount)) {
            this.currentAllocation = parseFloat(this.currentAllocation) + parseFloat(amount);
            this.remainingAllocation = parseFloat(this.allocatedAmount) - parseFloat(this.currentAllocation);
        }
    }
    decreaseCurrentAllocation(amount) {
        if (!isNaN(amount)) {
            this.currentAllocation = parseFloat(this.currentAllocation) - parseFloat(amount);
            this.remainingAllocation = parseFloat(this.allocatedAmount) - parseFloat(this.currentAllocation);
        }
    }
    ngDoCheck() {

        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.saveBtnDisable = false;
        this.route.paramMap.subscribe((data) => this.idview = + data.get('idview'));
        if (this.ajaxCount == 0) {
            this.spinner.hide();
        }

        if (this.ajaxCount == 0 && this.newbudgetactivitiesList.length > 0) {
            this.ajaxCount = 20;
            if (this.newbudgetactivitiesList[0]['f054fapprovalStatus'] == 1 || this.newbudgetactivitiesList[0]['f054fapprovalStatus'] == 2) {
                this.saveBtnDisable = true;
                this.viewDisabled = true;
                this.listshowLastRowPlus = false;

                $("#target input,select").prop("disabled", true);
                $("#target1 input,select").prop("disabled", true);
            }
        }
        if (this.ajaxCount == 0 && this.newbudgetactivitiesList.length > 0) {
            this.ajaxCount = 20;
            if (this.idview > 1) {
                this.saveBtnDisable = true;
                this.viewDisabled = true;
                this.listshowLastRowPlus = false;

                $("#target input,select").prop("disabled", true);
                $("#target1 input,select").prop("disabled", true);
            }
        }
    }
    deletemodule() {
        this.showLastRow = true;
        this.showIcons();
    }

    showIcons() {
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.newbudgetactivitiesList.length > 0) {
            this.showLastRowMinus = true;
            this.showLastRowPlus = true;
            this.listshowLastRowMinus = true;
        }

        if (this.newbudgetactivitiesList.length > 1) {
            if (this.showLastRow == false) {
                this.listshowLastRowPlus = false;
                this.listshowLastRowMinus = true;
                this.showLastRowPlus = true;

            } else {
                this.listshowLastRowPlus = true;
                this.listshowLastRowMinus = false;

            }
        }
        if (this.newbudgetactivitiesList.length == 1 && this.showLastRow == false) {
            this.listshowLastRowMinus = true;
            this.listshowLastRowPlus = false;

        }
        if (this.newbudgetactivitiesList.length == 1 && this.showLastRow == true) {
            this.listshowLastRowMinus = false;
            this.listshowLastRowPlus = true;

        }
        if (this.newbudgetactivitiesList.length == 0 && this.showLastRow == false) {
            this.showLastRowMinus = false;
            this.showLastRowPlus = true;
        }
        if (this.newbudgetactivitiesList.length == 0 && this.showLastRow == true) {
            this.showLastRowMinus = true;
            this.showLastRowPlus = false;
        }

        var totalAmountAllocated = 0;
        this.balanceAmount = 0;

        for (var i = 0; i < this.newbudgetactivitiesList.length; i++) {
            totalAmountAllocated = this.ConvertToFloat(this.newbudgetactivitiesList[i]['f054famount']) + this.ConvertToFloat(totalAmountAllocated);
        }
        this.balanceAmount = this.allocatedAmount - totalAmountAllocated;

        if (this.viewDisabled == true || this.newbudgetactivitiesList[0]['f054fapprovalStatus'] == 1 || this.newbudgetactivitiesList[0]['f054fapprovalStatus'] == 2) {
            this.listshowLastRowPlus = false;
        }

    }
    showNewRow() {
        var confirmPop = confirm("Do you want to Add?");
        if (confirmPop == false) {
            return false;
        }
        this.showLastRow = false;
        this.showIcons();
    }
    ngOnInit() {
        this.spinner.show();

        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        let budgetypeobj = {};
        budgetypeobj['name'] = 'Continues';
        budgetypeobj['id'] = 1;
        this.budgettype.push(budgetypeobj);
        budgetypeobj = {};
        budgetypeobj['name'] = 'Annum';
        budgetypeobj['id'] = 2;
        this.budgettype.push(budgetypeobj);
        this.ajaxCount = 0;

        if (this.id < 1) {
            this.showLastRow = false;
            this.showLastRowMinus = false;
            this.showLastRowPlus = true;
        } else {
            this.showLastRow = true;
            this.ajaxCount++;
            this.FundService.getItems().subscribe(
                data => {
                    this.ajaxCount--;
                    this.fundList = data['result']['data'];
                }
            );

        }
        this.viewDisabled = false;

        //Activity dropdown
        this.ajaxCount++;
        this.ActivitycodeService.getItemsByLevel().subscribe(
            data => {
                this.ajaxCount--;
                this.activitycodeList = data['result']['data'];
            }
        );

        //Account dropdown
        this.ajaxCount++;
        this.AccountcodeService.getItemsByLevel().subscribe(
            data => {
                this.ajaxCount--;
                this.accountcodeList = data['result']['data'];
            }
        );
        //SoCode Dropdown
        this.ajaxCount++;
        this.PurchaserequistionentryService.getSocodeItems().subscribe(
            data => {
                this.ajaxCount--;
                this.socodeList = data['result'];
            }, error => {
                console.log(error);
            });

        //costcenter

        this.allocatedAmount = 0;
        this.allocationAmount = 0;

        this.budgetactivitiesDataheader['f054fstatus'] = 1;
        // financial year dropdown

        this.ajaxCount++;
        this.BudgetyearService.getActiveItems().subscribe(
            data => {
                this.ajaxCount--;
                this.finantialyearList = data['result'];
                let curYear = new Date().getFullYear();
                for (var i = 0; i < this.finantialyearList.length; i++) {
                    if (curYear == parseInt(this.finantialyearList[i]['f110fyear'])) {
                        this.budgetactivitiesDataheader['f054fidFinancialyear'] = this.finantialyearList[i]['f110fid'];
                        this.getListOfPendingCostCenter();
                        break;
                    }
                }
                if (data['result'].length == 1) {
                    this.budgetactivitiesDataheader['f054fidFinancialyear'] = data['result'][0]['f110fid'];
                    this.getListOfPendingCostCenter();

                }

            }, error => {
                console.log(error);
            });

        //Fund dropdown
        this.route.paramMap.subscribe((data) => this.idview = + data.get('idview'));
        this.GlcodeService.getGlItems().subscribe(
            data => {
                this.ajaxCount--;
                this.glcodeList = data['result']['data'];
                if (this.id > 0) {
                    this.ajaxCount++;
                    this.BudgetactivitiesService.getItemsDetail(this.id).subscribe(
                        data => {
                            this.ajaxCount--;
                            if (data['result']['data'].length == 0) {
                                return false;
                            }
                            this.budgetactivitiesDataheader['f054fidFinancialyear'] = data['result']['data'][0]['f054fidFinancialyear'];
                            this.budgetactivitiesDataheader['f054fdepartment'] = data['result']['data'][0]['f054fdepartment'];
                            this.budgetactivitiesDataheader['f054ffund'] = data['result']['data'][0]['f054ffund'];
                            this.newbudgetactivitiesList = data['result']['data'];
                            this.remainingAllocation =0;
                            this.getListOfPendingCostCenter();

                            for (var i = 0; i < this.newbudgetactivitiesList.length; i++) {
                                // this.newbudgetactivitiesList[i]['f054fdepartmentCode'] = parseInt(this.newbudgetactivitiesList[i]['f054fdepartmentCode']);
                                this.newbudgetactivitiesList[i]['f054famount'] = this.ConvertToFloat(this.newbudgetactivitiesList[i]['f054famount']).toFixed(2);
                                this.newbudgetactivitiesList[i]['f054fq1'] = this.ConvertToFloat(this.newbudgetactivitiesList[i]['f054fq1']).toFixed(2);
                                this.newbudgetactivitiesList[i]['f054fq2'] = this.ConvertToFloat(this.newbudgetactivitiesList[i]['f054fq2']).toFixed(2);
                                this.newbudgetactivitiesList[i]['f054fq3'] = this.ConvertToFloat(this.newbudgetactivitiesList[i]['f054fq3']).toFixed(2);
                                this.getAmount();
                                setTimeout(() => {
                                    this.showIcons();
                                    this.getTotal();
                                    this.getFund();
                                }, 2000);
                                this.getCostCenter();
                                if (this.idview == 2) {
                                    this.viewDisabled = true;
                                    $("#target input,select").prop("disabled", true);
                                    $("#target1 input,select").prop("disabled", true);
                                }
                            }
                        }, error => {
                            console.log(error);
                        });
                }
            }, error => {
                console.log(error);
            });
    }
    getFund() {
        this.budgetactivitiesData['f054ffundCode'] = this.budgetactivitiesDataheader['f054ffund'];
    }
    getAllocatedAmountBasedOnFinancialYearAndDepartment(idfinancial, iddepat) {

    }
    getListOfPendingCostCenter() {
        this.ajaxCount++;
        var idfinancialYear = this.budgetactivitiesDataheader['f054fidFinancialyear'];
        this.DepartmentService.pendingCostCenterByIdFinancialYear(idfinancialYear).subscribe(
            data => {
                this.ajaxCount--;
                var depArray = data['result']['Department'];
                this.budgetcostcenterLists = depArray;

                this.fundList = data['result']['Fund'];
            }, error => {
                console.log(error);
            });
    }

    getCostCenter() {
        this.ajaxCount++;
        var idfinancialYear = this.budgetactivitiesDataheader['f054fidFinancialyear'];
        this.DepartmentService.getCostCenterByIdFinancialYear(idfinancialYear).subscribe(
            data => {
                this.ajaxCount--;
                var depArray = data['result'];
                for (var i = 0; i < depArray.length; i++) {
                    depArray[i]['combined'] = depArray[i]['f015fdepartment_code'] + ' - ' + depArray[i]['f015fdepartment_name']
                }
                this.budgetcostcenterLists = depArray;
            }, error => {
            });
    }

    getAmount() {
        this.ajaxCount++;
        this.allocatedAmount = 0;
        let idfinancialYear = this.budgetactivitiesDataheader['f054fidFinancialyear'];
        let iddept = this.budgetactivitiesDataheader['f054fdepartment'];
        let idfund = this.budgetactivitiesDataheader['f054ffund'];
        this.BudgetactivitiesService.getAllocatedAmountBasedOnFinancialYearAndDepartment(idfinancialYear, iddept, idfund).subscribe(
            data => {
                this.ajaxCount--;
                if (data['result'].length == 0) {
                    this.allocatedAmount = 0;
                    alert('Selected Fund and Costcenter has no allocated amount');
                } else if (data['result']['data'].length == 0) {
                    this.allocatedAmount = 0;
                    alert('Selected Fund and Costcenter has no allocated amount');

                } else {
                    this.remainingAllocation = this.allocatedAmount = data['result']['data'][0]['f051famount'];
                    if(this.id > 0){
                        this.remainingAllocation = 0;
                        this.currentAllocation = this.allocatedAmount;
                    }

                }
            }, error => {
            });

        var departmentCode = iddept.substring(0, 3);;
        this.BudgetactivitiesService.getCostCenterBasedOnDepartmentCode(departmentCode).subscribe(
            data => {
                this.budgetcostcenterList = data['result']['data'];
            }, error => {
            });

    }
    getTotal() {
        this.totalAmount = (this.ConvertToFloat(this.allocatedAmount) - this.ConvertToFloat(this.balanceAmount));
    }
    saveBudgetactivities() {

        if (this.showLastRow == false) {
            var addactivities = this.addBudgetactivities(1);
            if (addactivities == false) {
                return false;
            }

        }
        if (this.budgetactivitiesDataheader['f054fidFinancialyear'] == undefined) {
            alert('Select Budget Year');
            return false;
        }
        if (this.budgetactivitiesDataheader['f054fdepartment'] == undefined) {
            alert('Select Cost Center');
            return false;
        }
        
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.budgetactivitiesDataheader['f054fidFinancialyear'];
        this.budgetactivitiesDataheader['f054fdepartment'];
        this.budgetactivitiesDataheader['activity-details'] = this.newbudgetactivitiesList;
        var allocatedamt = 0;
        for (var i = 0; i < this.newbudgetactivitiesList.length; i++) {
            if ((this.ConvertToFloat(this.newbudgetactivitiesList[i]['f054famount'])) !==
                ((this.ConvertToFloat(this.newbudgetactivitiesList[i]['f054fq1'])) +
                    (this.ConvertToFloat(this.newbudgetactivitiesList[i]['f054fq2'])) +
                    (this.ConvertToFloat(this.newbudgetactivitiesList[i]['f054fq3'])) +
                    (this.ConvertToFloat(this.newbudgetactivitiesList[i]['f054fq4'])))) {
                alert('Amount should be equal to its Quater amount');
                return false;
            }
            allocatedamt = this.ConvertToFloat(allocatedamt) + this.ConvertToFloat(this.newbudgetactivitiesList[i]['f054famount']);

        }

        if (this.allocatedAmount > allocatedamt) {
            alert('Budget Cost Center Allocated Amount is greater than allocated Amount');
            return false;
        }
        if (this.allocatedAmount < allocatedamt) {
            alert('Budget Cost Center Allocated Amount is lesser than allocated Amount');
            return false;

        }
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        this.budgetactivitiesDataheader['f054ftotalAmount'] = allocatedamt;
        this.budgetactivitiesDataheader['deletedIds'] = this.deletedIds;

        if (this.id > 0) {
            this.BudgetactivitiesService.updateBudgetactivitiesItems(this.budgetactivitiesDataheader, this.id).subscribe(
                data => {
                    this.router.navigate(['budget/budgetactivities']);
                    alert("Budget Allocation has been Updated Successfully")
                }, error => {

                    alert("Fund,Department Already Exist");
                });
        } else {
            this.BudgetactivitiesService.insertBudgetactivitiesItems(this.budgetactivitiesDataheader).subscribe(
                data => {
                    if (data['status'] == '409') {
                        alert("This Cost Center has already been allocated with Budget")
                    } else {
                        this.router.navigate(['budget/budgetactivities']);
                        alert("Budget Allocation has been Added Successfully")
                    }
                }, error => {
                    alert("Fund,Department Already Exist");
                });
        }
    }
    ConvertToFloat(val) {
        return parseFloat(val);
    }
    deletebudgetactivites(object) {
        var cnf = confirm("Do you really want to delete");
        if (cnf == true) {
            var index = this.newbudgetactivitiesList.indexOf(object);
            this.deleteList.push(this.newbudgetactivitiesList[index]['f054fidDetails']);
            let deleteObject = {};
            deleteObject['id'] = this.deleteList;
            this.BudgetactivitiesService.getDeleteItems(deleteObject).subscribe(
                data => {
                }, error => {
                    console.log(error);
                });
            if (index > -1) {
                this.deletedIds.push(object['f054fidDetails']);
                this.newbudgetactivitiesList.splice(index, 1);
            }
            this.getBalanceAmount();
            this.showIcons();
            this.getTotal();
        }
    }
    getBalanceAmount() {
        var totalAmountAllocated = 0;

        for (var i = 0; i < this.newbudgetactivitiesList.length; i++) {
            totalAmountAllocated = this.ConvertToFloat(this.newbudgetactivitiesList[i]['f054fbalance']) + this.ConvertToFloat(totalAmountAllocated);
        }
        if (this.budgetactivitiesData['f054fbalance'] > 0) {
            this.allocationAmount = this.allocationAmount + this.ConvertToFloat(this.budgetactivitiesData['f054fbalance']);
        }
        this.balanceAmount = (this.ConvertToFloat(this.allocatedAmount) - this.ConvertToFloat(totalAmountAllocated));

    }
    addBudgetactivities(ids) {

        if (this.budgetactivitiesDataheader['f054fidFinancialyear'] == undefined) {
            alert('Select Budget Year');
            return false;
        }
        if (this.budgetactivitiesDataheader['f054fdepartment'] == undefined) {
            alert('Select Cost Center');
            return false;
        }
        if (this.budgetactivitiesData['f054ffundCode'] == undefined) {
            alert('Select Fund');
            return false;
        }
        if (this.budgetactivitiesData['f054factivityCode'] == undefined) {
            alert('Select Activity Code');
            return false;
        }
        if (this.budgetactivitiesData['f054fdepartmentCode'] == undefined) {
            alert('Select Cost Center');
            return false;
        }
        if (this.budgetactivitiesData['f054ffundCode'] != this.budgetactivitiesDataheader['f054ffund']) {
            alert('Cannot allocate to different Funds');
            return false;
        }
        if (this.budgetactivitiesData['f054faccountCode'] == undefined) {
            alert('Select Account Code');
            return false;
        }
        if (this.budgetactivitiesData['f054fsoCode'] == undefined) {
            alert('Enter So Code');
            return false;
        }
        if (this.budgetactivitiesData['f054fbudgetType'] == undefined) {
            alert('Select Budget Type ');
            return false;
        }
        if (this.budgetactivitiesData['f054famount'] == undefined) {
            alert('Enter Amount ');
            return false;
        }
        if (this.budgetactivitiesData['f054fq1'] == undefined) {
            this.budgetactivitiesData['f054fq1'] = 0;
        }
        if (this.budgetactivitiesData['f054fq2'] == undefined) {
            this.budgetactivitiesData['f054fq2'] = 0;
        }
        if (this.budgetactivitiesData['f054fq3'] == undefined) {
            this.budgetactivitiesData['f054fq3'] = 0;
        }

        if (this.budgetactivitiesData['f054fq4'] == undefined) {
            this.budgetactivitiesData['f054fq4'] = 0;
        }
        if (ids == 0) {
            var confirmPop = confirm("Do you want to Add?");
            if (confirmPop == false) {
                return false;
            }
        }
        if ((this.ConvertToFloat(this.budgetactivitiesData['f054famount'])) !==
            ((this.ConvertToFloat(this.budgetactivitiesData['f054fq1'])) +
                (this.ConvertToFloat(this.budgetactivitiesData['f054fq2'])) +
                (this.ConvertToFloat(this.budgetactivitiesData['f054fq3'])) +
                (this.ConvertToFloat(this.budgetactivitiesData['f054fq4'])))) {
            alert('Amount should be equal to its Quater amount');
            return false;
        }
        var totalAmountAllocated = 0;
        for (var i = 0; i < this.newbudgetactivitiesList.length; i++) {

            if (this.newbudgetactivitiesList[i]['f054faccountCode'] == this.budgetactivitiesData['f054faccountCode']) {
                alert("This Account Code has already been allocated, Change Account Code");
                return false;
            }
            if (this.newbudgetactivitiesList[i]['f054fdepartmentCode'] == this.budgetactivitiesData['f054fdepartmentCode']) {
                alert("This Cost Center Code has been already allocated Change the Cost Center Code");
                return false;
            }

            totalAmountAllocated = this.ConvertToFloat(this.newbudgetactivitiesList[i]['f054famount']) + this.ConvertToFloat(totalAmountAllocated);
        }
        this.remainingAmount = this.allocatedAmount - totalAmountAllocated;
        if (totalAmountAllocated + this.ConvertToFloat(this.budgetactivitiesData['f054famount']) > this.allocatedAmount) {
        }
        this.allocationAmount = this.allocationAmount + this.ConvertToFloat(this.budgetactivitiesData['f054famount']);
        this.balanceAmount = (this.ConvertToFloat(this.allocatedAmount) - this.ConvertToFloat(this.allocationAmount));
        this.budgetactivitiesData['f054fupdatedBy'] = 1;
        this.budgetactivitiesData['f054fcreatedBy'] = 1;
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        var dataofCurrentRow = this.budgetactivitiesData;
        this.newbudgetactivitiesList.push(dataofCurrentRow);
        this.budgetactivitiesData = {};
        this.getFund();
        this.showIcons();
        this.getTotal();
    }
    onlyNumberKey(event) {
        if (event.charCode == 46) {
        } else {
            return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
        }
    }
    maximize() {
        $("#myModal").addClass("modal");
        $("#overlay1").show();
        $(".maximize-icon").addClass("hidden");
        $(".minimize-icon").removeClass("hidden");
        $(".modal-backdrop").show();

    }
    minimize() {
        $("#myModal").removeClass("modal");
        $("#overlay1").hide();
        $(".maximize-icon").removeClass("hidden");
        $(".minimize-icon").addClass("hidden");
        $(".modal-backdrop").hide();
        $('#myModal').show();

    }

}
