import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { BudgetactivitiesService } from '../service/budgetactivities.service'
import { QueryBuilderConfig } from 'angular2-query-builder';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'Budgetactivities',
    templateUrl: 'budgetactivities.component.html'
})

export class BudgetactivitiesComponent implements OnInit {
    budgetactivitiesList = [];
    budgetcostcenterList = [];
    budgetactivitiesData = {};
    id: number;
    allocatedAmount: number;
    constructor(
        private BudgetactivitiesService: BudgetactivitiesService,
        private spinner: NgxSpinnerService


    ) {
    }
    query = {
        condition: 'and',
        rules: [
            { field: 'age', operator: '<=', value: 'Bob' },
            { field: 'gender', operator: '>=', value: 'm' }
        ]
    };

    config: QueryBuilderConfig = {
        fields: {
            age: { name: 'Age', type: 'number' },
            gender: {
                name: 'Gender',
                type: 'category',
                options: [
                    { name: 'Male', value: 'm' },
                    { name: 'Female', value: 'f' }
                ]
            }
        }
    }
    getSql() {
        console.log(this.config);
    }

    ngOnInit() {
        this.spinner.show();

        this.BudgetactivitiesService.getItems().subscribe(
            data => {
                this.spinner.hide();

                let activityData = [];
                activityData = data['data']['data'];
                for (var i = 0; i < activityData.length; i++) {
                    if (activityData[i]['f054fapprovalStatus'] == 0) {
                        activityData[i]['f054fapprovalStatus'] = 'Pending';
                    } else if (activityData[i]['f054fapprovalStatus'] == 1) {
                        activityData[i]['f054fapprovalStatus'] = 'Approved';
                    } else {
                        activityData[i]['f054fapprovalStatus'] = 'Rejected';
                    }
                }
                this.budgetactivitiesList = activityData;

            }, error => {
                console.log(error);
            });
        this.allocatedAmount = 100;
    }
}