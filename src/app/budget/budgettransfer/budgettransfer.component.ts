import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { BudgettransferService } from '../service/budgettransfer.service'


@Component({
    selector: 'Budgettransfer',
    templateUrl: 'budgettransfer.component.html'
})

export class BudgettransferComponent implements OnInit {
    BudgettransferList =  [];
    BudgettransferData = {};
    id: number

    constructor(
        
        private BudgettransferService: BudgettransferService

    ) { }

    ngOnInit() { 
        
        this.BudgettransferService.getItems().subscribe(
            data => {
                this.BudgettransferList = data['result']['data'];
        }, error => {
            console.log(error);
        });
    }
}