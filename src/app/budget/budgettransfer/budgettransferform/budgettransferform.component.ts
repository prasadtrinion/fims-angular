import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { BudgettransferService } from '../../service/budgettransfer.service'
import { DepartmentService } from '../../../generalsetup/service/department.service'


@Component({
    selector: 'BudgettransferFormComponent',
    templateUrl: 'budgettransferform.component.html'
})

export class BudgettransferFormComponent implements OnInit {

    BudgettransferList = [];
    BudgettransferData = {};
    DeptList = [];
    DeptData = {};
    id: number;
    constructor(
        private BudgettransferService: BudgettransferService,
        private DepartmentService: DepartmentService,
        private route: ActivatedRoute,
        private router: Router

    ) { }

    ngOnInit() {

        this.BudgettransferData['f056fstatus'] = 1;

        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        //Department dropdown
        this.DepartmentService.getItems().subscribe(
            data => {
                this.DeptList = data['result']['data'];
            }, error => {
                console.log(error);
            });

        if (this.id > 0) {
            this.BudgettransferService.getItemsDetail(this.id).subscribe(
                data => {
                    this.BudgettransferData['f056fstatus'] = 1;
                    this.BudgettransferData = data['result'];
                    if (data['result'].f056fstatus === true) {
                        console.log(data);
                        this.BudgettransferData['f056fstatus'] = 1;
                    } else {
                        this.BudgettransferData['f056fstatus'] = 0;
                    }
                }, error => {
                    console.log(error);
                });
        }
    }
    addBudgettransfer() {
        console.log(this.BudgettransferData);
        this.BudgettransferData['f056fupdatedBy'] = 1;
        this.BudgettransferData['f056fcreatedBy'] = 1;

        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.BudgettransferService.updateBudgettransferItems(this.BudgettransferData, this.id).subscribe(
                data => {
                    this.router.navigate(['budget/budgettransfer']);
                }, error => {
                    console.log(error);
                });
        } else {
            this.BudgettransferService.insertBudgettransferItems(this.BudgettransferData).subscribe(
                data => {
                    this.router.navigate(['budget/budgettransfer']);

                }, error => {
                    console.log(error);
                });
        }
    }
}