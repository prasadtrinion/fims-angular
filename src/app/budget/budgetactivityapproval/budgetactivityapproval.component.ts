import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Injector } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BudgetactivityapprovalService } from '../../budget/service/budgetactivityapproval.service'
import { BudgetcostcenterService } from '../../budget/service/budgetcostcenter.service'
import { NgxSpinnerService } from 'ngx-spinner';
@Component({
    selector: 'Budgetactivityapproval',
    templateUrl: 'budgetactivityapproval.component.html'
})
export class BudgetactivityapprovalComponent implements OnInit {
    budgetactivityapprovalList = [];
    budgetactivityapprovalData = {};
    approvalData={};
    id: number;
    selectAllCheckbox: false;
    budgetactivitiesList = [];
    budgetcostcenterList = [];

    constructor(
        private BudgetactivityapprovalService: BudgetactivityapprovalService,
        private route: ActivatedRoute,
        private BudgetcostcenterService: BudgetcostcenterService,
        private router: Router,
        private spinner: NgxSpinnerService


    ) { }
    ngOnInit() {
        
        this.getListData();

    }
    getListData() {
        this.spinner.show();
        this.BudgetcostcenterService.getItems().subscribe(
            data => {
                this.spinner.hide();
                this.budgetcostcenterList = data['result']['data'];
                this.BudgetactivityapprovalService.getItemsDetail().subscribe(
                    data => {
                        this.budgetactivityapprovalList = data['result'];

                        for (var i = 0; i < this.budgetactivityapprovalList.length; i++) {
                            for (var j = 0; j < this.budgetcostcenterList.length; j++) {
                                if (parseInt(this.budgetactivityapprovalList[i]['f054fidDepartment']) == parseInt(this.budgetcostcenterList[j]['f051fidDepartment'])) {
                                    if (parseInt(this.budgetactivityapprovalList[i]['f054fidFinancialyear']) == parseInt(this.budgetcostcenterList[j]['f051fidFinancialyear'])) {
                                        this.budgetactivityapprovalList[i]['allocatedAmount'] = this.budgetcostcenterList[j]['f051famount'];
                                        this.budgetactivityapprovalList[i]['pendingAmount'] = parseInt(this.budgetactivityapprovalList[i]['allocatedAmount']) - parseInt(this.budgetactivityapprovalList[i]['f054ftotalAmount']);
                                    }
                                }
                            }
                        }
                    }, error => {
                        console.log(error);
                    });
            }, error => {
                console.log(error);
            });

    }
    budgetactivityApprovalListData() {
        var approvalArray = [];
        for (var i = 0; i < this.budgetactivityapprovalList.length; i++) {
            if (this.budgetactivityapprovalList[i]['approvalstatus']==true) {
                approvalArray.push(this.budgetactivityapprovalList[i]['f054fid']);
            }
        }
        if(approvalArray.length<1) {
            alert("Select atleast one Cost Center to Approve");
            return false;
        }
        var confirmPop = confirm("Do you want to Approve?");
        if (confirmPop == false) {
            return false;
        }
        var budgetActivityDataObject = {}
        budgetActivityDataObject['id'] = approvalArray;
        budgetActivityDataObject['status'] = 1;
        budgetActivityDataObject['reason'] = '';

        this.BudgetactivityapprovalService.updateBudgetactivityApproval(budgetActivityDataObject).subscribe(
            data => {
                this.getListData();
                this.approvalData['reason']='';
                alert("Approved Successfully")
            }, error => {
                console.log(error);
            });
    }
    regectbudgetactivityApprovalListData() {
        var approvalArray = [];
        for (var i = 0; i < this.budgetactivityapprovalList.length; i++) {
            if (this.budgetactivityapprovalList[i]['approvalstatus'] == true) {
                approvalArray.push(this.budgetactivityapprovalList[i]['f054fid']);
            }
        }
        if(approvalArray.length<1) {
            alert(" Select atleast one Cost Center to Reject");
            return false;
        }
        if (this.budgetactivityapprovalData['reason'] == undefined) {
            alert(' Enter the Description');
            return false;
        }
        var confirmPop = confirm("Do you want to Reject?");
        if (confirmPop == false) {
            return false;
        }
        var budgetActivityDataObject = {}
        budgetActivityDataObject['id'] = approvalArray;
        budgetActivityDataObject['status'] = '2';
        budgetActivityDataObject['reason'] = this.budgetactivityapprovalData['reason'];
        this.BudgetactivityapprovalService.updateBudgetactivityApproval(budgetActivityDataObject).subscribe(
            data => {
                this.getListData();
                this.budgetactivityapprovalData['reason']='';
                alert("Rejected Successfully");
            }, error => {
                console.log(error);
            });
    }
}