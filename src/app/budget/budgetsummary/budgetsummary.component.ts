import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { BudgetsummaryService } from '../service/budgetsummary.service'
import { DepartmentService } from '../../generalsetup/service/department.service'
import { FundService } from '../../generalsetup/service/fund.service';
import { BudgetyearService } from "../service/budgetyear.service";
import { NgxSpinnerService } from 'ngx-spinner';


@Component({
    selector: 'Budgetsummary',
    templateUrl: 'budgetsummary.component.html'
})

export class BudgetsummaryComponent implements OnInit {
    budgetsummaryList = [];
    budgetsummaryData = {};
    finantialyearList = [];
    ajaxCount: number;
    itemList = [];
    fyearList = [];
    fundList = [];
    budgetcostcenterLists = [];
    id: number;
    constructor(
        private BudgetsummaryService: BudgetsummaryService,
        private FundService: FundService,
        private BudgetyearService: BudgetyearService,
        private DepartmentService: DepartmentService,
        private spinner: NgxSpinnerService

    ) { }

    ngOnInit() {
        //Fund dropdown
        this.ajaxCount++;
        this.FundService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.fundList = data['result']['data'];
            }
        );
        //Budget dropdown
        this.ajaxCount++;
        this.BudgetyearService.getActiveItems().subscribe(
            data => {
                this.ajaxCount--;
                this.finantialyearList = data['result'];
                let curYear = new Date().getFullYear();
                for (var i = 0; i < this.finantialyearList.length; i++) {
                    if (curYear == parseInt(this.finantialyearList[i]['f110fyear'])) {
                        this.budgetsummaryData['FinancialYear'] = this.finantialyearList[i]['f110fid'];
                        this.getListOfPendingCostCenter();
                        break;
                    }
                }
                if (data['result'].length == 1) {
                        this.budgetsummaryData['FinancialYear'] =  data['result'][0]['f110fid'];
                    this.getListOfPendingCostCenter();

                }
            }, error => {
                //console.log(error);
            });

        this.ajaxCount++;
        this.BudgetsummaryService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.budgetsummaryList = data['result']['data'];
                for(var i=0;i<this.budgetsummaryList.length;i++){
                    this.budgetsummaryList[i]['f108fbudget_pending'] = this.budgetsummaryList[i]['f108fallocated_amount'] - this.budgetsummaryList[i]['f108fbudget_commitment'] - this.budgetsummaryList[i]['f108fbudget_expensed'];
                }
            }, error => {
                console.log(error);
            });
    }

    ConvertToInt(val) {
        return parseInt(val);
    }
    getAllocatedAmountBasedOnFinancialYearAndDepartment(idfinancial, iddepat) {

    }
    getListOfPendingCostCenter() {
        this.spinner.show();

        this.ajaxCount++;
        var idfinancialYear = this.budgetsummaryData['FinancialYear'];
        this.DepartmentService.pendingCostCenterByIdFinancialYear(idfinancialYear).subscribe(
            data => {

                this.ajaxCount--;
                var depArray = data['result']['Department'];
                this.budgetcostcenterLists = depArray;

                this.fundList = data['result']['Fund'];
                this.spinner.hide();

                console.log(data);
            }, error => {
                //console.log(error);
            });
    }
    addBudgetsummary() {
        console.log(this.budgetsummaryData);
        this.BudgetsummaryService.insertBudgetsummaryItems(this.budgetsummaryData).subscribe(
            data => {
                this.budgetsummaryList = data['result'];
                for(var i=0;i<this.budgetsummaryList.length;i++){
                    this.budgetsummaryList[i]['f108fbudget_pending'] = this.budgetsummaryList[i]['f108fallocated_amount'] - this.budgetsummaryList[i]['f108fbudget_commitment'] - this.budgetsummaryList[i]['f108fbudget_expensed'];
                }
            }, error => {
                console.log(error);
            });
    }
    printDivPage() {
        console.log("asdf");
        var divToPrint = document.getElementById('DivIdToPrint');
        var newWin = window.open('', 'Print-Window');
        newWin.document.open();
        newWin.document.write('<html><body onload="window.print()">' + divToPrint.innerHTML + '</body></html>');
        newWin.document.close();
        setTimeout(function () { newWin.close(); }, 10);
    }
}