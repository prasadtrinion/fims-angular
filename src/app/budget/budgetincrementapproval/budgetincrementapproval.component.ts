import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Injector } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BudgetincrementapprovalService } from '../../budget/service/budgetincrementapproval.service'
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'Budgetincrementapproval',
    templateUrl: 'budgetincrementapproval.component.html'
})

export class BudgetincrementapprovalComponent implements OnInit {
    budgetincrementapprovalList = [];
    budgetincrementapprovalData = {};
    approvaloneData = {};
    id: number;
    selectAllCheckbox: false;
    constructor(
        private BudgetincrementapprovalService: BudgetincrementapprovalService,
        private route: ActivatedRoute,
        private router: Router,
        private spinner: NgxSpinnerService
    ) { }
    ngOnInit() {
        this.getListData();
    }
    getListData() {
        this.spinner.show();
        this.BudgetincrementapprovalService.getItemsDetail().subscribe(
            data => {
                this.spinner.hide();

                this.budgetincrementapprovalList = data['result'];
            }, error => {
                console.log(error);
            });
    }
    budgetincrementApprovalListData() {
        var approvalArray = [];
        for (var i = 0; i < this.budgetincrementapprovalList.length; i++) {
            if (this.budgetincrementapprovalList[i]['approvalstatus'] == true) {
                approvalArray.push(this.budgetincrementapprovalList[i]['f055fid']);
            }
        }
        if (approvalArray.length < 1) {
            alert("Please select atleast one cost center to approve");
            return false;
        }
        var confirmPop = confirm("Do you want to Approve?");
        if (confirmPop == false) {
            return false;
        }
        var budgetincrementDataObject = {}
        budgetincrementDataObject['id'] = approvalArray;
        budgetincrementDataObject['status'] = '1';
        budgetincrementDataObject['reason'] = '';
        this.approvaloneData['reason'] = '';

        this.BudgetincrementapprovalService.updateBudgetincrementItems(budgetincrementDataObject).subscribe(
            data => {
                this.getListData();
                alert("Approved Successfully")
            }, error => {
                console.log(error);
            });
    }
    rejectbudgetincrementApprovalListData() {
        var approvalArray = [];
        for (var i = 0; i < this.budgetincrementapprovalList.length; i++) {
            if (this.budgetincrementapprovalList[i]['approvalstatus'] == true) {
                approvalArray.push(this.budgetincrementapprovalList[i]['f055fid']);
            }
        }
        if (approvalArray.length < 1) {
            alert("Please select atleast one cost center to Reject");
            return false;
        }
        if (this.approvaloneData['reason'] == undefined) {
            alert('Please Enter the Description');
            return false;
        }
        var confirmPop = confirm("Do you want to Reject?");
        if (confirmPop == false) {
            return false;
        }

        var budgetincrementDataObject = {}
        budgetincrementDataObject['id'] = approvalArray;
        budgetincrementDataObject['status'] = '2';
        budgetincrementDataObject['reason'] = this.approvaloneData['reason'];
        this.BudgetincrementapprovalService.updateBudgetincrementItems(budgetincrementDataObject).subscribe(
            data => {
                this.getListData();
                this.approvaloneData['reason'] = '';
                alert("Rejected Successfully")
            }, error => {
                console.log(error);
            });
    }
}