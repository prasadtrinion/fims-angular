import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SummarydetailService } from '../service/summarydetail.service'



@Component({
    selector: 'Summarydetail',
    templateUrl: 'summarydetail.component.html'
})

export class SummarydetailComponent implements OnInit {
    summarydetailList = [];
    summarydetailData = {};
   
    id: number;

    constructor(   
        private SummarydetailService: SummarydetailService,
        
    ) { }
    ngOnInit() { 
        this.SummarydetailService.getItems().subscribe(
            data => {
                this.summarydetailList = data['result']['data'];
        }, error => {
            console.log(error);
        });
    }
    addSummarydetail()
    {
           console.log(this.summarydetailData);
        this.SummarydetailService.insertSummarydetailItems(this.summarydetailData).subscribe(
            data => {
                this.summarydetailList = data['todos'];
            }, error => {
                console.log(error);
            });

    }
}