import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HttpClient,HTTP_INTERCEPTORS } from '@angular/common/http';
import { QueryBuilderModule } from "angular2-query-builder";
import { DataTableModule } from "angular-6-datatable";
import { MaterialModule } from '../material/material.module';
import { NguiAutoCompleteModule } from '@ngui/auto-complete';

import { BudgetactivitiesComponent } from './budgetactivities/budgetactivities.component';
import { BudgetactivitiesFormComponent } from './budgetactivities/budgetactivitiesform/budgetactivitiesform.component';

import { BudgetplanningComponent } from './budgetplanning/budgetplanning.component';
import { BudgetplanningFormComponent } from './budgetplanning/budgetplanningform/budgetplanningform.component';

import { BudgetgeneralinformationComponent } from './budgetgeneralinformation/budgetgeneralinformation.component';
import { BudgetgeneralinformationFormComponent } from './budgetgeneralinformation/budgetgeneralinformationform/budgetgeneralinformationform.component';

import { BudgetcostcenterComponent } from './budgetcostcenter/budgetcostcenter.component';
import { BudgetcostcenterFormComponent } from './budgetcostcenter/budgetcostcenterform/budgetcostcenterform.component';

import { BudgetcontrolComponent } from './budgetcontrol/budgetcontrol.component';
import { BudgetcontrolFormComponent } from './budgetcontrol/budgetcontrolform/budgetcontrolform.component';

import { BudgetIncrementComponent } from './budgetincrement/budgetincrement.component';
import { BudgetIncrementFormComponent } from './budgetincrement/budgetincrementform/budgetincrementform.component';

import { BudgetcostcenterapprovalComponent } from './budgetcostcenterapproval/budgetcostcenterapproval.component';
import { BudgetactivityapprovalComponent } from './budgetactivityapproval/budgetactivityapproval.component';
import { BudgetvirementapprovalComponent } from './budgetvirementapproval/budgetvirementapproval.component';
import { BudgetincrementapprovalComponent } from './budgetincrementapproval/budgetincrementapproval.component';

import { BudgetvirementComponent } from './budgetvirement/budgetvirement.component';
import { BudgetvirementFormComponent } from './budgetvirement/budgetvirementform/budgetvirementform.component';

import { BudgettransferComponent } from '../budget/budgettransfer/budgettransfer.component';
import { BudgettransferFormComponent } from './budgettransfer/budgettransferform/budgettransferform.component';

import { DepartmentactivityComponent } from './departmentactivity/departmentactivity.component';
import { DepartmentactivityFormComponent } from './departmentactivity/departmentactivityform/departmentactivityform.component';

import { DepartmentbudgetComponent } from './departmentbudget/departmentbudget.component';
import { DepartmentbudgetFormComponent } from './departmentbudget/departmentbudgetform/departmentbudgetform.component';


import { BudgetsummaryComponent } from './budgetsummary/budgetsummary.component';
import { SummarydetailComponent } from './summarydetail/summarydetail.component';

import { BudgetyearComponent } from "./budgetyear/budgetyear.component";
import { budgetyearFormComponent } from "./budgetyear/budgetyearform/budgetyearform.component";
import { BudgetyearService } from "./service/budgetyear.service";

import { RemainingBalancereportComponent } from "./remainingbalancereport/remainingbalancereport.component";
import { RemainingBalancereportService } from "./service/remainingbalancereport.service";

import { ExpenditurereportComponent } from "./expenditurereport/expenditurereport.component";
import { ExpenditurereportService } from "./service/expenditurereport.service";

import { SummaryStatementreportComponent } from "./summarystatementreport/summarystatementreport.component";
import { SummaryStatementreportService } from "./service/summarystatementreport.service";

import { BudgetfileuploadComponent } from "./budgetfileupload/budgetfileupload.component";
import { BudgetfiluploadService } from "./service/budgetfileupload.service";

import { BudgetactivitiesService } from './service/budgetactivities.service';
import { BudgetactivityapprovalService } from './service/budgetactivityapproval.service';
import { BudgetplanningService } from './service/budgetplanning.service';
import { BudgetgeneralinformationService } from './service/budgetgeneralinformation.service';
import { SemesterService } from './service/semester.service';
import { BudgetcostcenterService } from './service/budgetcostcenter.service';
import { BudgetcostcenterapprovalService } from './service/budgetcostcenterapproval.service';

import { BudgettransferService } from '../budget/service/budgettransfer.service';
import { DepartmentactivityService } from './service/departmentactivity.service';
import { DepartmentService } from '../generalsetup/service/department.service';
import { DepartmentbudgetService } from '../budget/service/departmentbudget.service';
import { FinantialyearService } from '../generalledger/service/finantialyear.service';
import { GlcodeService } from '../generalsetup/service/glcode.service';
import { BudgetVirementService } from '../budget/service/budgetvirement.service';
import { BudgetvirementapprovalService } from '../budget/service/budgetvirementapproval.service';
import { ConfigService} from '../generalsetup/service/config.service';
import { BudgetincrementService } from './service/budgetincrement.service';
import { BudgetincrementapprovalService } from './service/budgetincrementapproval.service'
import { BudgetcontrolService } from './service/budgetcontrol.service'
import { BudgetsummaryService } from './service/budgetsummary.service'
import { SummarydetailService } from './service/summarydetail.service'
import { FunddonationService } from './service/funddonation.service';
import { FundDonationComponent } from "./funddonation/funddonation.component";
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { BudgetRoutingModule } from './budget.router.module';
import { JwtInterceptor } from '../_helpers/jwt.interceptors';
import { NgSelectModule } from '@ng-select/ng-select';
import { SharedModule } from "../shared/shared.module";
import { BudgetMatrixComponent } from "./budgetmatrix/budgetmatrix.component";
@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        MaterialModule,
        BudgetRoutingModule,
        Ng2SearchPipeModule,
        HttpClientModule,
        QueryBuilderModule,
        DataTableModule,
        NgSelectModule,
        SharedModule,
        NguiAutoCompleteModule

    ],
    exports: [],
    declarations: [
        BudgetactivitiesComponent,
        BudgetactivitiesFormComponent,
        BudgetplanningComponent,
        BudgetplanningFormComponent,
        BudgetgeneralinformationComponent,
        BudgetgeneralinformationFormComponent,
        BudgetcostcenterComponent,
        BudgetcostcenterapprovalComponent,
        BudgetcostcenterFormComponent,
        BudgettransferComponent,
        BudgettransferFormComponent,
        DepartmentactivityComponent,
        DepartmentactivityFormComponent,
        DepartmentbudgetComponent,
        DepartmentbudgetFormComponent,
        BudgetactivityapprovalComponent,
        BudgetvirementComponent,
        BudgetvirementFormComponent,
        BudgetvirementapprovalComponent,
        BudgetIncrementComponent,
        BudgetIncrementFormComponent,
        BudgetincrementapprovalComponent,
        BudgetcontrolComponent,
        BudgetcontrolFormComponent,
        BudgetsummaryComponent,
        SummarydetailComponent,
        RemainingBalancereportComponent,
        ExpenditurereportComponent,
        SummaryStatementreportComponent,
        BudgetyearComponent,
        budgetyearFormComponent,
        BudgetfileuploadComponent,
        FundDonationComponent,
        BudgetMatrixComponent

    ],
    providers: [
        BudgetactivitiesService,
        BudgetplanningService,
        BudgetgeneralinformationService,
        BudgetcostcenterService,
        SemesterService,
        BudgettransferService,
        DepartmentService,
        DepartmentactivityService,
        DepartmentbudgetService,
        GlcodeService,
        BudgetcostcenterapprovalService,
        BudgetactivityapprovalService,
        FinantialyearService,
        BudgetVirementService,
        BudgetvirementapprovalService,
        BudgetincrementService,
        ConfigService,
        BudgetincrementapprovalService,
        BudgetcontrolService,
        BudgetsummaryService,
        RemainingBalancereportService,
        ExpenditurereportService,
        SummaryStatementreportService,
        BudgetyearService,
        SummarydetailService,
        FunddonationService,
        BudgetfiluploadService,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: JwtInterceptor,
            multi: true
          }

    ],
})
export class BudgetModule { }
