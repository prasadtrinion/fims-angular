import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ExpenditurereportService } from "../service/expenditurereport.service";
import { BudgetcostcenterService } from "../service/budgetcostcenter.service";
import { BudgetyearService } from "../service/budgetyear.service";
import { NgxSpinnerService } from 'ngx-spinner';
import { environment } from '../../../environments/environment';

@Component({
    selector: 'ExpenditurereportComponent',
    templateUrl: 'expenditurereport.component.html'
})

export class ExpenditurereportComponent implements OnInit {

    expenditureList = [];
    expenditureData = {};
    fundList = [];
    fundData = {};
    numberList = [];
    finantialyearList = [];
    budgetcostcenterList = [];
    id: number;
    ajaxCount: number;
    downloadUrl: string = environment.api.downloadbase;
    constructor(
        private ExpenditurereportService: ExpenditurereportService,
        private BudgetyearService: BudgetyearService,
        private BudgetcostcenterService: BudgetcostcenterService,
        private route: ActivatedRoute,
        private spinner:NgxSpinnerService

    ) { }

    ngDoCheck() {
        const change = this.ajaxCount;
        if (this.ajaxCount == 0) {
            this.spinner.hide();
        }
    }

    downloadReport() {
        console.log(this.expenditureData);
        let newobje = {};
        newobje['financialYear'] = this.expenditureData['financialYear'];
        newobje['departmentCode'] = this.expenditureData['departmentCode'];
        this.ExpenditurereportService.insertBalancesheetreportItems(newobje).subscribe(
            data => {
                console.log(data['name']);
                window.open(this.downloadUrl + data['name']);
            }, error => {
                console.log(error);
            });
    }
    ngOnInit() {
        this.spinner.show();
        this.expenditureData['financialYear'] = '';
        for (var i = 1; i < 30; i++) {
            let number = {};
            number['key'] = i;
            number['value'] = i;
            this.numberList.push(number);
        }
        console.log(this.expenditureData);
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        
        this.ajaxCount = 0;
        // financial year dropdown
        this.ajaxCount++;
        this.BudgetyearService.getActiveItems().subscribe(
            data => {
                this.ajaxCount--;
                this.finantialyearList = data['result'];
                console.log(data['result']);
                if (data['result'].length == 1) {
                    this.expenditureData['financialYear'] = data['result'][0]['f110fid'];
                }
            }, error => {
            });
        //costcenter
        this.ajaxCount++;
        this.BudgetcostcenterService.getActiveCostCenter().subscribe(
            data => {
                this.ajaxCount--;
                this.budgetcostcenterList = data['result'];
            }, error => {
                console.log(error);
            });
    }
}

