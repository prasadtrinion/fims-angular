import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { DepartmentService } from '../../../generalsetup/service/department.service'
import { DepartmentactivityService } from '../../service/departmentactivity.service'
import { BudgetactivitiesService } from '../../service/budgetactivities.service'


@Component({
    selector: 'DepartmentactivityFormComponent',
    templateUrl: 'departmentactivityform.component.html'
})

export class DepartmentactivityFormComponent implements OnInit {

    DepartmentactivityList = [];
    DepartmentactivityData = {};
    budgetactivitiesList = [];
    budgetactivitiesdata = {};
    DeptList = [];
    DeptData = {};
    id: number;

    constructor(
        private DepartmentactivityService: DepartmentactivityService,
        private BudgetactivitiesService: BudgetactivitiesService,
        private DepartmentService: DepartmentService,
        private route: ActivatedRoute,
        private router: Router

    ) { }

    ngOnInit() {
        this.DepartmentactivityData['f055fstatus'] = 1;

        //Department dropdown
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.DepartmentService.getItems().subscribe(
            data => {
                this.DeptList = data['result']['data'];
            }, error => {
                console.log(error);
            });
        //Activities dropdown
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.BudgetactivitiesService.getItems().subscribe(
            data => {
                this.budgetactivitiesList = data['result']['data'];
            }, error => {
                console.log(error);
            });

        if (this.id > 0) {
            this.DepartmentactivityService.getItemsDetail(this.id).subscribe(
                data => {
                    this.DepartmentactivityData['f055fstatus'] = 1;
                    this.DepartmentactivityData = data['result'];
                    if (data['result'].f055fstatus === true) {
                        console.log(data);
                        this.DepartmentactivityData['f055fstatus'] = 1;
                    } else {
                        this.DepartmentactivityData['f055fstatus'] = 0;
                    }
                }, error => {
                    console.log(error);
                });
        }
    }

    addDepartmentactivity() {
        console.log(this.DepartmentactivityData);
        this.DepartmentactivityData['f055fupdatedBy'] = 1;
        this.DepartmentactivityData['f055fcreatedBy'] = 1;

        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.DepartmentactivityService.updateDepartmentactivityItems(this.DepartmentactivityData, this.id).subscribe(
                data => {
                    this.router.navigate(['budget/departmentactivity']);
                }, error => {
                    console.log(error);
                });
        } else {
            this.DepartmentactivityService.insertDepartmentactivityItems(this.DepartmentactivityData).subscribe(
                data => {
                    this.router.navigate(['budget/departmentactivity']);

                }, error => {
                    console.log(error);
                });
        }
    }
}