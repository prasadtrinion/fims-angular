import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DepartmentactivityService } from '../service/departmentactivity.service'


@Component({
    selector: 'Departmentactivity',
    templateUrl: 'departmentactivity.component.html'
})

export class DepartmentactivityComponent implements OnInit {
    DepartmentactivityList = [];
    DepartmentactivityData = {};
    id: number

    constructor(
        private DepartmentactivityService: DepartmentactivityService

    ) { }

    ngOnInit() { 
        
        this.DepartmentactivityService.getItems().subscribe(
            data => {
                this.DepartmentactivityList = data['result']['data'];
        }, error => {
            console.log(error);
        });
    }
}