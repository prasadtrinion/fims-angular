import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { BudgetplanningService } from '../../service/budgetplanning.service'
import { ActivatedRoute, Router } from '@angular/router';

@Component({
    selector: 'BudgetplanningFormComponent',
    templateUrl: 'budgetplanningform.component.html'
})

export class BudgetplanningFormComponent implements OnInit {
    budgetplanningList = [];
    budgetplanningdata = {};
    id: number;
    constructor(
        private BudgetplanningService: BudgetplanningService,
        private route: ActivatedRoute,
        private router: Router

    ) { }

    ngOnInit() {
        this.budgetplanningdata['f052fstatus'] = 1;
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        console.log(this.id);

        if (this.id > 0) {
            this.BudgetplanningService.getItemsDetail(this.id).subscribe(
                data => {
                    this.budgetplanningdata = data['result'];
                    if (data['result'].f052fstatus === true) {
                        console.log(data);
                        this.budgetplanningdata['f052fstatus'] = 1;
                    } else {
                        this.budgetplanningdata['f052fstatus'] = 0;
                    }
                }, error => {
                    console.log(error);
                });
        }
    }

    addBudgetplanning() {
        this.budgetplanningdata['f052fupdatedBy'] = 1;
        this.budgetplanningdata['f052fcreatedBy'] = 1;

        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.BudgetplanningService.updateBudgetplanningItems(this.budgetplanningdata, this.id).subscribe(
                data => {
                    this.router.navigate(['budget/budgetplanning']);
                }, error => {
                    console.log(error);
                });
        } else {
            this.BudgetplanningService.insertBudgetplanningItems(this.budgetplanningdata).subscribe(
                data => {
                    this.router.navigate(['budget/budgetplanning']);

                }, error => {
                    console.log(error);
                });
        }
    }
}