import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { BudgetplanningService } from '../service/budgetplanning.service'


@Component({
    selector: 'Budgetplanning',
    templateUrl: 'budgetplanning.component.html'
})

export class BudgetplanningComponent implements OnInit {
    budgetplanningList = [];
    budgetplanningData = {};
    id: number;

    constructor(
        private BudgetplanningService: BudgetplanningService

    ) { }
    ngOnInit() { 
        
        this.BudgetplanningService.getItems().subscribe(
            data => {
                this.budgetplanningList = data['result']['data'];
        }, error => {
            console.log(error);
        });
    }
}