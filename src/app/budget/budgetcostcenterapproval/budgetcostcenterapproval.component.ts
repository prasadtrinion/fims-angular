import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Injector } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BudgetcostcenterapprovalService } from '../../budget/service/budgetcostcenterapproval.service'
import { NgxSpinnerService } from 'ngx-spinner';
@Component({
    selector: 'Budgetcostcenterapproval',
    templateUrl: 'budgetcostcenterapproval.component.html'
})

export class BudgetcostcenterapprovalComponent implements OnInit {
    budgetcostcenterapprovalList = [];
    budgetcostcenterapprovalData = {};
    approvalData = {};

    id: number;
    selectAllCheckbox: false;

    constructor(

        private BudgetcostcenterapprovalService: BudgetcostcenterapprovalService,
        private route: ActivatedRoute,
        private router: Router,
        private spinner: NgxSpinnerService

    ) { }

    ngOnInit() {
        this.getListData();

    }
    getListData() {
        this.spinner.show();

        this.BudgetcostcenterapprovalService.getItemsDetail(0).subscribe(
            data => {
                this.spinner.hide();

                this.budgetcostcenterapprovalList = data['result'];
            }, error => {
                console.log(error);
            });
    }

    addBudgetcostcenterapproval() {
        var approvalArray = [];
        for (var i = 0; i < this.budgetcostcenterapprovalList.length; i++) {
            if (this.budgetcostcenterapprovalList[i]['approvalstatus'] == true) {
                approvalArray.push(this.budgetcostcenterapprovalList[i]['f051fid']);
            }
        }
        if (approvalArray.length < 1) {
            alert("Please select atleast one Cost Center to Approve");
            return false;
        }
        var confirmPop = confirm("Do you want to Approve?");
        if (confirmPop == false) {
            return false;
        }
        var budgetActivityDataObject = {}
        budgetActivityDataObject['id'] = approvalArray;
        budgetActivityDataObject['status'] = '1';
        budgetActivityDataObject['reason'] = ' ';
        this.approvalData['reason'] = '';
        this.BudgetcostcenterapprovalService.updateBudgetcostcenterItems(budgetActivityDataObject).subscribe(
            data => {
                this.getListData();
                alert("Approved Successfully")
            }, error => {
                console.log(error);
            });
    }

    regectBudgetcostcenterapproval() {
        var approvalArray = [];
        for (var i = 0; i < this.budgetcostcenterapprovalList.length; i++) {
            if (this.budgetcostcenterapprovalList[i]['approvalstatus'] == true) {
                approvalArray.push(this.budgetcostcenterapprovalList[i]['f051fid']);
            }
        }
        if (approvalArray.length < 1) {
            alert("Please select atleast one Cost Center to Reject");
            return false;
        }
        if (this.approvalData['reason'] == undefined) {
            alert('Enter the Description');
            return false;
        }

        var confirmPop = confirm("Do you want to Reject?");
        if (confirmPop == false) {
            return false;
        }
        var budgetActivityDataObject = {}
        budgetActivityDataObject['id'] = approvalArray;
        budgetActivityDataObject['status'] = '2';
        budgetActivityDataObject['reason'] = this.approvalData['reason'];
        this.approvalData['reason'] = '';

        this.BudgetcostcenterapprovalService.updateBudgetcostcenterItems(budgetActivityDataObject).subscribe(
            data => {
                this.getListData();
                alert("Rejected Successfully")
            }, error => {
                console.log(error);
            });
    }
    selectAll() {
        for (var i = 0; i < this.budgetcostcenterapprovalList.length; i++) {
            this.budgetcostcenterapprovalList[i].approvalstatus = this.selectAllCheckbox;
        }
    }
}