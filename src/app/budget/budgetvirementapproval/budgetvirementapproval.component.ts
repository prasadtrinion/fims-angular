import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Injector } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BudgetvirementapprovalService } from '../../budget/service/budgetvirementapproval.service'
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'Budgetvirementapproval',
    templateUrl: 'budgetvirementapproval.component.html'
})

export class BudgetvirementapprovalComponent implements OnInit {
    budgetvirementapprovalList = [];
    budgetvirementapprovalData = {};
    approvalData = {};
    id: number;
    selectAllCheckbox: false;

    constructor(
        private BudgetvirementapprovalService: BudgetvirementapprovalService,
        private route: ActivatedRoute,
        private router: Router,
        private spinner: NgxSpinnerService

    ) { }

    ngOnInit() {

        this.getListData();
    }
    getListData() {
        this.spinner.show();
        this.BudgetvirementapprovalService.getItemsDetail(0).subscribe(
            data => {
                this.spinner.hide();

                this.budgetvirementapprovalList = data['result'];
            }, error => {
                console.log(error);
            });
    }
    addBudgetvirementapproval() {

        var approvalArray = [];
        for (var i = 0; i < this.budgetvirementapprovalList.length; i++) {
            if (this.budgetvirementapprovalList[i].approvalstatus == true) {
                approvalArray.push(this.budgetvirementapprovalList[i].f056fid);
            }
        }
        if (approvalArray.length < 1) {
            alert("Please select atleast one Cost Center to Approve");
            return false;
        }

        var confirmPop = confirm("Do you want to Approve?");
        if (confirmPop == false) {
            return false;
        }
        var budgetCostcenterDataObject = {}
        budgetCostcenterDataObject['id'] = approvalArray;
        budgetCostcenterDataObject['status'] = 1;
        budgetCostcenterDataObject['reason'] = '';
        this.BudgetvirementapprovalService.updateBudgetvirementItems(budgetCostcenterDataObject).subscribe(
            data => {
                this.getListData();
                this.approvalData['reason'] = '';
                alert("Approved Successfully")
            }, error => {
                console.log(error);
            });
    }
    rejectaddBudgetvirementapproval() {
        var approvalArray = [];
        for (var i = 0; i < this.budgetvirementapprovalList.length; i++) {
            if (this.budgetvirementapprovalList[i].approvalstatus == true) {
                approvalArray.push(this.budgetvirementapprovalList[i].f056fid);
            }
        }
        if (approvalArray.length < 1) {
            alert("Please select atleast one cost center to Reject");
            return false;
        }
        if (this.approvalData['reason'] == undefined) {
            alert('Please Enter the Description');
            return false;
        }
        var confirmPop = confirm("Do you want to Reject?");
        if (confirmPop == false) {
            return false;
        }
        var budgetCostcenterDataObject = {}
        budgetCostcenterDataObject['id'] = approvalArray;
        budgetCostcenterDataObject['status'] = '2';
        budgetCostcenterDataObject['reason'] = this.approvalData['reason'];

        this.BudgetvirementapprovalService.updateBudgetvirementItems(budgetCostcenterDataObject).subscribe(
            data => {
                this.getListData();
                this.approvalData['reason'] = '';
                alert("Rejected Successfully")
            }, error => {
                console.log(error);
            });
    }
}