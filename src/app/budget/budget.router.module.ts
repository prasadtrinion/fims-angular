import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BudgetactivitiesComponent } from './budgetactivities/budgetactivities.component';
import { BudgetactivitiesFormComponent } from './budgetactivities/budgetactivitiesform/budgetactivitiesform.component';

import { BudgetvirementComponent } from './budgetvirement/budgetvirement.component';
import { BudgetvirementFormComponent } from './budgetvirement/budgetvirementform/budgetvirementform.component';

import { BudgetplanningComponent } from './budgetplanning/budgetplanning.component';
import { BudgetplanningFormComponent } from './budgetplanning/budgetplanningform/budgetplanningform.component';

import { BudgetgeneralinformationComponent } from './budgetgeneralinformation/budgetgeneralinformation.component';
import { BudgetgeneralinformationFormComponent } from './budgetgeneralinformation/budgetgeneralinformationform/budgetgeneralinformationform.component';

import { BudgetcostcenterComponent } from './budgetcostcenter/budgetcostcenter.component';
import { BudgetcostcenterFormComponent } from './budgetcostcenter/budgetcostcenterform/budgetcostcenterform.component';

import { BudgetcostcenterapprovalComponent } from './budgetcostcenterapproval/budgetcostcenterapproval.component';
import { BudgetactivityapprovalComponent } from './budgetactivityapproval/budgetactivityapproval.component';
import { BudgetvirementapprovalComponent } from './budgetvirementapproval/budgetvirementapproval.component';
import { BudgetfileuploadComponent } from "./budgetfileupload/budgetfileupload.component";
import { BudgettransferComponent } from './budgettransfer/budgettransfer.component';
import { BudgettransferFormComponent } from './budgettransfer/budgettransferform/budgettransferform.component';

import { BudgetIncrementComponent } from './budgetincrement/budgetincrement.component';
import { BudgetIncrementFormComponent } from './budgetincrement/budgetincrementform/budgetincrementform.component';

import { BudgetincrementapprovalComponent } from './budgetincrementapproval/budgetincrementapproval.component';

import { DepartmentactivityComponent } from './departmentactivity/departmentactivity.component';
import { DepartmentactivityFormComponent } from './departmentactivity/departmentactivityform/departmentactivityform.component';

import { DepartmentbudgetComponent } from './departmentbudget/departmentbudget.component';
import { DepartmentbudgetFormComponent } from './departmentbudget/departmentbudgetform/departmentbudgetform.component';

import { BudgetcontrolComponent } from './budgetcontrol/budgetcontrol.component';
import { BudgetcontrolFormComponent } from './budgetcontrol/budgetcontrolform/budgetcontrolform.component';

import { BudgetsummaryComponent } from './budgetsummary/budgetsummary.component';
import { SummarydetailComponent } from './summarydetail/summarydetail.component';

import { RemainingBalancereportComponent } from "./remainingbalancereport/remainingbalancereport.component";
import { ExpenditurereportComponent } from "./expenditurereport/expenditurereport.component";
import { SummaryStatementreportComponent } from "./summarystatementreport/summarystatementreport.component";

import { BudgetyearComponent } from "./budgetyear/budgetyear.component";
import { budgetyearFormComponent } from "./budgetyear/budgetyearform/budgetyearform.component";
import { BudgetMatrixComponent } from "./budgetmatrix/budgetmatrix.component";

const routes: Routes = [

  { path: 'budgetactivities', component: BudgetactivitiesComponent },
  { path: 'addnewbudgetactivities', component: BudgetactivitiesFormComponent },
  { path: 'editbudgetactivities/:id', component: BudgetactivitiesFormComponent },
  { path: 'viewbudgetactivities/:id/:idview', component: BudgetactivitiesFormComponent },

  { path: 'budgetvirement', component: BudgetvirementComponent },
  { path: 'addnewbudgetvirement', component: BudgetvirementFormComponent },
  { path: 'editbudgetvirement/:id', component: BudgetvirementFormComponent },
  { path: 'viewbudgetvirement/:id/:idview', component: BudgetvirementFormComponent },

  { path: 'budgetplanning', component: BudgetplanningComponent },
  { path: 'addnewbudgetplanning', component: BudgetplanningFormComponent },
  { path: 'editbudgetplanning/:id', component: BudgetplanningFormComponent },

  { path: 'budgetgeneralinformation', component: BudgetgeneralinformationComponent },
  { path: 'addnewbudgetgeneralinformation', component: BudgetgeneralinformationFormComponent },
  { path: 'editbudgetgeneralinformation/:id', component: BudgetgeneralinformationFormComponent },

  { path: 'budgetcostcenter', component: BudgetcostcenterComponent },
  { path: 'addnewbudgetcostcenter', component: BudgetcostcenterFormComponent },
  { path: 'editbudgetcostcenter/:id', component: BudgetcostcenterFormComponent },
  { path: 'viewbudgetcostcenter/:id/:idview', component: BudgetcostcenterFormComponent },

  { path: 'budgetcostcenterapproval', component: BudgetcostcenterapprovalComponent },
  { path: 'budgetactivityapproval', component: BudgetactivityapprovalComponent },
  { path: 'budgetvirementapproval', component: BudgetvirementapprovalComponent },
  { path: 'budgetincrementapproval', component: BudgetincrementapprovalComponent },

  { path: 'budgetincrement', component: BudgetIncrementComponent },
  { path: 'addnewbudgetincrement', component: BudgetIncrementFormComponent },
  { path: 'editbudgetincrement/:id', component: BudgetIncrementFormComponent },
  { path: 'viewbudgetincrement/:id/:idview', component: BudgetIncrementFormComponent },

  { path: 'budgettransfer', component: BudgettransferComponent },
  { path: 'addnewbudgettransfer', component: BudgettransferFormComponent },
  { path: 'editbudgettransfer/:id', component: BudgettransferFormComponent },

  { path: 'departmentactivity', component: DepartmentactivityComponent },
  { path: 'addnewdepartmentactivity', component: DepartmentactivityFormComponent },
  { path: 'editdepartmentactivity/:id', component: DepartmentactivityFormComponent },

  { path: 'departmentbudget', component: DepartmentbudgetComponent },
  { path: 'addnewdepartmentbudget', component: DepartmentbudgetFormComponent },
  { path: 'editdepartmentbudget/:id', component: DepartmentbudgetFormComponent },

  { path: 'budgetcontrol', component: BudgetcontrolComponent },
  { path: 'addnewbudgetcontrol', component: BudgetcontrolFormComponent },
  { path: 'editbudgetcontrol/:id', component: BudgetcontrolFormComponent },
  
  { path: 'budgetsummary', component: BudgetsummaryComponent },
  { path: 'summarydetail', component: SummarydetailComponent },

  { path: 'remainingbalancereport', component: RemainingBalancereportComponent },
  { path: 'expenditurereport', component: ExpenditurereportComponent },
  { path: 'summarystatementreport', component: SummaryStatementreportComponent },

  { path: 'budgetyear', component: BudgetyearComponent },
  { path: 'addbudgetyear', component: budgetyearFormComponent },
  { path: 'editbudgetyear/:id', component: budgetyearFormComponent },

  { path: 'budgetfileupload', component: BudgetfileuploadComponent },
  { path: 'budgetmatrix', component: BudgetMatrixComponent },


];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],

})
export class BudgetRoutingModule {

}