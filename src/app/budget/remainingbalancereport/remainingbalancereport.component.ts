import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RemainingBalancereportService } from "../service/remainingbalancereport.service";
import { DepartmentService } from '../../generalsetup/service/department.service'
import { FinancialyearService } from '../../generalledger/service/financialyear.service';
import { BudgetcostcenterService } from "../service/budgetcostcenter.service";
import { BudgetyearService } from "../service/budgetyear.service";
import { NgxSpinnerService } from 'ngx-spinner';
import { environment } from '../../../environments/environment';

@Component({
    selector: 'RemainingBalancereportComponent',
    templateUrl: 'remainingbalancereport.component.html'
})

export class RemainingBalancereportComponent implements OnInit {
    remainingbalanceList = [];
    remainingbalanceData = {};
    numberList = [];
    finantialyearList = [];
    budgetcostcenterList = [];
    id: number;
    ajaxCount: number;
    downloadUrl: string = environment.api.downloadbase;

    constructor(
        private RemainingBalancereportService: RemainingBalancereportService,
        private BudgetyearService: BudgetyearService,
        private BudgetcostcenterService: BudgetcostcenterService,
        private route: ActivatedRoute,
        private router: Router,
        private spinner: NgxSpinnerService
    ) { }

    ngDoCheck() {
        const change = this.ajaxCount;
        if (this.ajaxCount == 0) {
            this.spinner.hide();
        }
    }

    downloadReport() {
        console.log(this.remainingbalanceData);
        let newobje = {};
        newobje['financialYear'] = this.remainingbalanceData['financialYear'];
        newobje['departmentCode'] = this.remainingbalanceData['departmentCode'];
        this.RemainingBalancereportService.insertBalancesheetreportItems(newobje).subscribe(
            data => {
                console.log(data['name']);
                window.open(this.downloadUrl + data['name']);
            }, error => {
                console.log(error);
            });
    }
    ngOnInit() {
        this.spinner.show();
        this.remainingbalanceData['financialYear'] = '';
        // this.remainingbalanceData['Month'] = '';
        for (var i = 1; i < 30; i++) {
            let number = {};
            number['key'] = i;
            number['value'] = i;
            this.numberList.push(number);
        }
        this.ajaxCount = 0;

        // Budget year dropdown
        this.ajaxCount++;
        this.BudgetyearService.getActiveItems().subscribe(
            data => {
                this.ajaxCount--;
                this.finantialyearList = data['result'];
                console.log(data['result']);
                if (data['result'].length == 1) {
                    this.remainingbalanceData['financialYear'] = data['result'][0]['f110fid'];
                }
            }, error => {
            });
        //costcenter
        this.ajaxCount++;
        this.BudgetcostcenterService.getActiveCostCenter().subscribe(
            data => {
                this.ajaxCount--;
                this.budgetcostcenterList = data['result'];
                console.log(this.budgetcostcenterList);
            }, error => {
                console.log(error);
            });
    }
}