import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MaterialModule } from '../material/material.module';

import { DateComponent } from './date/date.component';
import { DateRoutingModule } from './date.router.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        MaterialModule,
        DateRoutingModule
    ],
    exports: [],
    declarations: [
        DateComponent,
    ],
    providers: [
    ],
})
export class DateModule { }
