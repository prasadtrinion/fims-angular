import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DateComponent } from './date/date.component';
 //import { DateRoutingModule } from './date.router.module';

const routes: Routes = [
  { path: 'date', component: DateComponent },
  

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  
})
export class DateRoutingModule { 
  
}