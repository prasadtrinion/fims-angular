import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
    selector: 'date',
    templateUrl: 'date.component.html'
})

export class DateComponent implements OnInit {
    
    date: Date = new Date();
    settings = {
    bigBanner: true,
    format: 'dd-MMM-yyyy hh:mm a',
    defaultOpen: true
}
    ngOnInit() { }
 }