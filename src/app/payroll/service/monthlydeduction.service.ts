import { Injectable } from '@angular/core';
import { HttpClientModule,HttpHeaders, HttpClient} from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({ 'X-Auth-Client': 'qazwsx123',
    'Content-Type': 'application/json' })
    
  };
  
@Injectable()

export class MonthlydeductionService {
    url: string = environment.api.base + environment.api.endPoints.monthlyDeduction;
    deleteurl: string = environment.api.base + environment.api.endPoints.deleteMonthlyDeductionDetails;
    constructor(private httpClient: HttpClient) { 
        
    }
    
    getItems() {
        return this.httpClient.get(this.url,httpOptions);
    }
    deleteDetail(deleteObject) {
        return this.httpClient.post(this.deleteurl,deleteObject,httpOptions);
    }
    getItemsDetail(id) {
        return this.httpClient.get(this.url+'/'+id,httpOptions);
    }

    insertItems(monthlyDeductionData): Observable<any> {
        return this.httpClient.post(this.url, monthlyDeductionData,httpOptions);
    }

    updateItems(monthlyDeductionData,id): Observable<any> {
        return this.httpClient.put(this.url+'/'+id, monthlyDeductionData,httpOptions);
       }
}