import { Injectable } from '@angular/core';
import { HttpClientModule,HttpHeaders, HttpClient} from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({ 'X-Auth-Client': 'qazwsx123',
    'Content-Type': 'application/json' })
    
  };
  
@Injectable()

export class SalaryService {
    url: string = environment.api.base + environment.api.endPoints.salary;
    deleteurl: string = environment.api.base + environment.api.endPoints.deleteSalaryDetails;

    constructor(private httpClient: HttpClient) { 
        
    }
    deleteDetail(deleteObject) {
        return this.httpClient.post(this.deleteurl,deleteObject,httpOptions);
    }
    getItems() {
        return this.httpClient.get(this.url,httpOptions);
    }

    getItemsDetail(id) {
        return this.httpClient.get(this.url+'/'+id,httpOptions);
    }

    insertSalaryItems(itemcategoryData): Observable<any> {
        return this.httpClient.post(this.url, itemcategoryData,httpOptions);
    }

    updateSalaryItems(itemcategoryData,id): Observable<any> {
        return this.httpClient.put(this.url+'/'+id, itemcategoryData,httpOptions);
       }
}