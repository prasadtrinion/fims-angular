import { Injectable } from '@angular/core';
import { HttpClientModule,HttpHeaders, HttpClient} from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({ 'X-Auth-Client': 'qazwsx123',
    'Content-Type': 'application/json' })
    
  };
  
@Injectable()

export class ProcesssalaryService {
    url: string = environment.api.base + environment.api.endPoints.processType;
    constructor(private httpClient: HttpClient) { 
        
    }
    
    getItems() {
        return this.httpClient.get(this.url,httpOptions);
    }

    getItemsDetail(id) {
        return this.httpClient.get(this.url+'/'+id,httpOptions);
    }

    insertProcesssalaryItems(processsalaryData): Observable<any> {
        return this.httpClient.post(this.url, processsalaryData,httpOptions);
    }

    updateProcesssalarytems(processsalaryData,id): Observable<any> {
        return this.httpClient.put(this.url+'/'+id, processsalaryData,httpOptions);
       }
}