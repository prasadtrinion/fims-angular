import { Injectable } from '@angular/core';
import { HttpClientModule, HttpHeaders, HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({
        'X-Auth-Client': 'qazwsx123',
        'Content-Type': 'application/json'
    })

};

@Injectable()

export class PayrolltaxService {
    urlpayrollSchdule: string = environment.api.base + environment.api.endPoints.payrollSchedule;
    urlpayrollContribution: string = environment.api.base + environment.api.endPoints.payrollContribution;
    urlpayrollDeduction: string = environment.api.base + environment.api.endPoints.payrollDeduction;
    urlpayrollExcemption: string = environment.api.base + environment.api.endPoints.payrollExcemption;
    urlpayrollDelete: string = environment.api.base + environment.api.endPoints.payrolltaxdelete;
    constructor(private httpClient: HttpClient) {

    }

    deleteItems(PayrolltaxData) {
        return this.httpClient.post(this.urlpayrollDelete,PayrolltaxData,httpOptions);
    }
    getPayrollTaxItemDetail() {
        return this.httpClient.get(this.urlpayrollSchdule, httpOptions);
    }
    insertPayrolltaxItems(PayrolltaxData): Observable<any> {
        return this.httpClient.post(this.urlpayrollSchdule, PayrolltaxData, httpOptions);
    }

   
    getPayrollTaxExcemptionItemDetail() {
        return this.httpClient.get(this.urlpayrollExcemption, httpOptions);
    }
    insertPayrolltaxexcemptionItems(PayrolltaxexceptionData): Observable<any> {
        return this.httpClient.post(this.urlpayrollExcemption, PayrolltaxexceptionData, httpOptions);
    }

   
    getPayrollTaxcontributionItemDetail() {
        return this.httpClient.get(this.urlpayrollContribution, httpOptions);
    }
    insertPayrolltaxcontributionItems(PayrolltaxcontibutionData): Observable<any> {
        return this.httpClient.post(this.urlpayrollContribution, PayrolltaxcontibutionData, httpOptions);
    }

  
    getPayrollTaxdeductionItemDetail() {
        return this.httpClient.get(this.urlpayrollDeduction, httpOptions);
    }
    insertPayrolltaxdeductionItems(PayrolltaxdeductionData): Observable<any> {
        return this.httpClient.post(this.urlpayrollDeduction, PayrolltaxdeductionData, httpOptions);
    }


}