import { Injectable } from '@angular/core';
import { HttpClientModule,HttpHeaders, HttpClient} from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({ 'X-Auth-Client': 'qazwsx123',
    'Content-Type': 'application/json' })
    
  };
  
@Injectable()

export class PayrollpostingService {
    url: string = environment.api.base + environment.api.endPoints.payroll;
    constructor(private httpClient: HttpClient) { 
        
    }
    
    getItems() {
        return this.httpClient.get(this.url,httpOptions);
    }

    getItemsDetail(id) {
        return this.httpClient.get(this.url+'/'+id,httpOptions);
    }

    insertPayrollpostingItems(payrollpostingData): Observable<any> {
        return this.httpClient.post(this.url, payrollpostingData,httpOptions);
    }

    updatePayrollpostingItems(payrollpostingData,id): Observable<any> {
        return this.httpClient.put(this.url+'/'+id, payrollpostingData,httpOptions);
       }
}