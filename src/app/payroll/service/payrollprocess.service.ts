import { Injectable } from '@angular/core';
import { HttpClientModule,HttpHeaders, HttpClient} from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({ 'X-Auth-Client': 'qazwsx123',
    'Content-Type': 'application/json' })
    
  };
  
@Injectable()

export class PayrollprocessService {
    url: string = environment.api.base + environment.api.endPoints.payrollprocess;
    urlepfcalculation: string = environment.api.base + environment.api.endPoints.urlepfcalculation;

    urlmoveSalary: string = environment.api.base + environment.api.endPoints.moveSalary;

    constructor(private httpClient: HttpClient) { 
        
    }
    
    getItems() {
        return this.httpClient.get(this.url,httpOptions);
    }

    getItemsDetail(id) {
        return this.httpClient.get(this.url+'/'+id,httpOptions);
    }

    insertPayrollprocessItems(payrollprocessData): Observable<any> {
        return this.httpClient.post(this.urlepfcalculation, payrollprocessData,httpOptions);
    }

    moveSalaryProcess(payrollprocessData): Observable<any> {
        return this.httpClient.post(this.urlmoveSalary, payrollprocessData,httpOptions);
       }
}