import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ProcesstypeService } from '../service/processtype.service'

@Component({
    selector: 'ProcesstypeComponent',
    templateUrl: 'processtype.component.html'
})
export class ProcesstypeComponent implements OnInit {
    processtypeList = [];
    processtypeData = {};
    id: number;
    constructor(
        private ProcesstypeService: ProcesstypeService
    ) { }

    ngOnInit() {

        this.ProcesstypeService.getItems().subscribe(
            data => {
                this.processtypeList = data['result']['data'];
            }, error => {
                console.log(error);
            });
    }
  
}