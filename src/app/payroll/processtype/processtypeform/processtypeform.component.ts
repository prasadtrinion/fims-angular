import { Injector } from '@angular/core';
import { Component, OnInit, ViewChild } from '@angular/core';

import { ActivatedRoute, Router } from '@angular/router';

import { ProcesstypeService } from "../../service/processtype.service";

import { DefinationmsService } from '../../../studentfinance/service/definationms.service';
import { AlertService } from './../../../_services/alert.service';


@Component({
    selector: 'ProcesstypeFormComponent',
    templateUrl: 'processtypeform.component.html'
})

export class ProcesstypeFormComponent implements OnInit {
    processDataHeader = {};
    ajaxCount: number;
    processtypeList = [];
    processtypeData = {};
    ftypecomponent=[];
    processList=[];
    id: number;
    constructor(
        private ProcesstypeService: ProcesstypeService,
        private DefinationmsService: DefinationmsService,
        private route: ActivatedRoute,
        private router: Router

    ) { }
    // ngDoCheck() {
    //     const change = this.ajaxCount;
    //     // console.log(this.ajaxCount);
    //     if (this.ajaxCount == 0) {
    //         this.editFunction();
    //         this.ajaxCount = 10;
    //     }
    // }
    
    editFunction() {
        this.processtypeData = {};

        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.ProcesstypeService.getItemsDetail(this.id).subscribe(
                data => {
                    this.processDataHeader = data['result'][0];

                    console.log(this.processtypeList);


                }, error => {
                    console.log(error);
                });
        }
    }
    ngOnInit() {
        // let typecomponentobj = {};
        // typecomponentobj['id'] = 1;
        // typecomponentobj['name'] = 'Permanenet';
        // this.ftypecomponent.push(typecomponentobj);
        // typecomponentobj = {};
        // typecomponentobj['id'] = 2;
        // typecomponentobj['name'] = 'Temporary';
        // this.ftypecomponent.push(typecomponentobj);
        // typecomponentobj = {};
        // typecomponentobj['id'] = 3;
        // typecomponentobj['name'] = 'Contract';
        // this.ftypecomponent.push(typecomponentobj);

        // typecomponentobj = {};
        // typecomponentobj['id'] = 2;
        // typecomponentobj['name'] = 'Overseas Studing Staff';
        // this.ftypecomponent.push(typecomponentobj);

         //processList drop down 
         this.DefinationmsService.getStafftypeItems('StaffType').subscribe(
            data => {
                this.processList = data['result'];
            }, error => {
                console.log(error);
            });
        // let mode = {};
        // mode['id'] = 1;
        // mode['name'] = 'Fixed Amount';
        // this.modeList.push(mode);
        // mode = {};
        // mode['id'] = 2;
        // mode['name'] = 'Percentage';
        // this.modeList.push(mode);



        this.processtypeData['f081fstatus'] = 1;

        this.ajaxCount = 0;

        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.ProcesstypeService.getItemsDetail(this.id).subscribe(
                data => {
                    this.processDataHeader['f103fname']= data['result'][0]['f103fname'];
                    this.processtypeList = data['result'];
                }, error => {
                    console.log(error);
                });
        }
    }
    saveProcesstype() {
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        if (this.processDataHeader['f103fname'] == undefined) {
            alert("Enter Process ");
            return false;
        }
       

        let processObject = {};
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        console.log(this.id);
        if (this.id > 0) {
            processObject['f103fid'] = this.id;
        }


        processObject['f103fname'] = this.processDataHeader['f103fname'];
        processObject['process-details'] = this.processtypeList;


        console.log(processObject);

       
            this.ProcesstypeService.insertProcesstypeItems(processObject).subscribe(
                data => {

                    if (data['status'] == 409) {
                        alert('Process Type Already Exist');

                    }
                    else {
                        // this.AlertService.success("Salary has been Added Sucessfully  !!")
                        alert('Process Type  has been Added Successfully !!"');
                        this.router.navigate(['payroll/processtype']);
                    }
                }, error => {
                    console.log(error);
                });
        

    }
    deleteEvent(object) {
        var cnf = confirm("Do you really want to delete");
        if (cnf == true) {
            console.log(this.processtypeList);
            var index = this.processtypeList.indexOf(object);;
            if (index > -1) {
                this.processtypeList.splice(index, 1);
            }
        }
    }
    addProcesstype() {
        console.log(this.processtypeData);
        var confirmPop = confirm("Do you want to Add?");
        if (confirmPop == false) {
            return false;
        }

        if (this.processtypeData['f103fidType'] == undefined) {
            alert(" Select Process Type ");
            return false;
        }
       

        var dataofCurrentRow = this.processtypeData;
        this.processtypeData = {};
        this.processtypeList.push(dataofCurrentRow);
        console.log(this.processtypeList);
    }


}