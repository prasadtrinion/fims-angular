import { Injector } from '@angular/core';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { EpfcategoryService } from "../../service/epfcategory.service";


@Component({
    selector: 'EpfcategoryFormComponent',
    templateUrl: 'epfcategoryform.component.html'
})

export class EpfcategoryFormComponent implements OnInit {
    epfcategoryform: FormGroup;
    epfformList = [];
    epfformData = {};

    id: number;
    constructor(

        private route: ActivatedRoute,
        private router: Router,
        private EpfcategoryService: EpfcategoryService,
       
    ) { }

    ngOnInit() {

        this.epfformData['f030fstatus'] = 1;


        this.epfcategoryform = new FormGroup({
            epfcategoryform: new FormControl('', Validators.required),
        });

        this.epfformData = {};
       
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        if (this.id > 0) {
            this.EpfcategoryService.getItemsDetail(this.id).subscribe(
                data => {
                    this.epfformData = data['result'][0];
                    this.epfformData['f030fstatus'] = parseInt(this.epfformData['f030fstatus']);

                    console.log(this.epfformData);

                }, error => {
                    console.log(error);
                });


        }
        console.log(this.id);
    }

    addEpfcategory() {
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }

        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.EpfcategoryService.updateEpfcategoryItems(this.epfformData, this.id).subscribe(
                data => {
                    alert('EPF Category has been Updated Successfuly');
                    this.router.navigate(['payroll/epfcategory']);
                }, error => {
                    console.log(error);
                });


        } else {

            this.EpfcategoryService.inserteEpfcategoryItems(this.epfformData).subscribe(
                data => {
                    alert('EPF Category  has been Added Successfully');                    
                    this.router.navigate(['payroll/epfcategory']);

                }, error => {
                    console.log(error);
                });

        }
    }

}