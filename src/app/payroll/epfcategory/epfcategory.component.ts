import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { EpfcategoryService } from "../service/epfcategory.service";

@Component({
    selector: 'EpfcategoryComponent',
    templateUrl: 'epfcategory.component.html'
})
export class EpfcategoryComponent implements OnInit {
    epfformList = [];
    epfformData = {};
    id: number;
    constructor(
        private EpfcategoryService: EpfcategoryService
    ) { }

    ngOnInit() {

        this.EpfcategoryService.getItems().subscribe(
            data => {
                this.epfformList = data['result']['data'];

                console.log(this.epfformList);
                
            }, error => {
                console.log(error);
            });
    }
}