import { Injector } from '@angular/core';
import { Component, OnInit, ViewChild } from '@angular/core';
import { StaffService } from '../../../loan/service/staff.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MonthlydeductionService } from '../../service/monthlydeduction.service'
import { SalaryService } from "../../service/salary.service";


@Component({
    selector: 'MonthlydeductionFormComponent',
    templateUrl: 'monthlydeductionform.component.html'
})

export class MonthlydeductionFormComponent implements OnInit {

    monthlyDeductionList = [];
    monthlyDeductionData = {};
    monthlyDeductionDataHeader = {};
    staffList = [];
    deleteList = [];
    salarycodeList = [];
    ajaxCount: number;
    id: number;
    showLastRow: boolean;
    showLastRowMinus: boolean;
    showLastRowPlus: boolean;
    listshowLastRowMinus: boolean;
    listshowLastRowPlus: boolean;
    constructor(

        private MonthlydeductionService: MonthlydeductionService,
        private SalaryService: SalaryService,
        private StaffService: StaffService,
        private route: ActivatedRoute,
        private router: Router

    ) { }

    ngDoCheck() {
        const change = this.ajaxCount;
        if (this.ajaxCount == 0) {

            console.log(this.ajaxCount);
            this.editFunction();
            this.ajaxCount = 10;
        }
    }
    deletemodule() {

        this.showLastRow = true;
        this.showIcons();
    }

    showIcons() {
        if (this.monthlyDeductionList.length > 0) {
            this.showLastRowPlus = true;
            this.showLastRowMinus = true;
            this.listshowLastRowMinus = true;
        }

        if (this.monthlyDeductionList.length > 1) {
            if (this.showLastRow == false) {
                this.listshowLastRowPlus = false;
                this.listshowLastRowMinus = true;
                this.showLastRowPlus = true;

            } else {
                this.listshowLastRowPlus = true;
                this.listshowLastRowMinus = false;

            }
        }

        if (this.monthlyDeductionList.length == 1 && this.showLastRow == false) {
            this.listshowLastRowMinus = true;
            this.listshowLastRowPlus = false;

        }
        if (this.monthlyDeductionList.length == 1 && this.showLastRow == true) {
            this.listshowLastRowMinus = false;
            this.listshowLastRowPlus = true;

        }
        if (this.monthlyDeductionList.length == 0 && this.showLastRow == false) {
            this.showLastRowMinus = false;
            this.showLastRowPlus = true;
        }
        if (this.monthlyDeductionList.length == 0 && this.showLastRow == true) {
            this.showLastRowMinus = true;
            this.showLastRowPlus = false;
        }
        console.log(this.monthlyDeductionList);
        console.log("Show last row" + this.showLastRow);
        console.log("showLastRowMinus" + this.showLastRowMinus);
        console.log("showLastRowPlus" + this.showLastRowPlus);
        console.log("listshowLastRowMinus" + this.listshowLastRowMinus);
        console.log("listshowLastRowPlus" + this.listshowLastRowPlus);
    }

    showNewRow() {
        var confirmPop = confirm("Do you want to Add?");
        if (confirmPop == false) {
            return false;
        }
        this.showLastRow = false;
        this.showIcons();
    }
    editFunction() {
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.MonthlydeductionService.getItemsDetail(this.id).subscribe(
                data => {
                    this.monthlyDeductionList = data['result'];
                    for (var i = 0; i < this.monthlyDeductionList.length; i++) {
                        this.monthlyDeductionList[i]['f082fdeductionCode'] = parseInt(this.monthlyDeductionList[i]['f082fdeductionCode']);
                    }
                    if (data['result'].length > 0) {
                        this.monthlyDeductionDataHeader['f082fidStaff'] = data['result'][0]["f082fidStaff"];
                        this.monthlyDeductionDataHeader['f082fname'] = data['result'][0]["f082fname"];
                    }
                    // this.monthlyDeductionDataHeader['f082fid'] = data['result'][0]["f082fid"];
                    console.log(this.monthlyDeductionList);
                    this.showIcons();
                }, error => {
                    console.log(error);
                });
        }
    }

    ngOnInit() {
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id < 1) {
            this.showLastRow = false;
            this.showLastRowMinus = false;
            this.showLastRowPlus = true;
        } else {
            this.showLastRow = true;

        }
        this.ajaxCount = 0;

        // Staff dropdown
        this.ajaxCount++;
        this.StaffService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.staffList = data['result']['data'];
            }, error => {
                console.log(error);
            });
        // Code  dropdown
        this.ajaxCount++;
        this.SalaryService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                let salaCode = data['data']['data'];
                let newSalaryCode = [];
                for (var i = 0; i < salaCode.length; i++) {
                    if (salaCode[i]['f081ftypeOfComponent'] == 2) {
                        salaCode[i]['fullname'] = salaCode[i]['f081fcode'] + '-' + salaCode[i]['f081fname']
                        newSalaryCode.push(salaCode[i]);
                    }
                }
                this.salarycodeList = newSalaryCode;

            }, error => {
                console.log(error);
            });
        this.showIcons();
    }

    dateComparison() {

        let startDate = Date.parse(this.monthlyDeductionData['f082fstartDate']);
        let endDate = Date.parse(this.monthlyDeductionData['f082fendDate']);

        if (startDate > endDate) {
            alert("Start Date cannot be greater than End Date !");
            this.monthlyDeductionData['f082fendDate'] = "";
        }

    }

    ConvertToFloat(val) {
        return parseFloat(val);
    }
    SaveMonthlyDeduction() {

        if (this.monthlyDeductionDataHeader['f082fidStaff'] == undefined) {
            alert("Select Staff Name");
            return false;
        }
        // if (this.monthlyDeductionDataHeader['f082fname'] == undefined) {
        //     alert("Enter Name");
        //     return false;
        // }
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        if (this.showLastRow == false) {
            var addactivities = this.addData();
            if (addactivities == false) {
                return false;
            }

        }
        let monthlyDeductionObject = {};
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        console.log(this.id);
        if (this.id > 0) {
            monthlyDeductionObject['f052fid'] = this.id;
        }

        monthlyDeductionObject['f082fidStaff'] = this.monthlyDeductionDataHeader['f082fidStaff'];
        monthlyDeductionObject['f082fname'] = '0';
        monthlyDeductionObject['f082fstatus'] = 1;
        monthlyDeductionObject['monthlyDeduction-details'] = this.monthlyDeductionList;

        for (var i = 0; i < monthlyDeductionObject['monthlyDeduction-details'].length; i++) {
            monthlyDeductionObject['monthlyDeduction-details'][i]['f082famount'] = this.ConvertToFloat(monthlyDeductionObject['monthlyDeduction-details'][i]['f082famount']).toFixed(2);
        }
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.MonthlydeductionService.updateItems(monthlyDeductionObject, this.id).subscribe(
                data => {
                    alert('Monthly Deduction has been Updated Successfully');
                    this.router.navigate(['payroll/monthlydeduction']);
                }, error => {
                    console.log(error);
                });


        } else {

            this.MonthlydeductionService.insertItems(monthlyDeductionObject).subscribe(
                data => {
                    alert('Monthly Deduction has been Added Successfully');
                    this.router.navigate(['payroll/monthlydeduction']);

                }, error => {
                    console.log(error);
                });

        }
    }

    deleteData(object) {
        var cnf = confirm("Do you really want to delete");
        if (cnf == true) {
            console.log(this.monthlyDeductionList);
            var index = this.monthlyDeductionList.indexOf(object);;
            this.deleteList.push(this.monthlyDeductionList[index]['f082fidDetails']);
            let deleteObject = {};
            deleteObject['id'] = this.deleteList
            this.MonthlydeductionService.deleteDetail(deleteObject).subscribe(
                data => {
                }, error => {
                    console.log(error);
                })
            if (index > -1) {
                this.monthlyDeductionList.splice(index, 1);
            }
            this.showIcons();
        }
    }

    addData() {
        console.log(this.monthlyDeductionData);

        if (this.monthlyDeductionData['f082fdeductionCode'] == undefined) {
            alert("Select Deduction Code");
            return false;
        }
        if (this.monthlyDeductionData['f082famount'] == undefined) {
            alert("Enter Amount");
            return false;
        }
        if (this.monthlyDeductionData['f082fstartDate'] == undefined) {
            alert("Select Start Date");
            return false;
        }
        if (this.monthlyDeductionData['f082fendDate'] == undefined) {
            alert("Select End Date");
            return false;
        }
        var confirmPop = confirm("Do you want to Add?");
        if (confirmPop == false) {
            return false;
        }

        var dataofCurrentRow = this.monthlyDeductionData;
        this.monthlyDeductionData = {};
        this.monthlyDeductionList.push(dataofCurrentRow);
        this.showIcons();
    }
    onlyNumberKey(event) {
        //console.log(event);
        if (event.charCode == 46) {

        } else {
            return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
        }
    }

}