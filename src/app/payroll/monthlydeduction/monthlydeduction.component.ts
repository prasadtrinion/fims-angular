import { Component, OnInit } from '@angular/core';
import { MonthlydeductionService } from '../service/monthlydeduction.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'MonthlydeductionComponent',
    templateUrl: 'monthlydeduction.component.html'
})
export class MonthlydeductionComponent implements OnInit {
    monthlyDeductionList = [];
    monthlyDeductionData = {};
    id: number;
    constructor(
        private MonthlydeductionService: MonthlydeductionService,
        private spinner: NgxSpinnerService
    ) { }

    ngOnInit() {
        this.spinner.show();
        this.MonthlydeductionService.getItems().subscribe(
            data => {
                this.spinner.hide();
                this.monthlyDeductionList = data['result']['data'];
            }, error => {
                console.log(error);
            });
    }
   
}