import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DeclareincometaxService } from "../service/Declareincometax.service";
import { ActivatedRoute, Router } from '@angular/router';

@Component({
    selector: 'DeclareincometaxComponent',
    templateUrl: 'declareincometax.component.html'
})
export class DeclareincometaxComponent implements OnInit {
    declareincometaxList = [];
    declareincometaxData = {};
    id: number;
    levelfourListdata = {};
    levelfourList = [];
    constructor(
         private DeclareincometaxService: DeclareincometaxService,
         private route: ActivatedRoute,
         private router: Router
    ) { }

    ngOnInit() {
        this.getList();
    }

    getList(){
        this.DeclareincometaxService.getItems().subscribe(
            data => {
                this.levelfourList = data['result']['data'];
            }, error => {
                console.log(error);
            });
    }
    saveLevelfour() {
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }

        let levelthreeObject = {};
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            levelthreeObject['f064fid'] = this.id;
        }

        levelthreeObject['data'] = this.levelfourList;

        this.DeclareincometaxService.inserteDeclareincometaxItems(this.levelfourList).subscribe(
            data => {
                this.getList();
                alert("Data has been updated");

            }, error => {
                console.log(error);
            });
    }

    addLevelfour() {
        console.log(this.levelfourListdata);
        this.levelfourListdata['f082fidStaff'] = 3060;
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        var dataofCurrentRow = this.levelfourListdata;
        this.levelfourList.push(dataofCurrentRow);
        console.log(this.levelfourListdata);
        this.levelfourListdata = {};

    }
}