
import { Injector } from '@angular/core';
import { Component, OnInit, ViewChild } from '@angular/core';
import { StaffService } from '../../../loan/service/staff.service';
import { ActivatedRoute, Router } from '@angular/router';
import { DeclareincometaxService } from '../../service/Declareincometax.service'
import { FinancialyearService } from '../../../generalledger/service/financialyear.service';
import { SalaryService } from "../../service/salary.service";


@Component({
    selector: 'DeclareincometaxFormComponent',
    templateUrl: 'declareincometaxform.component.html'
})

export class DeclareincometaxFormComponent implements OnInit {
    declareincometaxList = [];
    declareincometaxData = {};
    declareincometaxDataHeader = {};
    staffList = [];
    salarycodeList = [];
    fmonth = [];
    ajaxCount: number;
    fstafftype = [];
    fyearList=[];

    id: number;
    constructor(

        private DeclareincometaxService: DeclareincometaxService,
        private FinancialyearService:FinancialyearService,
        private SalaryService: SalaryService,
        private StaffService: StaffService,
        private route: ActivatedRoute,
        private router: Router

    ) { }

    ngDoCheck() {
        const change = this.ajaxCount;
        if (this.ajaxCount == 0) {

            console.log(this.ajaxCount);
            this.editFunction();
            this.ajaxCount = 10;
        }
    }

    editFunction() {
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.DeclareincometaxService.getItemsDetail(this.id).subscribe(
                data => {
                    this.declareincometaxList = data['result'];
                    if (data['result'].length > 0) {
                        this.declareincometaxDataHeader['f082fidStaff'] = data['result'][0]["f082fidStaff"];
                        this.declareincometaxDataHeader['f082fname'] = data['result'][0]["f082fname"];
                    }
                    // this.monthlyDeductionDataHeader['f082fid'] = data['result'][0]["f082fid"];
                    console.log(this.declareincometaxList);
                }, error => {
                    console.log(error);
                });
        }
    }

    ngOnInit() {
        let stafftypeobj = {};
        stafftypeobj['name'] = 'Resident';
        this.fstafftype.push(stafftypeobj);
        stafftypeobj = {};
        stafftypeobj['name'] = 'Non-Resident';
        this.fstafftype.push(stafftypeobj);

        var year = (new Date()).getFullYear().toString().substring(2);
        // var year = new Date('y');
        let month = {};
        month['name'] = 'Jan ' + year;
        this.fmonth.push(month);
        month = {};
        month['name'] = 'Feb ' + year;
        this.fmonth.push(month);
        month = {};
        month['name'] = 'Mar ' + year;
        this.fmonth.push(month);
        month = {};
        month['name'] = 'Apr ' + year;
        this.fmonth.push(month);
        month = {};
        month['name'] = 'May ' + year;
        this.fmonth.push(month);
        month = {};
        month['name'] = 'Jun ' + year;
        this.fmonth.push(month);
        month = {};
        month['name'] = 'Jul ' + year;
        this.fmonth.push(month);
        month = {};
        month['name'] = 'Aug ' + year;
        this.fmonth.push(month);
        month = {};
        month['name'] = 'Sep ' + year;
        this.fmonth.push(month);
        month = {};
        month['name'] = 'Oct ' + year;
        this.fmonth.push(month);
        month = {};
        month['name'] = 'Nov ' + year;
        this.fmonth.push(month);
        month = {};
        month['name'] = 'Dec ' + year;
        this.fmonth.push(month);

        this.ajaxCount = 0;
        //fyear dropdown
        this.FinancialyearService.getActiveItems().subscribe(
            data => {
                this.fyearList = data['result'];
            }, error => {
                console.log(error);
            });
        // Staff dropdown
        this.ajaxCount++;
        this.StaffService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.staffList = data['result']['data'];
            }, error => {
                console.log(error);
            });
        // Code  dropdown
        this.ajaxCount++;
        this.SalaryService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                let salaCode = data['data']['data'];
                let newSalaryCode = [];
                for (var i = 0; i < salaCode.length; i++) {
                    if (salaCode[i]['f081ftypeOfComponent'] == 2) {
                        salaCode[i]['fullname'] = salaCode[i]['f081fcode'] + '-' + salaCode[i]['f081fname']
                        newSalaryCode.push(salaCode[i]);
                    }
                }
                this.salarycodeList = newSalaryCode;

            }, error => {
                console.log(error);
            });
    }

    
    SaveDeclareincometax() {

        if (this.declareincometaxDataHeader['f082fidStaff'] == undefined) {
            alert("Select Staff Name");
            return false;
        }
        if (this.declareincometaxDataHeader['f070fmonth'] == undefined) {
            alert("Select Month");
            return false;
        }
        if (this.declareincometaxDataHeader['f092fyear'] == undefined) {
            alert("Select Financial Year");
            return false;
        }

        let declareDeductionObject = {};
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        console.log(this.id);
        if (this.id > 0) {
            declareDeductionObject['f052fid'] = this.id;
        }

        declareDeductionObject['f082fidStaff'] = this.declareincometaxDataHeader['f082fidStaff'];
        declareDeductionObject['f082fname'] = '0';
        declareDeductionObject['f082fstatus'] = 1;
        declareDeductionObject['monthlyDeduction-details'] = this.declareincometaxList;
        console.log(declareDeductionObject);

        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.DeclareincometaxService.updateDeclareincometaxItems(declareDeductionObject, this.id).subscribe(
                data => {
                    alert('Declare Incometax has been Updated');
                    this.router.navigate(['payroll/declareincometax']);
                }, error => {
                    console.log(error);
                });
        } else {

            this.DeclareincometaxService.inserteDeclareincometaxItems(declareDeductionObject).subscribe(
                data => {
                    alert('Declare Incometax has been has saved');
                    this.router.navigate(['payroll/declareincometax']);

                }, error => {
                    console.log(error);
                });
        }
    }

    deleteData(object) {
        var cnf = confirm("Do you really want to delete");
        if (cnf == true) {
            console.log(this.declareincometaxList);
            var index = this.declareincometaxList.indexOf(object);;
            if (index > -1) {
                this.declareincometaxList.splice(index, 1);
            }
        }
    }
    addData() {
        console.log(this.declareincometaxData);

        if (this.declareincometaxData['f082fdeductionCode'] == undefined) {
            alert("Please Enter Deduction Code");
            return false;
        }
        if (this.declareincometaxData['f082famount'] == undefined) {
            alert("Please Enter Amount");
            return false;
        }
        var dataofCurrentRow = this.declareincometaxData;
        this.declareincometaxData = {};
        this.declareincometaxList.push(dataofCurrentRow);
    }
    onlyNumberKey(event) {
        console.log(event);
        return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
    }

}