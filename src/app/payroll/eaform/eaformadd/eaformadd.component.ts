import { Injector } from '@angular/core';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { EaformService } from "../../service/eaform.service";
import { FinancialyearService } from '../../../generalledger/service/financialyear.service';




@Component({
    selector: 'EaformaddComponent',
    templateUrl: 'eaformadd.component.html'
})

export class EaformaddComponent implements OnInit {
    eaformaddform: FormGroup;
    eaformList = [];
    eaformData = {};
    fyearList=[];

    id: number;
    constructor(

        private route: ActivatedRoute,
        private router: Router,
        private EaformService: EaformService,
        private FinancialyearService:FinancialyearService
    ) { }

    ngOnInit() {

        this.eaformData['f092fstatus'] = 1;

        this.eaformaddform = new FormGroup({
            eaformadd: new FormControl('', Validators.required),
        });
        //fyear dropdown
        this.FinancialyearService.getActiveItems().subscribe(
            data => {
                this.fyearList = data['result'];
            }, error => {
                console.log(error);
            });
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        if (this.id > 0) {
            this.EaformService.getItemsDetail(this.id).subscribe(
                data => {
                    this.eaformData = data['result'][0];
                    this.eaformData['f092fstatus'] = parseInt(this.eaformData['f092fstatus']);
                    console.log(this.eaformData);
                }, error => {
                    console.log(error);
                });
        }
        console.log(this.id);
    }

    addEaform() {
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }

        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.EaformService.updateEaformItems(this.eaformData, this.id).subscribe(
                data => {
                    alert('EA Form has been Updated Successfully');
                    this.router.navigate(['payroll/eaform']);
                }, error => {
                    console.log(error);
                });
        } else {
            this.EaformService.inserteEaformItems(this.eaformData).subscribe(
                data => {
                    alert('EA Form has been Added Successfully');                    
                    this.router.navigate(['payroll/eaform']);

                }, error => {
                    console.log(error);
                });
        }
    }

}