import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { EaformService } from "../service/eaform.service";
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'EaformComponent',
    templateUrl: 'eaform.component.html'
})
export class EaformComponent implements OnInit {
    eaformList = [];
    eaformData = {};
    id: number;
    constructor(
        private EaformService: EaformService,
        private spinner: NgxSpinnerService
    ) { }

    ngOnInit() {
        this.spinner.show();
        this.EaformService.getItems().subscribe(
            data => {
                this.spinner.hide();
                this.eaformList = data['result']['data'];
            }, error => {
                console.log(error);
            });
    }
}