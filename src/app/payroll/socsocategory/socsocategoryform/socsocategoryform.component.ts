import { Injector } from '@angular/core';
import { Component, OnInit, ViewChild } from '@angular/core';

import { ActivatedRoute, Router } from '@angular/router';
import { SocsocategoryService } from "../../service/socsocategory.service";


@Component({
    selector: 'SocsocategoryformComponent',
    templateUrl: 'socsocategoryform.component.html'
})

export class SocsocategoryformComponent implements OnInit {

    socsocategoryList = [];
    socsocategoryData = {};
    ftypeList = [];
    id: number;
    constructor(

        private SocsocategoryService: SocsocategoryService,
        private route: ActivatedRoute,
        private router: Router,

    ) { }

    ngOnInit() {
        // let typecomponentobj = {};
        // typecomponentobj['id'] = 1;
        // typecomponentobj['name'] = 'Resident';
        // this.ftypeList.push(typecomponentobj);
        // typecomponentobj = {};
        // typecomponentobj['id'] = 2;
        // typecomponentobj['name'] = 'Non-Resident';
        // this.ftypeList.push(typecomponentobj);

        this.socsocategoryData['f091fstatus'] = 1;
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        if (this.id > 0) {
            this.SocsocategoryService.getItemsDetail(this.id).subscribe(
                data => {
                    this.socsocategoryData = data['result'][0];
                    this.socsocategoryData['f091fstatus'] = parseInt(this.socsocategoryData['f091fstatus']);
                    console.log(this.socsocategoryData);
                }, error => {
                    console.log(error);
                });
        }
    }
    addSocsocategory() {
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
      
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.SocsocategoryService.updateSocsocategoryItems(this.socsocategoryData, this.id).subscribe(
                data => {
                    alert('SOCSO Category has been Updated Successfully');
                    this.router.navigate(['payroll/socsocategory']);
                }, error => {
                    console.log(error);
                });


        } else {

            this.SocsocategoryService.inserteSocsocategoryItems(this.socsocategoryData).subscribe(
                data => {
                    console.log(data);
                    alert('SOCSO Category has been Added Successfully');
                    this.router.navigate(['payroll/socsocategory']);

                }, error => {
                    console.log(error);
                });

        }
    }
    onlyNumberKey(event) {
        console.log(event);
        return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
    }
}
