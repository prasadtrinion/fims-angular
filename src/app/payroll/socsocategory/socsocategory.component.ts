import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SocsocategoryService } from "../service/socsocategory.service";

@Component({
    selector: 'SocsocategoryComponent',
    templateUrl: 'socsocategory.component.html'
})
export class SocsocategoryComponent implements OnInit {
    socsocategoryList = [];
    socsocategoryData = {};
    id: number;
    constructor(
        private SocsocategoryService: SocsocategoryService
    ) { }

    ngOnInit() {

        this.SocsocategoryService.getItems().subscribe(
            data => {
                this.socsocategoryList = data['result']['data'];
            }, error => {
                console.log(error);
            });
    }

}
    
