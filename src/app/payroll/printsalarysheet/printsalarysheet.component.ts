import { Injector } from '@angular/core';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { PrintsalarysheetService } from "../service/printsalarysheet.service";
import { DepartmentService } from '../../generalsetup/service/department.service';
import { StaffService } from '../../loan/service/staff.service';


@Component({
    selector: 'PrintsalarysheetComponent',
    templateUrl: 'printsalarysheet.component.html'
})

export class PrintsalarysheetComponent implements OnInit {
    printsalarysheet : FormGroup;
    printsalarysheetList = [];
    printsalarysheetDate = {};
    DeptList=[];
    staffList=[];
    ajaxCount: number;

    id: number;
    constructor(

        private route: ActivatedRoute,
        private router: Router,
        private PrintsalarysheetService: PrintsalarysheetService,
        private DepartmentService:DepartmentService,
        private StaffService:StaffService

    ) { }

    ngOnInit() {

       

        this.printsalarysheet = new FormGroup({
            printsalarysheet: new FormControl('', Validators.required),
        });

          // Staff dropdown
          this.ajaxCount++;
          this.StaffService.getItems().subscribe(
              data => {
                  this.ajaxCount--;
                  this.staffList = data['result']['data'];
              }, error => {
                  console.log(error);
              });

                   // department dropdown
        this.ajaxCount++;
        this.DepartmentService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.DeptList = data['result']['data'];
            }, error => {
                //console.log(error);
            });   

        this.printsalarysheetDate = {};
       
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        if (this.id > 0) {
            this.PrintsalarysheetService.getItemsDetail(this.id).subscribe(
                data => {
                    this.printsalarysheetDate = data['result'][0];
                    //this.epfformData['f030fstatus'] = parseInt(this.epfformData['f030fstatus']);

                    console.log(this.printsalarysheetDate);

                }, error => {
                    console.log(error);
                });


        }
        console.log(this.id);
    }
}
//     addEpfcategory() {
//         var confirmPop = confirm("Do you want to Save?");
//         if (confirmPop == false) {
//             return false;
//         }

//         this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
//         if (this.id > 0) {
//             this.EpfcategoryService.updateEpfcategoryItems(this.epfformData, this.id).subscribe(
//                 data => {
//                     alert('EPF Category Updated');
//                     this.router.navigate(['payroll/epfcategory']);
//                 }, error => {
//                     console.log(error);
//                 });


//         } else {

//             this.EpfcategoryService.inserteEpfcategoryItems(this.epfformData).subscribe(
//                 data => {
//                     alert('EPF Category  has been saved');                    
//                     this.router.navigate(['payroll/epfcategory']);

//                 }, error => {
//                     console.log(error);
//                 });

//         }
//     }

// 