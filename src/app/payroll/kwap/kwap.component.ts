import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { KwapService } from "../service/kwap.service";

@Component({
    selector: 'KwapComponent',
    templateUrl: 'kwap.component.html'
})
export class KwapComponent implements OnInit {
    kwapList = [];
    kwapData = {};
    id: number;
    constructor(
         private KwapService: KwapService
    ) { }

    ngOnInit() {

        this.KwapService.getItems().subscribe(
            data => {
                this.kwapList = data['result']['data'];
            }, error => {
                console.log(error);
            });
    }
}