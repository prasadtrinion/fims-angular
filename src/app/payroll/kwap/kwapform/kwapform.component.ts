import { Injector } from '@angular/core';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { KwapService } from '../../service/kwap.service'



@Component({
    selector: 'KwapFormComponent',
    templateUrl: 'kwapform.component.html'
})

export class KwapFormComponent implements OnInit {
    kwapform: FormGroup; 
    kwapList = [];
    kwapData = {};

    id: number;
    constructor(

        private KwapService: KwapService,
        private route: ActivatedRoute,
        private router: Router

    ) { }

    ngOnInit() {

        this.kwapform = new FormGroup({
            kwap: new FormControl('', Validators.required),  
        });
      
       
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        
        if (this.id > 0) {
            this.KwapService.getItemsDetail(this.id).subscribe(
                data => {
                    this.kwapData = data['result'][0];
                    this.kwapData['f070fstatus'] = parseInt(this.kwapData['f070fstatus']);
                console.log(this.kwapData);
                }, error => {
                    console.log(error);
                });
        }
        console.log(this.id);

       
    }

    // dateComparison(){

    //     let startDate = Date.parse(this.kwapData['f070fstartDate']);
    //     let endDate = Date.parse(this.kwapData['f070fendDate']);

    //     if(startDate>endDate){
    //         alert("Start Date cannot be greater than End Date !");
    //         this.kwapData['f070fendDate'] = "";
    //     }

    // }
    addKwap() {
        if(this.kwapData['f102fkwapPercentage']<0){
            alert("KWAP Percentage cannot be negative..!");
            this.kwapData['f102fkwapPercentage'] = 0;
            return false;
        }
        var confirmPop = confirm("Do you want to Save?");
        if(confirmPop==false) 
        {
            return false;
        } 
       
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.KwapService.updatekwapItems(this.kwapData, this.id).subscribe(
                data => {
                    alert('KWAP has been Updated Successfully');
                    this.router.navigate(['payroll/kwap']);
                }, error => {
                    console.log(error);
                });


        } else {

            this.KwapService.insertkwapItems(this.kwapData).subscribe(
                data => {
                    alert('KWAP has been Added Successfully');
                    this.router.navigate(['payroll/kwap']);

                }, error => {
                    console.log(error);
                });

        }
    }

}