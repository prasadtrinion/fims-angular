import { Injector } from '@angular/core';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ProcesssalaryService } from '../../service/processsalary.service'
import { ProcesstypeService } from "../../service/processtype.service";



@Component({
    selector: 'ProcesssalaryFormComponent',
    templateUrl: 'processsalaryform.component.html'
})

export class ProcesssalaryFormComponent implements OnInit {
    processsalaryform: FormGroup; 
    processsalaryList = [];
    processsalaryData = {};
    fmonth = [];
    processtype=[];
    id: number;
    percentageForTaxDeduction = 28;
    constructor(

        private ProcesssalaryService: ProcesssalaryService,
        private ProcesstypeService:ProcesstypeService,
        private route: ActivatedRoute,
        private router: Router

    ) { }

    ngOnInit() {

        let employeeArray = [];
        let empObj = {};
        empObj['empId'] = '123';
        empObj['resident'] = '1';

        employeeArray.push(empObj);
        empObj = {};
        empObj['empId'] = '1234';
        empObj['resident'] = '0';

        employeeArray.push(empObj);

        let month = {};
        month['empId'] = '123';
        month['amount'] = '10000';
        month['earningcomponent'] = '1';
        this.fmonth.push(month);

        month = {};
        month['empId'] = '1234';
        month['amount'] = '10000';
        month['earningcomponent'] = '2';
        this.fmonth.push(month);

        month = {};
        month['empId'] = '1234';
        month['amount'] = '10000';
        month['earningcomponent'] = '1';
        this.fmonth.push(month);
        console.log(employeeArray);

        for(var i=0;i<employeeArray.length;i++) {
            var empId = employeeArray[i]['empId'];
            if(employeeArray[i]['resident']=='0') {
                employeeArray[i]['taxCalculated'] = this.nonResidentOfMalaysia(empId);
            } else  {
                
            }
        }


        console.log(employeeArray);

       
    }

    taxITCalculation() {
   //4 steps
    }

    nonResidentOfMalaysia(empId){
        let employeeEarning = 0
        for(var j=0;j<this.fmonth.length;j++){
            if(this.fmonth[j]['empId']==empId && this.fmonth[j]['earningcomponent']=='1'){
                employeeEarning = (employeeEarning) + parseFloat(this.fmonth[j]['amount']);

            }
        }
        return ((employeeEarning))*((this.percentageForTaxDeduction)/100);
    }
    

    addProcesssalary() {

        var confirmPop = confirm("Do you want to Save?");
        if(confirmPop==false) 
        {
            return false;
        } 
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.ProcesssalaryService.updateProcesssalarytems(this.processsalaryData, this.id).subscribe(
                data => {
                    alert('Processsalary has been Updated Successfully');
                    this.router.navigate(['payroll/processsalary']);
                }, error => {
                    console.log(error);
                });


        } else {

            this.ProcesssalaryService.insertProcesssalaryItems(this.processsalaryData).subscribe(
                data => {
                    alert('Processsalary has been Added Successfully');
                    this.router.navigate(['payroll/processsalary']);

                }, error => {
                    console.log(error);
                });

        }
    }

}