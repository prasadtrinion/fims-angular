import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ProcesssalaryService } from "../service/processsalary.service";

@Component({
    selector: 'ProcesssalaryComponent',
    templateUrl: 'processsalary.component.html'
})
export class ProcesssalaryComponent implements OnInit {
    processsalaryList = [];
    processsalaryData = {};
    id: number;
    constructor(
         private ProcesssalaryService: ProcesssalaryService
    ) { }

    ngOnInit() {

        this.ProcesssalaryService.getItems().subscribe(
            data => {
                this.processsalaryList = data['result']['data'];
            }, error => {
                console.log(error);
            });
    }
}