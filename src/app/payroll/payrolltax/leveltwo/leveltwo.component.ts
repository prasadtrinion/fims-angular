import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PayrolltaxService } from '../../service/payrolltax.service'
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
    selector: 'LeveltwoComponent',
    templateUrl: 'leveltwo.component.html'
})

export class LeveltwoComponent implements OnInit {
    leveltwoList = [];
    leveltwoData = {};
    deleteList = [];
    id: number;
    levelone: number;
    showLastRow: boolean;
    showLastRowMinus: boolean;
    showLastRowPlus: boolean;
    listshowLastRowMinus: boolean;
    listshowLastRowPlus: boolean;
    constructor(
        private PayrolltaxService: PayrolltaxService,
        private route: ActivatedRoute,
        private router: Router
    ) { }
    deletemodule() {
        this.showLastRow = true;
        this.showIcons();
    }

    showIcons() {
        if (this.leveltwoList.length > 0) {
            this.showLastRowMinus = true;
            this.showLastRowPlus = true;
            this.listshowLastRowMinus = true;
        }

        if (this.leveltwoList.length > 1) {
            if (this.showLastRow == false) {
                this.listshowLastRowPlus = false;
                this.listshowLastRowMinus = true;
                this.showLastRowPlus = true;

            } else {
                this.listshowLastRowPlus = true;
                this.listshowLastRowMinus = false;

            }
        }
        if (this.leveltwoList.length == 1 && this.showLastRow == false) {
            this.listshowLastRowMinus = true;
            this.listshowLastRowPlus = false;

        }
        if (this.leveltwoList.length == 1 && this.showLastRow == true) {
            this.listshowLastRowMinus = false;
            this.listshowLastRowPlus = true;

        }
        if (this.leveltwoList.length == 0 && this.showLastRow == false) {
            this.showLastRowMinus = false;
            this.showLastRowPlus = true;
        }
        if (this.leveltwoList.length == 0 && this.showLastRow == true) {
            this.showLastRowMinus = true;
            this.showLastRowPlus = false;
        }

        console.log(this.leveltwoList.length);
        console.log("Show last row" + this.showLastRow);
        console.log("showLastRowMinus" + this.showLastRowMinus);
        console.log("showLastRowPlus" + this.showLastRowPlus);
        console.log("listshowLastRowMinus" + this.listshowLastRowMinus);
        console.log("listshowLastRowPlus" + this.listshowLastRowPlus);

    }
    showNewRow() {
        var confirmPop = confirm("Do you want to Add?");
        if (confirmPop == false) {
            return false;
        }
        this.showLastRow = false;
        this.showIcons();
    }
    ngOnInit() {
        this.getList();
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id < 1) {
            this.showLastRow = false;
            this.showLastRowMinus = false;
            this.listshowLastRowPlus = false;
            this.showLastRowPlus = true;
            this.listshowLastRowMinus = true;
        } else {
            this.showLastRow = true;
        }
        this.showIcons();
    }
    onBlurMethod(object) {
        console.log(object);
        sessionStorage.removeItem("accountleveltwoparent");
        sessionStorage.removeItem("accountleveltwocode");
        // sessionStorage.setItem("accountleveltwoparent", object['f063fid']);s
        sessionStorage.setItem("accountleveltwocode", object['f059fcode']);

    }

    getList() {
        this.leveltwoData['f063fstatus'] = 1;

        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.PayrolltaxService.getPayrollTaxExcemptionItemDetail().subscribe(
            data => {
                this.leveltwoList = data['result']['data'];
            }, error => {
                console.log(error);
            });

        console.log(this.id);
    }

    saveLeveltwo() {
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        if (this.showLastRow == false) {
            var addactivities = this.addLeveltwo();
            if (addactivities == false) {
                return false;
            }

        }
        let leveltwoObject = {};
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        // if (this.id > 0) {
        //     leveltwoObject['f063fid'] = this.id;
        // }

        leveltwoObject['data'] = this.leveltwoList;

        this.PayrolltaxService.insertPayrolltaxexcemptionItems(leveltwoObject).subscribe(
            data => {
                this.getList();
                alert("Data has been updated");

            }, error => {
                console.log(error);
            });
    }


    addLeveltwo() {
        if (this.leveltwoData['f063fcategory'] == undefined) {
            alert("Please Enter Category ");
            return false;
        }
        if (this.leveltwoData['f063findividualExpenses'] == undefined) {
            alert("Please Enter Individual Expenses");
            return false;
        }
        if (this.leveltwoData['f063fspouseExpenses'] == undefined) {
            alert("Please Enter Spouse Expenses");
            return false;
        }
        if (this.leveltwoData['f063fchildExpenses'] == undefined) {
            alert("Please Enter Child Expenses");
            return false;
        }
        var confirmPop = confirm("Do you want to Add?");
        if (confirmPop == false) {
            return false;
        }
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        var dataofCurrentRow = this.leveltwoData;
        this.leveltwoList.push(dataofCurrentRow);
        console.log(this.leveltwoData);
        this.leveltwoData = {};
        this.showIcons();
    }
    deleteleveltwo(object) {

        var cnf = confirm("Do you really want to delete");
        var index = this.leveltwoList.indexOf(object);
        this.deleteList.push(this.leveltwoList[index]['f063fid']);
        let deleteObject = {};
        deleteObject['type'] = "payrollExcemption";
        deleteObject['id'] = this.deleteList;
        this.PayrolltaxService.deleteItems(deleteObject).subscribe(
            data => {
            }, error => {
                console.log(error);
            });
        if (index > -1) {
            this.leveltwoList.splice(index, 1);
        }
        this.showIcons();

    }
    onlyNumberKey(event) {
        //console.log(event);
        if (event.charCode == 46) {

        } else {
            return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
        }
    }
}
