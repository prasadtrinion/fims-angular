import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PayrolltaxService } from '../../service/payrolltax.service'

import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
    selector: 'LeveloneComponent',
    templateUrl: 'levelone.component.html'
})

export class LeveloneComponent implements OnInit {
    leveloneList = [];
    leveloneData = {};
    leveloneDataheader = {};
    categoryList = [];
    deleteList = [];
    id: number;
    showLastRow: boolean;
    showLastRowMinus: boolean;
    showLastRowPlus: boolean;
    listshowLastRowMinus: boolean;
    listshowLastRowPlus: boolean;
    constructor(
        private PayrolltaxService: PayrolltaxService,

        private route: ActivatedRoute,
        private router: Router
    ) { }

    ngOnInit() {
        this.getList();
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id < 1) {
            this.showLastRow = false;
            this.showLastRowMinus = false;
            this.listshowLastRowPlus = false;
            this.showLastRowPlus = true;
            this.listshowLastRowMinus = true;
        } else {
            this.showLastRow = true;
        }
        this.showIcons();
    }
    deletemodule() {
        this.showLastRow = true;
        this.showIcons();
    }

    showIcons() {
        if (this.leveloneList.length > 0) {
            this.showLastRowMinus = true;
            this.showLastRowPlus = true;
            this.listshowLastRowMinus = true;
        }

        if (this.leveloneList.length > 1) {
            if (this.showLastRow == false) {
                this.listshowLastRowPlus = false;
                this.listshowLastRowMinus = true;
                this.showLastRowPlus = true;

            } else {
                this.listshowLastRowPlus = true;
                this.listshowLastRowMinus = false;

            }
        }
        if (this.leveloneList.length == 1 && this.showLastRow == false) {
            this.listshowLastRowMinus = true;
            this.listshowLastRowPlus = false;

        }
        if (this.leveloneList.length == 1 && this.showLastRow == true) {
            this.listshowLastRowMinus = false;
            this.listshowLastRowPlus = true;

        }
        if (this.leveloneList.length == 0 && this.showLastRow == false) {
            this.showLastRowMinus = false;
            this.showLastRowPlus = true;
        }
        if (this.leveloneList.length == 0 && this.showLastRow == true) {
            this.showLastRowMinus = true;
            this.showLastRowPlus = false;
        }

        console.log(this.leveloneList.length);
        console.log("Show last row" + this.showLastRow);
        console.log("showLastRowMinus" + this.showLastRowMinus);
        console.log("showLastRowPlus" + this.showLastRowPlus);
        console.log("listshowLastRowMinus" + this.listshowLastRowMinus);
        console.log("listshowLastRowPlus" + this.listshowLastRowPlus);

    }
    showNewRow() {
        var confirmPop = confirm("Do you want to Add?");
        if (confirmPop == false) {
            return false;
        }
        this.showLastRow = false;
        this.showIcons();
    }

    saveLevelone() {
        if (this.showLastRow == false) {
            var addactivities = this.addLevelone();
            if (addactivities == false) {
                return false;
            }

        }
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        let leveloneObject = {};
        leveloneObject['data'] = this.leveloneList;
        this.PayrolltaxService.insertPayrolltaxItems(leveloneObject).subscribe(
            data => {

                this.getList();
                alert("Data has been updated");

            }, error => {
                console.log(error);
            });
    }

    getList() {
        this.leveloneData['f062fstatus'] = 1;

        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));



        this.PayrolltaxService.getPayrollTaxItemDetail().subscribe(
            data => {
                this.leveloneList = data['result']['data'];
            }, error => {
                console.log(error);
            });


        console.log(this.id);

    }

    addLevelone() {

        console.log(this.leveloneData);
        if (this.leveloneData['f062fpStart'] == undefined) {
            alert("Please Enter P Start");
            return false;
        }
        if (this.leveloneData['f062fpEnd'] == undefined) {
            alert("Please Enter P End");
            return false;
        }
        if (this.leveloneData['f062fm'] == undefined) {
            alert("Please Enter M");
            return false;
        }
        if (this.leveloneData['f062fr'] == undefined) {
            alert("Please Enter R");
            return false;
        }
        if (this.leveloneData['f062fc1'] == undefined) {
            alert("Please Enter C1");
            return false;
        }
        if (this.leveloneData['f062fc2'] == undefined) {
            alert("Please Enter C2");
            return false;
        }
        if (this.leveloneData['f062fc3'] == undefined) {
            alert("Please Enter C3");
            return false;
        }
        var confirmPop = confirm("Do you want to Add?");
        if (confirmPop == false) {
            return false;
        }
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        var dataofCurrentRow = this.leveloneData;
        this.leveloneList.push(dataofCurrentRow);
        console.log(this.leveloneData);
        this.leveloneData = {};
        this.showIcons();
    }
    deletelevelone(object) {

        var cnf = confirm("Do you really want to delete");
        var index = this.leveloneList.indexOf(object);
        this.deleteList.push(this.leveloneList[index]['f062fid']);
        let deleteObject = {};
        deleteObject['type'] = "payrollSchedule";
        deleteObject['id'] = this.deleteList;
        this.PayrolltaxService.deleteItems(deleteObject).subscribe(
            data => {
            }, error => {
                console.log(error);
            });
        if (index > -1) {
            this.leveloneList.splice(index, 1);
        }
    }
    onlyNumberKey(event) {
        //console.log(event);
        if (event.charCode == 46) {

        } else {
            return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
        }
    }
}


