import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PayrolltaxService } from '../../service/payrolltax.service'
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
    selector: 'LevelthreeComponent',
    templateUrl: 'levelthree.component.html'
})

export class LevelthreeComponent implements OnInit {
    levelthreeList = [];
    levelthreeData = {};
    deleteList = [];
    id: number;
    levelone: number;
    leveltwo: number;
    showLastRow: boolean;
    showLastRowMinus: boolean;
    showLastRowPlus: boolean;
    listshowLastRowMinus: boolean;
    listshowLastRowPlus: boolean;
    constructor(
        private PayrolltaxService: PayrolltaxService,
        private route: ActivatedRoute,
        private router: Router
    ) { }
    ngOnInit() {
        this.getList();
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id < 1) {
            this.showLastRow = false;
            this.showLastRowMinus = false;
            this.listshowLastRowPlus = false;
            this.showLastRowPlus = true;
            this.listshowLastRowMinus = true;
        } else {
            this.showLastRow = true;
        }
        this.showIcons();
    }
    deletemodule() {
        this.showLastRow = true;
        this.showIcons();
    }

    showIcons() {
        if (this.levelthreeList.length > 0) {
            this.showLastRowMinus = true;
            this.showLastRowPlus = true;
            this.listshowLastRowMinus = true;
        }

        if (this.levelthreeList.length > 1) {
            if (this.showLastRow == false) {
                this.listshowLastRowPlus = false;
                this.listshowLastRowMinus = true;
                this.showLastRowPlus = true;

            } else {
                this.listshowLastRowPlus = true;
                this.listshowLastRowMinus = false;

            }
        }
        if (this.levelthreeList.length == 1 && this.showLastRow == false) {
            this.listshowLastRowMinus = true;
            this.listshowLastRowPlus = false;

        }
        if (this.levelthreeList.length == 1 && this.showLastRow == true) {
            this.listshowLastRowMinus = false;
            this.listshowLastRowPlus = true;

        }
        if (this.levelthreeList.length == 0 && this.showLastRow == false) {
            this.showLastRowMinus = false;
            this.showLastRowPlus = true;
        }
        if (this.levelthreeList.length == 0 && this.showLastRow == true) {
            this.showLastRowMinus = true;
            this.showLastRowPlus = false;
        }

        console.log(this.levelthreeList.length);
        console.log("Show last row" + this.showLastRow);
        console.log("showLastRowMinus" + this.showLastRowMinus);
        console.log("showLastRowPlus" + this.showLastRowPlus);
        console.log("listshowLastRowMinus" + this.listshowLastRowMinus);
        console.log("listshowLastRowPlus" + this.listshowLastRowPlus);

    }
    showNewRow() {
        var confirmPop = confirm("Do you want to Add?");
        if (confirmPop == false) {
            return false;
        }
        this.showLastRow = false;
        this.showIcons();
    }

    getList() {

        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        console.log(this.id);
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        console.log(this.id);
        let levelId = 2;

        this.PayrolltaxService.getPayrollTaxcontributionItemDetail().subscribe(
            data => {
                this.levelthreeList = data['result']['data'];
            }, error => {
                console.log(error);
            });
        console.log(this.id);
    }

    saveLevelthree() {
        if (this.showLastRow == false) {
            var addactivities = this.addLevelthree();
            if (addactivities == false) {
                return false;
            }

        }
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        let levelthreeObject = {};
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            levelthreeObject['f059fid'] = this.id;
        }

        levelthreeObject['data'] = this.levelthreeList;

        this.PayrolltaxService.insertPayrolltaxcontributionItems(levelthreeObject).subscribe(
            data => {
                this.getList();
                alert("Data has been updated");

            }, error => {
                console.log(error);
            });
    }
    addLevelthree() {

        console.log(this.levelthreeData);
        if (this.levelthreeData['f061fmonth'] == undefined) {
            alert("Please Enter EPF Contribution per Month");
            return false;
        }
        var confirmPop = confirm("Do you want to Add?");
        if (confirmPop == false) {
            return false;
        }
        this.levelthreeData['f061fstatus'] = 2;
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        var dataofCurrentRow = this.levelthreeData;
        this.levelthreeList.push(dataofCurrentRow);
        console.log(this.levelthreeData);
        this.levelthreeData = {};
        this.showIcons();
    }
    deletelevelthree(object) {

        var cnf = confirm("Do you really want to delete");
        var index = this.levelthreeList.indexOf(object);
        this.deleteList.push(this.levelthreeList[index]['f061fid']);
        if (index > -1) {
            this.levelthreeList.splice(index, 1);
        }
        let deleteObject = {};
        deleteObject['type'] = "payrollContribution";
        deleteObject['id'] = this.deleteList;
        this.PayrolltaxService.deleteItems(deleteObject).subscribe(
            data => {
            }, error => {
                console.log(error);
            });

        this.showIcons();
    }
    onlyNumberKey(event) {
        //console.log(event);
        if (event.charCode == 46) {

        } else {
            return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
        }
    }
}
