import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PayrolltaxService } from '../../service/payrolltax.service'
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
    selector: 'LevelfourComponent',
    templateUrl: 'levelfour.component.html'
})

export class LevelfourComponent implements OnInit {
    levelfourList = [];
    levelfourData = {};
    deleteList = [];
    id: number;
    levelone: number;
    leveltwo: number;
    showLastRow: boolean;
    showLastRowMinus: boolean;
    showLastRowPlus: boolean;
    listshowLastRowMinus: boolean;
    listshowLastRowPlus: boolean;
    type: string;
    constructor(
        private PayrolltaxService: PayrolltaxService,
        private route: ActivatedRoute,
        private router: Router
    ) { }
    deletemodule() {
        this.showLastRow = true;
        this.showIcons();
    }

    showIcons() {
        if (this.levelfourList.length > 0) {
            this.showLastRowMinus = true;
            this.showLastRowPlus = true;
            this.listshowLastRowMinus = true;
        }

        if (this.levelfourList.length > 1) {
            if (this.showLastRow == false) {
                this.listshowLastRowPlus = false;
                this.listshowLastRowMinus = true;
                this.showLastRowPlus = true;

            } else {
                this.listshowLastRowPlus = true;
                this.listshowLastRowMinus = false;

            }
        }
        if (this.levelfourList.length == 1 && this.showLastRow == false) {
            this.listshowLastRowMinus = true;
            this.listshowLastRowPlus = false;

        }
        if (this.levelfourList.length == 1 && this.showLastRow == true) {
            this.listshowLastRowMinus = false;
            this.listshowLastRowPlus = true;

        }
        if (this.levelfourList.length == 0 && this.showLastRow == false) {
            this.showLastRowMinus = false;
            this.showLastRowPlus = true;
        }
        if (this.levelfourList.length == 0 && this.showLastRow == true) {
            this.showLastRowMinus = true;
            this.showLastRowPlus = false;
        }

        console.log(this.levelfourList.length);
        console.log("Show last row" + this.showLastRow);
        console.log("showLastRowMinus" + this.showLastRowMinus);
        console.log("showLastRowPlus" + this.showLastRowPlus);
        console.log("listshowLastRowMinus" + this.listshowLastRowMinus);
        console.log("listshowLastRowPlus" + this.listshowLastRowPlus);

    }
    showNewRow() {
        var confirmPop = confirm("Do you want to Add?");
        if (confirmPop == false) {
            return false;
        }
        this.showLastRow = false;
        this.showIcons();
    }
    ngOnInit() {
        this.getList();
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id < 1) {
            this.showLastRow = false;
            this.showLastRowMinus = false;
            this.listshowLastRowPlus = false;
            this.showLastRowPlus = true;
            this.listshowLastRowMinus = true;
        } else {
            this.showLastRow = true;
        }
        this.showIcons();
    }

    getList() {
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        console.log(this.id);
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        console.log(this.id);
        let levelId = 2;

        this.PayrolltaxService.getPayrollTaxdeductionItemDetail().subscribe(
            data => {
                this.levelfourList = data['result']['data'];
            }, error => {
                console.log(error);
            });
        console.log(this.id);
    }
    saveLevelfour() {
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        if (this.showLastRow == false) {
            var addactivities = this.addLevelfour();
            // if (addactivities == false) {
            //     return false;
            // }

        }
        let levelthreeObject = {};
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            levelthreeObject['f064fid'] = this.id;
        }

        levelthreeObject['data'] = this.levelfourList;
        // levelthreeObject['data'] = this.deleteList;

        this.PayrolltaxService.insertPayrolltaxdeductionItems(levelthreeObject).subscribe(
            data => {
                this.getList();
                alert("Data has been updated");

            }, error => {
                console.log(error);
            });
    }
    deletelevelfour(object) {
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        var cnf = confirm("Do you really want to delete");
        if (cnf == true) {
            console.log(this.levelfourList);
            var index = this.levelfourList.indexOf(object);
            this.deleteList.push(this.levelfourList[index]['f064fid']);
            if (index > -1) {
                this.levelfourList.splice(index, 1);
            }
            let deleteObject = {};
            deleteObject['type'] = "payrollDeduction";
            deleteObject['id'] =this.deleteList;
            this.PayrolltaxService.deleteItems(deleteObject).subscribe(
                data => {
                }, error => {
                    console.log(error);
                });
           
            this.showIcons();
        }
    }
    addLevelfour() {

        console.log(this.levelfourData);
        // if (this.levelfourData['f064freferenceNumber'] == undefined) {
        //     alert("Please Enter Reference Number");
        //     return false;
        // }
        // if (this.levelfourData['f064fdescription'] == undefined) {
        //     alert("Please Enter Description");
        //     return false;
        // }

        // if (this.levelfourData['f064flimit'] == undefined) {
        //     alert("Please Enter Limit");
        //     return false;
        // }

        // if (this.levelfourData['f064fcombineLimit'] == undefined) {
        //     alert("Please Enter Combine Limit");
        //     return false;
        // }
        var confirmPop = confirm("Do you want to Add?");
        if (confirmPop == false) {
            return false;
        }
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        var dataofCurrentRow = this.levelfourData;
        this.levelfourList.push(dataofCurrentRow);
        console.log(this.levelfourData);
        this.levelfourData = {};
        this.showIcons();
    }
    onlyNumberKey(event) {
        //console.log(event);
        if (event.charCode == 46) {

        } else {
            return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
        }
    }
}
