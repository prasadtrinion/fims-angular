import { Injector } from '@angular/core';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { PrintPayslipService } from '../service/printpayslip.service';
import { DepartmentService } from '../../generalsetup/service/department.service';
import { StaffService } from '../../loan/service/staff.service';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';

@Component({
    selector: 'PrintPayslipComponent',
    templateUrl: 'printpayslip.component.html'
})

export class PrintPayslipComponent implements OnInit {
    kwapform: FormGroup;
    printpayslipList = [];
    printpayslipData = {};
    DeptList = [];
    staffList = [];
    dropdownSettings = {};
    ajaxCount: number;

    id: number;
    constructor(

        private PrintPayslipService: PrintPayslipService,
        private DepartmentService: DepartmentService,
        private StaffService: StaffService,
        private route: ActivatedRoute,
        private router: Router

    ) { }
    ngDoCheck() {
        const change = this.ajaxCount;
        //console.log(this.ajaxCount);
        if (this.ajaxCount == 0) {
            this.ajaxCount = 10;
        }
    }

    ngOnInit() {



        this.ajaxCount = 0;
        this.kwapform = new FormGroup({
            kwap: new FormControl('', Validators.required),
        });

        // department dropdown
        this.ajaxCount++;
        this.DepartmentService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.DeptList = data['result']['data'];
                console.log(this.DeptList);



            }, error => {
                //console.log(error);
            });
        // Staff dropdown
        // this.ajaxCount++;
        // this.StaffService.getItems().subscribe(
        //     data => {
        //         this.ajaxCount--;
        //         this.staffList = data['result']['data'];
        //     }, error => {
        //         console.log(error);
        //     });
    }

    getStaffBasedOnDepartment() {
        let deptId = this.printpayslipData['f043fdepartment'];
        console.log(deptId);
        this.PrintPayslipService.getstaff(deptId).subscribe(
            data => {
                this.staffList = data['result']['data'];

                this.dropdownSettings = {
                    singleSelection: false,
                    idField: 'f034fstaffId',
                    textField: 'f034fname',
                    selectAllText: 'Select All',
                    unSelectAllText: 'UnSelect All',
                    itemsShowLimit: 4,
                    allowSearchFilter: true
                };

            }
        );
    }

    // dateComparison(){

    //     let startDate = Date.parse(this.kwapData['f070fstartDate']);
    //     let endDate = Date.parse(this.kwapData['f070fendDate']);

    //     if(startDate>endDate){
    //         alert("Start Date cannot be greater than End Date !");
    //         this.kwapData['f070fendDate'] = "";
    //     }

    // }
    addPrintpayslip() {

        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.PrintPayslipService.updateprintpayslipItems(this.printpayslipData, this.id).subscribe(
                data => {
                    alert('KWAP has been Updated');
                    this.router.navigate(['payroll/printpayslip']);
                }, error => {
                    console.log(error);
                });


        } else {

            this.PrintPayslipService.insertprintpayslipItems(this.printpayslipData).subscribe(
                data => {
                    alert('KWAP has been saved');
                    this.router.navigate(['payroll/printpayslip']);

                }, error => {
                    console.log(error);
                });

        }
    }

}