import { Injector } from '@angular/core';
import { Component, OnInit, ViewChild } from '@angular/core';
import { StaffService } from '../../../loan/service/staff.service';
import { ActivatedRoute, Router } from '@angular/router';
import { StaffTaggingService } from "../../service/stafftagging.service";
import { FundService } from '../../../generalsetup/service/fund.service';
import { ActivitycodeService } from '../../../generalsetup/service/activitycode.service';
import { DepartmentService } from '../../../generalsetup/service/department.service';
import { AccountcodeService } from '../../../generalsetup/service/accountcode.service';
import { SocsocategoryService } from "../../service/socsocategory.service";
import { EpfcategoryService } from "../../service/epfcategory.service";
import { CountryService } from "../../../generalsetup/service/country.service";
import { StateService } from "../../../generalsetup/service/state.service";
import { BanksetupaccountService } from '../../../generalsetup/service/banksetupaccount.service';


@Component({
    selector: 'StaffTaggingFormComponent',
    templateUrl: 'stafftaggingform.component.html'
})

export class StaffTaggingFormComponent implements OnInit {

    stafftaggingList = [];
    stafftaggingData = {};
    loannameList = [];
    fundList = [];
    activitycodeList = [];
    accountcodeList = [];
    staffList = [];
    DeptList = [];
    socsocategoryList = [];
    epfList = [];
    countryList = [];
    stateList = [];
    banklist = [];
    ajaxCount: number;


    id: number;
    constructor(
        private StaffTaggingService: StaffTaggingService,
        private ActivitycodeService: ActivitycodeService,
        private DepartmentService: DepartmentService,
        private AccountcodeService: AccountcodeService,
        private StaffService: StaffService,
        private FundService: FundService,
        private SocsocategoryService: SocsocategoryService,
        private EpfcategoryService: EpfcategoryService,
        private StateService: StateService,
        private CountryService: CountryService,
        private BanksetupaccountService: BanksetupaccountService,
        private route: ActivatedRoute,
        private router: Router

    ) { }

    ngOnInit() {
        this.ajaxCount = 0;

        this.stafftaggingData['f043fstatus'] = 1;
        // this.stafftaggingData['f020floanName'];
        this.stafftaggingData['f043ftaxable'] = 1;


        // Staff dropdown
        this.ajaxCount++;
        this.StaffService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.staffList = data['result']['data'];
            }, error => {
                console.log(error);
            });

        //fund dropdown
        this.ajaxCount++;
        this.FundService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.fundList = data['result']['data'];
            }, error => {
                //console.log(error);
            });

        // activity dropdown
        this.ajaxCount++;
        this.ActivitycodeService.getItemsByLevel().subscribe(
            data => {
                //console.log(data);
                this.ajaxCount--;
                this.activitycodeList = data['result']['data'];
                for (var i = 0; i < this.activitycodeList.length; i++) {
                    this.activitycodeList[i]['f058fcompleteCode'] = this.activitycodeList[i]['f058fcompleteCode'];
                    this.activitycodeList[i]['f058fcompleteCode'].trim();
                }

                ////console.log(this.activitycodeList);
            }, error => {
                //console.log(error);
            });
        // account dropdown
        this.ajaxCount++;
        this.AccountcodeService.getItemsByLevel().subscribe(
            data => {
                this.ajaxCount--;
                this.accountcodeList = data['result']['data'];
            }, error => {
                //console.log(error);
            });

        // department dropdown
        this.ajaxCount++;
        this.DepartmentService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.DeptList = data['result']['data'];
            }, error => {
                //console.log(error);
            });

        // SOCSO dropdown
        this.ajaxCount++;
        this.SocsocategoryService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.socsocategoryList = data['result']['data'];
            }, error => {
                console.log(error);
            })

        // EPF Category dropdown
        this.ajaxCount++;
        this.EpfcategoryService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.epfList = data['result']['data'];
            }, error => {
                //console.log(error);
            });

        // Country  dropdown
        this.ajaxCount++;
        this.CountryService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.countryList = data['result']['data'];
            }, error => {
                //console.log(error);
            });

        // State  dropdown
        this.ajaxCount++;
        this.StateService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.stateList = data['result']['data'];
            }, error => {
                //console.log(error);
            });

        //Bank dropdown
        this.BanksetupaccountService.getItems().subscribe(
            data => {
                this.banklist = data['result']['data'];
            }, error => {
                console.log(error);
            });
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.StaffTaggingService.getItemsDetail(this.id).subscribe(
                data => {
                    this.stafftaggingData = data['result']['0'];
                    this.stafftaggingData['f043fstatus'] = parseInt(this.stafftaggingData['f043fstatus']);
                    console.log(this.stafftaggingData);
                }, error => {
                    console.log(error);
                });
        }
        console.log(this.id);
    }

    addProcessingFee() {
        console.log(this.stafftaggingData);
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.StaffTaggingService.updateStaffTaggingItems(this.stafftaggingData, this.id).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert('Loan Processing Fee Already Exist');
                    }
                    else {
                        alert("Staff Tagging has been Updated ");
                    }
                    this.router.navigate(['payroll/stafftagging']);
                }, error => {
                    console.log(error);
                });

        } else {
            this.StaffTaggingService.insertStaffTaggingItems(this.stafftaggingData).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert("No duplication of Loan Processing Fee");
                    }
                    else {
                        this.router.navigate(['payroll/stafftagging']);
                        alert("Staff Tagging has been Added ");

                    }
                    this.router.navigate(['payroll/stafftagging']);

                }, error => {
                    console.log(error);
                });


        }

    }
}