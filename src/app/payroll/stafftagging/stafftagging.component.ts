import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { StaffTaggingService } from "../service/stafftagging.service";


@Component({
    selector: 'StaffTaggingComponent',
    templateUrl: 'stafftagging.component.html'
})
export class StaffTaggingComponent implements OnInit {
    processingfeeList = [];
    processingfeeData = {};
    id: number;
    constructor(
        private StaffTaggingService: StaffTaggingService
    ) { }

    ngOnInit() {

        this.StaffTaggingService.getItems().subscribe(
            data => {
                this.processingfeeList = data['result']['data'];
            }, error => {
                console.log(error);
            });
    }
}