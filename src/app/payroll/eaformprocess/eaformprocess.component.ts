import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { EaformprocessService} from '../service/eaformprocess.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'EaformprocessComponent',
    templateUrl: 'eaformprocess.component.html'
})
export class EaformprocessComponent implements OnInit {
    eaformprocessList = [];
    eaformprocessData = {};
    id: number;
    constructor(
        private EaformprocessService: EaformprocessService,
        private spinner: NgxSpinnerService
   ) { }

    ngOnInit() {
        this.spinner.show();
        this.EaformprocessService.getItems().subscribe(
            data => {
                this.spinner.hide();
                this.eaformprocessList = data['result']['data'];
            }, error => {
                console.log(error);
            });
    }
    addDatemaintainance() {
        console.log(this.eaformprocessData);
        this.EaformprocessService.insertEaformprocessItems(this.eaformprocessData).subscribe(
            data => {
                this.eaformprocessList = data['todos'];
            }, error => {
                console.log(error);
            });

    }
}