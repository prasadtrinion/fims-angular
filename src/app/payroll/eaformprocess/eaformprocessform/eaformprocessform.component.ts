import { Injector } from '@angular/core';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { EaformprocessService } from '../../service/eaformprocess.service';
import { StaffService } from '../../../loan/service/staff.service';
import { CompanyService } from '../../../generalsetup/service/company.service';
import { FinancialyearService } from '../../../generalledger/service/financialyear.service'


@Component({
    selector: 'EaformprocessformComponent',
    templateUrl: 'eaformprocessform.component.html'
})

export class EaformprocessformComponent implements OnInit {
    eaformprocessform: FormGroup;
    eaformprocessList = [];
    eaformprocessData = {};
    staffList = [];
    companyList = [];
    fyearList=[];


    id: number;
    constructor(

        private EaformprocessService: EaformprocessService,
        private FinancialyearService:FinancialyearService,
        private route: ActivatedRoute,
        private router: Router,
        private CompanyService: CompanyService,
        private StaffService: StaffService

    ) { }

    ngOnInit() {

        this.eaformprocessData['f093fstatus'] = 1;
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        //staff dropdown
        this.StaffService.getItems().subscribe(
            data => {
                this.staffList = data['result']['data'];
            }, error => {
                console.log(error);
            });

        //dropdown
        this.CompanyService.getItems().subscribe(
            data => {
                this.companyList = data['result']['data'];
            }, error => {
                console.log(error);
            });
        //fyear dropdown
        this.FinancialyearService.getActiveItems().subscribe(
            data => {
                this.fyearList = data['result'];
            }, error => {
                console.log(error);
            });

        this.eaformprocessform = new FormGroup({
            eaformprocess: new FormControl('', Validators.required),
        });



        if (this.id > 0) {
            this.EaformprocessService.getItemsDetail(this.id).subscribe(
                data => {
                    this.eaformprocessData = data['result'][0];
                    this.eaformprocessData['f093fstatus'] = parseInt(this.eaformprocessData['f093fstatus']);
                    console.log(this.eaformprocessData);
                }, error => {
                    console.log(error);
                });
        }
    }

    addEaformprocess() {
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }

        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.EaformprocessService.updateEaformprocessItems(this.eaformprocessData, this.id).subscribe(
                data => {
                    alert('EA Form Process has been Updated Successfully');
                    this.router.navigate(['payroll/eaformprocess']);
                }, error => {
                    console.log(error);
                });


        } else {

            this.EaformprocessService.insertEaformprocessItems(this.eaformprocessData).subscribe(
                data => {
                    alert('EA Form Process has been Added Successfully');
                    this.router.navigate(['payroll/eaformprocess']);
                }, error => {
                    console.log(error);
                });

        }
    }

}