import { Injector } from '@angular/core';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { StaffService } from '../../../loan/service/staff.service';
import { ActivatedRoute, Router } from '@angular/router';
import { PayrollprocessService } from '../../service/payrollprocess.service';
import { CompanyService } from '../../../generalsetup/service/company.service';


@Component({
    selector: 'PayrollprocessFormComponent',
    templateUrl: 'payrollprocessform.component.html'
})

export class PayrollprocessFormComponent implements OnInit {
    payrollprocessform: FormGroup; 
    payrollprocessList = [];
    payrollprocessData = {};
    companyList = [];
    staffList=[];
    fmonth = [];
    staffIP = [];
    id: number;
    constructor(

        private PayrollprocessService: PayrollprocessService,
        private CompanyService: CompanyService,
        private route: ActivatedRoute,
        private StaffService: StaffService, 
        private router: Router

    ) { }

    ngOnInit() {

        let stafftypeobj = {};
        stafftypeobj['name'] = 'Process Type 1';
        stafftypeobj['id'] = 1
        this.staffIP.push(stafftypeobj);
        stafftypeobj = {};
        stafftypeobj['name'] = 'Process Type 2';
        stafftypeobj['id'] = 2
        this.staffIP.push(stafftypeobj);
        stafftypeobj = {};
        stafftypeobj['name'] = 'Process Type 3';
        stafftypeobj['id'] = 3 
        this.staffIP.push(stafftypeobj);

        let month = {};
        month['name'] = '01/2018';
        this.fmonth.push(month);
        month = {};
        month['name'] = '02/2018';
        this.fmonth.push(month);
        month = {};
        month['name'] = '03/2018';
        this.fmonth.push(month);
        month = {};
        month['name'] = '04/2018';
        this.fmonth.push(month);
        month = {};
        month['name'] = '05/2018';
        this.fmonth.push(month);
        month = {};
        month['name'] = '06/2018';
        this.fmonth.push(month);
        month = {};
        month['name'] = '07/2018';
        this.fmonth.push(month);
        month = {};
        month['name'] = '08/2018';
        this.fmonth.push(month); 
        month = {};
        month['name'] = '09/2018';
        this.fmonth.push(month);
        month = {};
        month['name'] = '10/2018';
        this.fmonth.push(month); 
        month = {};
        month['name'] = '11/2018';
        this.fmonth.push(month);
        month = {};
        month['name'] = '12/2018';
        this.fmonth.push(month);

        this.payrollprocessData['f044fstatus'] = 1;
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

          //staff dropdown
          this.StaffService.getItems().subscribe(
            data => {
                this.staffList = data['result']['data'];
            }, error => {
                console.log(error);
            });

            //dropdown
            this.CompanyService.getItems().subscribe(
                data => {
                    this.companyList = data['result']['data'];
                }, error => {
                    console.log(error);
                });

               
             this.payrollprocessform = new FormGroup({
                payrollprocess: new FormControl('', Validators.required),  
            });
          
           
            
            if (this.id > 0) {
                this.PayrollprocessService.getItemsDetail(this.id).subscribe(
                    data => {
                        this.payrollprocessData = data['result'][0];
                        this.payrollprocessData['f044fstatus'] = parseInt(this.payrollprocessData['f044fstatus']);
                    console.log(this.payrollprocessData);
                    }, error => {
                        console.log(error);
                    });
            }
            console.log(this.id);
     }

     processSalary(){
         console.log(this.payrollprocessData);
         let process = {};
         process['process_id'] = this.payrollprocessData['f044fstaffType'];
         process['month'] = this.payrollprocessData['f044fmonth'];
         this.PayrollprocessService.insertPayrollprocessItems(process).subscribe(
            data => {
                alert('Payroll Process has been Updated Successfully');
                this.router.navigate(['payroll/payrollprocess']);
            }, error => {
                console.log(error);
            });

     }

     moveSalary(){
        console.log(this.payrollprocessData);
        let process = {};
        process['month'] = this.payrollprocessData['f045fmonth'];
        process['delete'] = 'yes';
        this.PayrollprocessService.moveSalaryProcess(process).subscribe(
           data => {
               alert('Payroll Process has been Conrimed Successfully');
               this.router.navigate(['payroll/payrollprocess']);
           }, error => {
               console.log(error);
           });

    }
    addPayrollprocess() {
        
        var confirmPop = confirm("Do you want to Save?");
        if(confirmPop==false) 
        {
            return false;
        }
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.PayrollprocessService.insertPayrollprocessItems(this.payrollprocessData).subscribe(
                data => {
                    alert('Payroll Process has been Updated Successfully');
                    this.router.navigate(['payroll/payrollprocess']);
                }, error => {
                    console.log(error);
                });


        } else {

            this.PayrollprocessService.insertPayrollprocessItems(this.payrollprocessData).subscribe(
                data => {
                    alert('Payroll Process has been Added Successfully');
                    this.router.navigate(['payroll/payrollprocess']);

                }, error => {
                    console.log(error);
                });

        }
    }
}