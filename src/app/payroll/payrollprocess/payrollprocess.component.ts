import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { PayrollprocessService } from '../service/payrollprocess.service';

@Component({
    selector: 'PayrollprocessComponent',
    templateUrl: 'payrollprocess.component.html'
})
export class PayrollprocessComponent implements OnInit {
    payrollprocessList = [];
    payrollprocessData = {};
    id: number;
    constructor(
        private PayrollprocessService: PayrollprocessService
   ) { }

    ngOnInit() {

        this.PayrollprocessService.getItems().subscribe(
            data => {
                this.payrollprocessList = data['result']['data'];
            }, error => {
                console.log(error);
            });
    }
    
}