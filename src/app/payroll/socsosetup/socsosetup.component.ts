import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
// import { ItemcategoryService } from '../service/itemcategory.service'
import { SocsosetupService } from "../service/socsosetup.service";
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'SocsosetupComponent',
    templateUrl: 'socsosetup.component.html'
})
export class SocsosetupComponent implements OnInit {
    SocsoList = [];
    SocsoData = {};
    id: number;
    constructor(
        private SocsosetupService: SocsosetupService,
        private spinner: NgxSpinnerService
    ) { }

    ngOnInit() {
        this.spinner.show();
         this.SocsosetupService.getItems().subscribe(
             data => {
                 this.spinner.hide();
                 this.SocsoList = data['result']['data'];
             }, error => {
                 console.log(error);
             });
    }
    
}