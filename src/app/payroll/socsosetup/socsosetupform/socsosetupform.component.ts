import { Injector } from '@angular/core';
import { Component, OnInit, ViewChild } from '@angular/core';

import { ActivatedRoute, Router } from '@angular/router';
// import { ItemcategoryService } from ''
import { CompanyService } from '../../../generalsetup/service/company.service';
import { SocsosetupService } from "../../service/socsosetup.service";
import { SocsocategoryService } from "../../service/socsocategory.service";


@Component({
    selector: 'SocsosetupformComponent',
    templateUrl: 'socsosetupform.component.html'
})

export class SocsosetupformComponent implements OnInit {

    SocsoList = [];
    SocsoData = {};
    fstafftype = [];
    socsocategoryList = [];
    ajaxCount: number;
    socsocategoryListNew = [];
    //companyList = [];


    id: number;
    constructor(

        private route: ActivatedRoute,
        private router: Router,
        //private CompanyService:CompanyService,
        private SocsosetupService: SocsosetupService,
        private SocsocategoryService: SocsocategoryService


    ) { }

    ngDoCheck() {
        const change = this.ajaxCount;
        console.log(this.ajaxCount);
        if (this.ajaxCount == 0) {
            this.ajaxCount = 10;
        }
    }

    ngOnInit() {

        // SOCSO dropdown
        this.ajaxCount++;
        this.SocsocategoryService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.socsocategoryListNew = data['result']['data'];
                let socsoArray = [];
                for (var i = 0; i < this.socsocategoryListNew.length; i++) {
                    if (this.socsocategoryListNew[i]['f091fstatus'] == '1') {
                        socsoArray.push(this.socsocategoryListNew[i]);
                    }
                }
                this.socsocategoryList = socsoArray;

            }, error => {
                console.log(error);
            });

        let stafftypeobj = {};
        stafftypeobj['name'] = 'Resident';
        this.fstafftype.push(stafftypeobj);
        stafftypeobj = {};
        stafftypeobj['name'] = 'Non-Resident';
        this.fstafftype.push(stafftypeobj);

        this.SocsoData['f091fstatus'] = 1;
        // this.SocsoData['f091ftype'] ='';



        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.SocsosetupService.getItemsDetail(this.id).subscribe(
                data => {
                    this.SocsoData = data['result'][0];
                    this.SocsoData['f091fstatus'] = parseInt(this.SocsoData['f091fstatus']);
                    console.log(this.SocsoData);
                }, error => {
                    console.log(error);
                });
        }


        console.log(this.id);
    }
    ConvertToFloat(val) {
        return parseFloat(val);
    }

    addSocso() {
        console.log(this.SocsoData);
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        if (parseInt(this.SocsoData['f091fstartAmount']) > parseInt(this.SocsoData['f091fendAmount'])) {
            alert("Start Amount cannot be greater than End Amount");
            return false;
        }
        if (parseInt(this.SocsoData['f091femployerContribution']) < parseInt(this.SocsoData['f091femployerDeduction'])) {
            alert("Employer Contribution cannot be less than Employer Deduction ");
            return false;
        }
        this.SocsoData['f091fstartAmount'] = this.ConvertToFloat(this.SocsoData['f091fstartAmount']).toFixed(2);
        this.SocsoData['f091fendAmount'] = this.ConvertToFloat(this.SocsoData['f091fendAmount']).toFixed(2);
        this.SocsoData['f091femployerContribution'] = this.ConvertToFloat(this.SocsoData['f091femployerContribution']).toFixed(2);
        this.SocsoData['f091femployerDeduction'] = this.ConvertToFloat(this.SocsoData['f091femployerDeduction']).toFixed(2);

        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.SocsosetupService.updateSoscoItems(this.SocsoData, this.id).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert('SOCSO Code Already Exist');
                    }
                    else {
                        this.router.navigate(['payroll/socsosetup']);
                        alert('SOCSO Setup has been Updated Successfully');
                    }
                }, error => {
                    alert('SOCSO Code Already Exist');
                    console.log(error);
                });


        } else {
            this.SocsosetupService.insertSoscoItems(this.SocsoData).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert('SOCSO Code Already Exist');
                    }
                    else {
                        console.log(data);
                        this.router.navigate(['payroll/socsosetup']);
                        alert('SOCSO Setup has been Added Successfully');
                    }

                }, error => {
                    console.log(error);
                });

        }

    }
    onlyNumberKey(event) {
        //console.log(event);
        if (event.charCode == 46) {

        } else {
            return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
        }
    }
}