import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DatemaintainanceService } from '../service/datemaintainance.service';
import { NgxSpinnerService } from 'ngx-spinner';
@Component({
    selector: 'DatemaintainanceComponent',
    templateUrl: 'datemaintainance.component.html'
})
export class DatemaintainanceComponent implements OnInit {
    datemaintainanceList = [];
    datemaintainanceData = {};
    id: number;
    constructor(
         private DatemaintainanceService: DatemaintainanceService,
         private spinner: NgxSpinnerService
    ) { }

    ngOnInit() {
        this.spinner.show();
        this.DatemaintainanceService.getItems().subscribe(
            data => {
                this.spinner.hide();
                this.datemaintainanceList = data['result']['data'];
            }, error => {
                console.log(error);
            });
    }
    addDatemaintainance() {
        console.log(this.datemaintainanceData);
        this.DatemaintainanceService.insertdatemaintainanceItems(this.datemaintainanceData).subscribe(
            data => {
                this.datemaintainanceList = data['todos'];
            }, error => {
                console.log(error);
            });

    }
}