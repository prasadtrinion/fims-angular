import { Injector } from '@angular/core';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { StaffService } from '../../../loan/service/staff.service';
import { DatemaintainanceService } from '../../service/datemaintainance.service';
import { ProcesstypeService } from "../../service/processtype.service";

@Component({
    selector: 'DatemaintainanceFormComponent',
    templateUrl: 'datemaintainanceform.component.html'
})
export class DatemaintainanceFormComponent implements OnInit {
    datemaintainanceform: FormGroup;
    datemaintainanceList = [];
    datemaintainanceData = {};
    staffList = [];
    fstafftype = [];
    fmonth = [];
    processtype=[];

    id: number;
    constructor(
        private DatemaintainanceService: DatemaintainanceService,
        private ProcesstypeService:ProcesstypeService,
        private route: ActivatedRoute,
        private StaffService: StaffService,
        private router: Router

    ) { }
    ngOnInit() {
        let stafftypeobj = {};
        stafftypeobj['name'] = 'Resident';
        this.fstafftype.push(stafftypeobj);
        stafftypeobj = {};
        stafftypeobj['name'] = 'Non-Resident';
        this.fstafftype.push(stafftypeobj);
        var year = (new Date()).getFullYear().toString().substring(2);

       // var year = new Date('y');
        let month = {};
        month['name'] = 'Jan '+year;
        this.fmonth.push(month);
        month = {};
        month['name'] = 'Feb '+year;
        this.fmonth.push(month);
        month = {};
        month['name'] = 'Mar '+year;
        this.fmonth.push(month);
        month = {};
        month['name'] = 'Apr '+year;
        this.fmonth.push(month);
        month = {};
        month['name'] = 'May '+year;
        this.fmonth.push(month);
        month = {};
        month['name'] = 'Jun '+year;
        this.fmonth.push(month);
        month = {};
        month['name'] = 'Jul '+year;
        this.fmonth.push(month);
        month = {};
        month['name'] = 'Aug '+year;
        this.fmonth.push(month);
        month = {};
        month['name'] = 'Sep '+year;
        this.fmonth.push(month);
        month = {};
        month['name'] = 'Oct '+year;
        this.fmonth.push(month);
        month = {};
        month['name'] = 'Nov '+year;
        this.fmonth.push(month);
        month = {};
        month['name'] = 'Dec '+year;
        this.fmonth.push(month);

        this.datemaintainanceData['f070fstatus'] = 1;
        this.datemaintainanceData['f070fstafftype'] = '';
        //process type dropdown
        this.ProcesstypeService.getItems().subscribe(
            data => {
                this.processtype = data['result']['data'];
            }, error => {
                console.log(error);
            });

        //staff dropdown
        this.StaffService.getItems().subscribe(
            data => {
                this.staffList = data['result']['data'];
            }, error => {
                console.log(error);
            });

        //staff dropdown
        this.StaffService.getItems().subscribe(
            data => {
                this.staffList = data['result']['data'];
            }, error => {
                console.log(error);
            });

        this.datemaintainanceform = new FormGroup({
            datemaintainance: new FormControl('', Validators.required),
        });
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        if (this.id > 0) {
            this.DatemaintainanceService.getItemsDetail(this.id).subscribe(
                data => {
                    this.datemaintainanceData = data['result'][0];
                    this.datemaintainanceData['f070fstatus'] = parseInt(this.datemaintainanceData['f070fstatus']);
                    this.datemaintainanceData['f070fprocessType'] = parseInt(this.datemaintainanceData['f070fprocessType']);
                    console.log(this.datemaintainanceData);
                }, error => {
                    console.log(error);
                });
        }
        console.log(this.id);
    }
    dateComparison() {

        let startDate = Date.parse(this.datemaintainanceData['f070fstartDate']);
        let endDate = Date.parse(this.datemaintainanceData['f070fendDate']);
        if (startDate > endDate) {
            alert("Start Date cannot be greater than End Date !");
            this.datemaintainanceData['f070fendDate'] = "";
        }

    }
    addDatemaintainance() {

        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.DatemaintainanceService.updatedatemaintainanceItems(this.datemaintainanceData, this.id).subscribe(
                data => {
                    alert('Date Maintenance has been Updated Successfully');
                    this.router.navigate(['payroll/datemaintainance']);
                }, error => {
                    console.log(error);
                });


        } else {

            this.DatemaintainanceService.insertdatemaintainanceItems(this.datemaintainanceData).subscribe(
                data => {
                    alert('Date Maintenance has been Added Successfully');
                    this.router.navigate(['payroll/datemaintainance']);

                }, error => {
                    console.log(error);
                });

        }
    }

}