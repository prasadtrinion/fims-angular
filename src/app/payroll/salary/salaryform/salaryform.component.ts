import { Injector } from '@angular/core';
import { Component, OnInit, ViewChild } from '@angular/core';

import { ActivatedRoute, Router } from '@angular/router';
import { MonthlydeductionService } from '../../service/monthlydeduction.service'
import { SalaryService } from "../../service/salary.service";
import { FundService } from '../../../generalsetup/service/fund.service';
import { StaffService } from "../../../loan/service/staff.service";
import { AccountcodeService } from '../../../generalsetup/service/accountcode.service'
import { ActivitycodeService } from '../../../generalsetup/service/activitycode.service';
import { DepartmentService } from '../../../generalsetup/service/department.service';
import { AlertService } from './../../../_services/alert.service';
import { SupplierregistrationService } from '../../../procurement/service/supplierregistration.service';
import { DefinationmsService } from '../../../studentfinance/service/definationms.service';


@Component({
    selector: 'SalaryFormComponent',
    templateUrl: 'salaryform.component.html'
})

export class SalaryFormComponent implements OnInit {
    salaryDataHeader = {};
    ajaxCount: number;
    salaryList = [];
    salaryData = {};
    fundList = [];
    staffList = [];
    DeptList = [];
    accountcodeList = [];
    activitycodeList = [];
    ftypecomponent = [];
    modeList = [];
    frequancyList = [];
    salarytypeList = [];
    supplierList = [];
    id: number;
    stafftypeList = [];
    deleteList = [];
    viewDisabled: boolean;
    editDisabled: boolean;

    showLastRow: boolean;
    showLastRowMinus: boolean;
    showLastRowPlus: boolean;
    listshowLastRowMinus: boolean;
    listshowLastRowPlus: boolean;

    constructor(
        private SalaryService: SalaryService,
        private MonthlydeductionService: MonthlydeductionService,
        private FundService: FundService,
        private StaffService: StaffService,
        private AccountcodeService: AccountcodeService,
        private ActivitycodeService: ActivitycodeService,
        private DepartmentService: DepartmentService,
        private AlertService: AlertService,
        private SupplierregistrationService: SupplierregistrationService,
        private route: ActivatedRoute,
        private router: Router,
        private DefinationmsService: DefinationmsService,

    ) { }
    
    ngDoCheck() {
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        // console.log(this.ajaxCount);
        if (this.ajaxCount == 0 && this.salaryList.length > 0) {
            this.ajaxCount = 20;
            this.showIcons();

        }
        if (this.ajaxCount == 0) {
            this.editFunction();
            this.ajaxCount = 10;
        }
    }
    deletemodule() {

        this.showLastRow = true;
        this.showIcons();
    }

    showIcons() {
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));


        console.log(this.salaryList.length);
        console.log("Show last row" + this.showLastRow);
        console.log("showLastRowMinus" + this.showLastRowMinus);
        console.log("showLastRowPlus" + this.showLastRowPlus);
        console.log("listshowLastRowMinus" + this.listshowLastRowMinus);
        console.log("listshowLastRowPlus" + this.listshowLastRowPlus);

        if (this.salaryList.length > 0) {
            this.showLastRowPlus = true;
            this.showLastRowMinus = true;
            this.listshowLastRowMinus = true;
        }

        if (this.salaryList.length > 1) {
            if (this.showLastRow == false) {
                this.listshowLastRowPlus = false;
                this.listshowLastRowMinus = true;
                this.showLastRowPlus = true;

            } else {
                this.listshowLastRowPlus = true;
                this.listshowLastRowMinus = false;

            }
        }

        if (this.salaryList.length == 1 && this.showLastRow == false) {
            this.listshowLastRowMinus = true;
            this.listshowLastRowPlus = false;

        }
        if (this.salaryList.length == 1 && this.showLastRow == true) {
            this.listshowLastRowMinus = false;
            this.listshowLastRowPlus = true;

        }
        if (this.salaryList.length == 0 && this.showLastRow == false) {
            this.showLastRowMinus = false;
            this.showLastRowPlus = true;
        }
        if (this.salaryList.length == 0 && this.showLastRow == true) {
            this.showLastRowMinus = true;
            this.showLastRowPlus = false;
        }
        // if (this.salaryList.length > 0) {
        //     if (this.viewDisabled == true || this.salaryList[0]['f063fapproved1Status'] == 2 || this.salaryList[0]['f063fapproved2Status'] == 3 || this.salaryList[0]['f063fapproved3Status'] == 4) {
        //         this.listshowLastRowPlus = false;
        //     }
        // }
        // else {
        //     if (this.viewDisabled == true) {
        //         this.listshowLastRowPlus = false;
        //     }
        // }
        console.log(this.salaryList);
        console.log("Show last row" + this.showLastRow);
        console.log("showLastRowMinus" + this.showLastRowMinus);
        console.log("showLastRowPlus" + this.showLastRowPlus);
        console.log("listshowLastRowMinus" + this.listshowLastRowMinus);
        console.log("listshowLastRowPlus" + this.listshowLastRowPlus);
        console.log("list view disabled" + this.viewDisabled);

    }

    showNewRow() {
        var confirmPop = confirm("Do you want to Add?");
        if (confirmPop == false) {
            return false;
        }
        this.showLastRow = false;
        this.showIcons();
    }


    editFunction() {
        this.salaryData = {};

        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.SalaryService.getItemsDetail(this.id).subscribe(
                data => {
                    this.salaryDataHeader = data['result'][0];
                    // this.salaryDataHeader['f081fcode'] = data['result'][0]['f081fcode'];
                    // this.salaryDataHeader['f081fname'] = data['result'][0]['f081fname'];
                    // this.salaryDataHeader['f081fshortName'] = data['result'][0]['f081fshortName'];
                    // this.salaryDataHeader['f081ftypeOfComponent'] = data['result'][0]['f081ftypeOfComponent'];
                    // this.salaryDataHeader['f081ffrequency'] = data['result'][0]['f081ffrequency'];
                    // this.salaryDataHeader['f081fsalaryType'] = data['result'][0]['f081fsalaryType'];
                    // this.salaryDataHeader['f081faccountsCodeDeduction'] = data['result'][0]['f081faccountsCodeDeduction'];
                    // this.salaryDataHeader['f081faccountsCodeCr'] = data['result'][0]['f081faccountsCodeCr'];
                    this.salaryDataHeader['f081fvendorCode'] = parseInt(data['result'][0]['f081fvendorCode']);
                    // this.salaryDataHeader['f081fepfDeductable'] = data['result'][0]['f081fepfDeductable'];
                    // this.salaryDataHeader['f081fsocsoDeductable'] = data['result'][0]['f081fsocsoDeductable'];
                    // this.salaryDataHeader['f081ftaxDeductable'] = data['result'][0]['f081ftaxDeductable'];
                    // this.salaryDataHeader['f081feisDeductable'] = data['result'][0]['f081feisDeductable'];
                    if (data['result'][0]['f081fepfDeductable'] == 1) {
                        this.salaryDataHeader['f081fepfDeductable'] = true;
                    } else {
                        this.salaryDataHeader['f081fepfDeductable'] = false;

                    }
                    if (data['result'][0]['f081fsocsoDeductable'] == 1) {
                        this.salaryDataHeader['f081fsocsoDeductable'] = true;
                    } else {
                        this.salaryDataHeader['f081fsocsoDeductable'] = false;

                    }
                    if (data['result'][0]['f081ftaxDeductable'] == 1) {
                        this.salaryDataHeader['f081ftaxDeductable'] = true;
                    } else {
                        this.salaryDataHeader['f081ftaxDeductable'] = false;

                    }
                    if (data['result'][0]['f081feisDeductable'] == 1) {
                        this.salaryDataHeader['f081feisDeductable'] = true;
                    } else {
                        this.salaryDataHeader['f081feisDeductable'] = false;

                    }
                    this.salaryList = data['result'];


                    console.log(this.salaryDataHeader);
                    this.showIcons();


                }, error => {
                    console.log(error);
                });
        }
    }

    

    ngOnInit() {
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));


        if (this.id < 1) {
            this.showLastRow = false;
            this.showLastRowMinus = false;
            this.showLastRowPlus = true;
        } else {
            this.showLastRow = true;

        }
        this.viewDisabled = false;
        this.editDisabled = false;

        this.showIcons();

        let typecomponentobj = {};
        typecomponentobj['id'] = 1;
        typecomponentobj['name'] = 'Deduction';
        this.ftypecomponent.push(typecomponentobj);
        typecomponentobj = {};
        typecomponentobj['id'] = 2;
        typecomponentobj['name'] = 'Earnings';
        this.ftypecomponent.push(typecomponentobj);

        let mode = {};
        mode['id'] = 1;
        mode['name'] = 'Fixed Amount';
        this.modeList.push(mode);
        mode = {};
        mode['id'] = 2;
        mode['name'] = 'Percentage';
        this.modeList.push(mode);

        let frequancy = {};
        frequancy['id'] = 1;
        frequancy['name'] = 'Fixed Amount';
        this.frequancyList.push(frequancy);
        frequancy = {};
        frequancy['id'] = 2;
        frequancy['name'] = 'Period';
        this.frequancyList.push(frequancy);
        frequancy = {};
        frequancy['id'] = 3;
        frequancy['name'] = 'One Time Per Year';
        this.frequancyList.push(frequancy);

        let salarytype = {};
        salarytype['id'] = 1;
        salarytype['name'] = 'Additional Earning';
        this.salarytypeList.push(salarytype);
        salarytype = {};
        salarytype['id'] = 2;
        salarytype['name'] = 'Normal Earning';
        this.salarytypeList.push(salarytype);

        // salarytype = {};
        // salarytype['id'] = 3;
        // salarytype['name'] = 'Deduction';
        // this.salarytypeList.push(salarytype);

        this.salaryData['f081fstatus'] = 1;

        this.ajaxCount = 0;
        //Fund dropdown
        this.ajaxCount++;
        this.FundService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.fundList = data['result']['data'];
            }
        );
        // staff dropdown
        this.ajaxCount++;
        this.StaffService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.staffList = data['result']['data'];
            }, error => {
                console.log(error);
            });
        //Supplierregistration dropdown
        this.ajaxCount++;
        this.SupplierregistrationService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                // console.log(this.ajaxCount);

                this.supplierList = data['result']['data'];
                console.log(this.supplierList);

            }
        );
        //Department dropdown
        this.ajaxCount++;
        this.DepartmentService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.DeptList = data['result']['data'];
            }
        );

        this.DefinationmsService.getStafftypeItems('StaffType').subscribe(
            data => {
                this.stafftypeList = data['result'];
            }, error => {
                console.log(error);
            });

        //Activity dropdown
        this.ajaxCount++;
        this.ActivitycodeService.getItemsByLevel().subscribe(
            data => {
                this.ajaxCount--;
                this.activitycodeList = data['result']['data'];
            }
        );


        //Account dropdown
        this.ajaxCount++;
        this.AccountcodeService.getItemsByLevel().subscribe(
            data => {
                this.ajaxCount--;
                this.accountcodeList = data['result']['data'];
            }
        );
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.SalaryService.getItemsDetail(this.id).subscribe(
                data => {
                    this.salaryData = data['result'][0];
                }, error => {
                    console.log(error);
                });
        }
    }
    saveSalary() {
        
        if (this.salaryDataHeader['f081fcode'] == undefined) {
            alert("Enter the Payroll Code");
            return false;
        }
        if (this.salaryDataHeader['f081fname'] == undefined) {
            alert("Enter the Payroll Description");
            return false;
        }
        if (this.salaryDataHeader['f081fshortName'] == undefined) {
            alert("Enter the Payroll Short Description");
            return false;
        }

        if (this.salaryDataHeader['f081ftypeOfComponent'] == undefined) {
            alert("Select Type Of Component");
            return false;
        }

        if (this.salaryDataHeader['f081fmode'] == undefined) {
            alert("Select Mode");
            return false;
        }

        if (this.salaryDataHeader['f081ffrequency'] == undefined) {
            alert("Select Frequency");
            return false;
        }

        if (this.salaryDataHeader['f081fsalaryType'] == undefined) {
            alert("Select Salary Type");
            return false;
        }

        if (this.salaryDataHeader['f081faccountsCodeDeduction'] == undefined) {
            alert("Select Account Code Debit");
            return false;
        }

        if (this.salaryDataHeader['f081faccountsCodeCr'] == undefined) {
            alert("Select Account Code Credit");
            return false;
        }

        if (this.salaryDataHeader['f081fvendorCode'] == undefined) {
            alert("Select Vendor Code ");
            return false;
        }
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        if (this.showLastRow == false) {
            var addactivities = this.addSalary();
            if (addactivities == false) {
                return false;
            }

        }
        let salaryObject = {};
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        console.log(this.id);
        if (this.id > 0) {
            salaryObject['f081fid'] = this.id;
        }
        // detail entry code validation
        // if (this.cashadvanceappList.length > 0) {

        // } else {
        //     alert("Please add one details");
        //     return false;
        // }
        salaryObject['f081fcode'] = this.salaryDataHeader['f081fcode'];
        salaryObject['f081fname'] = this.salaryDataHeader['f081fname'];
        salaryObject['f081fshortName'] = this.salaryDataHeader['f081fshortName'];
        salaryObject['f081ftypeOfComponent'] = this.salaryDataHeader['f081ftypeOfComponent'];
        salaryObject['f081fmode'] = this.salaryDataHeader['f081fmode'];
        salaryObject['f081ffrequency'] = this.salaryDataHeader['f081ffrequency'];
        salaryObject['f081fsalaryType'] = this.salaryDataHeader['f081fsalaryType'];
        salaryObject['f081faccountsCodeDeduction'] = this.salaryDataHeader['f081faccountsCodeDeduction'];
        salaryObject['f081faccountsCodeCr'] = this.salaryDataHeader['f081faccountsCodeCr'];
        // salaryObject['f081fsocsoDeductable'] = this.salaryDataHeader['f081fsocsoDeductable'];
        // salaryObject['f081ftaxDeductable'] = this.salaryDataHeader['f081ftaxDeductable'];
        // salaryObject['f081feisDeductable'] = this.salaryDataHeader['f081feisDeductable'];
        salaryObject['f081fvendorCode'] = this.salaryDataHeader['f081fvendorCode'];



        if (this.salaryDataHeader['f081fepfDeductable'] == true) {
            salaryObject['f081fepfDeductable'] = 1;
        } else {
            salaryObject['f081fepfDeductable'] = 0;

        }
        if (this.salaryDataHeader['f081fsocsoDeductable'] == true) {
            salaryObject['f081fsocsoDeductable'] = 1;
        } else {
            salaryObject['f081fsocsoDeductable'] = 0;

        }
        if (this.salaryDataHeader['f081ftaxDeductable'] == true) {
            salaryObject['f081ftaxDeductable'] = 1;
        } else {
            salaryObject['f081ftaxDeductable'] = 0;

        }
        if (this.salaryDataHeader['f081feisDeductable'] == true) {
            salaryObject['f081feisDeductable'] = 1;
        } else {
            salaryObject['f081feisDeductable'] = 0;

        }

        // console.log(this.salaryDataHeader['f081fepfDeductable']);
        // salaryObject['f081fepfDeductable'] = this.salaryDataHeader['f081fepfDeductable'];

        salaryObject['f081fstaffStatus'] = 1;
        console.log(this.salaryList);
        salaryObject['salary-details'] = this.salaryList;


        if (this.id > 0) {
            this.SalaryService.updateSalaryItems(salaryObject, this.id).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert('Payroll Code Already Exist');
                    }
                    else {
                        alert("Salary has been Updated Sucessfully")
                        this.router.navigate(['payroll/salary']);
                    }
                }, error => {
                    console.log(error);
                    alert('Payroll Code Already Exist');
                });
        } else {
            this.SalaryService.insertSalaryItems(salaryObject).subscribe(
                data => {

                    if (data['status'] == 409) {
                        alert('Payroll Code Already Exist');

                    }
                    else {
                        // this.AlertService.success("Salary has been Added Sucessfully  !!")
                        alert('Salary has been Added Sucessfully');
                        this.router.navigate(['payroll/salary']);
                    }
                }, error => {
                    console.log(error);
                });
        }
        this.showIcons();

    }

    addSalary() {
        console.log(this.salaryData);


        if (this.salaryData['f081fstaffType'] == undefined) {
            alert(" Select Staff Type");
            return false;
        }
        if (this.salaryData['f081faccountCodeDebit'] == undefined) {
            alert(" Select Account Code Debit");
            return false;
        }
        if (this.salaryData['f081faccountCodeCredit'] == undefined) {
            alert(" Select Account Code Credit");
            return false;
        }
        var confirmPop = confirm("Do you want to Add?");
        if (confirmPop == false) {
            return false;
        }
        // if (this.salaryData['f052ffund'] == undefined) {
        //     alert("Please Select Fund");
        //     return false;
        // }
        // if (this.salaryData['f052factivity'] == undefined) {
        //     alert("Please Select Activity");
        //     return false;
        // }
        // if (this.salaryData['f052fdepartment'] == undefined) {
        //     alert("Please Select Cost Center");
        //     return false;
        // }
        // if (this.salaryData['f052faccount'] == undefined) {
        //     alert("Please Select Account Code");
        //     return false;
        // }
        // if (this.salaryData['f052fsoCode'] == undefined) {
        //     alert("Please Enter So Code");
        //     return false;
        // }
        // if (this.salaryData['f052famount'] == undefined) {
        //     alert("Please Enter Amount");
        //     return false;
        // }
        // if (this.salaryData['f052fquantity'] == undefined) {
        //     alert("Please Enter Quantity");
        //     return false;
        // }

        var dataofCurrentRow = this.salaryData;
        this.salaryData = {};
        this.salaryList.push(dataofCurrentRow);
        console.log(this.salaryList);
        this.showIcons();

    }
    deleteEvent(object) {
        var cnf = confirm("Do you really want to delete");
        if (cnf == true) {
            console.log(this.salaryList);
            var index = this.salaryList.indexOf(object);
            this.deleteList.push(this.salaryList[index]['f081fidDetails']);
            let deleteObject = {};
            deleteObject['id'] = this.deleteList
            this.SalaryService.deleteDetail(deleteObject).subscribe(
                data => {
                }, error => {
                    console.log(error);
                })
            if (index > -1) {
                this.salaryList.splice(index, 1);
            }
        }
    }

}