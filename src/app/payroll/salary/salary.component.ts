import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SalaryService } from '../service/salary.service'
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'SalaryComponent',
    templateUrl: 'salary.component.html'
})
export class SalaryComponent implements OnInit {
    salaryList = [];
    itemcategoryData = {};
    id: number;
    constructor(
        private SalaryService: SalaryService,
        private spinner: NgxSpinnerService
) { }

    ngOnInit() {
        this.spinner.show();
        this.SalaryService.getItems().subscribe(
            data => {
                this.spinner.hide();
                this.salaryList = data['data']['data'];
            }, error => {
                console.log(error);
            });
    }
  
}