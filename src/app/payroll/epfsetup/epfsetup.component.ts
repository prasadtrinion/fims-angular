import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { EpfsetupService } from "../service/epfsetup.service";
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'EpfsetupComponent',
    templateUrl: 'epfsetup.component.html'
})
export class EpfsetupComponent implements OnInit {
    EpfsetupList = [];
    EpfsetupData = {};
    id: number;
    constructor(
        private EpfsetupService: EpfsetupService,
        private spinner: NgxSpinnerService
    ) { }

    ngOnInit() {
        this.spinner.show();
        this.EpfsetupService.getItems().subscribe(
            data => {
                this.spinner.hide();
                this.EpfsetupList = data['result']['data'];
                // for(var i=0;i<this.EpfsetupList.length;i++){
                //     this.EpfsetupList[i]['f030femployerContribution'] = parseFloat(this.EpfsetupList[i]['f030femployerContribution']);
                //     this.EpfsetupList[i]['f030femployerDeduction'] = parseFloat(this.EpfsetupList[i]['f030femployerDeduction']);
                //     this.EpfsetupList[i]['f030fendAmount'] = parseFloat(this.EpfsetupList[i]['f030fendAmount']);
                //     this.EpfsetupList[i]['f030fstartAmount'] = parseFloat(this.EpfsetupList[i]['f030fstartAmount']);
                // }
            }, error => {
                console.log(error);
            });
    }
    addEpfsetup() {
        console.log(this.EpfsetupData);
        this.EpfsetupService.insertEpfsetupItems(this.EpfsetupData).subscribe(
            data => {
                this.EpfsetupList = data['todos'];
            }, error => {
                console.log(error);
            });

    }
}
    
