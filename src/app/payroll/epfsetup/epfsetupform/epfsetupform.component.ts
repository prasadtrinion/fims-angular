import { Injector } from '@angular/core';
import { Component, OnInit, ViewChild } from '@angular/core';

import { ActivatedRoute, Router } from '@angular/router';
import { EpfsetupService } from "../../service/epfsetup.service";
import { SocsocategoryService } from "../../service/socsocategory.service";
import { EpfcategoryService } from "../../service/epfcategory.service";



@Component({
    selector: 'EpfsetupformComponent',
    templateUrl: 'epfsetupform.component.html'
})

export class EpfsetupformComponent implements OnInit {

    EpfsetupList = [];
    EpfsetupData = {};
    ftypeList = [];
    socsocategoryList = [];
    id: number;
    epfList = [];
    epfListNew = [];
    ajaxCount: number;
    constructor(


        private route: ActivatedRoute,
        private router: Router,
        private EpfsetupService: EpfsetupService,
        private SocsocategoryService: SocsocategoryService,
        private EpfcategoryService: EpfcategoryService

    ) { }

    ngDoCheck() {
        const change = this.ajaxCount;
        console.log(this.ajaxCount);
        if (this.ajaxCount == 0) {
            this.ajaxCount = 10;
        }
    }

    ngOnInit() {
        // SOCSO dropdown
        this.ajaxCount++;
        this.EpfcategoryService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.epfListNew = data['result']['data'];
                let epfArray = [];
                for (var i = 0; i < this.epfListNew.length; i++) {
                    if (this.epfListNew[i]['f030fstatus'] == '1') {
                        epfArray.push(this.epfListNew[i]);
                    }
                }
                this.epfList = epfArray;
            }, error => {
                console.log(error);
            });

        let typecomponentobj = {};
        typecomponentobj['id'] = 1;
        typecomponentobj['name'] = 'Resident';
        this.ftypeList.push(typecomponentobj);
        typecomponentobj = {};
        typecomponentobj['id'] = 2;
        typecomponentobj['name'] = 'Non-Resident';
        this.ftypeList.push(typecomponentobj);

        this.EpfsetupData['f030fstatus'] = 1;
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        if (this.id > 0) {
            this.EpfsetupService.getItemsDetail(this.id).subscribe(
                data => {
                    this.EpfsetupData = data['result'][0];
                    this.EpfsetupData['f030fstatus'] = parseInt(this.EpfsetupData['f030fstatus']);
                    console.log(this.EpfsetupData);
                }, error => {
                    console.log(error);
                });
        }
    }
    ConvertToFloat(val) {
        return parseFloat(val);
    }
    addEpfsetup() {
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        if (parseInt(this.EpfsetupData['f030fstartAmount']) > parseInt(this.EpfsetupData['f030fendAmount'])) {
            alert("Start Amount cannot be greater than End Amount");
            return false;
        }
        if (parseInt(this.EpfsetupData['f030femployerContribution']) < parseInt(this.EpfsetupData['f030femployerDeduction'])) {
            alert("Employer Contribution cannot be Less than Employer Deduction");
            return false;
        }
        this.EpfsetupData['f030fstartAmount'] = this.ConvertToFloat(this.EpfsetupData['f030fstartAmount']).toFixed(2);
        this.EpfsetupData['f030fendAmount'] = this.ConvertToFloat(this.EpfsetupData['f030fendAmount']).toFixed(2);
        this.EpfsetupData['f030femployerContribution'] = this.ConvertToFloat(this.EpfsetupData['f030femployerContribution']).toFixed(2);
        this.EpfsetupData['f030femployerDeduction'] = this.ConvertToFloat(this.EpfsetupData['f030femployerDeduction']).toFixed(2);

        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.EpfsetupService.updateEpfsetupItems(this.EpfsetupData, this.id).subscribe(
                data => {
                    alert('EPF Setup has been Updated Successfully');
                    this.router.navigate(['payroll/epfsetup']);
                }, error => {
                    console.log(error);
                });


        } else {

            this.EpfsetupService.insertEpfsetupItems(this.EpfsetupData).subscribe(
                data => {
                    console.log(data);
                    alert('EPF Setup has been Added Successfully');
                    this.router.navigate(['payroll/epfsetup']);

                }, error => {
                    console.log(error);
                });

        }
    }
    onlyNumberKey(event) {
        //console.log(event);
        if (event.charCode == 46) {

        } else {
            return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
        }
    }

}
