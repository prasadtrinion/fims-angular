import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../material/material.module';
import { Ng4LoadingSpinnerModule } from 'ng4-loading-spinner';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { DataTableModule } from "angular-6-datatable";

import { HttpClientModule, HttpClient,HTTP_INTERCEPTORS } from '@angular/common/http';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { PayrollRoutingModule } from './payroll.router.module';

import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { MonthlydeductionComponent } from './monthlydeduction/monthlydeduction.component';
import { MonthlydeductionFormComponent } from './monthlydeduction/monthlydeductionform/monthlydeductionform.components';
import { MonthlydeductionService } from './service/monthlydeduction.service';

import { PayrollpostingComponent } from './payrollposting/payrollposting.component';
import { PayrollpostingFormComponent } from './payrollposting/payrollpostingform/payrollpostingform.component';
import { PayrollpostingService } from './service/payrollposting.service';
import { BanksetupaccountService } from '../generalsetup/service/banksetupaccount.service';

import { EaformComponent } from './eaform/eaform.component';
import { EaformaddComponent } from './eaform/eaformadd/eaformadd.component';
import { EaformService } from './service/eaform.service';
import { StaffService } from '../loan/service/staff.service';
import { PayrollprocessComponent } from './payrollprocess/payrollprocess.component';
import { PayrollprocessFormComponent } from './payrollprocess/payrollprocessform/payrollprocessform.component';
import { PayrollprocessService } from './service/payrollprocess.service';

import { StafftaxdeductionComponent } from './stafftaxdeduction/stafftaxdeduction.component';
import { StafftaxdeductionFormComponent } from './stafftaxdeduction/stafftaxdeductionform/stafftaxdeductionform.component';
import { StafftaxdeductionService } from './service/stafftaxdeduction.service';

import { EaformprocessComponent } from './eaformprocess/eaformprocess.component';
import { EaformprocessformComponent } from './eaformprocess/eaformprocessform/eaformprocessform.component';
import { EaformprocessService } from './service/eaformprocess.service';

import { PrintsalarysheetComponent } from "./printsalarysheet/printsalarysheet.component";
import { PrintsalarysheetService } from "./service/printsalarysheet.service";


import { CompanyService } from '../generalsetup/service/company.service';
import { EpfsetupComponent } from './epfsetup/epfsetup.component';
import { EpfsetupformComponent } from './epfsetup/epfsetupform/epfsetupform.component';
import { EpfsetupService } from './service/epfsetup.service';

import { DatemaintainanceComponent } from './datemaintainance/datemaintainance.component';
import { DatemaintainanceFormComponent } from './datemaintainance/datemaintainanceform/datemaintainanceform.component';
import { DatemaintainanceService } from './service/datemaintainance.service';

import { SocsosetupComponent } from './socsosetup/socsosetup.component';
import { SocsosetupformComponent } from './socsosetup/socsosetupform/socsosetupform.component';
import { SocsosetupService } from './service/socsosetup.service';

import { SalaryentryComponent } from './salaryentry/salaryentry.component';
import { SalaryentryFormComponent } from './salaryentry/salaryentryform/salaryentryform.component';
import { SalaryentryService } from './service/salaryentry.service';

import { EpfcategoryComponent } from "./epfcategory/epfcategory.component";
import { EpfcategoryFormComponent } from "./epfcategory/epfcategoryform/epfcategoryform.component";
import { EpfcategoryService } from "./service/epfcategory.service";

import { SalaryComponent } from './salary/salary.component';
import { SalaryFormComponent } from './salary/salaryform/salaryform.component';
import { SalaryService } from './service/salary.service';
import { PayrolltaxService } from './service/payrolltax.service';

import { LeveloneComponent } from './payrolltax/levelone/levelone.component';
import { LeveltwoComponent } from './payrolltax/leveltwo/leveltwo.component';
import { LevelthreeComponent } from './payrolltax/levelthree/levelthree.component';
import { LevelfourComponent } from './payrolltax/levelfour/levelfour.component';
import { JwtInterceptor } from '../_helpers/jwt.interceptors';

import { EmpSalaryComponent } from './empsalary/empsalary.component'
import { EmpsalaryService } from "./service/empsalary.service";

import { ConfirmpayslipComponent } from './confirmpayslip/confirmpayslip.component';
import { ConfirmpayslipService } from './service/confirmpayslip.service';

import { StaffContributionComponent } from "./staffcontribution/staffcontribution.component";
import { StaffContributionFormComponent } from "./staffcontribution/staffcontributionform/staffcontributionform.component";
import { StaffContributionService } from "./service/staffcontribution.service";


import { SocsocategoryComponent } from './socsocategory/socsocategory.component';
import { SocsocategoryformComponent } from './socsocategory/socsocategoryform/socsocategoryform.component';
import { SocsocategoryService } from './service/socsocategory.service';

import { ProcesstypeComponent } from './processtype/processtype.component';
import { ProcesstypeFormComponent } from './processtype/processtypeform/processtypeform.component';
import { ProcesstypeService } from './service/processtype.service';


// import { ArraySortPipe } from "../sort.pipe";

import { KwapComponent } from "./kwap/kwap.component";
import { KwapFormComponent } from "./kwap/kwapform/kwapform.component";
import { KwapService } from "./service/kwap.service";

import { StaffTaggingComponent } from "./stafftagging/stafftagging.component";
import { StaffTaggingFormComponent } from "./stafftagging/stafftaggingform/stafftaggingform.component";
import { StaffTaggingService } from "./service/stafftagging.service";

import { PrintPayslipComponent } from "./printpayslip/printpayslip.component";
import { PrintPayslipService } from "./service/printpayslip.service";

import { ProcesssalaryComponent } from "./processsalary/processsalary.component";
import { ProcesssalaryFormComponent } from "./processsalary/processsalaryform/processsalaryform.component";
import { ProcesssalaryService } from "./service/processsalary.service";

import { DeclareincometaxComponent } from "./declareincometax/declareincometax.component";
import { DeclareincometaxFormComponent } from "./declareincometax/declareincometaxform/declareincometaxform.component";
import { DeclareincometaxService } from "./service/Declareincometax.service";

// import { ArraySortPipe } from "../sort.pipe";
import { NgSelectModule } from '@ng-select/ng-select';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { SharedModule } from "../shared/shared.module";




@NgModule({
    imports: [
        CommonModule,
        DataTableModule,
        FormsModule,
        ReactiveFormsModule,
        MaterialModule,
        PayrollRoutingModule,
        HttpClientModule,
        TabsModule.forRoot(),
        Ng2SearchPipeModule,
        HttpClientModule,
        FontAwesomeModule,
        NgSelectModule,
        NgMultiSelectDropDownModule.forRoot(),
        SharedModule
    ],
    exports: [],
    declarations: [
       
        MonthlydeductionComponent,
        MonthlydeductionFormComponent,
        PayrollpostingFormComponent,
        PayrollpostingComponent,
        EaformaddComponent,
        EaformComponent,
        EaformprocessComponent,
        EaformprocessformComponent,
        EpfsetupComponent,
        EpfsetupformComponent, 
        PayrollprocessFormComponent,
        PayrollprocessComponent,
        EaformprocessformComponent,
        SocsosetupComponent,
        SocsosetupformComponent,
        SalaryentryComponent,
        SalaryentryFormComponent,
        StafftaxdeductionFormComponent,
        StafftaxdeductionComponent,
        EaformprocessformComponent,
        LeveloneComponent,
        LeveltwoComponent,
        LevelthreeComponent,
        LevelfourComponent,
        EmpSalaryComponent,
        StaffContributionComponent,
        StaffContributionFormComponent,
        DatemaintainanceComponent,
        SalaryFormComponent,
        SalaryComponent,
        DatemaintainanceFormComponent,
        PrintsalarysheetComponent,
        ConfirmpayslipComponent,

        SocsocategoryComponent,
        SocsocategoryformComponent,
        ProcesstypeComponent,
        ProcesstypeFormComponent,

 
        EpfcategoryComponent,
        EpfcategoryFormComponent,

        KwapComponent,
        KwapFormComponent,
        StaffTaggingComponent,
        StaffTaggingFormComponent,
        PrintPayslipComponent,
        ProcesssalaryComponent,
        ProcesssalaryFormComponent,
        DeclareincometaxComponent,
        DeclareincometaxFormComponent,


    ],
    providers: [
       
        MonthlydeductionService,
        PayrollpostingService,
        EaformService,
        EaformService,
        EaformprocessService,
        CompanyService,
        EpfsetupService,
        PayrollprocessService,
        CompanyService,
        SocsosetupService,
        SalaryentryService,
        StafftaxdeductionService,
        SalaryService,
        SalaryentryService,
        StaffService,
        BanksetupaccountService,
        EmpsalaryService,
        PayrolltaxService,
        EpfcategoryService,
        StaffContributionService,
        KwapService,
        StaffTaggingService,
        DatemaintainanceService,
        SocsocategoryService,
        PrintPayslipService,
        ProcesstypeService,
        PrintsalarysheetService,
        ProcesssalaryService,
        DeclareincometaxService,
        ConfirmpayslipService,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: JwtInterceptor,
            multi: true
          }


    ],
})
export class PayrollModule { }
