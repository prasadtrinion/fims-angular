import { Injector } from '@angular/core';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { StaffService } from '../../../loan/service/staff.service';
import { StaffContributionService } from "../../service/staffcontribution.service";
import { ActivitycodeService } from '../../../generalsetup/service/activitycode.service';
import { DepartmentService } from '../../../generalsetup/service/department.service';
import { FundService } from '../../../generalsetup/service/fund.service';


@Component({
    selector: 'StaffContributionFormComponent',
    templateUrl: 'staffcontributionform.component.html'
})

export class StaffContributionFormComponent implements OnInit {
    staffcontributionform: FormGroup; 
    staffcontributionList = [];
    staffcontributionData = {};
    staffList=[];
    DeptList = [];
    fundList = [];
    activitycodeList = [];
    id: number;
    ajaxCount: number;
    constructor(

        private StaffContributionService: StaffContributionService,
        private ActivitycodeService: ActivitycodeService,
        private DepartmentService: DepartmentService,
        private FundService: FundService,
        private route: ActivatedRoute,
        private StaffService: StaffService,
        private router: Router

    ) { }

    ngDoCheck() {
        const change = this.ajaxCount;
        //console.log(this.ajaxCount);
        if (this.ajaxCount == 0) {
            this.ajaxCount = 10;
        }
    }

    ngOnInit() {

        this.staffcontributionData['f083fstatus'] = 1;
        
        //staff dropdown
        this.ajaxCount++;
        this.StaffService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.staffList = data['result']['data'];
            }, error => {
                console.log(error);
            });

        //Department dropdown
        this.ajaxCount++;
        this.DepartmentService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.DeptList = data['result']['data'];
            }
        );


        //Activity dropdown
        this.ajaxCount++;
        this.ActivitycodeService.getItemsByLevel().subscribe(
            data => {
                this.ajaxCount--;
                this.activitycodeList = data['result']['data'];
            }
        );
        //Fund dropdown
        this.ajaxCount++;
        this.FundService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.fundList = data['result']['data'];
            }
        );

        this.staffcontributionform = new FormGroup({
            staffcontribution: new FormControl('', Validators.required),  
        });
      
       
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
      
        if (this.id > 0) {
            this.StaffContributionService.getItemsDetail(this.id).subscribe(
                data => {
                    this.staffcontributionData = data['result'][0];
                    this.staffcontributionData['f083fstatus'] = parseInt(this.staffcontributionData['f083fstatus']);
                console.log(this.staffcontributionData);
                }, error => {
                    console.log(error);
                });
        }
        console.log(this.id);

       
    }
    addstaffContribution() {

        var confirmPop = confirm("Do you want to Save?");
        if(confirmPop==false) 
        {
            return false;
        } 
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        console.log(this.id);
        if (this.id > 0) {
            this.StaffContributionService.updateStaffContributionItems(this.staffcontributionData, this.id).subscribe(
                data => {
                    alert('StaffContribution has been Updated Successfully');
                    this.router.navigate(['payroll/staffcontribution']);
                }, error => {
                    console.log(error);
                });
            console.log(this.id);



        } else {

            this.StaffContributionService.insertStaffContributionItems(this.staffcontributionData).subscribe(
                data => {
                    alert('StaffContribution has been Added Successfully');
                    this.router.navigate(['payroll/staffcontribution']);

                }, error => {
                    console.log(error);
                });

            console.log(this.id);


        }
    }

}