import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { StaffContributionService } from "../service/staffcontribution.service";

@Component({
    selector: 'StaffContributionComponent',
    templateUrl: 'staffcontribution.component.html'
})
export class StaffContributionComponent implements OnInit {
    staffcontributionList = [];
    staffcontributionData = {};
    id: number;
    constructor(
         private StaffContributionService: StaffContributionService
    ) { }

    ngOnInit() {

        this.StaffContributionService.getItems().subscribe(
            data => {
                this.staffcontributionList = data['result']['data'];
            }, error => {
                console.log(error);
            });
    }
}