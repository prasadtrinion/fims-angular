import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { EmpsalaryService } from "../service/empsalary.service";
import { EpfsetupService } from "../service/epfsetup.service";

@Component({
    selector: 'EmpSalaryComponent',
    templateUrl: 'empsalary.component.html'
})
export class EmpSalaryComponent implements OnInit {
    empsalaryList = [];
    empsalaryData = {};
    id: number;
    EpfsetupList = [];
    constructor(
        private EmpsalaryService: EmpsalaryService,
        private EpfsetupService: EpfsetupService

    ) { }

    ngOnInit() {

        this.EmpsalaryService.getItems().subscribe(
            data => {
                this.empsalaryList = data['result']['data'];
            }, error => {
                console.log(error);
            });


            this.EpfsetupService.getItems().subscribe(
                data => {
                    this.EpfsetupList = data['result']['data'];
                }, error => {
                    console.log(error);
                });

    }

    generateCalculation() {
        this.generateEPF();
    }

    generateEPF(){
        for(var i=0;i<this.empsalaryList.length;i++) {
            let amount = this.empsalaryList[i]['f011famount'];
            let staffId = this.empsalaryList[i]['f011femployeeId'];
            this.empsalaryList[i]['epfamount'] = this.getEPFAmount(amount,staffId);
        }

        console.log(this.empsalaryList);
    }

    getEPFAmount(amount,staffId) {
        if(amount>5000) {
        // get the contribution from the emp epf setup table

        }else {
        // set up table

        for(var i=0;i<this.EpfsetupList.length;i++) {
            let epfendAmount = parseInt(this.EpfsetupList[i]['f030fendAmount']);
            let epfstartAmount = parseInt(this.EpfsetupList[i]['f030fstartAmount']);

            if(epfendAmount>amount && epfstartAmount<amount) {
                let employercontribution = (amount/100) * parseFloat(this.EpfsetupList[i]['f030femployerContribution']);
                let employeecontribution = (amount/100) * parseFloat(this.EpfsetupList[i]['f030femployerDeduction']);
                return employercontribution+'---'+employeecontribution;
            }
        }

        }
        
    }
}