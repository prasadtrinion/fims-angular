import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SalaryentryService } from '../service/salaryentry.service';

@Component({
    selector: 'SalaryentryComponent',
    templateUrl: 'salaryentry.component.html'
})
export class SalaryentryComponent implements OnInit {
    salaryentryList = [];
    salaryentryData = {};
    id: number;
    constructor(
         private SalaryentryService: SalaryentryService
    ) { }

    ngOnInit() {

        this.SalaryentryService.getItems().subscribe(
            data => {
                this.salaryentryList = data['result']['data'];
            }, error => {
                console.log(error);
            });
    }
    // addSalaryentry() {
    //     // console.log(this.salaryentryData);
    //     this.SalaryentryService.insertsalaryentryItems(this.salaryentryData).subscribe(
    //         data => {
    //             this.salaryentryList = data['todos'];
    //         }, error => {
    //             console.log(error);
    //         });

    // }
}