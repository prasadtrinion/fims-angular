import { Injector } from '@angular/core';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { StaffService } from '../../../loan/service/staff.service';
import { SalaryentryService } from '../../service/salaryentry.service';



@Component({
    selector: 'SalaryentryFormComponent',
    templateUrl: 'salaryentryform.component.html'
})

export class SalaryentryFormComponent implements OnInit {
    salaryentryform: FormGroup;
    salaryentryList = [];
    salaryentryData = {};
    paymentList = [];
    staffList = [];
    typeList =[];
    id: number;
    constructor(

        private SalaryentryService: SalaryentryService,
        private route: ActivatedRoute,
        private StaffService: StaffService,
        private router: Router

    ) { }

    ngOnInit() {
        let paymentobj = {};
        paymentobj['id'] = 1;
        paymentobj['name'] = 'SAB';
        this.paymentList.push(paymentobj);
        paymentobj = {};
        paymentobj['id'] = 2;
        paymentobj['name'] = 'Bankers Cheque';
        this.paymentList.push(paymentobj);

        let typeobj = {};
        typeobj['id'] = 1;
        typeobj['name'] = 'EPF';
        this.typeList.push(typeobj);
        typeobj = {};
        typeobj['id'] = 2;
        typeobj['name'] = 'SOCSO';
        this.typeList.push(typeobj);
        typeobj = {};
        typeobj['id'] = 3;
        typeobj['name'] = 'TAX';
        this.typeList.push(typeobj);

        this.salaryentryData['f081fstatus'] = 1;
        this.salaryentryData['f081fidStaff'] = '';

        //staff dropdown
        this.StaffService.getItems().subscribe(
            data => {
                this.staffList = data['result']['data'];
            }, error => {
                console.log(error);
            });

        this.salaryentryform = new FormGroup({
            salaryentry: new FormControl('', Validators.required),
        });


        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        if (this.id > 0) {
            this.SalaryentryService.getItemsDetail(this.id).subscribe(
                data => {
                    this.salaryentryData = data['result'][0];
                    this.salaryentryData['f081fstatus'] = parseInt(this.salaryentryData['f081fstatus']);
                    console.log(this.salaryentryData);
                }, error => {
                    console.log(error);
                });
        }
        console.log(this.id);


    }
    addSalaryentry() {

        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.SalaryentryService.updateSalaryentryItems(this.salaryentryData, this.id).subscribe(
                data => {

                    if (data['status'] == 409) {
                        alert('Cash Advance ID Already Exist');

                    }
                    else {
                        alert('Salary Entry has been Updated Successfully');
                        this.router.navigate(['payroll/salaryentry']);
                    }
                    // this.router.navigate(['payroll/salaryentry']);
                }, error => {
                    console.log(error);
                });


        } else {

            this.SalaryentryService.insertSalaryentryItems(this.salaryentryData).subscribe(
                data => {

                    alert('Salary Entry has been Added Successfully');                    
                    this.router.navigate(['payroll/salaryentry']);

                }, error => {
                    console.log(error);
                });

        }
    }

}
