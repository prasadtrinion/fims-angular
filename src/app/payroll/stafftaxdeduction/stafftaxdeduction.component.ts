import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
// import { ItemcategoryService } from '../service/itemcategory.service'
import { StafftaxdeductionService } from "../service/stafftaxdeduction.service";
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'StafftaxdeductionComponent',
    templateUrl: 'stafftaxdeduction.component.html'
})
export class StafftaxdeductionComponent implements OnInit {
    itemcategoryList = [];
    itemcategoryData = {};
    id: number;
    taxDeductionList=[];
    constructor(
        private StafftaxdeductionService: StafftaxdeductionService,
        private spinner: NgxSpinnerService

    ) { }

    ngOnInit() {
        this.spinner.show();
        this.StafftaxdeductionService.getItems().subscribe(
            data => {
                this.spinner.hide();
               this.taxDeductionList = data['result']['data'];
            }, error => {
                console.log(error);
            });
    }
    addItemcategory() {
        console.log(this.itemcategoryData);
        // this.ItemcategoryService.insertItemcategoryItems(this.itemcategoryData).subscribe(
        //     data => {
        //         this.itemcategoryList = data['todos'];
        //     }, error => {
        //         console.log(error);
        //     });

    }
}