import { Injector } from '@angular/core';
import { Component, OnInit, ViewChild } from '@angular/core';

import { ActivatedRoute, Router } from '@angular/router';
import { StafftaxdeductionService } from "../../service/stafftaxdeduction.service";
import { StaffService } from '../../../loan/service/staff.service';

@Component({
    selector: 'StafftaxdeductionFormComponent',
    templateUrl: 'stafftaxdeductionform.component.html'
})

export class StafftaxdeductionFormComponent implements OnInit {

    taxDeductionList = [];
    taxDeductionData = {};
    taxDeductionDataHeader = {};
    staffList = [];
    typeList = [];
    deptList = [];
    id: number;
    ajaxCount: number;
    showLastRow: boolean;
    showLastRowMinus: boolean;
    showLastRowPlus: boolean;
    listshowLastRowMinus: boolean;
    listshowLastRowPlus: boolean;
    constructor(

        private StafftaxdeductionService: StafftaxdeductionService,
        private StaffService: StaffService,
        private route: ActivatedRoute,
        private router: Router

    ) { }
    deletemodule() {


        this.showLastRow = true;
        this.showIcons();
    }

    showIcons() {
        if (this.taxDeductionList.length > 0) {
            this.showLastRowMinus = true;
            this.showLastRowPlus = true;
            this.listshowLastRowMinus = true;
        }

        if (this.taxDeductionList.length > 1) {
            if (this.showLastRow == false) {
                this.listshowLastRowPlus = false;
                this.listshowLastRowMinus = true;
                this.showLastRowPlus = true;

            } else {
                this.listshowLastRowPlus = true;
                this.listshowLastRowMinus = false;

            }
        }
        if (this.taxDeductionList.length == 1 && this.showLastRow == false) {
            this.listshowLastRowMinus = true;
            this.listshowLastRowPlus = false;

        }
        if (this.taxDeductionList.length == 1 && this.showLastRow == true) {
            this.listshowLastRowMinus = false;
            this.listshowLastRowPlus = true;

        }
        if (this.taxDeductionList.length == 0 && this.showLastRow == false) {
            this.showLastRowMinus = false;
            this.showLastRowPlus = true;
        }
        if (this.taxDeductionList.length == 0 && this.showLastRow == true) {
            this.showLastRowMinus = true;
            this.showLastRowPlus = false;
        }
        console.log(this.taxDeductionList.length);
        console.log("Show last row" + this.showLastRow);
        console.log("showLastRowMinus" + this.showLastRowMinus);
        console.log("showLastRowPlus" + this.showLastRowPlus);
        console.log("listshowLastRowMinus" + this.listshowLastRowMinus);
        console.log("listshowLastRowPlus" + this.listshowLastRowPlus);
    }
    showNewRow() {
        this.showLastRow = false;
        this.showIcons();
    }

    editFunction() {
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.StafftaxdeductionService.getItemsDetail(this.id).subscribe(
                data => {
                    this.taxDeductionList = data['result'];
                    this.taxDeductionDataHeader['f082fidStaff'] = data['result'][0]["f082fidStaff"];

                    console.log(this.taxDeductionList);
                    this.showIcons();
                }, error => {
                    console.log(error);
                });
        }
    }

    ngOnInit() {
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id < 1) {
            this.showLastRow = false;
            this.showLastRowMinus = false;
            this.showLastRowPlus = true;
        } else {
            this.showLastRow = true;

        }

        let typeobj = {};
        typeobj['id'] = 1;
        typeobj['name'] = 'EPF';
        this.typeList.push(typeobj);
        typeobj = {};
        typeobj['id'] = 2;
        typeobj['name'] = 'SOCSO';
        this.typeList.push(typeobj);
        typeobj = {};
        typeobj['id'] = 3;
        typeobj['name'] = 'TAX';
        this.typeList.push(typeobj);
        typeobj = {};
        typeobj['id'] = 4;
        typeobj['name'] = 'ZAKATH';
        this.typeList.push(typeobj);

        let deptobj = {};
        deptobj['id'] = 1;
        deptobj['name'] = 'CODE1';
        this.deptList.push(deptobj);
        deptobj = {};
        deptobj['id'] = 2;
        deptobj['name'] = 'CODE2';
        this.deptList.push(deptobj);

        // Staff dropdown
        this.ajaxCount++;
        this.StaffService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.staffList = data['result']['data'];
            }, error => {
                console.log(error);
            });

           this.editFunction();
            this.showIcons();
    }
    SaveTaxDeduction() {

        if (this.taxDeductionDataHeader['f082fidStaff'] == undefined) {
            alert("Select Staff Name");
            return false;
        }
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        // if (this.taxDeductionDataHeader['f082fname'] == undefined) {
        //     alert("Enter Tax Deduction Name");
        //     return false;
        // }
        // this.taxDeductionDataHeader['f082fname'] = '0';
        // this.taxDeductionDataHeader['f082fidStaff'] = '0';
        if (this.showLastRow == false) {
            var addactivities = this.addTaxData();
            if (addactivities == false) {
                return false;
            }

        }
        let taxDeductionObject = {};
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        console.log(this.id);
        if (this.id > 0) {
            taxDeductionObject['f082fid'] = this.id;
        }

        taxDeductionObject['f082fidStaff'] = this.taxDeductionDataHeader['f082fidStaff'];
        taxDeductionObject['f082fname'] = '0';
        taxDeductionObject['f082fstatus'] = 1;
        taxDeductionObject['taxDeduction-details'] = this.taxDeductionList;
        console.log(taxDeductionObject);




       
            this.StafftaxdeductionService.insertItemcategoryItems(taxDeductionObject).subscribe(
                data => {
                    alert('Tax Deduction has been saved');
                    this.router.navigate(['payroll/stafftaxdeduction']);

                }, error => {
                    console.log(error);
                });

       
    }

    deleteData(object) {
        var cnf = confirm("Do you really want to delete");
        if (cnf == true) {
            console.log(this.taxDeductionList);
            var index = this.taxDeductionList.indexOf(object);;
            if (index > -1) {
                this.taxDeductionList.splice(index, 1);
            }
            this.showIcons();
        }
    }

    dateComparison(){

        let startDate = Date.parse(this.taxDeductionData['f082fstartDate']);
        let endDate = Date.parse(this.taxDeductionData['f082fendDate']);

        if(startDate>endDate){
            alert("Start Date cannot be greater than End Date !");
            this.taxDeductionData['f082fendDate'] = "";
        }

    }


    addTaxData() {
        console.log(this.taxDeductionData);

        if (this.taxDeductionData['f082ftype'] == undefined) {
            alert("Select Type");
            return false;
        }
        // if (this.taxDeductionData['f082fdeductionCode'] == undefined) {
        //     alert("Select Deduction Code");
        //     return false;
        // }
        if (this.taxDeductionData['f082fstartDate'] == undefined) {
            alert("Select Start Date");
            return false;
        }
        if (this.taxDeductionData['f082fendDate'] == undefined) {
            alert("Select End Date");
            return false;
        }
        if (this.taxDeductionData['f082famount'] == undefined) {
            alert("Enter Amount");
            return false;
        }
        var confirmPop = confirm("Do you want to Add?");
        if (confirmPop == false) {
            return false;
        }
        this.taxDeductionData['f082fdeductionCode'] = '0';

        var dataofCurrentRow = this.taxDeductionData;
        this.taxDeductionData = {};
        this.taxDeductionList.push(dataofCurrentRow);
        this.showIcons();
    }
    onlyNumberKey(event) {
        //console.log(event);
        if (event.charCode == 46) {

        } else {
            return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
        }
    }
}