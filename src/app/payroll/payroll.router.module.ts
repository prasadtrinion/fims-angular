import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


import { MonthlydeductionComponent } from './monthlydeduction/monthlydeduction.component';
import { MonthlydeductionFormComponent } from './monthlydeduction/monthlydeductionform/monthlydeductionform.components';

import { PayrollpostingComponent } from './payrollposting/payrollposting.component';
import { PayrollpostingFormComponent } from './payrollposting/payrollpostingform/payrollpostingform.component';

import { EaformComponent } from './eaform/eaform.component';
import { EaformaddComponent } from './eaform/eaformadd/eaformadd.component';

import { PayrollprocessComponent } from './payrollprocess/payrollprocess.component';
import { PayrollprocessFormComponent } from './payrollprocess/payrollprocessform/payrollprocessform.component';

import { StafftaxdeductionComponent } from './stafftaxdeduction/stafftaxdeduction.component';
import { StafftaxdeductionFormComponent } from './stafftaxdeduction/stafftaxdeductionform/stafftaxdeductionform.component';

import { EaformprocessComponent } from './eaformprocess/eaformprocess.component';
import { EaformprocessformComponent } from './eaformprocess/eaformprocessform/eaformprocessform.component';

import { EpfsetupComponent } from './epfsetup/epfsetup.component';
import { EpfsetupformComponent } from './epfsetup/epfsetupform/epfsetupform.component';

import { SocsosetupComponent } from './socsosetup/socsosetup.component';
import { SocsosetupformComponent } from './socsosetup/socsosetupform/socsosetupform.component';


import { SalaryComponent } from './salary/salary.component';
import { SalaryFormComponent } from './salary/salaryform/salaryform.component';

import { PrintsalarysheetComponent } from "./printsalarysheet/printsalarysheet.component";

import { EpfcategoryComponent } from "./epfcategory/epfcategory.component";
import { EpfcategoryFormComponent } from "./epfcategory/epfcategoryform/epfcategoryform.component";

import { SalaryentryComponent } from './salaryentry/salaryentry.component';
import { SalaryentryFormComponent } from './salaryentry/salaryentryform/salaryentryform.component';


import { LeveloneComponent } from './payrolltax/levelone/levelone.component';
import { LeveltwoComponent } from './payrolltax/leveltwo/leveltwo.component';
import { LevelthreeComponent } from './payrolltax/levelthree/levelthree.component';
import { LevelfourComponent } from './payrolltax/levelfour/levelfour.component';

import { DatemaintainanceComponent } from './datemaintainance/datemaintainance.component';
import { DatemaintainanceFormComponent } from './datemaintainance/datemaintainanceform/datemaintainanceform.component';

import { StaffContributionComponent } from "./staffcontribution/staffcontribution.component";
import { StaffContributionFormComponent } from "./staffcontribution/staffcontributionform/staffcontributionform.component";

import { EmpSalaryComponent } from './empsalary/empsalary.component'
import { SocsocategoryComponent } from './socsocategory/socsocategory.component';
import { SocsocategoryformComponent } from './socsocategory/socsocategoryform/socsocategoryform.component';

import { ProcesstypeComponent } from './processtype/processtype.component';
import { ProcesstypeFormComponent } from './processtype/processtypeform/processtypeform.component';

import { KwapComponent } from "./kwap/kwap.component";
import { KwapFormComponent } from "./kwap/kwapform/kwapform.component";

import { ConfirmpayslipComponent } from './confirmpayslip/confirmpayslip.component';

import { StaffTaggingComponent } from "./stafftagging/stafftagging.component";
import { StaffTaggingFormComponent } from "./stafftagging/stafftaggingform/stafftaggingform.component";
import { ProcesssalaryComponent } from "./processsalary/processsalary.component";
import { ProcesssalaryFormComponent } from "./processsalary/processsalaryform/processsalaryform.component";
import { PrintPayslipComponent } from "./printpayslip/printpayslip.component";
import { DeclareincometaxComponent } from "./declareincometax/declareincometax.component";
import { DeclareincometaxFormComponent } from "./declareincometax/declareincometaxform/declareincometaxform.component";

const routes: Routes = [

  { path: 'monthlydeduction', component: MonthlydeductionComponent },
  { path: 'addnewmonthlydeduction', component: MonthlydeductionFormComponent },
  { path: 'editmonthlydeduction/:id', component: MonthlydeductionFormComponent },

  { path: 'payrollposting', component: PayrollpostingComponent },
  { path: 'addnewpayrollposting', component: PayrollpostingFormComponent },
  { path: 'editpayrollposting/:id', component: PayrollpostingFormComponent },

  { path: 'printsalaryscreen', component: PrintsalarysheetComponent },
  
  { path: 'payrollprocess', component: PayrollprocessComponent },
  { path: 'addnewpayrollprocess', component: PayrollprocessFormComponent },
  { path: 'editpayrollprocess/:id', component: PayrollprocessFormComponent },

  { path: 'salary', component: SalaryComponent },
  { path: 'addnewsalary', component: SalaryFormComponent },
  { path: 'editsalary/:id', component: SalaryFormComponent },

  { path: 'datemaintainance', component: DatemaintainanceComponent },
  { path: 'addnewdatemaintainance', component: DatemaintainanceFormComponent },
  { path: 'editdatemaintainance/:id', component: DatemaintainanceFormComponent },

  { path: 'eaform', component: EaformComponent },
  { path: 'addneweaform', component: EaformaddComponent },
  { path: 'editeaform/:id', component: EaformaddComponent },
  
  { path: 'epfcategory', component: EpfcategoryComponent },
  { path: 'addnewepfcategory', component: EpfcategoryFormComponent },
  { path: 'editepfcategory/:id', component: EpfcategoryFormComponent },

  { path: 'stafftaxdeduction', component: StafftaxdeductionComponent },
  { path: 'addnewstafftaxdeduction', component: StafftaxdeductionFormComponent },
  { path: 'editstafftaxdeduction/:id', component: StafftaxdeductionFormComponent },

  { path: 'eaformprocess', component: EaformprocessComponent },
  { path: 'addneweaformprocess', component: EaformprocessformComponent },
  { path: 'editeaformprocess/:id', component: EaformprocessformComponent },

  { path: 'epfsetup', component: EpfsetupComponent },
  { path: 'addnewepfsetup', component: EpfsetupformComponent },
  { path: 'editepfsetup/:id', component: EpfsetupformComponent },

  { path: 'socsosetup', component: SocsosetupComponent },
  { path: 'addnewsocsosetup', component: SocsosetupformComponent },
  { path: 'editsocsosetup/:id', component: SocsosetupformComponent },

  { path: 'salaryentry', component: SalaryentryComponent },
  { path: 'addnewsalaryentry', component: SalaryentryFormComponent },
  { path: 'editsalaryentry/:id', component: SalaryentryFormComponent },

  { path: 'levelone', component: LeveloneComponent },
  { path: 'leveltwo', component: LeveltwoComponent },
  { path: 'levelthree', component: LevelthreeComponent },
  { path: 'levelfour', component: LevelfourComponent },

  { path: 'empsalary', component: EmpSalaryComponent },

  { path: 'staffcontribution', component: StaffContributionComponent },
  { path: 'addnewstaffcontribution', component: StaffContributionFormComponent },
  { path: 'editstaffcontribution/:id', component: StaffContributionFormComponent },


  { path: 'socsocategory', component: SocsocategoryComponent },
  { path: 'addnewsocsocategory', component: SocsocategoryformComponent },
  { path: 'editsocsocategory/:id', component: SocsocategoryformComponent },

  { path: 'processtype', component: ProcesstypeComponent },
  { path: 'addnewprocesstype', component:ProcesstypeFormComponent },
  { path: 'editprocesstype/:id', component: ProcesstypeFormComponent },

  { path: 'kwap', component: KwapComponent },
  { path: 'addnewkwap', component: KwapFormComponent },
  { path: 'editkwap/:id', component: KwapFormComponent },

  { path: 'stafftagging', component: StaffTaggingComponent },
  { path: 'addnewstafftagging', component: StaffTaggingFormComponent },
  { path: 'editstafftagging/:id', component: StaffTaggingFormComponent },

  { path: 'printpayslip', component: PrintPayslipComponent },

  { path: 'confirmpayslip', component: ConfirmpayslipComponent },



  { path: 'processsalary', component: ProcesssalaryComponent },
  { path: 'addnewprocesssalary', component: ProcesssalaryFormComponent },
  { path: 'editprocesssalary/:id', component: ProcesssalaryFormComponent },

  { path: 'declareincometax', component: DeclareincometaxComponent },
  { path: 'addnewdeclareincometax', component: DeclareincometaxFormComponent },
  { path: 'editdeclareincometax/:id', component: DeclareincometaxFormComponent },

  // { path: 'editsalaryentry/:id', component: EmpSalaryComponent },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  
})
export class PayrollRoutingModule { 
  
}