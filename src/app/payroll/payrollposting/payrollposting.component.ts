import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { PayrollpostingService} from '../service/payrollposting.service';

@Component({
    selector: 'PayrollpostingComponent',
    templateUrl: 'payrollposting.component.html'
})
export class PayrollpostingComponent implements OnInit {
    payrollpostingList = [];
    payrollpostingData = {};
    id: number;
    constructor(
        private PayrollpostingService: PayrollpostingService
   ) { }

    ngOnInit() {

        this.PayrollpostingService.getItems().subscribe(
            data => {
                this.payrollpostingList = data['result']['data'];
            }, error => {
                console.log(error);
            });
    }
    addPayrollposting() {
        console.log(this.payrollpostingData);
        this.PayrollpostingService.insertPayrollpostingItems(this.payrollpostingData).subscribe(
            data => {
                this.payrollpostingList = data['todos'];
            }, error => {
                console.log(error);
            });

    }
}