import { Injector } from '@angular/core';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { PayrollpostingService } from '../../service/payrollposting.service';
import { BanksetupaccountService } from '../../../generalsetup/service/banksetupaccount.service';
import { StaffService } from '../../../loan/service/staff.service';
import { from } from 'rxjs';


@Component({
    selector: 'PayrollpostingFormComponent',
    templateUrl: 'payrollpostingform.component.html'
})

export class PayrollpostingFormComponent implements OnInit {
    payrollpostingform: FormGroup;
    payrollpostingList = [];
    payrollpostingData = {};
    staffList = [];
    banklist = [];
    fmonth = [];

    id: number;
    constructor(

        private PayrollpostingService: PayrollpostingService,
        private StaffService: StaffService,
        private BanksetupaccountService: BanksetupaccountService,
        private route: ActivatedRoute,
        private router: Router

    ) { }

    ngOnInit() {

        let month = {};
        month['name'] = 'January';
        this.fmonth.push(month);
        month = {};
        month['name'] = 'February';
        this.fmonth.push(month);
        month = {};
        month['name'] = 'March';
        this.fmonth.push(month);
        month = {};
        month['name'] = 'April';
        this.fmonth.push(month);
        month = {};
        month['name'] = 'May';
        this.fmonth.push(month);
        month = {};
        month['name'] = 'June';
        this.fmonth.push(month);
        month = {};
        month['name'] = 'July';
        this.fmonth.push(month);
        month = {};
        month['name'] = 'August';
        this.fmonth.push(month); 
        month = {};
        month['name'] = 'September';
        this.fmonth.push(month);
        month = {};
        month['name'] = 'October';
        this.fmonth.push(month); 
        month = {};
        month['name'] = 'November';
        this.fmonth.push(month);
        month = {};
        month['name'] = 'December';
        this.fmonth.push(month);

        this.payrollpostingData['f091fstatus'] = 1;
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        //dropdown
        this.StaffService.getItems().subscribe(
            data => {
                this.staffList = data['result']['data'];
            }, error => {
                console.log(error);
            });

        //dropdown
        this.BanksetupaccountService.getItems().subscribe(
            data => {
                this.banklist = data['result']['data'];
            }, error => {
                console.log(error);
            });

        this.payrollpostingform = new FormGroup({
            payrollprocess: new FormControl('', Validators.required),
        });
        if (this.id > 0) {
            this.PayrollpostingService.getItemsDetail(this.id).subscribe(
                data => {
                    this.payrollpostingData = data['result'][0];
                    this.payrollpostingData['f091fstatus'] = parseInt(this.payrollpostingData['f091fstatus']);
                    console.log(this.payrollpostingData);
                }, error => {
                    console.log(error);
                });
        }
        console.log(this.id);
    }

    addPayrollposting() {

        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.PayrollpostingService.updatePayrollpostingItems(this.payrollpostingData, this.id).subscribe(
                data => {
                    this.router.navigate(['payroll/payrollposting']);
                    alert("Payroll Posting has been Updated Successfully")
                }, error => {
                    console.log(error);
                });


        } else {

            this.PayrollpostingService.insertPayrollpostingItems(this.payrollpostingData).subscribe(
                data => {
                    this.router.navigate(['payroll/payrollposting']);
                    alert("Payroll Posting has been Added Successfully")


                }, error => {
                    console.log(error);
                });

        }
    }
}