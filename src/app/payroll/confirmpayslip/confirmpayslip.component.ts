import { Injector } from '@angular/core';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ConfirmpayslipService } from "../service/confirmpayslip.service";
import { DepartmentService } from '../../generalsetup/service/department.service';
import { StaffService } from '../../loan/service/staff.service';


@Component({
    selector: 'ConfirmpayslipComponent',
    templateUrl: 'confirmpayslip.component.html'
})

export class ConfirmpayslipComponent implements OnInit {
    confirmpayslip : FormGroup;
    confirmpayslipList = [];
    confirmpayslipData = {};
    DeptList=[];
    staffList=[];
    earningComponents = [];
    deductionComponents = [];
    employerComponents = [];
    ajaxCount: number;
    salaryDetails = {}

    id: number;
    constructor(

        private route: ActivatedRoute,
        private router: Router,
        private ConfirmpayslipService: ConfirmpayslipService,
        private DepartmentService:DepartmentService,
        private StaffService:StaffService

    ) { }

    ngOnInit() {

       

        this.confirmpayslip = new FormGroup({
            confirmpayslip: new FormControl('', Validators.required),
        });

          // Staff dropdown
          this.ajaxCount++;
          this.StaffService.getItems().subscribe(
              data => {
                  this.ajaxCount--;
                  this.staffList = data['result']['data'];
              }, error => {
                  console.log(error);
              });

                   // department dropdown
        this.ajaxCount++;
        this.DepartmentService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.DeptList = data['result']['data'];
            }, error => {
                //console.log(error);
            });   

        this.confirmpayslipData = {};
       
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        if (this.id > 0) {
            this.ConfirmpayslipService.getItemsDetail(this.id).subscribe(
                data => {
                    this.confirmpayslipData = data['result'][0];
                    //this.epfformData['f030fstatus'] = parseInt(this.epfformData['f030fstatus']);

                    console.log(this.confirmpayslipData);

                }, error => {
                    console.log(error);
                });


        }
        console.log(this.id);
    }

confirmpayslipShow() {
        let newObj = {};
        newObj['staff_id'] = 1190;
        newObj['month'] = '11/2018';
        this.ConfirmpayslipService.inserteConfirmpayslipItems(newObj).subscribe(
            data => {
               console.log(data);
                this.salaryDetails = data['result'];
               for(var i=0;i<data['result'][0]['details'].length;i++) {
                if(data['result'][0]['details'][i]['f081ftype_of_component']=='1') {
                    this.earningComponents.push(data['result'][0]['details'][i]);
                   }
                   if(data['result'][0]['details'][i]['f081ftype_of_component']=='2') {
                    this.deductionComponents.push(data['result'][0]['details'][i]);
                   }
                   if(data['result'][0]['details'][i]['f081ftype_of_component']=='3') {
                    this.employerComponents.push(data['result'][0]['details'][i]);
                   }
               }

            }, error => {
                console.log(error);
            });


    }
}

