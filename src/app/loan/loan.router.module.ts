import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoannameComponent } from "./loanname/loanname.component";
import { LoannameFormComponent } from "./loanname/loannameform/loannameform.component"

import { LoanperiodComponent } from "./loanperiod/loanperiod.component";
import { ProcessingFeeComponent } from "./processingfee/processingfee.component";

import { VehicletypeComponent } from "./vehicletype/vehicletype.component";
import { VehicletypeFormComponent } from "./vehicletype/vehicletypeform/vehicletypeform.component";

import { LoanperiodFormComponent } from "./loanperiod/loanperiodform/loanperiodform.component";
import { ProcessingFeeFormComponent } from "./processingfee/processingfeeform/processingfeeform.component";
import { ApplyloanComponent } from "./applyloan/applyloan.component";
import { ApplyloanFormComponent } from "./applyloan/applyloanform/applyloanform.component";
import { ApplyloanapprovalComponent } from "./applyloanapproval/applyloanapproval.component";

import { BlackListEmpComponent } from "./blacklistemp/blacklistemp.component";
import { BlackListEmpFormComponent } from "./blacklistemp/blacklistempform/blacklistempform.component";
import { LoancancellationComponent } from "./loancancellation/loancancellation.component";
import { LoancancellationFormComponent } from "./loancancellation/loancancellationform/loancancellationform.component";
import { SmartloanComponent } from "./smartloan/smartloan.component";
import { SmartloanFormComponent } from "./smartloan/smartloanform/smartloanform.component";

import { VehicleloanComponent } from "./vehicleloan/vehicleloan.component";
import { VehicleloanFormComponent } from "./vehicleloan/vehicleloanform/vehicleloanform.component";
import { VehicleloaneditFormComponent } from "./vehicleloan/vehicleloanedit/vehicleloaneditform.component";

import { VehicleloanupdateFormComponent } from "./vehicleloan/vehicleloanupdate/vehicleloanupdateform.component";
import { InsurancesetupComponent } from './insurancesetup/insurancesetup.component';
import { InsurancesetupFormComponent } from './insurancesetup/insurancesetupform/insurancesetupform.component';

import { SetupgradeComponent } from "./setupgrade/setupgrade.component";
import { SetupgradeFormComponent } from "./setupgrade/setupgradeform/setupgradeform.component";

import { LoantypesetupComponent } from "./loantypesetup/loantypesetup.component";
import { LoantypesetupFormComponent } from "./loantypesetup/loantypesetupform/loantypesetupform.component";
import { LoanofferComponent } from "./loanoffer/loanoffer.component"

import { ApprovalComponent } from "./approval/approval.component";

import { LoanreportComponent } from "./loanreport/loanreport.component";
import { SearchapplicationComponent } from "./searchapplication/searchapplication.component";

import { EmpreleaselistComponent } from "./empreleaselist/empreleaselist.component";
import { EmpreleaselistFormComponent } from "./empreleaselist/empreleaselistform/empreleaselistform.component";
import { InsurancecompanyComponent } from "./insurancecompany/insurancecompany.component";
import { InsurancecompanyFormComponent } from "./insurancecompany/insurancecompanyform/insurancecompanyform.component";
import { LoanprocessComponent } from "./loanprocess/loanprocess.component";


import { LoanlistingComponent } from "./loanlisting/loanlisting.component";
import { LoanhistoryComponent } from './loanhistory/loanhistory.component';
import { LoandegradationComponent } from './loandegradation/loandegradation.component';

import { LoanProcessingFeeComponent } from "./loanprocessingfee/loanprocessingfee.component";
import { LoanProcessingFeeFormComponent } from "./loanprocessingfee/loanprocessingfeeform/loanprocessingfeeform.component";
import { LoanPaymentsComponent } from "./loanpayments/loanpayments.component";
import { LoanPaymentsFormComponent } from "./loanpayments/loanpaymentsform/loanpaymentsform.component";

const routes: Routes = [

  { path: 'loanname', component: LoannameComponent },
  { path: 'addnewloanname', component: LoannameFormComponent },
  { path: 'editloanname/:id', component: LoannameFormComponent },

  { path: 'vehicletype', component: VehicletypeComponent },
  { path: 'addnewvehicletype', component: VehicletypeFormComponent },
  { path: 'editvehicletype/:id', component: VehicletypeFormComponent },

  { path: 'loanperiod', component: LoanperiodComponent },
  { path: 'addnewloanperiod', component: LoanperiodFormComponent },
  { path: 'editloanperiod/:id', component: LoanperiodFormComponent },

  { path: 'processingfee', component: ProcessingFeeComponent },
  { path: 'addnewprocessingfee', component: ProcessingFeeFormComponent },
  { path: 'editprocessingfee/:id', component: ProcessingFeeFormComponent },

  { path: 'applyloan', component: ApplyloanComponent },
  { path: 'addnewapplyloan', component: ApplyloanFormComponent },
  { path: 'editapplyloan/:id', component: ApplyloanFormComponent },

  { path: 'applyloanapproval', component: ApplyloanapprovalComponent },

  { path: 'blacklistemp', component: BlackListEmpComponent },
  { path: 'addnewblacklistemp', component: BlackListEmpFormComponent },
  { path: 'editblacklistemp/:id', component: BlackListEmpFormComponent },

  { path: 'loancancellation', component: LoancancellationComponent },
  { path: 'addnewloancancellation', component: LoancancellationFormComponent },
  { path: 'editloancancellation/:id', component: LoancancellationFormComponent },

  { path: 'smartloan', component: SmartloanComponent },
  { path: 'addnewsmartloan', component: SmartloanFormComponent },
  { path: 'editsmartloan/:id', component: SmartloanFormComponent },

  { path: 'insurancesetup', component: InsurancesetupComponent },
  { path: 'addnewinsurancesetup', component: InsurancesetupFormComponent },
  { path: 'editinsurancesetup/:id', component: InsurancesetupFormComponent },

  { path: 'vehicleloan', component: VehicleloanComponent },
  { path: 'addnewvehicleloan', component: VehicleloanFormComponent },
  { path: 'editvehicleloan/:id', component: VehicleloanupdateFormComponent },
  { path: 'viewvehicleloan/:id/:idview', component: VehicleloaneditFormComponent },

  { path: 'guranterone/:level/:status', component: ApplyloanapprovalComponent },
  { path: 'gurantertwo/:level/:status', component: ApplyloanapprovalComponent },
  { path: 'approvalthree/:level/:status', component: ApplyloanapprovalComponent },
  { path: 'approvalfour/:level/:status', component: ApplyloanapprovalComponent },
  { path: 'approvalfive/:level/:status', component: ApplyloanapprovalComponent },
  { path: 'approvalsix/:level/:status', component: ApplyloanapprovalComponent },
  { path: 'approvalseven/:level/:status', component: ApplyloanapprovalComponent },
  { path: 'approvaleight/:level/:status', component: ApplyloanapprovalComponent },
  { path: 'approvalnine/:level/:status', component: ApplyloanapprovalComponent },

  { path: 'setupgrade', component: SetupgradeComponent },
  { path: 'addnewsetupgrade', component: SetupgradeFormComponent },
  { path: 'editsetupgrade/:id', component: SetupgradeFormComponent },

  { path: 'approval', component: ApprovalComponent },

  { path: 'loantypesetup', component: LoantypesetupComponent },
  { path: 'addnewloantypesetup', component: LoantypesetupFormComponent },
  { path: 'editloantypesetup/:id', component: LoantypesetupFormComponent },

  { path: 'loanoffer', component: LoanofferComponent },

  { path: 'empreleaselist', component: EmpreleaselistComponent },
  { path: 'addnewempreleaselist', component: EmpreleaselistFormComponent },
  { path: 'editempreleaslist/:id', component: EmpreleaselistFormComponent },

  { path: 'loanreport', component: LoanreportComponent },
  { path: 'searchapplication', component: SearchapplicationComponent },
  { path: 'loanprocess', component: LoanprocessComponent },
  { path: 'loanlisting', component: LoanlistingComponent },
  { path: 'loanhistory', component: LoanhistoryComponent },
  { path: 'loandegradation', component: LoandegradationComponent },

  { path: 'insurancecompany', component: InsurancecompanyComponent },
  { path: 'addnewinsurancecompany', component: InsurancecompanyFormComponent },
  { path: 'editinsurancecompany/:id', component: InsurancecompanyFormComponent },

  {path: 'loanprocessingfee', component: LoanProcessingFeeComponent},
  {path: 'addloanprocessingfee', component: LoanProcessingFeeFormComponent},
  {path: 'editloanprocessingfee/:id', component: LoanProcessingFeeFormComponent},

  {path: 'loanpayment', component: LoanPaymentsComponent},
  {path: 'addloanpayment', component: LoanPaymentsFormComponent},
  {path: 'editloanpayment/:id', component: LoanPaymentsFormComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],

})
export class LoanRoutingModule {

}