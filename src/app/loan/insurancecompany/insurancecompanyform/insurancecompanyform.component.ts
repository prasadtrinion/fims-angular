import { Injector } from '@angular/core';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { InsurancecompanyService } from '../../service/insurancecompany.service';
import { SupplierregistrationService } from "../../../procurement/service/supplierregistration.service";
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { DefinationmsService } from '../../../studentfinance/service/definationms.service';

@Component({
    selector: 'InsurancecompanyFormComponent',
    templateUrl: 'insurancecompanyform.component.html'
})

export class InsurancecompanyFormComponent implements OnInit {
    applyloanform: FormGroup;
    insurancecompanyList = [];
    insurancecompanyData = {};
    insurancecompanyDataheader = {};
    staffList = [];
    fundList = [];
    id: number;
    ajaxCount: number;
    itemList = [];
    DeptList = [];
    fyearList = [];
    supplierList = [];
    idview: number;
    showLastRow: boolean;
    showLastRowMinus: boolean;
    showLastRowPlus: boolean;
    listshowLastRowMinus: boolean;
    listshowLastRowPlus: boolean;
    viewDisabled: boolean;
    edit: boolean = false;
    constructor(
        private InsurancecompanyService: InsurancecompanyService,
        private SupplierregistrationService: SupplierregistrationService,
        private route: ActivatedRoute,
        private router: Router,
        private DefinationmsService: DefinationmsService,

    ) { }
    deletemodule() {
        this.showLastRow = true;
        this.showIcons();
    }
    showIcons() {
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.insurancecompanyList.length > 0) {
            this.showLastRowMinus = true;
            this.showLastRowPlus = true;
            this.listshowLastRowMinus = true;
        }
        if (this.insurancecompanyList.length > 1) {
            if (this.showLastRow == false) {
                this.listshowLastRowPlus = false;
                this.listshowLastRowMinus = true;
                this.showLastRowPlus = true;

            } else {
                this.listshowLastRowPlus = true;
                this.listshowLastRowMinus = false;
            }
        }
        if (this.insurancecompanyList.length == 1 && this.showLastRow == false) {
            this.listshowLastRowMinus = true;
            this.listshowLastRowPlus = false;
        }
        if (this.insurancecompanyList.length == 1 && this.showLastRow == true) {
            this.listshowLastRowMinus = false;
            this.listshowLastRowPlus = true;

        }
        if (this.insurancecompanyList.length == 0 && this.showLastRow == false) {
            this.showLastRowMinus = false;
            this.showLastRowPlus = true;
        }
        if (this.insurancecompanyList.length == 0 && this.showLastRow == true) {
            this.showLastRowMinus = true;
            this.showLastRowPlus = false;
        }
    }
    showNewRow() {
        this.showLastRow = false;
        this.showIcons();
    }
    
    editFunction() {
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.edit = true;
            this.InsurancecompanyService.getItemsDetail(this.id).subscribe(
                data => {
                    this.insurancecompanyList = data['result'];
                    this.insurancecompanyDataheader['f101fidVendor'] = data['result'][0]['f101fidVendor'];
                    this.insurancecompanyDataheader['f101fproductName'] = data['result'][0]['f101fproductName'];
                    this.showIcons();
                }, error => {
                    console.log(error);
                });
        }
    }

    ngOnInit() {

        this.viewDisabled = false;
        this.ajaxCount = 0;
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.editFunction();
        if (this.id < 1) {
            this.showLastRow = false;
            this.showLastRowMinus = false;
            this.showLastRowPlus = true;
        } else {
            this.showLastRow = true;

        }
        this.ajaxCount++;
        if (this.id > 0) {
            this.SupplierregistrationService.getActiveItems().subscribe(
                data => {
                    this.ajaxCount--;
                    this.supplierList = data['result']['data'];
                }, error => {

                });
        }
        else {
            this.SupplierregistrationService.getUndefinedVendorCompanyInsurance().subscribe(
                data => {
                    this.ajaxCount--;
                    this.supplierList = data['result'];
                }, error => {

                });
        }

        this.showIcons();
    }
    ConvertToFloat(val) {
        return parseFloat(val);
    }
    saveInsurancecompany() {
        if (this.showLastRow == false) {
            var addactivities = this.addinsurancecompanylist();
            if (addactivities == false) {
                return false;
            }

        }
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        if(this.insurancecompanyDataheader['f101fidVendor'] == undefined){
            alert('Select Vendor');
            return false;
        }
        if(this.insurancecompanyDataheader['f101fproductName'] == undefined){
            alert('Enter Product Name');
            return false;
        }
        let InsurancecompanyObject = {};
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            InsurancecompanyObject['f101fid'] = this.id;
        }
        InsurancecompanyObject['f101fidVendor'] = this.insurancecompanyDataheader['f101fidVendor'];
        InsurancecompanyObject['f101fproductName'] = this.insurancecompanyDataheader['f101fproductName'];
        InsurancecompanyObject['f101fstatus'] = 0;

        InsurancecompanyObject['insurance-details'] = this.insurancecompanyList;
        for (var i = 0; i < InsurancecompanyObject['insurance-details'].length; i++) {
            InsurancecompanyObject['insurance-details'][i]['f101fyear1'] = this.ConvertToFloat(InsurancecompanyObject['insurance-details'][i]['f101fyear1']).toFixed(2);
            InsurancecompanyObject['insurance-details'][i]['f101fyear2'] = this.ConvertToFloat(InsurancecompanyObject['insurance-details'][i]['f101fyear2']).toFixed(2);
            InsurancecompanyObject['insurance-details'][i]['f101fyear3'] = this.ConvertToFloat(InsurancecompanyObject['insurance-details'][i]['f101fyear3']).toFixed(2);
            InsurancecompanyObject['insurance-details'][i]['f101fyear4'] = this.ConvertToFloat(InsurancecompanyObject['insurance-details'][i]['f101fyear4']).toFixed(2);
            InsurancecompanyObject['insurance-details'][i]['f101fyear5'] = this.ConvertToFloat(InsurancecompanyObject['insurance-details'][i]['f101fyear5']).toFixed(2);
            InsurancecompanyObject['insurance-details'][i]['f101fyear6'] = this.ConvertToFloat(InsurancecompanyObject['insurance-details'][i]['f101fyear6']).toFixed(2);
            InsurancecompanyObject['insurance-details'][i]['f101fyear7'] = this.ConvertToFloat(InsurancecompanyObject['insurance-details'][i]['f101fyear7']).toFixed(2);
            InsurancecompanyObject['insurance-details'][i]['f101fyear8'] = this.ConvertToFloat(InsurancecompanyObject['insurance-details'][i]['f101fyear8']).toFixed(2);
            InsurancecompanyObject['insurance-details'][i]['f101fyear9'] = this.ConvertToFloat(InsurancecompanyObject['insurance-details'][i]['f101fyear9']).toFixed(2);
            InsurancecompanyObject['insurance-details'][i]['f101fyear10'] = this.ConvertToFloat(InsurancecompanyObject['insurance-details'][i]['f101fyear10']).toFixed(2);
        }
        if (this.id > 0) {
            this.InsurancecompanyService.updateInsurancecompanyItems(InsurancecompanyObject, this.id).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert('Cash Advance ID Already Exist');
                    }
                    else {
                        alert('Insurance Company Setup has been Updated Successfully');
                        this.router.navigate(['loan/insurancecompany']);
                    }
                }, error => {
                });
        }
        else {
            this.InsurancecompanyService.insertInsurancecompanyItems(InsurancecompanyObject).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert('Cash Advance ID Already Exist');
                    }
                    else {
                        alert('Insurance Company Setup has been Added Successfully');
                        this.router.navigate(['loan/insurancecompany']);
                    }

                }, error => {
                });
        }
    }

    deleteinsurancecompany(object) {
        var cnf = confirm("Do you really want to delete");
        if (cnf == true) {
            var index = this.insurancecompanyList.indexOf(object);
            if (index > -1) {
                this.insurancecompanyList.splice(index, 1);
            }
            this.showIcons();
        }
    }

    addinsurancecompanylist() {

        if (this.insurancecompanyDataheader['f101fidVendor'] == undefined) {
            alert(" Select Vendor");
            return false;
        }
        if (this.insurancecompanyDataheader['f101fproductName'] == undefined) {
            alert(" Select Product Name");
            return false;
        }
        if (this.insurancecompanyData['f101fage'] == undefined) {
            alert(" Enter Year 1");
            return false;
        }
        if (this.insurancecompanyData['f101fyear1'] == undefined) {
            alert(" Enter Year 1");
            return false;
        }
        if (this.insurancecompanyData['f101fyear2'] == undefined) {
            alert(" Enter Year 2");
            return false;
        }
        if (this.insurancecompanyData['f101fyear3'] == undefined) {
            alert(" Enter Year 3");
            return false;
        }
        if (this.insurancecompanyData['f101fyear4'] == undefined) {
            alert(" Enter Year 4");
            return false;
        }
        if (this.insurancecompanyData['f101fyear5'] == undefined) {
            alert(" Enter Year 5");
            return false;
        }
        if (this.insurancecompanyData['f101fyear6'] == undefined) {
            alert(" Enter Year 6");
            return false;
        }
        if (this.insurancecompanyData['f101fyear7'] == undefined) {
            alert(" Enter Year 7");
            return false;
        }
        if (this.insurancecompanyData['f101fyear8'] == undefined) {
            alert(" Enter Year 8");
            return false;
        }
        if (this.insurancecompanyData['f101fyear9'] == undefined) {
            alert(" Enter Year 9");
            return false;
        }
        if (this.insurancecompanyData['f101fyear10'] == undefined) {
            alert(" Enter Year 10");
            return false;
        }
        var confirmPop = confirm("Do you want to Add?");
        if (confirmPop == false) {
            return false;
        }
        var dataofCurrentRow = this.insurancecompanyData;
        this.insurancecompanyData = {};
        this.insurancecompanyList.push(dataofCurrentRow);
        this.showIcons();
    }
    onlyNumberKey(event) {
        if (event.charCode == 46) {

        } else {
            return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
        }
    }
    maximize() {
        $("#myModal").addClass("modal");
        $("#overlay1").show();
        $(".maximize-icon").addClass("hidden");
        $(".minimize-icon").removeClass("hidden");
        $(".modal-backdrop").show();

    }
    minimize() {
        $("#myModal").removeClass("modal");
        $("#overlay1").hide();
        $(".maximize-icon").removeClass("hidden");
        $(".minimize-icon").addClass("hidden");
        $(".modal-backdrop").hide();
        $('#myModal').show();

    }
    checkDuplication() {
        let duplicationObj = {};
        if (this.id > 0) {
            duplicationObj['id'] = this.id;
        }
        duplicationObj['key'] = 'Loan Insurance Company';
        duplicationObj['companyName'] = this.insurancecompanyDataheader['f101fproductName'];
        if(this.insurancecompanyDataheader['f101fproductName'] != "" && this.insurancecompanyDataheader['f101fproductName'] != ""){
        this.DefinationmsService.checkDuplication(duplicationObj).subscribe(
            data => {
                if (parseInt(data['result']) == 1) {
                    alert("Loan Type Already Exist");
                    this.insurancecompanyDataheader['f101fproductName'] = '';
                }
            }, error => {
                console.log(error);
            });
        }
    }
}


