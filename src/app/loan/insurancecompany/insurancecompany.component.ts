import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { InsurancecompanyService } from '../service/insurancecompany.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'Insurancecompany',
    templateUrl: 'insurancecompany.component.html'
})
export class InsurancecompanyComponent implements OnInit {
    insurancecompanyList = [];
    insurancecompanyData = {};
    id: number;
    constructor(
        private InsurancecompanyService: InsurancecompanyService,
        private spinner: NgxSpinnerService
    ) { }

    ngOnInit() {
        this.spinner.show();
        this.InsurancecompanyService.getItems().subscribe(
            data => {
                this.spinner.hide();
                this.insurancecompanyList = data['result'];
                console.log(this.insurancecompanyList);
            }, error => {
                console.log(error);
            });
    }
}