import { Injectable } from '@angular/core';
import { HttpClientModule, HttpHeaders, HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({
        'X-Auth-Client': 'qazwsx123',
        'Content-Type': 'application/json'
    })
};

@Injectable()
export class SetupgradeService {
    url: string = environment.api.base + environment.api.endPoints.gradeSetup;
    groupurl: string = environment.api.base + environment.api.endPoints.groupList;
    constructor(private httpClient: HttpClient) {
    }

    getItems() {
        return this.httpClient.get(this.url, httpOptions);
    }


    getItemsDetail(id) {
        return this.httpClient.get(this.url + '/' + id, httpOptions);
    }
    getGroups() {
        return this.httpClient.get(this.groupurl, httpOptions);
    }

    insertSetupgradeItems(setupgradeData): Observable<any> {
        return this.httpClient.post(this.url, setupgradeData, httpOptions);
    }

    updateSetupgradeItems(setupgradeData, id): Observable<any> {
        return this.httpClient.put(this.url + '/' + id, setupgradeData, httpOptions);
    }
}