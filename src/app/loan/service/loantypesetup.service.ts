import { Injectable } from '@angular/core';
import { HttpClientModule,HttpHeaders, HttpClient} from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({ 'X-Auth-Client': 'qazwsx123',
    'Content-Type': 'application/json' })
  };

@Injectable()
export class LoantypesetupService {
    url: string = environment.api.base + environment.api.endPoints.loanTypeSetup;
    getListByLoanNameurl: string = environment.api.base + environment.api.endPoints.getloannamedetails;
    staffurl: string = environment.api.base + environment.api.endPoints.staff;

    constructor(private httpClient: HttpClient) {    
    }
    
    getItems() {
        return this.httpClient.get(this.url,httpOptions);
    }

    getItemsDetail(id) {
        return this.httpClient.get(this.url+'/'+id,httpOptions);
    }

    getStaffMaster(id) {
        return this.httpClient.get(this.staffurl+'/'+id,httpOptions);

    }

    getloantypeDetails(id){
        return this.httpClient.get(this.getListByLoanNameurl+'/'+id,httpOptions);

    }
    insertLoantypesetupItems(loannameData): Observable<any> {
        return this.httpClient.post(this.url, loannameData,httpOptions);
    }

    updateLoantypesetupItems(loannameData,id): Observable<any> {
        return this.httpClient.put(this.url+'/'+id, loannameData,httpOptions);
       }
}