import { Injectable } from '@angular/core';
import { HttpClientModule, HttpHeaders, HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({
        'X-Auth-Client': 'qazwsx123',
        'Content-Type': 'application/json'
    })
};
 
@Injectable()
export class LoannameService {
    url: string = environment.api.base + environment.api.endPoints.loanName;
    ActiveListurl: string = environment.api.base + environment.api.endPoints.Activeloanname;
    undefinedloanprocessingfeeurl: string = environment.api.base + environment.api.endPoints.undefinedloanprocessingfee;
    undefinedLoanParameterurl: string = environment.api.base + environment.api.endPoints.undefinedloanparameter;
    undefinedloangradeurl: string = environment.api.base + environment.api.endPoints.undefinedloangrade;
    loanpaymenturl: string = environment.api.base + environment.api.endPoints.loanpayments;

    constructor(private httpClient: HttpClient) {
    }

    getItems() {
        return this.httpClient.get(this.url, httpOptions);
    }
    getActiveItems() {
        return this.httpClient.get(this.ActiveListurl, httpOptions);
    }
    getUndefinedLoanProcessingFee() {
        return this.httpClient.get(this.undefinedloanprocessingfeeurl, httpOptions);
    }
    getUndefinedLoanParameters() {
        return this.httpClient.get(this.undefinedLoanParameterurl, httpOptions);
    }
    getUndefinedLoanGrades() {
        return this.httpClient.get(this.undefinedloangradeurl, httpOptions);
    }

    getItemsDetail(id) {
        return this.httpClient.get(this.url + '/' + id, httpOptions);
    }

    insertLoannameItems(loannameData): Observable<any> {
        return this.httpClient.post(this.url, loannameData, httpOptions);
    }

    updateLoannameItems(loannameData, id): Observable<any> {
        return this.httpClient.put(this.url + '/' + id, loannameData, httpOptions);
    }
    getLoanPayments() {
        return this.httpClient.get(this.loanpaymenturl, httpOptions);
    }
    insertLoanPayment(data): Observable<any> {
        return this.httpClient.post(this.loanpaymenturl, data, httpOptions);
    }
}