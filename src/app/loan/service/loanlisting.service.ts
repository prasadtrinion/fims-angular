import { Injectable } from '@angular/core';
import { HttpClientModule,HttpHeaders, HttpClient} from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({ 'X-Auth-Client': 'qazwsx123',
    'Content-Type': 'application/json' })
    
  };
@Injectable()

export class LoanlistingService {

     url: string = environment.api.base + environment.api.endPoints.loanlisting
     approvalPuturl: string = environment.api.base + environment.api.endPoints.processLoan
    //  approveoneurl: string = environment.api.base + environment.api.endPoints.rejectOne
    constructor(private httpClient: HttpClient) {    
    }
    getItems() {
        return this.httpClient.get(this.url+'/'+2,httpOptions);
    }
    getItemsDetail() {
        return this.httpClient.get(this.url, httpOptions);
    }
    insertLoanlistingItems(loanlistingData): Observable<any> {
        return this.httpClient.post(this.url, loanlistingData,httpOptions);
    }
    updateLoanlistingItems(loanlistingData): Observable<any> {
        return this.httpClient.post(this.url, loanlistingData,httpOptions);
    }
   
}
