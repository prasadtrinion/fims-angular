import { Injectable } from '@angular/core';
import { HttpClientModule,HttpHeaders, HttpClient} from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({ 'X-Auth-Client': 'qazwsx123',
    'Content-Type': 'application/json' })
    
  };
@Injectable()

export class ApplyloanapprovalService {

     url: string = environment.api.base + environment.api.endPoints.loanApprovalList
     approveVehicleLoanurl: string = environment.api.base + environment.api.endPoints.approveVehicleLoan
     vehicleLoanListurl: string = environment.api.base + environment.api.endPoints.getLoanGurantors
     approveLoanGurantorurl: string = environment.api.base+ environment.api.endPoints.approveLoanGurantor
    constructor(private httpClient: HttpClient) {    
    }
    getItems() {
        return this.httpClient.get(this.url+'/'+2,httpOptions);
    }
    getItemsDetail(id,status) {
        return this.httpClient.get(this.url+'/'+id+'/'+status, httpOptions);
    }



    getPeronalList(id,status){
        return this.httpClient.get(this.vehicleLoanListurl, httpOptions);
        
    }
    
    updateApplyloanapprovalItems(applyloanapprovalData): Observable<any> {
        return this.httpClient.post(this.approveVehicleLoanurl, applyloanapprovalData,httpOptions);
    }
    updateApplyloanapprovalItemsByUser(ApprovaloneData): Observable<any> {
        return this.httpClient.post(this.approveLoanGurantorurl, ApprovaloneData,httpOptions);
    }
}

// getItems() {
//     return this.httpClient.get(this.url+'/'+2,httpOptions);
// }
// getItemsDetail(id) {
//     return this.httpClient.get(this.url+'/'+id, httpOptions);
// }
// insertApplyloanapprovalItems(applyloanapprovalData): Observable<any> {
//     return this.httpClient.post(this.url, applyloanapprovalData,httpOptions);
// }
// updateApplyloanapprovalItems(applyloanapprovalData): Observable<any> {
//     return this.httpClient.post(this.approvalPuturl, applyloanapprovalData,httpOptions);
// }