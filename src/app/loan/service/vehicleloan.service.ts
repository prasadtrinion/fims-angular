import { Injectable } from '@angular/core';
import { HttpClientModule,HttpHeaders, HttpClient} from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({ 'X-Auth-Client': 'qazwsx123',
    'Content-Type': 'application/json' })
    
  };
  
@Injectable()

export class VehicleloanService {
    url: string = environment.api.base + environment.api.endPoints.VehicleLoan;
    urlGuarantorList : string = environment.api.base + environment.api.endPoints.GuarantorList;
    getCreditnoteNumberUrl : string = environment.api.base + environment.api.endPoints.generateNumber;
    urlgetLoanGrades : string = environment.api.base + environment.api.endPoints.getLoanGrades;
    smartphone : string = environment.api.base+environment.api.endPoints.smartPhoneLoan;
    vehicle : string = environment.api.base+environment.api.endPoints.vehicleLoanReport;
    insuranceAmturl : string = environment.api.base+environment.api.endPoints.getInsuranceValue;
    employeeEarningSalary : string = environment.api.base+environment.api.endPoints.employeeEarningSalary;
    noneligibleUrl : string = environment.api.base+environment.api.endPoints.noneligiblegurantors;
    staffloansUrl : string = environment.api.base+environment.api.endPoints.staffloans;
    loanamountUrl : string = environment.api.base+environment.api.endPoints.loanamount;


    constructor(private httpClient: HttpClient) { 
        
    }

    getEmployeeSalary(data) {
        //return this.httpClient.get(this.url+'/'+id,httpOptions);
        return this.httpClient.post(this.employeeEarningSalary, data,httpOptions);

    }

    getInsruanceAmount(data) {
        return this.httpClient.post(this.insuranceAmturl, data,httpOptions);

    }

    getVehicleLoanGradeAmountDetails(data) {
        return this.httpClient.post(this.urlgetLoanGrades, data,httpOptions);

    }
    
    getNumber(data) {
        return this.httpClient.post(this.getCreditnoteNumberUrl, data,httpOptions);

    }
    checkNumberOfGuarantee(data) {
        return this.httpClient.post(this.urlGuarantorList, data,httpOptions);

    }
    downloadVehicleReport(invoiceData): Observable<any> {
        return this.httpClient.post(this.vehicle, invoiceData,httpOptions);
    }
    downloadComputerReport(invoiceData): Observable<any> {
        return this.httpClient.post(this.smartphone, invoiceData,httpOptions);
    }

    getItems() {
        return this.httpClient.get(this.url,httpOptions);
    }
    getNonEligibleGurantors() {
        return this.httpClient.get(this.noneligibleUrl,httpOptions);
    }

    getItemsDetail(id) {
        return this.httpClient.get(this.url+'/'+id,httpOptions);
    }
    getLoanAmount(id) {
        return this.httpClient.get(this.loanamountUrl+'/'+id,httpOptions);
    }
    getEmployeeLoans(id) {
        return this.httpClient.get(this.staffloansUrl+'/'+id,httpOptions);
    }

    insertVehicleloanItems(vehicleloanData): Observable<any> {
        return this.httpClient.post(this.url, vehicleloanData,httpOptions);
    }

    updateVehicleloanItems(vehicleloanData,id): Observable<any> {
        return this.httpClient.put(this.url+'/'+id, vehicleloanData,httpOptions);
       }
}