import { Injectable } from '@angular/core';
import { HttpClientModule,HttpHeaders, HttpClient} from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
const httpOptions = {
    headers: new HttpHeaders({ 'X-Auth-Client': 'qazwsx123',
    'Content-Type': 'application/json' })
  };

@Injectable()
export class StaffService {
    url: string = environment.api.base + environment.api.endPoints.staff;
    loanstaffurl: string = environment.api.base + environment.api.endPoints.loanstaff;
    url2: string = environment.api.base + environment.api.endPoints.staff2;
    url3: string = environment.api.base + environment.api.endPoints.allstaff;
    url1: string = environment.api.base + environment.api.endPoints.nonBlackList;
    activeListUrl: string = environment.api.base + environment.api.endPoints.ActiveStaff; 
    emailUrl: string = environment.api.base + environment.api.endPoints.getemployeeemail; 
    constructor(private httpClient: HttpClient) {    
    }
    
    getItems() {
        return this.httpClient.get(this.url,httpOptions);
    }
    getLoanStaff() {
        return this.httpClient.get(this.loanstaffurl,httpOptions);
    }
    getAllItems() {
        return this.httpClient.get(this.url3,httpOptions);
    }
    getNewItems() {
        return this.httpClient.get(this.url2,httpOptions);
    }
    getNonBlackList() {
        return this.httpClient.get(this.url1,httpOptions);
    }

    getItemsDetail(id) {
        return this.httpClient.get(this.url+'/'+id,httpOptions);
    }
    getEmail(id) {
        return this.httpClient.get(this.emailUrl+'/'+id,httpOptions);
    }

    getActiveList() {
        return this.httpClient.get(this.activeListUrl,httpOptions);

    }

    insertstaffItems(staffData): Observable<any> {
        return this.httpClient.post(this.url, staffData,httpOptions);
    }

    updatestaffItems(staffData,id): Observable<any> {
        return this.httpClient.put(this.url+'/'+id, staffData,httpOptions);
       }
}