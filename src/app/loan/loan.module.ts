import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../material/material.module';
import { Ng4LoadingSpinnerModule } from 'ng4-loading-spinner';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { DataTableModule } from "angular-6-datatable";

import { HttpClientModule, HttpClient, HTTP_INTERCEPTORS } from '@angular/common/http';

import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { LoanRoutingModule } from './loan.router.module';

import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { LoannameComponent } from "./loanname/loanname.component";
import { LoannameFormComponent } from "./loanname/loannameform/loannameform.component";
import { LoannameService } from "./service/loanname.service";
import { NguiAutoCompleteModule } from '@ngui/auto-complete';

import { ApplyloanService } from "./service/applyloan.service";
import { LoanperiodComponent } from "./loanperiod/loanperiod.component";
import { ProcessingFeeComponent } from "./processingfee/processingfee.component";

import { ApplyloanComponent } from "./applyloan/applyloan.component";
import { ApplyloanFormComponent } from "./applyloan/applyloanform/applyloanform.component";
import { ApplyloanapprovalComponent } from "./applyloanapproval/applyloanapproval.component";
import { ApplyloanapprovalService } from "./service/applyloanapproval.service";

import { BlackListEmpComponent } from "./blacklistemp/blacklistemp.component";
import { BlackListEmpFormComponent } from "./blacklistemp/blacklistempform/blacklistempform.component";
import { BlacklisttempService } from "./service/blacklisttemp.service";
import { CustomerService } from '../accountsrecivable/service/customer.service';

import { LoancancellationComponent } from "./loancancellation/loancancellation.component";
import { LoancancellationFormComponent } from "./loancancellation/loancancellationform/loancancellationform.component";
import { LoancancellationService } from "./service/loancancellation.service";
import { InsurancesetupComponent } from './insurancesetup/insurancesetup.component';
import { InsurancesetupFormComponent } from './insurancesetup/insurancesetupform/insurancesetupform.component';
import { InsurancesetupService } from './service/insurancesetup.service';
import { VehicletypeComponent } from "./vehicletype/vehicletype.component";
import { VehicletypeFormComponent } from "./vehicletype/vehicletypeform/vehicletypeform.component";
import { VehicletypeService } from "./service/vehicletype.service";

import { LoanperiodFormComponent } from "./loanperiod/loanperiodform/loanperiodform.component";
import { ProcessingFeeFormComponent } from "./processingfee/processingfeeform/processingfeeform.component";

import { StaffService } from "./service/staff.service";
import { LoanperiodService } from "./service/loanperiod.service";
import { ProcessingFeeService } from "./service/processingfee.service";
import { JwtInterceptor } from '../_helpers/jwt.interceptors';
import { from } from 'rxjs';

import { SmartloanComponent } from "./smartloan/smartloan.component";
import { SmartloanFormComponent } from "./smartloan/smartloanform/smartloanform.component";
import { SmartloanService } from "./service/smartloan.service";

import { VehicleloanComponent } from "./vehicleloan/vehicleloan.component";
import { VehicleloanFormComponent } from "./vehicleloan/vehicleloanform/vehicleloanform.component";
import { VehicleloanService } from "./service/vehicleloan.service";
import { SupplierregistrationService } from "./../procurement/service/supplierregistration.service";
import { VehicleloanupdateFormComponent } from "./vehicleloan/vehicleloanupdate/vehicleloanupdateform.component";

import { SetupgradeComponent } from "./setupgrade/setupgrade.component";
import { SetupgradeFormComponent } from "./setupgrade/setupgradeform/setupgradeform.component";
import { SetupgradeService } from "./service/setupgrade.service";
import { SearchapplicationService } from './service/searchapplication.service';
import { SearchapplicationComponent } from "./searchapplication/searchapplication.component";
import { ApprovalComponent } from "./approval/approval.component";
import { ApprovalService } from "./service/approval.service";

import { LoantypesetupComponent } from "./loantypesetup/loantypesetup.component";
import { LoantypesetupFormComponent } from "./loantypesetup/loantypesetupform/loantypesetupform.component";
import { LoantypesetupService } from "./service/loantypesetup.service";

import { LoanofferComponent } from "./loanoffer/loanoffer.component";
import { LoanofferService } from "./service/loanoffer.service";

import { LoanprocessComponent } from "./loanprocess/loanprocess.component";
import { LoanprocessService } from "./service/loanprocess.service";

import { EmpreleaselistComponent } from "./empreleaselist/empreleaselist.component";
import { EmpreleaselistFormComponent } from "./empreleaselist/empreleaselistform/empreleaselistform.component";
import { EmpreleastlistService } from "./service/empreleaslist.service";

import { InsurancecompanyComponent } from "./insurancecompany/insurancecompany.component";
import { InsurancecompanyFormComponent } from "./insurancecompany/insurancecompanyform/insurancecompanyform.component";
import { InsurancecompanyService } from "./service/insurancecompany.service"

import { LoanlistingComponent } from "./loanlisting/loanlisting.component";
import { LoanlistingService } from "./service/loanlisting.service";

import { LoanhistoryComponent } from "./loanhistory/loanhistory.component";
import { LoanhistoryService } from "./service/loanhistory.service";

import { LoandegradationComponent } from "./loandegradation/loandegradation.component";
import { LoandegradationService } from "./service/loandegradation.service";


import { LoanreportService } from "./service/loanreport.service";
import { LoanreportComponent } from "./loanreport/loanreport.component";

import { LoanProcessingFeeService } from "./service/loanprocessingfee.service";
import { LoanProcessingFeeComponent } from "./loanprocessingfee/loanprocessingfee.component";
import { LoanProcessingFeeFormComponent } from "./loanprocessingfee/loanprocessingfeeform/loanprocessingfeeform.component";

import { NgSelectModule } from '@ng-select/ng-select';
import { VehicleloaneditFormComponent } from "./vehicleloan/vehicleloanedit/vehicleloaneditform.component";
import { SharedModule } from "../shared/shared.module";

import { LoanPaymentsComponent } from "./loanpayments/loanpayments.component";
import { LoanPaymentsFormComponent } from "./loanpayments/loanpaymentsform/loanpaymentsform.component";
@NgModule({
    imports: [
        CommonModule,
        DataTableModule,
        FormsModule,
        ReactiveFormsModule,
        MaterialModule,
        LoanRoutingModule,
        HttpClientModule,
        TabsModule.forRoot(),
        Ng2SearchPipeModule,
        HttpClientModule,
        FontAwesomeModule,
        NguiAutoCompleteModule,
        NgSelectModule,
        SharedModule
    ],
    exports: [],
    declarations: [
        VehicleloanupdateFormComponent,
        LoannameComponent,
        LoannameFormComponent,
        LoanperiodComponent,
        ProcessingFeeComponent,
        LoanperiodFormComponent,
        ProcessingFeeFormComponent,
        ApplyloanComponent,
        ApplyloanFormComponent,
        ApplyloanapprovalComponent,
        VehicleloaneditFormComponent,
        BlackListEmpComponent,
        BlackListEmpFormComponent,
        LoancancellationComponent,
        LoancancellationFormComponent,
        VehicletypeComponent,
        VehicletypeFormComponent,
        SmartloanComponent,
        SmartloanFormComponent,
        VehicleloanComponent,
        VehicleloanFormComponent,
        ApprovalComponent,
        SetupgradeComponent,
        SetupgradeFormComponent,
        LoanreportComponent,
        LoantypesetupFormComponent,
        LoantypesetupComponent,
        LoanofferComponent,
        EmpreleaselistComponent,
        EmpreleaselistFormComponent,
        SearchapplicationComponent,
        LoanprocessComponent,
        InsurancecompanyComponent,
        InsurancesetupComponent,
        InsurancesetupFormComponent,
        InsurancecompanyFormComponent,
        LoanlistingComponent,
        LoanhistoryComponent,
        LoandegradationComponent,
        LoanProcessingFeeComponent,
        LoanProcessingFeeFormComponent,
        LoanPaymentsComponent,
        LoanPaymentsFormComponent
    ],
    providers: [
        LoannameService,
        LoanperiodService,
        ProcessingFeeService,
        LoancancellationService,
        CustomerService,
        BlacklisttempService,
        StaffService,
        VehicletypeService,
        ApplyloanapprovalService,
        SmartloanService,
        SupplierregistrationService,
        ApplyloanService,
        VehicleloanService,
        ApprovalService,
        LoanreportService,
        SetupgradeService,
        InsurancesetupService,
        LoantypesetupService,
        SearchapplicationService,
        LoanofferService,
        EmpreleastlistService,
        LoanprocessService,
        InsurancecompanyService,
        LoanhistoryService,
        LoandegradationService,
        LoanProcessingFeeService,
        LoanlistingService,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: JwtInterceptor,
            multi: true
        }

    ],
})
export class LoanModule { }
