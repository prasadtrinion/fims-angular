import { Injector } from '@angular/core';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApplyloanService } from '../../service/applyloan.service'
import { StaffService } from '../../service/staff.service'
import { LoannameService } from '../../service/loanname.service';
import { AccountcodeService } from '../../../generalsetup/service/accountcode.service';
import { InsurancesetupService } from '../../service/insurancesetup.service';


import { FormGroup, FormBuilder, Validators,FormControl  } from '@angular/forms';

@Component({
    selector: 'ApplyloanFormComponent',
    templateUrl: 'applyloanform.component.html'
})

export class ApplyloanFormComponent implements OnInit {
    applyloanform: FormGroup; 
    applyloanList = [];
    applyloanData = {};
    applyloanDataheader = {};
    accountcodeList = [];
    staffList=[];
    recevierList=[];
    loannameList=[];
    id: number;
    ajaxCount:number;
    insurancesetupList = [];
    constructor(
        private ApplyloanService: ApplyloanService,
        private AccountcodeService:AccountcodeService,
        private StaffService:StaffService,
        private LoannameService:LoannameService,
        private InsurancesetupService:InsurancesetupService,
        private route: ActivatedRoute,
        private router: Router,
    ) { }

    ngDoCheck() {
        const change = this.ajaxCount;
        console.log(this.ajaxCount);
        if (this.ajaxCount == 0) {
            this.editFunction();
            this.ajaxCount = 10;
        }
    }
    editFunction(){
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if(this.id>0) {
            this.ApplyloanService.getItemsDetail(this.id).subscribe(
                data => {
                    // this.applyloanList = data['result'];
                    this.applyloanDataheader['f045fidName'] = data['result'][0]['f045fidName'];
                    this.applyloanDataheader['f045fdescription'] = data['result'][0]['f045fdescription'];
                    this.applyloanDataheader['f045ffile'] = data['result'][0]['f045ffile'];
                    this.applyloanDataheader['f045fnoOfMonths'] = data['result'][0]['f045fnoOfMonths'];
                    this.applyloanDataheader['f045fidStaff'] = data['result'][0]['f045fidStaff'];
                    this.applyloanDataheader['f045fguarantee'] = data['result'][0]['f045fguarantee'];
                    this.applyloanList = data['result'];
                }, error => {
                    console.log(error);
                });
        }
    }
    ngOnInit() {
        this.applyloanDataheader['f045fidName'] = '';
        this.applyloanDataheader['f045fidStaff'] = '';
        this.applyloanData['f045faccountCode'] = '';
        
        this.ajaxCount = 0;

        // this.applyloanData['f089fidCategory'] = '';
        this.ajaxCount++;
        this.InsurancesetupService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.insurancesetupList = data['result']['data'];
        }, error => {
            console.log(error);
        });
        this.ajaxCount++;
    
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
          // account dropdown
          this.AccountcodeService.getItemsByLevel().subscribe(
            data => {
                this.ajaxCount--;
                this.accountcodeList = data['result']['data'];
        }, error => {
            console.log(error);
        });

       // Staff dropdown
       this.ajaxCount++;
        this.StaffService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.staffList = data['result']['data'];
            }, error => {
                console.log(error);
            });

             //  Customer
        this.ajaxCount++;
        this.LoannameService.getActiveItems().subscribe(
            data => {
                this.ajaxCount--;
                this.loannameList = data['result']['data'];
            }, error => {
                console.log(error);
            });
    }
    saveApplyloan() {

        let applyloanObject = {};
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        console.log(this.id);
        if (this.id > 0) {
            applyloanObject['f045fid'] = this.id;
        }
        applyloanObject['f045fidName'] = this.applyloanDataheader['f045fidName'];
        applyloanObject['f045fdescription'] = this.applyloanDataheader['f045fdescription'];
        applyloanObject['f045ffile'] = this.applyloanDataheader['f045ffile'];
        applyloanObject['f045fnoOfMonths'] = this.applyloanDataheader['f045fnoOfMonths'];
        applyloanObject['f045fidStaff'] = this.applyloanDataheader['f045fidStaff'];
        applyloanObject['f045fguarantee'] = this.applyloanDataheader['f045fguarantee'];
        applyloanObject['f045fstatus'] = 0;
        applyloanObject['loan-details'] = this.applyloanList;
        console.log(applyloanObject);

        if (this.id > 0) {
            this.ApplyloanService.updateApplyloanItems(applyloanObject, this.id).subscribe(
                data => {

                    this.router.navigate(['loan/applyloan']);
                    // this.AlertService.success(" Updated Sucessfully ! ");
                }, error => {
                    console.log(error);
                });
        } else {
            this.ApplyloanService.insertApplyloanItems(applyloanObject).subscribe(
                data => {
                    this.router.navigate(['loan/applyloan']);
                    // this.AlertService.success(" Added Sucessfully ! ");

                }, error => {
                    console.log(error);
                });
        }
    }
    deleteapplyloan(object) {
        var cnf = confirm("Do you really want to delete");
        if (cnf == true) {
            var index = this.applyloanList.indexOf(object);
            if (index > -1) {
                this.applyloanList.splice(index, 1);
            }
        }
    }
    addapplyloanlist() {
        console.log(this.applyloanData);
        var dataofCurrentRow = this.applyloanData;
        this.applyloanData = {};
        this.applyloanList.push(dataofCurrentRow);
        console.log(this.applyloanList);
    }
    
}


