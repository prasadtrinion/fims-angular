import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ApplyloanService } from '../service/applyloan.service'
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'Applyloan',
    templateUrl: 'applyloan.component.html'
})
export class ApplyloanComponent implements OnInit {
    applyloanList = [];
    applyloanData = {};
    id: number;
    constructor(
        private ApplyloanService: ApplyloanService,
        private spinner: NgxSpinnerService
    ) { }

    ngOnInit() {
        this.spinner.show();
        this.ApplyloanService.getItems().subscribe(
            data => {
                this.spinner.hide();
                this.applyloanList = data['result']['data'];
            }, error => {
                console.log(error);
            });
    }
}