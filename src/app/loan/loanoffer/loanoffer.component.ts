import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LoanofferService } from "../service/loanoffer.service";

@Component({
    selector: 'Loanoffer',
    templateUrl: 'loanoffer.component.html'
})
export class LoanofferComponent implements OnInit {
    loanofferList = [];
    loanofferData = {};
    id: number;
    constructor(
        private LoanofferService: LoanofferService
       
    ) { }

    ngOnInit() {

        this.LoanofferService.getItems().subscribe(
            data => {
                this.loanofferList = data['result']['data'];
            }, error => {
                console.log(error);
            });
    }

     
}