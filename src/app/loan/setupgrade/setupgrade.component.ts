
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SetupgradeService } from '../service/setupgrade.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'Setupgrade',
    templateUrl: 'setupgrade.component.html'
})
export class SetupgradeComponent implements OnInit {
    setupgradeList = [];
    setupgradeData = {};
    id: number;
    constructor(
        private SetupgradeService: SetupgradeService,
        private spinner: NgxSpinnerService
    ) { }

    ngOnInit() {
        this.spinner.show();
        this.SetupgradeService.getItems().subscribe(
            data => {
                this.spinner.hide();
                this.setupgradeList = data['result']['data'];
                
            }, error => {
                console.log(error);
            });
    }
}

