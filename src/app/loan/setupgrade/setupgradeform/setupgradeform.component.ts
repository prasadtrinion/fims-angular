import { Injector } from '@angular/core';
import { Component, OnInit, ViewChild } from '@angular/core';
import { SetupgradeService } from '../../service/setupgrade.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AlertService } from './../../../_services/alert.service';
import { LoannameService } from '../../service/loanname.service';


@Component({
    selector: 'SetupgradeFormComponent',
    templateUrl: 'setupgradeform.component.html'
})

export class SetupgradeFormComponent implements OnInit {
    cashadvancesform: FormGroup;
    setupgradeList = [];
    groupList = [];
    setupgradeData = {};
    loanList = [];
    groupListNew = [];
    groupListadd = [];
    fvehicle = [];
    vehicleConditionDiv: boolean;
    edit: boolean = false;
    id: number;
    constructor(
        private SetupgradeService: SetupgradeService,
        private LoannameService: LoannameService,
        private route: ActivatedRoute,
        private router: Router,
        private AlertService: AlertService
    ) { }
    ngOnInit() {

        let vehicletypeobj = {};
        vehicletypeobj['id'] = 1;
        vehicletypeobj['name'] = 'New';
        this.fvehicle.push(vehicletypeobj);
        vehicletypeobj = {};
        vehicletypeobj['id'] = 2;
        vehicletypeobj['name'] = 'Used';
        this.fvehicle.push(vehicletypeobj);
        this.setupgradeData['f046fstatus'] = 1;
        this.cashadvancesform = new FormGroup({
            loanname: new FormControl('', Validators.required),
        });
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        // if (this.id > 0) {
            this.LoannameService.getActiveItems().subscribe(
                data => {
                    this.loanList = data['result']['data'];
                }, error => {
                    console.log(error);
                });
        // }
        // else {
        //     this.LoannameService.getUndefinedLoanGrades().subscribe(
        //         data => {
        //             this.loanList = data['result'];
        //         }, error => {
        //             console.log(error);
        //         });

        // }

        this.SetupgradeService.getGroups().subscribe(
            data => {
                this.groupList = data['result']['data'];

            }, error => {
                console.log(error);
            });
        if (this.id > 0) {
            this.edit = true;
            this.SetupgradeService.getItemsDetail(this.id).subscribe(
                data => {
                    this.setupgradeData = data['result'][0];
                    this.setupgradeData['f046fstatus'] = parseInt(this.setupgradeData['f046fstatus']);
                    if (this.setupgradeData['f046fgradeName'] == '0') {
                        this.setupgradeData['f046fgradeName'] = 'All';
                    }
                    this.hideshowvc();
                }, error => {
                    console.log(error);
                });
        }
        console.log(this.id);
    }

    hideshowvc() {
        console.log(this.setupgradeData['f046floanType']);
        console.log(this.loanList);
        for (var i = 0; i < this.loanList.length; i++) {
            if (this.loanList[i]['f020fid'] == this.setupgradeData['f046floanType']) {
                if (this.loanList[i]['f020ftypeOfLoan'] == 'Vehicle Loan') {
                    this.vehicleConditionDiv = true;
                } else {
                    this.vehicleConditionDiv = false;
                }
            }
        }
    }
    addSetupgrade() {
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        console.log(this.setupgradeData['f046fgradeFrom']);

        if (parseInt(this.setupgradeData['f046fgradeFrom']) > parseInt(this.setupgradeData['f046fgradeTo'])) {
            alert("Loan maximum limit cannot less than loan minimum limit.");
            return false;
        }
        if (parseInt(this.setupgradeData['f046fgradeMonthFrom']) > parseInt(this.setupgradeData['f046fgradeMonthTo'])) {
            alert("Maximum loan period cannot less than minimum loan period.");
            return false;
        }
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.setupgradeData['f046fgradeFrom'] = parseFloat(this.setupgradeData['f046fgradeFrom']).toFixed(2);
        this.setupgradeData['f046fgradeTo'] = parseFloat(this.setupgradeData['f046fgradeTo']).toFixed(2);
        if (this.id > 0) {
            this.SetupgradeService.updateSetupgradeItems(this.setupgradeData, this.id).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert('Loan Description Already Exist');
                    }
                    else {
                        this.router.navigate(['loan/setupgrade']);
                        alert("Loan Grade has been Updated Sucessfully !!")

                    }
                }, error => {
                    console.log(error);
                });

        } else {

            this.SetupgradeService.insertSetupgradeItems(this.setupgradeData).subscribe(
                data => {
                    if (data['status'] == 409) {
                        this.AlertService.error("Grade Name Already Exist");
                    }
                    else {
                        this.router.navigate(['loan/setupgrade']);
                        alert("Loan Grade has been Added Sucessfully !!")

                    }
                    this.router.navigate(['loan/setupgrade']);
                }, error => {
                    console.log(error);
                });
        }
    }
    onlyNumberKey(event) {
        if (event.charCode == 46) {
        } else {
            return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
        }
    }
    checkMonthLimit() {
        if (parseInt(this.setupgradeData['f046fgradeMonthFrom']) < 12) {
            alert('Minimum Loan Period must be above 12 months');
            this.setupgradeData['f046fgradeMonthFrom'] = 0;
            return false;
        }
    }
    checkLoanLimit() {
        if (this.setupgradeData['f046fgradeFrom'] < 500) {
            alert("Loan Minimum Limit cannot less than 500");
            this.setupgradeData['f046fgradeFrom'] = 0;
            return false;
        }
    }
}