
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { LoandegradationService } from "../service/loandegradation.service"
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
@Component({
    selector: 'Loandegradation',
    templateUrl: 'loandegradation.component.html'
})
export class LoandegradationComponent implements OnInit {
    loanDeductionRatioArray = {};

    // loantypesetupData = {};
    id: number;
    constructor(
        
        private route: ActivatedRoute,
        private LoandegradationService: LoandegradationService,
        private spinner: NgxSpinnerService
    ) { }

    ngOnInit() {
        this.spinner.show();
        this.LoandegradationService.getItems().subscribe(
            data => {
                console.log(data);
                this.spinner.hide();
                 this.loanDeductionRatioArray['f131fid'] = data['result'][0]['f131fid'];
                 this.loanDeductionRatioArray['f131fdeductionPercentage'] = data['result'][0]['f131fdeductionPercentage'];
            }, error => {
                console.log(error);
            });
    }
    saveLoanDeductionRatio() {
        console.log(this.loanDeductionRatioArray);
        this.loanDeductionRatioArray['f131fstatus']='1';
        if(this.loanDeductionRatioArray['f131fid']) {
            this.LoandegradationService.updateLoandegradationItems(this.loanDeductionRatioArray).subscribe(
                data => {
                    this.spinner.hide();
                }, error => {
                    console.log(error);
                });
        } else {
            this.LoandegradationService.insertLoandegradationItems(this.loanDeductionRatioArray).subscribe(
                data => {
                    this.spinner.hide();
                }, error => {
                    console.log(error);
                });
        }
        
    }
    
onlyNumberKey(event) {
    if (event.charCode == 46) {

    } else {
        return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
    }
}
}