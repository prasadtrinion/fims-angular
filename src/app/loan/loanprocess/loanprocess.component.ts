import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { LoanprocessService } from "../service/loanprocess.service";
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'Loanprocess',
    templateUrl: 'loanprocess.component.html'
})

export class LoanprocessComponent implements OnInit {

    loanprocessList = [];
    loanprocessData = {};
    selectAllCheckbox = true;

    constructor(
        private LoanprocessService: LoanprocessService,
        private spinner: NgxSpinnerService
    ) { }

    ngOnInit() {
        this.getListData();
    }
    getListData() {
        this.spinner.show();
        this.LoanprocessService.getItemsDetail().subscribe(
            data => {
                this.spinner.hide();
                this.loanprocessList = data['result'];
                for (var i = 0; i < this.loanprocessList.length; i++) {
                    this.loanprocessList[i]['currentMonth'] = new Date();
                }
            }, error => {
                console.log(error);
            });
    }
    LoanprocessListData() {
        var confirmPop = confirm("Do you want to process?");
        if (confirmPop == false) {
            return false;
        }
        var loanprocessIds = [];
        for (var i = 0; i < this.loanprocessList.length; i++) {
            if (this.loanprocessList[i].processStatus == true) {
                loanprocessIds.push(this.loanprocessList[i].f043fid);
            }
        }
        if (loanprocessIds.length > 0) {

        } else {
            alert("Select atleast one Staff to Post Salary Deduction");
            return false;
        }
        var loanprocessUpdate = {};
        loanprocessUpdate['id'] = loanprocessIds;
        this.LoanprocessService.insertLoanprocessItems(loanprocessUpdate).subscribe(
            data => {
                alert("Loan Process has been Processed");

                this.getListData();
            }, error => {
                console.log(error);
            });
    }
}
