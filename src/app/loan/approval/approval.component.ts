
import {Injector} from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { ApprovalService } from '../service/approval.service'
import { AlertService } from './../../_services/alert.service';
@Component({
    selector: 'Approval',
    templateUrl: 'approval.component.html'
})

export class ApprovalComponent implements OnInit {
   
    approvalList =  [];
    approvalData = {};
    selectAllCheckbox = true;

    constructor(
        
        private ApprovalService: ApprovalService,
        private AlertService: AlertService,
    ) { }

    ngOnInit() {    
       this.getListData();
    }
    getListData(){
        this.ApprovalService.getItemsDetail(0).subscribe(
            data => {
                this.approvalList = data['result'];
        }, error => {
            console.log(error);
        });
    }
    
    approvalListData() {
        console.log(this.approvalList);
        var approvaloneIds = [];
        for (var i = 0; i < this.approvalList.length; i++) {
            if(this.approvalList[i].f045fstatus==true) {
                approvaloneIds.push(this.approvalList[i].f045fid);
            }
        }
        var approvaloneUpdate = {};
        approvaloneUpdate['id'] = approvaloneIds;
        this.ApprovalService.updateApprovalItems(approvaloneUpdate).subscribe(
            data => {
                this.AlertService.success(" Updated Sucessfully ! ");

                this.getListData();
        }, error => {
            console.log(error);
        });
    }

    selectAll() {
        console.log("asdf");
        console.log(this.selectAllCheckbox);
        for (var i = 0; i < this.approvalList.length; i++) {
                this.approvalList[i].f045fapprovalStatus = this.selectAllCheckbox;
        }
        console.log(this.approvalList);
      }

    }
