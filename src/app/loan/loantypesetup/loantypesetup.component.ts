
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { LoantypesetupService } from "../service/loantypesetup.service"
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
@Component({
    selector: 'Loantypesetup',
    templateUrl: 'loantypesetup.component.html'
})
export class LoantypesetupComponent implements OnInit {
    loantypesetupList = [];
    // loantypesetupData = {};
    id: number;
    constructor(
        
        private route: ActivatedRoute,
        private LoantypesetupService: LoantypesetupService,
        private spinner: NgxSpinnerService
    ) { }

    ngOnInit() {
        this.spinner.show();
        this.LoantypesetupService.getItems().subscribe(
            data => {
                this.spinner.hide();
                this.loantypesetupList = data['result']['data'];
            }, error => {
                console.log(error);
            });
    }
    
}