import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { LoantypesetupService } from "../../service/loantypesetup.service"
import { LoannameService } from "../../service/loanname.service";
import { ActivatedRoute, Router } from '@angular/router';
import { DefinationmsService } from '../../../studentfinance/service/definationms.service';

@Component({
    selector: 'LoantypesetupFormComponent',
    templateUrl: 'loantypesetupform.component.html'
})
export class LoantypesetupFormComponent implements OnInit {
    loantypesetupList = [];
    loantypesetupData = {};
    loannameList = [];
    id: number;
    empstatusdesignation = [];
    empstatustype = [];
    edit:boolean = false;
    constructor(
        private LoantypesetupService: LoantypesetupService,
        private LoannameService: LoannameService,
        private route: ActivatedRoute,
        private router: Router,
        private DefinationmsService: DefinationmsService,

    ) { }

    ngOnInit() {
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        let paytypeobj = {};
        paytypeobj['id'] = '1';
        paytypeobj['name'] = 'Permanent';
        this.empstatusdesignation.push(paytypeobj);
        paytypeobj = {};
        paytypeobj['id'] = '3';
        paytypeobj['name'] = 'Contract';
        this.empstatusdesignation.push(paytypeobj);

        let loantypeobj = {};
        loantypeobj['id'] = '1';
        loantypeobj['name'] = 'Yes';
        this.empstatustype.push(loantypeobj);
        loantypeobj = {};
        loantypeobj['id'] = '2';
        loantypeobj['name'] = 'No';
        this.empstatustype.push(loantypeobj);

        //this.loantypesetupData['f020fstatus'] = 1;
        // this.loantypesetupData['f044fempType'] = '';

        // this.loantypesetupData['f044fidLoan'] = '';

        // loan name dropdown
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        // if(this.id > 0){

        this.LoannameService.getActiveItems().subscribe(
            data => {
                this.loannameList = data['result']['data'];
            }, error => {
                console.log(error);
            });
        // }
        // else{
        //     this.LoannameService.getUndefinedLoanParameters().subscribe(
        //         data => {
        //             this.loannameList = data['result'];
        //         }, error => {
        //             console.log(error);
        //         });
        // }


        if (this.id > 0) {
            this.edit = true;
            this.LoantypesetupService.getItemsDetail(this.id).subscribe(
                data => {
                    this.loantypesetupData = data['result'][0];
                    // this.blacklistempData['f018fstatus'] = parseInt(this.blacklistempData['f018fstatus']);
                }, error => {
                    console.log(error);
                });
        }
    }
    addLoantypesetup() {
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }

        if (this.loantypesetupData['f044fid'] > 0) {
            this.LoantypesetupService.updateLoantypesetupItems(this.loantypesetupData, this.loantypesetupData['f044fid']).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert('Loan Parameter Already Exist');
                    }
                    else {
                        alert("Loan Parameter has been Updated successfully");
                        this.router.navigate(['loan/loantypesetup']);

                    }

                }, error => {
                    console.log(error);
                });
        } else {
            this.LoantypesetupService.insertLoantypesetupItems(this.loantypesetupData).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert("No duplication of Loan Name");
                    }
                    else {
                        this.router.navigate(['loan/loantypesetup']);
                        alert("Loan Parameter has been added successfully");
                        this.loantypesetupData = {};
                    }
                }, error => {
                    console.log(error);
                });
        }
    }
    onlyNumberKey(event) {
        if (event.charCode == 46) {

        } else {
            return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
        }
    }
    checkDuplication() {
        let duplicationObj = {};
        if (this.id > 0) {
            duplicationObj['id'] = this.id;
        }
        duplicationObj['key'] = 'Loan Parameters';
        duplicationObj['loanType'] = this.loantypesetupData['f044fidLoan'];
        duplicationObj['empStatus'] = this.loantypesetupData['f044fempStatus'];
        duplicationObj['empType'] = this.loantypesetupData['f044fempType'];
        if(this.loantypesetupData['f044fidLoan'] != undefined && this.loantypesetupData['f044fidLoan'] != "" && this.loantypesetupData['f044fempStatus'] != undefined && this.loantypesetupData['f044fempStatus'] != "" && this.loantypesetupData['f044fempType'] != undefined && this.loantypesetupData['f044fempType'] != ""){
        this.DefinationmsService.checkDuplication(duplicationObj).subscribe(
            data => {
                if (parseInt(data['result']) == 1) {
                    alert("Loan parameters Already Exist");
                    this.loantypesetupData['f044fidLoan'] = undefined;
                    this.loantypesetupData['f044fempStatus'] = undefined;
                    this.loantypesetupData['f044fempType'] = undefined;
                }
            }, error => {
                console.log(error);
            });
        }
    }
}