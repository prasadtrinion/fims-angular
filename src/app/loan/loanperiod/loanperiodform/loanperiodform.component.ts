import { Injector } from '@angular/core';
import { Component, OnInit, ViewChild } from '@angular/core';

import { ActivatedRoute, Router } from '@angular/router';
import { LoanperiodService } from '../../service/loanperiod.service';
import { LoannameService } from '../../service/loanname.service';


@Component({
    selector: 'LoanperiodFormComponent',
    templateUrl: 'loanperiodform.component.html'
})

export class LoanperiodFormComponent implements OnInit {

    loanperiodlist = [];
    loanperioddata = {};

    id: number;
    constructor(
        private LoanperiodService: LoanperiodService,
        private LoannameService: LoannameService,
        private route: ActivatedRoute,
        private router: Router

    ) { }

    ngOnInit() {
        this.loanperioddata['f022fstatus'] = 1;
        this.loanperioddata['f022fgrade'] = '';
        this.loanperioddata['f022fidLoanName'] = '';

        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        // loan name dropdown        
        this.LoannameService.getItems().subscribe(
            data => {
                this.loanperiodlist = data['result']['data'];
            }, error => {
                console.log(error);
            });


        if (this.id > 0) {
            this.LoanperiodService.getItemsDetail(this.id).subscribe(
                data => {
                    this.loanperioddata = data['result'][0];
                    this.loanperioddata['f022fstatus'] = parseInt(this.loanperioddata['f022fstatus']);
                }, error => {
                    console.log(error);
                });
        }
    }

    addLoanperiod() {
        console.log(this.loanperioddata);
        if (parseInt(this.loanperioddata['f022fminimumMonth']) > parseInt(this.loanperioddata['f022fmaximumMonth'])) {
            alert("Minimum Months cannot be greater than Maximum Months");
            return false;
        }
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.LoanperiodService.updateLoanperiodItems(this.loanperioddata, this.id).subscribe(
                data => {
                    this.router.navigate(['loan/loanperiod']);
                }, error => {
                    console.log(error);
                });
        } else {
            this.LoanperiodService.insertLoanperiodItems(this.loanperioddata).subscribe(
                data => {
                    this.router.navigate(['loan/loanperiod']);

                }, error => {
                    console.log(error);
                });
        }
    }
}