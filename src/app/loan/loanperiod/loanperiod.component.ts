import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LoanperiodService } from '../service/loanperiod.service';

@Component({
    selector: 'LoanPeriod',
    templateUrl: 'loanperiod.component.html'
})
export class LoanperiodComponent implements OnInit {
    loanperiodlist = [];
    loanperioddata = {};
    id: number;
    constructor(
        private LoanperiodService: LoanperiodService
       
    ) { }

    ngOnInit() {

        this.LoanperiodService.getItems().subscribe(
            data => {
                this.loanperiodlist = data['result']['data'];
                console.log(this.loanperiodlist);
            }, error => {
                console.log(error);
            });
    }
}