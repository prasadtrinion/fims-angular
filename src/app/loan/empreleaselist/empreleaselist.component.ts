import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { EmpreleastlistService } from "../service/empreleaslist.service";

@Component({
    selector: 'Empreleaselist',
    templateUrl: 'empreleaselist.component.html'
})
export class EmpreleaselistComponent implements OnInit {
    empreleaseList = [];
    empreleaseData = {};
    id: number;
    title: string;
    type: string;

    constructor(
        private route: ActivatedRoute,
        private EmpreleastlistService: EmpreleastlistService
    ) { }

    ngOnInit() {
        this.EmpreleastlistService.getItems().subscribe(
            data => {
                this.empreleaseList = data['result']['data'];

            }, error => {
                console.log(error);
            });
    }

}