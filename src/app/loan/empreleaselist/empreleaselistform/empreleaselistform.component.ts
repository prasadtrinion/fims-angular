import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { StaffService } from "../../service/staff.service";
import { EmpreleastlistService } from "../../service/empreleaslist.service";

@Component({
    selector: 'EmpreleaselistFormComponent',
    templateUrl: 'empreleaselistform.component.html'
})

export class EmpreleaselistFormComponent implements OnInit {
    empreleaseList = [];
    empreleaseData = {};
    staffList = [];
    id: number;

    constructor(
        private EmpreleastlistService: EmpreleastlistService,
        private StaffService: StaffService,
        private route: ActivatedRoute,
        private router: Router,

    ) { }
    ngOnInit() {
        this.empreleaseData['f018fstatus'] = 1;
        this.empreleaseData['f018fidStaff'] = '';

        //staff dropdown
        this.StaffService.getItems().subscribe(
            data => {
                this.staffList = data['result']['data'];
            }, error => {
                console.log(error);
            });

        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.EmpreleastlistService.getItemsDetail(this.id).subscribe(
                data => {
                    this.empreleaseData = data['result'][0];
                    this.empreleaseData['f018fstatus'] = parseInt(this.empreleaseData['f018fstatus']);

                }, error => {
                    console.log(error);
                });
        }
    }
    addEmpreleaselist() {
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.EmpreleastlistService.updateEmpreleastlistItems(this.empreleaseData, this.id).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert('Release Staff Already Exist');
                    }
                    else {
                        alert("Release Staff has been Updated successfully");
                    }
                    this.router.navigate(['loan/empreleaselist']);
                }, error => {
                    console.log(error);
                });
        } else {
            this.EmpreleastlistService.insertEmpreleastlistItems(this.empreleaseData).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert("No duplication of Release Staff");
                    }
                    else {
                        this.router.navigate(['loan/loanname']);
                        alert("Release Staff has been added successfully");
                    }
                    this.router.navigate(['loan/empreleaselist']);

                }, error => {
                    console.log(error);
                });
        }
    }

}