import { Injector } from '@angular/core';
import { Component, OnInit, ViewChild } from '@angular/core';

import { LoancancellationService } from '../../service/loancancellation.service';
import { StaffService } from "../../service/staff.service";
import { AlertService } from './../../../_services/alert.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
    selector: 'LoancancellationFormComponent',
    templateUrl: 'loancancellationform.component.html'
})

export class LoancancellationFormComponent implements OnInit {
    loancancleform: FormGroup;
    loancancleList = [];
    loancancleData = {};
    loanstaffList = [];
    id: number;
    constructor(

        private LoancancellationService: LoancancellationService,
        private AlertService: AlertService,
        private route: ActivatedRoute,
        private router: Router,
        private StaffService: StaffService

    ) { }

    ngOnInit() {
        this.loancancleData['f070fstatus'] = 1;
        this.loancancleData['f070fidStaff'] = '';

        //staff dropdown
        this.StaffService.getItems().subscribe(
            data => {
                this.loanstaffList = data['result']['data'];
            }, error => {
                console.log(error);
            });

        this.loancancleform = new FormGroup({
            loancancle: new FormControl('', Validators.required),
        });
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.LoancancellationService.getItemsDetail(this.id).subscribe(
                data => {
                    this.loancancleData = data['result'][0];
                    this.loancancleData['f070fstatus'] = parseInt(this.loancancleData['f070fstatus']);

                }, error => {
                    console.log(error);
                });
        }
    }

    addLoancancellation() {

        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.LoancancellationService.updateLoancancellationItems(this.loancancleData, this.id).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert('Loan Description Already Exist');
                    }
                    else {
                        this.router.navigate(['loan/loanname']);
                        this.AlertService.success("Loan Cancellation Request has been Updated Sucessfully !!")
                    }
                    this.router.navigate(['loan/loancancellation']);
                    this.AlertService.success("Updated Sucessfully !!")
                }, error => {
                    console.log(error);
                });

        } else {
            this.LoancancellationService.insertLoancancellationItems(this.loancancleData).subscribe(
                data => {
                    if (data['status'] == 409) {
                        this.AlertService.error("Loan Description Already Exist");
                    }
                    else {
                        this.router.navigate(['loan/loanname']);
                        this.AlertService.success("Loan Cancellation Request has been Added Sucessfully ! ");
                    }
                    this.router.navigate(['loan/loancancellation']);
                    this.AlertService.success("Added Sucessfully !!")

                }, error => {
                    console.log(error);
                });
        }
    }
}