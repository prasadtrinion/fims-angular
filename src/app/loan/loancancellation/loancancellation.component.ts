import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LoancancellationService } from "../service/loancancellation.service";

@Component({
    selector: 'Loancancellation',
    templateUrl: 'loancancellation.component.html'
})
export class LoancancellationComponent implements OnInit {
    loancancleList = [];
    loancancleData = {};
    
    id: number;
    constructor(
        private LoancancellationService: LoancancellationService
    ) { }

    ngOnInit() {

        this.LoancancellationService.getItems().subscribe(
            data => {
                this.loancancleList = data['result']['data'];
               
            }, error => {
                console.log(error);
            });
    }
}

