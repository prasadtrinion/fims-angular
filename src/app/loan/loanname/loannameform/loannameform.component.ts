import { Component, OnInit, ViewChild } from '@angular/core';
import { LoannameService } from '../../service/loanname.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { FundService } from '../../../generalsetup/service/fund.service';
import { ActivitycodeService } from '../../../generalsetup/service/activitycode.service';
import { DepartmentService } from '../../../generalsetup/service/department.service';
import { AccountcodeService } from '../../../generalsetup/service/accountcode.service';
import { DefinationmsService } from '../../../studentfinance/service/definationms.service';

@Component({
    selector: 'LoannameFormComponent',
    templateUrl: 'loannameform.component.html'
})

export class LoannameFormComponent implements OnInit {
    loannameform: FormGroup;
    loannameList = [];
    loannameData = {};
    id: number;
    fundList = [];
    activitycodeList = [];
    accountcodeList = [];
    DeptList = [];
    fpaytype = [];
    loanCategoryList = [];
    ajaxCount: number;

    constructor(
        private LoannameService: LoannameService,
        private route: ActivatedRoute,
        private router: Router,
        private ActivitycodeService: ActivitycodeService,
        private DepartmentService: DepartmentService,
        private AccountcodeService: AccountcodeService,
        private FundService: FundService,
        private DefinationmsService: DefinationmsService,
    ) { }
    ngOnInit() {
        let paytypeobj = {};
        paytypeobj['name'] = 'Vendor';
        this.fpaytype.push(paytypeobj);
        paytypeobj = {};
        paytypeobj['name'] = 'Staff';
        this.fpaytype.push(paytypeobj);

        this.DefinationmsService.getScheme('Loan Type').subscribe(
            data => {
                this.loanCategoryList = data['result'];
            }, error => {
                console.log(error);
            });
        this.loannameData['f020fstatus'] = 1;
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        this.ajaxCount++;
        this.FundService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.fundList = data['result']['data'];
            }, error => {
            });

        this.ajaxCount++;
        this.ActivitycodeService.getItemsByLevel().subscribe(
            data => {
                this.ajaxCount--;
                this.activitycodeList = data['result']['data'];
                for (var i = 0; i < this.activitycodeList.length; i++) {
                    this.activitycodeList[i]['f058fcompleteCode'] = this.activitycodeList[i]['f058fcompleteCode'];
                    this.activitycodeList[i]['f058fcompleteCode'].trim();
                }
            }, error => {
            });

        this.ajaxCount++;
        this.AccountcodeService.getItemsByLevel().subscribe(
            data => {
                this.ajaxCount--;
                this.accountcodeList = data['result']['data'];
            }, error => {
            });

        this.ajaxCount++;
        this.DepartmentService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.DeptList = data['result']['data'];
            }, error => {
            });

        if (this.id > 0) {
            this.LoannameService.getItemsDetail(this.id).subscribe(
                data => {
                    this.loannameData = data['result'];
                    this.loannameData['f020fstatus'] = parseInt(this.loannameData['f020fstatus']);
                }, error => {
                    console.log(error);
                });
        }
    }

    addLoanname() {
        if (isNaN(this.loannameData['f020fpercentage'])) {
            alert("Percentage cannot be string");
            return false;
        }
        if (parseFloat(this.loannameData['f020fpercentage']) > 100) {
            alert("Percentage cannot be greater than 100");
            return false;
        }
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        this.loannameData['f020fpercentage'] = parseFloat(this.loannameData['f020fpercentage']).toFixed(2);
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.LoannameService.updateLoannameItems(this.loannameData, this.id).subscribe(
                data => {
                        this.router.navigate(['loan/loanname']);
                        alert("Loan Type has been Updated Sucessfully !!")
                }, error => {
                    console.log(error);
                });
        } else {
            this.LoannameService.insertLoannameItems(this.loannameData).subscribe(
                data => {
                        this.router.navigate(['loan/loanname']);
                        alert("Loan Type has been Added Sucessfully ! ");
                }, error => {
                    console.log(error);
                });
        }
    }
    onlyNumberKey(event) {
        if (event.charCode == 46) {
 
        } else {
            return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
        }
    }
    checkDuplication() {
        let duplicationObj = {};
        if (this.id > 0) {
            duplicationObj['id'] = this.id;
        }
        duplicationObj['key'] = 'Loan Name';
        duplicationObj['loanName'] = this.loannameData['f020floanName'];
        if(this.loannameData['f020floanName'] != undefined && this.loannameData['f020floanName'] != ""){
        this.DefinationmsService.checkDuplication(duplicationObj).subscribe(
            data => {
                if (parseInt(data['result']) == 1) {
                    alert("Loan Type Already Exist");
                    this.loannameData['f020floanName'] = '';
                }
            }, error => {
                console.log(error);
            });
        }
    }
}