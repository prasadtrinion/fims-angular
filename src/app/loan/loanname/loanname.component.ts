
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LoannameService } from '../service/loanname.service';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'Loanname',
    templateUrl: 'loanname.component.html'
})
export class LoannameComponent implements OnInit {
    loannameList = [];
    loannameData = {};
    id: number;
    constructor(
        private LoannameService: LoannameService,
        private route: ActivatedRoute,
        private router: Router,
        private spinner: NgxSpinnerService
    ) { }

    ngOnInit() {

        this.getList();
        this.loannameData['f020fstatus'] = 1;

    }
    getList() {
        this.spinner.show();
        this.LoannameService.getItems().subscribe(
            data => {
                this.spinner.hide();
                this.loannameList = data['result']['data'];

            }, error => {
                console.log(error);
            });
    }
    addLoanname() {

        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        if (this.loannameData['f020fid'] > 0) {
            this.LoannameService.updateLoannameItems(this.loannameData, this.loannameData['f020fid']).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert('Loan Type Description Already Exist');
                    }
                    else {
                        alert("Loan Type has been Updated successfully");
                        this.getList();
                        this.loannameData = {};
                        this.loannameData['f020fstatus'] = 1;
                    }

                }, error => {
                    console.log(error);
                });
        } else {
            this.LoannameService.insertLoannameItems(this.loannameData).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert("No duplication of Loan Type");
                    }
                    else {
                        this.router.navigate(['loan/loanname']);
                        alert("Loan Type has been added successfully");
                        this.getList();
                        this.loannameData = {};
                        this.loannameData['f020fstatus'] = 1;
                    }
                }, error => {
                    console.log(error);
                });
        }
    }

    prefillData(id) {
        this.LoannameService.getItemsDetail(id).subscribe(
            data => {
                this.loannameData = data['result'];
                this.loannameData['f020fstatus'] = parseInt(this.loannameData['f020fstatus']);
                console.log(this.loannameData);
            }, error => {
                console.log(error);
            });
    }
}

