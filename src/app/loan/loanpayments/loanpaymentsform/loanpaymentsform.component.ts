import { Component, OnInit, ViewChild } from '@angular/core';
import { LoannameService } from '../../service/loanname.service';
import { ActivatedRoute, Router } from '@angular/router';
import { DefinationmsService } from '../../../studentfinance/service/definationms.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { StaffService } from "../../service/staff.service";
import { VehicleloanService } from "../../service/vehicleloan.service";

@Component({
    selector: 'LoanPaymentsFormComponent',
    templateUrl: 'loanpaymentsform.component.html'
})

export class LoanPaymentsFormComponent implements OnInit {
    loanpaymentList = [];
    loanpaymentData = {};
    id: number;
    staffList = [];
    loanList = [];
    vehicleloanData = {};
    constructor(
        private LoannameService: LoannameService,
        private route: ActivatedRoute,
        private router: Router,
        private DefinationmsService: DefinationmsService,
        private StaffService: StaffService,
        private VehicleloanService: VehicleloanService,
    ) { }
    ngOnInit() {
        this.StaffService.getLoanStaff().subscribe(
            data => {
                this.staffList = data['result']['data'];
            }, error => {
                console.log(error);
            });
    }

    addLoanPayment() {

        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
            this.LoannameService.insertLoanPayment(this.loanpaymentData).subscribe(
                data => {
                        this.router.navigate(['loan/loanpayment']);
                        alert("Loan Payment has been added successfully");
                }, error => {
                    console.log(error);
                });
    }
    getLoans(){
        this.VehicleloanService.getEmployeeLoans(this.loanpaymentData['f148fstaff']).subscribe(
            data => {
                this.loanList = data['result']['data'];
            }, error => {
                console.log(error);
            });
    }
    getAmount(){
            this.VehicleloanService.getLoanAmount(this.loanpaymentData['f148fidLoan']).subscribe(
                data => {
                    this.vehicleloanData = data['result']['data'][0];
                    this.loanpaymentData['f148famount'] = this.vehicleloanData['f043fmonthlyInstallment']
                }
            );
    }
    
    onlyNumberKey(event) {
        if (event.charCode == 46) {
 
        } else {
            return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
        }
    }
   
}