
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LoannameService } from '../service/loanname.service';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'LoanPayments',
    templateUrl: 'loanpayments.component.html'
})
export class LoanPaymentsComponent implements OnInit {
    loanpaymentList = [];
    loanpaymentData = {};
    id: number;
    constructor(
        private LoannameService: LoannameService,
        private route: ActivatedRoute,
        private router: Router,
        private spinner: NgxSpinnerService
    ) { }

    ngOnInit() {

        this.getList();
        this.loanpaymentData['f020fstatus'] = 1;

    }
    getList() {
        this.LoannameService.getLoanPayments().subscribe(
            data => {
                this.loanpaymentList = data['result']['data'];

            }, error => {
                console.log(error);
            });
    }
    

    prefillData(id) {
        this.LoannameService.getItemsDetail(id).subscribe(
            data => {
                this.loanpaymentData = data['result'];
                this.loanpaymentData['f020fstatus'] = parseInt(this.loanpaymentData['f020fstatus']);
                console.log(this.loanpaymentData);
            }, error => {
                console.log(error);
            });
    }
}

