
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SearchapplicationService } from '../service/searchapplication.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
    selector: 'Searchapplication',
    templateUrl: 'searchapplication.component.html'
})
export class SearchapplicationComponent implements OnInit {
    searchappList = [];
    searchappData = {};
    id: number;
    constructor(
        private SearchapplicationService: SearchapplicationService,
        private route: ActivatedRoute,
        private router: Router,
    ) { }

    ngOnInit() {

        this.getList();
        this.searchappData['f020fstatus'] = 1;

    }
    getList() {
        this.SearchapplicationService.getItems().subscribe(
            data => {

                this.searchappList = data['result']['data'];
                for (var i = 0; i < this.searchappList.length; i++) {
                    if (this.searchappList[i]['f020ftypeOfLoan'] == '1') {
                        this.searchappList[i]['f020ftypeOfLoanname'] = 'Vehicle Loan';
                    } else {
                        this.searchappList[i]['f020ftypeOfLoanname'] = 'SmartPhone Loan';

                    }
                }

            }, error => {
                console.log(error);
            });
    }
    addSearchapplication() {

        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        if (this.searchappData['f020fid'] > 0) {
            this.SearchapplicationService.updateSearchapplicationItems(this.searchappData, this.searchappData['f020fid']).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert('Loan Type Description Already Exist');
                    }
                    else {
                        alert("Loan Type has been Updated successfully");
                        this.getList();
                        this.searchappData = {};
                        this.searchappData['f020fstatus'] = 1;
                    }

                }, error => {
                    console.log(error);
                });
        } else {
            this.SearchapplicationService.insertSearchapplicationItems(this.searchappData).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert("No duplication of Loan Type");
                    }
                    else {
                        this.router.navigate(['loan/loanname']);
                        alert("Loan Type has been added successfully");
                        this.getList();
                        this.searchappData = {};
                        this.searchappData['f020fstatus'] = 1;
                    }
                }, error => {
                    console.log(error);
                });
        }
    }
    prefillData(id) {
        this.SearchapplicationService.getItemsDetail(id).subscribe(
            data => {
                this.searchappData = data['result'];
                this.searchappData['f020fstatus'] = parseInt(this.searchappData['f020fstatus']);
                console.log(this.searchappData);
            }, error => {
                console.log(error);
            });
    }
}

