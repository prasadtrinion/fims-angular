import { Injector } from '@angular/core';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { InsurancesetupService } from '../../service/insurancesetup.service';
import { SupplierregistrationService } from '../../../procurement/service/supplierregistration.service';
import { AlertService } from '../../../_services/alert.service';

@Component({
    selector: 'InsurancesetupFormComponent',
    templateUrl: 'insurancesetupform.component.html'
})

export class InsurancesetupFormComponent implements OnInit {

    insurancesetupList = [];
    insurancesetupData = {};
    supplierList = [];
    ajaxCount: number;


    id: number;
    constructor(

        private InsurancesetupService: InsurancesetupService,
        private SupplierregistrationService: SupplierregistrationService,
        private AlertService: AlertService,
        private route: ActivatedRoute,
        private router: Router

    ) { }

    ngOnInit() {
        this.insurancesetupData['f114fstatus'] = 1;
        this.insurancesetupData['f114fvendor'];

        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        // staff dropdown
        this.SupplierregistrationService.getItems().subscribe(
            data => {
                this.supplierList = data['result']['data'];
            }, error => {
                console.log(error);
            });

        if (this.id > 0) {
            this.InsurancesetupService.getItemsDetail(this.id).subscribe(
                data => {
                    this.insurancesetupData = data['result'][0];
                    this.insurancesetupData['f114fstatus'] = parseInt(this.insurancesetupData['f114fstatus']);
                }, error => {
                    console.log(error);
                });
        }
    }
    addInsurancesetup() {
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }

        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.InsurancesetupService.updateInsurancesetupItems(this.insurancesetupData, this.id).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert('Insurancesetup Already Exist');
                    }
                    else {
                        this.router.navigate(['loan/insurancesetup']);
                        this.AlertService.success("Updated Sucessfully !!")
                        alert(" Updated Sucessfully !!");
                    }
                }, error => {
                    alert('Insurancesetup Already Exist');
                    return false;
                });
        } else {
            this.InsurancesetupService.insertInsurancesetupItems(this.insurancesetupData).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert('Insurancesetup Already Exist');
                    }
                    else {
                        this.router.navigate(['loan/insurancesetup']);
                        this.AlertService.success("Added Sucessfully !!")
                        alert(" Added Sucessfully !!");
                    }
                }, error => {
                    console.log(error);
                });
        }
    }
}