import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Injector } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LoanreportService } from "../service/loanreport.service";
import { VehicleloanService } from "../service/vehicleloan.service";


@Component({
    selector: 'LoanreportComponent',
    templateUrl: 'loanreport.component.html'
})

export class LoanreportComponent implements OnInit {
    loanreportList = [];
    loanreportData = {};
    id: number;
    selectAllCheckbox: false;
    type: string;
    title = '';

    constructor(

        private LoanreportService: LoanreportService,
        private VehicleloanService: VehicleloanService,
        private route: ActivatedRoute,

    ) { }
    ngOnInit() {
        this.getList();
    }
    getList() {
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));


        this.type = 'vehicleloan';
        this.title = 'Loan Report';

        this.VehicleloanService.getItems().subscribe(
            data => {
                this.loanreportList = data['result']['data'];
            }, error => {
                console.log(error);
            });
    }

    loanReportListData() {
        let approveDarrayList = [];
        console.log(this.loanreportList);
        for (var i = 0; i < this.loanreportList.length; i++) {
            if (this.loanreportList[i].f079fstatus == true) {
                approveDarrayList.push(this.loanreportList[i].f091fid);
            }
        }
        var approvalObject = {};
        approvalObject['id'] = approveDarrayList;

        this.LoanreportService.updateApproval(approvalObject).subscribe(
            data => {
                this.getList();
            }, error => {
                console.log(error);
            });
    }
}