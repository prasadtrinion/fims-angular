import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { VehicleloanService } from "../service/vehicleloan.service";
import { LoanlistingService } from "../service/loanlisting.service";
import { NgxSpinnerService } from 'ngx-spinner';
import { environment } from '../../../environments/environment';

@Component({
    selector: 'Vehicleloan',
    templateUrl: 'vehicleloan.component.html'
})
export class VehicleloanComponent implements OnInit {
    vehicleloanList = [];
    vehicleloanData = {};
    id: number;
    loanexist:boolean = true;
    downloadUrl: string = environment.api.downloadbase;
    constructor(
        private VehicleloanService: VehicleloanService,
        private LoanlistingService: LoanlistingService,
        private spinner: NgxSpinnerService,


    ) { }

    ngOnInit() {
        this.spinner.show();
        var staffId = localStorage.getItem('staffId');
        console.log(staffId);
        this.LoanlistingService.getItemsDetail().subscribe(
            data => {
                this.spinner.hide();
                this.vehicleloanList = data['result']['data'];
                if(this.vehicleloanList.length > 1){
                    this.loanexist = false;
                }
                for (var i = 0; i < this.vehicleloanList.length; i++) {
                    if(this.vehicleloanList[i]['f043ffinalStatus'] == 1){
                        this.vehicleloanList[i]['showAccept'] = true; 
                    }
                    else{
                        this.vehicleloanList[i]['showAccept'] = false;
                    }
                    if (this.vehicleloanList[i]['f043fapprover8'] != 0) {
                        if (this.vehicleloanList[i]['f043fapprover8'] == 1) {
                            this.vehicleloanList[i]['currentStatus'] = 'Approved by Registrar';
                        } else {
                            this.vehicleloanList[i]['currentStatus'] = 'Rejected by Registrar ';

                        }
                    } else if (this.vehicleloanList[i]['f043fapprover7'] != 0) {
                        if (this.vehicleloanList[i]['f043fapprover7'] == 1) {
                            this.vehicleloanList[i]['currentStatus'] = 'Approved by Vice Chancellor';
                        } else {
                            this.vehicleloanList[i]['currentStatus'] = 'Rejected by Vice Chancellor';

                        }
                    } else if (this.vehicleloanList[i]['f043fapprover6'] != 0) {
                        if (this.vehicleloanList[i]['f043fapprover6'] == 1) {
                            this.vehicleloanList[i]['currentStatus'] = 'Approved by Bursar';
                        } else {
                            this.vehicleloanList[i]['currentStatus'] = 'Rejected by Bursar';

                        }
                    } else if (this.vehicleloanList[i]['f043fapprover5'] != 0) {
                        if (this.vehicleloanList[i]['f043fapprover5'] == 1) {
                            this.vehicleloanList[i]['currentStatus'] = 'Approved by Verfiy Loan';
                        } else {
                            this.vehicleloanList[i]['currentStatus'] = 'Rejected by Verfiy Loan';

                        }
                    } else if (this.vehicleloanList[i]['f043fapprover4'] != 0) {
                        if (this.vehicleloanList[i]['f043fapprover4'] == 1) {
                            this.vehicleloanList[i]['currentStatus'] = 'Approved by Loan Checking';
                        } else {
                            this.vehicleloanList[i]['currentStatus'] = 'Rejected by Loan Checking';

                        }
                    } else if (this.vehicleloanList[i]['f043fapprover3'] != 0) {
                        if (this.vehicleloanList[i]['f043fapprover3'] == 1) {
                            this.vehicleloanList[i]['currentStatus'] = 'Approved by HOD Approval';
                        } else {
                            this.vehicleloanList[i]['currentStatus'] = 'Rejected by HOD Approval';

                        }
                    } else if (this.vehicleloanList[i]['f043fapprover2'] != 0) {
                        if (this.vehicleloanList[i]['f043fapprover2'] == 1) {
                            this.vehicleloanList[i]['currentStatus'] = 'Approved by Guarantors';
                        } else {
                            this.vehicleloanList[i]['currentStatus'] = 'Rejected by Guarantors';

                        }
                    } else {
                        this.vehicleloanList[i]['currentStatus'] = 'Pending for Approval';
                    }
                }
            }, error => {
                console.log(error);
            });
    }
    downloadPDF(id, type) {
        console.log(id);
        let newobj = {};
        newobj['idLoan'] = id;
        if (type == 1) {
            this.VehicleloanService.downloadVehicleReport(newobj).subscribe(
                data => {
                    console.log(data['name']);
                    window.open(this.downloadUrl + data['name']);
                }, error => {
                    console.log(error);
                });
        }
        else {
            this.VehicleloanService.downloadComputerReport(newobj).subscribe(
                data => {
                    console.log(data['name']);
                    window.open(this.downloadUrl + data['name']);
                }, error => {
                    console.log(error);
                });
        }
    }
}