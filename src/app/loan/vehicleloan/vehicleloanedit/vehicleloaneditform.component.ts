import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { VehicleloanService } from "../../service/vehicleloan.service";
import { SupplierregistrationService } from "../../../procurement/service/supplierregistration.service";
import { StaffService } from "../../service/staff.service";
import { VehicletypeService } from "../../service/vehicletype.service";
import { LoannameService } from '../../service/loanname.service';
import { BlacklisttempService } from "../../service/blacklisttemp.service";
import { NgxSpinnerService } from 'ngx-spinner';
import { InsurancecompanyService } from '../../service/insurancecompany.service';
import { ApplyloanapprovalService } from "../../service/applyloanapproval.service";
import { DatePipe } from '@angular/common';

@Component({
    selector: 'VehicleloaneditFormComponent',
    templateUrl: 'vehicleloanedit.component.html'
})

export class VehicleloaneditFormComponent implements OnInit {

    vehicleloanList = [];
    vehicleloanData = {};
    vendorlist = [];
    customerList = [];
    smartStaffList = [];
    vehicletypeList = [];
    loannameList = [];
    supplierList = [];
    loantypesetupData = {}
    staffDetails = {};
    loannameData = {};
    setupgradeList = [];
    insuranceList = [];
    supplierListNew = [];
    insurancesetupList = [];
    ajaxCount: number;
    id: number;
    viewDisabled: boolean;
    currentEmpGrande: string;
    supplierListFromView = [];
    typeofLoan;
    hideUsedDetails: number;
    hideNewDetails: number;
    loanPercentage: number;
    currentEmpDesignation: string;
    guaranteeListCount: number;
    guaranteeList = [];
    blacklistempList = [];
    maxMonth: number;
    maxAmount: number;
    powerNumber: number;
    tempStaffList = [];
    jkrList = [];
    gradeStaffList = [];
    fvehicle = [];
    currentAge: number;
    departmentName: string;
    f034fstaffId: string;
    designation: string;
    f043icnumber: string;
    address: string;
    showVendor: string;
    idview;
    Vehiclelist = [];
    insurancecompanyList = [];
    minMonth;
    eos:any;
    constructor(
        private VehicleloanService: VehicleloanService,
        private SupplierregistrationService: SupplierregistrationService,
        private LoannameService: LoannameService,
        private StaffService: StaffService,
        private VehicletypeService: VehicletypeService,
        private route: ActivatedRoute,
        private router: Router,
        private BlacklisttempService: BlacklisttempService,
        private ApplyloanapprovalService: ApplyloanapprovalService,
        private spinner: NgxSpinnerService,
        private InsurancecompanyService: InsurancecompanyService,
    ) { }
    checkEntitlement() {
        if (this.vehicleloanData['f043fpurchasePrice'] == undefined) {
            alert("Enter purchase price");
            return false;
        }
        if (parseFloat(this.vehicleloanData['f043floanEntitlement']) < parseFloat(this.vehicleloanData['f043floanAmount'])) {
            this.vehicleloanData['f043floanAmount'] = 0;
            alert("Your Loan Amount should be less than the Entitlement");
            return false;
        }
    }
    checkInsurance() {
        if (this.vehicleloanData['f043finsuranceComp'] == undefined || this.vehicleloanData['f043finsuranceComp'] == '') {
            alert("Please select insurance company");
            return false;
        }
    }
    checkLimit() {
        if (this.vehicleloanData['f043finstallmentPeriod'] > this.maxMonth) {
            alert("Cannot be greater than " + this.maxMonth + " Months");
            this.vehicleloanData['f043finstallmentPeriod'] = 0;
            return false;
        }
        if (this.vehicleloanData['f043finstallmentPeriod'] < this.minMonth) {
            alert("Cannot be less than " + this.minMonth + " Months");
            this.vehicleloanData['f043finstallmentPeriod'] = 0;
            return false;
        }
    }
    monthlyemicalculate() 
    {
        let endService = this.eos;
        let currentDate =new Date();
        let installmentMonths = parseInt(this.vehicleloanData['f043finstallmentPeriod']);
        currentDate.setMonth(currentDate.getMonth()+installmentMonths);
        let newDate = new Date(currentDate).toISOString().substr(0, 10); 
        let endDate = new Date(endService).toISOString().substr(0, 10); 
        if(endDate < newDate){
            alert('Enter installment period prior to End of Service');
            return false;
        }
        if (this.vehicleloanData['f043finstallmentPeriod'] > this.maxMonth) {
            alert("Cannot be greater than " + this.maxMonth + " Months");
            this.vehicleloanData['f043finstallmentPeriod'] = 0;
            return false;
        }
        if (this.vehicleloanData['f043finstallmentPeriod'] < this.minMonth) {
            alert("Cannot be less than " + this.minMonth + " Months");
            this.vehicleloanData['f043finstallmentPeriod'] = 0;
            return false;
        }
        var amount = parseFloat(this.vehicleloanData['f043floanAmount']);
        var period = parseInt(this.vehicleloanData['f043finstallmentPeriod']);
        this.vehicleloanData['f043fmonthlyemi'] = amount / period;
        this.vehicleloanData['f043fmonthlyemi'] = Math.floor(this.vehicleloanData['f043fmonthlyemi'] * 100) / 100;
        let insurancePeriod = {};
        insurancePeriod['age'] = this.currentAge;
        insurancePeriod['period'] = period;

        this.spinner.show();
        this.VehicleloanService.getInsruanceAmount(insurancePeriod).subscribe(
            data => {
                this.spinner.hide();
                if (data == '') {
                    alert('Insurance Amount not defined for your age');
                    this.router.navigate(['loan/insurancecompany']);
                }
                else {
                    this.vehicleloanData['f043insuranceAmt'] = ((parseFloat(data['value']) * parseFloat(this.vehicleloanData['f043floanAmount'])) / 1000).toFixed(2);
                    if(isNaN(this.vehicleloanData['f043insuranceAmt'])){
                        this.vehicleloanData['f043insuranceAmt'] = '';
                        return false;
                    }
                    else{
                        this.calculateTotal();
                    }
                }
            }, error => {
                this.spinner.hide();
                console.log(error);
            });
    }
    calculateTotal() {
       
        if (this.vehicleloanData['f043floanAmount'] != '') {
            var lasttwodigits = this.vehicleloanData['f043floanAmount'].toString().slice(-2);
            if (lasttwodigits != '00') {
                this.vehicleloanData['f043floanAmount'] = 0;
                alert("Loan amount should be multiple of 100");
                return false;
            }
            this.vehicleloanData['f043ftotalLoan'] = parseFloat(this.vehicleloanData['f043insuranceAmt']) + parseFloat(this.vehicleloanData['f043floanAmount'])
        }
    }
    calculateLoan() {
        if(this.vehicleloanData['f043fidLoanName'] == undefined){
            alert('Select Loan Type');
            return false;
        }
        if(this.vehicleloanData['f043finsuranceComp'] == undefined){
            alert('Select Insurance Company');
            return false;
        }
        if(this.vehicleloanData['f043fpurchasePrice'] == undefined){
            alert('Enter Purchase Price');
            return false;
        }
        if(this.vehicleloanData['f043floanAmount'] == undefined){
            alert('Enter Loan Amount');
            return false;
        }
        if(this.vehicleloanData['f043finstallmentPeriod'] == undefined){
            alert('Enter Installment Period');
            return false;
        }
        if(this.vehicleloanData['f043insuranceAmt'] == undefined){
            alert('Insurance Amount not defined');
            return false;
        }
        let loanAmount = parseFloat(this.vehicleloanData['f043floanAmount']);
        let insuranceAmount = parseFloat(this.vehicleloanData['f043insuranceAmt']);
        let totalLoan = loanAmount + insuranceAmount;
        let period = parseInt(this.vehicleloanData['f043finstallmentPeriod']);
        let loanPercentage = this.loanPercentage;
        let interestRatePerMonth = ((loanPercentage / 100) / 12).toFixed(10);;
        let powerNumber = 1 + parseFloat(interestRatePerMonth);
        let profitRatio = (Math.pow(powerNumber, period).toFixed(10)).toString();
        let loanAmountperMonth = ((totalLoan) * (parseFloat(profitRatio)) * (parseFloat(interestRatePerMonth)));
        let monthlyInstallment = (loanAmountperMonth) / (parseFloat(profitRatio) - 1);
        let principal = totalLoan / period;
        let profitAmount = monthlyInstallment - principal;

        this.vehicleloanData['f043fmonthlyInstallment'] = monthlyInstallment.toFixed(2);
        this.vehicleloanData['f043fprincipal'] = principal.toFixed(2);
        this.vehicleloanData['f043fprofitAmount'] = profitAmount.toFixed(2);
        this.vehicleloanData['f043ftotalLoan'] = totalLoan.toFixed(2);

    }
    ngOnInit() {
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.route.paramMap.subscribe((data) => this.idview = + data.get('idview'));

        let jkrobj = {};
        jkrobj['id'] = 1;
        jkrobj['name'] = '1 Year';
        this.jkrList.push(jkrobj);
        jkrobj = {};
        jkrobj['id'] = 2;
        jkrobj['name'] = '2 Years';
        this.jkrList.push(jkrobj);
        this.vehicleloanData['f043fempName'] = localStorage.getItem('staffId');

        // this.spinner.show();
        this.BlacklisttempService.getItems().subscribe(
            data => {
                // this.spinner.hide();
                this.blacklistempList = data['result']['data'];
                for (var i = 0; i < this.blacklistempList.length; i++) {
                    if (parseInt(this.blacklistempList[i]['f018fidStaff']) == parseInt(this.vehicleloanData['f043fempName'])&& parseInt(this.blacklistempList[i]['f018fstatus']) == 1) {
                        alert('You are blacklisted');
                        this.router.navigate(['loan/vehicleloan']);
                    }
                }
            }, error => {
                this.spinner.hide();
                console.log(error);
            });
        this.viewDisabled = true;
        this.vehicleloanData['f043fstatus'] = 1;
        this.vehicleloanData['f043fvehicleType'] = '';
        this.vehicleloanData['f043fvehicleCondition'] = '';
        this.vehicleloanData['f043finsuranceComp'] = '';
        this.vehicleloanData['f043ffirstGuarantor'] = '';
        this.vehicleloanData['f043fsecondGuarantor'] = '';
        // this.vehicleloanData['f043fidLoanName'] = 0;
        this.vehicleloanData['f043fempName'] = localStorage.getItem('staffId');

        this.SupplierregistrationService.getItems().subscribe(
            data => {
                this.supplierListFromView = data['result']['data'];
                this.supplierListNew = this.supplierListFromView;
            }, error => {
                this.spinner.hide();
                console.log(error);
            });

        let vehicleobj = {};
        vehicleobj['id'] = 1;
        vehicleobj['name'] = 'New';
        this.Vehiclelist.push(vehicleobj);
        vehicleobj = {};
        vehicleobj['id'] = 2;
        vehicleobj['name'] = 'Used';
        this.Vehiclelist.push(vehicleobj);
        if (this.id > 0) {
            this.VehicleloanService.getItemsDetail(this.id).subscribe(
                data => {
                    this.vehicleloanData = data['result'];
                    this.StaffService.getAllItems().subscribe(
                        data => {
            
                            this.smartStaffList = data['result']['data'];
                            var staffId = this.vehicleloanData['f043fempName'];
                            this.vehicleloanData['f043fempName'] = staffId;
                            for (var i = 0; i < this.smartStaffList.length; i++) {
                                for (var j = 0; j < this.blacklistempList.length; j++) {
                                    if (this.smartStaffList[i]['f034fstaffId'] == this.blacklistempList[j]['f018fidStaff'] && parseInt(this.blacklistempList[j]['f018fstatus']) == 1) {
                                        this.smartStaffList.splice(i, 1);
                                    }
                                }
            
                                this.smartStaffList[i]['gradeCustomerName'] = this.smartStaffList[i]['f034fstaffId'] + '-' + this.smartStaffList[i]['f034fname'] + '(' + this.smartStaffList[i]['f034fgradeId'] + ')';
                                if (this.smartStaffList[i]['f034fstaffId'] == staffId) {
                                    this.vehicleloanData['departmentName'] = this.smartStaffList[i]['departmentName'];
                                    this.vehicleloanData['f034fstaffId'] = this.smartStaffList[i]['f034fstaffId'];
                                    this.vehicleloanData['f034freportingName'] = this.smartStaffList[i]['hod_name'] + '(' + this.smartStaffList[i]['hod'] + ')';
                                    this.vehicleloanData['designation'] = this.smartStaffList[i]['employeedesignationname'];
                                    this.vehicleloanData['f043icnumber'] = this.smartStaffList[i]['f630nokpbaru'];
                                    this.vehicleloanData['dor'] = this.smartStaffList[i]['employeedateofjoining'];
                                    this.eos = this.smartStaffList[i]['endofservice'];
                                    var datePipe = new DatePipe("en-US");
                                    this.vehicleloanData['dor'] = datePipe.transform(this.vehicleloanData['dor'], 'dd/mm/yyyy');
                                    this.vehicleloanData['address'] = this.smartStaffList[i]['f630almtt1'] + ' ' + this.smartStaffList[i]['f630almtt2'] + ' ' + this.smartStaffList[i]['f630almtt3'];
                                    let fromdate = new Date(this.smartStaffList[i]['dateofbirth']);
                                    let todate = new Date();
                                    var timeDiff = Math.abs(todate.getTime() - fromdate.getTime());
                                    var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)) / 365;
                                    this.currentAge = parseInt(diffDays.toString());
                                    this.currentEmpGrande = this.smartStaffList[i]['f034fgradeId'];
                                    this.currentEmpDesignation = this.smartStaffList[i]['departmentName'];
                                }
                            }
                            for (var i = 0; i < this.smartStaffList.length; i++) {
                                if (this.smartStaffList[i]['f034fgradeId'] >= this.currentEmpGrande) {
                                    this.tempStaffList.push(this.smartStaffList[i]);
                                }
                            }
                            this.gradeStaffList = this.tempStaffList;
                            this.getLoanTypeDetails();
                    this.hideDetailsOfNew();
            
                        }, error => {
                            this.spinner.hide();
                            console.log(error);
                        });
                    
                    this.guaranteeList = data['result']['guarantor-list'];
                }, error => {
                    this.spinner.hide();
                    console.log(error);
                });
        }
      
        this.LoannameService.getActiveItems().subscribe(
            data => {
                this.loannameList = data['result']['data'];
            }, error => {
                this.spinner.hide();
                console.log(error);
            });

        this.VehicletypeService.getItems().subscribe(
            data => {
                this.vehicletypeList = data['result']['data'];
            }, error => {
                this.spinner.hide();
                console.log(error);
            });

        this.InsurancecompanyService.getItems().subscribe(
            data => {
                this.insurancecompanyList = data['result'];
            }, error => {
                this.spinner.hide();
                console.log(error);
            });
       
    }
    onlyNumberKey(event) {
        if (event.charCode == 46) {

        } else {
            return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
        }
    }
    hideDetailsOfNew() {
        let empdetails = {};
        empdetails['idstaff'] = localStorage.getItem('staffId');
        empdetails['idgrade'] = this.currentEmpGrande;
        empdetails['idloantype'] = this.vehicleloanData['f043fidLoanName'];
        empdetails['f043fvehicleCondition'] = this.vehicleloanData['f043fvehicleCondition'];

        this.spinner.show();
        this.VehicleloanService.getVehicleLoanGradeAmountDetails(empdetails).subscribe(
            newData => {
                this.spinner.hide();
                if (newData['result']!=false) {
                    this.maxMonth = parseInt(newData['result']['f046fgradeMonthTo']);
                    this.minMonth = parseInt(newData['result']['f046fgradeMonthFrom']);
                    this.vehicleloanData['f043floanEntitlement'] = this.maxAmount = parseInt(newData['result']['f046fgradeTo']);
                }
                else{
                    alert('Loan Entitlement not defined');
                    this.router.navigate(['loan/setupgrade']);
                }
            }, error => {
                this.spinner.hide();
                console.log(error);
            }
        );
        if (this.typeofLoan == 'Vehicle Loan') {
            if (this.vehicleloanData['f043fvehicleCondition'] == 1) {
                this.hideUsedDetails = 0;
                this.hideNewDetails = 1;
            } else {
                this.hideUsedDetails = 1;
                this.hideNewDetails = 0;
            }
        }
        else {
            this.hideUsedDetails = 0;
            this.hideNewDetails = 1;
        }

    }
    getLoanTypeDetails() {
        let empdetails = {};
        empdetails['idstaff'] = localStorage.getItem('staffId');
        empdetails['idgrade'] = this.currentEmpGrande;
        empdetails['idloantype'] = this.vehicleloanData['f043fidLoanName'];

        this.spinner.show();
        this.VehicleloanService.getVehicleLoanGradeAmountDetails(empdetails).subscribe(
            newData => {
                this.spinner.hide();
                if (newData['result']!=false) {
                    this.maxMonth = parseInt(newData['result']['f046fgradeMonthTo']);
                    this.minMonth = parseInt(newData['result']['f046fgradeMonthFrom']);
                    this.vehicleloanData['f043floanEntitlement'] = this.maxAmount = parseInt(newData['result']['f046fgradeTo']);
                }
                else{
                    alert('Loan Entitlement not defined');
                    this.router.navigate(['loan/setupgrade']);
                }
            }, error => {
                this.spinner.hide();
                console.log(error);
            }
        );
        for (var i = 0; i < this.loannameList.length; i++) {
            if (this.loannameList[i]['f020fid'] == this.vehicleloanData['f043fidLoanName']) {
                this.typeofLoan = this.loannameList[i]['f020ftypeOfLoan'];
                this.showVendor = this.loannameList[i]['f020fpaytype'];
                this.loanPercentage = parseInt(this.loannameList[i]['f020fpercentage']);
            }
        }
        if (this.typeofLoan == 'Vehicle Loan') {
            this.hideUsedDetails = 0;
            this.hideNewDetails = 0;
        } else {
            this.vehicleloanData['f043fvehicleCondition'] = '';

            this.hideUsedDetails = 0;
            this.hideNewDetails = 1;
        }
        var idloanTypeSetup = this.vehicleloanData['f043fidLoanName'];
    }
    getLoantype() {
        var idloanTypeSetup = this.vehicleloanData['f043fidLoanName'];

        this.spinner.show();
        this.LoannameService.getItemsDetail(idloanTypeSetup).subscribe(
            data => {
                this.spinner.hide();
                this.loannameData = data['result'];
            }, error => {
                this.spinner.hide();
                console.log(error);
            });
    }
    guarantorLevel(obj, index) {
        for (var i = 0; i < this.guaranteeList.length; i++) {
            if (i == index) {
                continue;
            }
            if ((obj['guaranteeId'] == this.guaranteeList[i]['guaranteeId']) && obj['guaranteeId'] != undefined) {
                alert("Same staff cannot be selected");
                this.guaranteeList[index]['guaranteeId'] = '';
            }
        }
    }
    applyloanapprovalListData() {
        var approvaloneUpdate = {};
        approvaloneUpdate['id'] = this.id;
        approvaloneUpdate['level'] = this.idview;
        approvaloneUpdate['status'] = '1';
        approvaloneUpdate['reason'] = this.vehicleloanData['reason'];
        var confirmPop = confirm("Do you want to approve?");
        if (confirmPop == false) {
            return false;
        }
        if (this.idview > 1) {
            this.ApplyloanapprovalService.updateApplyloanapprovalItems(approvaloneUpdate).subscribe(
                data => {
                    alert("Loan approved successfully!!");
                    if (this.idview == 1) {
                        this.router.navigate(['loan/guranterone/1/0']);
                    }
                    else if (this.idview == 3) {
                        this.router.navigate(['loan/approvalthree/3/0']);
                    }
                    else if (this.idview == 4) {
                        this.router.navigate(['loan/approvalfour/4/0']);
                    }
                    else if (this.idview == 5) {
                        this.router.navigate(['loan/approvalfive/5/0']);
                    }
                    else if (this.idview == 6) {
                        this.router.navigate(['loan/approvalsix/6/0']);
                    }
                    else if (this.idview == 7) {
                        this.router.navigate(['loan/approvalseven/7/0']);
                    }
                    else if (this.idview == 8) {
                        this.router.navigate(['loan/approvaleight/8/0']);
                    }
                    else if (this.idview == 9) {
                        this.router.navigate(['loan/approvalnine/9/0']);
                    }
                    else if (this.idview == 11) {
                        this.router.navigate(['loan/vehicleloan']);
                    }

                }, error => {
                    console.log(error);
                });
        }
        else {
            this.ApplyloanapprovalService.updateApplyloanapprovalItemsByUser(approvaloneUpdate).subscribe(
                data => {
                    alert("Loan approved successfully!!");
                    if (this.idview == 1) {
                        this.router.navigate(['loan/guranterone/1/0']);
                    }
                    else if (this.idview == 3) {
                        this.router.navigate(['loan/approvalthree/3/0']);
                    }
                    else if (this.idview == 4) {
                        this.router.navigate(['loan/approvalfour/4/0']);
                    }
                    else if (this.idview == 5) {
                        this.router.navigate(['loan/approvalfive/5/0']);
                    }
                    else if (this.idview == 6) {
                        this.router.navigate(['loan/approvalsix/6/0']);
                    }
                    else if (this.idview == 7) {
                        this.router.navigate(['loan/approvalseven/7/0']);
                    }
                    else if (this.idview == 8) {
                        this.router.navigate(['loan/approvaleight/8/0']);
                    }
                    else if (this.idview == 9) {
                        this.router.navigate(['loan/approvalnine/9/0']);
                    }
                    else if (this.idview == 11) {
                        this.router.navigate(['loan/vehicleloan']);
                    }

                }, error => {
                    console.log(error);
                });
        }
    }
    applyloanRejectListData() {
        var approvaloneUpdate = {};
        approvaloneUpdate['id'] = this.id;
        approvaloneUpdate['level'] = this.idview;
        approvaloneUpdate['status'] = '2';
        approvaloneUpdate['reason'] = this.vehicleloanData['reason'];
        if (this.vehicleloanData['reason'] == undefined) {
            alert('Enter reason to reject!!');
            return false;
        }
        var confirmPop = confirm("Do you want to reject?");
        if (confirmPop == false) {
            return false;
        }
        if (this.idview > 1) {
            this.ApplyloanapprovalService.updateApplyloanapprovalItems(approvaloneUpdate).subscribe(
                data => {
                    alert("Loan rejected successfully!!");
                    if (this.idview == 1) {
                        this.router.navigate(['loan/guranterone/1/0']);
                    }
                    else if (this.idview == 3) {
                        this.router.navigate(['loan/approvalthree/3/0']);
                    }
                    else if (this.idview == 4) {
                        this.router.navigate(['loan/approvalfour/4/0']);
                    }
                    else if (this.idview == 5) {
                        this.router.navigate(['loan/approvalfive/5/0']);
                    }
                    else if (this.idview == 6) {
                        this.router.navigate(['loan/approvalsix/6/0']);
                    }
                    else if (this.idview == 7) {
                        this.router.navigate(['loan/approvalseven/7/0']);
                    }
                    else if (this.idview == 8) {
                        this.router.navigate(['loan/approvaleight/8/0']);
                    }
                    else if (this.idview == 9) {
                        this.router.navigate(['loan/approvalnine/9/0']);
                    }
                    else if (this.idview == 11) {
                        this.router.navigate(['loan/vehicleloan']);
                    }

                }, error => {
                    console.log(error);
                });
        }
        else {
            this.ApplyloanapprovalService.updateApplyloanapprovalItemsByUser(approvaloneUpdate).subscribe(
                data => {
                    alert("Loan rejected successfully!!");
                    if (this.idview == 1) {
                        this.router.navigate(['loan/guranterone/1/0']);
                    }
                    else if (this.idview == 3) {
                        this.router.navigate(['loan/approvalthree/3/0']);
                    }
                    else if (this.idview == 4) {
                        this.router.navigate(['loan/approvalfour/4/0']);
                    }
                    else if (this.idview == 5) {
                        this.router.navigate(['loan/approvalfive/5/0']);
                    }
                    else if (this.idview == 6) {
                        this.router.navigate(['loan/approvalsix/6/0']);
                    }
                    else if (this.idview == 7) {
                        this.router.navigate(['loan/approvalseven/7/0']);
                    }
                    else if (this.idview == 8) {
                        this.router.navigate(['loan/approvaleight/8/0']);
                    }
                    else if (this.idview == 9) {
                        this.router.navigate(['loan/approvalnine/9/0']);
                    }
                    else if (this.idview == 11) {
                        this.router.navigate(['loan/vehicleloan']);
                    }
                }, error => {
                    console.log(error);
                });
        }
    }
}