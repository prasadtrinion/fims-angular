import { Injector } from '@angular/core';
import { Component, OnInit, ViewChild } from '@angular/core';
import { AlertService } from './../../../_services/alert.service';
import { ActivatedRoute, Router } from '@angular/router';
import { SmartloanService } from "../../service/smartloan.service";
import { SupplierregistrationService } from "../../../procurement/service/supplierregistration.service";
import { CustomerService } from '../../../accountsrecivable/service/customer.service';
import { StaffService } from "../../service/staff.service";



@Component({
    selector: 'SmartloanFormComponent',
    templateUrl: 'smartloanform.component.html'
})

export class SmartloanFormComponent implements OnInit {

   smartloanList = [];
   smartloanData = {};
   vendorList = [];
   customerList = [];
   smartStaffList = [];
   supplierList=[];
   firstStaffList=[];
    id: number;
    constructor(
        private SmartloanService: SmartloanService,
        private SupplierregistrationService: SupplierregistrationService,
        private route: ActivatedRoute,
        private CustomerService: CustomerService,
        private StaffService:StaffService,
        private AlertService: AlertService,
        private router: Router

    ) { }

    ngOnInit() {
         this.smartloanData['f043fstatus'] = 1;
        this.smartloanData['f043fidVendor'] = '';
        this.smartloanData['f043finsurance'] = '';
        this.smartloanData['f043fempId'] = '';
        this.smartloanData['f043ffirstGuarantor']='';
         

        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        // Vendor dropdown
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        this.SupplierregistrationService.getItems().subscribe(
            data => {
                this.supplierList = data['result']['data'];
        }, error => {
            console.log(error);
        });
        //  Customer dropdown
        this.CustomerService.getItems().subscribe(
            data => {
                this.customerList = data['result']['data'];
            }, error => {
                console.log(error);
            });
        //  staff dropdown
        this.StaffService.getItems().subscribe(
        data => {
            this.smartStaffList = data['result']['data'];
        }, error => {
            console.log(error);
        });
        //  first gurantor dropdown
        this.StaffService.getItems().subscribe(
            data => {
                this.firstStaffList = data['result']['data'];
            }, error => {
                console.log(error);
            });
        
        if (this.id > 0) {
            this.SmartloanService.getItemsDetail(this.id).subscribe(
                data => {
                    this.smartloanData = data['result'][0];
                    this.smartloanData['f043fstatus'] = parseInt(this.smartloanData['f043fstatus']);
                }, error => {
                    console.log(error);
                });
        }
    }

    addSmartloan() {
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.SmartloanService.updateSmartloanItems(this.smartloanData, this.id).subscribe(
                data => {
                    this.router.navigate(['loan/smartloan']);
                    this.AlertService.success("Updated Sucessfully !!")

                }, error => {
                    console.log(error);
                });
        } else {
            this.SmartloanService.insertSmartloanItems(this.smartloanData).subscribe(
                data => {
                    this.router.navigate(['loan/smartloan']);
                    this.AlertService.success("Added Sucessfully !!")

                }, error => {
                    console.log(error);
                });
        }
    }
}