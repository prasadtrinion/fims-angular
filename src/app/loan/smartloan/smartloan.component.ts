import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SmartloanService } from "../service/smartloan.service";

@Component({
    selector: 'SmartLoan',
    templateUrl: 'smartloan.component.html'
})
export class SmartloanComponent implements OnInit {
    smartloanList = [];
    smartloanData = {};
    id: number;
    constructor(
        private SmartloanService: SmartloanService

    ) { }

    ngOnInit() {

        this.SmartloanService.getItems().subscribe(
            data => {
                this.smartloanList = data['result']['data'];
                console.log(this.smartloanList);
            }, error => {
                console.log(error);
            });
    }

   
}