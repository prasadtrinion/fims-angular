import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { LoanlistingService } from "../service/loanlisting.service";
import { VehicleloanService } from "../service/vehicleloan.service";
import { NgxSpinnerService } from 'ngx-spinner';
import { environment } from "../../../environments/environment";

@Component({
    selector: 'Loanlisting',
    templateUrl: 'loanlisting.component.html'
})

export class LoanlistingComponent implements OnInit {

    loanlistingList = [];
    loanlistingData = {};
    selectAllCheckbox = true;
    loanlistingListNew = [];
    level: number;
    typeText: string;
    approveBtnLabel:string;
    rejectBtnLabel:string;
    downloadUrl: string = environment.api.downloadbase;
    constructor(
        private LoanlistingService: LoanlistingService,
        private VehicleloanService: VehicleloanService,
        private spinner: NgxSpinnerService

    ) { }

    ngOnInit() {
        this.getListData();
    }
    getListData() {
        this.spinner.show();
        this.VehicleloanService.getItems().subscribe(
            data => {
                this.spinner.hide();
                this.loanlistingListNew = data['result']['data'];
                let fianlListingEmp = [];
                let currentStaffid = localStorage.getItem('staffId');
                for (var i = 0; i < this.loanlistingListNew.length; i++) {
                    if (this.loanlistingListNew[i]['f043fempName'] == currentStaffid) {
                        fianlListingEmp.push(this.loanlistingListNew[i]);
                    }

                    if (this.loanlistingListNew[i]['f043fapprover8'] != 0) {
                        if (this.loanlistingListNew[i]['f043fapprover8'] == 1) {
                            this.loanlistingListNew[i]['currentStatus'] = 'Approved by Registrar';
                        } else {
                            this.loanlistingListNew[i]['currentStatus'] = 'Rejected by Registrar ';

                        }
                    } else if (this.loanlistingListNew[i]['f043fapprover7'] != 0) {
                        if (this.loanlistingListNew[i]['f043fapprover7'] == 1) {
                            this.loanlistingListNew[i]['currentStatus'] = 'Approved by Vice Chancellor';
                        } else {
                            this.loanlistingListNew[i]['currentStatus'] = 'Rejected by Vice Chancellor';

                        }
                    } else if (this.loanlistingListNew[i]['f043fapprover6'] != 0) {
                        if (this.loanlistingListNew[i]['f043fapprover6'] == 1) {
                            this.loanlistingListNew[i]['currentStatus'] = 'Approved by Bursar';
                        } else {
                            this.loanlistingListNew[i]['currentStatus'] = 'Rejected by Bursar';

                        }
                    } else if (this.loanlistingListNew[i]['f043fapprover5'] != 0) {
                        if (this.loanlistingListNew[i]['f043fapprover5'] == 1) {
                            this.loanlistingListNew[i]['currentStatus'] = 'Approved by Verfiy Loan';
                        } else {
                            this.loanlistingListNew[i]['currentStatus'] = 'Rejected by Verfiy Loan';

                        }
                    } else if (this.loanlistingListNew[i]['f043fapprover4'] != 0) {
                        if (this.loanlistingListNew[i]['f043fapprover4'] == 1) {
                            this.loanlistingListNew[i]['currentStatus'] = 'Approved by Loan Checking';
                        } else {
                            this.loanlistingListNew[i]['currentStatus'] = 'Rejected by Loan Checking';

                        }
                    } else if (this.loanlistingListNew[i]['f043fapprover3'] != 0) {
                        if (this.loanlistingListNew[i]['f043fapprover3'] == 1) {
                            this.loanlistingListNew[i]['currentStatus'] = 'Approved by HOD Approval';
                        } else {
                            this.loanlistingListNew[i]['currentStatus'] = 'Rejected by HOD Approval';

                        }
                    } else if (this.loanlistingListNew[i]['f043fapprover2'] != 0) {
                        if (this.loanlistingListNew[i]['f043fapprover2'] == 1) {
                            this.loanlistingListNew[i]['currentStatus'] = 'Approved by Guarantors';
                        } else {
                            this.loanlistingListNew[i]['currentStatus'] = 'Rejected by Guarantors';

                        }
                    } else {
                        this.loanlistingListNew[i]['currentStatus'] = 'Pending for Approval';

                    }
                }
                this.loanlistingList = fianlListingEmp;

            }, error => {
                console.log(error);
            });
    }

    downloadPDF(id, type) {
        console.log(id);
        let newobj = {};
        newobj['idLoan'] = id;
        if (type == 1) {
            this.VehicleloanService.downloadVehicleReport(newobj).subscribe(
                data => {
                    console.log(data['name']);
                    window.open(this.downloadUrl + data['name']);
                }, error => {
                    console.log(error);
                });

        }
        else {
            this.VehicleloanService.downloadComputerReport(newobj).subscribe(
                data => {
                    console.log(data['name']);
                    window.open(this.downloadUrl + data['name']);
                }, error => {
                    console.log(error);
                });
        }
    }
    LoanlistingListData() {
        var confirmPop = confirm("Do you want to process?");
        if (confirmPop == false) {
            return false;
        }
        var loanlistingIds = [];
        for (var i = 0; i < this.loanlistingList.length; i++) {
            if (this.loanlistingList[i].processStatus == true) {
                loanlistingIds.push(this.loanlistingList[i].f052fid);
            }
        }
        var loanlistingUpdate = {};
        loanlistingUpdate['id'] = loanlistingIds;
        this.LoanlistingService.insertLoanlistingItems(loanlistingUpdate).subscribe(
            data => {
                alert("Loan Listing has been Processed");
                this.getListData();
            }, error => {
                console.log(error);
            });
    }

    selectAll() {
        console.log(this.selectAllCheckbox);
        for (var i = 0; i < this.loanlistingList.length; i++) {
            this.loanlistingList[i].processStatus = false;
        }
    }

}
