import { Injector } from '@angular/core';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LoanProcessingFeeService } from '../../service/loanprocessingfee.service';
import { LoannameService } from '../../service/loanname.service';
import { FundService } from '../../../generalsetup/service/fund.service';
import { ActivitycodeService } from '../../../generalsetup/service/activitycode.service';
import { DepartmentService } from '../../../generalsetup/service/department.service';
import { AccountcodeService } from '../../../generalsetup/service/accountcode.service';
@Component({
    selector: 'LoanProcessingFeeFormComponent',
    templateUrl: 'loanprocessingfeeform.component.html'
})

export class LoanProcessingFeeFormComponent implements OnInit {

    processingfeeList = [];
    processingfeeData = {};
    loannameList = [];
    fundList = [];
    activitycodeList = [];
    accountcodeList = [];
    DeptList = [];
    multipleFactList = [];
    ajaxCount: number;
    id: number;
    constructor(
        private LoanProcessingFeeService: LoanProcessingFeeService,
        private route: ActivatedRoute,
        private LoannameService: LoannameService,
        private router: Router,
        private ActivitycodeService: ActivitycodeService,
        private DepartmentService: DepartmentService,
        private AccountcodeService: AccountcodeService,
        private FundService: FundService,

    ) { }

    ngOnInit() {
        this.ajaxCount = 0;

        this.processingfeeData['f142fstatus'] = 1;
        this.processingfeeData['f142ffeeType']=1;
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        let multipleObj = {};
        multipleObj['name'] = 'Lesser Factor';
        this.multipleFactList.push(multipleObj);
        multipleObj = {};
        multipleObj['name'] = 'Greater Factor';
        this.multipleFactList.push(multipleObj);

        //drop down
        this.ajaxCount++;
        this.LoannameService.getActiveItems().subscribe(
            data => {
                this.ajaxCount--;
                this.loannameList = data['result']['data'];
            }, error => {
                console.log(error);
            });
            this.ajaxCount++;
            this.FundService.getItems().subscribe(
                data => {
                    this.ajaxCount--;
                    this.fundList = data['result']['data'];
                }, error => {
                });
    
            // activity dropdown
            this.ajaxCount++;
            this.ActivitycodeService.getItemsByLevel().subscribe(
                data => {
                    this.ajaxCount--;
                    this.activitycodeList = data['result']['data'];
                    // for (var i = 0; i < this.activitycodeList.length; i++) {
                    //     this.activitycodeList[i]['f058fcompleteCode'].trim();
                    // }
                }, error => {
                });
            // account dropdown
            this.ajaxCount++;
            this.AccountcodeService.getItemsByLevel().subscribe(
                data => {
                    this.ajaxCount--;
                    this.accountcodeList = data['result']['data'];
                }, error => {
                });
    
            // department dropdown
            this.ajaxCount++;
            this.DepartmentService.getItems().subscribe(
                data => {
                    this.ajaxCount--;
                    this.DeptList = data['result']['data'];
                }, error => {
                });
        if (this.id > 0) {
            this.LoanProcessingFeeService.getItemsDetail(this.id).subscribe(
                data => {
                    this.processingfeeData = data['result']['0'];
                    this.processingfeeData['f142fstatus'] = parseInt(this.processingfeeData['f142fstatus']);
                    this.processingfeeData['f142ffeeType'] = parseInt(this.processingfeeData['f142ffeeType']);
                }, error => {
                    console.log(error);
                });
        }
    }

    addProcessingFee() {
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        this.processingfeeData['f021famount'] = parseFloat(this.processingfeeData['f021famount']).toFixed(2);
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.LoanProcessingFeeService.updateProcessingFeeItems(this.processingfeeData, this.id).subscribe(
                data => {

                    alert("Loan Processing Fee has been Updated successfully");
                    this.router.navigate(['loan/loanprocessingfee']);
                }, error => {
                    console.log(error);
                });
        } else {
            this.LoanProcessingFeeService.insertProcessingFeeItems(this.processingfeeData).subscribe(
                data => {
                    alert("Loan Processing Fee has been added successfully");
                    this.router.navigate(['loan/loanprocessingfee']);

                }, error => {
                    console.log(error);
                });
        }
    }
    onlyNumberKey(event) {
        console.log(event);
        if (event.charCode == 46) {

        } else {
            return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
        }
    }
}