import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LoanProcessingFeeService } from '../service/loanprocessingfee.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'LoanProcessingFee',
    templateUrl: 'loanprocessingfee.component.html'
})
export class LoanProcessingFeeComponent implements OnInit {
    processingfeeList = [];
    processingfeeData = {};
    id: number;
    constructor(
        private LoanProcessingFeeService: LoanProcessingFeeService,
        private spinner: NgxSpinnerService,
    ) { }

    ngOnInit() {
        this.spinner.show();
        this.LoanProcessingFeeService.getItems().subscribe(
            data => {
                this.spinner.hide();
                this.processingfeeList = data['result']['data'];
            }, error => {
                console.log(error);
            });
    }
}