
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { VehicletypeService } from '../service/vehicletype.service';

@Component({
    selector: 'Vehicletype',
    templateUrl: 'vehicletype.component.html'
})
export class VehicletypeComponent implements OnInit {
    vehicletypeList = [];
    vehicletypeData = {};
    id: number;
    constructor(
        private VehicletypeService: VehicletypeService
    ) { }

    ngOnInit() {

        this.VehicletypeService.getItems().subscribe(
            data => {
                this.vehicletypeList = data['result']['data'];
                this.vehicletypeData['f042fstatus'] = parseInt(this.vehicletypeData['f042fstatus']);

            }, error => {
                console.log(error);
            });
    }
    addVehicletype() {
        console.log(this.vehicletypeData);
        this.VehicletypeService.insertVehicletypeItems(this.vehicletypeData).subscribe(
            data => {
                this.vehicletypeList = data['todos'];
            }, error => {
                console.log(error);
            });

    }
}

