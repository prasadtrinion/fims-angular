import { Injector } from '@angular/core';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { VehicletypeService } from '../../service/vehicletype.service';
import { AlertService } from './../../../_services/alert.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
    selector: 'VehicletypeFormComponent',
    templateUrl: 'vehicletypeform.component.html'
})

export class VehicletypeFormComponent implements OnInit {
    vehicleform: FormGroup;

    vehicletypeList = [];
    vehicletypeData = {};

    id: number;

    constructor(

        private VehicletypeService: VehicletypeService,
        private route: ActivatedRoute,
        private router: Router,
        private AlertService: AlertService
    ) { }

    ngOnInit() {

        this.vehicletypeData['f042fstatus'] = 1;
        this.vehicleform = new FormGroup({
            loanname: new FormControl('', Validators.required),
        });

        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.VehicletypeService.getItemsDetail(this.id).subscribe(
                data => {
                    this.vehicletypeData = data['result'][0];
                    this.vehicletypeData['f042fstatus'] = parseInt(this.vehicletypeData['f042fstatus']);
                    console.log(this.vehicletypeData);
                }, error => {
                    console.log(error);
                });
        }
    }
    addVehicletype() {

        console.log(this.vehicletypeData['f042fmonths']);
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.VehicletypeService.updateVehicletypeItems(this.vehicletypeData, this.id).subscribe(
                data => {
                    this.router.navigate(['loan/vehicletype']);
                    this.AlertService.success("Updated Sucessfully !!")

                }, error => {
                    console.log(error);
                });
        } else {
            this.VehicletypeService.insertVehicletypeItems(this.vehicletypeData).subscribe(
                data => {
                    this.router.navigate(['loan/vehicletype']);
                    this.AlertService.success("Added Sucessfully !!")
                });
        }
    }
}