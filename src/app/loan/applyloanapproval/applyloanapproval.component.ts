import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApplyloanapprovalService } from '../service/applyloanapproval.service'
import { AlertService } from './../../_services/alert.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'Applyloanapproval',
    templateUrl: 'applyloanapproval.component.html'
})

export class ApplyloanapprovalComponent implements OnInit {

    applyloanapprovalList = [];
    applyloanapprovalData = {};
    selectAllCheckbox = true;
    level: number;
    status: number;
    typeText: string;
    approveBtnLabel: string;
    rejectBtnLabel: string;

    constructor(
        private ApplyloanapprovalService: ApplyloanapprovalService,
        private AlertService: AlertService,
        private router: Router,
        private route: ActivatedRoute,
        private spinner: NgxSpinnerService,
    ) { }

    ngOnInit() {
        this.getListData();
    }
    getListData() {
        this.spinner.show();

        this.route.paramMap.subscribe((data) => this.level = + data.get('level'));
        this.route.paramMap.subscribe((data) => this.status = + data.get('status'));

        console.log(this.level);

        this.approveBtnLabel = "Approve";
        this.rejectBtnLabel = "Reject";

        switch (this.level) {
            case 0: this.typeText = 'Apply Loan';
                break;
            case 1: this.typeText = 'Guarantor';
                break;
            case 3: this.typeText = 'HOD Approval';
                break;
            case 4: this.typeText = 'Loan Checking';
                this.approveBtnLabel = "Received";
                this.rejectBtnLabel = "Cancel";
                break;
            case 5: this.typeText = 'Verify Loan';
                this.approveBtnLabel = "Verify";
                break;
            case 6: this.typeText = 'Bursar Approval';
                break;
            case 7: this.typeText = 'Vice Chancellor ';
                break;
            case 8: this.typeText = 'Registrar Approval';
                break;
            case 9: this.typeText = 'Offer Loan';
                this.approveBtnLabel = "Offer";
                break;
        }
        if (this.level == 1) {
            this.ApplyloanapprovalService.getPeronalList(this.level, this.status).subscribe(
                data => {
                    this.spinner.hide();
                    console.log(data['result']);
                    this.applyloanapprovalList = data['result']['data'];
                    for (var i = 0; i < this.applyloanapprovalList.length; i++) {
                        if (this.applyloanapprovalList[i]['f044fstatus'] == '1') {
                            this.applyloanapprovalList[i]['f044fstatus'] = 'Approved';
                        } else if (this.applyloanapprovalList[i]['f044fstatus'] == '2') {
                            this.applyloanapprovalList[i]['f044fstatus'] = 'Not Approved';
                        } else {
                            this.applyloanapprovalList[i]['f044fstatus'] = 'P';

                        }
                    }
                }, error => {
                    console.log(error);
                });
        } else {
            this.spinner.show();

            this.ApplyloanapprovalService.getItemsDetail(this.level, this.status).subscribe(
                data => {
                    this.spinner.hide();

                    console.log(data['result']);
                    this.applyloanapprovalList = data['result']['data'];
                }, error => {
                    console.log(error);
                });
        }
    }
    applyloanapprovalListData() {
        this.route.paramMap.subscribe((data) => this.level = + data.get('level'));

        console.log(this.applyloanapprovalList);
        var approvaloneIds = [];
        var approvalId = 0;
        for (var i = 0; i < this.applyloanapprovalList.length; i++) {
            if (this.applyloanapprovalList[i].f045fapprovalStatus == true) {
                approvalId = this.applyloanapprovalList[i].f043fid;
            }
        }
        var approvaloneUpdate = {};
        approvaloneUpdate['id'] = approvalId;
        approvaloneUpdate['level'] = this.level;
        approvaloneUpdate['status'] = '1';
        approvaloneUpdate['reason'] = this.applyloanapprovalData['reason'];

        if (this.level == 1) {
            console.log(this.applyloanapprovalList);
            var approvaloneIds = [];
            var approvalId = 0;
            for (var i = 0; i < this.applyloanapprovalList.length; i++) {
                if (this.applyloanapprovalList[i].f045fapprovalStatus == true) {
                    approvaloneIds.push(this.applyloanapprovalList[i].f044fid);
                }
            }
            var approvaloneUpdate = {};
            approvaloneUpdate['id'] = approvaloneIds;
            approvaloneUpdate['level'] = this.level;
            approvaloneUpdate['status'] = '1';
            approvaloneUpdate['reason'] = this.applyloanapprovalData['reason'];
            console.log("asdf")
            console.log("asdf")
            console.log(approvaloneUpdate);
            this.ApplyloanapprovalService.updateApplyloanapprovalItemsByUser(approvaloneUpdate).subscribe(
                data => {
                    this.AlertService.success(" Updated Sucessfully ! ");

                    this.getListData();
                }, error => {
                    console.log(error);
                });
        } else {
            console.log(this.applyloanapprovalList);
            var approvaloneIds = [];
            var approvalId = 0;
            for (var i = 0; i < this.applyloanapprovalList.length; i++) {
                if (this.applyloanapprovalList[i].f045fapprovalStatus == true) {
                    approvaloneIds.push(this.applyloanapprovalList[i].f043fid);
                }
            }
            if (approvaloneIds.length < 1) {
                alert("Please select atleast one loan to approve");
                return false;
            }
            var confirmPop = confirm("Do you want to approve?");
            if (confirmPop == false) {
                return false;
            }
            var approvaloneUpdate = {};
            approvaloneUpdate['id'] = approvalId;
            approvaloneUpdate['level'] = this.level;
            approvaloneUpdate['status'] = '1';
            approvaloneUpdate['reason'] = this.applyloanapprovalData['reason'];
            this.ApplyloanapprovalService.updateApplyloanapprovalItems(approvaloneUpdate).subscribe(
                data => {
                    this.AlertService.success(" Updated Sucessfully ! ");

                    this.getListData();
                }, error => {
                    console.log(error);
                });
        }
    }
    applyloanRejectListData() {
        this.route.paramMap.subscribe((data) => this.level = + data.get('level'));
        if (this.applyloanapprovalData['reason'] == undefined) {
            alert('Enter the Description to reject');
            return false;
        }
        console.log(this.applyloanapprovalList);
        var approvaloneIds = [];
        var approvalId = 0;
        for (var i = 0; i < this.applyloanapprovalList.length; i++) {
            if (this.applyloanapprovalList[i].f045fapprovalStatus == true) {
                approvaloneIds.push(this.applyloanapprovalList[i].f043fid);
            }
        }
        if (approvaloneIds.length < 1) {
            alert("Select atleast one loan to reject");
            return false;
        }
        var confirmPop = confirm("Do you want to Reject?");
        if (confirmPop == false) {
            return false;
        }
        var approvaloneUpdate = {};
        approvaloneUpdate['id'] = approvaloneIds;
        approvaloneUpdate['level'] = this.level;
        approvaloneUpdate['status'] = '2';
        approvaloneUpdate['reason'] = this.applyloanapprovalData['reason'];
        this.ApplyloanapprovalService.updateApplyloanapprovalItems(approvaloneUpdate).subscribe(
            data => {
                this.AlertService.success(" Updated Sucessfully ! ");

                this.getListData();
            }, error => {
                console.log(error);
            });
    }
}
