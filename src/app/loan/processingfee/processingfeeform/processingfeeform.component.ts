import { Injector } from '@angular/core';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ProcessingFeeService } from '../../service/processingfee.service';
import { LoannameService } from '../../service/loanname.service';
import { FundService } from '../../../generalsetup/service/fund.service';
import { ActivitycodeService } from '../../../generalsetup/service/activitycode.service';
import { DepartmentService } from '../../../generalsetup/service/department.service';
import { AccountcodeService } from '../../../generalsetup/service/accountcode.service';


@Component({
    selector: 'ProcessingFeeFormComponent',
    templateUrl: 'processingfeeform.component.html'
})

export class ProcessingFeeFormComponent implements OnInit {

    processingfeeList = [];
    processingfeeData = {};
    loannameList = [];
    fundList = [];
    activitycodeList = [];
    accountcodeList = [];
    DeptList = [];
    ajaxCount: number;
    id: number;
    edit: boolean = false;
    constructor(
        private ProcessingFeeService: ProcessingFeeService,
        private ActivitycodeService: ActivitycodeService,
        private DepartmentService: DepartmentService,
        private AccountcodeService: AccountcodeService,
        private FundService: FundService,
        private route: ActivatedRoute,
        private LoannameService: LoannameService,
        private router: Router

    ) { }

    ngOnInit() {
        this.ajaxCount = 0;

        this.processingfeeData['f021fstatus'] = 1;
        this.processingfeeData['f020floanName'];
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));

        //drop down
        this.ajaxCount++;
        // if (this.id > 0) {
            // this.edit = true;
            this.LoannameService.getActiveItems().subscribe(
                data => {
                    this.ajaxCount--;
                    this.loannameList = data['result']['data'];
                }, error => {
                    console.log(error);
                });
        // }
        // else {
        //     this.LoannameService.getUndefinedLoanProcessingFee().subscribe(
        //         data => {
        //             this.ajaxCount--;
        //             this.loannameList = data['result'];
        //         }, error => {
        //             console.log(error);
        //         });
        // }
        //fund dropdown
        this.ajaxCount++;
        this.FundService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.fundList = data['result']['data'];
            }, error => {
            });

        // activity dropdown
        this.ajaxCount++;
        this.ActivitycodeService.getItemsByLevel().subscribe(
            data => {
                this.ajaxCount--;
                this.activitycodeList = data['result']['data'];
                for (var i = 0; i < this.activitycodeList.length; i++) {
                    this.activitycodeList[i]['f058fcompleteCode'] = this.activitycodeList[i]['f058fcompleteCode'];
                    this.activitycodeList[i]['f058fcompleteCode'].trim();
                }
            }, error => {
            });
        // account dropdown
        this.ajaxCount++;
        this.AccountcodeService.getItemsByLevel().subscribe(
            data => {
                this.ajaxCount--;
                this.accountcodeList = data['result']['data'];
            }, error => {
            });

        // department dropdown
        this.ajaxCount++;
        this.DepartmentService.getItems().subscribe(
            data => {
                this.ajaxCount--;
                this.DeptList = data['result']['data'];
            }, error => {
            });

        if (this.id > 0) {
            this.edit = true;
            this.ProcessingFeeService.getItemsDetail(this.id).subscribe(
                data => {
                    this.processingfeeData = data['result']['0'];
                    this.processingfeeData['f021fstatus'] = parseInt(this.processingfeeData['f021fstatus']);
                }, error => {
                    console.log(error);
                });
        }
    }

    addProcessingFee() {
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        this.processingfeeData['f021famount'] = parseFloat(this.processingfeeData['f021famount']).toFixed(2);
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.ProcessingFeeService.updateProcessingFeeItems(this.processingfeeData, this.id).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert('Loan Processing Fee Already Exist');
                    }
                    else {
                        alert("Loan Processing Fee has been Updated successfully");
                    }
                    this.router.navigate(['loan/processingfee']);
                }, error => {
                    console.log(error);
                });
        } else {
            this.ProcessingFeeService.insertProcessingFeeItems(this.processingfeeData).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert("No duplication of Loan Processing Fee");
                    }
                    else {
                        this.router.navigate(['loan/loantypesetup']);
                        alert("Loan Processing Fee has been added successfully");
                    }
                    this.router.navigate(['loan/processingfee']);

                }, error => {
                    console.log(error);
                });
        }
    }
    onlyNumberKey(event) {
        console.log(event);
        if (event.charCode == 46) {

        } else {
            return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
        }
    }
}