import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ProcessingFeeService } from '../service/processingfee.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'ProcessingFee',
    templateUrl: 'processingfee.component.html'
})
export class ProcessingFeeComponent implements OnInit {
    processingfeeList = [];
    processingfeeData = {};
    id: number;
    constructor(
        private ProcessingFeeService: ProcessingFeeService,
        private spinner: NgxSpinnerService,
    ) { }

    ngOnInit() {
        this.spinner.show();
        this.ProcessingFeeService.getItems().subscribe(
            data => {
                this.spinner.hide();
                this.processingfeeList = data['result']['data'];
            }, error => {
                console.log(error);
            });
    }
}