import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { LoanlistingService } from "../service/loanlisting.service";
import { VehicleloanService } from "../service/vehicleloan.service";
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'loanhistory',
    templateUrl: 'loanhistory.component.html'
})

export class LoanhistoryComponent implements OnInit {

    loanhistoryList = [];
    loanhistoryData = {};
    selectAllCheckbox = true;
    loanlistingListNew = [];
    level: number;
    typeText: string;
    approveBtnLabel:string;
    rejectBtnLabel:string;
    constructor(
        private LoanlistingService: LoanlistingService,
        private VehicleloanService: VehicleloanService,
        private spinner: NgxSpinnerService

    ) { }

    ngOnInit() {
        this.getListData();
    }
    getListData() {
      
    }

   

}
