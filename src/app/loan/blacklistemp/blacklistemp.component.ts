import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { BlacklisttempService } from "../service/blacklisttemp.service";
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'BlackListEmp',
    templateUrl: 'blacklistemp.component.html'
})

export class BlackListEmpComponent implements OnInit {
    blacklistempListNew = [];
    blacklistempList = [];
    blacklistempData = {};
    id: number;
    title: string;
    type: string;

    constructor(
        private route: ActivatedRoute,
        private BlacklisttempService: BlacklisttempService,
        private spinner: NgxSpinnerService,
    ) { }
    ngOnInit() {
        this.spinner.show();
        this.BlacklisttempService.getItems().subscribe(
            data => {
                this.spinner.hide();
                this.blacklistempList = data['result']['data'];
            }, error => {
                console.log(error);
            });
    }
    
}