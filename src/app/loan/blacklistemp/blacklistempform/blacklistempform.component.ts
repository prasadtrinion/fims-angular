import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { StaffService } from "../../service/staff.service";
import { BlacklisttempService } from "../../service/blacklisttemp.service";
// import { release } from 'os';

@Component({
    selector: 'BlackListEmpFormComponent',
    templateUrl: 'blacklistempform.component.html'
})

export class BlackListEmpFormComponent implements OnInit {
    blacklistempList = [];
    blacklistempData = {};
    staffList = [];
    id: number;
    edit:boolean;
    constructor(
        private BlacklisttempService: BlacklisttempService,
        private StaffService: StaffService,
        private route: ActivatedRoute,
        private router: Router,

    ) { }
    ngOnInit() {
        this.blacklistempData['f018fstatus'] = 1;
        this.blacklistempData['f018fidStaff'] = '';

        //staff dropdown
        

        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
            if(this.id > 0){
                this.StaffService.getItems().subscribe(
                    data => {
                        this.staffList = data['result']['data'];
                    }, error => {
                        console.log(error);
                    });
            }
            else{
                this.StaffService.getNonBlackList().subscribe(
                    data => {
                        this.staffList = data['result']['data'];
                    }, error => {
                        console.log(error);
                    });
            }
        if (this.id > 0) {
            this.edit = true;
            this.BlacklisttempService.getItemsDetail(this.id).subscribe(
                data => {
                    this.blacklistempData = data['result'][0];
                    if (this.blacklistempData['f018freleaseDate'] == '1970-01-01') {
                        this.blacklistempData['f018freleaseDate'] = '';
                    }
                    this.blacklistempData['f018fstatus'] = parseInt(this.blacklistempData['f018fstatus']);
                }, error => {
                    console.log(error);
                });
        }
    }
    dateComparison() {

        let startDate = Date.parse(this.blacklistempData['f018feffectiveDate']);
        let endDate = Date.parse(this.blacklistempData['f018freleaseDate']);
        let currentDate = new Date();
        let tempDate = Date.parse(currentDate.toString());
        if (startDate > endDate) {
            alert("Blacklist From cannot be greater than Release Date !");
            this.blacklistempData['f018freleaseDate'] = "";
            return false;
        }
        if( startDate < tempDate ) {
            alert("Start Date should be greater than today !")
            this.blacklistempData['f018feffectiveDate'] = "";
            return false;
        }
        if(endDate == undefined){
            alert("Please enter release date !")
            return false;
        }

    }

    addblacklisttemp() {
        var confirmPop = confirm("Do you want to Save?");
        if (confirmPop == false) {
            return false;
        }
        this.route.paramMap.subscribe((data) => this.id = + data.get('id'));
        if (this.id > 0) {
            this.BlacklisttempService.updateBlacklisttempItems(this.blacklistempData, this.id).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert('Blacklist Staff Already Exist');
                    }
                    else {
                        alert("Blacklist Staff has been Updated successfully");
                    }
                    this.router.navigate(['loan/blacklistemp']);
                }, error => {
                    console.log(error);
                });
        } else {
            this.BlacklisttempService.insertBlacklisttempItems(this.blacklistempData).subscribe(
                data => {
                    if (data['status'] == 409) {
                        alert("No duplication of Blacklist Staff Name");
                    }
                    else {
                        this.router.navigate(['loan/loanname']);
                        alert("Staff has been Blacklisted");
                    }
                    this.router.navigate(['loan/blacklistemp']);

                }, error => {
                    console.log(error);
                });
        }
    }

}