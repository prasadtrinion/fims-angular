const path = require('path');
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const errorhandler = require('errorhandler');
const mongoose = require('mongoose');
const morgan = require('morgan');
const validator = require('express-validator');

const isProduction = process.env.NODE_ENV === 'production';
const app = express();
const router = express.Router();
const config = require('./server/config');

app.use(cors());
app.use(morgan('dev'));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(validator());
app.use(express.static(path.join(__dirname, 'dist')));

mongoose.connect(config.db.uri, function (err) {
    console.log(err); 
});

if (!isProduction) {
    app.use(errorhandler());
}

const routes = require('./server/routes')(app, router, config);

/// load index.html for all other routes
app.use(function(req, res, next) {
    res.status(200);
    res.sendFile(path.join(__dirname, 'dist/index.html'));
});

/// error handlers
if (isProduction) {
    // production error handler
    // no stacktraces leaked to user
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.json({
            error: err.message
        })
    });
} else {
    // development error handler
    // will print stacktrace

    app.use(function(err, req, res, next) {
        console.log(err.stack);

        res.status(err.status || 500);
        res.json({
            error: err.message
        })
    });   
}

const server = app.listen( process.env.PORT || 3000, function(){
    console.log('Listening on port ' + server.address().port);
});