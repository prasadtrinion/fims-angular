const isAuthenticated = function (req, res, next) {
    res.status(400);
    res.json({
        error: 'Authorization error'
    });
}

module.exports = {
    isAuthenticated: isAuthenticated
}