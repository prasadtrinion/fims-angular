const bcrypt = require('bcrypt');
const validator = require('../validators/users');
const User = require('../models/user');
const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');

const create = function (req, res) {
    let validationResult = validator.create(req);

    if (validationResult.status) {
        bcrypt.hash(req.body.password, 10, function(err, hash) {
            if (err) {
               return res.status(500).json({
                  error: err
               });
            } else {
               const user = new User({
                  _id: new  mongoose.Types.ObjectId(),
                  email: req.body.email,
                  password: hash    
               });
                
               user.save().then(function(result) {
                    const JWTToken = jwt.sign(
                        {
                            email: user.email,
                            _id: user._id
                        },
                        'secret',
                        { expiresIn: '2h' }
                    );
                    
                    return res.status(200).json({
                        token: JWTToken
                    });
               }).catch(error => {
                  return res.status(500).json({
                     error: err
                  });
               });
            }
         });
    } else {
        return res.status(422).json({
            error: validationResult.error
        });
    }
};

const get = function (req, res) {

};

module.exports = {
    create: create,
    get: get
}