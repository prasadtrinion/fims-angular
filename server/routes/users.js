module.exports = function (app, router, config) {
    const controller = require('../controllers/users');
    const jwt = require('../middlewares/jwt');

    router.route('/').post(controller.create);

    router.post('/api/users', jwt.isAuthenticated);

    app.use('/api/users', router);
};