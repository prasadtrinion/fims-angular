module.exports = function (app, router, config) {
    require('./auth')(app, router, config);
    require('./users')(app, router, config);
};