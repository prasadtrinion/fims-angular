module.exports = function (app, router, config) {
    const controller = require('../controllers/auth');

    router.route('/login').post(controller.login);

    app.use('/auth', router);
};