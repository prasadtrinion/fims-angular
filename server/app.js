const express = require('express');
const app = express();
app.use('/api/posts',(req,res,next)=>{
    const posts= [
        {
            id:"1",
            title:"title1"
        },
        {
            id:"2",
            title:"title2"
        }
    ];
    return res.status(200).json({
        message:"Success",
        posts:posts
    });
});

module.exports = app;